<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');   

class Home extends CI_Controller { 
    
    // Load Default View 
    public function index(){
       $data = array();
       $data['title'] = "Happy Edit - Home";  
       $this->load->view('ant_tool_page/ant_tool_view');
       
    }
    public function loadInitialHomeView()
    {
       $this->load->view('ant_tool_page/ant_tool_view');
    	
    }

    public function signinPage()
    {
       $this->load->view('dashboard/partials/ui-login');
    	
    }    
     public function signupPage()
    {
       $this->load->view('dashboard/partials/ui-register');
    	
    }
    public function password()
    {
       $this->load->view('dashboard/partials/password');
      
    }     
       
}