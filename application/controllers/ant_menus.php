<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ant_menus extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('menus_model'); 
        $_REQUEST = json_decode(file_get_contents('php://input'), true);
        

    }

	public function create_parent_menu($parent_menu_name,$project_id,$user_id)
	{
		echo $this->menus_model->create_parent_menu($parent_menu_name,$project_id,$user_id);  
	} 
	public function get_parent_menu()
	{
		echo json_encode($this->menus_model->get_parent_menu(), true);  
	}

	public function add_submenu($submenu_name,$parent_menu_id)
	{
		echo $this->menus_model->add_submenu($submenu_name,$parent_menu_id);
	}
	public function get_submenu($parent_menu_id)
	{
		echo json_encode($this->menus_model->get_submenu($parent_menu_id), true);
	}
	
}