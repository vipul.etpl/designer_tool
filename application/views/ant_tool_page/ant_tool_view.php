<?php
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html > 
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Happy Edit</title>
		<meta name="description" content=""/>	    
		<meta http-equiv="Content-Language" content="en-EN" />
		
		<!-- MOBILE SETUP -->
<!-- 		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
 -->		<!-- GLOBAL CSS -->	
 		<meta name="viewport" content="width=480, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,target-densitydpi=device-dpi, user-scalable=no" />
		<link rel="shortcut icon" href="<?php echo base_url()?>img/Fevicon.png" />
        <link rel="stylesheet" href="<?php echo base_url()?>css/ant_tool_css/jquery-ui.css">	
		<link rel="stylesheet" href="<?php echo base_url()?>css/ant_tool_css/system_style.css" />
		<link rel="stylesheet" href="<?php echo base_url()?>css/ant_tool_css/janvas_style.css" />
		<link rel="stylesheet" href="<?php echo base_url()?>css/ant_tool_css/animate.min.css" />
<!-- 		<link rel="stylesheet" href="<?php echo base_url()?>css/ant_tool_css/ant_tool_css/codemirror.css" />	
 -->		<link rel="stylesheet" href="<?php echo base_url()?>css/ant_tool_css/happystyle.css" />
		<link href="<?php echo base_url()?>css/ant_tool_css/media.css" rel="stylesheet"/>
		<link href="<?php echo base_url()?>css/ant_tool_css/font.css" rel="stylesheet"/>
		<link href="<?php echo base_url()?>css/ant_tool_css/font-awesome.css" rel="stylesheet"/>
		<!-- <link href="css/font-awesome.min.css" rel="stylesheet"/>	 -->
		<link href="img/favicon.ico" rel="shortcut icon"/>	

		<!--Scroll CSS here-->
		<link href="<?php echo base_url()?>css/ant_tool_css/jquery.mCustomScrollbar.css" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url()?>css/ant_tool_css/ruler.css" type="text/css" rel="stylesheet">

		<script src="<?php echo base_url()?>js/ant_tool_js/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>js/ant_tool_js/jQueryUI.js"></script>    		
		<script src="<?php echo base_url()?>js/ant_tool_js/jquery.mCustomScrollbar.js" type="text/javascript"></script>

		<!-- Happy Edit MINIFIED -->	    		
		<script language="javascript" src="<?php echo base_url()?>js/ant_tool_js/janvas_minified.js"></script>
		<script language="javascript" src="<?php echo base_url()?>js/ant_tool_js/jquery.ruler.js"></script>
		<script language="javascript" src="<?php echo base_url()?>js/ant_tool_js/modernizr-2.6.2.min.js"></script>
		
		<!-- GOOGLE FONT -->
		<script src="<?php echo base_url()?>js/ant_tool_js/antWebfont.js"></script>

		
		<!-- GOOGLE APIS -->
		<script type="text/javascript" src="<?php echo base_url()?>js/ant_tool_js/googleclient.js"></script>
		<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-73794668-1']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		
		</script>
	
	</head>

<body class="unselectable" >
<!-- <canvas class='ruler' id='ruler'></canvas> -->
		
	<div id="PRINTABLE_CONTENT" >
	</div>
	
	<div id="TEMPLATES" >
	</div> <!-- TEMPLATES -->
			
	<div id="APPLICATION" tabindex="1" >
		
		<!-- <div id="LAYOUT" > -->	
		<div class="header">

		<div class="top_navsection">
			<div class="page-topbar"><!-- START TOPBAR -->
				<div class="logo-area">
				    <a href="http://happyedit.com"><img src="<?php echo base_url()?>img/logo.png" style="height: 27px; padding: 5px 0px 0px;"></a>
				</div>
				<div class="quick-area ">
			    <div class="pull-left pagetitle">
			        <h1 class=""></h1>
			    </div>

			    <div class="edit_section">
					<ul>
						<li><a onmouseout="showMyMenu('docMenus', 'off')" onmouseover="showMyMenu('docMenus', 'on')" href="javascript:void(0);">File</a></li>
						<li><a onmouseout="showMyMenu('editMenus', 'off')" onmouseover="showMyMenu('editMenus', 'on')" href="javascript:void(0);">Edit</a></li>
						<li>
							<a onmouseout="showMyMenu('fontMenus', 'off')" onmouseover="showMyMenu('fontMenus', 'on')" href="javascript:void(0);">Tools</a>
							<ul style="display: none;" id="fontMenus" class="popupMenu dropShadow">
								<li onclick="showBlocks('fonts')" id="fontTools">Fonts</li>
								<li onclick="showBlocks('gradtools')" id="GradientTools">Gradient Tool</li>
								<li onclick="showBlocks('pattern')" id="patternTools">Pattern Tool</li>
								<li onclick="showBlocks('texture')" id="textureTools">Texture Tool</li>
							</ul>
						</li>
						<li><a onmouseout="showMyMenu('imageEditMenus', 'off')" onmouseover="showMyMenu('imageEditMenus', 'on')" href="javascript:void(0);">Image</a>
							<ul style="display: none;" id="imageEditMenus" class="popupMenu dropShadow">
								<li onclick="openFileBrowser()" id="">Image Drag</li>
								<li onclick="imageEditPopup()" id="">Edit Image</li>
							</ul>
						</li>
						<li>
							<a onclick="showTemplates('websiteTemplates')" href="javascript:void(0);">Library</a>
						</li>
						<!-- <li style="float:right;display:none;"><a>Welcome Aish</a></li> -->
						
						
						<li onclick="AnimationPanel()"><a href="javascript:void(0);" >Animation</a></li>
						<li style="display:none;"><a href="http://192.168.1.114/happEdit/happyEdit_demo/index.php/dashboard">Dashboard</a></li>
						<li style="display:none;"><a href="">Window</a></li>
						<li style="display:none;"><a href="">Help</a></li>
						<input type="hidden" id="userId" value="<?php echo $this->session->userdata("user_id"); ?>">

					</ul>
				</div>
				<div class="upperBtns"  style="float:left">
					<button class="myBtns blue" onclick="downloadPdf()">Download PDF</button>
				</div>
				<div class="btnRightBox">
					<button class="myBtns blue" onclick="previewSite()">Preview Site</button>
					<button class="myBtns red" onclick="openPublishPopups()">Publish Site</button>
				</div>
				<div class="user_profile_box">
					
				<ul>
				  <li >
				   <a>
				    <span class="user_pic">
				    	<?php 

				    	//$imgPth="base_url()"."saveImg/".base64_encode($this->session->userdata("username"))."/profile.png";
				    	$userId=$this->session->userdata("user_id");
				    	$encry_user=base64_encode($this->session->userdata("user_id"));
				    	$uImg=mysql_query("select images from ant_register where user_id='".$userId."'");
						while($rows=mysql_fetch_array($uImg))
						{
							if($rows['images']=="profile.png")
							{
								?>
								<img src="<?php echo base_url()?>saveImg/<?php echo $encry_user; ?>/profile.png" class="img-circle_avatar"> </span>
								<?php
							}
							else
							{
								?>
								<img src="../img/default-user-image.png" class="img-circle_avatar"> </span>
								<?php
							}
						}
						?>
				    	
				    <span class="user_name"><?php echo "Welcome ".$this->session->userdata("username");?></span>
				   </a>
				  </li>
				</ul>
				
				</div>
				
    
   	</div>
</div>
<!-- END TOPBAR -->
</div>

			<!--<div class="top_navsection">
				<div class="logo_section"><a href="index.html">Happy Edit</a></div>
				<div class="edit_section">
					<ul>
						<li><a href="javascript:void(0);" onmouseover="showDocMenu('on')" onmouseout="showDocMenu('off')">File</a></li>
						<li><a href="javascript:void(0);" onmouseover="showEditMenu('on')" onmouseout="showEditMenu('off')">Edit</a></li>
						<li>
							<a href="javascript:void(0);" onmouseover="showFontTools('on')" onmouseout="showFontTools('off')">Tools</a>
							<ul class="popupMenu dropShadow" id="fontMenus" style="display:none;" >
								<li id="fontTools" onclick="showBlocks('fonts')">Fonts</li>
								<li id="GradientTools" onclick="showBlocks('gradtools')">Gradient Tool</li>
							</ul>
						</li>
						<li style="display:none;"><a href="">Layer</a></li>
						<li style="display:none;"><a href="">Window</a></li>
						<li style="display:none;"><a href="">Help</a></li>
					</ul>
				</div>
			</div>-->
			
		<div id="blocks" class="toolBlocks" style="display:none;">
			<div class="blockTitle hbar"><span id="toolName">Tools</span><span class="closeBtn" onclick="closeToolBlocks()">&times;</span></div><p></p>
			<div class="blockContent">
				<div id="FONTS_LIBRARY_PANEL" class="myContent" style="display:none;" ></div>
				<div id="GRADIENTS_LIBRARY_PANEL" class="myContent" style="display:none;"></div>
				<div id="PATTERNS_LIBRARY_PANEL" class="myContent" style="display:none;"></div>
				<div id="TEXTURE_LIBRARY_PANEL" class="myContent" style="display:none;"></div>
				<div id="VIDEO_LIBRARY_PANEL" class="myContent" style="display:none;"></div>
				
				<div id="TEXT_SAMPLE_LIBRARY_PANEL" class="myContent" style="display:none;"></div>
				<!--<img src='img/720.gif' id="myLoadrs" style="display:none;width:110px;height:40px;margin-top:80px;">-->
			</div>
		</div>

			<div class="tool_optional_section" >
				<div class="tol_btn " style="display:none;"><a href="index.html">Tools Option <i class="fa fa-angle-right"></i></a></div>
				<div class="top_arow" style="display:none;"><img src="<?php echo base_url()?>img/ant_tool_img/arow.jpg"></div>
				<div class="no_obj" style="display:none;">No Object Selected</div>
				<div class="no_obj_shap" style="display:none;">
					<div class="shap_wrapper mr10"><img src="<?php echo base_url()?>img/ant_tool_img/1.png"></div>
					<div class="shap_wrapper sm"><img src="<?php echo base_url()?>img/ant_tool_img/3.png"></div>
					<div class="shap_wrapper sm"><img src="<?php echo base_url()?>img/ant_tool_img/4.png"></div>
					<div class="shap_wrapper sm"><img src="<?php echo base_url()?>img/ant_tool_img/5.png"></div>
				</div>
				<div class="no_obj_defult" style="display:none;">
					<input type="checkbox" value="Bike" name="vehicle">Default Fill and Stroke
				</div>
				<div class="no_obj_align" style="display:none;">
					<div class="blank_aligment">
						<span class="align_bl"><img src=""></span>
						<span class="align_bl"><img src=""></span>
						<span class="align_bl"><img src=""></span>
					</div>
					<div class="blank_aligment pl0">
						<span class="align_bl"><img src=""></span>
						<span class="align_bl"><img src=""></span>
						<span class="align_bl"><img src=""></span>
					</div>
					<div class="blank_aligment">
						<span class="align_bl"><img src=""></span>
						<span class="align_bl"><img src=""></span>
						<span class="align_bl"><img src=""></span>
					</div>
					<div class="blank_aligment pdl0">
						<span class="align_bl"><img src=""></span>
						<span class="align_bl"><img src=""></span>
						<span class="align_bl"><img src=""></span>
					</div>
				</div>
			</div>
			

		</div>			


			
		<div id="CONTENT" >

			<div class="lt_menu_section">
				<!-- MAIN_TOOLBAR_2D -->
					<div class="tools_heading"><a href="javascript:void(0)"> Tools <i class="fa fa-angle-down"></i></a></div>
					<div class="">

					<ul id="MAIN_TOOLBAR_2D" class="toolbar main_tools hbar "  >	
					
					<li id="SELECT_GLOBAL_2D_toolButton"  toolName="selectGlobal_tool" localize="title" title="Objects Selection" >
						<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/11_1.png"></a>
					</li>
				
				<li id="SELECT_LOCAL_2D_toolButton"  toolName="selectLocal_tool" localize="title" title="Points Selection" >
					<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/points.png" style="height: 15px;margin: 8px 0 0;width: 15px;">
					</a>
				</li>

				
		    	<li id="PAN_2D_toolButton"  toolName="panView_tool"  title="Hand Tool"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/Hand-tool-icon.png"></a></li>
		    	<li id="ZOOM_2D_toolButton"  toolName="zoomView_tool"  title="Zoom In/Zoom Out"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/28.png"></a></li>
				
				
				<li id="PATH_TOOLS_2D_toolButton" class="arrowSubElements" toolName="drawPath_tool" title="Path Selection">
					<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/penTool.svg"></a>
		        	<ul class="toolbar hbar dropShadow shr">
				        <li id="PATH_2D_toolButton"  toolName="drawPath_tool"  title="Path Creation" onclick="setIconToShape('PATH_2D_toolButton a','<?php echo base_url()?>img/ant_tool_img/janvas_icons/penTool.svg')"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/penTool.svg"></a></li>

				        <li id="ADD_PATH_POINT_2D_toolButton"  toolName="addPathPoint_tool" title="Add Path" onclick="setIconToShape('ADD_PATH_POINT_2D_toolButton a','<?php echo base_url()?>img/ant_tool_img/janvas_icons/penTooladd.svg')"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/penTooladd.svg"></a></li>

						<li id="REMOVE_PATH_POINT_2D_toolButton"  toolName="removePathPoint_tool" title="Remove Path" onclick="setIconToShape('REMOVE_PATH_POINT_2D_toolButton a','<?php echo base_url()?>img/ant_tool_img/janvas_icons/penToolminus.svg')"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/penToolminus.svg"></a></li>

				        <li id="SPLIT_PATH_2D_toolButton"  toolName="splitPath_tool"  title="Delete Path" onclick="setIconToShape('SPLIT_PATH_2D_toolButton a','<?php echo base_url()?>img/ant_tool_img/janvas_icons/penCut.svg')"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/penCut.svg"></a></li>

				        <li id="CONVERT_PATH_POINT_2D_toolButton"  toolName="convertPathPoint_tool" title="Convert Path" onclick="setIconToShape('CONVERT_PATH_POINT_2D_toolButton a','<?php echo base_url()?>img/ant_tool_img/janvas_icons/pens.svg')"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/pens.svg"></a></li>
		        	</ul>
		        	<a></a>       	
		        </li>

		        <li id="DRAW_FREEHAND_TOOLS_2D_toolButton" class="arrowSubElements" toolName="drawFreeHand_tool"  title="Free Drawing">
		        	<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/pencils.svg"></a>
					<ul class="toolbar hbar dropShadow shr" >
						<li id="FREEHAND_2D_toolButton"  toolName="drawFreeHand_tool" title="Pencil" onclick="setIconToShape('FREEHAND_2D_toolButton a','<?php echo base_url()?>img/ant_tool_img/janvas_icons/pencils.svg')"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/pencils.svg"></a></li>
						<li id="BRUSH_2D_toolButton"  toolName="drawBrush_tool"  title="Brush" onclick="setIconToShape('BRUSH_2D_toolButton a','<?php echo base_url()?>img/ant_tool_img/janvas_icons/brushs.svg')"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/brushs.svg"></a></li>
					</ul>
					<a></a>
				</li>
				
		        <li id="DRAW_TOOLS_2D_toolButton" class="arrowSubElements " toolName="drawRectangle_tool" title="Shape">
		        	<a><img class="mCS_img_loaded shapeIconsSize" src="<?php echo base_url()?>img/ant_tool_img/shapes.png"></a>
		        	<ul class="toolbar hbar dropShadow shr">
						<li id="RECT_2D_toolButton"  toolName="drawRectangle_tool" title="Rectangle" onclick="setIconToShape('RECT_2D_toolButton a','<?php echo base_url()?>img/ant_tool_img/shapes.png')"><a><img class="mCS_img_loaded shapeIconsSize" src="<?php echo base_url()?>img/ant_tool_img/shapes.png"></a></li>
						<li id="ELLIPSE_2D_toolButton"  toolName="drawEllipse_tool" onclick="setIconToShape('ELLIPSE_2D_toolButton a','<?php echo base_url()?>img/ant_tool_img/circle2.png')"  title="Circle"><a><img class="mCS_img_loaded shapeIconsSize" src="<?php echo base_url()?>img/ant_tool_img/circle2.png"></a></li>
						<li id="LINE_2D_toolButton"  toolName="drawLine_tool" title="Line" onclick="setIconToShape('LINE_2D_toolButton a','<?php echo base_url()?>img/ant_tool_img/line-2.png')"><a><img class="mCS_img_loaded shapeIconsSize" src="<?php echo base_url()?>img/ant_tool_img/line-2.png"></a></li>
						
					</ul>
		        	<a></a>       	
		        </li>
				
				<li id="TEXT_2D_toolButton"  toolName="drawTextBox_tool" title="Text Tool" ><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/25.png"></a></li>
				
				<li id="EDIT_GRAPHIC_ATTRIBUTES_2D_toolButton" class="arrowSubElements" toolName="eyeDropper_tool" title="Color Picker">
					<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/eyeDrop.svg"></a>
					<ul class="toolbar hbar dropShadow shr">
						<li id="EDIT_GRADIENT_POINTS_2D_toolButton"  toolName="editGradientPoints_tool" title="Gradient Tool" onclick="setIconToShape('EDIT_GRADIENT_POINTS_2D_toolButton a','<?php echo base_url()?>img/ant_tool_img/janvas_icons/gradPointer.svg')"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/gradPointer.svg"></a></li>

						<li id="EYEDROPPER_2D_toolButton"  toolName="eyeDropper_tool" title="Color Dropper" onclick="setIconToShape('EYEDROPPER_2D_toolButton a','<?php echo base_url()?>img/ant_tool_img/janvas_icons/eyeDrop.svg')"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/eyeDrop.svg"></a></li>
					</ul>
					<a></a>
				</li>

				<li id="EDIT_TRANSFORM_2D_toolButton" class="arrowSubElements"  title="Rotate Tool">
					<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/rotations.svg"></a>
					<ul class="toolbar hbar dropShadow shr">
						<li id="ROTATE_2D_toolButton"  toolName="rotate_tool"  title="Free Rotation"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/rotations.svg"></a></li>

						<li id="HOR_REFLECT_toolButton"  title="Vertical Rotation" ><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/verticals.svg"></a></li>

						<li id="VER_REFLECT_toolButton"  title="Horizontal Rotation" ><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/horizontal.svg"></a></li>
					</ul>
					<a></a>
				</li>

				<li id="BRING_FRONT_toolButton" class="arrowSubElements" style="display:none;">
					<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/29.png"></a>
					<ul class="toolbar hbar dropShadow shr">
						<li id="BRING_UP_toolButton"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/29.png"></a></li>
					</ul>
					
				</li>
				<li id="SEND_BACK_toolButton" class="arrowSubElements" style="display:none;">
					<a><img class="mCS_img_loaded" src=""></a>
					<ul class="toolbar hbar dropShadow shr">
						<li id="BRING_DOWN_toolButton"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/30.png"></a></li>
					</ul>
					<a></a>
				</li>
				
				
				<li id="ALIGNMENTS_toolButton" class="arrowSubElements" title="Text Alignment Tool">
					<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/center.svg"></a>
					<ul class="toolbar hbar dropShadow ali">
						<li id="TEXT_ALIGN_LEFT" title="Left Alignment" onclick="setIconToShape('ALIGNMENTS_toolButton a','<?php echo base_url()?>img/ant_tool_img/janvas_icons/left.svg')"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/left.svg"></a></li>

						<li id="TEXT_ALIGN_CENTER" title="Center Alignment" onclick="setIconToShape('ALIGNMENTS_toolButton a','<?php echo base_url()?>img/ant_tool_img/janvas_icons/center.svg')"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/center.svg"></a></li>

						<li id="TEXT_ALIGN_RIGHT" title="Right Alignment" onclick="setIconToShape('ALIGNMENTS_toolButton a','<?php echo base_url()?>img/ant_tool_img/janvas_icons/right.svg')"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/right.svg"></a></li>
		

						<li id="ALIGN_LEFT"  style="display:none;"><a><img class="mCS_img_loaded" src=""></a></li>
		        		<li id="ALIGN_VCENTER"  style="display:none;"><a><img class="mCS_img_loaded" src=""></a></li> 
				        <li id="ALIGN_RIGHT"  style="display:none;"><a><img class="mCS_img_loaded" src=""></a></li>
				        
				        <li id="ALIGN_TOP"  style="display:none;"><a><img class="mCS_img_loaded" src=""></a></li>
				        <li id="ALIGN_HCENTER" style="display:none;"><a><img class="mCS_img_loaded" src=""></a></li>
				        <li id="ALIGN_BOTTOM" style="display:none;"><a><img class="mCS_img_loaded" src=""></a></li>
		
				        <li id="DISTRIBUITE_LEFT" style="display:none;"><a><img class="mCS_img_loaded" src=""></a></li>
		        		<li id="DISTRIBUITE_VCENTER" style="display:none;"><a><img class="mCS_img_loaded" src=""></a></li> 
				        <li id="DISTRIBUITE_RIGHT" style="display:none;"><a><img class="mCS_img_loaded" src=""></a></li>
				        
				        <li id="DISTRIBUITE_TOP" style="display:none;"><a><img class="mCS_img_loaded" src=""></a></li>
				        <li id="DISTRIBUITE_HCENTER" style="display:none;"><a><img class="mCS_img_loaded" src=""></a></li>
				        <li id="DISTRIBUITE_BOTTOM" style="display:none;"><a><img class="mCS_img_loaded" src=""></a></li>
				        
				        <li id="DISTRIBUITE_HSPACE" style="display:none;"><a><img class="mCS_img_loaded" src=""></a></li>
				        <li id="DISTRIBUITE_VSPACE" style="display:none;"><a><img class="mCS_img_loaded" src=""></a></li>
		        	</ul>
				</li>
				
				<li id="TRASH_toolButton" title="Trash Tool"><a><i class="fa fa-trash mCS_img_loaded" style="color: rgb(204, 204, 204); font-size: 19px; margin-top:5px;"></i></a></li>
				<li id="UNDO_toolButton" title="Undo Tool"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/undo.svg"></a></li>
		    	<li id="REDO_toolButton" title="Redo Tool"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/redo.svg"></a></li>
		    	<li id="CUSTOM_MENU_toolButton" title="Custom menu"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/mobile-menu-icon.png"></a></li>

				<input type="file" id="fileUplaods" style="opacity:0.0;width:50px;">
				<li id="PRINT_toolButton" style="display:none;"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/35.png"></a></li>
				<li id="SAVE_toolButton" class="arrowSubElements"  >
					<a style="display:none;"><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/36.png"></a>
					<ul class="popupMenu dropShadow nedoc" id="docMenus" style="display:none;left:-1px; top:100%;" >
						<li  id="CREATE_FILE_HTML_menuItem" class="html_icon16x16" style="background-size: 16px 16px; background-position-x: 8px;" docTitle="Untitled" fileExtension="html" >New document</li>
						<li id="" onclick="openProjects()"><a href="javascript:void(0);">Projects</a></li>
					    <li id="DRIVE_FILE_BROWSER_WIN_button"><a href="javascript:void(0);">Open file</a></li>
					    <li class="divisor" style="display:none;"></li>
						<li id="OPEN_menuItem" style="display:none;">Open…</li>
						<li id="OPEN_GOOGLE_DRIVE_menuItem"  style="display:none;"><a style="color:white;" href="https://drive.google.com" target="_blank" >Open Google Drive</a></li>
						<li class="divisor" style="display:none;"></li>
				        <li id="SAVE_menuItem" style="display:block;">Save HTML</li>
				        <li id="" onclick="rename_file()">Rename File</li>
				        <li id="SAVE_AS_menuItem" style="display:none;">Save as...</li>
				        <li class="divisor" style="display:none;"></li>
				        <li id="UPLOAD_FILE_menuItem" style="display:none;position:relative;" >Upload file...<input id="UPLOAD_FILE_TEXTFIELD" type="file" style="position:absolute; display: block; left:0; top:0; opacity:0; width:100%;"/></li>
				        <li class="divisor" style="display:none;"></li>
				        <li id="SAVE_AS_START_TEMPLATE_menuItem" style="display:none;">Save</li>
				        <li id="RESET_START_TEMPLATE_menuItem" style="display:none;">Clear</li>
				        <li class="divisor" ></li>
						<li id="GENERATE_IMAGE_PNG_menuItem" draggable="true" >Save as PNG image</li>
						<li id="GENERATE_IMAGE_JPG_menuItem" draggable="true" style="display:none;">Preview JPG image</li>						
		        	</ul>
		        	<!--Gif creation code-->
					
					<!--end of Gif creation code-->
		        	
				</li>
				<div class="frameAnimation" id="frmAnim" style="display:none;">
						<div class="faHeader"><span class="pull-left animationTitle">Animation Panel</span><span class="pull-right" onclick="AnimationPanel()"><a href="javascript:void(0);"><img src="<?php echo base_url();?>img/deletes2.png" style="margin: 6px 7px 0 0;width: 20px;"></a></span></div>
						<div class="faBody">
							<ul id="imagePanels">
								<!--<li class="imgOutPnl"><img src="../img/default_image1.gif" class="imgSize"></li>-->
							</ul>
						</div>
						<div class="faFooter">
							<ul class="">
								<li class="footerTexts" id="saveImgForGifs"><a href="javascript:void(0);">Get Active Layer</a></li>
								<li class="delayCls">
									Delay Time: <input type="number" id="delayTime" value="0" min="0" max="50" step="0.1" title="Delay Time" style="background: #464646 none repeat scroll 0 0;border: 1px solid #999999;color: #cccccc;width: 50px;">
								</li>
								<!--<li class="delayCls">
									Loop: <input type="number" id="loopTime" value="0" min="0" max="50" step="1" title="Loop Time" style="width:60px;">
								</li>-->
								<li class="footerTexts"><a href="javascript:void(0);" onclick="generateGIFS()" title="Generate GIF">Generate GIF</a></li>

								</ul>
						</div>
					</div>

				<!-- <li id="DRIVE_FILE_BROWSER_WIN_button" style="display:none;"><a><img class="mCS_img_loaded" src="img/32.png"></a></li> -->
		    	<li id="HELP_toolButton" style="display:none;"><a><img class="mCS_img_loaded" src=""></a></li>	
				<li id="SELECTION_ACTIONS_2D_toolButton" class="arrowSubElements">
					<a style="display:none;"><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/28.png"></a>
					<ul class="popupMenu dropShadow alfld" id="editMenus" style="display:none;left:-1px; top:100%;" >
				        <li id="CUT_menuItem" >Cut</li>
				        <li id="COPY_menuItem" >Copy</li>
				        <li id="PASTE_menuItem" >Paste</li>
				        <li id="PASTEMY_menuItem" STYLE="display:none;">mY Paste<span class="keyboardTitle">Ctrl + V</span></li>
				        <li id="PASTE_IN_PLACE_menuItem" >Paste in place</li>
				        <li class="divisor"></li>
				        <li id="SELECT_ALL_menuItem" >Select all</li>
				        <li class="divisor"></li>
				        <li id="DUPLICATE_menuItem"  >Duplicate</li>
				        <li class="divisor"></li>
		        		<li id="GROUP_menuItem">Group</li>
		        		<li id="UNGROUP_menuItem"   >Ungroup</li>
		        		<li class="divisor"></li>
						<li id="LOCK_SELECTION_menuItem" >Lock selection</li>
						<li id="UNLOCK_ALL_menuItem" >Unlock all</li>
						<li class="divisor" style="display:none;"></li>
		        		<li id="CREATE_COMPOSED_PATH_menuItem" style="display:none;">Compose Path</li>
		        		<li id="CREATE_MASK_menuItem" style="display:none;">Create Mask</li>
		        		<li class="divisor" style="display:none;"></li>			        
				        <li id="JOIN_POINTS_menuItem" style="display:none;">Join points</li>
				        <li id="TEXT_TO_PATH_menuItem" style="display:none;">Text to path</li>
				        <li class="divisor" style="display:none;"></li>			        
				        <li id="EFFECT_OUTER_SHADOW_menuItem" effectClassName="XOS_Shadow_effect" effectProperties="{'shadowOffsetX':5,'shadowOffsetY':5,'shadowBlur':5,'shadowColor':'rgba(0,0,0,.5)'}" onclick="hideSubmenu('editMenus')">Outer shadow</li>			        
				        <li id="EFFECT_INNER_SHADOW_menuItem" effectClassName="XOS_Shadow_effect" effectProperties="{'shadowOffsetX':-1,'shadowOffsetY':-1,'shadowBlur':4,'shadowColor':'rgba(0,0,0,.75)'}" onclick="hideSubmenu('editMenus')">Inner shadow</li>			        
		        		<li class="divisor" style="display:none;"></li>			        
				        <li id="USE_IMAGE_AS_PATTERN_menuItem" style="display:none;">Use Image as Pattern</li>
		        	</ul>
				</li>
				</ul><!-- MAIN_TOOLBAR_2D -->
			</div>

			<div class="lay" style="display:none;">
				<div class="tools_heading liht"><a href=""> Pages <i class="fa fa-angle-right"></i></a></div>
				<div class="tools_heading liht"><a href=""> Layers <i class="fa fa-angle-right"></i></a></div>
			</div>
				
			</div>
				
				
				<div class="column" style="left:0; right:370px;" >
					<ul id="" class="tabBar"></ul>
					<div id="DOCUMENTS" class="radioPanels"></div>
				</div>
				
				<div class="myPanelBorders" style="right:194px;" ></div>
				
				<div class="column" id="pageLayerPanel" style="width:194px; right:175px; background-color:#333333;border-right: 1px solid #777777;" >
					<div class="page_heading">Layers</div>

					<div id="WIN_CONTENT" class="content" style="display:block !important;">				
							<div id="LAYERS_2D_PANEL" class="listView scrollableY darkScrollbar" ></div>
							<ul id="LAYERS_EDITING_TOOLBAR" class="footer toolbar hbar" style="overflow:visible;" >
				        		<li id="CREATE_LAYER_button" title="Add Layer"><a><i class="fa fa-plus"></i></a></li>
				        		<li id="REMOVE_LAYER_button" title="Remove Layer"><a><i class="fa fa-trash"></i></a></li>
				        		<li id="MOVE_LAYER_UP_button" title="Move Layer To Up"><a><img src="<?php echo base_url()?>img/ant_tool_img/bring_up_icon.png"></a></li>
				        		<li id="MOVE_LAYER_DOWN_button" title="Move Layer To Down"><a><img src="<?php echo base_url()?>img/ant_tool_img/bring_down_icon.png"></a></li>
				        		<li id="MOVE_SELECTION_IN_LAYER_button" title="" style="display:none;"><a></a></li>
				        		<li id="DUPLICATE_LAYER_button" title="Duplicate Layer"><a><img src="<?php echo base_url();?>img/copy-xxl.png"></a></li>
				        		<li><a style="float: right; margin: 0px 0px 0px 0px;" title="Rename layer" href="javascript:void(0);"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a></li>
							</ul>				
					</div>
					
					
					<div class="pages_wrapper">
					<div class="page_heading">Pages<div class="" style="float:right;margin: 0 19px 0 0;"><a href="javascript:void(0)" onclick="rename_file()" title="rename file" style="margin: 0 9px 0 0;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="javascript:void(0)" onclick="dublicate_page()" title="dublicate page" style="margin: 0 9px 0 0;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="javascript:void(0)" id="addPages" class="html_icon16x16" docTitle="Untitled" fileExtension="html" title="add page" style="margin: 0 9px 0 0;"><i class="fa fa-plus"></i></a></div></div>
					<div class="page_heading"><div id="currentProject" style="float:left;"></div></div>
					<div class="page_list_wrapper ht400 all_tool_section">
						<ul id="TABS_DOCUMENTS"></ul>
					</div>
				</div>

					<div id="LAYERS_WIN" class="win dropShadow layers_win closed"  style="display:none;">
						<div id="WIN_HEAD" class="head hbar">
							<div id="WIN_TITLE" class="title">Layers</div>
						</div>
						<div id="WIN_CLOSE_BUTTON" class="close_button">X</div>
						<div id="WIN_COLLAPSE_BUTTON" class="collapse_button"></div>
						<div id="WIN_CONTENT" class="content"  >				
							<div id="LAYERS_2D_PANEL" class="listView scrollableY darkScrollbar" ></div>
							<ul id="LAYERS_EDITING_TOOLBAR" class="footer toolbar hbar" style="overflow:visible;" >
				        		<li id="CREATE_LAYER_button" title=""><a><i class="fa fa-plus"></i></a></li>
				        		<li id="REMOVE_LAYER_button" title=""><a><i class="fa fa-trash"></i></a></li>
				        		<li id="MOVE_LAYER_UP_button" title=""><a><img src="<?php echo base_url()?>img/ant_tool_img/bring_up_icon.png"></a></li>
				        		<li id="MOVE_LAYER_DOWN_button" title=""><a><img src="<?php echo base_url()?>img/ant_tool_img/bring_down_icon.png"></a></li>
								<li id="MOVE_SELECTION_IN_LAYER_button"   title=""><a></a></li>
							</ul>				
						</div>
						<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
					</div>

				</div>


				<div class="column" id="propertyPanel" style="width:175px; right:0; background-color: #333333;" >
				
					<div class="page_heading">Properties</div>

					<div id="INSPECTOR_PANEL" class="scrollableY darkScrollbar radioPanels" >
					
					
					<div id="PAGE_PROPERTIES_2D_PANEL" >
							
							<div class="title" ></div>
							
							
							<div class="inspector_parameter parameter_pageFormats" style="display:none">	
							  	<!-- <div class="title">Page presets</div> -->
							  	<div class="property_container"  >
									<!-- <label for="DOC_SIZE_MENU">d</label> -->
									<select id="DOC_SIZE_MENU" style="display:none"   >
										<!-- <option selected="true" value="">Format</option>
										<optgroup label="Press">
											<option  value="595,842">A4 Vertical</option>
											<option  value="842,595">A4 Horizontal</option>
											<option  value="842,1190">A3 Vertical</option>
											<option  value="1190,842">A3 Horizontal</option>
											<option  value="516,728">B5  Vertical</option>
											<option  value="728,516">B5  Horizontal</option>
											<option  value="728,1032">B4  Vertical</option>
											<option  value="1032,728">B4  Horizontal</option>
											<option  value="792,1224">Tabloid Horizontal</option>
											<option  value="1224,792">Tabloid Vertical</option>
											<option  value="612,1008">Legal Horizontal</option>
											<option  value="1008,612">Legal Vertical</option>
											<option  value="612,792">Lettera Horizontal</option>
											<option  value="792,612">Lettera Vertical</option>
										</optgroup>
										<optgroup label="Web">
											<option  value="1024,768">1024 x 768</option>
											<option  value="980,600">980 x 600</option>
											<option  value="640,480">640 x 480</option>
										</optgroup>
										<optgroup label="Mobile">
											<option  value="320,240">320 x 240</option>
											<option  value="640,480">640 x 480</option>
											<option  value="640,320">640 x 320</option>
										</optgroup> -->
									</select>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_pageSize" >	
							  	<div class="title">Page Size</div>
							  	<div class="property_container"  >
							  		<label for="sizeX">x</label>
							  		<input  type="number" name="x" id="sizeX" onclick="this.select();" value="595"/>
							  	</div>
							  	<div class="property_container">														
							  		<label for="sizeY">y</label>
							  		<input  type="number" name="y" id="sizeY" onclick="this.select();"  value="842"/>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter parameter_pageAlignment" style="display:none" >	
							  	<div class="title">Page deployment</div>
							  	<div class="property_container"  >
									<label for="pageAlignment">PG</label>
									<select id="pageAlignment" name="pageAlignment"  >
											<option value="none">none</option>
											<option value="center-horizontally">center horizontally</option>
											<option value="center-horizontally-vertically">center horizontally vertically</option>
											<option value="no-margins">no margins</option>
									</select>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter" style="display:none">	
							  	<div class="title">Page background</div>
							  	<div class="property_container">
						  			<div class="color_container">
						  				<div id="bodyBackgroundColor" class="color">
						  				</div>
						  			</div>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_unityAndDecimal"  style="display:none">	
							  	<div class="title">units / decimals</div>
							  	
							  	<div class="property_container" >
							  		<label  for="UNITY_MENU">u</label>
									<select id="UNITY_MENU" >
										<option selected="true" value="UNIT_PIXEL">px</option>
										<option value="UNIT_MILLIMETER">mm</option>
										<option value="UNIT_CENTIMETER">cm</option>
										<option value="UNIT_METER">m</option>
										<option value="UNIT_KILOMETER">km</option>
										<option value="UNIT_FOOT">foot</option>
										<option value="UNIT_INCH">inch</option>
										<option value="UNIT_YARD">yard</option>
										<option value="UNIT_MILE">mile</option>
										<option value="UNIT_NAUTICAL_MILE">nautical mile</option>
									</select>
							  	</div>
							  	<div class="property_container">
									<label for="UNITY_DECIMALS_MENU">d</label>
									<select id="UNITY_DECIMALS_MENU"   >
										<option value="0">0</option>
										<option value="1">1</option>
										<option selected="true" value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
									</select>
							  	</div>
						  	</div>
						  	
						  	
						  	<div class="inspector_parameter  parameter_snapValue" style="display:none">	
							  	<div class="title">Snap range</div>
							  	<div class="property_container ">
							  		<label for="screenSnapValue">s</label>
							  		<input  type="text" name="screenSnapValue" id="screenSnapValue" onclick="this.select();" value="0"/>
							  	</div>
						  	</div>
						  	
						  	
						  	<div class="inspector_parameter  parameter_grid" style="display:none">	
							  	<div class="title">Snap grid</div>
							  	<div class="property_container">
							  		<label for="gridSnapValue">v</label>
							  		<input  type="text" name="gridSnapValue" id="gridSnapValue" onclick="this.select();"  value="0"/>
							  	</div>
						  	</div>
						  	
						  	
						  	<div class="inspector_parameter  parameter_moveByArrowValue" style="display:none">	
							  	<div class="title">Move value</div>
							  	<div class="property_container">
							  		<label for="moveByArrowValue">v</label>
							  		<input  type="text" name="moveByArrowValue" id="moveByArrowValue" onclick="this.select();"  value="0"/>
							  	</div>
						  	</div>
						  	
						  	
						  	<div class="inspector_parameter  " style="display:none">	
							  	<div class="title">Include files</div>
							  	<div class="property_container">
							  		<button  id="editIncludes_button"> CSS-JS </button>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  " style="display:none">	
							  	<div class="title">Page Title and Desc</div>
							  	<div class="property_container">
							  		<button  id="editTitleDescription_button"> Title - Desc... </button>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  " style="display:none">	
							  	<div class="title">Test page in browser</div>
							  	<div class="property_container">
							  		<button  id="showPagePreview_button"> Preview HTML </button>
							  	</div>
						  	</div>
						  	
						  								  
						</div> <!-- PAGE_PROPERTIES_2D_PANEL  -->




					<div id="GRAPHIC_PROPERTIES_2D_PANEL"  >
							
							<div class="title" >Object 2D</div>
						  	
						  	<div class="inspector_parameter  parameter_position_2D" >	
							  	<div class="title">Position</div>
							  	<div class="property_container">
							  		<label for="positionX">x</label>
							  		<input  type="text" name="positionX" id="positionX"  onclick="this.select();" />
							  	</div>
							  	<div class="property_container">														
							  		<label for="positionY">y</label>
							  		<input  type="text" name="positionY" id="positionY"  onclick="this.select();" />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_size_2D" >	
							  	<div class="title">Size</div>
							  	<div class="property_container">
							  		<label for="sizeX">x</label>
							  		<input  type="text" name="sizeX" id="sizeX"  onclick="this.select();"  />
							  	</div>
							  	<div class="property_container">														
							  		<label for="sizeY">y</label>
							  		<input  type="text" name="sizeY" id="sizeY"  onclick="this.select();" />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_rotation_2D" >	
							  	<div class="title">Rotation</div>
							  	<div class="property_container">
							  		<label for="rotation">r</label>
							  		<input  type="text" name="rotation" id="rotation"  onclick="this.select();"  />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_scale_2D" >	
							  	<div class="title">Scale</div>
							  	<div class="property_container">
							  		<label for="scaleX">x</label>
							  		<input  type="text" name="scaleX" id="scaleX"  onclick="this.select();"  />
							  	</div>
							  	<div class="property_container">														
							  		<label for="scaleY">y</label>
							  		<input  type="text" name="scaleY" id="scaleY"  onclick="this.select();" />
							  	</div>
						  	</div>
						  	
						  	
						  	<div class="inspector_parameter RemoveBlends">	
							  	<div class="title" >Paint Bucket</div>
							  	<div class="property_container"  >
									<label for="compositeOperation" style="display:none;">c</label>
									<select id="compositeOperation" name="compositeOperation" >
											<!--
<option  value="source-atop">source-atop</option>
											<option  value="source-in">source-in</option>
											<option  value="source-out">source-out</option>
											<option  value="destination-atop">destination-atop</option>
											<option  value="destination-in">destination-in</option>
											<option  value="destination-out">destination-out</option>
											<option  value="destination-over">destination-over</option>
											<option  value="lighter">lighter</option>
											<option  value="copy">copy</option>
											<option  value="xor">xor</option>
-->
											<option  value="source-over">normal</option>
											<option  value="multiply">multiply</option>
											<option  value="screen">screen</option>
											<option  value="overlay">overlay</option>
											<option  value="darken">darken</option>
											<option  value="lighten">lighten</option>
											<option  value="color-dodge">color-dodge</option>
											<option  value="color-burn">color-burn</option>
											<option  value="hard-light">hard-light</option>
											<option  value="soft-light">soft-light</option>
											<option  value="difference">difference</option>
											<option  value="exclusion">exclusion</option>
											<option  value="hue">hue</option>
											<option  value="saturation">saturation</option>
											<option  value="color">color</option>
											<option  value="luminosity">luminosity</option>
											
											
											
														
									</select>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  " >	
							  	<div class="title">Stroke width</div>
							  	<div class="property_container">														
							  		<label for="lineWidth">s</label>
							  		<input  type="number" name="lineWidth" id="lineWidth"  onclick="this.select();" />
							  	</div>
							  	<div id="LINE_OPTIONS_BUTTON" class="icon arrowSubElements">
						        	<a></a>							  	
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_fontSize">	
							  	<div class="title">Font size</div>
							  	<div class="property_container">														
							  		<input  type="number" name="fontSize" id="fontSize" onblur="window.scrollTo(0, 0);" onclick="this.select();"/>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_textLineHeight"  style="display:none !important;">	
							  	<div class="title">Interline</div>
							  	<div class="property_container">														
							  		<label for="lineHeight">i</label>
							  		<input  type="number" name="lineHeight" id="lineHeight" onblur="window.scrollTo(0, 0);" onclick="this.select();" />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_font" style="display:none !important;">	
							  	<div class="title">Font</div>
							  	<div class="property_container">														
							  		<label for="font">f</label>
							  		<input  type="text" name="font" id="font"  onclick="this.select();" />
							  	</div>
							  	<div id="FONT_LIST_MENU" class="icon arrowSubElements">
							  		<a>
						        	</a>
							  	</div>
						  	</div>
						  	
						  	
						  	<!--
<div class="inspector_parameter  parameter_font" >	
							  	<div class="title">Font</div>
							  	<div class="property_container">														
							  		<label for="font">f</label>
							  		<input  type="text" name="font" id="font"  onclick="this.select();" />
							  	</div>
							  	<div id="FONT_LIST_MENU" class="icon arrowSubElements">
						        	<a>
						        		<select id="fontList" name="fontList"  ></select>
						        	</a>
						        								  	
							  	</div>
						  	</div>
-->
						  	
						  	<!--
<div class="inspector_parameter parameter_fontStyle"  >	
							  	<div class="title">Font Style</div>
							  	<div class="property_container"  >
									<label for="fontStyle">s</label>
									<select id="fontStyle" name="fontStyle"  >
											<option  value="normal">Normal</option>
											<option  value="italic">Italic</option>
											<option  value="bold">Bold</option>
											<option  value="bold italic">Italic + Bold</option>
									</select>
							  	</div>
						  	</div>
-->
						  	
						  	
							<div class="inspector_parameter parameter_fontStyle"  style="display:none !important;">	
							  	<div class="title">Font Style</div>
							  	<div class="property_container"  >
									<label for="fontStyle">s</label>
									<select id="fontStyle" name="fontStyle"  >
											<option  value="normal">Normal</option>
											<option  value="italic">Italic</option>
											<option  value="oblique">Oblique</option>
									</select>
							  	</div>
						  	</div>

						  	<div class="inspector_parameter parameter_fontWeight"  >	
							  	<div class="title">Font Weight</div>
							  	<div class="property_container"  >
									<label for="fontWeight">s</label>
									<select id="fontWeight" name="fontWeight"  >
											<option  value="normal">Normal</option>
											<option  value="bold">Bold</option>	
											<option  value="lighter">Lighter</option>	
											<option  value="bold">Bold(Thick)</option>	
											<option  value="100">100</option>
											<option  value="200">200</option>
											<option  value="300">300</option>
											<option  value="400">400</option>
											<option  value="500">500</option>
											<option  value="600">600</option>
											<option  value="700">700</option>
											<option  value="800">800</option>
											<option  value="900">900(Thicker)</option>
									</select>
							  	</div>
						  	</div>

						  	<div class="inspector_parameter placeHtml"  >	
							  	<div class="title">Place into HTML</div>
							  	<div class="property_container"  >
									<label for="placeIntoHtmlLayer">h</label>
									<select id="placeIntoHtmlLayer" name="placeIntoHtmlLayer"  >
											<option  value="no">no</option>
											<option  value="yes">yes</option>
									</select>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter parameter_instanceId" >	
							  	<div class="title">Instance</div>
							  	<div class="property_container">
							  		<label for="id">id</label>
							  		<input  type="text" name="id" id="id"    />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter parameter_class" >	
							  	<div class="title">Class</div>
							  	<div class="property_container">
							  		<label for="classNameList">cl</label>
							  		<input  type="text" name="classNameList" id="classNameList"    />
							  	</div>
						  	</div>
						  	
						  	
						  	<div class="inspector_parameter parameter_anchorLink" >	
							  	<div class="title">Link url</div>
							  	<div class="property_container">
							  		<label for="anchorLink">u</label>
							  		<input  type="text" name="anchorLink" id="anchorLink"    />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter parameter_anchorLinkTarget" >	
							  	<div class="title">Link target</div>
							  	<div class="property_container">
							  		<label for="anchorLinkTarget">t</label>
							  		<input  type="text" name="anchorLinkTarget" id="anchorLinkTarget"    />
							  	</div>
						  	</div>
						  							  	
						  	<!--
<div class="inspector_parameter parameter_contentUrl" >	
							  	<div class="title">Source url</div>
							  	<div class="property_container">
							  		<label for="sourceUrl">u</label>
							  		<input  type="text" name="sourceUrl" id="sourceUrl"    />
							  	</div>
						  	</div>
-->
						  	
						  	<div class="inspector_parameter parameter_contentUrl" >	
							  	<div class="title">Image url</div>
							  	<div class="property_container">
							  		<label for="imageUrl">u</label>
							  		<input  type="text" name="imageUrl" id="imageUrl"    />
							  	</div>
						  	</div>

						  	
						  	<div class="inspector_parameter parameter_contentUrl" >
						  		<div class="title">Video title</div>
							  	<div class="property_container">
							  		<label for=""></label>
							  		<input  type="text" name="videoTitles" id="videoTitles" onchange="getVideoTitle()"/>
							  	</div>
							  	<div class="title">Include url</div>
							  	<div class="property_container">
							  		<label for="includeUrl">u</label>
							  		<input  type="text" name="includeUrl" id="includeUrl"    />
							  	</div>
						  	</div>
						  	
						  	
						  	
							<!--
<div class="inspector_parameter parameter_contentUrl"  >	
							  	<div class="title">Include mode</div>
							  	<div class="property_container"  >
									<label for="includeMode">i</label>
									<select id="includeMode" name="includeMode"  >
											<option value="includeSVG-js">include SVG - js</option>
											<option value="includeSVG-php">include SVG - php</option>
											<option value="includeHTML-php">include HTML - php</option>
											<option value="iframe">iframe</option>
											<option value="inlineHTML">inline HTML</option>
									</select>
							  	</div>
						  	</div>
-->

						  	
						  	<div class="inspector_parameter  " >	
							  	<div class="title">Inline content</div>
							  	<div class="property_container">
							  		<button name="inlineContent" id="editInlineContent_button"> Inline content... </button>
							  	</div>
						  	</div>
						  	
						  	<!--
<div class="inspector_parameter parameter_contentUrl"  >	
							  	<div class="title">Content Scrolling</div>
							  	<div class="property_container"  >
									<label for="scrolling">s</label>
									<select id="scrolling" name="scrolling"  >
											<option value="no">no</option>
											<option value="yes">yes</option>
									</select>
							  	</div>
						  	</div>
-->
						  			

						  	<div class="inspector_parameter  " >	
							  	<div class="title">Title and Description</div>
							  	<div class="property_container">
							  		<button  id="editTitleDescription_button"> Title - Desc... </button>
							  	</div>
						  	</div>
						  	
						  	
						</div><!-- GRAPHIC_PROPERTIES_2D_PANEL  -->


						<div id="EFFECT_PROPERTIES_2D_PANEL"  >
							
							<div class="title" >Shadow Effect</div>

						  	<div class="inspector_parameter  parameter_position_2D" >	
							  	<div class="title">Offset</div>
							  	<div class="property_container">
							  		<label for="shadowOffsetX">x</label>
							  		<input  type="text" name="shadowOffsetX" id="shadowOffsetX"  onclick="this.select();" />
							  	</div>
							  	<div class="property_container">														
							  		<label for="shadowOffsetY">y</label>
							  		<input  type="text" name="shadowOffsetY" id="shadowOffsetY"  onclick="this.select();" />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter" >	
							  	<div class="title">Blur</div>
							  	<div class="property_container">
							  		<label for="shadowBlur">b</label>
							  		<input type="text" name="shadowBlur" id="shadowBlur"  onclick="this.select();"  />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter" >	
							  	<div class="title">Color</div>
							  	<div class="property_container">
						  			<div class="color_container">
						  				<div id="shadowColor" class="color">
						  				</div>
						  			</div>
							  	</div>
						  	</div>
						  	
						  
						</div> <!-- EFFECT_PROPERTIES_2D_PANEL  -->
						
						
						
						<div id="ANIMATION_PROPERTIES_2D_PANEL"  >
							
							<div class="title" >Animation</div>
							
							
							<div class="animationPreview" style="width:100%; height:100px; position:relative; border:1px solid gray;  background-color:black; margin-bottom:10px; overflow:hidden; ">
								<div id="ANIMATED_ELEMENT" style="position:absolute; color:white; font-size:16px; width:62px; height:18px; left:50%; margin-left:-31px; top:50%; margin-top:-9px;" >Animate</div>
							</div>
							
							<div class="inspector_parameter parameter_instanceId" >	
							  	<div class="title">Animation name</div>
							  	<div class="property_container">
							  		<label for="animationName">n</label>
							  		<!-- <input type="text" name="name" id="name"  onclick="this.select();"  /> -->
							  		<select id="ANIMATION_LIST" name="animationName" id="animationName">
							  		<optgroup label="Utils">
							          <option value="hide">hide</option>
							        </optgroup>
							        
							        <optgroup label="Attention Seekers">
							          <option value="bounce">bounce</option>
							          <option value="flash">flash</option>
							          <option value="pulse">pulse</option>
							          <option value="rubberBand">rubberBand</option>
							          <option value="shake">shake</option>
							          <option value="swing">swing</option>
							          <option value="tada">tada</option>
							          <option value="wobble">wobble</option>
							        </optgroup>
							
							        <optgroup label="Bouncing Entrances">
							          <option value="bounceIn">bounceIn</option>
							          <option value="bounceInDown">bounceInDown</option>
							          <option value="bounceInLeft">bounceInLeft</option>
							          <option value="bounceInRight">bounceInRight</option>
							          <option value="bounceInUp">bounceInUp</option>
							        </optgroup>
							
							        <optgroup label="Bouncing Exits">
							          <option value="bounceOut">bounceOut</option>
							          <option value="bounceOutDown">bounceOutDown</option>
							          <option value="bounceOutLeft">bounceOutLeft</option>
							          <option value="bounceOutRight">bounceOutRight</option>
							          <option value="bounceOutUp">bounceOutUp</option>
							        </optgroup>
							
							        <optgroup label="Fading Entrances">
							          <option value="fadeIn">fadeIn</option>
							          <option value="fadeInDown">fadeInDown</option>
							          <option value="fadeInDownBig">fadeInDownBig</option>
							          <option value="fadeInLeft">fadeInLeft</option>
							          <option value="fadeInLeftBig">fadeInLeftBig</option>
							          <option value="fadeInRight">fadeInRight</option>
							          <option value="fadeInRightBig">fadeInRightBig</option>
							          <option value="fadeInUp">fadeInUp</option>
							          <option value="fadeInUpBig">fadeInUpBig</option>
							        </optgroup>
							
							        <optgroup label="Fading Exits">
							          <option value="fadeOut">fadeOut</option>
							          <option value="fadeOutDown">fadeOutDown</option>
							          <option value="fadeOutDownBig">fadeOutDownBig</option>
							          <option value="fadeOutLeft">fadeOutLeft</option>
							          <option value="fadeOutLeftBig">fadeOutLeftBig</option>
							          <option value="fadeOutRight">fadeOutRight</option>
							          <option value="fadeOutRightBig">fadeOutRightBig</option>
							          <option value="fadeOutUp">fadeOutUp</option>
							          <option value="fadeOutUpBig">fadeOutUpBig</option>
							        </optgroup>
							
							        <optgroup label="Flippers">
							          <option value="flip">flip</option>
							          <option value="flipInX">flipInX</option>
							          <option value="flipInY">flipInY</option>
							          <option value="flipOutX">flipOutX</option>
							          <option value="flipOutY">flipOutY</option>
							        </optgroup>
							
							        <optgroup label="Lightspeed">
							          <option value="lightSpeedIn">lightSpeedIn</option>
							          <option value="lightSpeedOut">lightSpeedOut</option>
							        </optgroup>
							
							        <optgroup label="Rotating Entrances">
							          <option value="rotateIn">rotateIn</option>
							          <option value="rotateInDownLeft">rotateInDownLeft</option>
							          <option value="rotateInDownRight">rotateInDownRight</option>
							          <option value="rotateInUpLeft">rotateInUpLeft</option>
							          <option value="rotateInUpRight">rotateInUpRight</option>
							        </optgroup>
							
							        <optgroup label="Rotating Exits">
							          <option value="rotateOut">rotateOut</option>
							          <option value="rotateOutDownLeft">rotateOutDownLeft</option>
							          <option value="rotateOutDownRight">rotateOutDownRight</option>
							          <option value="rotateOutUpLeft">rotateOutUpLeft</option>
							          <option value="rotateOutUpRight">rotateOutUpRight</option>
							        </optgroup>
							
							        <optgroup label="Sliders">
							          <option value="slideInDown">slideInDown</option>
							          <option value="slideInLeft">slideInLeft</option>
							          <option value="slideInRight">slideInRight</option>
							          <option value="slideOutLeft">slideOutLeft</option>
							          <option value="slideOutRight">slideOutRight</option>
							          <option value="slideOutUp">slideOutUp</option>
							        </optgroup>
							
							        <optgroup label="Specials">
							          <option value="hinge">hinge</option>
							          <option value="rollIn">rollIn</option>
							          <option value="rollOut">rollOut</option>
							        </optgroup>
							      </select>
							  	</div>
						  	</div>
						  			
						  	<div class="inspector_parameter parameter_tweenBegin" >	
							  	<div class="title">Delay</div>
							  	<div class="property_container">
							  		<label for="delay">b</label>
							  		<input type="text" name="delay" id="delay"  onclick="this.select();"  />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter parameter_tweenDur" >	
							  	<div class="title">Duration</div>
							  	<div class="property_container">
							  		<label for="duration">d</label>
							  		<input type="text" name="duration" id="duration"  onclick="this.select();"  />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter parameter_tweenRepeatCount" >	
							  	<div class="title">Repeat count</div>
							  	<div class="property_container">
							  		<label for="iterationCount">n</label>
							  		<input type="text" name="iterationCount" id="iterationCount"  onclick="this.select();"  />
							  	</div>
						  	</div>
						  	
						  	
						  	
						  	<div class="inspector_parameter "  >	
							  	<div class="title">Fill mode</div>
							  	<div class="property_container"  >
									<label for="fillMode">f</label>
									<select id="fillMode" name="fillMode"  >
											<option value="none">none</option>
											<option value="forwards">forwards</option>
											<option value="backwards">backwards</option>
											<option value="both">both</option>
									</select>
							  	</div>
						  	</div>
						  	
						  	
						  	<div class="inspector_parameter "  style="display:none;">	
							  	<div class="title">Notice</div>
							  	<div class="property_container" style="padding:0 10px 0 10px;"  >
									To use these animations<br/>you have to include<br/>the animate.css file<br/>in your page.
									<br/><br/>
									info at:<br/><a href="http://daneden.github.io/animate.css/" target="_blank">daneden animate.css</a> 
									<br/><br/>
									download:<br/><a href="css/animate.min.css" target="_blank">animate.min.css</a>
									<br/>
									
							  	</div>
						  	</div>
						  	
						  
						</div> <!-- ANIMATION_PROPERTIES_2D_PANEL  -->
						
						
						

						<div id="FILL_STROKE_PROPERTIES_PANEL"  style="display:none;"  >
						  		<div id="FILL_STROKE_PROPERTIES">
									<div id="STROKE_BUTTON" ><div></div></div>				
									<div id="FILL_BUTTON" ></div>
								</div>
								<div id="SWAP_FILL_STROKE_BUTTON" style="display:none;"></div>
								<div id="FILL_STROKE_TYPES">									
									<div id="DIFFUSE_BUTTON"  ></div>
									<div id="GRADIENT_BUTTON"  ></div>
									<div id="PATTERN_BUTTON"  ></div>
									<div id="NONE_BUTTON"  ></div>
								</div>
								<div id="FILL_STROKE_ALPHA" >
									<div style="padding:5px;">Opacity</div>
									<div id="ALPHA_SLIDER" class="slider"><div></div></div>
								</div>
						</div><!-- FILL_STROKE_PROPERTIES_PANEL -->
						
		</div><!-- end panel -->


		<ul id="WINDOWS_TOOLBAR" class="footer toolbar hbar" style="display:none;">
			<li id="RESOURCES_WIN_button" ><a></a></li>
			<li id="LAYERS_WIN_button"  ><a></a></li>
			<li id="EFFECTS_WIN_button" ><a></a></li>
			<li id="ANIMATIONS_WIN_button" ><a></a></li>
			<!-- <li id="DRIVE_FILE_BROWSER_WIN_button" ><a></a></li> -->
		</ul>


</div><!-- end column -->


		
		
		
		
		
		
	
			</div> <!-- CONTENT -->
			
				
			
			
			
		
		
		<div id="WINDOWS">
		
			<div id="MESSAGE_WIN" class="win dropShadow message_win closed" >
				<div id="WIN_HEAD" class="head hbar">
					<div id="WIN_TITLE" class="title">Message</div>
				</div>
				<div id="WIN_CLOSE_BUTTON" class="close_button">X</div>
				<div id="WIN_COLLAPSE_BUTTON" class="collapse_button"></div>
				<div id="WIN_CONTENT" class="content" >
					<textarea id="WIN_MESSAGE_CONTENT" ></textarea>					
				</div>
				<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
			</div> <!-- MESSAGE_WIN -->
			
			
			
		<div id="OPEN_DOCUMENT_WIN" class="win openDocument_win dropShadow closed" >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Open document</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button">X
			</div>
			<div id="WIN_CONTENT" class="content">
				<div id="FILES_VIEW_HEADER" class="hbar" >
					<div id="DIR_UP_BUTTON"></div>
					<div id="SELECTED_DOCUMENT_TITLE">Select a file</div>
				</div>
				
				<ul id="FILES_VIEW">
					
				</ul>
				
				<div id="FILES_VIEW_FOOTER" class="hbar" style="display:none" >
					<input id="CANCEL_BUTTON" name="cancel" type="button"  value="     Cancel    " onclick="" />
					<input id="OPEN_BUTTON" name="open" type="button" disabled="disabled"  value="      Open      "/>
				</div>					
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>
		
		
		
		<div id="SAVE_DOCUMENT_WIN" class="win saveDocument_win dropShadow closed" style="display:none;z-index:1000;">
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Save Document File</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button" onclick="closeOverlay()">X
			</div>
			<div id="WIN_CONTENT" class="content">
				<div id="FILES_VIEW_HEADER" class="hbar">
					<div id="DIR_UP_BUTTON"></div>
					Save with name: <input id="INPUT_FILE_NAME" name="fileName" type="text"  value="untitled" />
				</div>
				<ul id="FILES_VIEW"></ul>
				<div id="FILES_VIEW_FOOTER" class="hbar" >
					<input id="CREATE_DIR_BUTTON" name="createDir" style="float:left;" type="button"  value="   New folder…  " />
					<input id="CANCEL_BUTTON" name="cancel"  type="button"  value="     Cancel    "  />
					<input id="SAVE_BUTTON" name="save"  type="button"  value="      Save      "/>
				</div>					
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>

			
			
		<div id="INCLUDES_EDIT_WIN" class="win dropShadow includesEdit_win closed"  >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Include CSS and JS files</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button">X
			</div>
			<div id="WIN_CONTENT" class="content" >
				<div>
					<div>
						CSS
						<textarea id="CSS_INCLUDES_TEXT_CONTAINER" onblur="window.scroll(0,0);" ></textarea>
					</div>
					<div>
						Javascript
						<textarea id="JS_INCLUDES_TEXT_CONTAINER" onblur="window.scroll(0,0);" ></textarea>																	
					</div>
				</div>
				<input id="SAVE_INCLUDES_BUTTON" type="button" style="position:absolute; bottom:5px; width:100px; right:14px; "  value="Apply" />
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>
		
		
		
		<!--
<div id="INLINE_CONTENT_EDIT_WIN" class="win dropShadow inlineContentEdit_win closed"  >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Inline content</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button">
			</div>
			<div id="WIN_CONTENT" class="content" >
				<textarea id="INLINE_CONTENT_TEXT_CONTAINER" onblur="window.scroll(0,0);" ></textarea>									
				<input id="UPDATE_INLINE_CONTENT_BUTTON" type="button" style="position:absolute; bottom:5px; width:100px; right:14px; "  value="apply" />
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>
-->
		
		<div id="INLINE_CONTENT_EDIT_WIN" class="win dropShadow inlineContentEdit_win closed"  >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Inline content</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button">X
			</div>
			<div id="WIN_CONTENT" class="content" >
				<textarea id="INLINE_CONTENT_TEXT_CONTAINER" onblur="window.scroll(0,0);" ></textarea>													
			</div>
			<!-- e fuori dal content perchè codemirror ridimensiona la textarea al 100% del content in altezza -->
			<input id="UPDATE_INLINE_CONTENT_BUTTON" type="button" style="position:absolute; bottom:5px; width:100px; right:20px; "  value="apply" />
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>

				
		<!--

		<div id="TEXTBOX_EDIT_WIN" class="win dropShadow textBoxEdit_win"  >
			<div id="WIN_HEAD" class="head bar">
				<div id="WIN_TITLE" class="title">Edit Text</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button">
			</div>
			<div id="WIN_CONTENT" class="content" >
				<textarea id="TEXT_CONTAINER" onblur="window.scroll(0,0);" ></textarea>									
				<input id="UPDATE_TEXT_BUTTON" type="button" style="position:absolute; bottom:5px; width:100px; right:14px; "  value="apply" />
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>
-->
		
		
		
		<div id="TITLE_DESCRIPTION_EDIT_WIN" class="win dropShadow titleDescriptionEdit_win closed"  >
			<div id="WIN_HEAD" class="head page_heading">
				<div id="WIN_TITLE" class="title">Title</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button">X
			</div>
			<div id="WIN_CONTENT" class="content popup_cont" >
				
					<div class="ti">
						Title
						<textarea id="ELEMENT_TITLE" onblur="window.scroll(0,0);" ></textarea>
					</div>
					<div class="ti">
						Description
						<textarea id="ELEMENT_DESCRIPTION" onblur="window.scroll(0,0);" ></textarea>																	
					</div>
				
				<input id="SAVE_TITLE_DESCRIPTION_BUTTON" type="button" value="Apply" class="appbtn"/>
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>


		
		
		
		<div id="RESOURCES_WIN" class="win dropShadow resources_win "  style="display:none;">
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Library</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button" style="display:none !important;">X</div>
			<div class="close_button" onclick="closeVideoPnl()">X</div>
			<div id="WIN_COLLAPSE_BUTTON" class="collapse_button" style="display:none !important;"></div>
			
			<div id="WIN_CONTENT" class="content"  style="display:block !important;">				
					
					<div id="RESOURCES_PANELS" class="radioPanels"  >
						<ul id="FONTS_LIBRARY_PANEL" class="scrollableY darkScrollbar recordList panel activated" style="display:none;"></ul>
						<div id="SYMBOLS_LIBRARY_PANEL" class="panel activated">
							<div class="header hbar" style="display:none;">Search <input type="search" name="googlesearch"  placeholder="insert a word"> in openclipart.org</div>
							<ul class="myHideCls scrollableY darkScrollbar recordList">

							</ul>
						</div>
						<ul id="GRADIENTS_LIBRARY_PANEL" class="scrollableY darkScrollbar recordList panel" style="display:none;"></ul>
						<ul id="PATTERNS_LIBRARY_PANEL" class="scrollableY darkScrollbar recordList panel" style="display:none;"></ul>
					</div>

					
					
					<ul id="TABS_RESOURCES_PANELS" class="footer toolbar hbar" style="display:block;overflow:visible;" >
						
						<li id="FONTS_LIBRARY_button"  style="display:none;">	
							<ul  class="popupMenu dropShadow" style="left:0px; bottom:24px;"  >
								<li libraryType="fontLibrary" category="Sans Serif" title="" >Font Sans Serif</li>
								<li libraryType="fontLibrary" category="Display" title="" >Font Display</li>
								<li libraryType="fontLibrary" category="Serif" title="" >Font Serif</li>
								<li libraryType="fontLibrary" category="Handwriting" title="" >Font Handwriting</li>
								<!-- <li libraryType="fontLibrary" category="Miscellanea" title="" >Font Miscellanea</li> -->
					    	</ul>
							<a></a>
						</li>
						
						<li id="SYMBOLS_LIBRARY_button" class="selected" style="background:none !important;">
							<ul class="popupMenu dropShadow myIcons popupMenu_ajst" style="display:block;margin: 0 0 0 -28px;">
								<!--<li url="../../DATA/janvas/library2D/symbols/girls/index.html"  title="">Silhouettes</li>
								<li url="../../DATA/janvas/library2D/symbols/animals/animals.html"  title="">Animals</li>
								<li url="../../DATA/janvas/library2D/symbols/user_interface/user_interface.html"  title="">User interface</li>-->
								<li url="<?php echo base_url();?>components.html" class="compoClass"  title="Youtube Component" >Youtube<!--<img src="../img/ant_tool_img/YouTube_icons.png" class="iconSizes">--></li>
								<li url="<?php echo base_url();?>Invoice_templates.html"  title="Invoice Templates">Invoice<!--<img src="../img/ant_tool_img/invoice-xxl.png" class="iconSizes">--></li>
								<li url="<?php echo base_url();?>website_templates.html"  title="Website Templates">Website<!--<img src="../img/ant_tool_img/websites_icons.png" class="iconSizes">--></li>
								<li url="<?php echo base_url();?>menu_templates.html"  title="Menu Templates">Menus</li>
								<!--<li url="../../DATA/janvas/library2D/symbols/furniture/furniture.html"  title="">Furniture</li>
								<li url="../../DATA/janvas/library2D/symbols/decorations/decorations.html"  title="">Decorations</li>
								
<!-- 								<li url="http://openclipart.org/search/json/?query=christmas&page=1&amount=20"  title="">Openclipart.org</li> -->
					    	</ul>
							<a><img src='<?php echo base_url();?>img/menu.png' style="width:20px;height:20px;display:none;"></a>
						</li>
						
						<li id="GRADIENTS_LIBRARY_button" style="display:none;">
							<ul class="popupMenu dropShadow" style="left:0px; bottom:24px;  "  >
								<li url="<?php echo base_url();?>js/linears.html"  title="">Linear gradients</li>
<!--
								<li url="http://192.168.1.156/happEdit/newHappyEdit/js/linears.html"  title="">Linear gradients</li>
-->
<!--
		    					<li url="../../DATA/janvas/library2D/gradients/radials.html"  title="">Radial gradients</li>
-->
					    	</ul>
							<a></a>
						</li>
						
						<li id="PATTERNS_LIBRARY_button" style="display:none;">
							<ul  class="popupMenu dropShadow" style="left:0px; bottom:24px;  "  >
								<li url="patterncooler.html"  title="">patterncooler.com</li>
								<li url="../../DATA/janvas/library2D/patterns/subtlepatterns.html"  title="">subtlepatterns.com</li>
					    	</ul>
							<a></a>
						</li>
						
						<!--
<li id="ANIMATION_LIBRARY_button" >
							<a></a>
						</li>
-->
						
					</ul>				
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>

				

		
		
		
		
		
		
		
		
		<div id="EFFECTS_WIN" class="win dropShadow effects_win closed"  >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Effects</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button">x</div>
			<div id="WIN_COLLAPSE_BUTTON" class="collapse_button"></div>
			
			<div id="WIN_CONTENT" class="content"  >				
					
					<div id="EFFECTS_2D_PANEL" class="listView scrollableY darkScrollbar" ></div>
					
					<ul id="EFFECTS_EDITING_TOOLBAR" class="footer toolbar hbar" style="overflow:visible;" >
							<li id="REMOVE_EFFECT_button"   title=""><a></a></li>
	        				<li id="MOVE_EFFECT_UP_button"   title=""><a></a></li>
	        				<li id="MOVE_EFFECT_DOWN_button"   title=""><a></a></li>
					</ul>				
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>
		
		
		
		<div id="ANIMATIONS_WIN" class="win dropShadow animations_win closed"  >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Animation</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button">X</div>
			<div id="WIN_COLLAPSE_BUTTON" class="collapse_button"></div>
			
			<div id="WIN_CONTENT" class="content"  >				
					
					<div id="ANIMATIONS_2D_PANEL" class="listView scrollableY darkScrollbar" ></div>
					
					<ul id="ANIMATIONS_EDITING_TOOLBAR" class="footer toolbar hbar" style="overflow:visible;" >
							<li id="CREATE_ANIMATION_button"   title=""><a></a></li>
							<li id="REMOVE_ANIMATION_button"   title=""><a></a></li>
					</ul>				
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>
		
		
		
		
		<div id="DRIVE_FILE_BROWSER_WIN" class="win dropShadow driveFileBrowser_win closed"  style="width:516px !important;">
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">
					 <select onchange="changeExtention()" id="extId" style="float:right;margin-right:30px">
						<option value="htmls" selected>Html</option>
						<option value="pngs">Png</option>
						<option value="gifs">Gif</option>
					</select>  Open File</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button" onclick="closeThisPops()">X</div>
			<div id="WIN_COLLAPSE_BUTTON" class="collapse_button" style="margin-right: 20px;"></div>
			
			<div id="WIN_CONTENT" class="content myFilesContents" style="overflow:auto;">				
					
					<div id="DRIVE_FILE_BROWSER_PANEL"  class="treeView ImgViews pkPadd myHtmlPanel">
						<?php
							$filename =  '';
							$i=1;
							$id = 'happyedit_'; 
							 
							$pro_name=$_COOKIE["mycookie"];
							$filepath = glob('../../saveImg/'.base64_encode($this->session->userdata("user_id")).'/'.$pro_name.'/*.html');
							// print_r($test);
							// echo count($test);
							if (count($filepath)>0) {
								foreach ($filepath as $filename) {
							    // echo basename($filename). "\n";
							   echo  '<li itemtype="html" downloadurl="'.basename($filename).'" webcontentlink="'.base_url().'saveImg/'.base64_encode($this->session->userdata("user_id")).'/'.$pro_name.'/'.basename($filename).'" draggable="true" class="unselected" id='.$id.$i++.'><div class="html_icon16x16">'.basename($filename);'</div></li>';
							   echo '<li style="float:right;margin-top: -19px;">'.filesize($filename).'&nbsp;bytes</li>';
								}
							}else{
								echo "<li>No data</li>";
							}
							?>
						  <!-- <li itemtype="html" downloadurl="<?php echo base_url()?>saveImg/MTM=/pk.html" webcontentlink="<?php echo base_url()?>saveImg/MTM=/pk.html" id='0B22PV7OE-vzfc0lPMUxxUHZiMVk' draggable="true" class="unselected"><div class="html_icon16x16">t2.html</div><a></a></li>  
						  <li itemtype="html" id='0B22PV7OE-vzfVUpOMDFpWGdHOUk' downloadurl="<?php echo base_url()?>saveImg/MTM=/404.html" webcontentlink="<?php echo base_url()?>saveImg/MTM=/404.html" draggable="true" class="unselected"><div class="html_icon16x16">404.html</div><a></a></li>  --> 
						
					</div>
					<div id="DRIVE_FILE_BROWSER_PANEL2"  class="treeView ImgViews pkPadd" style="display:none;">
						<?php
							$filename =  '';
							$i=1;
							$id = 'happyedit_';
							$pro_name=$_COOKIE["mycookie"];
							$filepath = glob('../../saveImg/'.base64_encode($this->session->userdata("user_id"))."/".$pro_name.'/*.png');
							// print_r($test);
							// echo count($test);
							if (count($filepath)>0) {
								foreach ($filepath as $filename) {
							    // echo basename($filename). "\n";
							   echo  '<li itemtype="html" downloadurl="'.basename($filename).'" webcontentlink="'.base_url().'saveImg/'.base64_encode($this->session->userdata("user_id")).'/'.$pro_name.'/'.basename($filename).'" draggable="true" class="unselected" id='.$id.$i++.' style="float:left;border:1px solid #999;margin:3px;padding:3px;"><div class="html_icon16x16"><img src="'.base_url().'saveImg/'.base64_encode($this->session->userdata("user_id")).'/'.$pro_name.'/'.basename($filename).'" style="width:50px;height:50px;"></div></li>';
							   //echo '<li style="float:right;margin-top: -19px;">'.filesize($filename).'&nbsp;bytes</li>';
								}
							}else{
								echo "<li>No data</li>";
							}
							?>
						  <!-- <li itemtype="html" downloadurl="<?php echo base_url()?>saveImg/MTM=/pk.html" webcontentlink="<?php echo base_url()?>saveImg/MTM=/pk.html" id='0B22PV7OE-vzfc0lPMUxxUHZiMVk' draggable="true" class="unselected"><div class="html_icon16x16">t2.html</div><a></a></li>  
						  <li itemtype="html" id='0B22PV7OE-vzfVUpOMDFpWGdHOUk' downloadurl="<?php echo base_url()?>saveImg/MTM=/404.html" webcontentlink="<?php echo base_url()?>saveImg/MTM=/404.html" draggable="true" class="unselected"><div class="html_icon16x16">404.html</div><a></a></li>  --> 
						
					</div>
					<div id="DRIVE_FILE_BROWSER_PANEL3"  class="treeView ImgViews pkPadd" style="display:none;">
						<?php
							$filename =  '';
							$i=1;
							$id = 'happyedit_';
							$pro_name=$_COOKIE["mycookie"];
							$filepath = glob('../../saveImg/'.base64_encode($this->session->userdata("user_id"))."/".$pro_name.'/*.gif');
							// print_r($test);
							// echo count($test);
							if (count($filepath)>0) {
								foreach ($filepath as $filename) {
							    // echo basename($filename). "\n";
							   echo  '<li itemtype="html" downloadurl="'.basename($filename).'" webcontentlink="'.base_url().'saveImg/'.base64_encode($this->session->userdata("user_id")).'/'.$pro_name.'/'.basename($filename).'" draggable="true" class="unselected" id='.$id.$i++.' style="float:left;border:1px solid #999;margin:3px;padding:3px; "><div class="html_icon16x16"><img src="'.base_url().'saveImg/'.base64_encode($this->session->userdata("user_id")).'/'.$pro_name.'/'.basename($filename).'" style="width:50px;height:50px;"></div></li>';
							  // echo '<li style="float:right;margin-top: -19px;">'.filesize($filename).'&nbsp;bytes</li>';
								}
							}else{
								echo "<li>No data</li>";
							}
							?>
						  <!-- <li itemtype="html" downloadurl="<?php echo base_url()?>saveImg/MTM=/pk.html" webcontentlink="<?php echo base_url()?>saveImg/MTM=/pk.html" id='0B22PV7OE-vzfc0lPMUxxUHZiMVk' draggable="true" class="unselected"><div class="html_icon16x16">t2.html</div><a></a></li>  
						  <li itemtype="html" id='0B22PV7OE-vzfVUpOMDFpWGdHOUk' downloadurl="<?php echo base_url()?>saveImg/MTM=/404.html" webcontentlink="<?php echo base_url()?>saveImg/MTM=/404.html" draggable="true" class="unselected"><div class="html_icon16x16">404.html</div><a></a></li>  --> 
						
					</div>
					
					<ul style="display:none" id="DRIVE_FILE_BROWSER_TOOLBAR" class="footer toolbar hbar" style="overflow:visible;" >
							
							<li id="NEW_FOLDER_button"   title="New Folder"><a></a></li>
					        <li id="NEW_SHARED_WEB_FOLDER_button"   title="New Shared Folder"><a></a></li>
							<li id="OPEN_AS_WEB_PAGE_button" title="Open With Browser" ><a></a></li>
							<li id="REMOVE_FILE_button"   title="Remove File"><a></a></li>
							<li id="RENAME_FILE_button"   title="Rename File"><a></a></li>
					        <li id="DOWNLOAD_FILE_button" title="Download File"><a></a></li>
					        <li id="UPLOAD_FILE_button"   title="Upload File"><a></a><input id="UPLOAD_FILE_TEXTFIELD" type="file" style="position:absolute; display: block; left:0; top:0; opacity:0; width:100%; cursor:pointer;" /></li>

							<!-- <li id="EDIT_button" >
								<ul  class="popupMenu dropShadow" style="right:0; bottom:0; "   >
					        		<li  id="CREATE_FILE_JS_menuItem"  class="js_icon16x16" style="background-position-x: 8px;" fileExtension="js"   fileMimeType="text/javascript" title="">New file - JS</li>
					        		<li  id="CREATE_FILE_CSS_menuItem" class="css_icon16x16" style="background-position-x: 8px;" fileExtension="css"  fileMimeType="text/css" title="">New file - CSS</li>
					        		<li  id="CREATE_FILE_HTML_menuItem" class="html_icon16x16" style="background-position-x: 8px;" fileExtension="html" fileMimeType="text/html" title="">New file - HTML</li>
					        		<li class="divisor"></li>
					        		<li  id="NOTIFICATION_SERVER_menuItem"   title="">Notification Server...</li>
					        		<li  id="SEND_NOTIFICATION_menuItem"   title="">Send Notification</li>					        		
					        	</ul>
					        	<a></a>
							</li> -->
							
							<li id="LOADING_ICON" ><a></a></li>
					</ul>				
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>
		
		
			<div id="SAVE_PROGRESS_WIN" class="win dropShadow saveProgress_win closed"  >
				<div id="WIN_HEAD" class="head hbar">
					<div id="WIN_TITLE" class="title">Save progress</div>
				</div>
				<div id="WIN_CLOSE_BUTTON" class="close_button">X
				</div>
				<div id="WIN_CONTENT" class="content" >
					<div>
						<div id="LABEL" >Document saving...</div>
						<progress id="BAR"  max="100" ></progress>
					</div>
				</div>
				<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
			</div><!-- SAVE_PROGRESS_WIN -->
			
		</div><!-- WINDOWS -->
		
		
		<div id="DIALOGS">	
		</div> <!-- DIALOGS -->
		
	</div>
	
	
	<div id="CONTEXT_MENUS" oncontextmenu="return false;" style="">

		<ul id="EDIT_2D_CONTEXT_MENU" class="popupMenu dropShadow" style="display:none;left:200px; top:45px; " >
		        	<li id="FILL_menuItem" >Fill</li>
			        <li id="STROKE_menuItem"  >Stroke</li>
			        <li id="STROKE_WIDTH_menuItem"  >Stroke width</li>
			        <li class="divisor"></li>
			        <li id="DUPLICATE_menuItem"  >Duplicate</li>
	        		<li id="GROUP_menuItem"  >Group</li>
	        		<li id="UNGROUP_menuItem"   >Ungroup</li>
	        		<li class="divisor"></li>
					<li id="LOCK_SELECTION_menuItem" >Lock selection</li>
					<li id="UNLOCK_ALL_menuItem" >Unlock all</li>
        	</ul>
			
			<ul id="LINE_OPTIONS_CONTEXT_MENU" class="popupMenu dropShadow" style="left:200px; top:45px;" >
			        <li  value="1" id="LINE_WIDTH_menuItem"  >1 pt</li>
			        <li  value="2" id="LINE_WIDTH_menuItem"  >2 pt</li>
					<li  value="3" id="LINE_WIDTH_menuItem" >3 pt</li>
					<li  value="4" id="LINE_WIDTH_menuItem" >4 pt</li>
					<li  value="5" id="LINE_WIDTH_menuItem" >5 pt</li>
	        		<li  value="6" id="LINE_WIDTH_menuItem"  >6 pt</li>
	        		<li  value="7" id="LINE_WIDTH_menuItem"  >7 pt</li>
	        		<li  value="8" id="LINE_WIDTH_menuItem"   >8 pt</li>
	        		<li class="divisor"></li>
			        <li  id="LINE_CAP_BUTT_menuItem"  >Line cap Butt</li>
			        <li  id="LINE_CAP_ROUND_menuItem"  >Line cap Round</li>
					<li  id="LINE_CAP_SQUARE_menuItem" >Line cap Square</li>
					<li  id="LINE_JOIN_MITER_menuItem" >Line join Square</li>
					<li  id="LINE_JOIN_ROUND_menuItem" >Line join Round</li>
	        		<li  id="LINE_JOIN_BEVEL_menuItem"  >Line join Bevel</li>		        
	        </ul>
	        
	        <div id="GRADIENT_EDITOR" class="dropShadow">
				<div id="GRADIENT_ALPHA_BACKGROUND" >	
					<div id="GRADIENT" >
						<div id="GRADIENT_POINT" style="left: 0px;" ><div id="COLOR"></div></div>
						<div id="GRADIENT_POINT" style="left: 50px;" ><div id="COLOR"></div></div>
						<div id="GRADIENT_POINT" style="left: 150px;" ><div id="COLOR"></div></div>
					</div>
				</div>
				<select id="GRADIENT_TYPE"   >
						<option  value="linear">Linear</option>
						<option  value="radial">Radial</option>
				</select>
			</div>
		
		<div id="COLOR_PICKER" class="dropShadow">	
				<input id="HEX_COLOR_INPUT" type="text" onblur="window.scrollTo(0, 0);" onclick="select();" />
				<div id="ALPHA_BACKGROUND" >					
					<div id="ALPHA_PANEL" >
						<div id="ALPHA_POINT" ></div>
					</div>
				</div>
				<div id="HUE_PANEL" >
					<div id="HUE_POINT" ></div>
				</div>
				<div id="SATURATION_COLOR" >
					<div id="SATURATION_PANEL" >
						<div id="SATURATION_POINT" ></div>
					</div>
				</div>
			</div>
		
	</div> <!-- CONTEXT_MENUS -->
	
	
	<div id="AUTH_REQUEST_PANEL" class="authRequestPanel dropShadow hbar" >
			<div class="google_drive_logo" ></div>
			<div class="info" >
			Google Drive<br/>
			Authorization<br/>
			required
			</div>
			<button id="authorize_button" >Authorize</button>
			<div id="close_button" ></div>
		</div>

  <div id="ABOUT_PANEL" class="" >
			<!-- <div class="versionInfo" >version 3.0.0</div> -->
			<div class="logo" style="display:none;" ></div>
			<div class="copyrightInfo" >
				<!-- Copyright © XOSystem - All rights reserved - <a href="http://www.janvas.com" target="_blank" >janvas.com</a> -->
			</div>
	</div>
	
	
	<div id="ADVERTISING_PREMIUM_PANEL" class="dialogPanel dropShadow  hbar" >			
			<div class="logo" ></div>
			<!--
<div class="info" >
				Your FREE account is LIMITED<br/>
				to 10 saves<br/>
				in a time range of 30 days.
			</div>
-->
			<div id="ADVERTISING_PREMIUM_LINK" ></div>
			<div id="ACCOUNT_FREE_STATUS" class="account_free_status" ></div>
			<div id="close_button" ></div>
	</div>
	
<img src="" id="loadingImg" class="loadingImages" style="display:none;">
	<div class="successPops" id="myFileSuccess" style="display:none;"><center>Your Image Save Successfully..!</center>
	</div>
	 
	 <div id="dialog" title="Save Document File"style="display:none;z-index:1000;">
		<label>Save with name</label> 
		<input type="text" name="" id="filename" value="index.html" class="form-contol">
		<a id="saveHtmlPage" class="btn btn-primary">Save</a>
	</div>

	 <div id="updateHtmlDesignFile" title="Completed" style="display:none !important;color:#fff;">
		<!-- <label>Save with name</label>
		<input type="text" name="" id="filename" value="untitled.html" class="form-contol">
		<a id="saveHtmlPage" class="btn btn-primary" style="padding-left: 120px;">Save</a> -->
		Please wait we are in process..
	</div>
	
	<!--popup for image Preview-->
<!--
	<div class="imgPopups" id="imagePreviewPopup" style="display:none;">
		<div class="PreivewTitle"><span id="prevTitle"></span><span onclick="closePop('imagePreviewPopup')">&times;</span></div>
		<div class="PreviewImgs" id="ImgPreviewSrc"></div>
	</div>
-->
	<!--popup for image Preview-->
<!-- custom scrollbar plugin -->
<!--my private poup-->

<div id="myPrivatePopup" class="toolBlocks" style="display:none;">
	<div class="popHead"><li downloadurl="https://doc-10-0k-docs.googleusercontent.com/docs/securesc/pn7adqm313529jvfnkvoeerr625m8fos/rbumha8hkkrkapc4ifpq6e1pm9ppblcs/1455170400000/07218793672984450628/07218793672984450628/0B_7H0LbXD4ixSmxrMlBRVnBwVGs?e=download&amp;gd=true" itemtype="file" id="0B_7H0LbXD4ixSmxrMlBRVnBwVGs" title="404.html" class="selectedItem"><div style="background-image: url(&quot;https://lh6.googleusercontent.com/TkMWTxkg4dmxUxI8DWqY1TQ3EhdPUjgK3kpom7bE5yvPwBEfVQ-yHr1bND8qiv1m-q6GBA=s220&quot;);" class="thumbnail" id="PREVIEW_CONTAINER"></div><div>404.html</div></li></div>
	<div class="popBody"><div class="hbar" >
					<input id="CANCEL_BUTTON" name="cancel" type="button"  value="     Cancel    " onclick="" />
					<input id="OPEN_BUTTON" name="open" type="button" value="      Open      "/>
				</div>		</div>
</div>
<!--ennd of popyup-->

<div class="myImgEdits" id="imgEditPop" style="display:none;height:550px !important;">
		
		<div class="popup_left">
			<div class="editPopHead page_heading">Image Edit</div>
			<div class="popup_menu">
				<ul>
					<li><a href="javascript:void(0);" onclick="placeImage()">Image</a></li>
					<li><a href="javascript:void(0);" onmouseout="showMyMenu('cropMenus', 'off')" onmouseover="showMyMenu('cropMenus', 'on')" >Crop load</a>
						<ul class="popupMenu dropShadow" id="cropMenus" style="display:none;margin: 31px 0 0 0px;">
							<li onclick="myCrop('startCrop')">No Guide</li>
							<li onclick="myCrop('centerLines')">Center Lines</li>
							<li onclick="myCrop('ruleOfThirds')" >Rule Of Thirds</li>
							<li onclick="myCrop('goldenSections')">Golden Sections</li>
						</ul>
					</li>
					<li><a href="javascript:void(0);" id="crop">Crop</a></li>
					<li><a href="javascript:void(0);" id="perspectiveMode">Perspective</a></li>
					<li><a href="javascript:void(0);" id="deleteImages">Delete</a></li>
					<li><a href="javascript:void(0);" id="filterMode" style="display:none;">Filters</a></li>
					<!--<li><a href="javascript:void(0);">Effects</a></li>-->
				</ul>
				<ul class="toggleFunctions ImageEdit_toprightside_opt" style="display:none;">
					<li >Filter</li>
					<li class="ImgEdt_tp_lft_btn"><div class="toggle-button"><button></button></div></li>
					<li>Transform</li>
				</ul> 
				<input type="file" class="imgLoaders" id="imgLoader" style="opacity:0.0;width:50px;">
			</div>
			<div class="editBody">

				<div class="leftPart">
					<canvas id="myCanvas" width="630" height="410"></canvas>
					<div id="transformCanvases"></div>
					<img id="myGlfxImg" style="display:none;">
				</div>
				
				<img id="myCanvasimg" style="display:none;">
				<div class="rightPart" id="savedImgess"><center><span class="imgSavedTitle">Saved Images</span></center><div class="img1"></div></div>
			</div>
			<!--filter setting popup-->
			<div class="filterSettings" style="display:none;">
				<div class="settingsTitle">Setting</div>
				<div class="settingsBody" id="settingBdy"></div>
			</div>
			<!--end of filter setting popup-->
			<div class="editFooter">
				<button onclick="closeMyPopups()">Close</button>
				<button id="fabricSaveBtn" onclick="saveCanvasImage()">Save</button>
				<button id="glfxSaveBtn" onclick="saveGlfxImage()" style="display:none;">Save</button>
			</div>
		</div>

		<div class="popup_right">
			<div class="cls clsbtnCross" onclick="closeMyPopups()">X</div>
			<div class="page_heading">Images Filter<div class="" style="float:right;display:none;"><img src="<?php echo base_url();?>img/minimize-window-xxl.png" onclick="minimizePopups()" title="Minimize Tab" style="cursor:pointer;width:15px;height:14px;margin: 0 36px 0 0;"></div></div>
			<div class="popup_lar">
				<div id="filterDiv"> 
			
<!----yogesh changes---24-2-16---->


<div class="image-filter-name-control-wrapp">
<div class="image-filter-name"><span>Grayscale:</span> </div>
<div class="image-filter-control"><input type="checkbox" id="grayscale" class="checkboxinput"> </div>
</div>


<div class="image-filter-name-control-wrapp">
<div class="image-filter-name"><span>Invert:</span>  </div>
<div class="image-filter-control"><input type="checkbox" id="invert" class="checkboxinput"> </div>
</div>

<div class="image-filter-name-control-wrapp">
<div class="image-filter-name"><span>Sepia:</span>  </div>
<div class="image-filter-control"><input type="checkbox" id="sepia" class="checkboxinput"> </div>
</div>


<div class="image-filter-name-control-wrapp">
<div class="image-filter-name"><span>Sepia2:</span>  </div>
<div class="image-filter-control"><input type="checkbox" id="sepia2" class="checkboxinput"> </div>
</div>


<div class="image-filter-name-control-wrapp">
<div class="image-filter-name"><span>Remove white:</span> </div>
<div class="image-filter-control"> <input type="checkbox" id="remove-white" class="checkboxinput"> </div>
<div class="image-filter-scroll-bottom">

<div class="range-label">Threshold: </div>
<div class="range-input">
<input type="range" max="255" min="0" value="60" id="remove-white-threshold"> 
</div>

</div>
<div class="image-filter-scroll-bottom">
<div class="range-label">Distance: </div>
<div class="range-input">
<input type="range" max="255" min="0" value="10" id="remove-white-distance"> 
</div>
</div>

</div>


<div class="image-filter-name-control-wrapp">
<div class="image-filter-name"><span>Brightness:</span>  </div>
<div class="image-filter-control"> <input type="checkbox" id="brightness" class="checkboxinput"></div>
<div class="image-filter-scroll-bottom">
<div class="range-label">Value: </div>
<div class="range-input">
<input type="range" max="255" min="0" value="100" id="brightness-value"> </div>
</div>
</div>

<div class="image-filter-name-control-wrapp">
<div class="image-filter-name"><span>Noise:</span>  </div>
<div class="image-filter-control"><input type="checkbox" id="noise" class="checkboxinput" > </div>
<div class="image-filter-scroll-bottom">
<div class="range-label"> Value:</div>
<div class="range-input">
<input type="range" max="1000" min="0" value="100" id="noise-value"></div>
</div>
</div>


<div class="image-filter-name-control-wrapp">
<div class="image-filter-name"><span>Gradient Transparency:</span>  </div>
<div class="image-filter-control"><input type="checkbox" id="gradient-transparency" class="checkboxinput"> </div>
<div class="image-filter-scroll-bottom">

<div class="range-label"> Value:</div>
<div class="range-input">
<input type="range" max="255" min="0" value="100" id="gradient-transparency-value"> </div>
</div>
</div>


<div class="image-filter-name-control-wrapp">
<div class="image-filter-name"><span>Pixelate</span>  </div>
<div class="image-filter-control"><input type="checkbox" id="pixelate" class="checkboxinput"> </div>
<div class="image-filter-scroll-bottom">

<div class="range-label">Value:</div>
<div class="range-input">
<input type="range" max="20" min="2" value="4" id="pixelate-value"></div>
</div>
</div>


<div class="image-filter-name-control-wrapp">
<div class="image-filter-name"><span>Blur:</span>  </div>
<div class="image-filter-control"><input type="checkbox" id="blur" class="checkboxinput"> </div>


</div>


<div class="image-filter-name-control-wrapp">
<div class="image-filter-name"><span>Sharpen:</span>  </div>
<div class="image-filter-control"><input type="checkbox" id="sharpen" class="checkboxinput"></div>

</div>


<div class="image-filter-name-control-wrapp">
<div class="image-filter-name"><span>Emboss:</span>  </div>
<div class="image-filter-control"><input type="checkbox" id="emboss" class="checkboxinput"> </div>

</div>

<div class="image-filter-name-control-wrapp">
<div class="image-filter-name"><span>Tint:</span>  </div>
<div class="image-filter-control"><input type="checkbox" id="tint" class="checkboxinput"> </div>

<div class="image-filter-name-color-lbl">Color:  </div>
<div class="image-filter-control-color-input"><input type="color" value="" id="tint-color"> </div>


<div class="image-filter-scroll-bottom">

<div class="range-label">Opacity: </div>
<div class="range-input">
<input type="range" step="0.1" value="1" max="1" min="0" id="tint-opacity"> </div>
</div>
</div>


<div class="image-filter-name-control-wrapp">
<div class="image-filter-name"><span>Multiply:</span>  </div>
<div class="image-filter-control"><input type="checkbox" id="multiply" class="checkboxinput"> </div>
<div class="image-filter-name-color-lbl">Color:  </div>
<div class="image-filter-control-color-input"> <input type="color" value="" id="multiply-color"></div>

</div>

<div class="image-filter-name-control-wrapp">
<div class="image-filter-name"><span>Blend:</span>  </div>
<div class="image-filter-control"> <input type="checkbox" id="blend"></div>
<div class="image-filter-name-mode-lbl"><span> Mode:</span> </div>
<div class="image-filter-control-mode-input">
<select name="blend-mode" id="blend-mode" type="select">
		  <option value="add" selected="">Add</option>
		  <option value="diff">Diff</option>
		  <option value="subtract">Subtract</option>
		  <option value="multiply">Multiply</option>
		  <option value="screen">Screen</option>
		  <option value="lighten">Lighten</option>
		  <option value="darken">Darken</option>
		</select> 
</div>

<div class="image-filter-name-color-lbl">Color:  </div>
<div class="image-filter-control-color-input"><input type="color" value="#00f900" id="blend-color"></label> </div>

</div>

<!---end yogesh-24-2-16-->
</div><!--filterdiv end-->
<div id="perspectiveDiv" class="myHideClse" style="display:none;">
	<span>a:</span><input type="number" value="1" min="-200" max="200" id="a" step="0.1"><p></p>
      <span>b:</span><input type="number" value="0" min="-98" max="98" step="0.1" id="b"><p></p>
      <span>c:</span><input type="number" value="0" min="-98" max="98" step="0.1" id="c"><p></p>
      <span>d:</span><input type="number" value="1" min="-2.4" max="2.4" step="0.1" id="d"><p></p>
     <!--<span>tx:</span><input type="number" value="0" min="-200" max="200" step="0.1" id="tx"><p></p>
      <span>ty:</span><input type="number" value="0" min="-200" max="200" step="0.1" id="ty"><p></p>-->  <br>

      <code style="display:none;">
    transformMatrix == [
    <span id="a-val">1</span>,
    <span id="b-val">0</span>,
    <span id="c-val">0</span>,
    <span id="d-val">1</span>
    <!--<span id="tx-val">0</span>,
    <span id="ty-val">0</span>-->
    ]
  </code>
</div>
<div id="transformDiv" class="myHideClse" style="display:none;">
   
<div class="ImagesFilter_wrp">
    <span class="ImagesFilter_option_wrp">
	 <span class="left_sd">Swirl:</span><br>
	</span>
	<span class="ImagesFilter_option_wrp">
	 <span class="left_sd">Bulge/Pinch:</span><br>
	</span>
	<span class="ImagesFilter_option_wrp"> 
	  <span class="left_sd">Strength:</span><br>
	  <span class="right_sd"><input type="range" id="myWarp9"  value="0" min="-1" max="1" step="0.1"></span>
	</span>
	<span class="ImagesFilter_option_wrp">
	  <span class="left_sd">X:</span><br>
	  <span class="right_sd"><input type="range" id="myWarpX"  value="0" min="0" max="800" step="1"></span>
	</span> 
	<span class="ImagesFilter_option_wrp ">
	<span class="left_sd">Y:</span><br>
	<span class="right_sd"><input type="range" id="myWarpY"  value="0" min="0" max="800" step="1"></span>
    </span>
</div>	
	
</div>
	</div>
</div>
</div>

<div class="loadingPopups" id="loadingPopups1" style="display:none;">
<center><img src="<?php echo base_url()?>img/happy-lodar1.gif" id="saveLoading" style="width:100px;height:100px;"></center><br>
<span id="waitTexts" style="font-size: 19px;margin: 8px 0 0 20px;">&nbsp;Please Wait...</span>
</div>
<div class="loadingPopups2" id="loadingPopups2" style="display:none;">
<center><img src="<?php echo base_url()?>img/happy-lodar1.gif" id="saveLoading" style="width:100px;height:100px;"></center><br>
<span id="waitTexts" style="font-size: 19px;margin: 8px 0 0 20px;">&nbsp;Please Wait...</span>
</div>
<!--image for storing temp image-->
<!--ImageEditPopupTab-->
<div class="ImageEditTab" id="imgEditPopTab" style="display:none;"><span style="float:left;">Image Edit</span><div class="" style="float:right;"><img src="<?php echo base_url(); ?>img/maximize-window-xxl.png" onclick="maximizePopups()" title="Maximize Tab" style="cursor:pointer;width:15px;height:14px;"><span class="" style="margin-left:10px;" onclick="closeMyPopups()">X</span></div></div>

<div id="myoverlay"></div>

<!--projects popup-->
<div class="projectPanel" id="projectsPanel" style="display:none">
	<div class="projectHead" onclick="close_pro_div()">X</div>
	<div class="projectBody">
		<div class="new_project_header">
			<div class="pro_heading">New Project</div>
			<input type="text" class="pro_texts" id="projectsName" placeholder="New Project">
		</div>
		<div class="projects_list">
			<div class="pro_heading" style="margin: 0 0 7px;">All Projects</div>
			<ul id="projectList">
					<!-- <small>No Projects</small>  -->
			</ul>
		</div>
	</div>
		<div class="footersBtn">
			<button class="my_pro_btn" onclick="newProjects()">Create</button>
			<button class="my_pro_btn" onclick="close_pro_div()">Cancel</button>
		</div>
</div>
<input type="hidden" id="project_ids">
<div class="myImgEdits2" id="menuPopup" style="display:none;height:550px !important;">
	<div class="cls clsbtnCross" onclick="closeMenuPopups()">X</div>
	<div class="page_heading">Custom menu<div class="" style="float:right;display:none;"><img src="<?php echo base_url();?>img/minimize-window-xxl.png" onclick="minimizePopups()" title="Minimize Tab" style="cursor:pointer;width:15px;height:14px;margin: 0 36px 0 0;"></div>
	</div>
	<div class="menuBody">
		<div class="createMenusBox">
				<span class="menuTitles">Create New Menu</span><br>
				<input type="text" class="form-control" id="parentMenu" placeholder="Create Menu">
				<button class="myBtns" onclick="createMenus()">Create Menu</button>
				<select class="" id="submenus">
					<option value="" selected>Select Menu</option>
				</select>
			</div>
		<div class="menuAddColumns">
			<span class="menuTitles">Create Sub Menu</span><br>
			<input type="text" class="form-control" id="addItems" placeholder="Add new submenu">
			<button class="myBtns" onclick="addNewMenu()">Add submenu</button>
			<section id="demo">
				<ol class="sortable">
				</ol>
			</section> <!-- END #demo -->
			<!-- <p>
				<button name="" id="getHtmlcode" class="myBtns" onclick="getHtmlCode()">Get Html code</button><br>
			</p> -->
			<code id="htmlCodes"></code>
			<p>
				<button name="" id="getHtmlcode" class="myBtns" onclick="saveMenu()">Save Menu</button><br>
			</p>
			<p>
				<em>Note: This demo has the <code>maxLevels</code> option set to '3'.</em>
			</p>
		</div>
		<div class="visualPanel" style="float:right;">
			<h2>Menu visual panel</h2>
			<ul id="visualMenus">
				<!-- <li>Home<br><img src="black.png" class="imgSizew"><br><img src="blue.jpg" class="imgSizew" id="menuDown"><img src="blue.jpg" class="imgSizew" id="menuHorizontal" style="display:none;"></li> -->
			</ul>
		</div>
	</div>

</div>

<!--end of popup-->
<!--Menu popup-->
<!--end menu popup-->
	<script>
	function showMyMenu(menuId,vals)
	{
		if(vals=="on")
		{
		$("#"+menuId).show();	
		}
		if(vals=="off")
		{
		$("#"+menuId).hide();	
		}
		
		$("#"+menuId).mouseover(function()
		{
			$(this).show();
			}).mouseout(function(){$(this).hide()});
	}
	$(document).ready(function()
	{
		//alert("sadfsadfsad");	
		$.noConflict();	
		$(".mCS_img_loaded").mouseover(function()
		   {
			$(".logo_section").find('a').css("color","orange");
		   }).mouseout(function()
		   {
			$(".logo_section").find('a').css("color","#fff");
			});
			$("#FONTS_LIBRARY_PANEL").find('img').hide();
		   
		    $("#myLoadrs").show();
		 // $.ajax({
			//  type:"GET",
			//  url:"getImages.php",
			// success:function(mydata)
			// {
			// 	 $("#myLoadrs").hide();
			// 	 $(".pages_wrapper").find("ul").append(mydata);
			// }
			//  });
		 var heights=$(window).height();
		 $("#pageLayerPanel.column").css("height",heights);
		  //$("#propertyPanel.column").css("height",heights);
		  $("#delayTime, #loopTime").keypress(function(event){
			  if ( event.which == 45 ) {
			      event.preventDefault();
			   }
			});

		 
		  if($("#DRIVE_FILE_BROWSER_PANEL li").find("div").text()=="FileUploadProcess.html")
		  {
		  	$("#DRIVE_FILE_BROWSER_PANEL li").hide();
		  }

		  //$("body").ruler(); this code for ruler
	});

function showMyMenu2(menuId,vals)
{
	if(vals=="on")
	{
	$("."+menuId).show();	
	}
	if(vals=="off")
	{
	$("."+menuId).hide();	
	}
	
	$("."+menuId).mouseover(function()
	{
		$(this).show();
		}).mouseout(function(){$(this).hide()});
}

function showBlocks(tools)
{	
	$("#blocks").show();
	$("#blocks").draggable({ disabled: false });

	switch(tools)
	{
		case "fonts":$(".myContent").hide();$("#FONTS_LIBRARY_PANEL").show();
						$("#FONTS_LIBRARY_PANEL li").find("button").hide();
						$("#toolName").html("Fonts Tool");
						break;
		
		case "gradtools":$(".myContent").hide();$("#GRADIENTS_LIBRARY_PANEL").show();
						$("#toolName").html("Gradient Tool");
						break;
							
		case "pattern":$(".myContent").hide();$("#PATTERNS_LIBRARY_PANEL").show();
						$("#toolName").html("Pattern Tool");
						break;

		case "texture":$("#blocks").draggable({ disabled: true });
						$(".myContent").hide();$("#TEXTURE_LIBRARY_PANEL").show();
						$("#myLoadrs").show();
		 			   $("#TEXTURE_LIBRARY_PANEL").load('texture.html',function()
		 			   	{
		 			   		$("#myLoadrs").hide();
		 			   	});
		 			   $("#toolName").html("Texture Tool");
						break; 
						
				
	}
	$("#fontMenus").hide();
}
function closeToolBlocks()
{
	$("#blocks").hide();
}
$(window).load(function()
{
	//alert("sdfs");
	setInterval(function(){if($("#userId").val()=="<?php echo $this->session->userdata('user_id'); ?>")
	{}else{alert("You have must login to the system");}},5000);
	
	$(".parameter_font").hide();
	$(".RemoveBlends").hide();
	$(".placeHtml").hide();
	$("#SELECTION_ACTIONS_2D_toolButton").css({"background":"none","cursor":"none"});
	$("#SAVE_toolButton").css({"background":"none","cursor":"none"});
	localStorage.clear();
	var ab=new Array();
	var a=$("#FONTS_LIBRARY_PANEL").find("li").length;
	for(var i=0;i<a;i++)
	{
	// ab.push($("#FONTS_LIBRARY_PANEL").find("a").eq(i).text());
	var b=$("#FONTS_LIBRARY_PANEL").find("a").eq(i).text();
	 $("#FONTS_LIBRARY_PANEL").find("a").eq(i).css("font-family",b); 
	}
	 $("#imagePreviewPopup").draggable();
	 $("#FONTS_LIBRARY_PANEL li").find("button").hide();
	 $("#LAYERS_2D_PANEL").find("li").last().hide();
	// $('canvas').on('contextmenu', function(e){ return false; });
	$("#DOCUMENTS").bind('contextmenu', function(e) {
    return false;
	});
	$("#transformCanvases").children().hide();
	//$("#myWorkspace").removeClass('lockedTrue');
	//$("#myWorkspace").addClass('lockedFalse');


	// setTimeout(function(){},1000);
	
	
	
});
$(document).on('click', '.toggle-button', function() {
    $(this).toggleClass('toggle-button-selected'); 
    $("#loadingPopups2").show();
    if($(".toggleFunctions").find("div").attr("class")=="toggle-button toggle-button-selected")
    {
    	//alert("Transform");
    	$(".myHideClse").hide();
    	$("#imgEditPop").find("ul").eq(0).hide();
    	$("#filterDiv").hide();
    	$("#transformDiv").show();
    	$("#myCanvas").parent().hide();
    	$("#transformCanvases").children().show();
    	getPngImageFromPreCanvas();
    	setTimeout(function(){loadGlfx();$("#transformCanvases").find("canvas").eq(1).remove();},1000);
    	$("#fabricSaveBtn").hide();
    	$("#glfxSaveBtn").show();
    	$("#loadingPopups2").hide();
    	
    }
    else
    {
    	//alert("Filter");
    	$(".myHideClse").hide();
    	$("#imgEditPop").find("ul").eq(0).show();
    	$("#transformDiv").hide();
    	$("#filterDiv").show();
    	$("#transformCanvases").children().hide();
    	$("#myCanvas").parent().show();
    	getPngImageFromPostCanvas();
    	$("#glfxSaveBtn").hide();
    	$("#fabricSaveBtn").show();
    	$("#loadingPopups2").hide();

    }
});
function showmyPrivateblocks()
{

	$("#myPrivatePopup").show();
}
function getPngImageFromPreCanvas()//this function for getting image from previous canvas(fabric canvas)
{
	//var	canvas = new fabric.Canvas("myCanvas");
	canvas.deactivateAll().renderAll();
   $("#myGlfxImg").attr("src",canvas.toDataURL('image/png'));
}
var extArrays=['html'];
function changeExtention()
{
	var viewConditions=$("#extId").val();
	switch(viewConditions)
	{
		case "htmls":$(".ImgViews").hide();
					$(".myHtmlPanel").show();
						
						break;

		case "pngs":$(".ImgViews").hide();
							$("#DRIVE_FILE_BROWSER_PANEL2").show();
						
						break;

		case "gifs":$(".ImgViews").hide();
						$("#DRIVE_FILE_BROWSER_PANEL3").show();
						break;
	}
}
</script>
<script src="<?php echo base_url();?>js/ant_tool_js/fabric.js"></script>
<script src="<?php echo base_url();?>js/ant_tool_js/fabricImageEdit.js"></script>
<script src="<?php echo base_url();?>js/ant_tool_js/imageFilers.js"></script>
<script src="<?php echo base_url();?>js/ant_tool_js/glfx.js"></script>
<script src="<?php echo base_url();?>js/ant_tool_js/glfx_Img_warp.js"></script>


<script>
function minimizePopups()
{

}
function getPngImageFromPostCanvas()//this function for getting image from post canvas(glfx canvas)
{

}
function closeThisPops()
{
	$("#DRIVE_FILE_BROWSER_WIN").hide();
}
</script>
	
	
</body>
<!--Gif creation code-->
<!--<div class="frameAnimation" id="frmAnim" style="display:none;">
	<div class="faHeader"><span class="pull-left animationTitle">Animation Panel</span><span class="pull-right" onclick="AnimationPanel()"><a href="javascript:void(0);"><img src="../img/deletes2.png" style="margin: 6px 7px 0 0;width: 20px;"></a></span></div>
	<div class="faBody">
		<ul id="imagePanels">
			<li class="imgOutPnl"><img src="../img/default_image1.gif" class="imgSize"></li>
			<li class="imgOutPnl"><img src="../img/default_image1.gif" class="imgSize"></li>
		</ul>
	</div>
	<div class="faFooter">
		<ul class="">
			<li class="footerTexts" onclick="convertLayerToImage()"><a href="javascript:void(0);">Get Active Layer</a></li>
			<li><a href="javascript:void(0);" onclick="removeFrames()" title="Remove Frame"><img src="../img/white-x.png" class="gifBtn"></a></li>
			<li><a href="javascript:void(0);" onclick="generateGIFS()" title="Generate GIF"><img src="../img/check-mark-xxl.png" class="gifBtn"></a></li>
			</ul>
	</div>
</div>-->
<!--end of Gif creation code-->	
<script src="<?php echo base_url();?>js/ant_tool_js/jquery-1.7.2.min.js"></script>
<script src="<?php echo base_url();?>js/ant_tool_js/jquery-ui-1.8.16.custom.min.js"></script>
<script src="<?php echo base_url();?>js/ant_tool_js/jquery.ui.touch-punch.js"></script>
<script src="<?php echo base_url();?>js/ant_tool_js/jquery.mjs.nestedSortable.js"></script>


</html>