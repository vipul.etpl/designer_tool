<!DOCTYPE html>
<html >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Happy Edit</title>
		<meta name="description" content="The online graphics editor - Create Vector Graphics for the web in HTML, SVG and PHP"/>	    
		<meta http-equiv="Content-Language" content="en-EN" />
		
		<!-- MOBILE SETUP -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />		

		<!-- GLOBAL CSS -->	
                <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">	
		<link rel="stylesheet" href="<?php echo base_url()?>css/ant_tool_css/system_style.css" />
		<link rel="stylesheet" href="<?php echo base_url()?>css/ant_tool_css/janvas_style.css" />
		<link rel="stylesheet" href="<?php echo base_url()?>css/ant_tool_css/animate.min.css" />
		<link rel="stylesheet" href="<?php echo base_url()?>css/ant_tool_css/ant_tool_css/codemirror.css" />	
		<link rel="stylesheet" href="<?php echo base_url()?>css/ant_tool_css/happystyle.css" />	
		<link href="<?php echo base_url()?>css/ant_tool_css/media.css" rel="stylesheet"/>
		<link href="<?php echo base_url()?>css/ant_tool_css/font.css" rel="stylesheet"/>
		<link href="<?php echo base_url()?>css/ant_tool_css/font-awesome.css" rel="stylesheet"/>
		<!-- <link href="<?php echo base_url()?>assets/skin/ant_tool_css/font-awesome.min.css" rel="stylesheet"/>	 -->
		<link href="img/favicon.ico" rel="shortcut icon"/>	

		<!--Scroll CSS here-->
		<link href="<?php echo base_url()?>css/ant_tool_css/jquery.mCustomScrollbar.css" type="text/css" rel="stylesheet">
		<script src="<?php echo base_url()?>js/ant_tool_js/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>js/ant_tool_js/jQueryUI.js"></script>    		
		<script src="<?php echo base_url()?>js/ant_tool_js/jquery.mCustomScrollbar.js" type="text/javascript"></script>

		<!-- JANVAS MINIFIED -->	    		
		<script language="javascript" src="<?php echo base_url()?>js/ant_tool_js/janvas_minified.js"></script>

		<!-- GOOGLE IN APP PAY PRODUCTION -->
		<!-- // <script src="https://wallet.google.com/inapp/lib/buy.js"></script> -->
		
		<!-- GOOGLE FONT -->
		<script src="<?php echo base_url()?>js/ant_tool_js/antWebfont.js"></script>
		
		<!-- GOOGLE APIS -->
		<script type="text/javascript" src="<?php echo base_url()?>js/ant_tool_js/googleclient.js"></script>
		<script type="text/javascript" src="<?php echo base_url()?>js/ant_tool_js/jspdf.min.js"></script>	
	
	</head>

<body class="unselectable" >	
		
	<div id="PRINTABLE_CONTENT" >
	</div>
	
	<div id="TEMPLATES" >
	</div> <!-- TEMPLATES -->
			
	<div id="APPLICATION" tabindex="1" >
		
		<!-- <div id="LAYOUT" > -->	
		<div class="header">
			<div class="top_navsection">
				<div class="logo_section"><a href="index.html">Happy Edit</a></div>
				<div class="edit_section">
					<ul>
						<li><a href="javascript:void(0);" onmouseover="showDocMenu('on')" onmouseout="showDocMenu('off')">File</a></li>
						<li><a href="javascript:void(0);" onmouseover="showEditMenu('on')" onmouseout="showEditMenu('off')">Edit</a></li>
						<li>
							<a href="javascript:void(0);" onmouseover="showFontTools('on')" onmouseout="showFontTools('off')">Tools</a>
							<ul class="popupMenu dropShadow" id="fontMenus" style="display:none;" >
								<li id="fontTools" onclick="showBlocks('fonts')">Fonts</li>
								<li id="GradientTools" onclick="showBlocks('gradtools')">Gradient Tool</li>
							</ul>
						</li>
						<li style="float:right"><a><?php echo "Welcome ".$this->session->userdata("first_name");?></a></li>
						<li><a href="<?php echo base_url()?>index.php/dashboard">Dashboard</a></li>
						<li style="display:none;"><a href="">Layer</a></li>
						<li style="display:none;"><a href="">Window</a></li>
						<li style="display:none;"><a href="">Help</a></li>
						<input type="hidden" id="userId" value="<?php echo $this->session->userdata("user_id"); ?>">
					</ul>
				</div>
				<!--
					<div class="top_navsection_rt">
						<ul>
							<li class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><a href=""><i class="fa fa-minus"></i></a></li>
							<li><a href=""><i class="fa fa-square-o"></i></a></li>
							<li><a href=""><i class="fa fa-times"></i></a></li>
						</ul>
					</div>
				-->	
			</div>
			
		<div id="blocks" class="toolBlocks" style="display:none;">
			<div class="blockTitle hbar"><span>Tools</span><span class="closeBtn" onclick="closeToolBlocks()">&times;</span></div><p></p>
			<div class="blockContent">
				<div id="FONTS_LIBRARY_PANEL" class="fontContent" style="display:none;" ></div>
				<div id="GRADIENTS_LIBRARY_PANEL" class="gradContent" style="display:none;"></div>
			</div>
		</div>

			<div class="tool_optional_section">
				<div class="tol_btn "><a href="index.html">Tools Option <i class="fa fa-angle-right"></i></a></div>
				<div class="top_arow"><img src="<?php echo base_url()?>img/ant_tool_img/arow.jpg"></div>
				<div class="no_obj" style="display:none;">No Object Selected</div>
				<div class="no_obj_shap" style="display:none;">
					<div class="shap_wrapper mr10"><img src="<?php echo base_url()?>img/ant_tool_img/1.png"></div>
					<div class="shap_wrapper sm"><img src="<?php echo base_url()?>img/ant_tool_img/3.png"></div>
					<div class="shap_wrapper sm"><img src="<?php echo base_url()?>img/ant_tool_img/4.png"></div>
					<div class="shap_wrapper sm"><img src="<?php echo base_url()?>img/ant_tool_img/5.png"></div>
				</div>
				<div class="no_obj_defult" style="display:none;">
					<input type="checkbox" value="Bike" name="vehicle">Default Fill and Stroke
				</div>
				<div class="no_obj_align" style="display:none;">
					<div class="blank_aligment">
						<span class="align_bl"><img src="<?php echo base_url()?>assets/img/ant_tool_img/7.png"></span>
						<span class="align_bl"><img src="<?php echo base_url()?>assets/img/ant_tool_img/8.png"></span>
						<span class="align_bl"><img src="<?php echo base_url()?>assets/img/ant_tool_img/9.png"></span>
					</div>
					<div class="blank_aligment pl0">
						<span class="align_bl"><img src="<?php echo base_url()?>assets/img/ant_tool_img/10.png"></span>
						<span class="align_bl"><img src="<?php echo base_url()?>assets/img/ant_tool_img/11.png"></span>
						<span class="align_bl"><img src="<?php echo base_url()?>assets/img/ant_tool_img/12_1.png"></span>
					</div>
					<div class="blank_aligment">
						<span class="align_bl"><img src="<?php echo base_url()?>assets/img/ant_tool_img/12.png"></span>
						<span class="align_bl"><img src="<?php echo base_url()?>assets/img/ant_tool_img/13.png"></span>
						<span class="align_bl"><img src="<?php echo base_url()?>assets/img/ant_tool_img/14.png"></span>
					</div>
					<div class="blank_aligment pdl0">
						<span class="align_bl"><img src="<?php echo base_url()?>assets/img/ant_tool_img/15.png"></span>
						<span class="align_bl"><img src="<?php echo base_url()?>assets/img/ant_tool_img/16.png"></span>
						<span class="align_bl"><img src="<?php echo base_url()?>assets/img/ant_tool_img/17.png"></span>
					</div>
				</div>
			</div>

		</div>			


			
		<div id="CONTENT" >

			<div class="lt_menu_section">
				<!-- MAIN_TOOLBAR_2D -->
					<div class="tools_heading"><a href=""> Tools <i class="fa fa-angle-down"></i></a></div>
					<div class="">

					<ul id="MAIN_TOOLBAR_2D" class="toolbar main_tools hbar "  >	
					
					<li id="SELECT_GLOBAL_2D_toolButton"  toolName="selectGlobal_tool" localize="title" title="Objects selection" >
						<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/11_1.png"></a>
					</li>
				
				<li id="SELECT_LOCAL_2D_toolButton"  toolName="selectLocal_tool" localize="title" title="Points selection" >
					<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/19.png"></a>
				</li>
				
		    	<li id="PAN_2D_toolButton"  toolName="panView_tool"  title="Hand Tool"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/Hand-tool-icon.png" style="width:20px;height:20px;"></a></li>
		    	<li id="ZOOM_2D_toolButton"  toolName="zoomView_tool"  title="Zoom In/Zoom Out"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/28.png"></a></li>
				
				
				<li id="PATH_TOOLS_2D_toolButton" class="arrowSubElements" toolName="drawPath_tool" title="Path Selection">
					<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/penTool.svg"></a>
		        	<ul class="toolbar hbar dropShadow shr">
				        <li id="PATH_2D_toolButton"  toolName="drawPath_tool"  title="Path Creation"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/penTool.svg"></a></li>
				        <li id="ADD_PATH_POINT_2D_toolButton"  toolName="addPathPoint_tool" title="Add Path"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/penTooladd.svg"></a></li>
						<li id="REMOVE_PATH_POINT_2D_toolButton"  toolName="removePathPoint_tool" title="Remove Path"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/penToolminus.svg"></a></li>
				        <li id="SPLIT_PATH_2D_toolButton"  toolName="splitPath_tool"  title="Delete Path"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/penCut.svg"></a></li>
				        <li id="CONVERT_PATH_POINT_2D_toolButton"  toolName="convertPathPoint_tool" title="Convert Path"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/pens.svg"></a></li>
		        	</ul>
		        	<a></a>       	
		        </li>

		        <li id="DRAW_FREEHAND_TOOLS_2D_toolButton" class="arrowSubElements" toolName="drawFreeHand_tool"  title="Free Drawing">
		        	<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/pencils.svg"></a>
					<ul class="toolbar hbar dropShadow shr" >
						<li id="FREEHAND_2D_toolButton"  toolName="drawFreeHand_tool" title="Pencil" ><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/pencils.svg"></a></li>
						<li id="BRUSH_2D_toolButton"  toolName="drawBrush_tool"  title="Brush"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/brushs.svg"></a></li>
					</ul>
					<a></a>
				</li>
				
		        <li id="DRAW_TOOLS_2D_toolButton" class="arrowSubElements" toolName="drawRectangle_tool" title="Shape">
		        	<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/shapes.png" style="width:20px;height:20px;"></a>
		        	<ul class="toolbar hbar dropShadow shr">
						<li id="RECT_2D_toolButton"  toolName="drawRectangle_tool" title="Rectangle" ><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/shapes.png" style="width:20px;height:20px;"></a></li>
						<li id="ELLIPSE_2D_toolButton"  toolName="drawEllipse_tool"  title="Circle"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/circle2.png" style="width:20px;height:20px;"></a></li>
						<li id="LINE_2D_toolButton"  toolName="drawLine_tool" title="Line"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/line-2.png" style="width:20px;height:20px;"></a></li>
						
					</ul>
		        	<a></a>       	
		        </li>
				
				<li id="TEXT_2D_toolButton"  toolName="drawTextBox_tool" title="Text Tool" ><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/25.png"></a></li>
				
				<li id="EDIT_GRAPHIC_ATTRIBUTES_2D_toolButton" class="arrowSubElements" toolName="eyeDropper_tool" title="Color Picker">
					<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/eyeDrop.svg"></a>
					<ul class="toolbar hbar dropShadow shr">
						<li id="EDIT_GRADIENT_POINTS_2D_toolButton"  toolName="editGradientPoints_tool" title="Gradient Tool"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/gradPointer.svg"></a></li>
						<li id="EYEDROPPER_2D_toolButton"  toolName="eyeDropper_tool" title="Color Dropper"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/eyeDrop.svg"></a></li>
					</ul>
					<a></a>
				</li>

				<li id="EDIT_TRANSFORM_2D_toolButton" class="arrowSubElements"  title="Rotate tool">
					<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/rotations.svg"></a>
					<ul class="toolbar hbar dropShadow shr">
						<li id="ROTATE_2D_toolButton"  toolName="rotate_tool"  title="free Rotation"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/rotations.svg"></a></li>
						<li id="HOR_REFLECT_toolButton"  title="Vertical Rotation"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/verticals.svg"></a></li>
						<li id="VER_REFLECT_toolButton"  title="Horizontal Rotation"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/horizontal.svg"></a></li>
					</ul>
					<a></a>
				</li>
				
				
				

				<li id="BRING_FRONT_toolButton" class="arrowSubElements" style="display:none;">
					<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/29.png"></a>
					<ul class="toolbar hbar dropShadow shr">
						<li id="BRING_UP_toolButton"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/29.png"></a></li>
					</ul>
					
				</li>
				<li id="SEND_BACK_toolButton" class="arrowSubElements" style="display:none;">
					<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/30.png"></a>
					<ul class="toolbar hbar dropShadow shr">
						<li id="BRING_DOWN_toolButton"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/30.png"></a></li>
					</ul>
					<a></a>
				</li>
				
				
				<li id="ALIGNMENTS_toolButton" class="arrowSubElements" title="Text Alignment Tool">
					<a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/center.svg"></a>
					<ul class="toolbar hbar dropShadow ali">
						<li id="TEXT_ALIGN_LEFT" title="Left Alignment"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/left.svg"></a></li>
						<li id="TEXT_ALIGN_CENTER" title="Center Alignment"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/center.svg"></a></li>
						<li id="TEXT_ALIGN_RIGHT" title="Right Alignment"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/right.svg"></a></li>
		

						<li id="ALIGN_LEFT"  style="display:none;"><a><img class="mCS_img_loaded" src="img/janvas_icons/align_left_icon.svg"></a></li>
		        		<li id="ALIGN_VCENTER"  style="display:none;"><a><img class="mCS_img_loaded" src="img/janvas_icons/align_vcenter_icon.svg"></a></li> 
				        <li id="ALIGN_RIGHT"  style="display:none;"><a><img class="mCS_img_loaded" src="img/janvas_icons/align_right_icon.svg"></a></li>
				        
				        <li id="ALIGN_TOP"  style="display:none;"><a><img class="mCS_img_loaded" src="img/janvas_icons/align_top_icon.svg"></a></li>
				        <li id="ALIGN_HCENTER" style="display:none;"><a><img class="mCS_img_loaded" src="img/janvas_icons/align_hcenter_icon.svg"></a></li>
				        <li id="ALIGN_BOTTOM" style="display:none;"><a><img class="mCS_img_loaded" src="img/janvas_icons/align_bottom_icon.svg"></a></li>
		
				        <li id="DISTRIBUITE_LEFT" style="display:none;"><a><img class="mCS_img_loaded" src="img/janvas_icons/distribuite_left_icon.svg"></a></li>
		        		<li id="DISTRIBUITE_VCENTER" style="display:none;"><a><img class="mCS_img_loaded" src="img/janvas_icons/distribuite_vcenter_icon.svg"></a></li> 
				        <li id="DISTRIBUITE_RIGHT" style="display:none;"><a><img class="mCS_img_loaded" src="img/janvas_icons/distribuite_right_icon.svg"></a></li>
				        
				        <li id="DISTRIBUITE_TOP" style="display:none;"><a><img class="mCS_img_loaded" src="img/janvas_icons/distribuite_top_icon.svg"></a></li>
				        <li id="DISTRIBUITE_HCENTER" style="display:none;"><a><img class="mCS_img_loaded" src="img/janvas_icons/distribuite_hcenter_icon.svg"></a></li>
				        <li id="DISTRIBUITE_BOTTOM" style="display:none;"><a><img class="mCS_img_loaded" src="img/janvas_icons/distribuite_bottom_icon.svg"></a></li>
				        
				        <li id="DISTRIBUITE_HSPACE" style="display:none;"><a><img class="mCS_img_loaded" src="img/janvas_icons/distribuite_hspace_icon.svg"></a></li>
				        <li id="DISTRIBUITE_VSPACE" style="display:none;"><a><img class="mCS_img_loaded" src="img/janvas_icons/distribuite_vspace_icon.svg"></a></li>

		        	</ul>
				</li>
	
				<li id="UNDO_toolButton" title="Undo Tool"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/undo.svg"></a></li>
		    	<li id="REDO_toolButton" title="Redo Tool"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/janvas_icons/redo.svg"></a></li>
				<li id="TRASH_toolButton" title="Trash Tool"><a><i class="fa fa-trash mCS_img_loaded" style="color: rgb(204, 204, 204); font-size: 19px;"></i></a></li>
				<li id="PRINT_toolButton" style="display:none;"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/35.png"></a></li>
				<li id="SAVE_toolButton" class="arrowSubElements"  >
					<a style="display:none;"><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/36.png"></a>
					<ul class="popupMenu dropShadow nedoc" id="docMenus" style="display:none;left:-1px; top:100%;" >
						<li  id="CREATE_FILE_HTML_menuItem" class="html_icon16x16" style="background-size: 16px 16px; background-position-x: 8px;" docTitle="untitled" fileExtension="html" >New document</li>
					    <li class="divisor" style="display:none;"></li>
						<li id="OPEN_menuItem"  style="display:none;">Open…</li>
						<li id="OPEN_GOOGLE_DRIVE_menuItem"  style="display:none;"><a style="color:white;" href="https://drive.google.com" target="_blank" >Open Google Drive</a></li>
						<li class="divisor" style="display:none;"></li>
				        <li id="SAVE_menuItem" style="display:none;">Save</li>
				        <li id="SAVE_AS_menuItem" style="display:none;">Save as...</li>
				        <li class="divisor" style="display:none;"></li>
				        <li id="UPLOAD_FILE_menuItem" style="display:none;position:relative;" >Upload file...<input id="UPLOAD_FILE_TEXTFIELD" type="file" style="position:absolute; display: block; left:0; top:0; opacity:0; width:100%;"/></li>
				        <li class="divisor" style="display:none;"></li>
				        <li id="SAVE_AS_START_TEMPLATE_menuItem">Save</li>
				        <li id="RESET_START_TEMPLATE_menuItem" >Reset</li>
				        <li class="divisor" ></li>
						<li id="GENERATE_IMAGE_PNG_menuItem" draggable="true" >Save PNG image</li>
						<li id="GENERATE_IMAGE_JPG_menuItem" style="display:none;" draggable="true" >Preview JPG image</li>						
		        	</ul>
		        	
				</li>

				<li id="DRIVE_FILE_BROWSER_WIN_button" style="display:none;"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/img/32.png"></a></li>
		    	<li id="HELP_toolButton" style="display:none;"><a><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/img/33.png"></a></li>	

		    	<li id="SELECTION_ACTIONS_2D_toolButton" class="arrowSubElements">
					<a style="display:none;"><img class="mCS_img_loaded" src="<?php echo base_url()?>img/ant_tool_img/28.png"></a>
					<ul class="popupMenu dropShadow alfld" id="editMenus" style="display:none;left:-1px; top:100%;" >
				        <li id="CUT_menuItem" >Cut</li>
				        <li id="COPY_menuItem" >Copy</li>
				        <li id="PASTE_menuItem" >Paste</li>
				        <li id="PASTE_IN_PLACE_menuItem" >Paste in place</li>
				        <li class="divisor"></li>
				        <li id="SELECT_ALL_menuItem" >Select all</li>
				        <li class="divisor"></li>
				        <li id="DUPLICATE_menuItem"  >Duplicate</li>
				        <li class="divisor"></li>
		        		<li id="GROUP_menuItem"  style="display:none;">Group</li>
		        		<li id="UNGROUP_menuItem"   >Ungroup</li>
		        		<li class="divisor"></li>
						<li id="LOCK_SELECTION_menuItem" >Lock selection</li>
						<li id="UNLOCK_ALL_menuItem" >Unlock all</li>
						<li class="divisor" style="display:none;"></li>
		        		<li id="CREATE_COMPOSED_PATH_menuItem" style="display:none;">Compose Path</li>
		        		<li id="CREATE_MASK_menuItem" style="display:none;">Create Mask</li>
		        		<li class="divisor" style="display:none;"></li>			        
				        <li id="JOIN_POINTS_menuItem" style="display:none;">Join points</li>
				        <li id="TEXT_TO_PATH_menuItem" style="display:none;">Text to path</li>
				        <li class="divisor" style="display:none;"></li>			        
				        <li id="EFFECT_OUTER_SHADOW_menuItem" effectClassName="XOS_Shadow_effect" effectProperties="{'shadowOffsetX':5,'shadowOffsetY':5,'shadowBlur':5,'shadowColor':'rgba(0,0,0,.5)'}" >Outer shadow</li>			        
				        <li id="EFFECT_INNER_SHADOW_menuItem" effectClassName="XOS_Shadow_effect" effectProperties="{'shadowOffsetX':-1,'shadowOffsetY':-1,'shadowBlur':4,'shadowColor':'rgba(0,0,0,.75)'}" >Inner shadow</li>			        
		        		<li class="divisor" style="display:none;"></li>			        
				        <li id="USE_IMAGE_AS_PATTERN_menuItem" style="display:none;">Use Image as Pattern</li>
		        	</ul>
				</li>
				</ul><!-- MAIN_TOOLBAR_2D -->
			</div>

			<div class="lay" style="display:none;">
				<div class="tools_heading liht"><a href=""> Pages <i class="fa fa-angle-right"></i></a></div>
				<div class="tools_heading liht"><a href=""> Layers <i class="fa fa-angle-right"></i></a></div>
			</div>
				
			</div>
				
				
				<div class="column" style="left:0; right:388px;" >
					<ul id="TABS_DOCUMENTS" class="tabBar"></ul>
					<div id="DOCUMENTS" class="radioPanels"></div>
				</div>
				
				<div class="columnsResizer" style="right:194px;" ></div>
				
				<div class="column" style="width:194px; right:194px; background-color: #333333;" >
					<div class="page_heading">Layers</div>

					<div id="WIN_CONTENT" class="content" style="display:block !important;">				
							<div id="LAYERS_2D_PANEL" class="listView scrollableY darkScrollbar" ></div>
							<ul id="LAYERS_EDITING_TOOLBAR" class="footer toolbar hbar" style="overflow:visible;" >
				        		<li id="CREATE_LAYER_button" title="Add Layer"><a><i class="fa fa-plus"></i></a></li>
				        		<li id="REMOVE_LAYER_button" title="Remove Layer"><a><i class="fa fa-trash"></i></a></li>
				        		<li id="MOVE_LAYER_UP_button" title="Move Layer To Up"><a><img src="<?php echo base_url()?>img/ant_tool_img/bring_up_icon.png"></a></li>
				        		<li id="MOVE_LAYER_DOWN_button" title="Move Layer To Down"><a><img src="<?php echo base_url()?>img/ant_tool_img/bring_down_icon.png"></a></li>
				        		<li id="MOVE_SELECTION_IN_LAYER_button" title="" style="display:none;"><a></a></li>
							</ul>				
					</div>
					
					
					<div class="pages_wrapper">
					<div class="page_heading">Pages</div>
					<div class="page_list_wrapper ht400 all_tool_section mCustomScrollbar _mCS_2"><div class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" id="mCSB_2" style="max-height: none;" tabindex="0"><div dir="ltr" style="position:relative; top:0; left:0;" class="mCSB_container" id="mCSB_2_container">
						<ul>
							<!-- <li class="active">
								<span class="page_lt_tham ylo"></span>
								<span class="page_lt_text">Page One</span>
							</li> -->
							
						</ul>
					</div><div class="mCSB_scrollTools mCSB_2_scrollbar mCS-light mCSB_scrollTools_vertical" id="mCSB_2_scrollbar_vertical" style="display: block;"><a oncontextmenu="return false;" class="mCSB_buttonUp" href="#" style="display: block;"></a><div class="mCSB_draggerContainer"><div oncontextmenu="return false;" style="position: absolute; min-height: 30px; top: 0px; display: block; height: 179px; max-height: 290px;" class="mCSB_dragger" id="mCSB_2_dragger_vertical"><div class="mCSB_dragger_bar" style="line-height: 30px;"></div></div><div class="mCSB_draggerRail"></div></div><a oncontextmenu="return false;" class="mCSB_buttonDown" href="#" style="display: block;"></a></div></div></div>
				</div>

					<div id="LAYERS_WIN" class="win dropShadow layers_win closed"  style="display:none;">
						<div id="WIN_HEAD" class="head hbar">
							<div id="WIN_TITLE" class="title">Layers</div>
						</div>
						<div id="WIN_CLOSE_BUTTON" class="close_button"></div>
						<div id="WIN_COLLAPSE_BUTTON" class="collapse_button"></div>
						<div id="WIN_CONTENT" class="content"  >				
							<div id="LAYERS_2D_PANEL" class="listView scrollableY darkScrollbar" ></div>
							<ul id="LAYERS_EDITING_TOOLBAR" class="footer toolbar hbar" style="overflow:visible;" >
				        		<li id="CREATE_LAYER_button" title=""><a><i class="fa fa-plus"></i></a></li>
				        		<li id="REMOVE_LAYER_button" title=""><a><i class="fa fa-trash"></i></a></li>
				        		<li id="MOVE_LAYER_UP_button" title=""><a><img src="<?php echo base_url()?>img/ant_tool_img/bring_up_icon.png"></a></li>
				        		<li id="MOVE_LAYER_DOWN_button" title=""><a><img src="<?php echo base_url()?>img/ant_tool_img/bring_down_icon.png"></a></li>
								<li id="MOVE_SELECTION_IN_LAYER_button"   title=""><a></a></li>
							</ul>				
						</div>
						<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
					</div>

				</div>


				<div class="column" style="width:194px; right:0; background-color: #333333;" >
				
					<div class="page_heading">Properties</div>

					<div id="INSPECTOR_PANEL" class="scrollableY darkScrollbar radioPanels" >
					
					
					<div id="PAGE_PROPERTIES_2D_PANEL" >
							
							<div class="title" ></div>
							
							
							<div class="inspector_parameter parameter_pageFormats"  >	
							  	<!-- <div class="title">Page presets</div> -->
							  	<div class="property_container"  >
									<!-- <label for="DOC_SIZE_MENU">d</label> -->
									<select id="DOC_SIZE_MENU" style="display:none"   >
										<!-- <option selected="true" value="">Format</option>
										<optgroup label="Press">
											<option  value="595,842">A4 Vertical</option>
											<option  value="842,595">A4 Horizontal</option>
											<option  value="842,1190">A3 Vertical</option>
											<option  value="1190,842">A3 Horizontal</option>
											<option  value="516,728">B5  Vertical</option>
											<option  value="728,516">B5  Horizontal</option>
											<option  value="728,1032">B4  Vertical</option>
											<option  value="1032,728">B4  Horizontal</option>
											<option  value="792,1224">Tabloid Horizontal</option>
											<option  value="1224,792">Tabloid Vertical</option>
											<option  value="612,1008">Legal Horizontal</option>
											<option  value="1008,612">Legal Vertical</option>
											<option  value="612,792">Lettera Horizontal</option>
											<option  value="792,612">Lettera Vertical</option>
										</optgroup>
										<optgroup label="Web">
											<option  value="1024,768">1024 x 768</option>
											<option  value="980,600">980 x 600</option>
											<option  value="640,480">640 x 480</option>
										</optgroup>
										<optgroup label="Mobile">
											<option  value="320,240">320 x 240</option>
											<option  value="640,480">640 x 480</option>
											<option  value="640,320">640 x 320</option>
										</optgroup> -->
									</select>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_pageSize" >	
							  	<div class="title">Page Size</div>
							  	<div class="property_container"  >
							  		<label for="sizeX">x</label>
							  		<input  type="number" name="x" id="sizeX" onclick="this.select();" value="595"/>
							  	</div>
							  	<div class="property_container">														
							  		<label for="sizeY">y</label>
							  		<input  type="number" name="y" id="sizeY" onclick="this.select();"  value="842"/>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter parameter_pageAlignment" style="display:none" >	
							  	<div class="title">HTML Page alignment</div>
							  	<div class="property_container"  >
									<label for="pageAlignment">a</label>
									<select id="pageAlignment" name="pageAlignment"  >
											<option value="none">none</option>
											<option value="center-horizontally">center horizontally</option>
											<option value="center-horizontally-vertically">center horizontally vertically</option>
											<option value="no-margins">no margins</option>
									</select>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter" style="display:none">	
							  	<div class="title">Page background</div>
							  	<div class="property_container">
						  			<div class="color_container">
						  				<div id="bodyBackgroundColor" class="color">
						  				</div>
						  			</div>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_unityAndDecimal"  style="display:none">	
							  	<div class="title">units / decimals</div>
							  	
							  	<div class="property_container" >
							  		<label  for="UNITY_MENU">u</label>
									<select id="UNITY_MENU" >
										<option selected="true" value="UNIT_PIXEL">px</option>
										<option value="UNIT_MILLIMETER">mm</option>
										<option value="UNIT_CENTIMETER">cm</option>
										<option value="UNIT_METER">m</option>
										<option value="UNIT_KILOMETER">km</option>
										<option value="UNIT_FOOT">foot</option>
										<option value="UNIT_INCH">inch</option>
										<option value="UNIT_YARD">yard</option>
										<option value="UNIT_MILE">mile</option>
										<option value="UNIT_NAUTICAL_MILE">nautical mile</option>
									</select>
							  	</div>
							  	<div class="property_container">
									<label for="UNITY_DECIMALS_MENU">d</label>
									<select id="UNITY_DECIMALS_MENU"   >
										<option value="0">0</option>
										<option value="1">1</option>
										<option selected="true" value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
									</select>
							  	</div>
						  	</div>
						  	
						  	
						  	<div class="inspector_parameter  parameter_snapValue" style="display:none">	
							  	<div class="title">Snap range</div>
							  	<div class="property_container ">
							  		<label for="screenSnapValue">s</label>
							  		<input  type="text" name="screenSnapValue" id="screenSnapValue" onclick="this.select();" value="0"/>
							  	</div>
						  	</div>
						  	
						  	
						  	<div class="inspector_parameter  parameter_grid" style="display:none">	
							  	<div class="title">Snap grid</div>
							  	<div class="property_container">
							  		<label for="gridSnapValue">v</label>
							  		<input  type="text" name="gridSnapValue" id="gridSnapValue" onclick="this.select();"  value="0"/>
							  	</div>
						  	</div>
						  	
						  	
						  	<div class="inspector_parameter  parameter_moveByArrowValue" style="display:none">	
							  	<div class="title">Move value</div>
							  	<div class="property_container">
							  		<label for="moveByArrowValue">v</label>
							  		<input  type="text" name="moveByArrowValue" id="moveByArrowValue" onclick="this.select();"  value="0"/>
							  	</div>
						  	</div>
						  	
						  	
						  	<div class="inspector_parameter  " style="display:none">	
							  	<div class="title">Include files</div>
							  	<div class="property_container">
							  		<button  id="editIncludes_button"> CSS-JS </button>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  " style="display:none">	
							  	<div class="title">Page Title and Desc</div>
							  	<div class="property_container">
							  		<button  id="editTitleDescription_button"> Title - Desc... </button>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  " style="display:none">	
							  	<div class="title">Test page in browser</div>
							  	<div class="property_container">
							  		<button  id="showPagePreview_button"> Preview HTML </button>
							  	</div>
						  	</div>
						  	
						  								  
						</div> <!-- PAGE_PROPERTIES_2D_PANEL  -->




					<div id="GRAPHIC_PROPERTIES_2D_PANEL"  >
							
							<div class="title" >Object 2D</div>
						  	
						  	<div class="inspector_parameter  parameter_position_2D" >	
							  	<div class="title">Position</div>
							  	<div class="property_container">
							  		<label for="positionX">x</label>
							  		<input  type="text" name="positionX" id="positionX"  onclick="this.select();" />
							  	</div>
							  	<div class="property_container">														
							  		<label for="positionY">y</label>
							  		<input  type="text" name="positionY" id="positionY"  onclick="this.select();" />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_size_2D" >	
							  	<div class="title">Size</div>
							  	<div class="property_container">
							  		<label for="sizeX">x</label>
							  		<input  type="text" name="sizeX" id="sizeX"  onclick="this.select();"  />
							  	</div>
							  	<div class="property_container">														
							  		<label for="sizeY">y</label>
							  		<input  type="text" name="sizeY" id="sizeY"  onclick="this.select();" />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_rotation_2D" >	
							  	<div class="title">Rotation</div>
							  	<div class="property_container">
							  		<label for="rotation">r</label>
							  		<input  type="text" name="rotation" id="rotation"  onclick="this.select();"  />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_scale_2D" >	
							  	<div class="title">Scale</div>
							  	<div class="property_container">
							  		<label for="scaleX">x</label>
							  		<input  type="text" name="scaleX" id="scaleX"  onclick="this.select();"  />
							  	</div>
							  	<div class="property_container">														
							  		<label for="scaleY">y</label>
							  		<input  type="text" name="scaleY" id="scaleY"  onclick="this.select();" />
							  	</div>
						  	</div>
						  	
						  	
						  	<div class="inspector_parameter RemoveBlends"  style="display:none;">	
							  	<div class="title" style="display:none;">Blend mode</div>
							  	<div class="property_container"  >
									<label for="compositeOperation" style="display:none;">c</label>
									<select id="compositeOperation" name="compositeOperation"  style="display:none;">
											<!--
<option  value="source-atop">source-atop</option>
											<option  value="source-in">source-in</option>
											<option  value="source-out">source-out</option>
											<option  value="destination-atop">destination-atop</option>
											<option  value="destination-in">destination-in</option>
											<option  value="destination-out">destination-out</option>
											<option  value="destination-over">destination-over</option>
											<option  value="lighter">lighter</option>
											<option  value="copy">copy</option>
											<option  value="xor">xor</option>
-->
											<option  value="source-over">normal</option>
											<option  value="multiply">multiply</option>
											<option  value="screen">screen</option>
											<option  value="overlay">overlay</option>
											<option  value="darken">darken</option>
											<option  value="lighten">lighten</option>
											<option  value="color-dodge">color-dodge</option>
											<option  value="color-burn">color-burn</option>
											<option  value="hard-light">hard-light</option>
											<option  value="soft-light">soft-light</option>
											<option  value="difference">difference</option>
											<option  value="exclusion">exclusion</option>
											<option  value="hue">hue</option>
											<option  value="saturation">saturation</option>
											<option  value="color">color</option>
											<option  value="luminosity">luminosity</option>
											
											
											
														
									</select>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  " >	
							  	<div class="title">Stroke width</div>
							  	<div class="property_container">														
							  		<label for="lineWidth">s</label>
							  		<input  type="number" name="lineWidth" id="lineWidth"  onclick="this.select();" />
							  	</div>
							  	<div id="LINE_OPTIONS_BUTTON" class="icon arrowSubElements">
						        	<a></a>							  	
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_fontSize">	
							  	<div class="title">Font size</div>
							  	<div class="property_container">														
							  		<input  type="number" name="fontSize" id="fontSize" onblur="window.scrollTo(0, 0);" onclick="this.select();"/>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_textLineHeight"  style="display:none !important;">	
							  	<div class="title">Interline</div>
							  	<div class="property_container">														
							  		<label for="lineHeight">i</label>
							  		<input  type="number" name="lineHeight" id="lineHeight" onblur="window.scrollTo(0, 0);" onclick="this.select();" />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter  parameter_font" style="display:none !important;">	
							  	<div class="title">Font</div>
							  	<div class="property_container">														
							  		<label for="font">f</label>
							  		<input  type="text" name="font" id="font"  onclick="this.select();" />
							  	</div>
							  	<div id="FONT_LIST_MENU" class="icon arrowSubElements">
							  		<a>
						        	</a>
							  	</div>
						  	</div>
						  	
						  	
						  	<!--
<div class="inspector_parameter  parameter_font" >	
							  	<div class="title">Font</div>
							  	<div class="property_container">														
							  		<label for="font">f</label>
							  		<input  type="text" name="font" id="font"  onclick="this.select();" />
							  	</div>
							  	<div id="FONT_LIST_MENU" class="icon arrowSubElements">
						        	<a>
						        		<select id="fontList" name="fontList"  ></select>
						        	</a>
						        								  	
							  	</div>
						  	</div>
-->
						  	
						  	<!--
<div class="inspector_parameter parameter_fontStyle"  >	
							  	<div class="title">Font Style</div>
							  	<div class="property_container"  >
									<label for="fontStyle">s</label>
									<select id="fontStyle" name="fontStyle"  >
											<option  value="normal">Normal</option>
											<option  value="italic">Italic</option>
											<option  value="bold">Bold</option>
											<option  value="bold italic">Italic + Bold</option>
									</select>
							  	</div>
						  	</div>
-->
						  	
						  	
							<div class="inspector_parameter parameter_fontStyle"  style="display:none !important;">	
							  	<div class="title">Font Style</div>
							  	<div class="property_container"  >
									<label for="fontStyle">s</label>
									<select id="fontStyle" name="fontStyle"  >
											<option  value="normal">Normal</option>
											<option  value="italic">Italic</option>
											<option  value="oblique">Oblique</option>
									</select>
							  	</div>
						  	</div>

						  	<div class="inspector_parameter parameter_fontWeight"  >	
							  	<div class="title">Font Weight</div>
							  	<div class="property_container"  >
									<label for="fontWeight">s</label>
									<select id="fontWeight" name="fontWeight"  >
											<option  value="normal">Normal</option>
											<option  value="bold">Bold</option>	
											<option  value="lighter">Lighter</option>	
											<option  value="bold">Bold(Thick)</option>	
											<option  value="100">100</option>
											<option  value="200">200</option>
											<option  value="300">300</option>
											<option  value="400">400</option>
											<option  value="500">500</option>
											<option  value="600">600</option>
											<option  value="700">700</option>
											<option  value="800">800</option>
											<option  value="900">900(Thicker)</option>
									</select>
							  	</div>
						  	</div>

						  	<div class="inspector_parameter placeHtml"  >	
							  	<div class="title">Place into HTML</div>
							  	<div class="property_container"  >
									<label for="placeIntoHtmlLayer">h</label>
									<select id="placeIntoHtmlLayer" name="placeIntoHtmlLayer"  >
											<option  value="no">no</option>
											<option  value="yes">yes</option>
									</select>
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter parameter_instanceId" >	
							  	<div class="title">Instance</div>
							  	<div class="property_container">
							  		<label for="id">id</label>
							  		<input  type="text" name="id" id="id"    />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter parameter_class" >	
							  	<div class="title">Class</div>
							  	<div class="property_container">
							  		<label for="classNameList">cl</label>
							  		<input  type="text" name="classNameList" id="classNameList"    />
							  	</div>
						  	</div>
						  	
						  	
						  	<div class="inspector_parameter parameter_anchorLink" >	
							  	<div class="title">Link url</div>
							  	<div class="property_container">
							  		<label for="anchorLink">u</label>
							  		<input  type="text" name="anchorLink" id="anchorLink"    />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter parameter_anchorLinkTarget" >	
							  	<div class="title">Link target</div>
							  	<div class="property_container">
							  		<label for="anchorLinkTarget">t</label>
							  		<input  type="text" name="anchorLinkTarget" id="anchorLinkTarget"    />
							  	</div>
						  	</div>
						  							  	
						  	<!--
<div class="inspector_parameter parameter_contentUrl" >	
							  	<div class="title">Source url</div>
							  	<div class="property_container">
							  		<label for="sourceUrl">u</label>
							  		<input  type="text" name="sourceUrl" id="sourceUrl"    />
							  	</div>
						  	</div>
-->
						  	
						  	<div class="inspector_parameter parameter_contentUrl" >	
							  	<div class="title">Image url</div>
							  	<div class="property_container">
							  		<label for="imageUrl">u</label>
							  		<input  type="text" name="imageUrl" id="imageUrl"    />
							  	</div>
						  	</div>
						  	
						  	
						  	<div class="inspector_parameter parameter_contentUrl" >	
							  	<div class="title">Include url</div>
							  	<div class="property_container">
							  		<label for="includeUrl">u</label>
							  		<input  type="text" name="includeUrl" id="includeUrl"    />
							  	</div>
						  	</div>
						  	
						  	
						  	
							<!--
<div class="inspector_parameter parameter_contentUrl"  >	
							  	<div class="title">Include mode</div>
							  	<div class="property_container"  >
									<label for="includeMode">i</label>
									<select id="includeMode" name="includeMode"  >
											<option value="includeSVG-js">include SVG - js</option>
											<option value="includeSVG-php">include SVG - php</option>
											<option value="includeHTML-php">include HTML - php</option>
											<option value="iframe">iframe</option>
											<option value="inlineHTML">inline HTML</option>
									</select>
							  	</div>
						  	</div>
-->

						  	
						  	<div class="inspector_parameter  " >	
							  	<div class="title">Inline content</div>
							  	<div class="property_container">
							  		<button name="inlineContent" id="editInlineContent_button"> Inline content... </button>
							  	</div>
						  	</div>
						  	
						  	<!--
<div class="inspector_parameter parameter_contentUrl"  >	
							  	<div class="title">Content Scrolling</div>
							  	<div class="property_container"  >
									<label for="scrolling">s</label>
									<select id="scrolling" name="scrolling"  >
											<option value="no">no</option>
											<option value="yes">yes</option>
									</select>
							  	</div>
						  	</div>
-->
						  			

						  	<div class="inspector_parameter  " >	
							  	<div class="title">Title and Description</div>
							  	<div class="property_container">
							  		<button  id="editTitleDescription_button"> Title - Desc... </button>
							  	</div>
						  	</div>
						  	
						  	
						</div><!-- GRAPHIC_PROPERTIES_2D_PANEL  -->


						<div id="EFFECT_PROPERTIES_2D_PANEL"  >
							
							<div class="title" >Shadow Effect</div>

						  	<div class="inspector_parameter  parameter_position_2D" >	
							  	<div class="title">Offset</div>
							  	<div class="property_container">
							  		<label for="shadowOffsetX">x</label>
							  		<input  type="text" name="shadowOffsetX" id="shadowOffsetX"  onclick="this.select();" />
							  	</div>
							  	<div class="property_container">														
							  		<label for="shadowOffsetY">y</label>
							  		<input  type="text" name="shadowOffsetY" id="shadowOffsetY"  onclick="this.select();" />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter" >	
							  	<div class="title">Blur</div>
							  	<div class="property_container">
							  		<label for="shadowBlur">b</label>
							  		<input type="text" name="shadowBlur" id="shadowBlur"  onclick="this.select();"  />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter" >	
							  	<div class="title">Color</div>
							  	<div class="property_container">
						  			<div class="color_container">
						  				<div id="shadowColor" class="color">
						  				</div>
						  			</div>
							  	</div>
						  	</div>
						  	
						  
						</div> <!-- EFFECT_PROPERTIES_2D_PANEL  -->
						
						
						
						<div id="ANIMATION_PROPERTIES_2D_PANEL"  >
							
							<div class="title" >Animation</div>
							
							
							<div class="animationPreview" style="width:100%; height:100px; position:relative; border:1px solid gray;  background-color:black; margin-bottom:10px; overflow:hidden; ">
								<div id="ANIMATED_ELEMENT" style="position:absolute; color:white; font-size:16px; width:62px; height:18px; left:50%; margin-left:-31px; top:50%; margin-top:-9px;" >Animate</div>
							</div>
							
							<div class="inspector_parameter parameter_instanceId" >	
							  	<div class="title">Animation name</div>
							  	<div class="property_container">
							  		<label for="animationName">n</label>
							  		<!-- <input type="text" name="name" id="name"  onclick="this.select();"  /> -->
							  		<select id="ANIMATION_LIST" name="animationName" id="animationName">
							  		<optgroup label="Utils">
							          <option value="hide">hide</option>
							        </optgroup>
							        
							        <optgroup label="Attention Seekers">
							          <option value="bounce">bounce</option>
							          <option value="flash">flash</option>
							          <option value="pulse">pulse</option>
							          <option value="rubberBand">rubberBand</option>
							          <option value="shake">shake</option>
							          <option value="swing">swing</option>
							          <option value="tada">tada</option>
							          <option value="wobble">wobble</option>
							        </optgroup>
							
							        <optgroup label="Bouncing Entrances">
							          <option value="bounceIn">bounceIn</option>
							          <option value="bounceInDown">bounceInDown</option>
							          <option value="bounceInLeft">bounceInLeft</option>
							          <option value="bounceInRight">bounceInRight</option>
							          <option value="bounceInUp">bounceInUp</option>
							        </optgroup>
							
							        <optgroup label="Bouncing Exits">
							          <option value="bounceOut">bounceOut</option>
							          <option value="bounceOutDown">bounceOutDown</option>
							          <option value="bounceOutLeft">bounceOutLeft</option>
							          <option value="bounceOutRight">bounceOutRight</option>
							          <option value="bounceOutUp">bounceOutUp</option>
							        </optgroup>
							
							        <optgroup label="Fading Entrances">
							          <option value="fadeIn">fadeIn</option>
							          <option value="fadeInDown">fadeInDown</option>
							          <option value="fadeInDownBig">fadeInDownBig</option>
							          <option value="fadeInLeft">fadeInLeft</option>
							          <option value="fadeInLeftBig">fadeInLeftBig</option>
							          <option value="fadeInRight">fadeInRight</option>
							          <option value="fadeInRightBig">fadeInRightBig</option>
							          <option value="fadeInUp">fadeInUp</option>
							          <option value="fadeInUpBig">fadeInUpBig</option>
							        </optgroup>
							
							        <optgroup label="Fading Exits">
							          <option value="fadeOut">fadeOut</option>
							          <option value="fadeOutDown">fadeOutDown</option>
							          <option value="fadeOutDownBig">fadeOutDownBig</option>
							          <option value="fadeOutLeft">fadeOutLeft</option>
							          <option value="fadeOutLeftBig">fadeOutLeftBig</option>
							          <option value="fadeOutRight">fadeOutRight</option>
							          <option value="fadeOutRightBig">fadeOutRightBig</option>
							          <option value="fadeOutUp">fadeOutUp</option>
							          <option value="fadeOutUpBig">fadeOutUpBig</option>
							        </optgroup>
							
							        <optgroup label="Flippers">
							          <option value="flip">flip</option>
							          <option value="flipInX">flipInX</option>
							          <option value="flipInY">flipInY</option>
							          <option value="flipOutX">flipOutX</option>
							          <option value="flipOutY">flipOutY</option>
							        </optgroup>
							
							        <optgroup label="Lightspeed">
							          <option value="lightSpeedIn">lightSpeedIn</option>
							          <option value="lightSpeedOut">lightSpeedOut</option>
							        </optgroup>
							
							        <optgroup label="Rotating Entrances">
							          <option value="rotateIn">rotateIn</option>
							          <option value="rotateInDownLeft">rotateInDownLeft</option>
							          <option value="rotateInDownRight">rotateInDownRight</option>
							          <option value="rotateInUpLeft">rotateInUpLeft</option>
							          <option value="rotateInUpRight">rotateInUpRight</option>
							        </optgroup>
							
							        <optgroup label="Rotating Exits">
							          <option value="rotateOut">rotateOut</option>
							          <option value="rotateOutDownLeft">rotateOutDownLeft</option>
							          <option value="rotateOutDownRight">rotateOutDownRight</option>
							          <option value="rotateOutUpLeft">rotateOutUpLeft</option>
							          <option value="rotateOutUpRight">rotateOutUpRight</option>
							        </optgroup>
							
							        <optgroup label="Sliders">
							          <option value="slideInDown">slideInDown</option>
							          <option value="slideInLeft">slideInLeft</option>
							          <option value="slideInRight">slideInRight</option>
							          <option value="slideOutLeft">slideOutLeft</option>
							          <option value="slideOutRight">slideOutRight</option>
							          <option value="slideOutUp">slideOutUp</option>
							        </optgroup>
							
							        <optgroup label="Specials">
							          <option value="hinge">hinge</option>
							          <option value="rollIn">rollIn</option>
							          <option value="rollOut">rollOut</option>
							        </optgroup>
							      </select>
							  	</div>
						  	</div>
						  			
						  	<div class="inspector_parameter parameter_tweenBegin" >	
							  	<div class="title">Delay</div>
							  	<div class="property_container">
							  		<label for="delay">b</label>
							  		<input type="text" name="delay" id="delay"  onclick="this.select();"  />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter parameter_tweenDur" >	
							  	<div class="title">Duration</div>
							  	<div class="property_container">
							  		<label for="duration">d</label>
							  		<input type="text" name="duration" id="duration"  onclick="this.select();"  />
							  	</div>
						  	</div>
						  	
						  	<div class="inspector_parameter parameter_tweenRepeatCount" >	
							  	<div class="title">Repeat count</div>
							  	<div class="property_container">
							  		<label for="iterationCount">n</label>
							  		<input type="text" name="iterationCount" id="iterationCount"  onclick="this.select();"  />
							  	</div>
						  	</div>
						  	
						  	
						  	
						  	<div class="inspector_parameter "  >	
							  	<div class="title">Fill mode</div>
							  	<div class="property_container"  >
									<label for="fillMode">f</label>
									<select id="fillMode" name="fillMode"  >
											<option value="none">none</option>
											<option value="forwards">forwards</option>
											<option value="backwards">backwards</option>
											<option value="both">both</option>
									</select>
							  	</div>
						  	</div>
						  	
						  	
						  	<div class="inspector_parameter "  >	
							  	<div class="title">Notice</div>
							  	<div class="property_container" style="padding:0 10px 0 10px;"  >
									To use these animations<br/>you have to include<br/>the animate.css file<br/>in your page.
									<br/><br/>
									info at:<br/><a href="http://daneden.github.io/animate.css/" target="_blank">daneden animate.css</a> 
									<br/><br/>
									download:<br/><a href="https://googledrive.com/host/0BwRlR3z6e0egZWdfSmJqSGZKZnM/animate.min.css" target="_blank">animate.min.css</a>
									<br/>
									
							  	</div>
						  	</div>
						  	
						  
						</div> <!-- ANIMATION_PROPERTIES_2D_PANEL  -->
						
						
						

						<div id="FILL_STROKE_PROPERTIES_PANEL"  style="display:none;"  >
						  		<div id="FILL_STROKE_PROPERTIES">
									<div id="STROKE_BUTTON" ><div></div></div>				
									<div id="FILL_BUTTON" ></div>
								</div>
								<div id="SWAP_FILL_STROKE_BUTTON" ></div>
								<div id="FILL_STROKE_TYPES">									
									<div id="DIFFUSE_BUTTON"  ></div>
									<div id="GRADIENT_BUTTON"  ></div>
									<div id="PATTERN_BUTTON"  ></div>
									<div id="NONE_BUTTON"  ></div>
								</div>
								<div id="FILL_STROKE_ALPHA" >
									<div style="padding:5px;">Opacity</div>
									<div id="ALPHA_SLIDER" class="slider"><div></div></div>
								</div>
						</div><!-- FILL_STROKE_PROPERTIES_PANEL -->
						
		</div><!-- end panel -->


		<ul id="WINDOWS_TOOLBAR" class="footer toolbar hbar" style="display:none;">
			<li id="RESOURCES_WIN_button" ><a></a></li>
			<li id="LAYERS_WIN_button"  ><a></a></li>
			<li id="EFFECTS_WIN_button" ><a></a></li>
			<li id="ANIMATIONS_WIN_button" ><a></a></li>
			<!-- <li id="DRIVE_FILE_BROWSER_WIN_button" ><a></a></li> -->
		</ul>


</div><!-- end column -->


		
		
		
		
		
		
	
			</div> <!-- CONTENT -->
			
				
			
			
			
		
		
		<div id="WINDOWS">
		
			<div id="MESSAGE_WIN" class="win dropShadow message_win closed" >
				<div id="WIN_HEAD" class="head hbar">
					<div id="WIN_TITLE" class="title">Message</div>
				</div>
				<div id="WIN_CLOSE_BUTTON" class="close_button"></div>
				<div id="WIN_COLLAPSE_BUTTON" class="collapse_button"></div>
				<div id="WIN_CONTENT" class="content" >
					<textarea id="WIN_MESSAGE_CONTENT" ></textarea>					
				</div>
				<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
			</div> <!-- MESSAGE_WIN -->
			
			
			
		<div id="OPEN_DOCUMENT_WIN" class="win openDocument_win dropShadow closed" >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Open document</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button">
			</div>
			<div id="WIN_CONTENT" class="content">
				<div id="FILES_VIEW_HEADER" class="hbar" >
					<div id="DIR_UP_BUTTON"></div>
					<div id="SELECTED_DOCUMENT_TITLE">Select a file</div>
				</div>
				<ul id="FILES_VIEW"></ul>
				<div id="FILES_VIEW_FOOTER" class="hbar" >
					<input id="CANCEL_BUTTON" name="cancel" type="button"  value="     Cancel    " onclick="" />
					<input id="OPEN_BUTTON" name="open" type="button" disabled="disabled"  value="      Open      "/>
				</div>					
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>
		
		
		
		<div id="SAVE_DOCUMENT_WIN" class="win saveDocument_win dropShadow closed" >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Save document file</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button">
			</div>
			<div id="WIN_CONTENT" class="content">
				<div id="FILES_VIEW_HEADER" class="hbar">
					<div id="DIR_UP_BUTTON"></div>
					Save with name: <input id="INPUT_FILE_NAME" name="fileName" type="text"  value="untitled" />
				</div>
				<ul id="FILES_VIEW"></ul>
				<div id="FILES_VIEW_FOOTER" class="hbar" >
					<input id="CREATE_DIR_BUTTON" name="createDir" style="float:left;" type="button"  value="   New folder…  " />
					<input id="CANCEL_BUTTON" name="cancel"  type="button"  value="     Cancel    "  />
					<input id="SAVE_BUTTON" name="save"  type="button"  value="      Save      "/>
				</div>					
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>

			
			
		<div id="INCLUDES_EDIT_WIN" class="win dropShadow includesEdit_win closed"  >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Include CSS and JS files</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button">
			</div>
			<div id="WIN_CONTENT" class="content" >
				<div>
					<div>
						CSS
						<textarea id="CSS_INCLUDES_TEXT_CONTAINER" onblur="window.scroll(0,0);" ></textarea>
					</div>
					<div>
						Javascript
						<textarea id="JS_INCLUDES_TEXT_CONTAINER" onblur="window.scroll(0,0);" ></textarea>																	
					</div>
				</div>
				<input id="SAVE_INCLUDES_BUTTON" type="button" style="position:absolute; bottom:5px; width:100px; right:14px; "  value="Apply" />
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>
		
		
		
		<!--
<div id="INLINE_CONTENT_EDIT_WIN" class="win dropShadow inlineContentEdit_win closed"  >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Inline content</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button">
			</div>
			<div id="WIN_CONTENT" class="content" >
				<textarea id="INLINE_CONTENT_TEXT_CONTAINER" onblur="window.scroll(0,0);" ></textarea>									
				<input id="UPDATE_INLINE_CONTENT_BUTTON" type="button" style="position:absolute; bottom:5px; width:100px; right:14px; "  value="apply" />
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>
-->
		
		<div id="INLINE_CONTENT_EDIT_WIN" class="win dropShadow inlineContentEdit_win closed"  >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Inline content</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button">
			</div>
			<div id="WIN_CONTENT" class="content" >
				<textarea id="INLINE_CONTENT_TEXT_CONTAINER" onblur="window.scroll(0,0);" ></textarea>													
			</div>
			<!-- e fuori dal content perchè codemirror ridimensiona la textarea al 100% del content in altezza -->
			<input id="UPDATE_INLINE_CONTENT_BUTTON" type="button" style="position:absolute; bottom:5px; width:100px; right:20px; "  value="apply" />
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>

				
		<!--

		<div id="TEXTBOX_EDIT_WIN" class="win dropShadow textBoxEdit_win"  >
			<div id="WIN_HEAD" class="head bar">
				<div id="WIN_TITLE" class="title">Edit Text</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button">
			</div>
			<div id="WIN_CONTENT" class="content" >
				<textarea id="TEXT_CONTAINER" onblur="window.scroll(0,0);" ></textarea>									
				<input id="UPDATE_TEXT_BUTTON" type="button" style="position:absolute; bottom:5px; width:100px; right:14px; "  value="apply" />
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>
-->
		
		
		
		<div id="TITLE_DESCRIPTION_EDIT_WIN" class="win dropShadow titleDescriptionEdit_win closed"  >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Title</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button">
			</div>
			<div id="WIN_CONTENT" class="content" >
				<div>
					<div>
						Title
						<textarea id="ELEMENT_TITLE" onblur="window.scroll(0,0);" ></textarea>
					</div>
					<div>
						Description
						<textarea id="ELEMENT_DESCRIPTION" onblur="window.scroll(0,0);" ></textarea>																	
					</div>
				</div>
				<input id="SAVE_TITLE_DESCRIPTION_BUTTON" type="button" style="position:absolute; bottom:5px; width:100px; right:14px; "  value="Apply" />
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>
		
		
		
		<div id="RESOURCES_WIN" class="win dropShadow resources_win closed"  >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Library</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button"></div>
			<div id="WIN_COLLAPSE_BUTTON" class="collapse_button"></div>
			
			<div id="WIN_CONTENT" class="content"  style="display:block !important;">				
					
					<div id="RESOURCES_PANELS" class="radioPanels"  >
						<ul id="FONTS_LIBRARY_PANEL" class="scrollableY darkScrollbar recordList panel activated" ></ul>
						<div id="SYMBOLS_LIBRARY_PANEL" class="panel" >
							<div class="header hbar" >Search <input type="search" name="googlesearch"  placeholder="insert a word"> in openclipart.org</div>
							<ul class="scrollableY darkScrollbar recordList" ></ul>
						</div>	
						<ul id="GRADIENTS_LIBRARY_PANEL" class="scrollableY darkScrollbar recordList panel" ></ul>
						<ul id="PATTERNS_LIBRARY_PANEL" class="scrollableY darkScrollbar recordList panel" ></ul>
					</div>

					
					
					<ul id="TABS_RESOURCES_PANELS" class="footer toolbar hbar" style="overflow:visible;" >
						
						<li id="FONTS_LIBRARY_button" class="selected" >	
							<ul  class="popupMenu dropShadow" style="left:0px; bottom:24px;"  >
								<li libraryType="fontLibrary" category="Sans Serif" title="" >Font Sans Serif</li>
								<li libraryType="fontLibrary" category="Display" title="" >Font Display</li>
								<li libraryType="fontLibrary" category="Serif" title="" >Font Serif</li>
								<li libraryType="fontLibrary" category="Handwriting" title="" >Font Handwriting</li>
								<!-- <li libraryType="fontLibrary" category="Miscellanea" title="" >Font Miscellanea</li> -->
					    	</ul>
							<a></a>
						</li>
						
						<li id="SYMBOLS_LIBRARY_button" >
							<ul class="popupMenu dropShadow" style="left:0px; bottom:24px;  "  >
								<li url="../../DATA/janvas/library2D/symbols/girls/index.html"  title="">Silhouettes</li>
								<li url="../../DATA/janvas/library2D/symbols/animals/animals.html"  title="">Animals</li>
								<li url="../../DATA/janvas/library2D/symbols/user_interface/user_interface.html"  title="">User interface</li>
								<li url="../../DATA/janvas/components/components.html"  title="">Components</li>
								<li url="../../DATA/janvas/library2D/symbols/furniture/furniture.html"  title="">Furniture</li>
								<li url="../../DATA/janvas/library2D/symbols/decorations/decorations.html"  title="">Decorations</li>
								<li url="../../DATA/janvas/library2D/symbols/office/office.html"  title="">Office</li>
<!-- 								<li url="http://openclipart.org/search/json/?query=christmas&page=1&amount=20"  title="">Openclipart.org</li> -->
					    	</ul>
							<a></a>
						</li>
						
						<li id="GRADIENTS_LIBRARY_button" >
							<ul class="popupMenu dropShadow" style="left:0px; bottom:24px;  "  >
								<li url="<?php echo base_url()?>assets/js/ant_tool_js/linears.html"  title="">Linear gradients</li>
<!--
								<li url="http://192.168.1.156/happEdit/newHappyEdit/js/linears.html"  title="">Linear gradients</li>
-->
<!--
		    					<li url="../../DATA/janvas/library2D/gradients/radials.html"  title="">Radial gradients</li>
-->
					    	</ul>
							<a></a>
						</li>
						
						<li id="PATTERNS_LIBRARY_button" >
							<ul  class="popupMenu dropShadow" style="left:0px; bottom:24px;  "  >
								<li url="../../DATA/janvas/library2D/patterns/patterncooler.html"  title="">patterncooler.com</li>
								<li url="../../DATA/janvas/library2D/patterns/subtlepatterns.html"  title="">subtlepatterns.com</li>
					    	</ul>
							<a></a>
						</li>
						
						<!--
<li id="ANIMATION_LIBRARY_button" >
							<a></a>
						</li>
-->
						
					</ul>				
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>

				

		
		
		
		
		
		
		
		
		<div id="EFFECTS_WIN" class="win dropShadow effects_win closed"  >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Effects</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button"></div>
			<div id="WIN_COLLAPSE_BUTTON" class="collapse_button"></div>
			
			<div id="WIN_CONTENT" class="content"  >				
					
					<div id="EFFECTS_2D_PANEL" class="listView scrollableY darkScrollbar" ></div>
					
					<ul id="EFFECTS_EDITING_TOOLBAR" class="footer toolbar hbar" style="overflow:visible;" >
							<li id="REMOVE_EFFECT_button"   title=""><a></a></li>
	        				<li id="MOVE_EFFECT_UP_button"   title=""><a></a></li>
	        				<li id="MOVE_EFFECT_DOWN_button"   title=""><a></a></li>
					</ul>				
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>
		
		
		
		<div id="ANIMATIONS_WIN" class="win dropShadow animations_win closed"  >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Animation</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button"></div>
			<div id="WIN_COLLAPSE_BUTTON" class="collapse_button"></div>
			
			<div id="WIN_CONTENT" class="content"  >				
					
					<div id="ANIMATIONS_2D_PANEL" class="listView scrollableY darkScrollbar" ></div>
					
					<ul id="ANIMATIONS_EDITING_TOOLBAR" class="footer toolbar hbar" style="overflow:visible;" >
							<li id="CREATE_ANIMATION_button"   title=""><a></a></li>
							<li id="REMOVE_ANIMATION_button"   title=""><a></a></li>
					</ul>				
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>
		
		
		
		
		<div id="DRIVE_FILE_BROWSER_WIN" class="win dropShadow driveFileBrowser_win closed"  >
			<div id="WIN_HEAD" class="head hbar">
				<div id="WIN_TITLE" class="title">Google Drive</div>
			</div>
			<div id="WIN_CLOSE_BUTTON" class="close_button"></div>
			<div id="WIN_COLLAPSE_BUTTON" class="collapse_button"></div>
			
			<div id="WIN_CONTENT" class="content"  >				
					
					<div id="DRIVE_FILE_BROWSER_PANEL" onActivate="openGoogleDrive" class="treeView scrollableY darkScrollbar" ></div>
					
					<ul id="DRIVE_FILE_BROWSER_TOOLBAR" class="footer toolbar hbar" style="overflow:visible;" >
							
							<li id="NEW_FOLDER_button"   title="New folder"><a></a></li>
					        <li id="NEW_SHARED_WEB_FOLDER_button"   title="New shared folder"><a></a></li>
							<li id="OPEN_AS_WEB_PAGE_button" title="Open with Browser" ><a></a></li>
							<li id="REMOVE_FILE_button"   title="Remove file"><a></a></li>
							<li id="RENAME_FILE_button"   title="Rename file"><a></a></li>
					        <li id="DOWNLOAD_FILE_button" title="Download file"><a></a></li>
					        <li id="UPLOAD_FILE_button"   title="Upload file"><a></a><input id="UPLOAD_FILE_TEXTFIELD" type="file" style="position:absolute; display: block; left:0; top:0; opacity:0; width:100%; cursor:pointer;" /></li>
<!--
							<li id="EDIT_button" >
								<ul  class="popupMenu dropShadow" style="right:0; bottom:0; "   >
					        		<li  id="CREATE_FILE_JS_menuItem"  class="js_icon16x16" style="background-position-x: 8px;" fileExtension="js"   fileMimeType="text/javascript" title="">New file - JS</li>
					        		<li  id="CREATE_FILE_CSS_menuItem" class="css_icon16x16" style="background-position-x: 8px;" fileExtension="css"  fileMimeType="text/css" title="">New file - CSS</li>
					        		<li  id="CREATE_FILE_HTML_menuItem" class="html_icon16x16" style="background-position-x: 8px;" fileExtension="html" fileMimeType="text/html" title="">New file - HTML</li>
					        		<li class="divisor"></li>
					        		<li  id="NOTIFICATION_SERVER_menuItem"   title="">Notification Server...</li>
					        		<li  id="SEND_NOTIFICATION_menuItem"   title="">Send Notification</li>					        		
					        	</ul>
					        	<a></a>
							</li>
-->							
							<li id="LOADING_ICON" ><a></a></li>
					</ul>				
			</div>
			<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
		</div>
		
		
			<div id="SAVE_PROGRESS_WIN" class="win dropShadow saveProgress_win closed"  >
				<div id="WIN_HEAD" class="head hbar">
					<div id="WIN_TITLE" class="title">Save progress</div>
				</div>
				<div id="WIN_CLOSE_BUTTON" class="close_button">
				</div>
				<div id="WIN_CONTENT" class="content" >
					<div>
						<div id="LABEL" >Document saving...</div>
						<progress id="BAR"  max="100" ></progress>
					</div>
				</div>
				<div id="WIN_RESIZE_BUTTON" class="resize_button"></div>
			</div><!-- SAVE_PROGRESS_WIN -->
			
		</div><!-- WINDOWS -->
		
		
		<div id="DIALOGS">	
		</div> <!-- DIALOGS -->
		
	</div>
	
	
	<div id="CONTEXT_MENUS" oncontextmenu="return false;" style="">
	
		
		
		
		<ul id="EDIT_2D_CONTEXT_MENU" class="popupMenu dropShadow" style="display:none;left:200px; top:45px; " >
		        	<li id="FILL_menuItem" >Fill</li>
			        <li id="STROKE_menuItem"  >Stroke</li>
			        <li id="STROKE_WIDTH_menuItem"  >Stroke width</li>
			        <li class="divisor"></li>
			        <li id="DUPLICATE_menuItem"  >Duplicate</li>
	        		<li id="GROUP_menuItem"  >Group</li>
	        		<li id="UNGROUP_menuItem"   >Ungroup</li>
	        		<li class="divisor"></li>
					<li id="LOCK_SELECTION_menuItem" >Lock selection</li>
					<li id="UNLOCK_ALL_menuItem" >Unlock all</li>
        	</ul>
			
			<ul id="LINE_OPTIONS_CONTEXT_MENU" class="popupMenu dropShadow" style="left:200px; top:45px;" >
			        <li  value="1" id="LINE_WIDTH_menuItem"  >1 pt</li>
			        <li  value="2" id="LINE_WIDTH_menuItem"  >2 pt</li>
					<li  value="3" id="LINE_WIDTH_menuItem" >3 pt</li>
					<li  value="4" id="LINE_WIDTH_menuItem" >4 pt</li>
					<li  value="5" id="LINE_WIDTH_menuItem" >5 pt</li>
	        		<li  value="6" id="LINE_WIDTH_menuItem"  >6 pt</li>
	        		<li  value="7" id="LINE_WIDTH_menuItem"  >7 pt</li>
	        		<li  value="8" id="LINE_WIDTH_menuItem"   >8 pt</li>
	        		<li class="divisor"></li>
			        <li  id="LINE_CAP_BUTT_menuItem"  >Line cap Butt</li>
			        <li  id="LINE_CAP_ROUND_menuItem"  >Line cap Round</li>
					<li  id="LINE_CAP_SQUARE_menuItem" >Line cap Square</li>
					<li  id="LINE_JOIN_MITER_menuItem" >Line join Square</li>
					<li  id="LINE_JOIN_ROUND_menuItem" >Line join Round</li>
	        		<li  id="LINE_JOIN_BEVEL_menuItem"  >Line join Bevel</li>		        
	        </ul>
	        
	        <div id="GRADIENT_EDITOR" class="dropShadow">
				<div id="GRADIENT_ALPHA_BACKGROUND" >	
					<div id="GRADIENT" >
						<div id="GRADIENT_POINT" style="left: 0px;" ><div id="COLOR"></div></div>
						<div id="GRADIENT_POINT" style="left: 50px;" ><div id="COLOR"></div></div>
						<div id="GRADIENT_POINT" style="left: 150px;" ><div id="COLOR"></div></div>
					</div>
				</div>
				<select id="GRADIENT_TYPE"   >
						<option  value="linear">Linear</option>
						<option  value="radial">Radial</option>
				</select>
			</div>
		
			
<!--
			
-->
		<div id="COLOR_PICKER" class="dropShadow">	
				<input id="HEX_COLOR_INPUT" type="text" onblur="window.scrollTo(0, 0);" onclick="select();" />
				<div id="ALPHA_BACKGROUND" >					
					<div id="ALPHA_PANEL" >
						<div id="ALPHA_POINT" ></div>
					</div>
				</div>
				<div id="HUE_PANEL" >
					<div id="HUE_POINT" ></div>
				</div>
				<div id="SATURATION_COLOR" >
					<div id="SATURATION_PANEL" >
						<div id="SATURATION_POINT" ></div>
					</div>
				</div>
			</div>
		
	</div> <!-- CONTEXT_MENUS -->
	
	
	<div id="AUTH_REQUEST_PANEL" class="authRequestPanel dropShadow hbar" style="display:none;">
			<div class="google_drive_logo" ></div>
			<div class="info" >
			Google Drive<br/>
			Authorization<br/>
			required
			</div>
			<button id="authorize_button" >Authorize</button>
			<div id="close_button" ></div>
		</div>
		
		
		
  <div id="ABOUT_PANEL" class="" >
			<!-- <div class="versionInfo" >version 3.0.0</div> -->
			<div class="logo" style="" ></div>
			<div class="copyrightInfo" >
				<!-- Copyright © XOSystem - All rights reserved - <a href="http://www.janvas.com" target="_blank" >janvas.com</a> -->
			</div>
	</div>
	
	
	<div id="ADVERTISING_PREMIUM_PANEL" class="dialogPanel dropShadow  hbar" >			
			<div class="logo" ></div>
			<!--
<div class="info" >
				Your FREE account is LIMITED<br/>
				to 10 saves<br/>
				in a time range of 30 days.
			</div>
-->
			<div id="ADVERTISING_PREMIUM_LINK" ></div>
			<div id="ACCOUNT_FREE_STATUS" class="account_free_status" ></div>
			<div id="close_button" ></div>
	</div>
	

<div id="dialog" title="Confirmation box">
		<p>Are you want to download the file?</p>
		<a href="#" id="programaticall"  download="designData.html" style="padding-left: 120px;">Yes</a>
	</div>
<!-- custom scrollbar plugin -->
	<script>
		(function($){
			$(window).load(function(){
				$(".all_tool_section").mCustomScrollbar({
					scrollButtons:{
						enable:true
					},
					callbacks:{
						onScrollStart:function(){ myCallback(this,"#onScrollStart") },
						onScroll:function(){ myCallback(this,"#onScroll") },
						onTotalScroll:function(){ myCallback(this,"#onTotalScroll") },
						onTotalScrollOffset:60,
						onTotalScrollBack:function(){ myCallback(this,"#onTotalScrollBack") },
						onTotalScrollBackOffset:50,
						whileScrolling:function(){ 
							myCallback(this,"#whileScrolling"); 
							$("#mcs-top").text(this.mcs.top);
							$("#mcs-dragger-top").text(this.mcs.draggerTop);
							$("#mcs-top-pct").text(this.mcs.topPct+"%");
							$("#mcs-direction").text(this.mcs.direction);
							$("#mcs-total-scroll-offset").text("60");
							$("#mcs-total-scroll-back-offset").text("50");
						},
						alwaysTriggerOffsets:false
					}
				});
				
				function myCallback(el,id){
					if($(id).css("opacity")<1){return;}
					var span=$(id).find("span");
					clearTimeout(timeout);
					span.addClass("on");
					var timeout=setTimeout(function(){span.removeClass("on")},350);
				}
				
				$(".all_tool_section a").click(function(e){
					e.preventDefault();
					$(this).parent().toggleClass("off");
					if($(e.target).parent().attr("id")==="alwaysTriggerOffsets"){
						var opts=$(".all_tool_section").data("mCS").opt;
						if(opts.callbacks.alwaysTriggerOffsets){
							opts.callbacks.alwaysTriggerOffsets=false;
						}else{
							opts.callbacks.alwaysTriggerOffsets=true;
						}
					}
				});
			});
		})(jQuery);
		
		/*$("#FILL_BUTTON").click(function()
		{
			$("#COLOR_PICKER").toggle();
		});*/
	$(document).ready(function()
	{
		
		
						
		$(".mCS_img_loaded").mouseover(function()
		   {
			$(".logo_section").find('a').css("color","orange");
		   }).mouseout(function()
		   {
			$(".logo_section").find('a').css("color","#fff");
			});
			$("#FONTS_LIBRARY_PANEL").find('img').hide();
		   var userIds=$("#userId").val();
	$.ajax({type:"GET",url:base_url+"script/saveDesign/getImages.php",data:"userIds="+userIds,success:function(mydata){ $(".pages_wrapper").find("ul").append(mydata);}});
		   /*$("#CONTENT").click(function()
		   {
			  // alert("remove classes");
			   $("#COLOR_PICKER").removeClass('activated');
			   });*/
	});
function showDocMenu(vals)
{
	if(vals=="on")
	{
	$("#docMenus").show();	
	}
	if(vals=="off")
	{
	$("#docMenus").hide();	
	}
	
	$("#docMenus").mouseover(function()
	{
		$(this).show();
		}).mouseout(function(){$(this).hide()});
}

function showEditMenu(vals1)
{
	if(vals1=="on")
	{
	$("#editMenus").show();	
	}
	if(vals1=="off")
	{
	$("#editMenus").hide();	
	}
	
	//$("#editMenus").show();
	$("#editMenus").mouseover(function()
	{
		$(this).show();
		}).mouseout(function(){$(this).hide()});
}
function showFontTools(vals2)
{
	
	if(vals2=="on")
	{
	$("#fontMenus").show();	
	}
	if(vals2=="off")
	{
	$("#fontMenus").hide();	
	}
	
	//$("#fontMenus").show();
	$("#fontMenus").mouseover(function()
	{
		$(this).show();
		}).mouseout(function(){$(this).hide()});
}

function showBlocks(tools)
{	
	$("#blocks").show();
	$("#blocks").draggable();
	switch(tools)
	{
		case "fonts":$(".gradContent").hide();$(".fontContent").show();	
						var ab=new Array();
						var a=$("#FONTS_LIBRARY_PANEL").find("li").length;
						//alert(a);
						for(var i=0;i<a;i++)
						{
						  ab.push($("#FONTS_LIBRARY_PANEL").find("a").eq(i).text());
						}
						for(var i=0;i<ab.length;i++){
						$("#FONTS_LIBRARY_PANEL").find("a").eq(i).css("font-family",ab[i]);
						}
						$("#FONTS_LIBRARY_PANEL").find("button").html("<i class=\"fa fa-get-pocket\"></i>").css("color","#008000");
						break;
		
		case "gradtools":$(".fontContent").hide();$(".gradContent").show();
						break;
	}
}
function closeToolBlocks()
{
	$("#blocks").hide();
}
$(window).load(function()
{
	$(".parameter_font").hide();
	$(".RemoveBlends").hide();
	$(".placeHtml").hide();
	$("#SELECTION_ACTIONS_2D_toolButton").css("background","none");
	$("#SAVE_toolButton").css("background","none");
	localStorage.clear();
});
</script>

	
	
</body>

</html>

