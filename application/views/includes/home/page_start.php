<!DOCTYPE HTML>
<html lang="en" ng-app="routerApp">
<head>
<meta charset="utf-8">
<title><?php echo $title;?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Styles -->
<link rel="shortcut icon" href="<?php echo base_url()?>img/Fevicon.png" />
<link href="<?php echo base_url()?>css/ant_home_css/font-awesome.css" rel="stylesheet">
<!-- <link href="<?php echo base_url()?>css/ant_home_css/css/font-awesome.min.css" rel="stylesheet"> -->
<!-- <link href="<?php echo base_url()?>css/ant_home_css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url()?>css/ant_home_css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>css/ant_home_css/bootstrap-theme.css" rel="stylesheet">
<link href="<?php echo base_url()?>css/ant_home_css/bootstrap-theme.min.css" rel="stylesheet"> -->
<link rel='stylesheet' href='<?php echo base_url()?>js/ant_user_dashboard/bower_components/angular-loading-bar/build/loading-bar.min.css' type='text/css' media='all' />
    <!-- Angular Loader -->
    <link href="<?php echo base_url()?>js/ant_user_dashboard/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>js/ant_user_dashboard/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <!--     <link href="../bower_components/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" /> -->
    <link href="<?php echo base_url()?>css/ant_user_dashboard/material-icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>js/ant_user_dashboard/bower_components/animate.css/animate.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>css/ant_home_css/style.css" rel="stylesheet">
<link href="<?php echo base_url()?>css/ant_home_css/media.css" rel="stylesheet">
<link href="<?php echo base_url()?>css/ant_home_css/fonts/font.css" rel="stylesheet">

<!--:::::::::::::::Reponsive Menu:::::::::::::::-->
<!--<script type="text/javascript" src="<?php echo base_url()?>js/ant_home_js/jquery.min.js"></script>-->

</head> 
<body id="top">

	<div class="page-topbar"><!-- START TOPBAR -->
	<div class="logo-area ">
	  <a href="http://happyedit.com">  <img src="<?php echo base_url()?>img/logo.png" style="padding: 0px; height: 40px;"> </a>
	</div>
<div class="quick-area ">
    
    <div class="pull-left pagetitle">
        <!--<span class="line"></span>-->
        <h1 class="ng-binding"></h1>
        <!--<div></div>-->
    </div>

    <!--<div class="col-md-3 menu">
			<a href="#"></a>
			<a href="#">Create a Site</a>
		</div>-->
		<div class="col-md-2 pull-right lg">
			<!-- <a class="" data-toggle="modal" data-target="#GSCCModal">Sign In</a> -->
	    	<!-- <a href="<?php echo base_url()?>index.php/login">Sign In</a> -->
	    	<?php if ($this->session->userdata('user_id') == '') {?>
	    	<a onclick="window.location = 'http://<?php echo HOME_URL;?>/#/login' ">Sign In</a>
	    	<?php }else{?>
	    	<a onclick="dashboardUrl('<?php echo $this->session->userdata('username');?>')">Dashboard</a>
	    	<!-- <a href="<?php echo $this->session->userdata('username').".".HOME_URL."/index.php/dashboard";?>">Dashboard</a> -->
	    	<?php }?>

		</div>
    
    
    
</div>
<!-- END TOPBAR -->
</div>