<!DOCTYPE html>
<html lang="en" ng-app="app" class="no-js {{app.settings.layoutBoxed ? 'boxed' : ''}}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;?></title>
    
    <!-- Favicon -->
    <!-- CSS FRAMEWORK - START -->
    <link rel="shortcut icon" href="<?php echo base_url()?>img/Fevicon.png" />
    <link rel='stylesheet' href='<?php echo base_url()?>js/ant_user_dashboard/bower_components/angular-loading-bar/build/loading-bar.min.css' type='text/css' media='all' />
    <!-- Angular Loader -->
    <link href="<?php echo base_url()?>js/ant_user_dashboard/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>js/ant_user_dashboard/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <!--     <link href="../bower_components/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" /> -->
    <link href="<?php echo base_url()?>css/ant_user_dashboard/material-icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>js/ant_user_dashboard/bower_components/animate.css/animate.min.css" rel="stylesheet" type="text/css" />
    <!-- CSS FRAMEWORK - END -->

    <link rel="stylesheet" href="<?php echo base_url()?>css/ant_user_dashboard/app.css">
    <!--
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/app.style2.css">
    <link rel="stylesheet" href="css/app.style3.css">
    <link rel="stylesheet" href="css/app.style4.css"> 
    -->
</head>
<body ng-controller="AppCtrl" class="{{app.settings.layoutBoxed ? 'boxed' : ''}}" >