 <!-- JS FRAMEWORK - START -->
    <script src="<?php echo base_url()?>js/ant_user_dashboard/bower_components/jquery/dist/jquery.min.js" type="text/javascript"></script>
    <!-- Angular Scripts -->
    <script src="<?php echo base_url()?>js/ant_user_dashboard/bower_components/angular/angular.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/bower_components/angular-animate/angular-animate.js"></script>
    <!--    <script src="../bower_components/angular-cookies/angular-cookies.js"></script>-->
    <!--    <script src="../bower_components/angular-resource/angular-resource.js"></script>-->
        <script src="<?php echo base_url()?>js/ant_user_dashboard/bower_components/angular-sanitize/angular-sanitize.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/bower_components/angular-touch/angular-touch.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/bower_components/angular-ui-router/release/angular-ui-router.js"></script>
    <!--<script src="../bower_components/ngstorage/ngStorage.js"></script>-->
    <script src="<?php echo base_url()?>js/ant_user_dashboard/bower_components/angular-ui-utils/ui-utils.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
    <!-- Angular ui bootstrap -->
    <script src="<?php echo base_url()?>js/ant_user_dashboard/bower_components/oclazyload/dist/ocLazyLoad.js"></script>
    <!-- lazyload -->
    <script type='text/javascript' src='<?php echo base_url()?>js/ant_user_dashboard/bower_components/angular-loading-bar/build/loading-bar.min.js'></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/bower_components/perfect-scrollbar/min/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/bower_components/angular-perfect-scrollbar/src/angular-perfect-scrollbar.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/bower_components/angular-inview/angular-inview.js" type="text/javascript"></script>
    <!-- JS FRAMEWORK - END -->

    <!-- App JS - Start -->
    <script src="<?php echo base_url()?>js/ant_user_dashboard/app.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/app.config.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/app.lazyload.js"></script>

    <script src="<?php echo base_url()?>js/ant_user_dashboard/app.router.js"></script>
    <!--
    <script src="js/app.router.js"></script>
    <script src="js/app.router.hospital.js"></script>
    <script src="js/app.router.university.js"></script>
    <script src="js/app.router.music.js"></script>
    -->



    <script src="<?php echo base_url()?>js/ant_user_dashboard/app.main.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/checklist-model.js"></script>
    <!-- App JS - End -->

    <!-- App JS Utilities - Start -->
    <script src="<?php echo base_url()?>js/ant_user_dashboard/services/ui-load.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/filters/moment-fromNow.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/directives/nganimate.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/directives/ui-jq.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/directives/ui-module.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/directives/ui-nav.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/directives/ui-bootstrap.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/directives/ui-chatwindow.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/directives/ui-sectionbox.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/controllers/bootstrap.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/controllers/topbar.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/controllers/chat.js"></script>
    <script src='<?php echo base_url()?>js/ant_user_dashboard/bower_components/Chart.js/Chart.min.js'></script>
    <script src='<?php echo base_url()?>js/ant_home_js/bootstrap.min.js'></script>

</body>

</html>
