<style type="text/css">
.modal-backdrop{
	display: none;
}
</style>
<div class="wrapper">
	    <header class="top_header">
	    	<div class="full-width-video__element">
	    		<video preload="auto" poster="//microlancer.lancerassets.com/v2/services/ec/cf7ba0a2ad11e4a0d19fbd01f431c7/medium_video_thumbnail_studio-bg.jpg" muted="" loop="" class="full-width-video__player" autoplay="">
	    			<source type="video/webm" src=""></source>
	    			<source type="video/mp4" src="http://microlancer.lancerassets.com/v2/services/cc/6bdf10ab3711e493e6e9f43d9f67d3/medium_video_studio.mp4"></source>
	    		</video>
	    	</div>

	    	<div class="container">
	    		<div class="row">
	    			<div class="col-md-2 logo"><a href="#">Happy Edit</a></div>
	    			<div class="col-md-8 menu">
	    				<a href="#">Browse Categories </a>
	    				<a href="#">How it works</a>
	    			</div>
	    			<div class="col-md-2 pull-right lg">
	    				<a href="<?php echo base_url()?>index.php/login">Sign In</a>
	    				<!-- <button class="btn btn-primary" data-toggle="modal" data-target="#GSCCModal">Login</button> -->
	    			</div>
	    		</div>
	    		<div id="GSCCModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				 <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
				        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
				      </div>
				      <div class="modal-body">
				        ...
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary">Save changes</button>
				      </div>
				    </div>
				  </div>
				</div>
	       
				<div class="header_content">
		        	<div class="play"><a href="#"><i class="fa fa-play-circle"></i></a></div>
		        	<div class="hand_text">Hand-picked designers and <br> developers ready for your project.</div>
		        	<div class="ser_section">
		        		<div class="large-search" am-grid-row="l:start" am-grid-col="l:pre1 l:10">
		        			<input type="text" placeholder="Find a service: eg. 'Logo Design'" name="search[query]" id="search_query" class="large-search__body" vk_16c85="subscribed">
		        			<button type="submit" class="button button--green large-search__button">
		        				<span class="large-search__button__label">Search</span>
		        			</button>
		        		</div>
		        	</div>
		        	<div class="links_wrapper">
		        		<p class="lh-flexed--spaced s-no-below th-size-medium">
	        				<a href="#">Install WordPress Theme</a>
	        				<a href="#">Flyer Design</a>
	        				<a href="#">App Development</a>
	        				<a href="#">Business Cards</a>
	        			</p>
		        	</div>
		        </div>

	         </div>

	        <div class="clear"></div>
	    </header>    

	    <div class="fixed_wrapper_homepage">
	    	<div class="container">
	    		<ul class="lh-flexed--spaced">
	    			<li class="lh-inline-block"><a href="#">Logo Design &amp; Branding</a></li>
	    			<li class="lh-inline-block"><a href="#">WordPress</a></li>
    				<li class="lh-inline-block"><a href="#">Design &amp; Graphics</a></li>
    				<li class="lh-inline-block"><a href="#">Websites &amp; Programming</a></li>
    				<li class="lh-inline-block"><a href="#">Business &amp; Marketing</a></li>
    				<li class="lh-inline-block"><a href="#">Video &amp; Animation</a></li>
    				<li class="lh-inline-block"><a href="#">Voice-overs</a></li>
	    		</ul>
	    	</div>
	    </div>

	    <section class="cata_wrapper">
	    	<div class="container">
		    	<h3 class="cata_heading">Popular Categories</h3>
		    	<div class="cata_all">
					<div class="row">
						<div class="col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
						<div class="col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img2.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
						<div class="col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img3.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
						<div class="col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img4.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
						<div class="col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
						<div class="col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img2.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
						<div class="col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img3.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
						<div class="col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img4.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
					</div>
	    		</div>
	    	</div>	
	    </section>

	    <section class="pro_wrapper">
	    	<div class="container">
		    	<h3 class="cata_heading">Popular Categories</h3>
			    <div class="cata_all">
			    	<div class="row">
			    		<div class="col-md-12 pr">
			    			<img src="<?php echo base_url()?>img/ant_home_img/pro.jpg">
			    			<div class="popular_lt_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</div>
			    			<div class="popular_rt_text">
			    				<span><a href=""><i class="fa fa-play-circle"></i></a></span>
			    				<span>Lorem Ipsum is simply dummy text</span>
			    			</div>
			    		</div>
			    	</div>
			    </div>
			</div>    
	    </section>

	    <section class="pur_wrapper">
	    	<div class="container">
	    		<div class="row">
	    			<div class="col-md-6 tw">
	    				<div class="lt_img"><img src="<?php echo base_url()?>img/ant_home_img/1.png"></div>
	    				<div class="rt_text">Lorem Ipsum is simply dummy text of the printing and type setting industry. </div>
	    			</div>
	    			<div class="col-md-6 tw">
	    				<div class="lt_img"><img src="<?php echo base_url()?>img/ant_home_img/2.png"></div>
	    				<div class="rt_text">Lorem Ipsum is simply dummy text of the printing and type setting industry. </div>
	    			</div>
	    		</div>
	    	</div>
	    </section>

	    <section class="easy_wrapper">
	    	<div class="container">
				<div class="easy_header">
					<span>Lorem Ipsum is simply dummy</span> text of the printing and typesetting
				</div>
				<div class="row">
					<div class="slider">
						<div id="owl-example" class="owl-carousel">
			                <div class="item">
			                	<div class="overaly"><p>Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing and typesetting lorum.</p></div>
			                	<img src="<?php echo base_url()?>img/ant_home_img/pic1.jpg" alt=""></div>
			                <div class="item">
			                	<div class="overaly"><p>Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing and typesetting lorum.</p></div>
			                	<img src="<?php echo base_url()?>img/ant_home_img/pic2.jpg" alt=""></div>
			                <div class="item">
			                	<div class="overaly"><p>Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing and typesetting lorum.</p></div>
			                	<img src="<?php echo base_url()?>img/ant_home_img/pic3.jpg" alt=""></div>
			                <div class="item">
			                	<div class="overaly"><p>Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing and typesetting lorum.</p></div>
			                	<img src="<?php echo base_url()?>img/ant_home_img/pic4.jpg" alt="">
			                </div>
			                <div class="item">
			                	<div class="overaly"><p>Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing and typesetting lorum.</p></div>
			                	<img src="<?php echo base_url()?>img/ant_home_img/pic5.jpg" alt=""></div>
		              	</div>
				    </div>
				</div>    
			</div>		    	
		</section>

		<section class="work_wrapper">
			<div class="container bg">
				<div class="row">
					<div class="col-md-10 wk">
						<h2>Lorem Ipsum is simply dummy</h2>
						<h3>text of the printing <br>and typesetting text of <br>the printing and type setting text</h3>
					</div>
					<div class="col-md-2 rt_oic">
						<img src="<?php echo base_url()?>img/ant_home_img/pic6.jpg" alt="">
						<p>and typesetting text of the printing and type setting text</p>
					</div>
				</div>	
			</div>
		</section>

		<section class="easy_wrapper padbt">
			<div class="container">
				<div class="large-search_bor ">
					<input type="text" vk_16c85="subscribed" class="large-search__body" id="search_query" name="search[query]" placeholder="What job can we help you with?">
					<button class="button button--green large-search__button" type="submit">
						<span class="large-search__button__label">Search</span>
					</button>
				</div>
			</div>
		</section>

		<section class="bg_img_full">
			<div class="container">
				<div class="row">
					<div class="col-md-9 wk">
						<h2>Lorem Ipsum is simply dummy</h2>
						<h3>text of the printing <br>and typesetting text of <br>the printing and typesettingtext</h3>
					</div>
					<div class="col-md3 rt_oic">
						<a href="#">Happy Edit</a>
					</div>
				</div>
			</div>
		</section>

		<footer class="footer_wrapper">
			<div class="container">
				<ul class="footer-list col-md-3">
					<li class="footer-list_label">About Studio</li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">How It Works</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">FAQ</a></li>
					<li class="footer-list_item"><a href="#" rel="nofollow">Contact Us</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">About Us</a></li>
					<li class="footer-list_item"><a href="#">Testimonials</a></li>
					<li class="footer-list_item"><a href="#">Affiliates</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">Blog</a></li>
					<li class="footer-list_item"><a href="#">Meetups</a></li>
					<li class="footer-list_item"><a href="#">Terms &amp; Conditions</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">Content Policy</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">Privacy Policy</a></li>
				</ul>
				<ul class="footer-list col-md-3">
					<li class="footer-list_label">Categories</li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">How It Works</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">FAQ</a></li>
					<li class="footer-list_item"><a href="#" rel="nofollow">Contact Us</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">About Us</a></li>
					<li class="footer-list_item"><a href="#">Testimonials</a></li>
					<li class="footer-list_item"><a href="#">Affiliates</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">Blog</a></li>
					<li class="footer-list_item"><a href="#">Meetups</a></li>
					<li class="footer-list_item"><a href="#">Terms &amp; Conditions</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">Content Policy</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">Privacy Policy</a></li>
				</ul>

				<ul class="footer-list col-md-6">
					<li class="footer-list_label">Other Sites
					<li class="footer-list_item mr15">
						<span class="footer_img"><a href=""><img src="<?php echo base_url()?>img/ant_home_img/pic8.jpg" alt=""></a></span>
						<span class="footer_text">text of the printing and typesetting text of the printing and type text of the printing and typesetting text of the printing and type text of the printing and printing and  <br><a href="">Learn now</a></span>
					</li>
					<li class="footer-list_item mr15">
						<span class="footer_img"><a href=""><img src="<?php echo base_url()?>img/ant_home_img/pic8.jpg" alt=""></a></span>
						<span class="footer_text">text of the printing and typesetting text of the printing and type text of the printing and type text of the <br><a href="">Learn now</a></span>
					</li>
				</ul>

				<div class="sub_footer">
					<p>Copyright © 2016 Happy Edit. All Rights Reserved</p>
					<p class="pull-right"><a href="#">Happy Edit</a></p>
				</div>

			</div>
		</footer>

	<p id="back-top"><a href="#top"><i class="fa fa-arrow-up"></i></a></p>
</div>



