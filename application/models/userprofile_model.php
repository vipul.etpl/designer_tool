<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userprofile_model extends CI_Model{

	public function get_max_id($table, $id){
        $data = 0;
        $this->db->select_max($id);
        $query = $this->db->get($table);
        $row = $query->row_array();
        if($query->num_rows() > 0){
          $data = $row[$id] + 1;
        }else{
          $data = 1;
        }
        return $data;
    }

 public function getPngImgThroughId($user_id){
    $data = $this->db->get_where('ant_images', array('user_id' => $user_id,'extName'=>'html'));
    //$this->db->order_by("date", "DESC");
    if ($data->num_rows()>0) {
      return $data->result_array();
    }else{
      return 105;
    }
  }
  public function deletemyproject($user_id, $pronames){
      $this->db->query("delete from ant_projects where ant_projects.user_id=".$user_id." and ant_projects.project_name='".$pronames."'");
      $path="saveImg/".base64_encode($this->session->userdata('user_id'))."/".$pronames;
      $this->load->helper("file"); // load the helper
      delete_files($path, true); // delete all files/folders

      // $qry = $this->db->query("SELECT * FROM ant_projects where user_id = '".$this->session->userdata('user_id')."'");
      // if ($qry->num_rows()>0) {
      //   return $qry->result_array();
      // }else{
      //   return 101;
      // }
  }

  public function insertTransaction($transinfo){
      $query = $this->db->insert_batch('ant_payment_record',$transinfo);
      if ($query) {
        // $sendPdfLink = $this->send_pdfLink_email($transinfo);
        // if ($sendPdfLink) {
        //   return true;
        // }else{
        //   return false;
        // }
        return true;
      }else{
        return false;
      }
  }

  public function updateImageStatusValue($product_id){
      $data = array('is_purchase ' => '1');

      $this->db->where('id', $product_id);
      $update = $this->db->update('ant_projects', $data); 
      if ($update) {
        return true;
      }else{
        return false;
      }
  }

   public function updatePdfStatusValue($product_id){
      $data = array('is_pdf_dwnload ' => '1');

      $this->db->where('id', $product_id);
      $update = $this->db->update('ant_images', $data); 
      if ($update) {
        return true;
      }else{
        return false;
      }
  }

  // Send PDF link to payer - Aishvarya_Shrivastava
     public function send_pdfLink_email($transinfo){
        $getPaymetInfoInarr = $this->db->query("SELECT ar.*,apr.*,ai.*,al.* FROM ant_register as ar
        INNER JOIN ant_lgn_details as al
            ON ar.user_id = al.user_id
        INNER JOIN ant_images as ai
            ON ai.user_id = al.user_id
        INNER JOIN ant_payment_record as apr
            ON apr.user_id = ai.user_id where apr.user_id = ".$transinfo['user_id']." and (apr.product_id = ".$transinfo['product_id']." and ai.id = ".$transinfo['product_id'].")");
        if ($getPaymetInfoInarr->num_rows()>0) {
        $result_array = $getPaymetInfoInarr->result_array();
        $enc_user_id = base64_encode($result_array[0]['user_id']);
        $pdfName = '';
        require_once("script/libmail.php");
        //Init. user email
        $mail = $result_array[0]['email'];
        $message = file_get_contents("media/emails/PaymetSucess.html");
        $message = preg_replace('/{user}/', $result_array[0]['first_name'], $message);
        $message = preg_replace('/{user_id}/', $enc_user_id, $message);
        $message = preg_replace('/{pdfName}/', base64_encode($result_array[0]['imgName']), $message);
        //Send email actions;
        $m = new Mail ();
        //Sender email
        $m->From ("admin@happyedit.com");
        //Registration user mail
        $m->To ($mail);
        //Theme
        $m->Subject ("Happy Edit - Payment Successful");
        //Mail content
        $m->Body ($message,"html");
        $m->Send ();
        return true;
        }else{

        }
        
    }

    // Aishvarya - Get Base64 File from DB using P_id
    public function getBase64TxtFileVal($p_id){
      $this->db->select('imgName');
      $fileName = $this->db->get_where('ant_images', array('id' => $p_id));
      if ($fileName->num_rows()>0) {
        return $fileName->result_array();
      }else{
        return 0;
      }
    }
    public function getPdfStatuslist($user_id)
    {
       $data = $this->db->get_where('ant_images', array('user_id' => $user_id,'is_pdf_dwnload'=>'1'));
        if ($data->num_rows()>0) {
          return $data->result_array();
        }else{
          return 105;
        }
    }

     public function getTransactionList()
    {
      $user_id = $this->session->userdata('user_id');
       $data = $this->db->query("SELECT apr.*,apro.* FROM ant_payment_record as apr inner join ant_projects as apro on apr.product_id = apro.id where (apr.user_id  = $user_id)");
        if ($data->num_rows()>0) {
          return $data->result_array();
        }else{
          return 105;
        }
    }

    public function updateUserInformation($data)
    {
      if(isset($data['first_name']) && isset($data['user_id']))
      {
       $ant_registerTbl = $this->db->query("UPDATE ant_register SET first_name = '".$data['first_name']."' WHERE user_id = '".$data['user_id']."' ");
       if ($ant_registerTbl) {
        return true;
       }else{
        return false;
       }
      }
      else if(isset($data['last_name']) && isset($data['user_id']))
      {
        $ant_registerTbl = $this->db->query("UPDATE ant_register SET last_name = '".$data['last_name']."' WHERE user_id = '".$data['user_id']."' ");
         if ($ant_registerTbl) {
        return true;
       }else{
        return false;
       }
      }
      else if(isset($data['email']) && isset($data['user_id']))
      {
           $chkEmailExist=$this->db->query("select email from ant_lgn_details where email='".$data['email']."'");
          if($chkEmailExist->num_rows() > 0)
          {
            echo '406';
          }
          else
          {
            $ant_lgn_details = $this->db->query("UPDATE ant_lgn_details SET email = '".$data['email']."' WHERE user_id = '".$data['user_id']."' ");
              if ($ant_lgn_details) {
                   return true;
                 }else{
                  return false;
                 }   
          }
      
      }else{
          return false;
        }

    }
    public function getPriceValWidDomainExt($extname)
    {
        $this->db->select('price');
        $extPrice = $this->db->get_where('pricelist', array('domain_ext' => ".".$extname));
        if ($extPrice->num_rows()>0) {
          return $extPrice->result_array();
        }else{
          return 0;
        }
    }
    public function chkProfileImg()
    {
      $user_id = $this->session->userdata('user_id');
      $qry=$this->db->query("select images from ant_register where user_id='".$user_id."'");
      if ($qry->num_rows()>0) {
         $imgname =  $qry->row_array();
           if ($imgname['images'] == '') {
              return "img/profile-information/default_large.png";

             }else{
                  return "saveImg/".base64_encode($user_id)."/profile.png";        
             }
      }else{
      }
    }

    // public function getPngImgThumbname()
    // {
    //    $directory = "saveImg/".base64_encode($this->session->userdata('user_id'))."/";
    //     $files = glob($directory . "*");
    //     // $dir_arr = array();
    //     foreach($files as $file)
    //     {
    //      //check to see if the file is a folder/directory
    //      if(is_dir($file))
    //      {

    //         //array_search("red",$a);
    //         $folderName=explode(base64_encode($this->session->userdata('user_id'))."/", $file);
    //         $file_1st_letter=substr($folderName[1],0,1); 
    //         $stat = stat($directory);
    //         $fileDtae=date ("F d Y", $stat['mtime']);
    //         $dir_arr[]=array("foldername"=>$folderName, "file_modify"=>$fileDtae, "file_1st_letter"=>ucwords($file_1st_letter));
    //       //echo "<li class='pro_list_box' onclick=\"openThisProject('".$folderName[1]."')\"><a href='javascript:void(0);'><div class='pro_lefts'><div class='my_Folder_img ".strtolower($file_1st_letter)."'><div class='folder_letter'>".ucwords($file_1st_letter)."</div></div></div><div class='pro_rights'>".ucwords($folderName[1])."<br><small>".$fileDtae."</small></div></a></li>";
            
    //      }
    //     }
    //        return $dir_arr;
    // }

     public function getPngImgThumbname()
    {
      $qry = $this->db->query("SELECT * FROM ant_projects where user_id = '".$this->session->userdata('user_id')."'");
      if ($qry->num_rows()>0) {
        return $qry->result_array();
      }else{
        return 101;
      }

    }

     public function getPngImgFileThumbname($foldernames)
    {
     // $datas=$_REQUEST['foldernames'];
      //echo "data=".$foldernames;
     //$datas = $_COOKIE['projectName'];
      $qry = $this->db->query("select * from ant_projects,ant_images where ant_projects.id=ant_images.project_id and ant_projects.project_name='".$foldernames."' and ant_projects.user_id='".$this->session->userdata('user_id')."' and extName='htmls'");
      if ($qry->num_rows()>0) {
        return $qry->result_array();
      }else{
        //echo $qry->num_rows();
        return 101;
      }

    }
    
}