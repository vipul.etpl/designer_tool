<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projects_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_project_list()
	{
		$this->db->select("*");
		$this->db->from("ant_projects left join ant_register on ant_register.user_id=ant_projects.user_id");
		$data=$this->db->get();
		if($data->num_rows() > 0)
		{
			return $data->result_array();
		}
		else
		{
			echo "101";
		}
	}
	public function delete_this_project($value)
	{
		$this->db->query("delete from ant_projects where id='".$value."'");
	}
	public function get_file_rate()
	{
		$this->db->select("*");
		$this->db->from("projects_rate");
		$qry=$this->db->get();	
		return $qry->result_array();
	}
	public function chnage_file_rate($value)
	{
		$this->db->query("update projects_rate set per_file_rate='$value'");
	}

	public function get_project_files($proids)
	{
		$this->db->select("*");
		$this->db->from("ant_images");
		$this->db->where("project_id = $proids");
		$qry=$this->db->get();
		if($qry->num_rows() > 0)
		return $qry->result_array();
	}
	public function get_project_fiels_list($parametre)
	{
		$parametre1=explode('_', $parametre);
		$this->db->select("*");
		$this->db->from("ant_projects,ant_images");
		$this->db->where("ant_projects.user_id = ant_images.user_id and ant_projects.user_id = ".$parametre1[1]." and ant_projects.project_name = '".$parametre1[0]."'");
		$qry=$this->db->get();
		if($qry->num_rows() > 0)
		{	
			return $qry->result_array();
		}
	}
	public function decide_file_cost($fileprice)
	{
		$this->db->select("*");
		$this->db->from("ant_file_cost");
		$qry=$this->db->get();
		if($qry->num_rows() > 0)
		{
			$this->update_cost($fileprice);
		}
		else
		{
			$created_date=date('Y-m-d');
			$data=array('cost'=>$fileprice,'created_date'=>$created_date,'updated_date'=>$created_date);
			$this->db->insert('ant_file_cost',$data);
		}
		
	}
	public function update_cost($prices)
	{
		 $updated_date=date('Y-m-d');
		 $this->db->query("update ant_file_cost set cost='".$prices."', updated_date='".$updated_date."'");
		 // $this->db->set("updated_date", $updated_date);
   //  	 $this->db->update("ant_file_cost");
	}
	public function best_designs($design_data)
	{
		$created_date=date('Y-m-d');
		$data=array('design_names'=>$design_data, 'created_date'=>$created_date);
		$this->db->insert('ant_best_design',$data);
	}

	public function get_projects_price($proname)
	{
		$this->db->select("count(imgName) as cnt");
		$this->db->from("ant_images, ant_projects");
		$this->db->where("ant_projects.id = ant_images.project_id and ant_projects.project_name='".$proname."'");
		$qry=$this->db->get();
		if($qry->num_rows() > 0)
		{
			$file_cnt=$qry->result_array();
			$this->db->select("cost");
			$this->db->from("ant_file_cost");
			$costs=$this->db->get();
			$tot_cost=$costs->result_array();
			// return $file_cnt[0]['cnt'];
			$costss=$file_cnt[0]['cnt'] * $tot_cost[0]['cost'];
			return $costss;
		}
	}

	public function getBase64String($proname,$user_id)
	{
		$fp=file_get_contents("saveImg/".base64_encode($user_id)."/".$proname."/index.txt");
		return $fp;
	}

}