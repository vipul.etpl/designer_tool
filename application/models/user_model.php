<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_user_list()
	{
		$this->db->select("*");
		$this->db->from("ant_register,ant_lgn_details");
		$this->db->where("ant_register.user_id = ant_lgn_details.user_id");
		$data=$this->db->get();
		if($data->num_rows() > 0)
		{
			return $data->result_array();
		}
	}
	public function delete_this_user($value)
	{
		$this->db->query("delete from ant_register where user_id='".$value."'");
	}
	public function get_user_details($user_ids)
	{
		$this->db->select("*");
		$this->db->from("ant_register, ant_lgn_details");
		$this->db->where("ant_register.user_id = ant_lgn_details.user_id and ant_register.user_id = ".$user_ids);
		$qry=$this->db->get();
		if($qry->num_rows() > 0)
		{
			return $qry->result_array();
		}
	}
}