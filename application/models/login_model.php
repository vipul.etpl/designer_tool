<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class Login_model extends CI_Model{
    function  __construct() {
    parent::__construct();
    $errors = array();
    $data = array();
    $_REQUEST = json_decode(file_get_contents('php://input'), true);
    //$this->load->model('product');
  }  
    // Get All User IDs
    public function user_ids(){
        $this->db->select("user_id");
        $this->db->from('ant_register');
        $query = $this->db->get();
        $row = $query->result();
        return $row;
    }
    
    // Get Current User ID
    public function current_id_to_validate($key, $salt){
        $ids = $this->user_ids();
        $id = "";
        foreach($ids as $k=>$v){
            if($key == md5(base64_encode(sha1($v->user_id . $salt, true) . $salt))){
                 $id = $v->user_id;
            }                         
        }
        return $id;
    }
    
    // Confirm User email successfully
    public function email_success($user_id){
        $data = array(
               'last_login' => date("Y-m-d h:i:s", time()),
               'is_email' => 1               
            );               
        $this->db->where('user_id', $user_id);
        $this->db->update('ant_register', $data); 
    }


    // Get user email address to verify email address
    public function get_user_email_and_name($user_id){
        $data = array();
        $this->db->select("email")->from('ant_lgn_details')->where("user_id", $user_id);
        $query = $this->db->get();
        $row = $query->row_array();
        $data["email"] = $row['email'];
        $this->db->select("*")->from('ant_register')->where("user_id", $user_id);
        $query = $this->db->get();
        $row = $query->row_array();
        $data["name"] = $row['first_name']. " " . $row['last_name'];
        return $data;
    } 

    // Validate the user login details 
    public function validate_user($data)
    {
        $pwd = base64_encode(sha1($data['password'] . $data['salt'], true) . $data['salt']);
        $where = "(email='{$data['email']}' OR username='{$data['email']}') AND password='$pwd'";
        $this->db->where($where);
        $query = $this->db->get('ant_lgn_details');
        if($query->num_rows() == 1){
            return true;
        }else{
            return false;
        }       
    }
    
    // Validate user - Main Area 
    public function validate_me(){
       $data = array();
        $server_info = $_SERVER['SERVER_NAME'];
       $get_username = explode('.', $server_info);
      // echo $get_username[0];
       //exit();
       $data['email'] = $get_username[0]; 
       $data['password'] = $_REQUEST['password']; 
       $data['salt'] = $this->get_salt_using_email($get_username[0]);
       return $this->validate_user($data);
    }
    
    // Get User ID using email or User name 
    public function get_userid_uname_or_email($unemail){
        $data = "";
        $this->db->select('user_id')->from('ant_lgn_details');
        $this->db->where("email", $unemail);
        $this->db->or_where("username", $unemail);
        $query = $this->db->get();
        $row = $query->row_array();
        if($query->num_rows() > 0){     
            $data = $row["user_id"];
        }
        return $data; 
    }
    
     // Get Salt Using Email address 
    public function get_salt_using_email($email){ 
        $this->db->select("salt")->from("ant_lgn_details")->where('email', $email);
        $query = $this->db->get();
        $row = $query->row_array();
        if($query->num_rows() > 0){  
         return $row["salt"];
        }else{
            return $this->get_salt_using_username($email);
        }
    }     
          
    public function get_salt_using_username($username){
        $this->db->select("salt")->from("ant_lgn_details")->where('username', $username);
        $query = $this->db->get();
        $row = $query->row_array();
        if($query->num_rows() > 0){  
         return $row["salt"];
        }
    }
    
    // Check username is exist
    public function check_username_exist($username){ 
        $this->db->select("username")->from('ant_lgn_details')->where("username", $username);
        $query = $this->db->get();
        $row = $query->row_array();
        if($query->num_rows() > 0){
           return true;
        }else{
          return false;
        }
    }
    
    // check is user login or not
    public function is_login(){
        if(!$this->session->userdata('user_data')){
           redirect('home'); 
        }
    }
    
    public function already_login(){
        if($this->session->userdata('user_data')){
           redirect('dashboard'); 
        }
    }
    
    // Check is user email is verified of not
    public function is_email_verified($user_id){
       $this->db->select("is_email")->from('ant_register');
       $this->db->where("user_id", $user_id);  
        $query = $this->db->get();
        $row = $query->row_array();
        if($query->num_rows() > 0){
          if($row["is_email"] == "1"){
              return true;
          }else{
              return false;
          }
        }
    }     
}



