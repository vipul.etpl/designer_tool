<?php
  require_once '../includes/DB_Function.php';
  class Email extends  DB_Functions{ 
    function __construct() {
         require_once '../includes/DB_Connect.php';
        $this->db = new DB_Connect();
        $this->db->connect();
    }
    function __destruct(){ 
    } 
    
    // Encrypt user id to send for email verification 
    public function enrypt_user_id($user_id, $salt){
        return base64_encode(sha1($user_id . $salt, true) . $salt);                                                                    
    } 
    
        // Send email confirmation email - 1/16/2013
    public function send_email_confirmation($email, $user_id, $salt, $name){
            $enc_user_id = $this->enrypt_user_id($user_id, $salt);
            $enc_user_id = md5($enc_user_id);
            $salt = urldecode($salt);
            require_once("../../script/libmail.php");
            //Init. user email
            $mail = $email;
            $message = file_get_contents("../../media/emails/email_confirmation.html");
            $message = preg_replace('/{user}/', $name, $message);
            $message = preg_replace('/{user_id}/', $enc_user_id, $message);
            $message = preg_replace('/{salt}/', $salt, $message);
            //Send email actions;
            $m = new Mail ();
            //Sender email
            $m->From ("admin@happyedit.com");
            //Registration user mail
            $m->To ($mail);
            //Theme
            $m->Subject ("Happy Edit Account Confirmation");
            //Mail content
            $m->Body ($message,"html");
            $m->Send ();
            return 1;
    } 
}

    $user_id = $_REQUEST["user_id"];
    $ob = new Email();
    $udata = $ob->user_details($user_id);
    echo $ob->send_email_confirmation($udata["email"], $user_id, $udata["salt"], $udata["name"]);
?>