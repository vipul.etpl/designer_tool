<?php

		
		define('HOST', 'localhost');
		define('USER', 'root');
		define('PASS', 'root');
		define('DBNAME', 'omr');

		$db = new mysqli(HOST, USER, PASS, DBNAME);

		if ($db->connect_errno) {
		echo "Failed to connect to MySQL: (" . $db->connect_errno . ") "
		. $db->connect_error;
		} else {
		$sql = "SELECT expense_type_id, category_name FROM  omr_expense_type";
		$result_db = $db->query($sql);
		if (!$result_db) {
		echo $db->error . ' Error perform query!';
		} else {
			echo "<select name='category_name' id ='category_name' >";
			echo "<option value=''>Select...</option>";
			while ($row = $result_db->fetch_object())
			{
				echo "<option value='.$row->expense_type_id.'>";
				echo $row->category_name;
				echo "</option>";
			}
			echo "</select>";
		  }
		}
		$db->close();
?>