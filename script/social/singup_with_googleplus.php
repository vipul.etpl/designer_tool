<?php
    require_once '../includes/DB_Function.php';   
    class GooglePlus extends  DB_Functions{      
        function __construct() {
         require_once '../includes/DB_Connect.php';
        $this->db = new DB_Connect();
        $this->db->connect();
        }
        function __destruct(){ 
        }  
        
    }

    $data = array();
    $data["google_id"] = $_REQUEST["google_id"];
    $data["name"] = $_REQUEST["name"];
    $data["email"] = $_REQUEST["email"]; 
    $ob = new GooglePlus();
    $flname = array();
    $flname = $ob->get_fl_name($data["name"]);
    $data["first_name"] = $flname["first_name"];
    $data["last_name"] = $flname["last_name"];
    // Check If email is exist?
    if($ob->check_email_exist($data["email"])){
       // Not Exist 
       $user_id = "";
       $user_id = $ob->google_store_login_data($data);
       $user_data = array();
       $user_data = $ob->user_details($user_id);    
       echo md5($ob->enrypt_user_id($user_data["user_id"], $user_data["salt"]));
    }else{
        // Exist
        $user_id = $ob->get_user_id_by_email($data["email"]);
        // Update the data
        if($ob->update_social_id('google_id', $data["google_id"], $user_id)){
            $user_data = array();
            $user_data = $ob->user_details($user_id);    
            echo md5($ob->enrypt_user_id($user_data["user_id"], $user_data["salt"]));                                                                          
        }
    }
     
   
?>