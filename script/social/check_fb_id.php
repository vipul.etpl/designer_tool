<?php
require_once '../includes/DB_Function.php';
class Social extends  DB_Functions{ 
    function __construct() {
         require_once '../includes/DB_Connect.php';
        $this->db = new DB_Connect();
        $this->db->connect();
    }
    function __destruct(){ 
    }   
}

$social_id = $_REQUEST["id"];        
$social_id_value = $_REQUEST["value"];        
$ob = new Social();
if($ob->check_social_id($social_id, $social_id_value)){
    echo 1;
}else{
    echo 0;
}
?>