<?php
require_once '../includes/DB_Function.php';
class Signup extends  DB_Functions{ 
    function __construct() {
         require_once '../includes/DB_Connect.php';
        $this->db = new DB_Connect();
        $this->db->connect();
    }
    function __destruct(){ 
    }
    
    // Get First and last name  - YK
    public function get_fl_name($name){
        $name = explode(" ", $name);   
        $data = array(); 
        $data["first_name"] = $name[0];
        $data["last_name"] = $name[1];
        return $data;
    }
    
    // create username string  - YK
    public function username_string($name){
        
    }
    
    // store register data - YK
    public function store_register_data($name){
       $user_id = $this->get_max_id("omr_register", "user_id");
       $flname = $this->get_fl_name($name);
       $query = "INSERT INTO omr_register(user_id, first_name, last_name, register_date) VALUES('$user_id','{$flname['first_name']}','{$flname['last_name']}', now())"; 
       if(!$result = mysql_query($query)){
            exit(mysql_error());
       }
       return $user_id;
    }
    
    // Store Login data - YK
    public function store_login_data($data){
        $lgn_id = $this->get_max_id("omr_lgn_details", "lgn_id");
        $user_id = $this->store_register_data($data["name"]);
        $pdata = $this->encrypt_password($data["password"]);
        $query = "INSERT INTO omr_lgn_details(lgn_id, user_id, email, password, salt) VALUES('$lgn_id','$user_id', '{$data["email"]}','{$pdata['password']}','{$pdata['salt']}')";
        if(!$result = mysql_query($query)){
            exit(mysql_error());
        }
        // Send email
        $this->send_email_confirmation($data["email"], $user_id, $pdata["salt"], $data["name"]);
        $udata = array();
        $udata = $this->user_details($user_id);
        return $udata;
    }
    
    // Encrypt user id to send for email verification 
    public function enrypt_user_id($user_id, $salt){
        return base64_encode(sha1($user_id . $salt, true) . $salt);                                                                    
    }
    
    // Send email confirmation email - 1/16/2013
    public function send_email_confirmation($email, $user_id, $salt, $name){
            $enc_user_id = $this->enrypt_user_id($user_id, $salt);
            $enc_user_id = md5($enc_user_id);
            $salt = urldecode($salt);
            require_once("../../script/libmail.php");
            //Init. user email
            $mail = $email;
            $message = file_get_contents("../../media/emails/email_confirmation.html");
            $message = preg_replace('/{user}/', $name, $message);
            $message = preg_replace('/{user_id}/', $enc_user_id, $message);
            $message = preg_replace('/{salt}/', $salt, $message);
            //Send email actions;
            $m = new Mail ();
            //Sender email
            $m->From ("anything@admixtion.com");
            //Registration user mail
            $m->To ($mail);
            //Theme
            $m->Subject ("One Minute Receipt Account Confirmation");
            //Mail content
            $m->Body ($message,"html");
            $m->Send ();
    }
      
    public function checkhashSSHA($salt, $password) { 
        $hash = base64_encode(sha1($password . $salt, true) . $salt); 
        return $hash;
    }  
}

    $ob = new Signup(); 
    $response = array();
?>