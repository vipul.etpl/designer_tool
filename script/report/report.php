<?php
require_once '../includes/DB_Function.php';
  class Report extends  DB_Functions
  { 
    function __construct() 
    {
         require_once '../includes/DB_Connect.php';
        $this->db = new DB_Connect();
        $this->db->connect();
    }
    function __destruct()
    { 

    } 

    public function get_max_id($table, $id)
    {
        $data = 0;
        $query="select max(".$id.") from ".$table;
        if(!$result = mysql_query($query)){
          exit(mysql_error());
        }       

        if(mysql_num_rows($result) > 0){
          while ($row = mysql_fetch_array($result))
          {
              $data = $row[0] + 1;
          }
        }
        else
        {
          $data=1;
        }
        
        return $data;
    }
   

   public function report($data){
    
         $report_id = $this->get_max_id("omr_report_header", "report_id");

           $query = "SELECT orh.`receipt_id`,orh.`receipt_name`,orh.`total_amount`,orh.`expense_type_id`,oet.`category_name` FROM `omr_receipt_header` orh INNER JOIN  `omr_expense_type` oet on orh.`receipt_date` between '{$data['start_date']}' and '{$data['end_date']}' and orh.`expense_type_id`=oet.`expense_type_id` and orh.`user_id`='{$data['user']}'";
        if(!$result = mysql_query($query)){
            exit(mysql_error());
        }
        $rcount=mysql_num_rows($result);
        //exit();
         $user_id=$data['user'];
         $report_name=$data['report_name'];
         $start_date=$data['start_date'];
         $to_date=$data['end_date'];

        /****************insert into omr_report_header***************/

        if($rcount>0)
        {
            $insert='insert into omr_report_header (report_id,user_id,report_name,report_date,submited_date,from_date,to_date,created_by) VALUES ('.$report_id.','.$user_id.',"'.$report_name.'","'.date('Y-m-d h:i:s', time()).'","'.date('Y-m-d h:i:s', time()).'","'.$start_date.'","'.$to_date.'",'.$user_id.')';
            if(!$insert_result=mysql_query($insert))
            {
              exit(mysql_error());
            }

            // $report_id = mysql_insert_id();
             
             
            while($row=mysql_fetch_assoc($result))
            {
              $item_number = $this->get_max_id("omr_report_items", "item_number");
        
              $insert_item='insert into omr_report_items (item_number,user_id,report_id,receipt_id,name) VALUES ('.$item_number.','.$user_id.',"'.$report_id.'","'.$row['receipt_id'].'","'.$row['receipt_name'].'")';
              if(!$insert_item_result=mysql_query($insert_item))
              {
                exit(mysql_error());
              }
        
            }
        }
        else
        {
          echo "0";
        }
        
    
   
    return true;

    }

}

$data = array();
 $data["start_date"] = $_REQUEST['start_date'];
 $data["end_date"] = $_REQUEST['end_date'];
 $data["user"] = $_REQUEST['user'];
 $data["report_name"] = $_REQUEST['report_name'];
$ob = new Report();
$ob->report($data);

