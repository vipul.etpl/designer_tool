<?php
require_once '../includes/DB_Function.php';
  class Changemail extends  DB_Functions{ 
    function __construct() {
         require_once '../includes/DB_Connect.php';
        $this->db = new DB_Connect();
        $this->db->connect();
    }
    function __destruct(){ 
    } 

   public function get_max_id($table, $id)
    {
        $data = 0;
        $query="select max(".$id.") from ".$table;
        if(!$result = mysql_query($query)){
          exit(mysql_error());
        }       

        if(mysql_num_rows($result) > 0){
          while ($row = mysql_fetch_array($result))
          {
              $data = $row[0] + 1;
          }
        }
        else
        {
          $data=1;
        }
        
        return $data;
    }

    function update_email($data){
        //$query = "INSERT INTO ant_temp_email(user_id,email) VALUES('{$data['user_id']}','{$data['email']}')";
        $id = $this->get_max_id("ant_temp_email", "id");
        $email=$data["email"];
        $user_id=$data["user_id"];
    	$query = "insert into ant_temp_email (id,user_id,email) values ('.$id.','.$user_id.','$email')";
    	if(!$result = mysql_query($query)){
    		exit(mysql_error());
    	}
        $this->send_email_change_confirmation($email, $user_id);
    	return true;
    }

   /* function temp_email($data){
        $id = $this->get_max_id("ant_temp_email", "id");
        echo $email=$data["email"];
        $user_id=$data["user_id"];
        //$query = "INSERT INTO ant_temp_email(user_id,email) VALUES('{$data['user_id']}','{$data['email']}')";
        $query = "insert into ant_temp_email (id,user_id,email) values ('.$id.','.$user_id.','$email')";
        if(!$result = mysql_query($query)){
            exit(mysql_error());
        }
        return true;
    }*/

   public function enrypt_user_id($user_id, $salt){
        return base64_encode(sha1($user_id . $salt, true) . $salt);                                                                    
    } 

    public function send_email_change_confirmation($email, $user_id, $salt, $name){
            $enc_user_id = $this->enrypt_user_id($user_id, $salt);
            $enc_user_id = md5($enc_user_id);
            $salt = urldecode($salt);
            require_once("../../script/libmail.php");
            //Init. user email
            $mail = $email;
            $message = file_get_contents("../../media/emails/change_email_conf.html");
            $message = preg_replace('/{user}/', $name, $message);
            $message = preg_replace('/{user_id}/', $enc_user_id, $message);
            $message = preg_replace('/{salt}/', $salt, $message);
            //Send email actions;
            $m = new Mail ();
            //Sender email
            $m->From ("admin@happyedit.com");
            //Registration user mail
            $m->To ($mail);
            //Theme
            $m->Subject ("Happy Edit Email Change Confirmation");
            //Mail content
            $m->Body ($message,"html");
            $m->Send ();
            return 1;
    } 
}

$data = array();
$data["user_id"] = $_REQUEST['user_id'];
$data["email"] = $_REQUEST['email'];

$ob = new Changemail();
if($ob->update_email($data)){
	echo 1;
    //echo 'Check Mail For verification';
}
/*if($ob->temp_email($data)){
    echo 1;
    //echo 'Check Mail For verification';
}*/
//$udata = $ob->user_details($user_id);
//echo $ob->send_email_change_confirmation($data["email"], $data["user_id"], $udata["salt"], $udata["name"]);

?>