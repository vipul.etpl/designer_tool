var base_url = "http://"+window.location.hostname+"/designer_tool/";
var winArrays=[];
var myImgTempStorage=[];
var myCanvasPngImg=[];
var layerContent=[];
var videoContents=[];
var youtube_video_temp=[];
var svgTemplates=[];
var userSession=false;
window.requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(a, b) {
    return window.setTimeout(a, 1E3 / 60)
};
window.cancelCancelRequestAnimationFrame = window.cancelCancelRequestAnimationFrame || window.webkitCancelRequestAnimationFrame || window.mozCancelRequestAnimationFrame || window.oCancelRequestAnimationFrame || window.msCancelRequestAnimationFrame || window.clearTimeout;
var BROWSER_TYPE, BROWSER_VERSION; - 1 != navigator.userAgent.indexOf("Firefox") ? (BROWSER_TYPE = "firefox", BROWSER_VERSION = parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf("Firefox") + 8))) : -1 != navigator.userAgent.indexOf("Chrome") ? (BROWSER_TYPE = "chrome", BROWSER_VERSION = parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf("Chrome") + 7).split(" ")[0])) : -1 != navigator.userAgent.indexOf("Safari") && (BROWSER_TYPE = "safari", BROWSER_VERSION = -1 != navigator.userAgent.indexOf("Version") && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf("Version") +
    8).split(" ")[0]));
var IS_IPHONE = -1 != navigator.userAgent.indexOf("iPhone"),
    IS_IPOD = -1 != navigator.userAgent.indexOf("iPod"),
    IS_IPAD = -1 != navigator.userAgent.indexOf("iPad"),
    IS_IOS = IS_IPHONE || IS_IPOD || IS_IPAD,
    DEVICE_WITH_TOUCH_EVENTS = "ontouchstart" in window,
    DEBUG_MODE = !0;

function isWebglSupported() {
    var a = document.createElement("canvas"),
        b = {
            antialias: !0,
            stencil: !0
        },
        c = a.getContext("webgl", b);
    c || (c = a.getContext("experimental-webgl", b));
    c || (c = a.getContext("webkit-3d", b));
    c || (c = a.getContext("moz-webgl", b));
    return c ? !0 : !1
}

function GetUrlVars() {
    var a = {};
    window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(b, c, d) {
        a[c] = d
    });
    return a
}

function GetFunctionName(a) {
    var b = "function" == typeof a;
    a = b && (a.name && ["", a.name] || a.toString().match(/function ([^\(]+)/));
    return !b && "not a function" || a && a[1] || "anonymous"
}
var TransferData = null,
    ClipboardData = null;

function SetTransferData(a) {
    TransferData = a
}

function GetTransferData() {
    return TransferData
}

function SetClipboardData(a) {
    ClipboardData = a
}

function GetClipboardData() {
    return ClipboardData
}

function LocalizeContent(a) {
    a = a.querySelectorAll("[localize]");
    for (var b, c, d, e = a.length, f = 0; f < e; f++) b = a[f], c = b.getAttribute("localize"), "" != c ? (d = b.hasAttribute("idLocalization") ? b.getAttribute("idLocalization") : b.getAttribute(c), b.setAttribute(c, LOCALIZED_STRINGS[d])) : (d = b.hasAttribute("idLocalization") ? b.getAttribute("idLocalization") : b.innerHTML, b.innerHTML = LOCALIZED_STRINGS[d])
}

function ToHex(a) {
    a = parseInt(a, 10);
    if (isNaN(a)) return "00";
    a = Math.max(0, Math.min(a, 255));
    return "0123456789ABCDEF".charAt((a - a % 16) / 16) + "0123456789ABCDEF".charAt(a % 16)
}

function RgbToHex(a, b, c) {
    return ToHex(a) + ToHex(b) + ToHex(c)
}

function DegToRad(a) {
    return a * Math.PI / 180
}

function Intersection_RectRect(a, b) {
    return !(b.left > a.right || b.right < a.left || b.top > a.bottom || b.bottom < a.top)
}
Array.prototype.clone = function() {
    return this.slice(0)
};
Array.prototype.add = Array.prototype.push;
Array.prototype.addAt = function(a, b) {
    this.splice(b, 0, a)
};
Array.prototype.replaceAt = function(a, b) {
    var c = this[b];
    this.splice(b, 1, a);
    return c
};
Array.prototype.remove = function(a) {
    a = this.indexOf(a); - 1 < a && this.splice(a, 1)
};
Array.prototype.removeAt = function(a) {
    var b = this[a];
    this.splice(a, 1);
    return b
};
Date.prototype.daysInMonth = function() {
    return (new Date(this.getFullYear(), this.getMonth() + 1, 0)).getDate()
};
Date.prototype.dateToString = function(a) {
    var b = this.getFullYear(),
        c = this.getMonth(),
        d = this.getDate(),
        e = this.getDay(),
        f = c + 1,
        g = d,
        h = this.getHours(),
        k = this.getMinutes(),
        l = this.getSeconds();
    10 > f && (f = "0" + f);
    10 > g && (g = "0" + g);
    10 > h && (h = "0" + h);
    10 > k && (k = "0" + k);
    10 > l && (l = "0" + l);
    switch (a) {
        case "DATE_USE_NAMES":
            return Date.WEEK_DAY_NAMES[e] + " " + d + " " + Date.MONTH_NAMES[c] + " " + b;
        case "DATE_USE_SHORT_NAMES":
            return Date.WEEK_DAY_SHORT_NAMES[e] + " " + d + " " + Date.MONTH_SHORT_NAMES[c] + " " + b;
        case "FULL_TIMESTAMP":
            return b + "-" +
                f + "-" + g + " " + h + ":" + k + ":" + l;
        case "DATE_STAMP":
            return b + "-" + f + "-" + g;
        case "DATE_TIME":
            return g + "/" + f + "/" + b + " - " + h + ":" + k;
        case "HOURS_MINUTES":
            return h + ":" + k;
        case "HOURS_MINUTES_SECONDS":
            return h + ":" + k + ":" + l;
        default:
            return g + "/" + f + "/" + b
    }
};
Date.prototype.addHours = function(a) {
    var b = new Date(this);
    b.setHours(this.getHours() + a);
    return b
};
Date.fromTimestamp = function(a) {
    var b = a.split(" ");
    a = b[0].split("-");
    b = b[1].split(":");
    return new Date(a[0], a[1] - 1, a[2], b[0], b[1], b[2])
};
Date.MONTH_NAMES = "Gennaio Febbraio Marzo Aprile Maggio Giugno Luglio Agosto Settembre Ottobre Novembre Dicembre".split(" ");
Date.MONTH_SHORT_NAMES = "Gen Feb Mar Apr Mag Giu Lug Ago Set Ott Nov Dic".split(" ");
Date.WEEK_DAY_NAMES = "Domenica Luned\u00ec Marted\u00ec Mercoled\u00ec Gioved\u00ec Venerd\u00ec Sabato".split(" ");
Date.WEEK_DAY_SHORT_NAMES = "Dom Lun Mar Mer Gio Ven Sab".split(" ");
Date.WEEK_DAY_COLORS = "#FD96A0 #FFF #FFF #FFF #FFF #FFF #FFEFA9".split(" ");
Date.ONE_DAY_AS_MILLISECONDS = 864E5;
Date.ONE_HOUR_AS_MILLISECONDS = 36E5;
NodeList.prototype.indexOf = function(a) {
    return [].slice.call(this, 0).indexOf(a)
};
Element.prototype.selectAllTextInElement = function() {
    var a = document.createRange();
    a.selectNodeContents(this);
    var b = window.getSelection();
    b.removeAllRanges();
    b.addRange(a)
};
Element.prototype.enableScrollingWithTouchMove = function() {
    if (DEVICE_WITH_TOUCH_EVENTS) {
        var a = this;
        a.ontouchstart = function(b) {
            this.touchStartPosY = b.touches[0].pageY;
            this.maxScrollY = a.scrollHeight - a.offsetHeight;
            this.touchStartPosX = b.touches[0].pageX;
            this.maxScrollX = a.scrollWidth - a.offsetWidth
        };
        a.ontouchmove = function(b) {
            GLOBAL_MOUSE_LISTENER.lockedPageScroll || (b.touches[0].pageY > this.touchStartPosY ? 0 != a.scrollTop && (GLOBAL_MOUSE_LISTENER.preventPageScroll = !1) : a.scrollTop != this.maxScrollY && (GLOBAL_MOUSE_LISTENER.preventPageScroll = !1), b.touches[0].pageX > this.touchStartPosX ? 0 != a.scrollLeft && (GLOBAL_MOUSE_LISTENER.preventPageScroll = !1) : a.scrollLeft != this.maxScrollX && (GLOBAL_MOUSE_LISTENER.preventPageScroll = !1))
        }
    }
};
Element.prototype.indexOf = function(a) {
    return [].slice.call(this.childNodes, 0).indexOf(a)
};
Element.prototype.getGlobalPosition = function() {
    for (var a = this, b = 0, c = 0; a;) b += a.offsetLeft - a.scrollLeft + a.clientLeft, c += a.offsetTop - a.scrollTop + a.clientTop, a = a.offsetParent;
    return {
        x: b,
        y: c
    }
};
Element.prototype.globalToLocal = function(a) {
    var b = this.getGlobalPosition();
    return {
        x: a.x - b.x,
        y: a.y - b.y
    }
};
Element.prototype.getPosition = function() {
    return {
        x: this.offsetLeft,
        y: this.offsetTop
    }
};
Element.prototype.setPosition = function(a, b, c) {
    c = c || "px";
    this.style.left = a + c;
    this.style.top = b + c
};
Element.prototype.setPositionX = function(a, b) {
    this.style.left = a + (b || "px")
};
Element.prototype.setPositionY = function(a, b) {
    this.style.top = a + (b || "px")
};
Element.prototype.getSize = function() {
    return {
        x: this.offsetWidth,
        y: this.offsetHeight
    }
};
Element.prototype.setSize = function(a, b, c) {
    c = c || "px";
    this.style.width = a + c;
    this.style.height = b + c
};
Element.prototype.setSizeX = function(a, b) {
    this.style.width = a + (b || "px");
};
Element.prototype.setSizeY = function(a, b) {
    this.style.height = a + (b || "px")
};
Element.prototype.setTransform = function(a, b) {
    el.style.Transform = a;
    el.style.MozTransform = a;
    el.style.msTransform = a;
    el.style.OTransform = a;
    el.style.webkitTransform = a;
    void 0 != b && (el.style.TransformOrigin = b, el.style.MozTransformOrigin = b, el.style.msTransformOrigin = b, el.style.OTransformOrigin = b, el.style.webkitTransformOrigin = b)
};
Element.prototype.setRotation = function(a) {
    this.style.webkitTransform = "rotate(" + a + "deg) translateZ(0px)"
};
Element.prototype.getComputedDisplay = function() {
    return this.currentStyle ? this.currentStyle.display : getComputedStyle(this, null).display
};
Element.prototype.__defineSetter__("rotation", function(a) {
    this.style.webkitTransform = "rotate(" + a + "deg)"
});
Element.prototype.__defineGetter__("rotation", function() {
    var a = this.style.webkitTransform,
        b = a.indexOf("rotate(");
    return -1 == b ? 0 : a.substring(a.indexOf("rotate(") + 7, a.indexOf("deg)", b))
});
Element.prototype.__defineSetter__("positionX", function(a) {
    this.style.left = a + "px"
});
Element.prototype.__defineGetter__("positionX", function() {
    return parseInt(this.style.left)
});
Element.prototype.__defineSetter__("positionY", function(a) {
    this.style.top = a + "px"
});
Element.prototype.__defineGetter__("positionY", function() {
    return parseInt(this.style.top)
});
Element.prototype.__defineSetter__("sizeX", function(a) {
    this.style.width = a + "px"
});
Element.prototype.__defineGetter__("sizeX", function() {
    return parseInt(this.style.width)
});
Element.prototype.__defineSetter__("sizeY", function(a) {
    this.style.height = a + "px"
});
Element.prototype.__defineGetter__("sizeY", function() {
    return parseInt(this.style.height)
});
Node.prototype.getChildrenByNodeName = function(a, b) {
    for (var c = [], d = null, e = 0; e < this.childNodes.length; e++) d = this.childNodes[e], d.nodeName == a && c.push(d), b && c.concat(d.getChildrenByNodeName(a, b));
    return c
};
Node.prototype.getChildById = function(a) {
    for (var b = null, c = 0; c < this.childNodes.length && (b = this.childNodes[c], b.id != a) && !(b = b.getChildById(a)); c++);
    return b
};
Node.prototype.getChildByName = function(a) {
    for (var b = null, c = 0; c < this.childNodes.length && (b = this.childNodes[c], b.name != a) && !(b = b.getChildByName(a)); c++);
    return b
};
Node.prototype.getElementOrParentByAttribute = function(a, b) {
    return this.hasAttribute ? this.hasAttribute(a) && this.getAttribute(a) == b ? this : null != this.parentNode ? this.parentNode.getElementOrParentByAttribute(a, b) : null : null
};
Node.prototype.getElementOrParentById = function(a) {
    return this.id == a ? this : null != this.parentNode ? this.parentNode.getElementOrParentById(a) : null
};
Node.prototype.getElementOrParentByNodeName = function(a) {
    return this.nodeName == a ? this : null != this.parentNode ? this.parentNode.getElementOrParentByNodeName(a) : null
};
Node.prototype.getParentAsChildOfParent = function(a) {
    return this.parentNode == a ? this : null != this.parentNode ? this.parentNode.getParentAsChildOfParent(a) : null
};
Node.prototype.getFirstLevelInContainer = function(a) {
    return this.parentNode == a ? this : null != this.parentNode ? this.parentNode.getFirstLevelInContainer(a) : null
};
Node.prototype.getPreviousSiblingByNodeName = function(a) {
    return null != this.previousSibling ? this.previousSibling.nodeName == a ? this.previousSibling : this.previousSibling.getPreviousSiblingByNodeName(a) : null
};
Node.prototype.getNextSiblingByNodeName = function(a) {
    return null != this.nextSibling ? this.nextSibling.nodeName == a ? this.nextSibling : this.nextSibling.getNextSiblingByNodeName(a) : null
};

function SelectElement(a, b) {
    b = b || "radio";
    if ("radio" == b) {
        for (var c = a.parentNode.querySelectorAll(".selected"), d = 0; d < c.length; d++) c[d].classList.remove("selected");
        a.classList.add("selected")
    } else "check" == b && a.classList.toggle("selected");
    if (a.parentNode.onChange) a.parentNode.onChange(a)
}

function DeselectElements(a) {
    for (var b = 0; b < a.length; b++) a[b].classList.remove("selected")
}

function ShowElement(a, b, c) {
    b = b || "radio";
    if ("radio" == b) {
        b = a.parentNode.querySelectorAll(".showed");
        for (var d = 0; d < b.length; d++) b[d].classList.remove("showed");
        a.classList.add("showed")
    } else "check" == b && a.classList.toggle("showed");
    if (a.parentNode.onChange) a.parentNode.onChange(a);
    c && (a.style.left = c.x + "px", a.style.top = c.y + "px")
}

function ShowElementsInContainer(a, b, c) {
    a = a.querySelectorAll("[" + b + "]");
    for (var d, e, f = 0; f < a.length; f++) d = a[f], e = d.getAttribute(b), c[e] ? d.classList.add("showed") : d.classList.remove("showed")
}

function HideElements(a) {
    for (var b = 0; b < a.length; b++) a[b].classList.remove("showed")
}

function UpdateFields(a, b, c, d) {
    c = c || "name";
    for (var e = a.querySelectorAll("[" + c + "]"), f, g, h, k = e.length, l = 0; l < k; l++) {
        f = e[l];
        g = f.getAttribute(c);
        g = b[g];
        if (!0 == d)
            if (h = f.getFirstLevelInContainer(a), null == g || void 0 == g) {
                h.classList.remove("showed");
                continue
            } else h.classList.add("showed");
        if (null == g || void 0 == g) g = "";
        if (f.update) f.update(b);
        else switch (f.nodeName) {
            case "INPUT":
                "checkbox" == f.type ? f.checked = 1 == g ? !0 : !1 : f.value = g;
                break;
            case "SELECT":
                f.value = g;
                break;
            case "DIV":
                f.innerHTML = g;
                break;
            case "TEXTAREA":
                f.value =
                    g;
                break;
            case "A":
                f.setAttribute("href", g);
                break;
            case "IMG":
                f.setAttribute("src", g)
        }
    }
}

function PathInfo(a) {
    var b = a.split("/");
    a = b[b.length - 1];
    var c = "";
    1 < b.length && (b.pop(), c = b.join("/"));
    var b = a.split("."),
        d = "",
        e = "";
    1 < b.length && (e = b.pop(), d = b.join("."));
    return {
        dir: c,
        fileName: a,
        fileNameNoExtension: d,
        fileExtension: e
    }
}

function SendAndLoad(a, b, c, d, e, f) {
    var g = new XMLHttpRequest;
    g.open("POST", a, !0);
    c && (g.onload = c);
    d && (g.upload.onprogress = d);
    e && (g.onerror = e);
    f && (g.onabort = f);
    if (b) {
        a = null;
        if (b instanceof HTMLFormElement) a = new FormData(b);
        else {
            a = new FormData;
            for (var h in b) a.append(h, b[h])
        }
        g.send(a)
    } else g.send("");
    return g
}

function EventResponseToJSON(a) {
    a = a.target;
    try {
        return window.JSON.parse(a.responseText)
    } catch (b) {
        return {
            result: "error",
            message: b + " - server data: " + a.responseText
        }
    }
}

function DefaultResponseXML(a) {
    a = a.target;
    try {
        var b = a.responseXML,
            c = b.getElementsByTagName("state")[0].firstChild.nodeValue,
            d = b.getElementsByTagName("message")[0].firstChild.nodeValue;
        if ("SUCCESS" == c) return !0;
        "ERROR" == c && alert(d);
        return !1
    } catch (e) {}
    alert("Si \u00e8 verificato un errore: " + a.responseText)
}

function BrowserWinSize() {
    var a = 980,
        b = 560,
        a = window.innerWidth,
        b = window.innerHeight;
    return {
        x: a,
        y: b
    }
}

function ExtendClass(a, b) {
    function c() {}
    c.prototype = b.prototype;
    a.prototype = new c;
    a.prototype.constructor = a;
    a.baseConstructor = b;
    a.superClass = b.prototype
}

function Bind(a, b) {
    b || (console.log("Bind function is undefined"), console.log("caller is " + arguments.callee.caller.toString()), console.log(a));
    return function() {
        return b.apply(a, arguments)
    }
}

function GetKeyEventChar(a) {
    a = a || window.event;
    return String.fromCharCode(a.which || a.charCode || a.keyCode)
}

function EventPreventDefault(a) {
    a.preventDefault && a.preventDefault();
    a.stopPropagation && a.stopPropagation();
    return !1
}

function HTML(a) {
    return document.all ? document.all[a] : document.getElementById(a)
}

function CreateEventListener(a, b, c, d, e) {
    c = d ? Bind(d, c) : c;
    e = e || !1;
    if (a.addEventListener) {
        if (DEVICE_WITH_TOUCH_EVENTS) switch (b) {
            case "mousedown":
                b = "touchstart";
                break;
            case "mousemove":
                b = "touchmove";
                break;
            case "mouseup":
                b = "touchend"
        }
        a.addEventListener(b, c, e)
    } else a.attachEvent ? a.attachEvent("on" + b, c) : a["on" + b] = c;
    return c
}

function DeleteEventListener(a, b, c, d) {
    if (a.addEventListener) {
        if (DEVICE_WITH_TOUCH_EVENTS) switch (b) {
            case "mousedown":
                b = "touchstart";
                break;
            case "mousemove":
                b = "touchmove";
                break;
            case "mouseup":
                b = "touchend"
        }
        a.removeEventListener(b, c, d)
    } else a.attachEvent && a.detachEvent("on" + b, c)
}

function IncludeJavascript(a, b) {
    var c = document.getElementsByTagName("head")[0],
        d = document.createElement("script");
    d.src = a;
    d.type = "text/javascript";
    void 0 != b && null != b && (d.readyState ? d.onreadystatechange = function() {
        if ("loaded" == d.readyState || "complete" == d.readyState) d.onreadystatechange = null, b()
    } : d.onload = function() {
        b()
    });
    c.appendChild(d)
}

function IncludeCSS(a) {
    var b = document.getElementsByTagName("head")[0],
        c = document.createElement("link");
    c.setAttribute("rel", "stylesheet");
    c.setAttribute("type", "text/css");
    c.setAttribute("href", a);
    b.appendChild(c)
};

function XOS_EventDispatcher() {
    this.listeners = {}
}
_ = XOS_EventDispatcher.prototype;
_.dispatch = function(a) {
    "string" == typeof a && (a = {
        type: a
    });
    a.target || (a.target = this);
    if (!a.type) throw Error("Event object missing 'type' property.");
    if (this.listeners[a.type] instanceof Array)
        for (var b = this.listeners[a.type], c = 0, d = b.length; c < d; c++) b[c].call(this, a)
};
_.addEventListener = function(a, b) {
    "undefined" == typeof this.listeners[a] && (this.listeners[a] = []);
    this.listeners[a].push(b)
    //alert('dghfhgf');
};
_.removeEventListener = function(a, b) {
    if (this.listeners[a] instanceof Array)
        for (var c = this.listeners[a], d = 0, e = c.length; d < e; d++)
            if (c[d] === b) {
                c.splice(d, 1);
                break
            }
};

function XOS_GlobalMouseListener() {
    this.mouseDownPt = {
        x: 0,
        y: 0
    };
    this.mouseUpPt = {
        x: 0,
        y: 0
    };
    this.mouseMovePt = {
        x: 0,
        y: 0
    };
    this.checkForMouseDragPause = this.isDragAction = this.isMouseButtonPressed = !1;
    this.previousMouseMovePt = {
        x: 0,
        y: 0
    };
    this.checkForMouseMovePauseIntervalId = null;
    this.isMouseMovePaused = !1;
    this.snapValue = 20;
    this.isSnapped = !1;
    this.objectTarget = null;
    this.snapToAxisXY = this.snapToAngle = !0;
    this.lastMouseUpTime = (new Date).getTime();
    this.initEventListeners();
    this.preventPageScroll = !0;
    this.lockedPageScroll = !1
}
_ = XOS_GlobalMouseListener.prototype;
_.initEventListeners = function() {
    var a = document.getElementsByTagName("html")[0];
    DEVICE_WITH_TOUCH_EVENTS ? (CreateEventListener(a, "touchstart", this.onTouchStart, this, !0), CreateEventListener(a, "touchmove", this.onTouchMove, this), CreateEventListener(a, "touchend", this.onTouchEnd, this)) : (CreateEventListener(a, "mousedown", this.onMouseDown, this), CreateEventListener(a, "mousemove", this.onMouseMove, this), CreateEventListener(a, "mouseup", this.onMouseUp, this))
};
_.startSnap = function(a) {
    this.snapValue = a || 20;
    this.isSnapped = !0;
    this.previousMouseMovePt.x = this.mouseMovePt.x;
    this.previousMouseMovePt.y = this.mouseMovePt.y
};
_.lookForMouseMovePause = function(a) {
    this.isSnapped || (this.previousMouseMovePt.x == this.mouseMovePt.x && this.previousMouseMovePt.y == this.mouseMovePt.y ? this.isMouseMovePaused || (this.onMouseMoveStartPause(a), this.isMouseMovePaused = !0) : (this.isMouseMovePaused && (this.onMouseMoveEndPause(a), this.isMouseMovePaused = !1), this.previousMouseMovePt.x = this.mouseMovePt.x, this.previousMouseMovePt.y = this.mouseMovePt.y))
};
_.onMouseClick = function(a) {};
_.onMouseDoubleClick = function(a) {};
_.onStartDrag = function(a) {
    //console.log("my drag start");
    this.checkForMouseDragPause && (this.checkForMouseMovePauseIntervalId = setInterval(Bind(this, this.lookForMouseMovePause), 100));
    if (this.objectTarget && this.objectTarget.onGlobalMouseStartDrag) this.objectTarget.onGlobalMouseStartDrag(a)
};
_.onEndDrag = function(a) {
    this.checkForMouseDragPause && (clearInterval(this.checkForMouseMovePauseIntervalId), this.isSnapped = this.isMouseMovePaused = !1);
    if (this.objectTarget && this.objectTarget.onGlobalMouseEndDrag) this.objectTarget.onGlobalMouseEndDrag(a);
    this.snapToAngle = !0
};
_.onDrag = function(a) {
    //console.log("my mouse drag"); 
    if (this.objectTarget && this.objectTarget.onGlobalMouseDrag) this.objectTarget.onGlobalMouseDrag(a)
};
_.onMouseWheel = function(a) {};
_.onMouseMoveStartPause = function(a) {
    if (this.isDragAction) this.onStartPauseDrag(a)
};
_.onMouseMoveEndPause = function(a) {
    if (this.isDragAction) this.onEndPauseDrag(a)
};
_.onStartPauseDrag = function(a) {
    if (this.objectTarget && this.objectTarget.onGlobalMouseDragStartPause) this.objectTarget.onGlobalMouseDragStartPause(a)
};
_.onEndPauseDrag = function(a) {
    if (this.objectTarget && this.objectTarget.onGlobalMouseDragEndPause) this.objectTarget.onGlobalMouseDragEndPause(a)
};
_.onMouseDown = function(a) {
    this.eventLocationToPoint(a, this.mouseDownPt);
    this.mouseMovePt.x = this.mouseDownPt.x;
    this.mouseMovePt.y = this.mouseDownPt.y;
    if (!(a.srcElement instanceof HTMLUListElement) && (this.isMouseButtonPressed = !0, this.objectTarget && this.objectTarget.onGlobalMouseDown)) this.objectTarget.onGlobalMouseDown(a)
};
_.onMouseUp = function(a) {
    this.eventLocationToPoint(a, this.mouseUpPt);
    if (this.objectTarget && this.objectTarget.onGlobalMouseUp) this.objectTarget.onGlobalMouseUp(a);
    var b = (new Date).getTime();
    if (250 > b - this.lastMouseUpTime && this.objectTarget && this.objectTarget.onGlobalMouseDoubleClick) this.objectTarget.onGlobalMouseDoubleClick(a);
    this.lastMouseUpTime = b;
    this.isMouseButtonPressed = !1;
    if (this.isDragAction) this.onEndDrag(a);
    this.isDragAction = !1;
    this.objectTarget = null
};
_.onMouseClick = function(a) {
    if (this.objectTarget && this.objectTarget.onGlobalMouseClick) this.objectTarget.onGlobalMouseClick(a)
};
_.onMouseMove = function(a) {
    DEVICE_WITH_TOUCH_EVENTS && (this.preventPageScroll && a.preventDefault(), this.preventPageScroll = !0);
    this.eventLocationToPoint(a, this.mouseMovePt);
    if (this.isSnapped) {
        if (this.distanceFromPoints(this.mouseMovePt, this.previousMouseMovePt) < this.snapValue) return;
        this.isSnapped = !1
    }
    if (this.isMouseButtonPressed)
        if (this.isDragAction) this.onDrag(a);
        else 5 < this.distanceFromPoints(this.mouseMovePt, this.mouseDownPt) && (this.isDragAction = !0, this.onStartDrag(a));
    else if (this.objectTarget && this.objectTarget.onGlobalMouseMove) this.objectTarget.onGlobalMouseMove(a)
};
_.startDrag = function(a) {
    this.isDragAction = !0;
    this.onStartDrag(null)
};
_.onTouchStart = function(a) {
    this.onMouseDown(a)
};
_.onTouchMove = function(a) {
    this.onMouseMove(a)
};
_.onTouchEnd = function(a) {
    if (0 == a.targetTouches.length) this.onMouseUp(a)
};
_.onTouchCancel = function(a) {};
_.resetState = function() {
    this.isMouseMovePaused = this.isMouseButtonPressed = this.isSnapped = this.isDragAction = !1;
    this.objectTarget = null
};
_.eventLocationToPoint = function(a, b) {
    if (DEVICE_WITH_TOUCH_EVENTS) 0 < a.targetTouches.length && (b.x = a.targetTouches[0].pageX, b.y = a.targetTouches[0].pageY);
    else if (a || (a = window.event || window.Event), !a) b.x = b.y = 0;
    else if (a.pageX || a.pageY) b.x = a.pageX, b.y = a.pageY;
    else if (a.layerX || a.layerY) b.x = a.layerX, b.y = a.layerY;
    else if (a.clientX || a.clientY) b.x = a.clientX + document.body.scrollLeft + document.documentElement.scrollLeft, b.y = a.clientY + document.body.scrollTop + document.documentElement.scrollTop
};
_.distanceFromPoints = function(a, b) {
    var c = b.x - a.x,
        d = b.y - a.y;
    return Math.sqrt(c * c + d * d)
};
_.calculateSnapToAngle = function() {
    var a = Math.PI / 4,
        b = this.mouseMovePt.x - this.mouseDownPt.x,
        c = this.mouseMovePt.y - this.mouseDownPt.y,
        d = Math.atan2(c, b),
        a = Math.round(d / a) * a,
        d = a / (Math.PI / 180);
    0 == d || 180 == d ? this.mouseMovePt.y = this.mouseDownPt.y : 90 == d || -90 == d ? this.mouseMovePt.x = this.mouseDownPt.x : (b = Math.sqrt(b * b + c * c), this.mouseMovePt.x = this.mouseDownPt.x + b * Math.cos(a), this.mouseMovePt.y = this.mouseDownPt.y + b * Math.sin(a))
};
_.elementChildrenToDraggableElements = function(a) {
    for (var b = 0; b < a.childNodes.length; b++) 3 != a.childNodes[b].nodeType && this.createDraggableElement(a.childNodes[b])
};
_.createDraggableElement = function(a) {
    CreateEventListener(a, "mousedown", this.onMouseDownOnDraggableElement, this)
};
_.onMouseDownOnDraggableElement = function(a) {
    this.draggableElement = a.target;
    a = this.draggableElement.getPosition();
    this.draggableElement.dragOffsetX = GLOBAL_MOUSE_LISTENER.mouseMovePt.x - a.x;
    this.draggableElement.dragOffsetY = GLOBAL_MOUSE_LISTENER.mouseMovePt.y - a.y;
    GLOBAL_MOUSE_LISTENER.objectTarget = this
};
_.onGlobalMouseDrag = function(a) {
    this.draggableElement.setPosition(GLOBAL_MOUSE_LISTENER.mouseMovePt.x - this.draggableElement.dragOffsetX, GLOBAL_MOUSE_LISTENER.mouseMovePt.y - this.draggableElement.dragOffsetY)
};
var GLOBAL_MOUSE_LISTENER = new XOS_GlobalMouseListener;

function XOS_Window(a) {
    this.htmlElement = a;
    this.initEventHandlers();
    this.dragOffset = {
        x: 0,
        y: 0
    };
    this.bringToFront();
    this.dragAction = null;
    this.minSizeX = 150;
    this.minSizeY = 50;
    this.appOwner = null;
    this.content = this.htmlElement.getChildById("WIN_CONTENT");
    this.eventDispatcher = new XOS_EventDispatcher
}
XOS_Window.CLOSE = "CLOSE";
_ = XOS_Window.prototype;
_.addEventListener = function(a, b) {
    this.eventDispatcher.addEventListener(a, b)
};
_.removeEventListener = function(a, b) {
    this.eventDispatcher.removeEventListener(a, b)
};
_.initEventHandlers = function() {
    CreateEventListener(this.htmlElement, "mousedown", this.onMouseDown, this)
};
_.setTitle = function(a) {
    this.htmlElement.getChildById("WIN_TITLE").innerHTML = a
};
_.open = function(a) {
    if (!this.isOpen()) {
        this.htmlElement.classList.remove("closed");
        if (!0 == a) {
            a = BrowserWinSize();
            var b = this.htmlElement.getSize();
            this.setPosition(a.x / 2 - b.x / 2, a.y / 2 - b.y / 2)
        }
        this.bringToFront()
    }
};
_.close = function(a) {
    this.isOpen() && (this.htmlElement.classList.add("closed"), !0 != a && this.eventDispatcher.dispatch({
        type: XOS_Window.CLOSE
    }))
};
_.isOpen = function() {
    return this.htmlElement.classList.contains("closed") ? !1 : !0
};
_.bringToFront = function() {
    this.htmlElement.parentNode.maxLevel || (this.htmlElement.parentNode.maxLevel = 1);
    this.htmlElement.parentNode.maxLevel++;
    this.htmlElement.style.zIndex = this.htmlElement.parentNode.maxLevel
};
_.getPosition = function() {
    return {
        x: this.htmlElement.offsetLeft,
        y: this.htmlElement.offsetTop
    }
};
_.setPosition = function(a, b) {
    this.htmlElement.style.left = a + "px";
    this.htmlElement.style.top = b + "px"
};
_.setSize = function(a, b) {
    this.htmlElement.style.width = a + "px";
    this.htmlElement.style.height = b + "px"
};
_.getSize = function() {
    return {
        x: this.htmlElement.offsetWidth,
        y: this.htmlElement.offsetHeight
    }
};
_.onMouseDown = function(a) {
    this.bringToFront();
    if (a = a.srcElement ? a.srcElement : a.target)
        if (a.parentNode && "WIN_HEAD" == a.parentNode.getAttribute("id") || a.parentNode && "WIN_TITLE" == a.parentNode.getAttribute("id") || a.getAttribute && "WIN_HEAD" == a.getAttribute("id")) this.dragAction = "DRAG_WIN", a = this.getPosition(), this.dragOffset.x = GLOBAL_MOUSE_LISTENER.mouseMovePt.x - a.x, this.dragOffset.y = GLOBAL_MOUSE_LISTENER.mouseMovePt.y - a.y, GLOBAL_MOUSE_LISTENER.objectTarget = this;
        else if (a.getAttribute && "WIN_CLOSE_BUTTON" ==
        a.getAttribute("id")) this.close();
    else if (a.getAttribute && "WIN_RESIZE_BUTTON" == a.getAttribute("id")) {
        this.dragAction = "RESIZE_WIN";
        a = this.getPosition();
        var b = this.getSize();
        this.dragOffset.x = a.x + b.x - GLOBAL_MOUSE_LISTENER.mouseMovePt.x;
        this.dragOffset.y = a.y + b.y - GLOBAL_MOUSE_LISTENER.mouseMovePt.y;
        GLOBAL_MOUSE_LISTENER.objectTarget = this
    } else a.getAttribute && "WIN_COLLAPSE_BUTTON" == a.getAttribute("id") && this.htmlElement.classList.toggle("collapsed")
};
_.onGlobalMouseStartDrag = function() {};
_.onGlobalMouseEndDrag = function() {
    this.dragAction = GLOBAL_MOUSE_LISTENER.objectTarget = null
};
_.onGlobalMouseDrag = function(a) {
    if ("DRAG_WIN" == this.dragAction) this.setPosition(GLOBAL_MOUSE_LISTENER.mouseMovePt.x - this.dragOffset.x, GLOBAL_MOUSE_LISTENER.mouseMovePt.y - this.dragOffset.y);
    else if ("RESIZE_WIN" == this.dragAction) {
        var b = this.getPosition();
        a = GLOBAL_MOUSE_LISTENER.mouseMovePt.x - b.x + this.dragOffset.x;
        b = GLOBAL_MOUSE_LISTENER.mouseMovePt.y - b.y + this.dragOffset.y;
        a < this.minSizeX && (a = this.minSizeX);
        b < this.minSizeY && (b = this.minSizeY);
        this.setSize(a, b)
    }
};

function XOS_ListView(a) {
    this.htmlViewElement = a;
    this.htmlViewElement.enableScrollingWithTouchMove();
    this.selectedItemClassName = "selected";
    this.dragOverItemClassName = "dragOver";
    this.selectedItems = [];
    this.lastMouseDownTime = null;
    this.serviceUrl = "";
    this.loadingState = this.serviceVars = null;
    this.createEventHandlers();
    this.locked = !1
}
_ = XOS_ListView.prototype;
_.createEventHandlers = function() {
    CreateEventListener(this.htmlViewElement, "mousedown", this.onMouseDown, this)
};
_.onMouseDown = function(a) {

    if ("LI" == a.target.parentElement.nodeName)
        //console.log(a.target.parentElement.nodeName);
        if ("DIV" == a.target.nodeName) {
            if (this.selectItem(a.target.parentElement), this.isDoubleClick()) this.onDoubleClick(a)
        } else 3 == a.which && this.showItemContextMenu(a)
};
_.isDoubleClick = function() {
    var a = (new Date).getTime();
    if (250 > a - this.lastMouseDownTime) return !0;
    this.lastMouseDownTime = a;
    return !1
};
_.onDoubleClick = function(a) {
    if($("#LAYERS_2D_PANEL li.selected").attr("id")=="myWorkspace")
    {}
    else
    {
         "DIV" == a.target.nodeName && "title" == a.target.getAttribute("id") && this.editItemName(a.target);
    }
};
_.selectItem = function(a) {
    this.deselectAllItems();
    this.selectedItems.push(a);
    a.classList.add(this.selectedItemClassName)
};
_.deselectItem = function(a) {
    var b = this.selectedItems.indexOf(a); - 1 != b && (this.selectedItems.splice(b, 1), a.classList.remove(this.selectedItemClassName))
};
_.deselectAllItems = function() {
    for (var a = this.selectedItems.length, b = 0; b < a; b++) this.selectedItems[b].classList.remove(this.selectedItemClassName);
    this.selectedItems = []
};
_.lockInteraction = function() {
    this.disableDraggableItems();
    this.locked = !0
};
_.unlockInteraction = function() {
    this.enableDraggableItems();
    this.locked = !1
};
_.disableDraggableItems = function() {
    for (var a = this.htmlViewElement.querySelectorAll('li[draggable="true"]'), b = 0; b < a.length; b++) a[b].setAttribute("draggable", "disabled")
};
_.enableDraggableItems = function() {
    for (var a = this.htmlViewElement.querySelectorAll('li[draggable="disabled"]'), b = 0; b < a.length; b++) a[b].setAttribute("draggable", "true")
};
_.editSelectedItemName = function() {
    0 != this.selectedItems.length && this.editItemName(this.selectedItems[0].getChildById("title"))
};
_.editItemName = function(a) {
    //this.lockInteraction();
    var b = a.textContent;
    console.log(b);
    a.innerHTML = '<input type="text"  class="inputEditItemName" value="' + b + '" onChange="this.parentNode.innerHTML=this.value;" maxlength="15"></input>';
    var c = this;
    //console.log(this.parentNode);
    setTimeout(function() {
        //a.firstChild.focus();
       // console.log(a.firstChild);
        CreateEventListener(a.firstChild, "change", function() {
            if ("" != this.value && this.value != b) c.onEndEditItemName(this);
            this.blur()
        })
    }, 100)
};
_.onEndEditItemName = function(a) {
    this.unlockInteraction()
};
_.showItemContextMenu = function(a) {};
_.loadItemList = function(a, b) {
    a && (this.serviceUrl = a);
    b && (this.serviceVars = b);
    this.htmlViewElement.innerHTML = "";
    this.showLoadingState();
    var c = this;
    SendAndLoad(this.serviceUrl, this.serviceVars, function(a) {
        a = a.target;
        //console.log(a.responseText);
        a = window.JSON.parse(a.responseText);
        c.onLoadItemList(a)
    })
};
_.onLoadItemList = function(a) {
    this.hideLoadingState();
    if (a.items)
        for (var b = 0; b < a.items.length; b++) this.htmlViewElement.appendChild(this.createItemFromData(a.items[b]))
};
_.createItemFromData = function(a) {};
_.createItem = function(a) {
  //  console.log("createItem");
    var b = document.createElement("li"),
        c = document.createElement("div");
    c.setAttribute("id", "title");
    c.appendChild(document.createTextNode(a));
    b.appendChild(c);
    return b
};
_.showLoadingState = function() {
    this.loadingState && (this.loadingState.style.display = "block")
};
_.hideLoadingState = function() {
    //this.loadingState && (this.loadingState.style.display = "none")
    alert('load');
};

function XOS_TreeView(a) {
    this.htmlViewElement = a;
    this.htmlViewElement.enableScrollingWithTouchMove();
    this.selectedNodeClassName = "selected";
    this.expandedNodeClassName = "expanded";
    this.dragOverNodeClassName = "dragOver";
    this.selectedNodes = [];
    this.lastMouseDownTime = null;
    this.serviceUrl = "";
    this.loadingState = this.serviceVars = null;
    this.enableNodeDrop = this.enableNodeDrag = !1;
    this.createEventHandlers()
}
_ = XOS_TreeView.prototype;
_.createEventHandlers = function() {
    CreateEventListener(this.htmlViewElement, "mousedown", this.onMouseDown, this)
};
_.onMouseDown = function(a) {
    if ("LI" == a.target.parentElement.nodeName) {
        if ("A" == a.target.nodeName) this.expandOrCollapseNode(a.target.parentElement);
        else if ("DIV" == a.target.nodeName) {
           // console.log(a.target.nodeName);
            this.selectNode(a.target.parentElement);
            this.onDoubleClickOnNodeName(a);
            return
        }
        3 == a.which && this.showNodeContextMenu(a)
    } else this.deselectAllNodes()
};
_.isDoubleClick = function() {
    var a = (new Date).getTime();
    if (250 > a - this.lastMouseDownTime) return !0;
    this.lastMouseDownTime = a;
    return !1
};
_.onDoubleClickOnNodeName = function(a) {
    this.editNodeName(a.target)
};
_.getDropableNodeTarget = function(a) {
    return a.target.getElementOrParentByAttribute("dropable", "true")
};
_.onDragOverNode = function(a) {
    var b = this.getDropableNodeTarget(a);
    b && b.classList.add(this.dragOverNodeClassName);
    a.stopPropagation();
    a.preventDefault()
};
_.onDragLeaveNode = function(a) {
    var b = this.getDropableNodeTarget(a);
    b && b.classList.remove(this.dragOverNodeClassName);
    a.stopPropagation();
    a.preventDefault()
};
_.onDragStartNode = function(a) {
    SetTransferData(a.target);
    a.altKey || (a.dataTransfer.effectAllowed = "move");
    a.stopPropagation()
};
_.onDropOnNode = function(a) {
    var b = this.getDropableNodeTarget(a);
    this.onDragAndDropEnd(GetTransferData(), b);
    this.onDragLeaveNode(a);
    a.stopPropagation();
    a.preventDefault()
};
_.onDragAndDropEnd = function(a, b) { };
_.moveNodes = function(a, b) {
    a.parentNode.removeChild(a);
    this.getNodeElement_ul(b).appendChild(a)
};
_.disableDraggableNodes = function() {
    for (var a = this.htmlViewElement.querySelectorAll('li[draggable="true"]'), b = 0; b < a.length; b++) a[b].setAttribute("draggable", "disabled")
};
_.enableDraggableNodes = function() {
    for (var a = this.htmlViewElement.querySelectorAll('li[draggable="disabled"]'), b = 0; b < a.length; b++) a[b].setAttribute("draggable", "true")
};
_.editSelectedNodeName = function() {
    0 != this.selectedNodes.length && this.editNodeName(this.selectedNodes[0].querySelector("div"))
};
_.getNodeName = function(a) {
    return a.querySelector("div").textContent
};
_.editNodeName = function(a) {
    this.disableDraggableNodes();
    var b = a.textContent;
    a.innerHTML = '<input type="text"  class="inputNodeName" value="' + b + '" onBlur="this.parentNode.innerHTML=this.value;" ></input>';
    var c = this;
    setTimeout(function() {
        a.firstChild.focus();
        CreateEventListener(a.firstChild, "change", function() {
            if ("" != this.value && this.value != b) c.onEndEditNodeName(this);
            this.blur()
        })
    }, 100)
};
_.onEndEditNodeName = function(a) {
    this.enableDraggableNodes()
};
_.showNodeContextMenu = function(a) {};
_.expandOrCollapseNode = function(a) {
    a.classList.contains(this.expandedNodeClassName) ? this.collapseNode(a) : this.expandNode(a)
};
_.expandNode = function(a) {
    a.classList.add(this.expandedNodeClassName);
    return (a = this.getNodeElement_ul(a)) ? a : null
};
_.collapseNode = function(a) {
    a.classList.remove(this.expandedNodeClassName)
};
_.loadNodeChildren = function(a) {
    var b = null;
    a && (b = this.getNodeElement_ul(a), a.getAttribute("id"));
    b || (b = this.htmlViewElement);
    b.innerHTML = "";
    this.showLoadingState();
    var c = this;
    SendAndLoad(this.serviceUrl, this.serviceVars, function(a) {
        a = window.JSON.parse(a.target.responseText);
        c.onLoadNodeChildren(a, b)
    })
};
_.onLoadNodeChildren = function(a, b) {
    this.hideLoadingState();
    if (a.items)
        for (var c = null, d = 0; d < a.items.length; d++) c = this.createNodeChildFromData(a.items[d]), b.appendChild(c), this.onNodeAdded(c)
};
_.createNodeChildFromData = function(a) {
    var b;
    b = this.createTreeViewNode(a.itemType + "_icon16x16", a.itemTitle, null, a.hasChildren);
    b.setAttribute("itemType", a.itemType);
    b.setAttribute("id", a.id);
    return b
};
_.onNodeAdded = function(a) {
    this.enableNodeDrag && (a.draggable = !0, a.ondragstart = Bind(this, this.onDragStartNode));
    this.enableNodeDrop && (a.setAttribute("dropable", "true"), a.ondrop = Bind(this, this.onDropOnNode), a.ondragover = Bind(this, this.onDragOverNode), a.ondragleave = Bind(this, this.onDragLeaveNode))
};
_.getNodeElement_ul = function(a) {
    return a.getElementsByTagName("ul")[0]
};
_.getSelecteNodes = function() {
    return this.selectedNodes
};
_.selectNode = function(a) {
    this.deselectAllNodes();
    this.selectedNodes.push(a);
    a.classList.add(this.selectedNodeClassName)
};
_.deselectNode = function(a) {
    var b = this.selectedNodes.indexOf(a); - 1 != b && (this.selectedNodes.splice(b, 1), a.classList.remove(this.selectedNodeClassName))
};
_.deselectAllNodes = function() {
    for (var a = this.selectedNodes.length, b = 0; b < a; b++) this.selectedNodes[b].classList.remove(this.selectedNodeClassName);
    this.selectedNodes = []
};
_.createTreeViewNode = function(a, b, c, d) {
    var e = document.createElement("li");
    c && e.setAttribute("title", c);
    c = document.createElement("div");
    c.className = a;
    c.appendChild(document.createTextNode(b));
    e.appendChild(c);
    !0 == d && (element_ul = document.createElement("ul"), e.appendChild(element_ul));
    a = document.createElement("a");
    e.appendChild(a);
    return e
};
_.showLoadingState = function() {
    this.loadingState && (this.loadingState.style.display = "block")
};
_.hideLoadingState = function() {
    this.loadingState && (this.loadingState.style.display = "none")
};

function XOS_Widget(a, b, c) {
    this.htmlElement = a;
    this.collapsedClassName = "collapsed";
    this.widgetHeaderHeight = 27;
    b && (this.onExpandCallback = b);
    c && (this.onCollapseCallback = c);
    this.init()
}
_ = XOS_Widget.prototype;
_.init = function() {
    this.viewElement = this.htmlElement.querySelector(".view");
    this.expandCollapseButtonElement = this.htmlElement.querySelector("#EXPAND_COLLAPSE_BUTTON");
    var a = this;
    CreateEventListener(this.expandCollapseButtonElement, "mousedown", function() {
        a.expandOrCollapseWidget()
    });
    this.updateLayout()
};
_.expandOrCollapseWidget = function() {
    if (this.htmlElement.classList.contains(this.collapsedClassName)) {
        if (this.onExpandCallback) this.onExpandCallback()
    } else if (this.onCollapseCallback) this.onCollapseCallback();
    this.htmlElement.classList.toggle(this.collapsedClassName);
    this.updateLayout()
};
_.updateLayout = function() {
    var a = this.htmlElement.parentNode.querySelectorAll(".widget"),
        b = a.length;
    //console.log("maxWidgets: " + b);
    var c = b * this.widgetHeaderHeight;
   // console.log("allHeadersHeight: " + c);
    var d = this.htmlElement.parentNode.offsetHeight,
        c = d - c;
   // console.log("widgetParentHeight: " + d);
   // console.log("totalAutoResizablesHeight: " + c);
    for (var e, f, d = [], g = 0; g < b; g++) e = a[g], e.classList.contains(this.collapsedClassName) ? e.style.height = this.widgetHeaderHeight + "px" : 0 < parseInt(e.style.maxHeight) ? (f = parseInt(e.style.maxHeight),
        e.style.height = f + "px", c -= f - this.widgetHeaderHeight) : d.push(e);
    a = d.length;
    b = c / a + this.widgetHeaderHeight;
   // console.log("autoHeight");
   // console.log(b);
    for (g = 0; g < a; g++) d[g].style.height = b + "px"
};
XOS_Widget.updateAll = function() {
    for (var a = null, b = document.querySelectorAll(".widget"), c = b.length, d = 0; d < c; d++) a != b[d].parentNode && b[d].widgetController.updateLayout(), a = b[d].parentNode
};
XOS_Widget.parseDocument = function() {
    for (var a = document.querySelectorAll(".widget"), b = a.length, c = 0; c < b; c++) a[c].widgetController = new XOS_Widget(a[c])
};

function XOS_ColumnsResizer(a, b) {
    this.htmlElement = a;
    this.updateLayoutCallback = b;
    this.dragOffset = this.dragPos = 0;
    CreateEventListener(this.htmlElement, "mousedown", this.onMouseDown, this)
}
_ = XOS_ColumnsResizer.prototype;
_.onMouseDown = function(a) {
    GLOBAL_MOUSE_LISTENER.objectTarget = this
};
_.onGlobalMouseDrag = function(a) {
    this.dragOffset = GLOBAL_MOUSE_LISTENER.mouseMovePt.x - this.dragPos;
    this.dragPos = GLOBAL_MOUSE_LISTENER.mouseMovePt.x;
    this.onChange()
};
_.onGlobalMouseStartDrag = function(a) {
    this.dragPos = GLOBAL_MOUSE_LISTENER.mouseDownPt.x
};
_.onGlobalMouseEndDrag = function(a) {};
_.onChange = function(a) {
    a = 0;
    "" != this.htmlElement.style.left ? (a = parseInt(this.htmlElement.style.left) + this.dragOffset, this.htmlElement.style.left = a + "px") : "" != this.htmlElement.style.right && (a = parseInt(this.htmlElement.style.right) - this.dragOffset, this.htmlElement.style.right = a + "px");
    var b = this.htmlElement.getPreviousSiblingByNodeName("DIV"),
        c = this.htmlElement.getNextSiblingByNodeName("DIV");
    "" != b.style.width && (b.style.width = parseInt(b.style.width) + this.dragOffset + "px", "" != c.style.left && (c.style.left =
        a + 4 + "px", "" != c.style.width && (c.style.width = parseInt(c.style.width) - this.dragOffset + "px")));
    "" != b.style.right && (b.style.right = parseInt(b.style.right) - this.dragOffset + "px", "" != c.style.width && (c.style.width = parseInt(c.style.width) - this.dragOffset + "px"));
    this.updateLayoutCallback()
};
XOS_ColumnsResizer.parseDocument = function(a) {
    for (var b = document.querySelectorAll(".columnsResizer"), c = b.length, d = 0; d < c; d++) new XOS_ColumnsResizer(b[d], a)
};

function XOS_RadioButtons(a, b) {
    this.displayElement = a;
    CreateEventListener(a, "mousedown", this.onClick, this);
    this.onChangeFunctionListener = b
}
_ = XOS_RadioButtons.prototype;
_.onClick = function(a) {
    for (a = a.target; a && a.parentNode != this.displayElement;) a = a.parentNode;
    a && this.setSelectedRadioButton(a)
};
_.onChange = function() {
    if (this.onChangeFunctionListener) this.onChangeFunctionListener(this)
};
_.setSelectedRadioButton = function(a) {
    var b = this.getSelectedRadioButton();
    if (a != b && (!b || this.onDeselectRadioButton(b)) && this.onSelectRadioButton(a)) this.onChange()
};
_.getSelectedRadioButton = function() {
    return this.displayElement.querySelector(".selected")
};
_.onDeselectRadioButton = function(a) {
    a.classList.remove("selected");
    return !0
};
_.onSelectRadioButton = function(a) {
    a.classList.add("selected");
    return !0
};

function XOS_RadioPanels(a, b) {
    this.displayElement = a;
    this.onChangeFunctionListener = b
}
_ = XOS_RadioPanels.prototype;
_.onChange = function() {
    if (this.onChangeFunctionListener) this.onChangeFunctionListener(this)
};
_.setActiveRadioPanel = function(a) {
    var b = this.getActiveRadioPanel();
    if (a != b && (!b || this.onDeactivateRadioPanel(b)) && this.onActivateRadioPanel(a)) this.onChange()
};
_.getActiveRadioPanel = function() {
    return this.displayElement.parentNode.querySelector("#" + this.displayElement.id + ">*.activated")
};
_.onActivateRadioPanel = function(a) {
    a.classList.add("activated");
    return !0
};
_.onDeactivateRadioPanel = function(a) {
    a.classList.remove("activated");
   // var els=document.getElementById("COLOR_PICKER");
  // els.classList.remove("activated");
    return !0
};

function XOS_TabbedPanels(a, b, c) {
    this.buttonsElement = a;
    this.panelsElement = b;
    this.onChangeFunctionListener = c;
    CreateEventListener(this.buttonsElement, "mousedown", this.onClickOnButtons, this)
}
_ = XOS_TabbedPanels.prototype;
_.onClickOnButtons = function(a) {
    for (a = a.target; a && a.parentNode != this.buttonsElement;) a = a.parentNode;
    if (a) this.onClickOnTabElement(a)
};
_.getPanelList = function() {
    return this.panelsElement.parentNode.querySelectorAll("#" + this.panelsElement.id + " > .panel")
};
_.getTabsList = function() {
    return this.buttonsElement.parentNode.querySelectorAll("#" + this.buttonsElement.id + ">li")
};
_.onClickOnTabElement = function(a) {
    var b = this.getTabsList().indexOf(a);
    this.setActivePanel(this.getPanelList()[b], a)
};
_.setActivePanel = function(a, b) {
    var c = this.getActivePanel();
    a == c || c && !this.onDeactivatePanel(c) || !this.onActivatePanel(a) || (b || (c = this.panelsElement.indexOf(a), b = this.buttonsElement.childNodes[c]), c = this.buttonsElement.querySelector(".selected"), b != c && (c && c.classList.remove("selected"), b.classList.add("selected")), this.onChange())
};
_.getActivePanel = function() {
    return this.panelsElement.parentNode.querySelector("#" + this.panelsElement.id + ">*.activated")
};
_.onActivatePanel = function(a) {
    a.classList.add("activated");
    return !0
};
_.onDeactivatePanel = function(a) {
    a.classList.remove("activated");
    return !0
};
_.onChange = function() {
    if (this.onChangeFunctionListener) this.onChangeFunctionListener(this)
};
ExtendClass(XOS_TabbedDocsPanels, XOS_TabbedPanels);

function XOS_TabbedDocsPanels(a, b, c, d) {
    this.appRef = a;
    XOS_TabbedDocsPanels.baseConstructor.call(this, b, c, d)
}
_ = XOS_TabbedDocsPanels.prototype;
_.onClickOnButtons = function(a) {
    XOS_TabbedDocsPanels.superClass.onClickOnButtons.call(this, a);
    "A" == a.target.nodeName && this.removePanel(this.getActivePanel())
};
_.getPanelById = function(a) {
    return this.panelsElement.querySelector('[id="' + a + '"]')
};
_.addPanel = function(a) {
    this.panelsElement.appendChild(a);
    var b = a.getAttribute("id"),
        c = a.getAttribute("title"),
        d = document.createElement("li");
    d.setAttribute("id", b);
    d.setAttribute("title", c);
    d.innerHTML = "<div>" + c + "</div><a></a>";
    this.buttonsElement.appendChild(d);
    this.setActivePanel(a, d);
    this.updateStates(a)
};
_.removePanel = function(a) {
    if (!a.documentController.isChanged || a.documentController.canDocumentBeClosed()) {
         myCustomDialog();
        // var b = this.panelsElement.indexOf(a),
        //     b = this.buttonsElement.childNodes[b];
        // this.panelsElement.removeChild(a);
        // this.buttonsElement.removeChild(b);
        // a = this.panelsElement.childNodes.length;
        // 0 != a && this.setActivePanel(this.panelsElement.childNodes[a - 1])
    }
};
_.onActivatePanel = function(a) {
    XOS_TabbedDocsPanels.superClass.onActivatePanel.call(this, a);
    this.appRef.activeDocumentController = a.documentController;
    this.appRef.activeDocumentController.onShow();
    return !0
};
_.onDeactivatePanel = function(a) {
    XOS_TabbedDocsPanels.superClass.onDeactivatePanel.call(this, a);
    this.appRef.activeDocumentController.onHide();
    return !0
};
_.updateStates = function(a) {
    var b = this.getPanelList().indexOf(a),
        b = this.getTabsList()[b];
    a.documentController.isChanged ? b.classList.add("unsaved") : b.classList.remove("unsaved");
    b.title = a.title;
    b.id = a.id;
    b = b.childNodes[0];
    b.innerHTML = a.title;
    var c = PathInfo(a.title);
    b.className = "" != c.fileExtension ? c.fileExtension + "_icon16x16" : a.documentController.getDocumentTabIconClass()
};
_.areAllDocumentSaved = function() {
    for (var a = this.getPanelList(), b, c = a.length, d = 0; d < c; d++)
        if (b = a[d], b.documentController && b.documentController.isChanged) return !1;
    return !0
};

function XOS_Slider(a, b, c, d, e, f, g) {
    this.htmlElement = a;
    this.controlPoint = this.htmlElement.children[0];
    this.minValue = b;
    this.maxValue = c;
    this.rangeValue = c - b;
    this.currentRatioValue = this.currentValue = 0;
    this.setValue(d);
    this.previousRatioValue = this.previousValue = null;
    this.dragOffset = {
        x: 0,
        y: 0
    };
    this.isChanged = !1;
    this.onChangeStartFunction = f;
    this.onChangeFunction = e;
    this.onChangeEndFunction = g;
    this.init();
    this.updateControlPoint(this.currentRatioValue)
}
_ = XOS_Slider.prototype;
_.init = function() {
    this.initEventHandlers()
};
_.initEventHandlers = function() {
    CreateEventListener(this.htmlElement, "mousedown", this.onMouseDown, this)
};
_.setValue = function(a) {
    this.currentValue = a;
    this.currentRatioValue = this.htmlElement.offsetWidth / this.rangeValue * (this.currentValue - this.minValue) / this.htmlElement.offsetWidth;
    this.updateControlPoint(this.currentRatioValue)
};
_.onControlPointMoved = function() {
    var a = GLOBAL_MOUSE_LISTENER.mouseMovePt.x - this.globalPosition.x;
    0 > a ? a = 0 : a > this.htmlElement.offsetWidth && (a = this.htmlElement.offsetWidth);
    this.currentRatioValue = a / this.htmlElement.offsetWidth;
    this.valueChanged()
};
_.updateControlPoint = function(a) {
    a *= this.htmlElement.offsetWidth;
    var b = Math.ceil(this.controlPoint.offsetWidth / 2);
    this.controlPoint.setPosition(Math.round(a - b), 0);
    this.currentValue = this.minValue + this.currentRatioValue * this.rangeValue;
    0 < this.controlPoint.children.length && (this.controlPoint.children[0].innerHTML = Math.ceil(this.currentValue))
};
_.valueChanged = function() {
    this.updateControlPoint(this.currentRatioValue);
    this.isChanged = !0
};
_.onMouseDown = function(a) {
    this.globalPosition = this.htmlElement.getGlobalPosition();
    GLOBAL_MOUSE_LISTENER.objectTarget = this;
    GLOBAL_MOUSE_LISTENER.startDrag();
    a.preventDefault()
};
_.onGlobalMouseStartDrag = function(a) {
    this.previousValue = this.currentValue;
    this.previousRatioValue = this.currentRatioValue;
    this.dispatchChangeStartEvent()
};
_.onGlobalMouseDrag = function(a) {
    this.onControlPointMoved();
    this.dispatchChangeEvent()
};
_.onGlobalMouseEndDrag = function(a) {
    GLOBAL_MOUSE_LISTENER.objectTarget = null;
    this.dispatchChangeEndEvent()
};
_.dispatchChangeStartEvent = function(a) {
    if (null != this.onChangeStartFunction) this.onChangeStartFunction(this)
};
_.dispatchChangeEvent = function(a) {
    if (this.isChanged && null != this.onChangeFunction) this.onChangeFunction(this);
    this.isChanged = !1
};
_.dispatchChangeEndEvent = function(a) {
    if (null != this.onChangeEndFunction) this.onChangeEndFunction(this)
};

function XOS_ContextMenu(a) {
    this.htmlElement = a;
    XOS_ContextMenu.inizialized || (CreateEventListener(HTML("APPLICATION"), "mousedown", XOS_ContextMenu.hideActiveContextMenu, XOS_ContextMenu, !0), XOS_ContextMenu.inizialized = !0)
}
_ = XOS_ContextMenu.prototype;
_.showAt = function(a, b) {
    this.htmlElement.setPosition(a, b);
    this.show()
};
_.show = function() {
    this.htmlElement.classList.add("activated")
};
XOS_ContextMenu.hideActiveContextMenu = function(a) {
    a = HTML("CONTEXT_MENUS").querySelectorAll(".activated");
   //document.getElementById("CONTEXT_MENUS").children[3].classList.remove("activated");
    for (var b = 0; b < a.length; b++) a[b].classList.remove("activated");
   
};
ExtendClass(XOS_ColorPicker, XOS_ContextMenu);

function XOS_ColorPicker(a, b, c, d) {
    XOS_ColorPicker.baseConstructor.call(this, a);
    this.htmlElement = a;
    this.hexColor_input = this.alpha_point = this.alpha_panel = this.saturation_point = this.saturation_panel = this.hue_point = this.hue_panel = null;
    this.onChangeStartFunction = c;
    this.onChangeFunction = b;
    this.onChangeEndFunction = d;
    this.mouseElementTarget = null;
    this.rgb = {
        r: 0,
        g: 0,
        b: 0
    };
    this.hsv = {
        h: 0,
        s: 0,
        v: 0
    };
    this.hexColor = "#000000";
    this.alpha = 1;
    this.isChanged = null;
    this.previous_rgb = {
        r: 0,
        g: 0,
        b: 0
    };
    this.previous_alpha = this.previous_hexColor =
        0;
    this.init()
}
_ = XOS_ColorPicker.prototype;
_.init = function() {
    this.hue_panel = HTML("HUE_PANEL");
    this.hue_point = HTML("HUE_POINT");
    this.saturation_panel = HTML("SATURATION_PANEL");
    this.saturation_point = HTML("SATURATION_POINT");
    this.alpha_panel = HTML("ALPHA_PANEL");
    this.alpha_point = HTML("ALPHA_POINT");
    this.hexColor_input = HTML("HEX_COLOR_INPUT");
    this.initEventHandlers()
};
_.initEventHandlers = function() {
    DEVICE_WITH_TOUCH_EVENTS ? CreateEventListener(this.htmlElement, "touchstart", this.onMouseDown, this) : CreateEventListener(this.htmlElement, "mousedown", this.onMouseDown, this);
    CreateEventListener(this.hexColor_input, "change", this.onHexColorInputChange, this)
};
_.showAndSet = function(a, b, c, d, e, f, g) {
    this.showAt(a, b);
    this.setColorAndAlpha(c, d);
    this.hue_panel.globalPosition = this.hue_panel.getGlobalPosition();
    this.alpha_panel.globalPosition = this.alpha_panel.getGlobalPosition();
    this.saturation_panel.globalPosition = this.saturation_panel.getGlobalPosition();
    this.onChangeFunction = f;
    this.onChangeEndFunction = g
};
_.onMouseDown = function(a) {
    this.saveCurrentColorValues();
    var b = a.srcElement ? a.srcElement : a.target;
    if (b == this.hue_panel || b == this.hue_point) this.mouseElementTarget = "HUE_TARGET", GLOBAL_MOUSE_LISTENER.objectTarget = this, a.preventDefault();
    else if (b == this.saturation_panel || b == this.saturation_point) this.mouseElementTarget = "SATURATION_TARGET", GLOBAL_MOUSE_LISTENER.objectTarget = this, a.preventDefault();
    else if (b == this.alpha_panel || b == this.alpha_point) this.mouseElementTarget = "ALPHA_TARGET", GLOBAL_MOUSE_LISTENER.objectTarget =
        this, a.preventDefault()
};
_.onGlobalMouseUp = function(a) {
    if (!GLOBAL_MOUSE_LISTENER.isDragAction) {
        if ("HUE_TARGET" == this.mouseElementTarget) this.onChangeHSV_H_ByMouse();
        else if ("SATURATION_TARGET" == this.mouseElementTarget) this.onChangeHSV_SV_ByMouse();
        else if ("ALPHA_TARGET" == this.mouseElementTarget) this.onChangeAlpha_ByMouse();
        this.dispatchChangeEvent();
        this.dispatchChangeEndEvent()
    }
};
_.saveCurrentColorValues = function() {
    this.previous_rgb.r = this.rgb.r;
    this.previous_rgb.g = this.rgb.g;
    this.previous_rgb.b = this.rgb.b;
    this.previous_alpha = this.alpha;
    this.previous_hexColor = this.hexColor
};
_.onGlobalMouseStartDrag = function() {
    this.dispatchChangeStartEvent()
};
_.onGlobalMouseDrag = function(a) {
    if ("HUE_TARGET" == this.mouseElementTarget) this.onChangeHSV_H_ByMouse();
    else if ("SATURATION_TARGET" == this.mouseElementTarget) this.onChangeHSV_SV_ByMouse();
    else if ("ALPHA_TARGET" == this.mouseElementTarget) this.onChangeAlpha_ByMouse();
    this.dispatchChangeEvent()
};
_.onGlobalMouseEndDrag = function() {
    this.mouseElementTarget = GLOBAL_MOUSE_LISTENER.objectTarget = null;
    this.dispatchChangeEndEvent()
};
_.onChangeAlpha_ByMouse = function() {
    var a = GLOBAL_MOUSE_LISTENER.mouseMovePt.x - this.alpha_panel.globalPosition.x;
    0 > a ? a = 0 : 150 < a && (a = 150);
    this.alpha = a / 150;
    this.colorChanged()
};
_.onChangeHSV_H_ByMouse = function() {
    var a = GLOBAL_MOUSE_LISTENER.mouseMovePt.x - this.hue_panel.globalPosition.x;
    0 > a ? a = 0 : 150 < a && (a = 150);
    this.hsv.h = a / 150;
    this.rgb = this.hsvToRgb(this.hsv.h, this.hsv.s, this.hsv.v);
    this.colorChanged()
};
_.onChangeHSV_SV_ByMouse = function() {
    var a = this.saturation_panel.globalPosition,
        b = GLOBAL_MOUSE_LISTENER.mouseMovePt.x - a.x;
    0 > b ? b = 0 : 150 < b && (b = 150);
    a = GLOBAL_MOUSE_LISTENER.mouseMovePt.y - a.y;
    0 > a ? a = 0 : 150 < a && (a = 150);
    this.hsv.s = 1 - a / 150;
    this.hsv.v = b / 150;
    this.rgb = this.hsvToRgb(this.hsv.h, this.hsv.s, this.hsv.v);
    this.colorChanged()
};
_.onHexColorInputChange = function() {
    this.saveCurrentColorValues();
    this.dispatchChangeStartEvent(null);
    this.rgb = this.hexToRgb(this.hexColor_input.value, {
        r: 0,
        g: 0,
        b: 0
    });
    this.hsv = this.rgbToHsv(this.rgb.r, this.rgb.g, this.rgb.b);
    this.colorChanged();
    this.dispatchChangeEndEvent(null)
};
_.currentColorAsHtmlStringRGBA = function() {
    return "rgba(" + Math.round(255 * this.rgb.r) + "," + Math.round(255 * this.rgb.g) + "," + Math.round(255 * this.rgb.b) + "," + this.alpha + ")"
};
_.setColorAndAlpha = function(a, b) {
    this.rgb = this.htmlColorDefinitionToRGBA(a);
    this.hsv = this.rgbToHsv(this.rgb.r, this.rgb.g, this.rgb.b);
    b && (this.rgb.a = b);
    this.alpha = this.rgb.a;
    this.colorChanged()
};
_.setAlpha = function(a) {
    this.alpha = a;
    this.colorChanged()
};
_.colorChanged = function() {
    var a = this.rgbToHex(this.rgb.r, this.rgb.g, this.rgb.b);
    this.hexColor = a;
    var b = this.hsvToRgb(this.hsv.h, 1, 1),
        b = this.rgbToHex(b.r, b.g, b.b);
    this.hexColor_input.style.background = a;
    this.hexColor_input.value = a;
    this.hexColor_input.style.color = 65 > (100 * this.rgb.r + 100 * this.rgb.g + 100 * this.rgb.b) / 3 ? "#FFFFFF" : "#000000";
    this.saturation_panel.parentNode.style.background = b;
    a = (255 * this.rgb.r).toFixed(0) + "," + (255 * this.rgb.g).toFixed(0) + "," + (255 * this.rgb.b).toFixed(0);
    b = "" + ("background: -moz-linear-gradient(left,  rgba(" +
        a + ",0) 0%, rgba(" + a + ",1) 100%);") + ("background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(" + a + ",0)), color-stop(100%,rgba(" + a + ",1)));");
    b += "background: -webkit-linear-gradient(left,  rgba(" + a + ",0) 0%,rgba(" + a + ",1) 100%);";
    b += "background: -o-linear-gradient(left,  rgba(" + a + ",0) 0%,rgba(" + a + ",1) 100%);";
    b += "background: -ms-linear-gradient(left,  rgba(" + a + ",0) 0%,rgba(" + a + ",1) 100%);";
    b += "background: linear-gradient(left,  rgba(" + a + ",0) 0%,rgba(" + a + ",1) 100%);";
    this.alpha_panel.style.cssText =
        b;
    this.setSaturationPointByPos(150 * this.hsv.v, 150 * (1 - this.hsv.s));
    this.setHuePointByPos(150 * this.hsv.h);
    this.setAlphaPointByPos(150 * this.alpha);
    this.isChanged = !0
};
_.setAlphaPointByPos = function(a) {
    a -= 6; - 6 > a && (a = -6);
    a > this.alpha_panel.offsetWidth - 6 && (a = this.alpha_panel.offsetWidth - 6);
    this.alpha_point.setPosition(Math.round(a), 0)
};
_.setHuePointByPos = function(a) {
    a -= 6; - 6 > a && (a = -6);
    a > this.hue_panel.offsetWidth - 6 && (a = this.hue_panel.offsetWidth - 6);
    this.hue_point.setPosition(Math.round(a), 0)
};
_.setSaturationPointByPos = function(a, b) {
    a -= 4;
    b -= 4; - 4 > a ? a = -4 : a > this.saturation_panel.offsetWidth - 4 && (a = this.saturation_panel.offsetWidth - 4); - 4 > b ? b = -4 : b > this.saturation_panel.offsetHeight - 4 && (b = this.saturation_panel.offsetHeight - 4);
    this.saturation_point.setPosition(Math.round(a), Math.round(b))
};
_.dispatchChangeStartEvent = function(a) {
    if (null != this.onChangeStartFunction) this.onChangeStartFunction(this)
};
_.dispatchChangeEvent = function() {
    if (this.isChanged && null != this.onChangeFunction) this.onChangeFunction(this);
    this.isChanged = !1
};
_.dispatchChangeEndEvent = function(a) {
    if (null != this.onChangeEndFunction) this.onChangeEndFunction(this)
};
_.hsvToRgb = function(a, b, c) {
    var d, e, f;
    if (0 == c) f = e = d = 0;
    else {
        var g = Math.floor(6 * a),
            h = 6 * a - g;
        a = c * (1 - b);
        var k = c * (1 - b * h);
        b = c * (1 - b * (1 - h));
        switch (g) {
            case 1:
                d = k;
                e = c;
                f = a;
                break;
            case 2:
                d = a;
                e = c;
                f = b;
                break;
            case 3:
                d = a;
                e = k;
                f = c;
                break;
            case 4:
                d = b;
                e = a;
                f = c;
                break;
            case 5:
                d = c;
                e = a;
                f = k;
                break;
            case 6:
            case 0:
                d = c, e = b, f = a
        }
    }
    return {
        r: d,
        g: e,
        b: f
    }
};
_.rgbToHsv = function(a, b, c) {
    var d = Math.max(Math.max(a, b), c),
        e = Math.min(Math.min(a, b), c);
    if (e == d) e = a = 0;
    else {
        var f = d - e,
            e = f / d;
        a = (a == d ? (b - c) / f : b == d ? 2 + (c - a) / f : 4 + (a - b) / f) / 6;
        0 > a && (a += 1);
        1 < a && (a -= 1)
    }
    return {
        h: a,
        s: e,
        v: d
    }
};
_.hexToRgb = function(a, b) {
    void 0 == b && (b = null);
    "#" == a.substr(0, 1) && (a = a.substr(1));
    var c, d, e;
    if (3 == a.length) c = a.substr(0, 1), c += c, d = a.substr(1, 1), d += d, e = a.substr(2, 1), e += e;
    else if (6 == a.length) c = a.substr(0, 2), d = a.substr(2, 2), e = a.substr(4, 2);
    else return b;
    c = parseInt(c, 16);
    d = parseInt(d, 16);
    e = parseInt(e, 16);
    return isNaN(c) || isNaN(d) || isNaN(e) ? b : {
        r: c / 255,
        g: d / 255,
        b: e / 255
    }
};
_.rgbToHex = function(a, b, c, d) {
    a = Math.round(255 * a);
    b = Math.round(255 * b);
    c = Math.round(255 * c);
    void 0 == d && (d = !0);
    a = a.toString(16);
    1 == a.length && (a = "0" + a);
    b = b.toString(16);
    1 == b.length && (b = "0" + b);
    c = c.toString(16);
    1 == c.length && (c = "0" + c);
    return ((d ? "#" : "") + a + b + c).toUpperCase()
};
_.htmlColorDefinitionToRGBA = function(a) {
    var b = {
            r: 0,
            g: 0,
            b: 0,
            a: 1
        },
        c = a.indexOf("rgb("),
        d; - 1 != c ? (c += 4, d = a.indexOf(")", c), a = a.substring(c, d).split(","), b.r = 1 / 255 * parseInt(a[0]), b.g = 1 / 255 * parseInt(a[1]), b.b = 1 / 255 * parseInt(a[2]), b.a = 1) : (c = a.indexOf("rgba("), -1 != c ? (c += 5, d = a.indexOf(")", c), a = a.substring(c, d).split(","), b.r = 1 / 255 * parseInt(a[0]), b.g = 1 / 255 * parseInt(a[1]), b.b = 1 / 255 * parseInt(a[2]), b.a = parseFloat(a[3])) : (b = this.hexToRgb(a), b.a = 1));
    return b
};

function XOS_Application() {
    this.version = "3.0.0";
    this.contextMenus = {};
    this.activeDocumentController = null;
    window.addEventListener("unload", Bind(this, this.onExit), !1);
    window.onbeforeunload = Bind(this, this.onBeforeExit);
    window.onresize = Bind(this, this.onBrowserResize);
    DEVICE_WITH_TOUCH_EVENTS && document.addEventListener("orientationchange", Bind(this, this.onOrientationChanged));
    //gapi.client.load("drive", "v2", null);
    this.initGlobalInterface() 
}
_ = XOS_Application.prototype;
_.initGlobalInterface = function() {
    XOS_ColumnsResizer.parseDocument(Bind(this, this.onBrowserResize));
    var a = HTML("DOCUMENTS"),
        b = HTML("APPLICATION");
    CreateEventListener(b, "keydown", this.onKeyDown, this);
    CreateEventListener(b, "keyup", this.onKeyUp, this);
    this.documentsPanel = new XOS_TabbedDocsPanels(this, HTML("TABS_DOCUMENTS"), a);
    var c = this;
    HTML("TABS_RESOURCES_PANELS") && (this.resourcesPanel = new XOS_TabbedPanels(HTML("TABS_RESOURCES_PANELS"), HTML("RESOURCES_PANELS")));
    this.driveFileBrowserPanel = new DriveFileBrowser_panel(HTML("DRIVE_FILE_BROWSER_PANEL"),
        this);
    if (a = document.querySelector("#UPLOAD_FILE_menuItem #UPLOAD_FILE_TEXTFIELD")) c = this, a.addEventListener("change", function(a) {
        a = this.files[0];
        c.saveGeneralFileInDrive(a.name, a, null, null)
    }, !1);
    HTML("INSPECTOR_PANEL") && (this.inspectorPanel = new XOS_RadioPanels(HTML("INSPECTOR_PANEL")), HTML("INSPECTOR_PANEL").enableScrollingWithTouchMove(), this.inspectorPanel.panels = {});
    HTML("COLOR_PICKER") && (this.colorPicker = new XOS_ColorPicker(HTML("COLOR_PICKER"), null));
    this.messageWin = new XOS_Window(HTML("MESSAGE_WIN"));
    this.openDocumentWin = new DriveOpenDocument_window(HTML("OPEN_DOCUMENT_WIN"), this);
    //this.saveDocumentWin = new DriveSaveDocument_window(HTML("SAVE_DOCUMENT_WIN"), this);
    this.saveProgressWin = new XOS_Window(HTML("SAVE_PROGRESS_WIN"));
    this.saveProgressWin.uploadBytePerSeconds = 2E4;
    this.saveProgressWin.loaded = 0;
    this.saveProgressWin.total = 2E5;
    var d = this.saveProgressWin.htmlElement.querySelector("#BAR"),
        e = this.saveProgressWin.htmlElement.querySelector("#LABEL"),
        f = this.saveProgressWin;
    this.saveProgressWin.startProgress =
        function(a) {
            f.progressState = "progress";
            d.value = 0;
            f.loaded = 0;
            f.total = a;
            f.startProgressTime = (new Date).getTime();
            f.onProgress()
        };
    this.saveProgressWin.endProgress = function() {
        f.progressState = "end";
        f.endProgressTime = (new Date).getTime();
        f.uploadBytePerSeconds = f.total / ((f.endProgressTime - f.startProgressTime) / 1E3);
        d.value = 100;
        e.innerHTML = "Progress: 100%";
        setTimeout(function() {
            f.close()
        }, 500)
    };
    this.saveProgressWin.onProgress = function() {
        var a = Math.round(100 * f.loaded / f.total);
        100 > a ? (e.innerHTML = "Progress: " +
            a + "%", d.value = a, f.loaded += f.uploadBytePerSeconds, setTimeout(f.onProgress, 1E3)) : "progress" == f.progressState && (f.loaded -= f.uploadBytePerSeconds, setTimeout(f.onProgress, 1E3))
    };
    var g = HTML("ADVERTISING_PREMIUM_PANEL");
    CreateEventListener(g.getChildById("close_button"), "mousedown", function(a) {
        g.style.display = "none";
        TrackAnalyticsEvent("premium", "no thank you", null, "advertising")
    });
    CreateEventListener(g.getChildById("ADVERTISING_PREMIUM_LINK"), "mousedown", Bind(this, this.onClick_BuyPremium));
    this.showAboutPanel()
};
_.showApplicationContext = function(a) {
    HTML("APPLICATION").className = a
};
_.onKeyDown = function(a) {
    var b = a.srcElement ? a.srcElement : a.target;
    if ("DIV" == b.tagName && "APPLICATION" == b.id) return this.activeDocumentController.doKeyDownAction(a)
};
_.onKeyUp = function(a) {
    var b = a.srcElement ? a.srcElement : a.target;
    if ("DIV" == b.tagName && "APPLICATION" == b.id) return this.activeDocumentController.doKeyUpAction(a)
};
_.onOrientationChanged = function(a) {
    this.onBrowserResize()
};
_.onBrowserResize = function(a) {
    this.activeDocumentController.onResize(a)
};
_.onBeforeExit = function(a) {
  if (!this.documentsPanel.areAllDocumentSaved()) return "Do you really want to exit the application?"
};
_.onExit = function() {};
_.showAboutPanel = function(a) {
    var b = HTML("ABOUT_PANEL");
    b.hide = function(a) {
        b.style.display = "none";
        DeleteEventListener(document, "mousedown", b.hide, !0)
    };
    b.style.display = "block";
    CreateEventListener(document, "mousedown", b.hide, null, !0)
};
_.showAdvertisingPremiumPanel = function(a) {
    HTML("ADVERTISING_PREMIUM_PANEL").style.display = "block";
    HTML("ACCOUNT_FREE_STATUS").innerHTML = a
};
_.onClick_BuyPremium = function(a) {
    HTML("ADVERTISING_PREMIUM_PANEL").style.display = "none";
    TrackAnalyticsEvent("premium", "has clicked buy premium", null, "advertising");
    SendAndLoad("account.php", {
        action: "getPremiumAccountJWT",
        google_user_id: USER_INFO.user_id
    }, function(a) {
        a = a.target;
        4 == a.readyState && 200 == a.status && google.payments.inapp.buy({
            parameters: {},
            jwt: a.responseText,
            success: function() {
                TrackAnalyticsEvent("premium payment success", null, null, "payments");
                window.alert("Thank you and happy design.\nNow you can save your documents without limit.")
            },
            failure: function() {
                TrackAnalyticsEvent("premium payment failure", null, null, "payments");
                window.alert("The payment was cancelled.")
            }
        })
    })
};
_.createDocument = function(a, b, c, d, e) {
    
    var f = null;
    if (b) {
        if (f = this.documentsPanel.getPanelById(b)) return this.documentsPanel.setActivePanel(f), console.log("Document already open with this id - " + b), f.documentController
    } else b = "new_" + (new Date).getTime();
    (a = this.createDocumentByType(c, a, b, e)) && d && (-1 != d.indexOf("localStorage:") ? (d = d.split(":")[1], a.loadDocumentFromLocalStorage(d)) : a.loadDocumentFromDrive(d));
     // alert(d);
    return this.activeDocumentController = a
};
_.createDocumentByType = function(a, b, c, d) {
    var e = null;
    if (d) return e = CodeMirror.createDocument(this, b, c, a), this.documentsPanel.addPanel(e.htmlElement), e.onResize(), e;
    switch (a) {
        case "svg":
            e = Janvas_document2D.createDocument(this, b, c);
            this.documentsPanel.addPanel(e.htmlElement);
            e.onResize();
            e.fitToView();
            break;
        case "svg3d":
            e = Sophie3D_document3D.createDocument(this, b, c);
            this.documentsPanel.addPanel(e.htmlElement);
            e.onResize();
            break;
        case "html":
        case "htm":
        case "xml":
        case "json":
        case "js":
        case "css":
        case "php":
            e =
                CodeMirror.createDocument(this, b, c, a), this.documentsPanel.addPanel(e.htmlElement), e.onResize()
    }
    return e
};
_.printDocument = function(a) {
    var b = HTML("PRINTABLE_CONTENT");
    b.innerHTML = "";
    this.activeDocumentController.createPrintableDocument(b);
    var c = this,
        d = "mousedown";
    "chrome" == BROWSER_TYPE && (d = "mousemove");
    document.onPrintDialogClose = function(a) {
        b.innerHTML = "";
        DeleteEventListener(document, d, document.onPrintDialogClose, !0);
        "chrome" == BROWSER_TYPE && setTimeout(function() {
            c.colorPicker.htmlElement.setPosition(10, 10)
        }, 10)
    };
    setTimeout(function() {
        window.print();
        CreateEventListener(document, d, document.onPrintDialogClose,
            null, !0)
    }, 1E3)
};
_.saveGeneralFileInDrive = function(a, b, c, d) {
    var e = this;
    this.saveDocumentWin.open(a, function(a, g) {
        LoadDriveFileList("'" + g + "' in parents and title = '" + a + "' and trashed = false", function(h) {
            function k(a) {
                e.saveProgressWin.startProgress(a)
            }

            function l(a) {
                e.saveProgressWin.endProgress();
                e.driveFileBrowserPanel.updateNodeChildrenByFileId(g)
            }
            1 == h.items.length ? confirm("A file with name:'" + a + "'\n already exists in the destination folder!\nDo you want to replace it?") && UpdateDriveFile(h.items[0].id, c, b, l, d,
                k) : b instanceof File ? (console.log("UploadDriveFile _data instanceof File"), console.log(File), UploadDriveFile(b, g, l, a, k)) : InsertDriveFile(g, a, c, b, l, d, k)
        }, "id", 1);
        e.saveProgressWin.open(!0)
    })
};
_.saveDocument = function() {
    var a = this.activeDocumentController;
    a.canDocumentBeSaved() && ("new" == a.htmlElement.id.split("_")[0] ? this.saveDocumentAs() : this.updateDocumentToDrive())
};
_.saveDocumentAs = function() {
   // this.saveDocumentWin.open(this.activeDocumentController.htmlElement.title, Bind(this, this.onBeforeInsertDocumentToDrive))
 this.activeDocumentController.saveDocumentToLocalStorage("start_template_2d");
};
_.onBeforeInsertDocumentToDrive = function(a, b) {
   // console.log("onBeforeInsertDocumentToDrive");
    var c = this.activeDocumentController;
    this.getFileExtension(a) || (a += "." + c.defaultFileExtension);
    var d = this;
    LoadDriveFileList("'" + b + "' in parents and title = '" + a + "' and trashed = false", function(e) {
        1 == e.items.length ? (e = e.items[0].id, confirm("A document with name:'" + a + "'\n already exists in the destination folder!\nDo you want to replace it?") && (c.htmlElement.id = e, c.htmlElement.title = a, d.updateDocumentToDrive())) :
            d.insertDocumentToDrive(a, b)
    }, "id", 1)
};
_.getFileExtension = function(a) {
    a = a.split(".");
    return 1 == a.length ? null : a[a.length - 1]
};
_.getMimeTypeByFileName = function(a) {
    switch (this.getFileExtension(a)) {
        case "svg":
            return "image/svg+xml";
        case "svg3d":
            return "text/xml";
        case "html":
        case "htm":
            return "text/html";
        case "xml":
            return "text/xml";
        case "json":
            return "application/json";
        case "js":
            return "text/javascript";
        case "css":
            return "text/css";
        case "php":
            return "text/html"
    }
    return "text/plain"
};
_.insertDocumentToDrive = function(a, b) {
    var c = this,
        d = this.activeDocumentController,
        e = this.getFileExtension(a);
    e || (a += "." + d.defaultFileExtension);
    var f = this.getMimeTypeByFileName(a);
    EvaluateAccountAction("SAVE_DOCUMENT", function(g) {
        console.log(g);
        "YES" == g.actionResponse ? (InsertDriveFile(b, a, f, d.getSerializedDocument(e), function(a) {
                c.saveProgressWin.endProgress();
                c.driveFileBrowserPanel.updateNodeChildrenByFileId(b);
                d.onDocumentSaved(a)
            }, d.getDocumentThumbnail(), function(a) {
                c.saveProgressWin.startProgress(a)
            }),
            TrackAnalyticsEvent("save document", a), d.htmlElement.title = a, c.saveProgressWin.open(!0)) : (c.showAdvertisingPremiumPanel(g.totalSaveAction + " save action of " + g.saveActionLimit), alert("The document was not saved!\nSee the FREE ACCOUNT LIMIT."))
    })
};
_.updateDocumentToDrive = function() {
   // console.log("updateDocumentToDrive");
    var a = this,
        b = this.activeDocumentController,
        c = this.getFileExtension(b.htmlElement.title),
        d = this.getMimeTypeByFileName(b.htmlElement.title);
   // alert(d);
    console.log(b.htmlElement.id);
    updateDocumentToUserFolder(b.htmlElement.id,b.getSerializedDocument(c),b.getDocumentThumbnail(),b.htmlElement.title);
    // EvaluateAccountAction("SAVE_DOCUMENT", function(e) {
         
    //    // console.log(e);
    //     "YES" == e.actionResponse ? (UpdateDriveFile(b.htmlElement.id, d, b.getSerializedDocument(c), function(c) {
    //             a.saveProgressWin.endProgress();
    //             b.onDocumentSaved(c)
    //         }, b.getDocumentThumbnail(), function(b) {
    //             a.saveProgressWin.startProgress(b)
    //         }),
    //         TrackAnalyticsEvent("update document", b.htmlElement.title), a.saveProgressWin.open(!0)) : (a.showAdvertisingPremiumPanel(e.totalSaveAction + " save action of " + e.saveActionLimit), alert("The document was not saved!\nSee the FREE ACCOUNT LIMIT."))
    // })
};
_.chooseDocumentToOpen = function() {
    this.openDocumentWin.open(Bind(this, this.onDocumentChosen))
};
_.onDocumentChosen = function(a) {
    //alert('DCCC');
    var b = a.getAttribute("title").split(".").pop();
    this.createDocument(a.getAttribute("title"), a.getAttribute("id"), b, a.getAttribute("downloadUrl"))
};

function XOS_PropertyInspectorPanel(a, b) {
    this.htmlElement = a;
    this.appRef = b;
    this.fieldElements = [];
    this.editableObject = null;
    this.init()
}
_ = XOS_PropertyInspectorPanel.prototype;
_.init = function() {
    function a(a) {
        b.onInputPropertyChanged(this)
    }
    var b = this,
        c;
    this.fieldElements = this.htmlElement.querySelectorAll("[name]");
    for (var d = this.fieldElements.length, e = 0; e < d; e++) c = this.fieldElements[e], CreateEventListener(c, "change", a)
};
_.getValueForInputProperty = function(a) {
    return this.editableObject[a.name] ? this.editableObject[a.name] : ""
};
_.onInputPropertyChanged = function(a) {
    this.editableObject[a.name] = a.value
};
_.onBeforeUpdate = function() {};
_.update = function(a) {
    this.currentEditableGraphicObj = this.editableObject = a;
    this.onBeforeUpdate();
    for (var b, c = this.fieldElements.length, d = 0; d < c; d++) switch (a = this.fieldElements[d], a.getAttribute("name"), b = this.getValueForInputProperty(a), a.nodeName) {
        case "INPUT":
            "checkbox" == a.type ? a.checked = 1 == b ? !0 : !1 : a.value = b;
            break;
        case "SELECT":
            a.value = b;
            break;
        case "DIV":
        case "TEXTAREA":
            a.innerHTML = b;
            break;
        case "A":
            a.setAttribute("href", b);
            break;
        case "IMG":
            a.setAttribute("src", b)
    }
};

function XOS_Node() {
    this.id = this.name = this.type = "";
    this.timestamp = XOS_Node.GLOBAL_TIMESTAMP ? XOS_Node.GLOBAL_TIMESTAMP : (new Date).getTime();
    this.idi = this.timestamp + "_" + XOS_Node.GLOBAL_IDI;
    XOS_Node.GLOBAL_IDI++;
    this.parent = null;
    this.children = []
}
_ = XOS_Node.prototype;
_.numChildren = function() {
    return this.children.length
};
_.addChild = function(a) {
    this.children.push(a);
    a.parent = this;
    a.onAdded();
    return a
};
_.removeChild = function(a) {
    this.removeChildAt(this.getChildIndex(a))
};
_.removeChildAt = function(a) {
    var b = this.children[a];
    this.children.splice(a, 1);
    b.onRemoved()
};
_.removeAllChildren = function() {
    var a = this.numChildren();
    if (0 != a) {
        for (var b = 0; b < a; b++) this.children[b].onRemoved();
        this.children = []
    }
};
_.addChildAt = function(a, b) {
    a.parent = this;
    0 == this.numChildren() || b > this.children.length - 1 ? this.children.push(a) : this.children.splice(b, 0, a);
    a.onAdded();
   // console.log("addChildAt");
    //console.log(this.children);
   // console.log(a)
};
_.onAdded = function() {};
_.onRemoved = function() {};
_.getChildIndex = function(a) {
    return a.parent != this ? (alert("the child " + a.name + " is not children of " + this), -1) : this.children.indexOf(a)
};
_.getChildAt = function(a) {
    return this.children[a]
};
_.setChildIndex = function(a, b) {
    var c = this.getChildIndex(a);
    if (-1 == c || b == c || 0 > b || b > this.children.length - 1) return !1;
    this.children.splice(c, 1);
    this.children.splice(b, 0, a);
    return !0
};
_.getRoot = function() {
    return null == this.parent ? this : this.parent.getRoot()
};
_.getParentByClass = function(a) {
    return this instanceof a ? this : null != this.parent ? this.parent.getParentByClass(a) : null
};
_.getChildrenByPropertyValue = function(a, b, c) {
    var d = [],
        e = this.numChildren();
    if (c)
        for (var f = 0; f < e; f++) c = this.children[f], a in c ? c[a] == b ? d.push(c) : d = d.concat(c.getChildrenByPropertyValue(a, b, !0)) : d = d.concat(c.getChildrenByPropertyValue(a, b, !0));
    else
        for (f = 0; f < e; f++) c = this.children[f], a in c && c[a] == b && d.push(c), d = d.concat(c.getChildrenByPropertyValue(a, b, !1));
    return d
};
_.getChildByName = function(a) {
    for (var b = this.numChildren(), c = 0; c < b; c++)
        if (this.children[c].name == a) return this.children[c];
    return null
};
_.clone = function(a, b) {
    var c = new XOS_Node;
    this.clonePropertiesTo(c);
    a && a.addChild(c);
    !1 != b && this.cloneChildren(c);
    return c
};
_.clonePropertiesTo = function(a) {
    a.name = this.name;
    a.type = this.type;
    a.id = this.id
};
_.cloneChildren = function(a) {
    for (var b = this.numChildren(), c = 0; c < b; c++) this.children[c].clone(a)
};
_.getType = function() {
    return "NODE"
};
XOS_Node.GLOBAL_IDI = 0;
XOS_Node.GLOBAL_TIMESTAMP = null;
XOS_Node.CreateGlobalTimestamp = function() {
    return XOS_Node.GLOBAL_TIMESTAMP = (new Date).getTime()
};
XOS_Node.RemoveGlobalTimestamp = function() {
    XOS_Node.GLOBAL_TIMESTAMP = null
};

function XOS_History() {
    this.redoList = [];
    this.undoList = [];
    this.currentUndoID = -1
}
XOS_History.prototype.addUndoRedoActions = function(a, b) {
    this.currentUndoID < this.undoList.length - 1 && (this.undoList.splice(this.currentUndoID + 1), this.redoList.splice(this.currentUndoID + 1));
    this.undoList.push(a);
    this.redoList.push(b);
    this.currentUndoID = this.undoList.length - 1
};
XOS_History.prototype.undo = function() {
    -1 < this.currentUndoID && (this.undoList[this.currentUndoID].execute(), this.currentUndoID--)
};
XOS_History.prototype.redo = function() {
    this.currentUndoID < this.undoList.length - 1 && (this.currentUndoID++, this.redoList[this.currentUndoID].execute())
};

function XOS_Action(a) {
    this.callbackFunction = a
}
XOS_Action.prototype.execute = function() {
    this.callbackFunction()
};
String.prototype.isMail = function() {
    return "" == this || !/^.+@.+\..{2,3}$/.test(this) || this.match(/[\(\)\<\>\,\;\:\\\"\[\]]/) ? !1 : !0
};
String.prototype.isValidWord = function(a, b) {
    return "" == this || this.length < (a || 8) || this.length > (b || 30) || !/^[A-Za-z0-9\xE0\xE8\xE9\xF9\xF2\xEC ]*[A-Za-z0-9\xE0\xE8\xE9\xF9\xF2\xEC]*$/.test(this) ? !1 : !0
};
String.prototype.evalAsMathExpression = function() {
    var a = this.replace(/\s+/g, "");
    return a.match(/^[-*/+().0-9]+$/) ? eval(a) : null
};
String.toNumericValue = function(a) {
    a = a.replace(/\s+/g, "");
    a = a.trim();
    a = a.replace(",", ".");
    return a = parseFloat(a)
};
String.addSlashes = function(a) {
    return String(a).replace(/\\/g, "\\\\").replace(/\u0008/g, "\\b").replace(/\t/g, "\\t").replace(/\n/g, "\\n").replace(/\f/g, "\\f").replace(/\r/g, "\\r").replace(/'/g, "\\'").replace(/"/g, '\\"')
};
String.prototype.getIndexOfEndSpacesAtLeft = function(a) {
    for (; this[a];) switch (this[a]) {
        case " ":
            a--;
            break;
        default:
            return a
    }
};
String.prototype.getIndexOfEndSpacesAtRight = function(a) {
    for (; this[a];) switch (this[a]) {
        case " ":
            a++;
            break;
        default:
            return a
    }
};
String.prototype.getIndexOfEndWordAtLeft = function(a) {
    for (; - 1 != a;) switch (this[a]) {
        case "_":
        case "a":
        case "b":
        case "c":
        case "d":
        case "e":
        case "f":
        case "g":
        case "h":
        case "i":
        case "l":
        case "m":
        case "n":
        case "o":
        case "p":
        case "q":
        case "r":
        case "s":
        case "t":
        case "u":
        case "v":
        case "w":
        case "z":
        case "x":
        case "y":
        case "k":
        case "j":
        case "A":
        case "B":
        case "C":
        case "D":
        case "E":
        case "F":
        case "G":
        case "H":
        case "I":
        case "L":
        case "M":
        case "N":
        case "O":
        case "P":
        case "Q":
        case "R":
        case "S":
        case "T":
        case "U":
        case "V":
        case "W":
        case "Z":
        case "X":
        case "Y":
        case "K":
        case "J":
            return a;
        default:
            a--
    }
};
String.prototype.getIndexOfStartWordAtRight = function(a) {
    for (; this[a];) switch (this[a]) {
        case "_":
        case "a":
        case "b":
        case "c":
        case "d":
        case "e":
        case "f":
        case "g":
        case "h":
        case "i":
        case "l":
        case "m":
        case "n":
        case "o":
        case "p":
        case "q":
        case "r":
        case "s":
        case "t":
        case "u":
        case "v":
        case "w":
        case "z":
        case "x":
        case "y":
        case "k":
        case "j":
        case "A":
        case "B":
        case "C":
        case "D":
        case "E":
        case "F":
        case "G":
        case "H":
        case "I":
        case "L":
        case "M":
        case "N":
        case "O":
        case "P":
        case "Q":
        case "R":
        case "S":
        case "T":
        case "U":
        case "V":
        case "W":
        case "Z":
        case "X":
        case "Y":
        case "K":
        case "J":
            return a;
        default:
            a++
    }
};
String.prototype.extractWordAtLeft = function(a) {
    a = this.getIndexOfEndWordAtLeft(a);
    for (word = ""; this[a];) switch (this[a]) {
        case "_":
        case "a":
        case "b":
        case "c":
        case "d":
        case "e":
        case "f":
        case "g":
        case "h":
        case "i":
        case "l":
        case "m":
        case "n":
        case "o":
        case "p":
        case "q":
        case "r":
        case "s":
        case "t":
        case "u":
        case "v":
        case "w":
        case "z":
        case "x":
        case "y":
        case "k":
        case "j":
        case "A":
        case "B":
        case "C":
        case "D":
        case "E":
        case "F":
        case "G":
        case "H":
        case "I":
        case "L":
        case "M":
        case "N":
        case "O":
        case "P":
        case "Q":
        case "R":
        case "S":
        case "T":
        case "U":
        case "V":
        case "W":
        case "Z":
        case "X":
        case "Y":
        case "K":
        case "J":
            word = this[a] +
                word;
            a--;
            break;
        default:
            return {
                word: word,
                id: a
            }
    }
    return {
        word: word,
        id: a
    }
};
String.prototype.extractWordAtRight = function(a) {
    a = this.getIndexOfStartWordAtRight(a);
    for (word = ""; this[a];) switch (this[a]) {
        case "_":
        case "a":
        case "b":
        case "c":
        case "d":
        case "e":
        case "f":
        case "g":
        case "h":
        case "i":
        case "l":
        case "m":
        case "n":
        case "o":
        case "p":
        case "q":
        case "r":
        case "s":
        case "t":
        case "u":
        case "v":
        case "w":
        case "z":
        case "x":
        case "y":
        case "k":
        case "j":
        case "A":
        case "B":
        case "C":
        case "D":
        case "E":
        case "F":
        case "G":
        case "H":
        case "I":
        case "L":
        case "M":
        case "N":
        case "O":
        case "P":
        case "Q":
        case "R":
        case "S":
        case "T":
        case "U":
        case "V":
        case "W":
        case "Z":
        case "X":
        case "Y":
        case "K":
        case "J":
            word += this[a];
            a++;
            break;
        default:
            return {
                word: word,
                id: a
            }
    }
    return {
        word: word,
        id: a
    }
};
String.prototype.extractNumber = function(a) {
    for (var b = ""; this[a];) {
        switch (this[a]) {
            case "-":
            case "0":
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
            case ".":
                b += this[a];
                for (a++; this[a];) {
                    switch (this[a]) {
                        case "0":
                        case "1":
                        case "2":
                        case "3":
                        case "4":
                        case "5":
                        case "6":
                        case "7":
                        case "8":
                        case "9":
                        case ".":
                            b += this[a];
                            break;
                        case "e":
                            b += this[a];
                            a++;
                            b += this[a];
                            break;
                        default:
                            return {
                                number: Number(b),
                                nextId: a
                            }
                    }
                    a++
                }
                return {
                    number: Number(b),
                    nextId: a
                }
        }
        a++
    }
    return null
};
String.prototype.isValidFileName = function() {
    return /[^a-z0-9\.\s\-\_]/gi.test(this) ? !1 : !0
};
String.prototype.getAsFilePathInfo = function() {
    var a = this.split("?"),
        a = a[0].split("\\").join("/").split("/"),
        b = a.pop(),
        c = b.split("."),
        d = c[0],
        c = c.pop();
    return {
        dir: a.join("/"),
        name: b,
        nameWithoutExtension: d,
        type: c
    }
};
String.prototype.parseFloats = function() {
    for (var a = [], b = this.split(/\s+/), c = 0, d = 0, e = b.length; d < e; d++) c = parseFloat(b[d]), isNaN(c) || a.push(c);
    return a
};
String.prototype.parseIntegers = function() {
    for (var a = [], b = this.split(/\s+/), c = 0, d = 0, e = b.length; d < e; d++) c = parseInt(b[d]), isNaN(c) || a.push(c);
    return a
};

function XOS_ColorUtils() {}
XOS_ColorUtils.randColor = function(a) {
    var b = [256 * Math.random(), 256 * Math.random(), 256 * Math.random()];
    a = [51 * a, 51 * a, 51 * a];
    return "rgb(" + [b[0] + a[0], b[1] + a[1], b[2] + a[2]].map(function(a) {
        return Math.round(a / 2)
    }).join(",") + ")"
};
XOS_ColorUtils.htmlColorStringToRGBA = function(a) {
    var b = {},
        c = a.indexOf("rgb("),
        d; - 1 != c ? (c += 4, d = a.indexOf(")", c), a = a.substring(c, d).split(","), b.r = 1 / 255 * parseInt(a[0]), b.g = 1 / 255 * parseInt(a[1]), b.b = 1 / 255 * parseInt(a[2]), b.a = 1) : (c = a.indexOf("rgba("), -1 != c ? (c += 5, d = a.indexOf(")", c), a = a.substring(c, d).split(","), b.r = 1 / 255 * parseInt(a[0]), b.g = 1 / 255 * parseInt(a[1]), b.b = 1 / 255 * parseInt(a[2]), b.a = parseFloat(a[3])) : (b = XOS_ColorUtils.hexToRgb(a), b.a = 1));
    return b
};
XOS_ColorUtils.hsvToRgb = function(a, b, c) {
    var d, e, f;
    if (0 == c) f = e = d = 0;
    else {
        var g = Math.floor(6 * a),
            h = 6 * a - g;
        a = c * (1 - b);
        var k = c * (1 - b * h);
        b = c * (1 - b * (1 - h));
        switch (g) {
            case 1:
                d = k;
                e = c;
                f = a;
                break;
            case 2:
                d = a;
                e = c;
                f = b;
                break;
            case 3:
                d = a;
                e = k;
                f = c;
                break;
            case 4:
                d = b;
                e = a;
                f = c;
                break;
            case 5:
                d = c;
                e = a;
                f = k;
                break;
            case 6:
            case 0:
                d = c, e = b, f = a
        }
    }
    return {
        r: d,
        g: e,
        b: f
    }
};
XOS_ColorUtils.rgbToHsv = function(a, b, c) {
    var d = Math.max(Math.max(a, b), c),
        e = Math.min(Math.min(a, b), c);
    if (e == d) e = a = 0;
    else {
        var f = d - e,
            e = f / d;
        a = (a == d ? (b - c) / f : b == d ? 2 + (c - a) / f : 4 + (a - b) / f) / 6;
        0 > a && (a += 1);
        1 < a && (a -= 1)
    }
    return {
        h: a,
        s: e,
        v: d
    }
};
XOS_ColorUtils.hexToRgb = function(a, b) {
    void 0 == b && (b = null);
    "#" == a.substr(0, 1) && (a = a.substr(1));
    var c, d, e;
    if (3 == a.length) c = a.substr(0, 1), c += c, d = a.substr(1, 1), d += d, e = a.substr(2, 1), e += e;
    else if (6 == a.length) c = a.substr(0, 2), d = a.substr(2, 2), e = a.substr(4, 2);
    else return b;
    c = parseInt(c, 16);
    d = parseInt(d, 16);
    e = parseInt(e, 16);
    return isNaN(c) || isNaN(d) || isNaN(e) ? b : {
        r: c / 255,
        g: d / 255,
        b: e / 255
    }
};
XOS_ColorUtils.rgbToHex = function(a, b, c, d) {
    a = Math.round(255 * a);
    b = Math.round(255 * b);
    c = Math.round(255 * c);
    void 0 == d && (d = !0);
    a = a.toString(16);
    1 == a.length && (a = "0" + a);
    b = b.toString(16);
    1 == b.length && (b = "0" + b);
    c = c.toString(16);
    1 == c.length && (c = "0" + c);
    return ((d ? "#" : "") + a + b + c).toUpperCase()
};

function TrackAnalyticsEvent(a, b, c, d) {
    if (typeof(_gaq) !== 'undefined') {
    _gaq.push(["_setCustomVar", 1, "user_id", 1, 1]);
    d || (d = "actions");
    _gaq.push(["_trackEvent", d, a, b, c, !0])
    }else{

    }
    return true;
}

function EvaluateAccountAction(a, b) {
    TrackAccountEvent(a, b)
}

function TrackAccountEvent(a, b) {
    alert(a);
    alert(b);
    var c = (new Date).getTime();
    //console.log(b)
    SendAndLoad(base_url+"ant_tool_page/getHtmlFileContant/404.html", {
        action: "trackAccountEvent",
        google_user_id: 100685510263886543835,
        eventName: a
    }, function(a) {
        // alert(a.readyState);
        var e = (new Date).getTime() - c;
        3E3 < e && TrackAnalyticsEvent("slow response", "onTrackAccountEventResponse", e, "server");
        a = a.target;
        // constructorle.log(a.responseText);
        if (4 == a.readyState && 200 == a.status) {
            if (e = JSON.parse(a.responseText)) {
                b && b(e);
                return
            }
            TrackAnalyticsEvent("onTrackAccountEventResponse", "NO JSON DATA - " + a.responseText, null, "errors")
        } else TrackAnalyticsEvent("onTrackAccountEventResponse",
            "SERVER - " + a.responseText, null, "errors");
        alert("onTrackAccountEventResponse - There was an error.")
    })
};
var USER_INFO = {
    user_id: "anonymous"
};

function LoadGoogleUserInfo(a) {
    function b(b) {
        var d = (new Date).getTime() - c;
        3E3 < d && TrackAnalyticsEvent("slow response", "checkUserAccount", d, "server");
        b = b.target;
        if (4 == b.readyState && 200 == b.status) try {
            var e = JSON.parse(b.responseText);
            if (e)
                if (e.error && 1 <= e.error) TrackAnalyticsEvent("onCheckUserAccount", "ERROR NOT HANDLED - " + e.error, null, "errors");
                else {
                    USER_INFO.accountType = e.accountType;
                    a && a();
                    return
                } else TrackAnalyticsEvent("onCheckUserAccount", "NO JSON - " + b.responseText, null, "errors")
        } catch (k) {
            TrackAnalyticsEvent("onCheckUserAccount",
                "EXCEPTION - " + k + " - " + b.responseText, null, "errors")
        } else TrackAnalyticsEvent("onCheckUserAccount", "SERVER - " + b.readyState + " - " + b.status, null, "errors");
        // alert("checkUserAccount - There was an error.")
    }
    var c = (new Date).getTime(),
        d = gapi.auth.getToken(),
        e = new XMLHttpRequest;
    e.open("GET", "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=" + d.access_token);
    e.onload = function(a) {
        a = a.target;
        var d = (new Date).getTime() - c;
        3E3 < d && TrackAnalyticsEvent("slow response", "checkGoogleUserInfo", d, "server");
        if (4 == a.readyState && 200 == a.status) try {
            var e = JSON.parse(a.responseText);
            if (e) {
                if (e.user_id && e.email) {
                    USER_INFO = {
                        user_id: e.user_id
                    };
                    SendAndLoad("account.php", {
                        action: "checkUserAccount",
                        google_user_id: e.user_id,
                        email: e.email
                    }, b);
                    return
                }
                TrackAnalyticsEvent("checkGoogleUserInfo", "NO USER_ID EMAIL - " + a.responseText, null, "errors")
            } else TrackAnalyticsEvent("checkGoogleUserInfo", "NO JSON - " + a.responseText, null, "errors")
        } catch (k) {
            TrackAnalyticsEvent("checkGoogleUserInfo", "EXCEPTION - " + k + " - " + a.responseText,
                null, "errors")
        } else TrackAnalyticsEvent("checkGoogleUserInfo", "SERVER - " + a.readyState + " - " + a.status + " - " + a.responseText, null, "errors");
        alert("checkGoogleUserInfo - There was an error.")
    };
    e.send(null)
}

function CheckAuth(a) {
    function b(b) {
        var e = (new Date).getTime() - c;
        3E3 < e && TrackAnalyticsEvent("slow response", "checkLoginAuthResponse", e, "server");
        b && !b.error ? LoadGoogleUserInfo(a) : TrackAnalyticsEvent("checkLoginAuthResponse", "UNKNOWED", null, "errors")
    }
    var c = (new Date).getTime();
    gapi.auth.authorize({
        client_id: CLIENT_ID,
        scope: SCOPES,
        immediate: !0
    }, function(d) {
        if (d && !d.error) d = (new Date).getTime() - c, 3E3 < d && TrackAnalyticsEvent("slow response", "checkImmediateAuthResponse", d, "server"), LoadGoogleUserInfo(a);
        else {
            var e = HTML("AUTH_REQUEST_PANEL");
            e.style.display = "none";
            e.getChildById("authorize_button").onclick = function(a) {
                e.style.display = "none";
                gapi.auth.authorize({
                    client_id: CLIENT_ID,
                    scope: SCOPES,
                    immediate: !1
                }, b);
                TrackAnalyticsEvent("google drive access", "ok", null, "feedback")
            }
        }
    })
}

function CheckSessionState(a) {
    var b = gapi.auth.getToken();
    if (b) {
        var c = new XMLHttpRequest;
        c.open("GET", "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=" + b.access_token);
        c.onload = function(b) {
            b = JSON.parse(b.target.responseText);
            b.error ? "invalid_token" == b.error && CheckAuth(a) : a()
        };
        c.send()
    } else CheckAuth(a)
};

function base64encode(a) {
    return btoa(unescape(encodeURIComponent(a)))
}

function base64decode(a) {
    return decodeURIComponent(escape(atob(a)))
}

function Base64URLSafeString(a) {
    return a.replace(/\+/g, "-").replace(/\//g, "_").replace(/=+$/, "")
}

function Base64URLUnsafeString(a) {
    for (a = a.replace(/-/g, "+").replace(/_/g, "/"); a.length % 4;) a += "=";
    return a
}

function InsertDriveFile(a, b, c, d, e, f, g) {
    CheckSessionState(function() {
        var h = "";
        if (f) {
            var h = f.indexOf(","),
                k = f.substring(0, h - 7).split(":")[1];
            f = f.substr(h + 1);
            f = Base64URLSafeString(f);
            h = {
                title: b,
                mimeType: c,
                parents: [{
                    id: a
                }],
                thumbnail: {
                    image: f,
                    mimeType: k
                }
            }
        } else h = {
            title: b,
            mimeType: c,
            parents: [{
                id: a
            }]
        };
        k = base64encode(d);
        g && g(k.length);
        h = "\r\n---------314159265358979323846\r\nContent-Type: application/json\r\n\r\n" + JSON.stringify(h) + "\r\n---------314159265358979323846\r\nContent-Type: " + c + "\r\nContent-Transfer-Encoding: base64\r\n\r\n" +
            k + "\r\n---------314159265358979323846--";
        gapi.client.request({
            path: "/upload/drive/v2/files",
            method: "POST",
            params: {
                uploadType: "multipart"
            },
            headers: {
                "Content-Type": 'multipart/mixed; boundary="-------314159265358979323846"'
            },
            body: h
        }).execute(function(a) {
            a.error ? console.log("error: " + a.error.code) : e(a)
        })
    })
}

function UpdateDriveFile(a, b, c, d, e, f) {
    CheckSessionState(function() {
        if (e) {
            var g = e.indexOf(","),
                h = e.substring(0, g - 7).split(":")[1];
            e = e.substr(g + 1);
            e = Base64URLSafeString(e);
            metadata = {
                fileId: a,
                setModifiedDate: !0,
                thumbnail: {
                    image: e,
                    mimeType: h
                }
            }
        } else metadata = {
            fileId: a,
            setModifiedDate: !0
        };
        g = base64encode(c);
        f && f(g.length);
        g = "\r\n---------314159265358979323846\r\nContent-Type: application/json\r\n\r\n" + JSON.stringify(metadata) + "\r\n---------314159265358979323846\r\nContent-Type: " + b + "\r\nContent-Transfer-Encoding: base64\r\n\r\n" +
            g + "\r\n---------314159265358979323846--";
        gapi.client.request({
            path: "/upload/drive/v2/files/" + a,
            method: "PUT",
            params: {
                uploadType: "multipart"
            },
            headers: {
                "Content-Type": 'multipart/mixed; boundary="-------314159265358979323846"'
            },
            body: g
        }).execute(function(a) {
            a.error ? console.log("error: " + a.error.code) : d(a)
        })
    })
}

function UploadDriveFile(a, b, c, d, e) {
    CheckSessionState(function() {
        var f = new FileReader;
        f.readAsBinaryString(a);
        f.onload = function(g) {
            g = a.type || "application/octet-stream";
            var h = {
                    title: d || a.name,
                    mimeType: g,
                    parents: [{
                        id: b
                    }]
                },
                k = btoa(f.result);
            e && e(k.length);
            g = "\r\n---------314159265358979323846\r\nContent-Type: application/json\r\n\r\n" + JSON.stringify(h) + "\r\n---------314159265358979323846\r\nContent-Type: " + g + "\r\nContent-Transfer-Encoding: base64\r\n\r\n" + k + "\r\n---------314159265358979323846--";
            g = gapi.client.request({
                path: "/upload/drive/v2/files",
                method: "POST",
                params: {
                    uploadType: "multipart"
                },
                headers: {
                    "Content-Type": 'multipart/mixed; boundary="-------314159265358979323846"'
                },
                body: g
            });
            c || (c = function(a) {});
            g.execute(c)
        }
    })
}

function TrashDriveFile(a, b) {
    CheckSessionState(function() {
        gapi.client.drive.files.trash({
            fileId: a
        }).execute(b)
    })
}

function RenameDriveFile(a, b, c) {
    CheckSessionState(function() {
        gapi.client.request({
            path: "/drive/v2/files/" + a,
            method: "PUT",
            body: JSON.stringify({
                fileId: a,
                title: b
            })
        }).execute(c)
    })
}

function CreateDrivePublicFolder(a, b, c) {
    CheckSessionState(function() {
        gapi.client.drive.files.insert({
            resource: {
                title: a,
                mimeType: "application/vnd.google-apps.folder",
                parents: [{
                    id: b
                }]
            }
        }).execute(function(a) {
            gapi.client.drive.permissions.insert({
                fileId: a.id,
                resource: {
                    value: "",
                    type: "anyone",
                    role: "reader"
                }
            }).execute(c)
        })
    })
}

function LoadDriveFile(a, b, c) {
    //return 'mydata';
    //alert(c);
    CheckSessionState(function() {
        var d = gapi.auth.getToken().access_token,
            e = new XMLHttpRequest;
        e.open("GET", a);
        e.setRequestHeader("Authorization", "Bearer " + d);
        e.onload = function(a) {
            403 == a.target.status ? alert("The file can not be open!\n The file url can be expired.\nOpen and close the parent folder to update the inner filelist.") : b(a)
        };
        c && (e.onprogress = c);
        e.onerror = function(a) {
            alert("The file can not be open!\nThere was an error.");
            //console.log("LoadDriveFile error");
           // console.log(a)
        };
        e.send()
    })
}

function LoadDriveFileList(a, b, c, d) {
   
    CheckSessionState(function() {
        c || (c = "mimeType,iconLink,thumbnailLink,id,title,downloadUrl,webContentLink,webViewLink");
        d || (d = 1E3);
        var e = {
            maxResults: d,
            fields: "items(" + c + ")"
        };
        e.q = a;
        gapi.client.request({
            path: "/drive/v2/files",
            method: "GET",
            params: e
        }).execute(function(a) {
            a.error ? (console.log("onLoadDriveFileListResponse ERROR"), console.log("error: " + a.error.code)) : b(a)
        })
    })
};
ExtendClass(DriveFileBrowser_panel, XOS_TreeView);

function DriveFileBrowser_panel(a, b) {
    DriveFileBrowser_panel.baseConstructor.call(this, a);
    this.appRef = b;
    this.isLocked = !1;
    this.executableFilePath = "";
    this.executableFileWindow = null;
    this.noficationServerUrl = "http://www.replaceWithYourDomain.com/yourNotificationPage.php?KEY=YOUR_KEY";
    this.initActionElements()
}
_ = DriveFileBrowser_panel.prototype;
_.initActionElements = function() {
    var a = this.htmlViewElement.parentNode;
    this.loadingState = a.getChildById("LOADING_ICON");
    CreateEventListener(this.htmlViewElement, "dragover", function(a) {
        a.stopPropagation();
        a.preventDefault();
        return !1
    });
    var b = this;
    CreateEventListener(a.getChildById("RENAME_FILE_button"), "mousedown", this.editSelectedNodeName, this);
    CreateEventListener(a.getChildById("REMOVE_FILE_button"), "mousedown", this.removeFile, this);
    CreateEventListener(a.getChildById("DOWNLOAD_FILE_button"), "mousedown",
        this.downloadFile, this);
    CreateEventListener(a.getChildById("LOADING_ICON"), "mousedown", this.reloadSelectedNode, this);
    document.querySelector("#UPLOAD_FILE_button #UPLOAD_FILE_TEXTFIELD").addEventListener("change", function(a) {
        a = b.getSelecteNodes();
        var d = null;
        0 < a.length && (a = a[0], d = "dir" != a.getAttribute("itemType") ? a.getElementOrParentByAttribute("itemType", "dir") : a);
        b.uploadFileList(this.files, d)
    }, !1);
    CreateEventListener(a.getChildById("NEW_FOLDER_button"), "mousedown", this.createDir, this);
    CreateEventListener(a.getChildById("NEW_SHARED_WEB_FOLDER_button"),
        "mousedown", this.createPublicDir, this);
    a.getChildById("CREATE_FILE_XML_menuItem") && CreateEventListener(a.getChildById("CREATE_FILE_XML_menuItem"), "mousedown", this.createNewFile, this);
    a.getChildById("CREATE_FILE_JS_menuItem") && CreateEventListener(a.getChildById("CREATE_FILE_JS_menuItem"), "mousedown", this.createNewFile, this);
    a.getChildById("CREATE_FILE_PHP_menuItem") && CreateEventListener(a.getChildById("CREATE_FILE_PHP_menuItem"), "mousedown", this.createNewFile, this);
    a.getChildById("CREATE_FILE_JSON_menuItem") &&
        CreateEventListener(a.getChildById("CREATE_FILE_JSON_menuItem"), "mousedown", this.createNewFile, this);
    a.getChildById("CREATE_FILE_CSS_menuItem") && CreateEventListener(a.getChildById("CREATE_FILE_CSS_menuItem"), "mousedown", this.createNewFile, this);
        a.getChildById("CREATE_FILE_HTML_menuItem") && CreateEventListener(a.getChildById("CREATE_FILE_HTML_menuItem"), "mousedown", this.createNewFile, this);
    CreateEventListener(a.getChildById("OPEN_AS_WEB_PAGE_button"), "mousedown", this.openFileAsWebPage, this);
    a.getChildById("NOTIFICATION_SERVER_menuItem") &&
        (CreateEventListener(a.getChildById("NOTIFICATION_SERVER_menuItem"), "mousedown", this.setNotificationServer, this), CreateEventListener(a.getChildById("SEND_NOTIFICATION_menuItem"), "mousedown", this.sendNotification, this))
};
_.onDoubleClickOnNodeName = function(a) {
    var b = a.target.parentNode;
    //console.log(b);
    // alert(b);
    if (b.hasAttribute("itemType") && "dir" != b.getAttribute("itemType")) {
        //alert(this.getNodeFileName(b));

        var c = PathInfo(this.getNodeFileName(b));
        // alert(c.fileExtension);
        this.appRef.createDocument(c.fileName, b.getAttribute("id"), c.fileExtension, this.getNodePath(b), a.altKey)
    }
};
_.expandNode = function(a) {
    void 0 != DriveFileBrowser_panel.superClass.expandNode.call(this, a) && this.isDirectoryNode(a) && this.loadNodeChildren(a)
};
_.showLoadingState = function() {
    this.loadingState && this.loadingState.classList.add("loading")
};
_.hideLoadingState = function() {
    this.loadingState && this.loadingState.classList.remove("loading")
};
_.getNodeByFileId = function(a) {
    return this.htmlViewElement.querySelector('[id="' + a + '"]')
};
_.updateNodeChildrenByFileId = function(a) {
    (a = this.getNodeByFileId(a)) && this.loadNodeChildren(a)
};
_.reloadSelectedNode = function() {
    var a = null;
    0 < this.selectedNodes.length ? (a = this.selectedNodes[0], "dir" != a.getAttribute("itemType") && (a = a.getElementOrParentByAttribute("itemType", "dir")), this.loadNodeChildren(a)) : this.loadFileList(null, null)
};
_.loadNodeChildren = function(a) {
    this.showLoadingState();
    var b = null,
        c = null;
    a && (b = this.getNodeElement_ul(a), c = a.getAttribute("id"));
    this.loadFileList(c, b)
};
_.loadFileList = function(a, b) {
    this.showLoadingState();
    a || (a = "root", b = this.htmlViewElement);
    var c = this;
    b && (b.innerHTML = "");
    LoadDriveFileList("'" + a + "' in parents and trashed = false", function(a) {
        c.onLoadFileList(a, b)
    })
};
_.onLoadFileList = function(a, b) {
    function c(a, b) {
        return a.title < b.title ? -1 : a.title > b.title ? 1 : 0
    }
    this.hideLoadingState();
    b || (b = this.htmlViewElement);
    b.innerHTML = "";
    if (a.items) {
        a.items.sort(c);
        for (var d, e, f = 0; f < a.items.length; f++) d = a.items[f], "application/vnd.google-apps.folder" == d.mimeType ? (e = "dir", d.webViewLink && (e = "sharedWebDir"), e = this.createTreeViewNode(e + "_icon16x16", d.title, d.downloadUrl, !0), e.setAttribute("itemType", "dir"), d.webViewLink && e.setAttribute("webViewLink", d.webViewLink)) : (d.fileExtension =
            PathInfo(d.title).fileExtension, e = this.createTreeViewNode(d.fileExtension + "_icon16x16", d.title, null, !1), e.setAttribute("itemType", d.fileExtension), e.setAttribute("downloadUrl", d.downloadUrl), d.webContentLink && e.setAttribute("webContentLink", d.webContentLink)), e.setAttribute("id", d.id), e.draggable = !0, e.ondragover = Bind(this, this.onDragOverNode), e.ondragleave = Bind(this, this.onDragLeaveNode), e.ondragstart = Bind(this, this.onDragStartNode), e.ondrop = Bind(this, this.onDropOnNode), b.appendChild(e)
    }
};
_.onEndEditNodeName = function(a) {
    DriveFileBrowser_panel.superClass.onEndEditNodeName.call(this, a);
    var b = a.parentElement.parentElement;
    if ("LI" == b.nodeName) {
        var c = a.value;
        this.isValidFileName(c) ? b.hasAttribute("itemType") && this.renameFile(c, b, a.previousValue) : b.getElementsByTagName("div")[0].innerHTML = a.previousValue
     console.log("youtube");
    }
};
_.renameFile = function(a, b, c) {
    this.isLocked = !0;
    var d = b.getAttribute("id"),
        e = this;
    RenameDriveFile(d, a, function(f) {
        e.hideLoadingState();
        e.isLocked = !1;
        var g = b.getElementsByTagName("div")[0];
        if (!f.title) g.innerHTML = c;
        else if ("dir" != b.getAttribute("itemType")) {
            f = a.split(".");
            var h = "";
            0 < f.length && (h = f[f.length - 1] + "_icon16x16");
            g.className = h;
            if (g = e.appRef.documentsPanel.getPanelById(d)) g.setAttribute("title", a), e.appRef.documentsPanel.updateStates(g)
        }
    });
    this.showLoadingState()
};
_.isValidFileName = function(a) {
    return /[^a-z0-9\.\s\-\_]/gi.test(a) ? !1 : !0
};
_.getNodeFileType = function(a) {
    return a && a.hasAttribute("itemType") ? a.getAttribute("itemType") : null
};
_.isDirectoryNode = function(a) {
    return "dir" == this.getNodeFileType(a) ? !0 : !1
};
_.isFileNode = function(a) {
    return "dir" == this.getNodeFileType(a) ? !1 : !0
};
_.getNodeFileName = function(a) {
    return a.getElementsByTagName("div")[0].innerHTML
};
_.getNodePath = function(a) {
    return a.getAttribute("downloadUrl")
};
_.getNodeLocalPath = function(a) {
    var b = "",
        c = [this.getNodeFileName(a)];
    if ("dir" != a.getAttribute("itemType")) {
        a.hasAttribute("webContentLink") && (b = a.getAttribute("webContentLink"));
        for (a = a.getElementOrParentByAttribute("itemType", "dir"); a;) {
            if (a.hasAttribute("webViewLink")) b = a.getAttribute("webViewLink"), c.push(this.getNodeFileName(a));
            else break;
            a = a.parentNode.getElementOrParentByAttribute("itemType", "dir")
        }
        c.reverse();
        c.shift();
        b += c.join("/")
    } else a.hasAttribute("webViewLink") && (b = a.getAttribute("webViewLink"));
    return b
};
_.setNotificationServer = function(a) {
    a = prompt("Notification Server url and key", this.noficationServerUrl);
    null != a && (this.noficationServerUrl = a)
};
_.sendNotification = function() {
    var a = this.getNodeLocalPath(this.selectedNodes[0]).replace("https://www.googledrive.com/host/", "");
    this.appRef.notificationWin ? (this.appRef.notificationIframe.setAttribute("src", this.noficationServerUrl + "?path=" + a), this.appRef.notificationWin.open(!0)) : window.open(this.noficationServerUrl + "?path=" + a, "")
};
_.createDir = function(a) {
    a = "root";
    var b = null;
    if (0 < this.selectedNodes.length) var c = this.selectedNodes[0],
        b = "dir" != c.getAttribute("itemType") ? c.getElementOrParentByAttribute("itemType", "dir") : c;
    b && (a = b.getAttribute("id"));
    c = prompt("New Folder name", "new folder");
    if (null != c && "" != c) {
        var d = this;
        InsertDriveFile(a, c, "application/vnd.google-apps.folder", "", function(a) {
            d.hideLoadingState();
            d.loadNodeChildren(b)
        });
        this.showLoadingState()
    }
};
_.createPublicDir = function(a) {
    a = "root";
    var b = null;
    if (0 < this.selectedNodes.length) var c = this.selectedNodes[0],
        b = "dir" != c.getAttribute("itemType") ? c.getElementOrParentByAttribute("itemType", "dir") : c;
    b && (a = b.getAttribute("id"));
    var d = this,
        c = prompt("New Folder name", "new folder");
    null != c && "" != c && (CreateDrivePublicFolder(c, a, function(a) {
        d.hideLoadingState();
        d.loadNodeChildren(b)
    }), this.showLoadingState())
};
_.createNewFile = function(a) {
    var b = "root",
        c = null;
    if (0 < this.selectedNodes.length) var d = this.selectedNodes[0],
        c = "dir" != d.getAttribute("itemType") ? d.getElementOrParentByAttribute("itemType", "dir") : d;
    c && (b = c.getAttribute("id"));
    d = a.target;
    if (d.hasAttribute("fileExtension")) {
        a = d.getAttribute("fileExtension");
        var d = d.getAttribute("fileMimeType"),
            e = this;
        a = prompt("New File " + a, "Home." + a);
        null != a && "" != a && (InsertDriveFile(b, a, d, "EDIT ME", function(a) {
            e.hideLoadingState();
            e.loadNodeChildren(c)
        }), this.showLoadingState())
    }
};
_.removeFile = function(a) {
    function b(a) {
        this.hideLoadingState();
        c.parentNode.removeChild(c)
    }
    if (0 != this.selectedNodes.length) {
        var c = this.selectedNodes[0];
        a = c.getAttribute("id");
        var d = this.appRef.documentsPanel.getPanelById(a);
        d && d.documentController.isChanged ? alert("The document you want to remove is opened.\nClose the document before trash it!") : (d = this.getNodeFileName(c), confirm("Are you sure you want to remove:\n\n" + d) && (TrashDriveFile(a, Bind(this, b)), this.showLoadingState()))
    }
};
_.downloadFile = function(a) {
    0 != this.selectedNodes.length && (a = this.selectedNodes[0], "dir" != a.getAttribute("itemType") && a.hasAttribute("webContentLink") && window.open(a.getAttribute("webContentLink"), ""))
};
_.openFileAsWebPage = function(a) {
    0 != this.selectedNodes.length && window.open(this.getNodeLocalPath(this.selectedNodes[0]), "")
};
_.openFileInDeviceSimulator = function(a) {
    0 != this.selectedNodes.length && window.open("https://www.googledrive.com/host/0BwRlR3z6e0egZWdCUFV2bWJ4WU0/device_simulator.html?urlContent=" + this.getNodeLocalPath(this.selectedNodes[0]), "")
};
_.copyFile = function(a) {};
_.pasteFile = function(a) {};
_.uploadFileList = function(a, b) {
    for (var c, d = a.length, e = 0; e < d; e++) c = a[e], this.uploadFile(c, c.name, b)
};
_.uploadFile = function(a, b, c) {
    var d = "root";
    c && (d = c.getAttribute("id"));
    var e = this;
    LoadDriveFileList("'" + d + "' in parents and title = '" + b + "' and trashed = false", function(f) {
        if (1 != f.items.length || confirm("A document with name:'" + b + "'\n already exists in the destination folder!\nDo you want to replace it?")) {
            var g = e.createTreeViewNode("upload_progress_icon16x16", b, null, !1);
            f = e.panelView;
            c && (f = e.getNodeElement_ul(c));
            f.firstChild ? f.insertBefore(g, f.firstChild) : f.appendChild(g);
            g.draggable = !0;
            g.ondragover =
                Bind(e, e.onDragOverNode);
            g.ondragleave = Bind(e, e.onDragLeaveNode);
            g.ondragstart = Bind(e, e.onDragStartNode);
            g.ondrop = Bind(e, e.onDropOnNode);
            UploadDriveFile(a, d, function(a) {
                g.getElementsByTagName("div")[0].setAttribute("class", a.fileExtension + "_icon16x16");
                g.setAttribute("itemType", a.fileExtension);
                g.setAttribute("downloadUrl", a.downloadUrl);
                g.setAttribute("webContentLink", a.webContentLink);
                g.setAttribute("id", a.id)
            })
        }
    }, "id", 1)
};
_.onDropOnNode = function(a) {
    a.stopPropagation();
    a.preventDefault();
    var b;
    b = "dir" != a.target.getAttribute("itemType") ? a.target.getElementOrParentByAttribute("itemType", "dir") : a.target;
    this.onDragLeaveNode(a);
    0 < a.dataTransfer.files.length ? this.uploadFileList(a.dataTransfer.files, b) : (a = GetTransferData()) && this.moveFile(a, b)
};
_.onDragOverNode = function(a) {
    (a = a.target.getElementOrParentByAttribute("itemType", "dir")) && a.classList.add(this.dragOverNodeClassName)
};
_.onDragLeaveNode = function(a) {
    (a = a.target.getElementOrParentByAttribute("itemType", "dir")) && a.classList.remove(this.dragOverNodeClassName)
};
_.onDragStartNode = function(a) {
    a.dataTransfer.setData("text/plain", " ");
    a.target.driveFileBrowserRef = this;
    SetTransferData(a.target);
    a.altKey || (a.dataTransfer.effectAllowed = "move");
    a.stopPropagation()
};
_.duplicateFile = function(a, b) {};
_.moveFile = function(a, b) {
    function c(c) {
        c.error ? alert("Error on file move!") : (console.log(c), a.parentNode.removeChild(a), b.classList.contains("expanded") && d.loadNodeChildren(b));
        d.hideLoadingState()
    }
    if (a != b) {
        var d = this;
        gapi.client.drive.files.patch({
            fileId: a.id,
            resource: {
                parents: [{
                    id: b.id
                }]
            }
        }).execute(c);
        this.showLoadingState()
    }
};

function DriveFiles_view(a, b) {
    this.htmlElement = a;
    this.folderPaths = [];
    this.appendFolderPath("root", "Google Drive");
    this.selectionList = [];
    this.appRef = b;
    CreateEventListener(this.htmlElement, "mouseup", this.onMouseClick, this);
    this.htmlElement.enableScrollingWithTouchMove();
    this.eventDispatcher = new XOS_EventDispatcher
}
_ = DriveFiles_view.prototype;
DriveFiles_view.ON_LOAD_FILE_LIST = "ON_LOAD_FILE_LIST";
DriveFiles_view.ON_SELECT_FILE = "ON_SELECT_FILE";
DriveFiles_view.ON_DESELECT_ALL_FILES = "ON_DESELECT_ALL_FILES";
_.addEventListener = function(a, b) {
    this.eventDispatcher.addEventListener(a, b)
};
_.removeEventListener = function(a, b) {
    this.eventDispatcher.removeEventListener(a, b)
};
_.showLoadingState = function() {
    this.htmlElement.innerHTML = "";
    var a = document.createElement("div");
    a.setAttribute("id", "PROGRESS_STATE");
    this.htmlElement.appendChild(a)
};
_.hideLoadingState = function() {};
_.appendFolderPath = function(a, b) {
    this.folderPaths.push({
        id: a,
        title: b
    })
};
_.popFolderPath = function() {
    1 != this.folderPaths.length && this.folderPaths.pop()
};
_.getCurrentFolderPath = function() {
    return this.folderPaths[this.folderPaths.length - 1]
};
_.getCurrentFolderPathAsString = function() {
    for (var a = [], b = this.folderPaths.length, c = 0; c < b; c++) a.push(this.folderPaths[c].title);
    return a.join("/")
};
_.onMouseClick = function(a) {
    a = a.srcElement ? a.srcElement : a.target;
    var b = null;
    a.parentNode instanceof HTMLLIElement ? b = a.parentNode : a.parentNode.parentNode instanceof HTMLLIElement && (b = a.parentNode.parentNode);
    b ? this.selectFile(b) : this.deselectAllFiles()
};
_.selectFile = function(a) {
    this.deselectAllFiles();
    if ("dir" == a.getAttribute("itemType")) {
        var b = a.getAttribute("id");
        this.appendFolderPath(b, a.childNodes[1].innerHTML);
        this.loadCurrentFolderChildren()
    } else this.selectionList.push(a), a.className = "selectedItem";
    this.eventDispatcher.dispatch({
        type: DriveFiles_view.ON_SELECT_FILE,
        target: a
    })
};
_.deselectAllFiles = function() {
    for (var a = this.selectionList.length, b = 0; b < a; b++) this.selectionList[b].className = "unselectedItem";
    this.selectionList = [];
    this.eventDispatcher.dispatch(DriveFiles_view.ON_DESELECT_ALL_FILES)
};
_.loadParentFolderChildren = function() {
    this.popFolderPath();
    this.loadCurrentFolderChildren()
};
_.loadCurrentFolderChildren = function() {
    var a = this.getCurrentFolderPath().id;
    this.loadFileList(a)
};
_.loadFileList = function(a) {
    this.showLoadingState();
    LoadDriveFileList("'" + a + "' in parents and trashed = false", Bind(this, this.onLoadFileList))
};
_.onLoadFileList = function(a) {
    function b(a, b) {
        return a.title < b.title ? -1 : a.title > b.title ? 1 : 0
    }
    this.htmlElement.innerHTML = "";
    this.selectionList = [];
    if (a.items) {
        a.items.sort(b);
        for (var c, d, e, f, g = 0; g < a.items.length; g++) c = a.items[g], "application/vnd.google-apps.folder" == c.mimeType ? (d = "dir", e = "", f = "thumbnail dir", c.webViewLink && (f = "thumbnail sharedDir")) : (d = "file", c.thumbnailLink ? (e = c.thumbnailLink, f = "thumbnail") : (e = c.iconLink, f = "icon")), c = this.createFileListItem(c.id, c.downloadUrl, c.title, d, c.mimeType, e,
            f), this.htmlElement.appendChild(c)
    }
};
_.createFileListItem = function(a, b, c, d, e, f, g) {
    e = document.createElement("li");
    e.classList.add("unselectedItem");
    e.setAttribute("title", c);
    e.setAttribute("id", a);
    e.setAttribute("itemType", d);
    e.setAttribute("downloadUrl", b);
    a = document.createElement("div");
    a.setAttribute("id", "PREVIEW_CONTAINER");
    a.className = g;
    "" != f && (a.style.backgroundImage = "url('" + f + "')");
    e.appendChild(a);
    a = document.createElement("div");
    a.appendChild(document.createTextNode(c));
    e.appendChild(a);
    return e
};
ExtendClass(DriveOpenDocument_window, XOS_Window);

function DriveOpenDocument_window(a, b) {
    DriveOpenDocument_window.baseConstructor.call(this, a);
    this.appRef = b;
    this.fileView = new DriveFiles_view(this.htmlElement.getChildById("FILES_VIEW"));
    this.fileView.addEventListener(DriveFiles_view.ON_SELECT_FILE, Bind(this, this.onSelectFile));
    this.fileView.addEventListener(DriveFiles_view.ON_DESELECT_ALL_FILES, Bind(this, this.onDeselectAllFiles));
    this.dirUpButton = this.htmlElement.getChildById("DIR_UP_BUTTON");
    this.selectedDocumentTitle = this.htmlElement.getChildById("SELECTED_DOCUMENT_TITLE");
    this.openButton = this.htmlElement.getChildById("OPEN_BUTTON");
    this.cancelButton = this.htmlElement.getChildById("CANCEL_BUTTON");
    this.openDocumentCallBackFunction = null;
    CreateEventListener(this.dirUpButton, "mouseup", this.onDirUpButtonClick, this);
    CreateEventListener(this.cancelButton, "mouseup", this.onCancelButtonClick, this);
    CreateEventListener(this.openButton, "mouseup", this.onOpenButtonClick, this)
}
_ = DriveOpenDocument_window.prototype;
_.open = function(a) {
    DriveOpenDocument_window.superClass.open.call(this, !0);
    this.openDocumentCallBackFunction = a;
    this.fileView.loadCurrentFolderChildren();
    this.selectedDocumentTitle.innerHTML = "Select a file to open"
};
_.onDirUpButtonClick = function(a) {
    this.selectedDocumentTitle.innerHTML = "Select a file to open";
    this.fileView.loadParentFolderChildren();
    this.setTitle(this.fileView.getCurrentFolderPathAsString())
};
_.onCancelButtonClick = function(a) {
    this.close();
    this.openButton.disabled = !0
};
_.onOpenButtonClick = function(a) {
    // alert('dfdsf');
    0 != this.fileView.selectionList.length && (this.openDocumentCallBackFunction(this.fileView.selectionList[0]), this.openButton.disabled = !0, this.close())
};
_.onSelectFile = function(a) {
    a = a.target;
    "dir" == a.getAttribute("itemType") ? (this.setTitle(this.fileView.getCurrentFolderPathAsString()), this.selectedDocumentTitle.innerHTML = "Select a file to open") : (this.selectedDocumentTitle.innerHTML = a.getAttribute("title"), this.openButton.disabled = !1)
};
_.onDeselectAllFiles = function() {
    this.selectedDocumentTitle.innerHTML = "Select a file to open";
    this.openButton.disabled = !0
};
ExtendClass(DriveSaveDocument_window, XOS_Window);

function DriveSaveDocument_window(a, b) {
    DriveSaveDocument_window.baseConstructor.call(this, a);
    this.appRef = b;
    this.fileView = new DriveFiles_view(this.htmlElement.getChildById("FILES_VIEW"));
    this.fileView.addEventListener(DriveFiles_view.ON_SELECT_FILE, Bind(this, this.onSelectFile));
    this.inputFileName = this.htmlElement.getChildById("INPUT_FILE_NAME");
    this.saveButton = this.htmlElement.getChildById("SAVE_BUTTON");
    this.cancelButton = this.htmlElement.getChildById("CANCEL_BUTTON");
    this.dirUpButton = this.htmlElement.getChildById("DIR_UP_BUTTON");
    this.createDirButton = this.htmlElement.getChildById("CREATE_DIR_BUTTON");
    this.saveDocumentCallBackFunction = null;
    CreateEventListener(this.saveButton, "mouseup", this.onSaveButtonClick, this);
    CreateEventListener(this.cancelButton, "mouseup", this.onCancelButtonClick, this);
    CreateEventListener(this.dirUpButton, "mouseup", this.onDirUpButtonClick, this);
    CreateEventListener(this.createDirButton, "mouseup", this.onCreateDirButtonClick, this)
}
_ = DriveSaveDocument_window.prototype;
_.onCreateDirButtonClick = function(a) {
    a = prompt("New Folder name", "new folder");
    if (null != a) {
        var b = this;
        InsertDriveFile(this.fileView.getCurrentFolderPath().id, a, "application/vnd.google-apps.folder", "", function(a) {
            b.fileView.loadCurrentFolderChildren()
        })
    }
};
_.onCancelButtonClick = function(a) {
    this.close()
};
_.onSaveButtonClick = function(a) {
    this.saveDocumentCallBackFunction(this.inputFileName.value.trim(), this.fileView.getCurrentFolderPath().id);
    this.close()
};
_.onDirUpButtonClick = function(a) {
    this.fileView.loadParentFolderChildren();
    this.setTitle(this.fileView.getCurrentFolderPathAsString())
};
_.open = function(a, b) {
    DriveSaveDocument_window.superClass.open.call(this, !0);
    this.inputFileName.value = a;
    this.saveDocumentCallBackFunction = b;
    this.fileView.loadCurrentFolderChildren();
    this.inputFileName.focus()
};
_.onSelectFile = function(a) {
    a = a.target;
    "dir" == a.getAttribute("itemType") ? this.setTitle(this.fileView.getCurrentFolderPathAsString()) : (this.inputFileName.value = a.childNodes[1].innerHTML, this.idDocumentToSave = a.getAttribute("id"))
};
var Numerical = new function() {
    return {
        TOLERANCE: 1E-5,
        EPSILON: 1E-11
    }
};

function Segment(a, b, c, d, e, f) {
    this.point = new XOS_Point2D(0, 0);
    this.handleIn = new XOS_Point2D(0, 0);
    this.handleOut = new XOS_Point2D(0, 0);
    var g = arguments.length;
    0 != g && (1 == g ? a.point ? (this.point = a.point.clone(), this.handleIn = a.handleIn.clone(), this.handleOut = a.handleOut.clone()) : this.point = a.clone() : 6 > g ? 2 == g && void 0 === b.x ? this.point = new XOS_Point2D(a, b) : (this.point = a.clone(), this.handleIn = b.clone()) : 6 == g && (this.point = new XOS_Point2D(a, b), this.handleIn = new XOS_Point2D(c, d), this.handleOut = new XOS_Point2D(e,
        f)))
}
Segment.prototype.setHandleOut = function(a) {
    this.handleOut = new XOS_Point2D(a.x, a.y)
};

function PathSmoother(a, b) {
    this.segments = [];
    this.points = [];
    for (var c, d = a.length, e = 0; e < d; e++) {
        var f = a[e].clone();
        c && c.equals(f) || (this.points.push(f), c = f)
    }
    this.error = b
}
_ = PathSmoother.prototype;
_.fit = function() {
    this.segments.push(new Segment(this.points[0]));
    this.fitCubic(0, this.points.length - 1, this.points[1].subtract(this.points[0]).normalize(), this.points[this.points.length - 2].subtract(this.points[this.points.length - 1]).normalize());
    return this.segmentsToPath()
};
_.fitCubic = function(a, b, c, d) {
    if (1 == b - a) {
        a = this.points[a];
        b = this.points[b];
        var e = a.getDistance(b) / 3;
        this.addCurve([a, a.add(c.normalize(e)), b.add(d.normalize(e)), b])
    } else {
        for (var f = this.chordLengthParameterize(a, b), g = Math.max(this.error, this.error * this.error), h = 0; 4 >= h; h++) {
            var k = this.generateBezier(a, b, f, c, d),
                l = this.findMaxError(a, b, k, f);
            if (l.error < this.error) {
                this.addCurve(k);
                return
            }
            e = l.index;
            if (l.error >= g) break;
            this.reparameterize(a, b, f, k);
            g = l.error
        }
        f = this.points[e - 1].subtract(this.points[e]);
        g = this.points[e].subtract(this.points[e +
            1]);
        f = f.add(g).divide(2).normalize();
        this.fitCubic(a, e, c, f);
        this.fitCubic(e, b, f.negate(), d)
    }
};
_.addCurve = function(a) {
    this.segments[this.segments.length - 1].setHandleOut(a[1].subtract(a[0]));
    this.segments.push(new Segment(a[3], a[2].subtract(a[3])))
};
_.generateBezier = function(a, b, c, d, e) {
    var f = Numerical.EPSILON,
        g = this.points[a],
        h = this.points[b],
        k = [
            [0, 0],
            [0, 0]
        ],
        l = [0, 0],
        q = 0;
    for (b = b - a + 1; q < b; q++) {
        var m = c[q],
            p = 1 - m,
            E = 3 * m * p,
            L = p * p * p,
            p = E * p,
            E = E * m,
            H = m * m * m,
            m = d.normalize(p),
            F = e.normalize(E),
            L = this.points[a + q].subtract(g.multiply(L + p)).subtract(h.multiply(E + H));
        k[0][0] += m.dot(m);
        k[0][1] += m.dot(F);
        k[1][0] = k[0][1];
        k[1][1] += F.dot(F);
        l[0] += m.dot(L);
        l[1] += F.dot(L)
    }
    a = k[0][0] * k[1][1] - k[1][0] * k[0][1];
    var r;
    Math.abs(a) > f ? (r = k[0][0] * l[1] - k[1][0] * l[0], l = (l[0] * k[1][1] -
        l[1] * k[0][1]) / a, r /= a) : (a = k[0][0] + k[0][1], k = k[1][0] + k[1][1], l = Math.abs(a) > f ? r = l[0] / a : Math.abs(a) > f ? r = l[1] / k : r = 0);
    k = h.getDistance(g);
    f *= k;
    if (l < f || r < f) l = r = k / 3;
    return [g, g.add(d.normalize(l)), h.add(e.normalize(r)), h]
};
_.reparameterize = function(a, b, c, d) {
    for (var e = a; e <= b; e++) c[e - a] = this.findRoot(d, this.points[e], c[e - a])
};
_.findRoot = function(a, b, c) {
    for (var d = [], e = [], f = 0; 2 >= f; f++) d[f] = a[f + 1].subtract(a[f]).multiply(3);
    for (f = 0; 1 >= f; f++) e[f] = d[f + 1].subtract(d[f]).multiply(2);
    a = this.evaluate(3, a, c);
    d = this.evaluate(2, d, c);
    e = this.evaluate(1, e, c);
    b = a.subtract(b);
    e = d.dot(d) + b.dot(e);
    return Math.abs(e) < Numerical.TOLERANCE ? c : c - b.dot(d) / e
};
_.evaluate = function(a, b, c) {
    b = b.slice();
    for (var d = 1; d <= a; d++)
        for (var e = 0; e <= a - d; e++) b[e] = b[e].multiply(1 - c).add(b[e + 1].multiply(c));
    return b[0]
};
_.chordLengthParameterize = function(a, b) {
    for (var c = [0], d = a + 1; d <= b; d++) c[d - a] = c[d - a - 1] + this.points[d].getDistance(this.points[d - 1]);
    for (var d = 1, e = b - a; d <= e; d++) c[d] /= c[e];
    return c
};
_.findMaxError = function(a, b, c, d) {
    for (var e = Math.floor((b - a + 1) / 2), f = 0, g = a + 1; g < b; g++) {
        var h = this.evaluate(3, c, d[g - a]).subtract(this.points[g]),
            h = h.x * h.x + h.y * h.y;
        h >= f && (f = h, e = g)
    }
    return {
        error: f,
        index: e
    }
};
_.segmentsToPath = function() {
    for (var a = new XOS_Path, b, c, d, e = this.segments.length, f, g = 0; g < e; g++) f = this.segments[g], point = f.point, x = point.x, y = point.y, handleIn = f.handleIn, b ? handleIn.isZero() && b.isZero() ? a.addPoint(new XOS_LineTo(x, y)) : a.addPoint(new XOS_CubicCurveTo(c, d, handleIn.x + x, handleIn.y + y, x, y)) : a.addPoint(new XOS_MoveTo(x, y)), b = f.handleOut, c = b.x + x, d = b.y + y;
    return a
};
(function() {
    "undefined" == typeof Math.sgn && (Math.sgn = function(a) {
        return 0 == a ? 0 : 0 < a ? 1 : -1
    });
    var a = {
            subtract: function(a, b) {
                return {
                    x: a.x - b.x,
                    y: a.y - b.y
                }
            },
            dotProduct: function(a, b) {
                return a.x * b.x + a.y * b.y
            },
            square: function(a) {
                return Math.sqrt(a.x * a.x + a.y * a.y)
            },
            scale: function(a, b) {
                return {
                    x: a.x * b,
                    y: a.y * b
                }
            }
        },
        b = Math.pow(2, -65),
        c = function(b, c) {
            for (var f = [], g = c.length - 1, h = 2 * g - 1, k = [], l = [], r = [], B = [], v = [
                    [1, .6, .3, .1],
                    [.4, .6, .6, .4],
                    [.1, .3, .6, 1]
                ], s = 0; s <= g; s++) k[s] = a.subtract(c[s], b);
            for (s = 0; s <= g - 1; s++) l[s] = a.subtract(c[s +
                1], c[s]), l[s] = a.scale(l[s], 3);
            for (s = 0; s <= g - 1; s++)
                for (var O = 0; O <= g; O++) r[s] || (r[s] = []), r[s][O] = a.dotProduct(l[s], k[O]);
            for (s = 0; s <= h; s++) B[s] || (B[s] = []), B[s].y = 0, B[s].x = parseFloat(s) / h;
            h = g - 1;
            for (k = 0; k <= g + h; k++)
                for (s = Math.max(0, k - h), l = Math.min(k, g); s <= l; s++) j = k - s, B[s + j].y += r[j][s] * v[j][s];
            g = c.length - 1;
            B = d(B, 2 * g - 1, f, 0);
            h = a.subtract(b, c[0]);
            r = a.square(h);
            for (s = v = 0; s < B; s++) h = a.subtract(b, e(c, g, f[s], null, null)), h = a.square(h), h < r && (r = h, v = f[s]);
            h = a.subtract(b, c[g]);
            h = a.square(h);
            h < r && (r = h, v = 1);
            return {
                location: v,
                distance: r
            }
        },
        d = function(a, c, f, g) {
            var h = [],
                k = [],
                l = [],
                r = [],
                B = 0,
                v, s;
            s = Math.sgn(a[0].y);
            for (var O = 1; O <= c; O++) v = Math.sgn(a[O].y), v != s && B++, s = v;
            switch (B) {
                case 0:
                    return 0;
                case 1:
                    if (64 <= g) return f[0] = (a[0].x + a[c].x) / 2, 1;
                    var M, R, B = a[0].y - a[c].y;
                    s = a[c].x - a[0].x;
                    O = a[0].x * a[c].y - a[c].x * a[0].y;
                    v = max_distance_below = 0;
                    for (M = 1; M < c; M++) R = B * a[M].x + s * a[M].y + O, R > v ? v = R : R < max_distance_below && (max_distance_below = R);
                    R = s;
                    M = 0 * R - 1 * B;
                    v = 1 / M * (1 * (O - v) - 0 * R);
                    R = s;
                    s = O - max_distance_below;
                    M = 0 * R - 1 * B;
                    B = 1 / M * (1 * s - 0 * R);
                    s = Math.min(v, B);
                    if (Math.max(v,
                            B) - s < b) return l = a[c].x - a[0].x, r = a[c].y - a[0].y, f[0] = 0 + 1 / (0 * l - 1 * r) * 1 * (l * (a[0].y - 0) - r * (a[0].x - 0)), 1
            }
            e(a, c, .5, h, k);
            a = d(h, c, l, g + 1);
            c = d(k, c, r, g + 1);
            for (g = 0; g < a; g++) f[g] = l[g];
            for (g = 0; g < c; g++) f[g + a] = r[g];
            return a + c
        },
        e = function(a, b, c, d, e) {
            for (var f = [
                    []
                ], g = 0; g <= b; g++) f[0][g] = a[g];
            for (a = 1; a <= b; a++)
                for (g = 0; g <= b - a; g++) f[a] || (f[a] = []), f[a][g] || (f[a][g] = {}), f[a][g].x = (1 - c) * f[a - 1][g].x + c * f[a - 1][g + 1].x, f[a][g].y = (1 - c) * f[a - 1][g].y + c * f[a - 1][g + 1].y;
            if (null != d)
                for (g = 0; g <= b; g++) d[g] = f[g][0];
            if (null != e)
                for (g = 0; g <= b; g++) e[g] =
                    f[b - g][g];
            return f[b][0]
        },
        f = {},
        g = function(a) {
            var b = f[a];
            if (!b) {
                var b = [],
                    c = function(a) {
                        return function(b) {
                            return a
                        }
                    },
                    d = function() {
                        return function(a) {
                            return a
                        }
                    },
                    e = function() {
                        return function(a) {
                            return 1 - a
                        }
                    },
                    g = function(a) {
                        return function(b) {
                            for (var c = 1, d = 0; d < a.length; d++) c *= a[d](b);
                            return c
                        }
                    };
                b.push(new function() {
                    return function(b) {
                        return Math.pow(b, a)
                    }
                });
                for (var h = 1; h < a; h++) {
                    for (var k = [new c(a)], l = 0; l < a - h; l++) k.push(new d);
                    for (l = 0; l < h; l++) k.push(new e);
                    b.push(new g(k))
                }
                b.push(new function() {
                    return function(b) {
                        return Math.pow(1 -
                            b, a)
                    }
                });
                f[a] = b
            }
            return b
        },
        h = function(a, b) {
            for (var c = g(a.length - 1), d = 0, e = 0, f = 0; f < a.length; f++) d += a[f].x * c[f](b), e += a[f].y * c[f](b);
            return {
                x: d,
                y: e
            }
        },
        k = function(a, b, c) {
            for (var d = h(a, b), e = 0, f = 0 < c ? 1 : -1, g = null; e < Math.abs(c);) {
                b += .005 * f;
                var k = g = h(a, b),
                    d = Math.sqrt(Math.pow(k.x - d.x, 2) + Math.pow(k.y - d.y, 2)),
                    e = e + d,
                    d = g
            }
            return {
                point: g,
                location: b
            }
        },
        l = function(a, b) {
            var c = h(a, b),
                d = h(a.slice(0, a.length - 1), b),
                e = d.y - c.y,
                c = d.x - c.x;
            return 0 == e ? Infinity : Math.atan(e / c)
        };
    window.jsBezier = {
        distanceFromCurve: c,
        gradientAtPoint: l,
        gradientAtPointAlongCurveFrom: function(a, b, c) {
            b = k(a, b, c);
            1 < b.location && (b.location = 1);
            0 > b.location && (b.location = 0);
            return l(a, b.location)
        },
        nearestPointOnCurve: function(a, b) {
            var d = c(a, b);
            return {
                point: e(b, b.length - 1, d.location, null, null),
                location: d.location
            }
        },
        pointOnCurve: h,
        pointAlongCurveFrom: function(a, b, c) {
            return k(a, b, c).point
        },
        perpendicularToCurveAt: function(a, b, c, d) {
            b = k(a, b, null == d ? 0 : d);
            a = l(a, b.location);
            d = Math.atan(-1 / a);
            a = c / 2 * Math.sin(d);
            c = c / 2 * Math.cos(d);
            return [{
                x: b.point.x + c,
                y: b.point.y + a
            }, {
                x: b.point.x - c,
                y: b.point.y - a
            }]
        }
    }
})();

function XOS_Point2D(a, b) {
    this.x = a;
    this.y = b;
    this.isSelected = !1
}
_ = XOS_Point2D.prototype;
_.select = function() {
    this.isSelected = !0
};
_.deselect = function() {
    this.isSelected = !1
};
_.isZero = function() {
    return 0 == this.x && 0 == this.y
};
_.equals = function(a) {
    return this.x == a.x && this.y == a.y
};
_.add = function(a) {
    return void 0 === a.x ? new XOS_Point2D(this.x + a, this.y + a) : new XOS_Point2D(this.x + a.x, this.y + a.y)
};
_.subtract = function(a) {
    return void 0 === a.x ? new XOS_Point2D(this.x - a, this.y - a) : new XOS_Point2D(this.x - a.x, this.y - a.y)
};
_.multiply = function(a) {
    return void 0 === a.x ? new XOS_Point2D(this.x * a, this.y * a) : new XOS_Point2D(this.x * a.x, this.y * a.y)
};
_.divide = function(a) {
    return void 0 === a.x ? new XOS_Point2D(this.x / a, this.y / a) : new XOS_Point2D(this.x / a.x, this.y / a.y)
};
_.modulo = function(a) {
    return void 0 === a.x ? new XOS_Point2D(this.x % a, this.y % a) : new XOS_Point2D(this.x % a.x, this.y % a.y)
};
_.negate = function(a) {
    return new XOS_Point2D(-this.x, -this.y)
};
_.getLength = function(a) {
    var b = this.x * this.x + this.y * this.y;
    return a ? b : Math.sqrt(b)
};
_.normalize = function(a) {
    void 0 === a && (a = 1);
    var b = this.getLength();
    a = 0 != b ? a / b : 0;
    return new XOS_Point2D(this.x * a, this.y * a)
};
_.dot = function(a) {
    return this.x * a.x + this.y * a.y
};
_.cross = function(a) {
    return this.x * a.y - this.y * a.x
};
_.getPerpendicular = function() {
    return new XOS_Point2D(1, -(this.x / this.y))
};
_.getDistance = function(a, b) {
    var c = a.x - this.x,
        d = a.y - this.y,
        c = c * c + d * d;
    return b ? c : Math.sqrt(c)
};
_.getDistanceFromLine = function(a, b, c, d) {
    var e = c - a,
        f = d - b;
    0 == e && 0 == f && (c += 1, d += 1, e = f = 1);
    var g = ((this.x - a) * e + (this.y - b) * f) / (e * e + f * f);
    0 > g ? e = a : 1 < g ? (e = c, b = d) : (e = a + g * e, b += g * f);
    e -= this.x;
    f = b - this.y;
    return Math.sqrt(e * e + f * f)
};
_.rotate = function(a) {
    var b = Math.cos(a);
    a = Math.sin(a);
    var c = this.x * -a + this.y * b;
    this.x = this.x * b + this.y * a;
    this.y = c
};
_.translate = function(a, b) {
    this.x += a;
    this.y += b
};
_.scale = function(a, b) {
    this.x *= a;
    this.y *= b
};
_.set = function(a, b) {
    this.x = a;
    this.y = b
};
_.toString = function() {
    return this.x + "," + this.y
};
_.clone = function() {
    return new XOS_Point2D(this.x, this.y)
};
_.distanceFromPoint = function(a) {
    var b = this.x - a.x;
    a = this.y - a.y;
    return Math.sqrt(b * b + a * a)
};
_.getAngleFromPoint = function(a, b) {
    for (var c = Math.atan2(this.y - a.y, this.x - a.x); 0 > c;) c += 2 * Math.PI;
    !0 == b && (c = 180 * c / Math.PI);
    return c
};
XOS_Point2D.interpolatePoints = function(a, b, c) {
    "undefined" === typeof c && (c = .5);
    return {
        x: a.x + (b.x - a.x) * c,
        y: a.y + (b.y - a.y) * c
    }
};
ExtendClass(XOS_MoveTo, XOS_Point2D);

function XOS_MoveTo(a, b) {
    XOS_MoveTo.baseConstructor.call(this, a, b);
    this.pathRef = null
}
_ = XOS_MoveTo.prototype;
_.draw = function(a) {
    a.moveTo(this.x, this.y)
};
_.clone = function() {
    return new XOS_MoveTo(this.x, this.y)
};
ExtendClass(XOS_LineTo, XOS_Point2D);

function XOS_LineTo(a, b) {
    XOS_LineTo.baseConstructor.call(this, a, b);
    this.pathRef = null
}
_ = XOS_LineTo.prototype;
_.draw = function(a) {
    a.lineTo(this.x, this.y)
};
_.clone = function() {
    return new XOS_LineTo(this.x, this.y)
};
_.splitAtPoint = function(a) {
    return [new XOS_LineTo(a.x, a.y), new XOS_LineTo(this.x, this.y)]
};
ExtendClass(XOS_QuadraticCurveTo, XOS_Point2D);

function XOS_QuadraticCurveTo(a, b, c, d) {
    XOS_QuadraticCurveTo.baseConstructor.call(this, c, d);
    this.controlPt = new XOS_ControlPoint(a, b);
    this.pathRef = null
}
_ = XOS_QuadraticCurveTo.prototype;
_.select = function() {
    this.controlPt.isSelected = this.isSelected = !0
};
_.deselect = function() {
    this.controlPt.isSelected = this.isSelected = !1
};
_.draw = function(a) {
    a.quadraticCurveTo(this.controlPt.x, this.controlPt.y, this.x, this.y)
};
_.clone = function() {
    return new XOS_QuadraticCurveTo(this.controlPt.x, this.controlPt.y, this.x, this.y)
};
_.getPointOnCurve = function(a, b) {
    var c = {};
    c.x = b.x + a * (2 * (1 - a) * (this.controlPt.x - b.x) + a * (this.x - b.x));
    c.y = b.y + a * (2 * (1 - a) * (this.controlPt.y - b.y) + a * (this.y - b.y));
    return c
};
_.translate = function(a, b) {
    this.x += a;
    this.y += b;
    this.controlPt.x += a;
    this.controlPt.y += b
};
_.toCubicCurve = function(a) {
    return new XOS_CubicCurveTo(a.x + 2 / 3 * (this.controlPt.x - a.x), a.y + 2 / 3 * (this.controlPt.y - a.y), this.controlPt.x + 1 / 3 * (this.x - this.controlPt.x), this.controlPt.y + 1 / 3 * (this.y - this.controlPt.y), this.x, this.y)
};
ExtendClass(XOS_CubicCurveTo, XOS_Point2D);

function XOS_CubicCurveTo(a, b, c, d, e, f) {
    XOS_CubicCurveTo.baseConstructor.call(this, e, f);
    this.controlPt1 = new XOS_ControlPoint(a, b);
    this.controlPt2 = new XOS_ControlPoint(c, d);
    this.pathRef = null
}
_ = XOS_CubicCurveTo.prototype;
_.select = function() {
    this.controlPt1.isSelected = this.controlPt2.isSelected = this.isSelected = !0
};
_.deselect = function() {
    this.controlPt1.isSelected = this.controlPt2.isSelected = this.isSelected = !1
};
_.draw = function(a) {
    a.bezierCurveTo(this.controlPt1.x, this.controlPt1.y, this.controlPt2.x, this.controlPt2.y, this.x, this.y)
};
_.clone = function() {
    return new XOS_CubicCurveTo(this.controlPt1.x, this.controlPt1.y, this.controlPt2.x, this.controlPt2.y, this.x, this.y)
};
_.getPointOnCurve = function(a, b) {
    var c, d, e, f, g, h, k, l, q = {};
    e = 3 * (this.controlPt1.x - b.x);
    d = 3 * (this.controlPt2.x - this.controlPt1.x) - e;
    c = this.x - b.x - e - d;
    h = 3 * (this.controlPt1.y - b.y);
    g = 3 * (this.controlPt2.y - this.controlPt1.y) - h;
    f = this.y - b.y - h - g;
    k = a * a;
    l = k * a;
    q.x = c * l + d * k + e * a + b.x;
    q.y = f * l + g * k + h * a + b.y;
    return q
};
_.translate = function(a, b) {
    this.x += a;
    this.y += b;
    this.controlPt1.x += a;
    this.controlPt1.y += b;
    this.controlPt2.x += a;
    this.controlPt2.y += b
};
ExtendClass(XOS_ControlPoint, XOS_Point2D);

function XOS_ControlPoint(a, b) {
    XOS_ControlPoint.baseConstructor.call(this, a, b);
    this.pathRef = null
};

function XOS_Matrix2D(a, b, c, d, e, f) {
    this.a = a || 1;
    this.c = c || 0;
    this.e = e || 0;
    this.b = b || 0;
    this.d = d || 1;
    this.f = f || 0
}
_ = XOS_Matrix2D.prototype;
_.identity = function() {
    this.a = this.d = 1;
    this.c = this.e = this.b = this.f = 0
};
_.isIdentity = function() {
    return 1 == this.a && 1 == this.d && 0 == this.c && 0 == this.e && 0 == this.b && 0 == this.f ? !0 : !1
};
_.set = function(a, b, c, d, e, f) {
    this.a = a || 1;
    this.c = c || 0;
    this.e = e || 0;
    this.b = b || 0;
    this.d = d || 1;
    this.f = f || 0
};
_.clone = function() {
    return new XOS_Matrix2D(this.a, this.b, this.c, this.d, this.e, this.f)
};
_.copyTo = function(a) {
    a.a = this.a;
    a.c = this.c;
    a.e = this.e;
    a.b = this.b;
    a.d = this.d;
    a.f = this.f
};
_.concat = function(a) {
    return new XOS_Matrix2D(this.a * a.a + this.c * a.b, this.b * a.a + this.d * a.b, this.a * a.c + this.c * a.d, this.b * a.c + this.d * a.d, this.a * a.e + this.c * a.f + this.e, this.b * a.e + this.d * a.f + this.f)
};
_.invert = function() {
    var a = this.a,
        b = this.b,
        c = this.c,
        d = this.d,
        e = this.e,
        f = a * d - b * c;
    this.a = d / f;
    this.b = -b / f;
    this.c = -c / f;
    this.d = a / f;
    this.e = (c * this.f - d * e) / f;
    this.f = -(a * this.f - b * e) / f;
    return this
};
_.prependRotation = function(a) {
    var b = Math.cos(a);
    a = Math.sin(a);
    return (new XOS_Matrix2D(b, a, -a, b, 0, 0)).concat(this)
};
_.appendRotation = function(a) {
    var b = Math.cos(a);
    a = Math.sin(a);
    return this.concat(new XOS_Matrix2D(b, a, -a, b, 0, 0))
};
_.prependTranslation = function(a, b) {
    return (new XOS_Matrix2D(1, 0, 0, 1, a, b)).concat(this)
};
_.appendTranslation = function(a, b) {
    return this.concat(new XOS_Matrix2D(1, 0, 0, 1, a, b))
};
_.prependScale = function(a, b) {
    return (new XOS_Matrix2D(a, 0, 0, b, 0, 0)).concat(this)
};
_.appendScale = function(a, b) {
    return this.concat(new XOS_Matrix2D(a, 0, 0, b, 0, 0))
};
_.deltaTransformPoint = function(a) {
    return new XOS_Point2D(this.a * a.x + this.c * a.y, this.b * a.x + this.d * a.y)
};
_.transformPoint = function(a) {
    return new XOS_Point2D(this.a * a.x + this.c * a.y + this.e, this.b * a.x + this.d * a.y + this.f)
};
_.getTranslation = function() {
    return new XOS_Point2D(this.e, this.f)
};
_.getScale = function() {
    return new XOS_Point2D(Math.sqrt(this.a * this.a + this.b * this.b), Math.sqrt(this.c * this.c + this.d * this.d))
};
_.getRotation = function() {
    for (var a = this.deltaTransformPoint(new XOS_Point2D(0, 1)), a = Math.atan2(a.y, a.x) - Math.PI / 2; 0 > a;) a += 2 * Math.PI;
    return a
};
_.toString = function(a) {
    return a ? "matrix(" + [this.a.toFixed(a), this.b.toFixed(a), this.c.toFixed(a), this.d.toFixed(a), this.e.toFixed(a), this.f.toFixed(a)].join() + ")" : "matrix(" + [this.a, this.b, this.c, this.d, this.e, this.f].join() + ")"
};

function XOS_Bounding2D() {
    this.min = new XOS_Point2D(1E12, 1E12);
    this.max = new XOS_Point2D(-1E12, -1E12)
}
_ = XOS_Bounding2D.prototype;
_.identity = function() {
    this.min.x = 1E12;
    this.min.y = 1E12;
    this.max.x = -1E12;
    this.max.y = -1E12
};
_.addPt = function(a) {
    a.x < this.min.x && (this.min.x = a.x);
    a.x > this.max.x && (this.max.x = a.x);
    a.y < this.min.y && (this.min.y = a.y);
    a.y > this.max.y && (this.max.y = a.y)
};
_.addPoints = function(a) {
    for (var b = a.length, c = 0; c < b; c++) this.addPt(a[c])
};
_.addX = function(a) {
    a < this.min.x && (this.min.x = a);
    a > this.max.x && (this.max.x = a)
};
_.addY = function(a) {
    a < this.min.y && (this.min.y = a);
    a > this.max.y && (this.max.y = a)
};
_.addBounds = function(a) {
    a.min.x < this.min.x && (this.min.x = a.min.x);
    a.max.x > this.max.x && (this.max.x = a.max.x);
    a.min.y < this.min.y && (this.min.y = a.min.y);
    a.max.y > this.max.y && (this.max.y = a.max.y)
};
_.getSize = function() {
    return {
        x: this.getWidth(),
        y: this.getHeight()
    }
};
_.getWidth = function() {
    return this.max.x - this.min.x
};
_.getHeight = function() {
    return this.max.y - this.min.y
};
_.setWidth = function(a) {
    this.max.x = this.min.x + a
};
_.setHeight = function(a) {
    this.max.y = this.min.y + a
};
_.getCenter = function() {
    return new XOS_Point2D(this.min.x + this.getWidth() / 2, this.min.y + this.getHeight() / 2)
};
_.clone = function() {
    var a = new XOS_Bounding2D;
    a.min = this.min.clone();
    a.max = this.max.clone();
    return a
};
_.getPoints = function() {
    var a = this.min,
        b = this.max,
        c = this.getCenter();
    return [new XOS_Point2D(a.x, a.y), new XOS_Point2D(b.x, a.y), new XOS_Point2D(b.x, b.y), new XOS_Point2D(a.x, b.y), new XOS_Point2D(c.x, c.y)]
};
ExtendClass(XOS_DisplayObject2D, XOS_Node);

function XOS_DisplayObject2D() {
    XOS_DisplayObject2D.baseConstructor.call(this);
    this.pivotY = this.pivotX = 0;
    this.localTransform = new XOS_Matrix2D;
    this.globalTransform = new XOS_Matrix2D;
    this.effectList = [];
    this.tweenList = [];
    this.animationList = [];
    this.visible = !0;
    this.maintainAspectRatio = this.isSelected = this.isLocked = !1;
    this.graphicContext = this.displayRef = null;
    this.placeIntoHtmlLayer = "no";
    this.lineWidth = 1;
    this.lineCap = "butt";
    this.lineJoin = "miter";
    this.miterLimit = 3;
    this.brush = null;
    this.patternName = "";
    this.compositeOperation =
        "source-over";
    this.closePath = !1;
    this.strokeAlpha = this.strokeFillingType = 1;
    this.strokeColor = "#000000";
    this.strokePattern = this.strokeGradient = null;
    this.fillAlpha = this.fillFillingType = 1;
    this.fillColor = "#FFFFFF";
    this.fillPattern = this.fillGradient = null;
    this.globalAlpha = this.alpha = 1;
    this.bounds = new XOS_Bounding2D;
    this.invalidAlpha = this.invalidTransform = this.invalidBounds = !0;
    this.invalidGeometry = !1;
    this.description = this.title = this.classNameList = this.anchorLinkTarget = this.anchorLink = ""
}
_ = XOS_DisplayObject2D.prototype;
XOS_DisplayObject2D.NONE_FILLING = 0;
XOS_DisplayObject2D.DIFFUSE_FILLING = 1;
XOS_DisplayObject2D.GRADIENT_FILLING = 2;
XOS_DisplayObject2D.PATTERN_FILLING = 3;
XOS_DisplayObject2D.BRUSH_FILLING = 4;
_.__defineGetter__("positionX", function() {
    return this.getPositionInContent().x
});
_.__defineSetter__("positionX", function(a) {});
_.__defineGetter__("positionY", function() {
    return this.getPositionInContent().y
});
_.__defineSetter__("positionY", function(a) {});
_.__defineGetter__("x", function() {
    return this.getPosition().x
});
_.__defineSetter__("x", function(a) {
    this.setPosition(a, this.getPosition().y)
});
_.__defineGetter__("y", function() {
    return this.getPosition().y
});
_.__defineSetter__("y", function(a) {
    this.setPosition(this.getPosition().x, a)
});
_.__defineGetter__("globalPositionX", function() {
    return this.getPositionInContent().x
});
_.__defineSetter__("globalPositionX", function(a) {
    this.setPositionInContent(a, this.getPositionInContent().y)
});
_.__defineGetter__("globalPositionY", function() {
    return this.getPositionInContent().y
});
_.__defineSetter__("globalPositionY", function(a) {
    this.setPositionInContent(this.getPositionInContent().x, a)
});
_.__defineGetter__("scaleX", function() {
    return this.localTransform.getScale().x
});
_.__defineSetter__("scaleX", function(a) {
    this.setScale(a, this.localTransform.getScale().y)
});
_.__defineGetter__("scaleY", function() {
    return this.localTransform.getScale().y
});
_.__defineSetter__("scaleY", function(a) {
    this.setScale(this.localTransform.getScale().x, a)
});
_.__defineGetter__("sizeX", function() {
    //alert('XXXXXX');
    return this.getSize().x
});
_.__defineSetter__("sizeX", function(a) {
    this.setSize(a, this.getSize().y)
});
_.__defineGetter__("sizeY", function() {
    return this.getSize().y
});
_.__defineSetter__("sizeY", function(a) {
    this.setSize(this.getSize().x, a)
});
_.__defineGetter__("rotation", function() {
    return this.localTransform.getRotation()
});
_.__defineSetter__("rotation", function(a) {
    this.setRotation(a)
});
_.__defineGetter__("opacity", function() {
    return this.alpha
});
_.__defineSetter__("opacity", function(a) {
    this.setAlpha(a)
});
_.select = function() {
    this.isSelected = !0
};
_.deselect = function() {
    this.isSelected = !1
};
_.selectAllPoints = function() {};
_.deselectAllPoints = function() {};
_.globalTransformToLocalTransform = function(a) {
    this.localTransform = null != this.parent ? this.parent.globalTransform.clone().invert().concat(a) : a
};
_.translate = function(a, b, c) {
    if (!0 != c) {
        c = this.localTransform.getTranslation();
        var d = this.parent.localToGlobal(c);
        d.x += a;
        d.y += b;
        a = this.parent.globalToLocal(d);
        this.localTransform = this.localTransform.prependTranslation(a.x - c.x, a.y - c.y)
    } else this.localTransform = this.localTransform.appendTranslation(a, b);
    this.invalidateTransform()
};
_.scale = function(a, b) {
    this.localTransform = this.localTransform.appendScale(a, b);
    this.invalidateTransform()
};
_.rotate = function(a) {
    this.localTransform = this.localTransform.appendRotation(a);
    this.invalidateTransform()
};
_.setPosition = function(a, b) {
    var c = this.localTransform.getTranslation();
    this.localTransform = this.localTransform.prependTranslation(a - c.x, b - c.y);
    this.invalidateTransform()
};
_.getPosition = function() {
    return this.localTransform.getTranslation()
};
_.setScale = function(a, b) {
    var c = this.localTransform.getScale();
    this.localTransform = this.localTransform.appendScale(1 / c.x, 1 / c.y);
    this.localTransform = this.localTransform.appendScale(a, b);
    this.invalidateTransform()
};
_.getScale = function() {
    return this.localTransform.getScale()
};
_.setRotation = function(a) {
    this.rotate(a - this.localTransform.getRotation())
};
_.getRotation = function() {
    return this.localTransform.getRotation()
};
_.setAlpha = function(a) {
    this.alpha = a;
    this.invalidateAlpha()
};
_.addEffect = function(a) {
    a = a.clone();
    a.setTarget(this);
    this.effectList.push(a)
};
_.removeEffect = function(a) {
    this.effectList.remove(a)
};
_.removeAllEffects = function() {
    this.effectList = []
};
_.getEffectByName = function(a) {
    for (var b = this.effectList.length, c = 0; c < b; c++)
        if (this.effectList[c].name == a) return this.effectList[c];
    return null
};
_.addTween = function(a) {
    a = a.clone();
    a.setTarget(this);
    this.tweenList.push(a)
};
_.removeTween = function(a) {
    this.tweenList.remove(a)
};
_.removeAllTweens = function() {
    this.tweenList = []
};
_.getTweenByName = function(a) {
    for (var b = this.tweenList.length, c = 0; c < b; c++)
        if (this.tweenList[c].name == a) return this.tweenList[c];
    return null
};
_.addAnimation = function(a) {
    a = a.clone();
    a.setTarget(this);
    this.animationList.push(a)
};
_.removeAnimation = function(a) {
    this.animationList.remove(a)
};
_.removeAllAnimation = function() {
    this.animationList = []
};
_.getAnimationByName = function(a) {
    for (var b = this.animationList.length, c = 0; c < b; c++)
        if (this.animationList[c].name == a) return this.animationList[c];
    return null
};
_.getPositionInContent = function() {
    var a = this.getGlobalBounds();
    return this.displayRef.content.globalToLocal(a.min)
};
_.setPositionInContent = function(a, b) {
    var c = this.displayRef.content.localToGlobal(new XOS_Point2D(a, b)),
        d = this.getGlobalBounds(),
        c = c.subtract(d.min);
    this.translate(c.x, c.y, !1)
};
_.getSize = function() {
    var a = this.getLocalBounds().getSize(),
        b = this.getScale();
    return new XOS_Point2D(a.x * b.x, a.y * b.y)
};
_.setSize = function(a, b) {
    var c = this.getLocalBounds(),
        d = c.getSize();
    this.scaleFromPoint(a / d.x, b / d.y, this.localToGlobal(c.min), !0);
    this.normalizePoints()
};
_.scaleFromPoint = function(a, b, c, d) {
    !0 != d ? (c = this.parent.globalToLocal(c), this.localTransform = this.localTransform.prependTranslation(-c.x, -c.y), this.localTransform = this.localTransform.prependScale(a, b), this.localTransform = this.localTransform.prependTranslation(c.x, c.y)) : (c = this.globalToLocal(c), this.localTransform = this.localTransform.appendTranslation(c.x, c.y), this.localTransform = this.localTransform.appendScale(a, b), this.localTransform = this.localTransform.appendTranslation(-c.x, -c.y));
    this.invalidateTransform()
};
_.rotateFromPoint = function(a, b, c) {
    this.invalidTransform && this.updateGlobalTransform();
    !0 != c && (b = this.parent.globalToLocal(b), this.localTransform = this.localTransform.prependTranslation(-b.x, -b.y), this.localTransform = this.localTransform.prependRotation(a), this.localTransform = this.localTransform.prependTranslation(b.x, b.y), this.invalidateTransform(), this.invalidateBounds())
};
_.globalGradientToLocalGradient = function() {
    this.fillFillingType == XOS_DisplayObject2D.GRADIENT_FILLING && this.fillGradient.convertToLocal(this);
    this.strokeFillingType == XOS_DisplayObject2D.GRADIENT_FILLING && this.strokeGradient.convertToLocal(this);
    for (var a = this.numChildren(), b = 0; b < a; b++) this.children[b].globalGradientToLocalGradient()
};
_.updateGlobalTransform = function() {
    null != this.parent ? this.globalTransform = this.parent.globalTransform.concat(this.localTransform) : this.localTransform.copyTo(this.globalTransform);
    this.invalidTransform = !1;
    for (var a = this.numChildren(), b = 0; b < a; b++) this.children[b].updateGlobalTransform()
};
_.updateGlobalAlpha = function() {
    this.globalAlpha = null != this.parent ? this.parent.globalAlpha * this.alpha : this.alpha;
    this.invalidAlpha = !1;
    for (var a = this.numChildren(), b = 0; b < a; b++) this.children[b].updateGlobalAlpha()
};
_.invalidateAlpha = function() {
    this.invalidAlpha = !0
};
_.invalidateTransform = function() {
    this.invalidTransform = !0
};
_.invalidateBounds = function() {
    this.invalidBounds = !0;
    null != this.parent && (this.parent instanceof XOS_Layer || this.parent.invalidateBounds())
};
_.numChildren = function() {
    return this.children.length
};
_.addChild = function(a) {
    this.children.push(a);
    a.parent = this;
    a.invalidateAlpha();
    a.invalidateTransform();
    a.onAdded();
    return a
};
_.applyEffects = function() {
    var a = this.effectList.length;
    if (0 == a) return !1;
    for (var b = !1, c = 0; c < a; c++) this.effectList[c].visible && (this.effectList[c].apply(), b = !0);
    return b
};
_.applyTransform = function() {
    this.graphicContext.setTransform(this.globalTransform.a, this.globalTransform.b, this.globalTransform.c, this.globalTransform.d, this.globalTransform.e, this.globalTransform.f)
};
_.draw = function() {
    this.visible && (this.graphicContext.globalCompositeOperation = this.compositeOperation, this.invalidAlpha && this.updateGlobalAlpha(), this.invalidTransform && this.updateGlobalTransform(), this.applyTransform(), this.applyEffects() || this.drawChildren())
};
_.drawChildren = function() {
    for (var a = this.numChildren(), b = 0; b < a; b++) this.children[b].draw()
};
_.drawWireframe = function() {
    this.visible && (this.invalidTransform && this.updateGlobalTransform(), this.applyTransform(), this.drawGeometryWireframe(), this.closePath && this.graphicContext.closePath(), this.graphicContext.stroke(), this.drawChildrenWireframe())
};
_.drawChildrenWireframe = function() {
    for (var a = this.numChildren(), b = 0; b < a; b++) this.children[b].drawWireframe()
};
_.drawGeometry = function() {
    this.graphicContext.beginPath()
};
_.drawGeometryWireframe = function() {
    this.drawGeometry()
};
_.drawGeometryForHitTest = function() {
    this.drawGeometry()
};
_.drawPivot = function() {};
_.drawBoundsForHitTest = function(a) {
    var b = this.getLocalBounds();
    a *= 1 / this.globalTransform.getScale().x;
    this.graphicContext.beginPath();
    this.graphicContext.rect(b.min.x - a, b.min.y - a, b.getWidth() + a + a, b.getHeight() + a + a)
};
_.drawAsSelected = function() {
    var a = this.graphicContext;
    this.graphicContext = this.displayRef.interactionDisplay.graphicContext;
    this.invalidTransform && this.updateGlobalTransform();
    this.applyTransform();
    this.invalidBounds && this.calculateLocalBounds();
    this.graphicContext.beginPath();
    this.graphicContext.rect(this.bounds.min.x, this.bounds.min.y, this.bounds.getWidth(), this.bounds.getHeight());
    this.graphicContext.stroke();
    this.graphicContext = a
};
_.unlock = function() {
    this.isLocked = !1;
    for (var a = this.numChildren(), b = 0; b < a; b++) this.children[b].unlock()
};
_.getSelectedChildren = function() {
    for (var a = [], b, c = this.numChildren(), d = 0; d < c; d++) b = this.children[d], !1 == b.isLocked && !0 == b.isSelected ? a.push(b) : a = a.concat(b.getSelectedChildren());
    return a
};
_.hitTestObject = function(a, b, c) {
    if (c && this.isSelected || this.isLocked || !this.visible) return {
        hit: !1
    };
    var d = this.hitTestGeometry(a, b);
    return !0 == d.hit ? d : this.hitTestChildren(a, b, c)
};
_.hitTestChildren = function(a, b, c) {
    for (var d, e = this.numChildren() - 1; - 1 < e; e--)
        if (d = this.children[e].hitTestObject(a, b, c), !0 == d.hit) return d;
    return {
        hit: !1
    }
};
_.hitTestGeometry = function(a, b) {
    this.applyTransform();
    this.graphicContext.beginPath();
    this.drawGeometryForHitTest();
    return !0 == this.graphicContext.isPointInPath(a.x, a.y) ? {
        hit: !0,
        instance: this
    } : {
        hit: !1
    }
};
_.hitTestBounds = function(a, b) {
    this.drawBoundsForHitTest(b);
    return this.graphicContext.isPointInPath(a.x, a.y)
};
_.hitTestVertices = function(a, b) {};
_.getObjectsWithPointsInPath = function(a, b) {
    return this.isLocked ? null : !0 == this.hasPointsInPath() ? this : null
};
_.hasPointsInPath = function() {
    for (var a = this.getEditablePoints(), b = a.length, c = null, d = 0; d < b; d++)
        if (c = this.localToGlobal(a[d]), this.graphicContext.isPointInPath(c.x, c.y)) return !0;
    return !1
};
_.globalToLocal = function(a) {
    return this.globalTransform.clone().invert().transformPoint(a)
};
_.localToGlobal = function(a) {
    return this.globalTransform.transformPoint(a)
};
_.localToLocal = function(a, b) {
    return this.localToGlobal(b.globalToLocal(a))
};
_.globalPointsToLocalPoints = function(a, b) {
    var c = this.globalTransform.clone();
    c.invert();
    var d = a.length;
    if (b)
        for (var e, f = 0; f < d; f++) e = c.transformPoint(a[f]), a[f].x = e.x, a[f].y = e.y;
    else {
        e = [];
        for (f = 0; f < d; f++) e[f] = c.transformPoint(a[f]);
        return e
    }
};
_.localPointsToGlobalPoints = function(a, b) {
    var c = a.length;
    if (b)
        for (var d, e = 0; e < c; e++) d = this.globalTransform.transformPoint(a[e]), a[e].x = d.x, a[e].y = d.y;
    else {
        d = [];
        for (e = 0; e < c; e++) d[e] = this.globalTransform.transformPoint(a[e]);
        return d
    }
};
_.localPointsToLocalPoints = function(a, b, c) {
    return b.globalPointsToLocalPoints(this.localPointsToGlobalPoints(a, c), c)
};
_.getGlobalBounds = function() {
    this.invalidTransform && this.updateGlobalTransform();
    for (var a = this.localPointsToGlobalPoints(this.getLocalBounds().getPoints()), b = new XOS_Bounding2D, c = a.length, d = 0; d < c; d++) b.addPt(a[d]);
    return b
};
_.getLocalBounds = function() {
    this.invalidBounds && this.calculateLocalBounds();
    return this.bounds
};
_.calculateLocalBounds = function() {
    this.bounds.identity();
    this.bounds.addPoints(this.getEditablePoints());
    this.invalidBounds = !1
};
_.normalizePoints = function() {
    this.invalidTransform && this.updateGlobalTransform();
    this.globalTransformToLocalTransform(this.globalTransform);
    this.invalidateBounds();
    var a = this.effectList.length;
    if (0 < a)
        for (var b = this.globalTransform.getScale().x / this.displayRef.content.contentScale.x, c = 0; c < a; c++) this.effectList[c].scale = b
};
_.getInspectorPanelName = function() {
    return "instanceProperties2DPanel"
};
_.getEditablePoints = function() {
    for (var a = this.getLocalBounds(), b = this.bounds.min.x + (a.max.x - a.min.x) / 2, c = this.bounds.min.y + (a.max.y - a.min.y) / 2, d = Array(8), e = 0; 8 > e; e++) d[e] = new XOS_Point2D(0, 0);
    d[0].set(a.min.x, a.min.y);
    d[1].set(b, a.min.y);
    d[2].set(a.max.x, a.min.y);
    d[3].set(a.max.x, c);
    d[4].set(a.max.x, a.max.y);
    d[5].set(b, a.max.y);
    d[6].set(a.min.x, a.max.y);
    d[7].set(a.min.x, c);
    return d
};
_.onAdded = function() {
    var a = this.getParentByClass(XOS_Display2D);
    a && (this.setDisplay(a), this.onAddedToDisplay(this.displayRef))
};
_.onAddedToDisplay = function(a) {
    this.graphicContext = this.displayRef.graphicContext;
    if (0 < this.effectList.length) {
        var b = this.effectList.length;
        for (a = 0; a < b; a++) this.effectList[a].setTarget(this)
    }
    b = this.numChildren();
    for (a = 0; a < b; a++) this.children[a].onAddedToDisplay(this.displayRef)
};
_.onRemoved = function() {
    this.setDisplay(null)
};
_.clone = function(a) {
    var b = new XOS_DisplayObject2D;
    this.clonePropertiesTo(b);
    a && a.addChild(b);
    this.cloneChildren(b);
    return b
};
_.clonePropertiesTo = function(a) {
    XOS_DisplayObject2D.superClass.clonePropertiesTo.call(this, a);
    a.setDisplay(this.displayRef);
    a.id = this.id;
    a.name = this.name;
    a.anchorLink = this.anchorLink;
    a.anchorLinkTarget = this.anchorLinkTarget;
    a.classNameList = this.classNameList;
    a.lineCap = this.lineCap;
    a.lineJoin = this.lineJoin;
    a.miterLimit = this.miterLimit;
    a.compositeOperation = this.compositeOperation;
    a.closePath = this.closePath;
    a.maintainAspectRatio = this.maintainAspectRatio;
    a.strokeAlpha = this.strokeAlpha;
    a.fillAlpha = this.fillAlpha;
    a.lineWidth = this.lineWidth;
    a.fillFillingType = this.fillFillingType;
    a.strokeFillingType = this.strokeFillingType;
    a.alpha = this.alpha;
    a.placeIntoHtmlLayer = this.placeIntoHtmlLayer;
    a.effectList = [];
    for (var b = this.effectList.length, c = 0; c < b; c++) a.addEffect(this.effectList[c]);
    a.tweenList = [];
    b = this.tweenList.length;
    for (c = 0; c < b; c++) a.addTween(this.tweenList[c]);
    a.animationList = [];
    b = this.animationList.length;
    for (c = 0; c < b; c++) a.addAnimation(this.animationList[c]);
    this.strokeFillingType == XOS_DisplayObject2D.NONE_FILLING ?
        a.strokeColor = "none" : this.strokeFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING ? a.strokeColor = this.strokeColor : this.strokeFillingType == XOS_DisplayObject2D.GRADIENT_FILLING ? a.strokeGradient = this.strokeGradient.clone() : this.strokeFillingType == XOS_DisplayObject2D.PATTERN_FILLING && (a.strokePattern = this.strokePattern.clone());
    this.fillFillingType == XOS_DisplayObject2D.NONE_FILLING ? a.fillColor = "none" : this.fillFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING ? a.fillColor = this.fillColor : this.fillFillingType ==
        XOS_DisplayObject2D.GRADIENT_FILLING ? a.fillGradient = this.fillGradient.clone() : this.fillFillingType == XOS_DisplayObject2D.PATTERN_FILLING && (a.fillPattern = this.fillPattern.clone());
    a.localTransform = this.localTransform.clone();
    a.globalTransform = this.globalTransform.clone();
    a.bounds = this.bounds.clone()
};
_.setDisplay = function(a) {
    this.displayRef = a;
    this.graphicContext = null != a ? this.displayRef.graphicContext : null;
    for (var b = this.numChildren(), c = 0; c < b; c++) this.children[c].setDisplay(a)
};
_.getInteractionObjectName = function() {
    return ""
};
_.onMouseDown = function(a) {};
_.onMouseMove = function(a) {};
_.onMouseUp = function(a) {};
_.onTouchStart = function(a) {};
_.onTouchMove = function(a) {};
_.onTouchEnd = function(a) {};
_.onTouchCancel = function(a) {};
ExtendClass(XOS_Content2D, XOS_DisplayObject2D);

function XOS_Content2D() {
    XOS_Content2D.baseConstructor.call(this);
    this.type = "CONTENT";
    this.contentScale = 1
}
_ = XOS_Content2D.prototype;
_.updateGlobalTransform = function() {
    XOS_Content2D.superClass.updateGlobalTransform.call(this);
    this.contentScale = this.localTransform.getScale()
};
ExtendClass(XOS_Display2D, XOS_DisplayObject2D);

function XOS_Display2D(a) {
    XOS_Display2D.baseConstructor.call(this);
    this.canvas = a;
    (this.graphicContext = this.canvas.getContext) ? (this.graphicContext = this.canvas.getContext("2d"), this.invalidRender = !0, this.activeFontFamily = "helvetica", this.screenSnapValue = 4, this.symbolLibrary = {}, this.onEnterFrameListener = null, this.documentName = "", this.onBeforeDrawFunctionCallback = null) : alert("XOS_Display2D NO CANVAS CREATION")
}
_ = XOS_Display2D.prototype;
_.invalidateRender = function() {
    this.invalidRender = !0
};
_.onLoadImage = function() {
    this.invalidateRender()
};
_.onLoadImageError = function() {
    this.invalidateRender()
};
_.initEventHandlers = function() {};
_.draw = function(a) {
    if (!1 != this.invalidRender) {
        if (this.onBeforeDrawFunctionCallback) this.onBeforeDrawFunctionCallback(this);
        void 0 == a && (a = !0);
        a && this.clear();
        this.invalidTransform && this.updateGlobalTransform();
        this.applyTransform();
        this.graphicContext.globalCompositeOperation = "source-over";
        this.drawChildren();
        this.invalidRender = !1
    }
};
_.clear = function() {
    this.graphicContext.setTransform(1, 0, 0, 1, 0, 0);
    this.graphicContext.globalCompositeOperation = "source-over";
    this.graphicContext.globalAlpha = 1;
    this.graphicContext.clearRect(0, 0, this.canvas.width, this.canvas.height)
};
_.fillContent = function(a, b) {
    this.graphicContext.save();
    this.graphicContext.setTransform(1, 0, 0, 1, 0, 0);
    this.graphicContext.globalCompositeOperation = "source-over";
    this.graphicContext.fillStyle = a;
    this.graphicContext.globalAlpha = b;
    this.graphicContext.rect(0, 0, this.canvas.width, this.canvas.height);
    this.graphicContext.fill();
    this.graphicContext.restore()
};
_.resetContent = function() {
    this.content.removeAllChildren()
};
_.globalPointToContentPoint = function(a) {
    return this.content.globalToLocal(a)
};
_.hitTestAll = function(a, b, c) {
    this.invalidTransform && this.updateGlobalTransform();
    this.applyTransform();
    var d = this.screenSnapValue;
    b && (d = b);
    return this.hitTestChildren(a, d, c)
};
_.getSymbolFromLibrary = function(a) {
    return this.symbolLibrary[a]
};
_.addSymbolToLibrary = function(a, b) {
    this.symbolLibrary[a] || (this.symbolLibrary[a] = b)
};
_.removeSymbolFromLibrary = function(a) {
    this.symbolLibrary[a] && delete this.symbolLibrary[a]
};
CanvasRenderingContext2D.prototype.drawEllipse = function(a, b, c, d) {
    var e = c / 2,
        f = d / 2,
        g = a + e,
        h = b + f;
    c = a + c;
    d = b + d;
    e *= .5522848;
    f *= .5522848;
    this.moveTo(a, h);
    this.bezierCurveTo(a, h - f, g - e, b, g, b);
    this.bezierCurveTo(g + e, b, c, h - f, c, h);
    this.bezierCurveTo(c, h + f, g + e, d, g, d);
    this.bezierCurveTo(g - e, d, a, h + f, a, h)
};
CanvasRenderingContext2D.prototype.roundedRect = function(a, b, c, d, e) {
    0 > c && (c = 0 - c, a -= c);
    0 > d && (d = 0 - d, b -= d);
    this.moveTo(a + e, b);
    this.lineTo(a + c - e, b);
    this.quadraticCurveTo(a + c, b, a + c, b + e);
    this.lineTo(a + c, b + d - e);
    this.quadraticCurveTo(a + c, b + d, a + c - e, b + d);
    this.lineTo(a + e, b + d);
    this.quadraticCurveTo(a, b + d, a, b + d - e);
    this.lineTo(a, b + e);
    this.quadraticCurveTo(a, b, a + e, b)
};
ExtendClass(XOS_EditableDocument2D, XOS_Display2D);

function XOS_EditableDocument2D(a, b, c) {
    this.appRef = a;
    this.htmlElement = this.createHtmlDocument(b, c);
    this.htmlElement.documentController = this;
    this.bodyBackgroundColor = "rgba(255,255,255,1)";
    a = this.htmlElement.getElementsByTagName("canvas");
    XOS_EditableDocument2D.baseConstructor.call(this, a[0]);
    this.interactionDisplay = new XOS_Display2D(a[1]);
    this.metaTags = [];
    this.cssIncludes = [];
    this.jsIncludes = [];
    this.currentEditableGraphicObj = this;
    this.docBar = this.htmlElement.querySelector(".documentBar");
    this.history =
        new XOS_History;
    this.selectionList = [];
    this.content = new XOS_Content2D;
    this.content.name = "content";
    this.addChild(this.content);
    this.currentLayer = this.currentInteractionObject = this.currentEditableGraphicObj = null;
    this.isChanged = !1;
    this.tools = [];
    this.initTools();
    this.resetContent();
    this.initEventHandlers();
    this.isSpaceKeyPressed = !1;
    this.interactionMode = "boundsInteraction";
    this.interactionDisplay.interactionViewMode = "enabled";
    this.rotateBoundsIconUrl = 'data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" class="page" pagealignment="center-horizontally" x="0px" y="0px" width="25px" height="25px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve" xmlns:xml="http://www.w3.org/XML/1998/namespace"><defs></defs><style type="text/css"></style><style type="text/css"></style><g transform="matrix(1 0 0 1 0 0)" type="LAYER" name="workspace" id="workspace"></g><g transform="matrix(1 0 0 1 0 0)" id="Layer 01" type="LAYER" name="Layer 01"><g transform="matrix(1.0000000000000002 0 0 1.0000000000000002 -890.4860000000003 -620.8315000000002)"><g transform="matrix(1 0 0 1 0 0)"><path transform="matrix(1 0 0 1 0 0)" width="15.868000000000052" height="3.285250000000019" stroke-width="3" stroke-miterlimit="3" stroke="#FFFFFF" fill="none" d="M910.918,633.366 C906.534,637.744 899.431,637.744 895.05,633.364 "></path><path transform="matrix(1 0 0 1 0 0)" width="9.814014836795309" height="9.838838278931803" stroke-miterlimit="3" stroke="none" fill="#FFFFFF" d="M890.557,628.871 L900.3710148367953,632.8493041543028 L896.1879525222553,634.2499183976257 L894.5475727002968,638.7098382789318 Z "></path><path transform="matrix(1 0 0 1 0 0)" width="10.222026706231418" height="10.020298219584674" stroke-miterlimit="3" stroke="none" fill="#FFFFFF" d="M915.415,628.875 L911.3873353115728,638.8952982195847 L909.813657975555,634.3347845551633 L905.1929732937685,632.8863961424333 Z "></path></g></g><g transform="matrix(1.0000000000000002 0 0 1.0000000000000002 -890.4859999999987 -620.8315000000002)"><g transform="matrix(1 0 0 1 0 0)"><path transform="matrix(1 0 0 1 0 0)" width="15.868000000000052" height="3.285250000000019" stroke-width="1.26" stroke-miterlimit="3" stroke="#2E87FF" fill="none" d="M910.918,633.366 C906.534,637.744 899.431,637.744 895.05,633.364 "></path><path transform="matrix(1 0 0 1 0 0)" width="6.243000000000052" height="6.241999999999962" stroke-miterlimit="3" stroke="none" fill="#2E87FF" d="M891.905,630.219 L898.148,632.84 L895.286,633.599 L894.527,636.461 Z "></path><path transform="matrix(1 0 0 1 0 0)" width="6.2450000000000045" height="6.241000000000099" stroke-miterlimit="3" stroke="none" fill="#2E87FF" d="M914.066,630.223 L911.44,636.464 L910.683,633.601 L907.821,632.84 Z "></path></g></g></g></svg>';
    this.UNIT_PIXEL = 1;
    this.UNIT_KILOMETER = 3.53E-7;
    this.UNIT_METER = 3.52778E-4;
    this.UNIT_CENTIMETER = .035277778;
    this.UNIT_MILLIMETER = .352777778;
    this.UNIT_FOOT = .001157407;
    this.UNIT_INCH = .013888889;
    this.UNIT_YARD = 3.85802E-4;
    this.UNIT_MILE = 2.19E-7;
    this.UNIT_NAUTICAL_MILE = 1.9E-7;
    this.currentUnitOfMeasure = this.UNIT_PIXEL;
    this.fixedUnitDecimal = 2;
    this.drawMode = "wireframe";
    this.moveByArrowValue = this.gridSnapValue = 1;
    this.currentEditableProperty = "fill";
    this.initGradientsAndPatterns();
    this.lastTransformAction = null
}
_ = XOS_EditableDocument2D.prototype;
_.__defineGetter__("pageWidth", function() {
    return this.getPageSize().x
});
_.__defineSetter__("pageWidth", function(a) {
    this.setPageWidth(a)
});
_.__defineGetter__("pageHeight", function() {
    return this.getPageSize().y
});
_.__defineSetter__("pageHeight", function(a) {
    this.setPageHeight(a)
});
_.__defineGetter__("pageAlignment", function() {
    return this.getPageAlignment()
});
_.__defineSetter__("pageAlignment", function(a) {
    this.setPageAlignment(a)
});
_.onResize = function(a) {
    a = this.canvas.parentNode.getSize();
    0 != a.x && 0 != a.y && (this.interactionDisplay.canvas.width = this.canvas.width = a.x, this.interactionDisplay.canvas.height = this.canvas.height = a.y, this.invalidateRender(), this.interactionDisplay.invalidateRender())
};
_.onLoadImage = function(a) {
    this.invalidateRender();
    this.interactionDisplay.invalidateRender()
};
_.onLoadImageError = function(a) {
    this.invalidateRender()
};
_.createHtmlDocument = function(a, b) {
    var c = document.createElement("div");
    c.setAttribute("id", b);
    c.setAttribute("title", a);
    c.setAttribute("class", "document2D panel");
    var d = document.createElement("canvas");
    var Cids=$("#TABS_DOCUMENTS li.selected").attr('id');
    //d.setAttribute("id",Cids);
    $("#TABS_DOCUMENTS li").attr('ondblclick',"renameFiles()");
    $("#TABS_DOCUMENTS li.selected").attr('ondblclick',"renameFiles()");
    c.appendChild(d);
    d = document.createElement("canvas");
    c.appendChild(d);
    d = document.createElement("ul");
    d.setAttribute("class", "documentBar");
    c.appendChild(d);
    return c;
};
_.getDocumentFileExtension = function() {
    return "svg"
};
_.getDocumentMimeType = function() {
    return "image/svg+xml"
};
_.getDocumentTabIconClass = function() {
    return this.getDocumentFileExtension() + "_icon16x16"
};
_.openInBrowserAsImage = function(a) {
    this.isInIsolateLayerEditing() ? alert("To generate the image, exit from the group editing mode!") : this.currentPageToImage(a)
};
_.openInBrowserAsImage2 = function(a) {
    this.isInIsolateLayerEditing() ? alert("To generate the image, exit from the group editing mode!") : this.currentPageToImage2(a)
};
_.saveGifs = function(a)
{
this.saveImgForGIF(a);
};
_.canDocumentBeClosed = function() {
    return this.isChanged && !0 != confirm("The document is unsaved!\n Do you want to save design ?") ? !1 : !0
};
_.canDocumentBeSaved = function() {
    return this.isInIsolateLayerEditing() ? (alert("To save the document exit from the group editing mode!"), !1) : !0
};
_.getDocumentThumbnail = function() {
    return this.toImage(856, 614, !0, "png", .8, "#FFFFFF")
};
_.getSerializedDocument = function(a) {
    var b = new XOS_DisplayObject2DSerializer(this);
    if ("svg" == a) return b.serialize();
    if ("html" == a || "htm" == a || "php" == a) return b.serializeHTML()
};
_.createPrintableDocument = function(a) {
    var b = this.getPageAlignment();
    this.setPageAlignment("center-horizontally");
    var c = new XOS_DisplayObject2DSerializer(this);
    c.outputCSS = !1;
    a.innerHTML = c.serialize({
        printableVersion: !0
    });
    this.setPageAlignment(b)
};
_.openInBrowserAsSVG = function(a) {
    if (this.isInIsolateLayerEditing()) alert("To generate the Preview, exit from the group editing mode!");
    else {
        var b = this.htmlElement.title;
        a = b.split(".");
        1 < a.length && a.pop();
        b = a.join(a);
        a = this.getSerializedDocument("html");
        var b = window.open("text/html", b),
            c = b.document;
        c.write(a);
        c.close();
        b.focus()
    }
};
_.onDropData = function(a) {
    a.stopPropagation();
    a.preventDefault();
    if (this.getCurrentLayer().isLocked) return alert("The Layer target is locked!"), !1;
    var b = {
        x: 0,
        y: 0
    };
    GLOBAL_MOUSE_LISTENER.onMouseUp();
    GLOBAL_MOUSE_LISTENER.eventLocationToPoint(a, b);
    b = this.interactionDisplay.canvas.globalToLocal(b);
    if (null != GetTransferData()) {
        a = GetTransferData();
        if (a.hasAttribute && a.hasAttribute("downloadUrl") && a.hasAttribute("itemType")) {
            var c = a.getAttribute("itemType");
            if ("svg" == c || "htm" == c || "html" == c)this.onChange(),console.log("youtube"),
                this.placeSymbol(a.getAttribute("downloadUrl"), b.x, b.y);
            else if ("jpg" == c || "jpeg" == c || "png" == c) a = a.driveFileBrowserRef.getNodeLocalPath(a), this.placeImage(a, b.x, b.y)
        }
        SetTransferData(null)
    } else {
        (c = a.dataTransfer.getData("url")) && this.placeImage(c, b.x, b.y);
        if (0 < a.dataTransfer.files.length && (a = a.dataTransfer.files, 0 < a.length)) this.onDropFiles(a, b.x, b.y);
        return !1
    }
};
_.onDropFiles = function(a, b, c) {
    var d = this,
        e = null;
    if (0 < a.length)
        for (var f = 0; e = a[f]; f++) {
            var g = new FileReader,
                h = e.name.split("."),
                h = h[h.length - 1].toLowerCase();
            "svg" == h || "xml" == h || "htm" == h || "html" == h ? (g.onloadend = function(a) {
                a = (new DOMParser).parseFromString(a.target.result, "text/xml");
                d.onDropFile_SVG(a, b, c, {
                    ignorePageSize: !0
                })
            }, g.readAsText(e)) : "jpg" == h || "jpeg" == h || "png" == h || "gif" == h ? (g.onloadend = function(a) {
                d.onDropFile_IMG(a, b, c)
            }, g.readAsDataURL(e)) : alert("Unsupported file type:" + h)
        }
};
_.onDropFile_SVG = function(a, b, c, d) {
    var e = XOS_Node.CreateGlobalTimestamp();
    (new XOS_DisplayObject2DUnserializer(this)).unserialize(a, this.currentLayer, d);
    XOS_Node.RemoveGlobalTimestamp();
    a = this.getChildrenByPropertyValue("timestamp", e, !0);
    d = new RemoveGraphicObject_action(this);
    e = new AddGraphicObject_action(this);
    this.history.addUndoRedoActions(d, e);
    this.updateGlobalTransform();
    var f = this.content.localToGlobal({
        x: 0,
        y: 0
    });
    b -= f.x;
    c -= f.y;
    for (var f = a.length, g = 0; g < f; g++) a[g].parent instanceof XOS_Layer &&
        a[g].translate(b, c, !1), e.addObjectToAction(a[g]), d.addObjectToAction(a[g]);
    this.invalidateRender()
};
_.onDropFile_IMG = function(a, b, c) {
    this.placeImage(a.target.result, b, c)
};
_.placeSymbol = function(a, b, c) {
    function d(a) {

        e.hideLoadingState();
        a = a.target.responseText.replace(/\x3c!--startCDATA--\x3e/g, "<![CDATA[").replace(/\x3c!--endCDATA--\x3e/g, "]]\x3e");
         //a.innerHTML='<text x="0" y="15" fill="red">I love SVG!</text>';
        // a.replace("</svg>","<text x='0' y='15' fill='red'>I love SVG!</text></svg>");
         window.svgTemplates[0]=a;
         console.log(a);
        a = (new DOMParser).parseFromString(a, "text/xml").documentElement;
        console.log(a);
        e.onDropFile_SVG(a, b, c, {
            ignorePageSize: !0,
            ignoreLayers: !0
        });
        e.updateGlobalTransform();
        e.invalidateRender()
    }
    this.showLoadingState();
    var e = this; - 1 != a.indexOf("googleusercontent") ? LoadDriveFile(a, d) : SendAndLoad(a, null, d)
};
_.placeImage = function(a, b, c, d, e) {
    function f() {
        var b = new XOS_Image;
        b.load(a, function() {
            d ? b.setPosition(g.x - b.width / 2, g.y - b.height / 2) : b.setPosition(g.x, g.y);
            h.hideLoadingState();
            h.addGraphicObject_undable(b, null, -1);
            e && e(b)
        });
        b.name = "immagine";
        return b
    }
    var g = this.globalPointToContentPoint({
        x: b,
        y: c
    });
    this.showLoadingState();
    var h = this; - 1 != a.indexOf("googleusercontent") ? CheckSessionState(function() {
        f()
    }) : f()
};
_.showLoadingState = function() {
    this.hideLoadingState();
    var a = document.createElement("div");
    a.setAttribute("id", "PROGRESS_STATE");
    this.htmlElement.appendChild(a)
};
_.hideLoadingState = function() {
    var a = this.htmlElement.getChildById("PROGRESS_STATE");
    a && this.htmlElement.removeChild(a)
};
_.removeDocumentFromLocalStorage = function(a) {
    return window.localStorage.getItem(a) ? (window.localStorage.removeItem(a), !0) : !1
};
_.saveDocumentToLocalStorage = function(a) {
    if (!a) return !1;
    var b = (new XOS_DisplayObject2DSerializer(this)).serializeHTML();
        window.localStorage.setItem(a, b);
        //var cdd=window.localStorage.getItem(a);
        //console.log(cdd);
        //console.log(this.canvas.toDataURL("image/png")); 
//       window.myImgTempStorage[0]=this.canvas.toDataURL("image/png");
       /*$("myImgs123").attr("src",this.canvas.toDataURL("image/png"));
       var myImgsr=$("myImgs123");
       var h = this.canvas.getContext("2d");
       h.drawImage(myImgsr,10,10,100,100,0,0,this.canvas.width,this.canvas.height);
       console.log(myImgsr);*/
        myCustomDialog();
    return !0
};
_.loadDocumentFromLocalStorage = function(a) {
    if (!a) return !1;
    var b = window.localStorage.getItem(a);
    if (!b) return !1;
    a = new XOS_DisplayObject2DUnserializer(this);
    b = (new DOMParser).parseFromString(b, "text/xml");
    a.unserialize(b.documentElement, this.content);
    this.updateGlobalTransform();
    this.invalidateRender();
    return !0
};
_.loadDocumentFromDrive = function(a) {
    // alert(a);
    this.showLoadingState();
    var test= LoadDriveFile(a, this.onLoadDocumentFromDrive(a));
   // alert(test);
};
_.onLoadDocumentFromDrive = function(a) {
    this.hideLoadingState();
    var b = getHtmlData(a);
        a = new XOS_DisplayObject2DUnserializer(this);
            b = b.replace(/\x3c!--startCDATA--\x3e/g, "<![CDATA[").replace(/\x3c!--endCDATA--\x3e/g, "]]\x3e");
            b = (new DOMParser).parseFromString(b, "text/xml").documentElement;
            a.unserialize(b, this.content);
            this.updateGlobalTransform();
            this.invalidateRender();
            this.isChanged = !1;
            this.showSelectionProperties()
        };

function getHtmlData(a) {
   // $("#loadingPopups2").css("left","37% !important").show();
    $("#DRIVE_FILE_BROWSER_WIN").hide(); 
    var foldername=localStorage.getItem('projectName');
    var result="";
    $.ajax({
      url:base_url+"ant_tool_page/getHtmlFileContant/"+a,
      async: false,  
      success:function(data) {
        $("#loadingPopups2").hide();
         result = data;
      }
   });
   return result;
}

_.loadDocument = function(a, b) {
   // alert(a);
    this.showLoadingState();
    var c = this;
    SendAndLoad(a, null, function(a) {
        c.onLoadDocumentFromDrive(a);
        b && b(this)
    })
};
_.clearIsolateLayerEditingLabels = function() {
    this.docBar && (this.docBar.innerHTML = "", this.docBar.style.display = "none")
};
_.addIsolateLayerEditingLabel = function(a, b, c) {
    b = document.createElement("li");
    b.appendChild(document.createTextNode(a));
    this.docBar.appendChild(b);
    this.docBar.style.display = "block"
};
_.useSelectedImageAsPattern = function() {
    if (0 == this.selectionList.length) alert("Select an image to define the pattern!");
    else {
        var a = this.selectionList[0];
        a instanceof XOS_Image ? (this.deselectAllGraphicObjects(), this.setPatternOfSelectedObjects(a.image), this.showSelectionProperties()) : alert("Select an image to define the pattern!")
    }
};

_.custom_menu = function(){
   showMenuPopup();
   console.log(window.svgTemplates[0]);
}
_.initGradientsAndPatterns = function() {
    this.strokeGradient = new XOS_LinearGradient(this);
    this.strokeGradient.x1 = 0;
    this.strokeGradient.y1 = 0;
    this.strokeGradient.x2 = 100;
    this.strokeGradient.y2 = 100;
    this.strokeGradient.addColor({
        offset: 0,
        color: "#FFF"
    });
    this.strokeGradient.addColor({
        offset: 1,
        color: "#000"
    });
    this.fillGradient = new XOS_LinearGradient(this);
    this.fillGradient.x1 = 0;
    this.fillGradient.y1 = 0;
    this.fillGradient.x2 = 100;
    this.fillGradient.y2 = 100;
    this.fillGradient.addColor({
        offset: 0,
        color: "#FFF"
    });
    this.fillGradient.addColor({
        offset: 1,
        color: "#000"
    });
    this.strokePattern = new XOS_Pattern(this);
    this.strokePattern.load("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAIAAABvFaqvAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAADZJREFUeNpiZGBg+P//P5BkZGSkiAGhKAdMDFQCTEC3QViUMka9Nuq1Ua+Nem3Ua0PGawABBgCMajC1OPqsUwAAAABJRU5ErkJggg==");
    this.fillPattern = new XOS_Pattern(this);
    this.fillPattern.load("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAIAAABvFaqvAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAADZJREFUeNpiZGBg+P//P5BkZGSkiAGhKAdMDFQCTEC3QViUMka9Nuq1Ua+Nem3Ua0PGawABBgCMajC1OPqsUwAAAABJRU5ErkJggg==")
};
_.initEventHandlers = function() {
    DEVICE_WITH_TOUCH_EVENTS ? (CreateEventListener(this.interactionDisplay.canvas, "touchstart", this.onTouchStart, this), CreateEventListener(this.interactionDisplay.canvas, "touchmove", this.onTouchMove, this), CreateEventListener(this.interactionDisplay.canvas, "touchend", this.onTouchEnd, this), CreateEventListener(this.interactionDisplay.canvas, "gesturestart", this.onGestureStart, this), CreateEventListener(this.interactionDisplay.canvas, "gesturechange", this.onGestureChange, this),
        CreateEventListener(this.interactionDisplay.canvas, "gestureend", this.onGestureEnd, this)) : (CreateEventListener(this.interactionDisplay.canvas, "mousedown", this.onMouseDown, this), CreateEventListener(this.interactionDisplay.canvas, "mousemove", this.onMouseMove, this), CreateEventListener(this.interactionDisplay.canvas, "mouseup", this.onMouseUp, this), CreateEventListener(this.interactionDisplay.canvas, "dragover", this.handleDragOver, this), CreateEventListener(this.interactionDisplay.canvas, "drop", this.onDropData,
        this))
};
_.setCursor = function(a) {
    DEVICE_WITH_TOUCH_EVENTS || this.cursor == a || (this.cursor && this.interactionDisplay.canvas.classList.remove(this.cursor), this.interactionDisplay.canvas.classList.add(a), this.cursor = a)
};
_.getCursor = function() {
    return DEVICE_WITH_TOUCH_EVENTS ? null : this.cursor
};
_.onChange = function() {
    this.isChanged = !0
};
_.handleDragOver = function(a) {
    a.stopPropagation();
    a.preventDefault()
};
_.getInspectorPanelName = function() {
    return "pageProperties2DPanel"
};
_.setUnitOfMeasure = function(a) {
    this.currentUnitOfMeasure = a
};
_.setFixedDecimalToShow = function(a) {
    this.fixedUnitDecimal = a
};
_.baseToUnitOfMeasure = function(a) {
    return (a * this.currentUnitOfMeasure).toFixed(this.fixedUnitDecimal)
};
_.unitOfMeasureToBase = function(a) {
    return a / this.currentUnitOfMeasure
};
_.setPageWidth = function(a) {
    this.getLayerByName("workspace").children[0].width = a;
    this.fitToView();
    this.invalidateRender()
};
_.setPageHeight = function(a) {
    this.getLayerByName("workspace").children[0].height = a;
    this.fitToView();
    this.invalidateRender()
};
_.setPageSize = function(a, b) {
    var c = this.getLayerByName("workspace").children[0];
    c.width = Number(a);
    c.height = Number(b);
    this.invalidateRender()
};
_.getPageSize = function() {
    var a = this.getLayerByName("workspace").children[0];
    return {
        x: a.width,
        y: a.height
    }
};
_.getPageOrigin = function() {
    return this.getLayerByName("workspace").children[0].getPosition()
};
_.getPageAlignment = function() {
    return this.getPage().pageAlignment
};
_.setPageAlignment = function(a) {
    this.getPage().pageAlignment = a;
    this.onChange()
};
_.getPage = function() {
    return this.getLayerByName("workspace").children[0]
};
_.setScreenSnapValue = function(a) {
    this.screenSnapValue = a
};
_.setGridSnapValue = function(a) {
    this.gridSnapValue = a
};
_.setPageColor = function(a) {
    this.getLayerByName("workspace").children[0].fillColor = a;
    this.invalidateRender();
    this.onChange()
};
_.fitToView = function(a, b) {
    a = a || 100;
    b = b || 100;
    var c = this.canvas.parentNode.getSize(),
        d = this.getLayerByName("workspace").children[0],
        e = Math.min((c.x - a) / d.width, (c.y - b) / d.height);
    this.content.setScale(e, e);
    this.content.setPosition(c.x / 2 - d.width * e / 2, c.y / 2 - d.height * e / 2);
    this.interactionDisplay.visible = !0;
    this.interactionDisplay.invalidateRender();
    this.invalidateRender()
};
_.initTools = function() {
    this.tools.selectGlobal_tool = new XOS_Select_tool(this);
    this.tools.selectLocal_tool = new XOS_Select_tool(this);
    this.tools.selectLocal_tool.interactionMode = "pointsInteraction";
    this.tools.panView_tool = new XOS_Pan_tool(this);
    this.tools.zoomView_tool = new XOS_Zoom_tool(this);
    this.tools.drawRectangle_tool = new XOS_DrawRectangle_tool(this);
    this.tools.drawEllipse_tool = new XOS_DrawEllipse_tool(this);
    this.tools.drawLine_tool = new XOS_DrawLine_tool(this);
    this.tools.drawPath_tool = new XOS_DrawPath_tool(this);
    this.tools.drawFreeHand_tool = new XOS_DrawFreeHand_tool(this);
    this.tools.drawBrush_tool = new XOS_DrawBrush_tool(this);
    this.tools.rotate_tool = new XOS_Rotate_tool(this);
    this.tools.eyeDropper_tool = new XOS_EyeDropper_tool(this);
    this.tools.editGradientPoints_tool = new XOS_EditGradientPoints_tool(this);
    this.tools.translateObjects_tool = new XOS_TranslateObjects_tool(this);
    this.tools.splitPath_tool = new XOS_SplitPath_tool(this);
    this.tools.removePathPoint_tool = new XOS_RemovePathPoint_tool(this);
    this.tools.addPathPoint_tool =
        new XOS_AddPathPoint_tool(this);
    this.tools.convertPathPoint_tool = new XOS_ConvertPathPoint_tool(this);
    this.tools.drawTextBox_tool = new XOS_DrawTextBox_tool(this);
    this.tools.translatePointHandle_tool = new XOS_TranslatePointHandle2D_tool(this);
    this.tools.scaleBoundsHandle_tool = new XOS_ScaleBoundsHandle2D_tool(this);
    this.tools.rotateBoundsHandle_tool = new XOS_RotateBoundsHandle2D_tool(this)
};
_.setActiveToolByName = function(a, b) {
    //console.log(a);
    a || b && b.hasAttribute("toolName") && (a = b.getAttribute("toolName"));
    //console.log(this.tools[a]);
    if (this.tools[a]) {
        if (this.tools.activeTool != this.tools[a]) {
            var c = this.tools.activeTool;
            if (c) c.onDeactivate();
            this.tools.activeTool = this.tools[a];
            this.tools.activeTool.onActivate()
        }
        b && b.parentNode.parentNode.parentNode.classList.contains("toolbar") && (c = b.parentNode.parentNode, c.id = b.id, c.setAttribute("toolName", b.getAttribute("toolName")));
        this.updateActiveToolInterface()
    } else console.log("lo strumento: " +
        a + " non esiste ")
};
_.updateActiveToolInterface = function() {
    var a = "";
    for (a in this.tools)
        if (this.tools[a] == this.tools.activeTool) break;
    var b = HTML("MAIN_TOOLBAR_2D");
    if (b) {
        var c = b.querySelector(".selected");
        c && c.classList.remove("selected");
        c = b.querySelector('[toolName="' + a + '"]');
        c.classList.add("selected")
    }
};
_.doKeyDownAction = function(a) {
    var b = a.keyCode;
    if (!0 == a.metaKey || !0 == a.ctrlKey) {
        switch (b) {
            case 67:
                this.copySelectedGraphicObjects();
                break;
            case 86:
                b = {};
                a.altKey && (b = {
                    pasteInPlace: !0
                });
                this.pasteObjects(b);
                break;
            case 88:
                this.cutSelectedGraphicObjects();
                break;
            case 68:
                this.duplicateWithTransformAllSelectedGraphicObjects_undable();
                break;
            case 187:
            case 107:
                var b = 1.1,
                    c = {
                        x: this.canvas.width / 2,
                        y: this.canvas.height / 2
                    };
                this.scaleGraphicObjects([this.content], b, b, c);
                this.invalidateRender();
                this.interactionDisplay.invalidateRender();
                break;
            case 189:
            case 109:
                b = .9;
                c = {
                    x: this.canvas.width / 2,
                    y: this.canvas.height / 2
                };
                this.scaleGraphicObjects([this.content], b, b, c);
                this.invalidateRender();
                this.interactionDisplay.invalidateRender();
                break;
            case 65:
                this.selectAll();
                break;
            case 71:
                a.shiftKey ? this.ungroupSelectedGraphicObjects() : this.groupSelectedGraphicObjects();
                break;
            case 90:
                this.undo();
                break;
            case 89:
                this.redo()
        }
        return EventPreventDefault(a)
    }
    switch (b) {
        case 8:
        case 46:
            return this.removeSelectedGraphicObjects_undable(), EventPreventDefault(a);
        case 37:
            return "boundsInteraction" == this.interactionMode ? this.translateGraphicObjects_undable(this.selectionList, -this.moveByArrowValue, 0) : this.translateSelectedHandlePoints_undable(-this.moveByArrowValue, 0), EventPreventDefault(a);
        case 38:
            return "boundsInteraction" == this.interactionMode ? this.translateGraphicObjects_undable(this.selectionList, 0, -this.moveByArrowValue) : this.translateSelectedHandlePoints_undable(0, -this.moveByArrowValue), EventPreventDefault(a);
        case 39:
            return "boundsInteraction" == this.interactionMode ?
                this.translateGraphicObjects_undable(this.selectionList, this.moveByArrowValue, 0) : this.translateSelectedHandlePoints_undable(this.moveByArrowValue, 0), EventPreventDefault(a);
        case 40:
            return "boundsInteraction" == this.interactionMode ? this.translateGraphicObjects_undable(this.selectionList, 0, this.moveByArrowValue) : this.translateSelectedHandlePoints_undable(0, this.moveByArrowValue), EventPreventDefault(a);
        case 32:
            this.isSpaceKeyPressed = !0
    }
};
_.doKeyUpAction = function(a) {
    var b = a.srcElement ? a.srcElement : a.target;
    if ("INPUT" != b.tagName && "TEXTAREA" != b.tagName) return 32 == a.keyCode ? (this.isSpaceKeyPressed = !1, EventPreventDefault(a)) : null
};
_.draw = function(a) {
    XOS_EditableDocument2D.superClass.draw.call(this, a);
    this.interactionDisplay.visible && this.interactionDisplay.invalidRender && (this.interactionDisplay.clear(), this.interactionDisplay.graphicContext.lineWidth = 1 / this.content.contentScale.x, this.drawGraphicObjectListAsSelected(this.selectionList), this.interactionDisplay.draw(!1));
    this.isMagnifiedViewEnabled && this.drawMagnifiedView()
};
_.drawWireframe = function() {
    !1 != this.invalidRender && (this.clear(), this.invalidTransform && this.updateGlobalTransform(), this.applyTransform(), this.graphicContext.globalCompositeOperation = "source-over", this.drawChildrenWireframe(), this.invalidRender = !1)
};
_.undo = function() {
    this.deselectAllGraphicObjects();
    this.history.undo()
};
_.redo = function() {
    this.deselectAllGraphicObjects();
    this.history.redo()
};
_.resetContent = function() {
    this.history = new XOS_History;
    this.selectionList = [];
    this.content.removeAllChildren();
    this.addLayer("workspace").isLocked = !0;
    var a = new XOS_Page;
    this.addGraphicObject(a)
};
_.addDefaultLayers = function() {
    this.addLayer("Layer 01");
    this.showSelectionProperties()
};
_.addLayer = function(a, b) {
    var c = new XOS_Layer;
    c.name = a;
    this.addGraphicObject(c, this.content, b);
    this.setCurrentLayerByName(a);
    return c
};
_.addLayer_undable = function(a, b) {
    var c = new XOS_Layer;
    c.name = a;
    this.addGraphicObject_undable(c, this.content, b);
    return c
};

_.copyNewLayers = function(a, b)
{
    var all_layers_length=$("#LAYERS_2D_PANEL li").length;
    var visibleTrue_leyer_length=$("#LAYERS_2D_PANEL li.visibleTrue").length;
    var visibleFalse_leyer_length=$("#LAYERS_2D_PANEL li.visibleFalse").length;
    if(visibleTrue_leyer_length == 2)
    {
        var c = new XOS_Layer;
        c.name = a;
       
          this.getLayers()[this.getLayers().length-1].idi=+12;

          // console.log(this.getLayers()[0]);
        c.id=Math.random();
        this.selectAll();
        this.copySelectedGraphicObjects();
        c.children=this.pasteMyObjects();
        this.addGraphicObject_undable(c, this.content, b);
        console.log(c);
       // console.log(this.getLayers()[1].children.length);
        if((this.getCurrentLayer().children.length - 1)== 1)
        {
            //console.log("one");
            this.getCurrentLayer().children.splice(1,1);
        }
        else
        {
            //console.log("more than one");
            this.getCurrentLayer().children.splice((this.getCurrentLayer().children.length)/2,(this.getCurrentLayer().children.length)/2);
        }
    }
    else
    {
        alert("Please disable all the layers except selected one.");
    }

    
    
    //an.splice(an, 1);
};
_.removeLayer_undable = function(a) {
    a.displayRef.replaceGraphicObjects_undable([a], [], this.content)
};
_.moveLayer_undable = function(a, b) {
    function c() {
        a.parent.setChildIndex(a, e);
        g.currentLayer = a;
        g.invalidateRender();
        g.showSelectionProperties()
    }

    function d() {
        a.parent.setChildIndex(a, f);
        g.currentLayer = a;
        g.invalidateRender();
        g.showSelectionProperties()
    }
    var e = a.parent.getChildIndex(a),
        f;
    "up" == b ? f = e + 1 : "down" == b && (f = e - 1);
    if (!1 != a.parent.setChildIndex(a, f)) {
        var g = this;
        this.history.addUndoRedoActions(new XOS_Action(c), new XOS_Action(d));
        d()
    }
};
_.moveSelectedGraphicObjectsInCurrentLayer = function() {
    this.copySelectedGraphicObjects();
    this.removeSelectedGraphicObjects_undable();
    this.pasteObjects({
        ignoreLayers: !0,
        pasteInPlace: !0
    })
};
_.getLayers = function() {
    return this.content.children
};
_.getLayerByName = function(a) {
    return this.content.getChildByName(a)
};
_.setCurrentLayerByName = function(a) {
    this.currentLayer = this.getLayerByName(a)
};
_.setCurrentLayer = function(a) {
    this.currentLayer = a
};
_.getCurrentLayer = function() {
    return this.currentLayer
};
_.lockSelectedGraphicObjects = function() {
    0 != this.selectionList.length && (this.setPropertiesOfSelectedGraphicObjects_undable({
        isLocked: !0
    }), this.deselectAllGraphicObjects())
};
_.createComposedPathFromSelection = function() {
    var a = this.selectionList.length;
    if (!(2 > a)) {
        var b = this.sortGraphicObjectListByIndex(this.selectionList);
        if (null != b) {
            for (var c = b[0].clone(), d = c.paths[0].isClockwise(), e, f, g, h = 1; h < a; h++) e = b[h], f = e.paths[0].clone(), g = f.isClockwise(), d ? g && f.revertPoints() : g || f.revertPoints(), e.localPointsToGlobalPoints(f.getEditablePoints(), !0), c.globalPointsToLocalPoints(f.getEditablePoints(), !0), c.paths.add(f);
            this.replaceGraphicObjects_undable(b, [c])
        }
    }
};
_.createMaskFromSelection = function() {
    if (!(2 > this.selectionList.length)) {
        var a = this.sortGraphicObjectListByIndex(this.selectionList);
        null != a && (a[a.length - 1] instanceof XOS_Shape ? (a = this.groupSelectedGraphicObjects(), a.mask = a.children[a.children.length - 1], a.mask.isMask = !0) : alert("The mask must be a shape over all others objects"))
    }
};
_.getColorAtPoint = function(a) {
    this.canvas.getBoundingClientRect();
    a = this.graphicContext.getImageData(a.x, a.y, 1, 1).data;
    return {
        r: a[0],
        g: a[1],
        b: a[2]
    }
};
_.unlockAllGraphicObjects = function() {
    for (var a = [], b = this.content.children, c = b.length, d, e = 1; e < c; e++) d = b[e], a = a.concat(d.getChildrenByPropertyValue("isLocked", !0));
    this.setPropertiesOfObjects_undable({
        isLocked: !1
    }, a)
};
_.isInIsolateLayerEditing = function() {
    return this.getCurrentLayer().isolateObjectRef ? !0 : !1
};
_.editGraphicObjectInIsolateLayer = function(a) {
    var b = a.type;
    a.id && (b = a.id);
    b = this.addLayer_undable(b);
    b.isolateObjectRef = a;
    for (var c = this.content.globalTransform.clone().invert(), d = [], e, f = a.children.length, g = 0; g < f; g++) e = a.children[g].clone(), e.localTransform = c.concat(a.children[g].globalTransform.clone()), d.push(e);
    this.replaceGraphicObjects_undable([], d, b);
    this.setPropertiesOfObjects_undable({
        visible: !1
    }, [a], !0, !0)
};
_.exitFromIsolateLayerEditing = function() {
    var a = this.getLayers(),
        a = a[a.length - 1],
        b = a.isolateObjectRef.clone();
    b.children = [];
    for (var c = b.globalTransform.clone().invert(), d, e = a.children.length, f = 0; f < e; f++) d = a.children[f].clone(b), d.isMask && (b.mask = d), d.localTransform = c.concat(d.globalTransform);
    this.replaceGraphicObjects_undable([a.isolateObjectRef], [b], a.isolateObjectRef.parent, a.isolateObjectRef.parent.getChildIndex(a.isolateObjectRef));
    this.removeLayer_undable(a)
};
_.updateInstancesSymbol = function(a) {
    var b = this.getSymbolFromLibrary(a);
    a = this.getChildrenByPropertyValue("idSymbolRef", a);
    for (var c = a.length, d = 0; d < c; d++) a[d].children = [], b.cloneChildren(a[d]), a[d].invalidateBounds();
    this.invalidateRender()
};
_.addGraphicObject = function(a, b, c) {
    null == b && (b = this.currentLayer);
    b.addChild(a);
    null != c && -1 < c && b.setChildIndex(a, c)
};
_.addGraphicObject_undable = function(a, b, c) {
    this.replaceGraphicObjects_undable([], [a], b, c);
    this.onChange()
};
_.removeSelectedGraphicObjects = function() {
    for (var a = null, b = this.selectionList.length, c = 0; c < b; c++) a = this.selectionList[c], a.isSelected = !1, a.parent.removeChild(a);
    this.selectionList = [];
    this.currentEditableGraphicObj = null;
    this.removeAllObjectInteraction();
    this.invalidateRender();
    this.interactionDisplay.invalidateRender()
};
_.removeSelectedGraphicObjects_undable = function() {
    0 != this.selectionList.length && this.replaceGraphicObjects_undable(this.selectionList, [], null, null)
};
_.replaceGraphicObjects_undable = function(a, b, c, d) {
    function e() {
        l.deselectAllGraphicObjects();
        for (var a = g.length, e = 0; e < a; e++) f = g[e].object, f.parent.removeChild(f);
        for (var h = d, a = b.length, e = 0; e < a; e++)
            if (f = b[e], null != d && (h += e), l.addGraphicObject(f, c, h), f instanceof XOS_Layer || l.selectGraphicObject(f, !0), f instanceof XOS_InstanceSymbol2D) {
                var k = f.idSymbolRef;
                l.symbolLibrary[k] = f.clone();
                l.updateInstancesSymbol(k)
            }
        l.onChange();
        l.invalidateRender();
        l.showSelectionProperties()
    }
    for (var f, g = [], h = a.length, k =
            0; k < h; k++) f = a[k], g.push({
        object: f,
        objectParent: f.parent,
        index: f.parent.getChildIndex(f)
    });
    g.sort(function(a, b) {
        return a.index - b.index
    });
    var l = this;
    null == c && (c = this.currentLayer);
    this.history.addUndoRedoActions(new XOS_Action(function() {
        var a, d;
        l.deselectAllGraphicObjects();
        for (var e = b.length, h = 0; h < e; h++) f = b[h], c.removeChild(f);
        e = g.length;
        for (h = 0; h < e; h++) f = g[h].object, d = g[h].objectParent, a = g[h].index, l.addGraphicObject(f, d, a), f instanceof XOS_Layer || l.selectGraphicObject(f, !0), f instanceof XOS_InstanceSymbol2D &&
            (a = f.idSymbolRef, l.symbolLibrary[a] = f.clone(), l.updateInstancesSymbol(a));
        l.onChange();
        l.invalidateRender();
        l.showSelectionProperties()
    }), new XOS_Action(e));
    e()
};
_.cutSelectedGraphicObjects = function() {
    this.copySelectedGraphicObjects();
    this.removeSelectedGraphicObjects_undable()
};
_.copySelectedGraphicObjects = function() {
    var a = (new XOS_DisplayObject2DSerializer(this)).serializeSelection(),
        b = {
            ignoreLayers: !1
        };
    this.currentLayer.isolateObjectRef || (b.ignoreLayers = !0);
    SetClipboardData({
        properties: b,
        content: a
    })
};
_.pasteObjects = function(a) {
    this.deselectAllGraphicObjects();
    var b = XOS_Node.CreateGlobalTimestamp(),
        c = GetClipboardData(),
        d = c.properties,
        e = c.content.replace(/\x3c!--startCDATA--\x3e/g, "<![CDATA[").replace(/\x3c!--endCDATA--\x3e/g, "]]\x3e"),
        c = new XOS_DisplayObject2DUnserializer(this),
        e = (new DOMParser).parseFromString(e, "text/xml"),
        f = {
            ignorePageSize: !0
        };
    if (a && a.ignoreLayers || d.ignoreLayers) f.ignoreLayers = !0;
    this.currentLayer.isolateObjectRef && (f.ignoreLayers = !0);
    c.unserialize(e.documentElement, this.currentLayer,
        f);
    XOS_Node.RemoveGlobalTimestamp();
    b = this.getChildrenByPropertyValue("timestamp", b, !0);
    d = new RemoveGraphicObject_action(this);
    c = new AddGraphicObject_action(this);
    this.history.addUndoRedoActions(d, c);
    e = b.length;
    for (f = 0; f < e; f++) c.addObjectToAction(b[f]), d.addObjectToAction(b[f]), b[f] instanceof XOS_Layer ? this.selectAllInLayer(b[f]) : this.selectGraphicObject(b[f], !0);
    if (a && a.pasteInPlace) this.onChange();
    else 0 < this.selectionList.length && (this.content.updateGlobalTransform(), a = this.getSelectionBounds().getCenter(),
        this.translateGraphicObjects(this.selectionList, this.interactionDisplay.canvas.width / 2 - a.x, this.interactionDisplay.canvas.height / 2 - a.y, !1));
    this.onChange();
    this.invalidateRender();
    this.showSelectionProperties()
};

_.pasteMyObjects = function(a) {
    this.deselectAllGraphicObjects();
    var b = XOS_Node.CreateGlobalTimestamp(),
        c = GetClipboardData(),
        d = c.properties,
        e = c.content.replace(/\x3c!--startCDATA--\x3e/g, "<![CDATA[").replace(/\x3c!--endCDATA--\x3e/g, "]]\x3e"),
        c = new XOS_DisplayObject2DUnserializer(this),
        e = (new DOMParser).parseFromString(e, "text/xml"),
        f = {
            ignorePageSize: !0
        };
    if (a && a.ignoreLayers || d.ignoreLayers) f.ignoreLayers = !0;
    this.currentLayer.isolateObjectRef && (f.ignoreLayers = !0);
    c.unserialize(e.documentElement, this.currentLayer,
        f);
    XOS_Node.RemoveGlobalTimestamp();
    b = this.getChildrenByPropertyValue("timestamp", b, !0);
    d = new RemoveGraphicObject_action(this);
    c = new AddGraphicObject_action(this);
    this.history.addUndoRedoActions(d, c);
    e = b.length;
    for (f = 0; f < e; f++) c.addObjectToAction(b[f]), d.addObjectToAction(b[f]), b[f] instanceof XOS_Layer ? this.selectAllInLayer(b[f]) : this.selectGraphicObject(b[f], !0);
    if (a && a.pasteInPlace) this.onChange();
    else 0 < this.selectionList.length && (this.content.updateGlobalTransform(), a = this.getSelectionBounds().getCenter(),
        this.translateGraphicObjects(this.selectionList, this.interactionDisplay.canvas.width / 2 - a.x, this.interactionDisplay.canvas.height / 2 - a.y, !1));
    this.onChange();
    this.invalidateRender();
    this.showSelectionProperties();
    return b;
};
_.changeSelectedGraphicObjectsIndex_undable = function(a) {
    var b = this.selectionList.length;
    if (0 != b) {
        var c = new SetGraphicObjectIndex_action(this),
            d = new SetGraphicObjectIndex_action(this);
        this.history.addUndoRedoActions(c, d);
        for (var e = [], f = null, g = null, h = 0, h = g = 0; h < b; h++) f = this.selectionList[h], g = f.parent.getChildIndex(f), c.addObjectToAction(f, g), e.push({
            displayObject: f,
            index: g
        });
        if ("front" == a)
            for (e.sort(function(a, b) {
                    return a.index - b.index
                }), g = e.length, h = 0; h < g; h++) f = e[h].displayObject, f.parent.setChildIndex(f,
                f.parent.numChildren() - 1);
        else if ("back" == a)
            for (e.sort(function(a, b) {
                    return b.index - a.index
                }), g = e.length, h = 0; h < g; h++) f = e[h].displayObject, f.parent.setChildIndex(f, 0);
        else if ("1" == a)
            for (e.sort(function(a, b) {
                    return a.index - b.index
                }), g = e.length, h = 0; h < g; h++) f = e[h].displayObject, f.parent.setChildIndex(f, e[h].index + 1);
        else if ("-1" == a)
            for (e.sort(function(a, b) {
                    return b.index - a.index
                }), g = e.length, h = 0; h < g; h++) f = e[h].displayObject, f.parent.setChildIndex(f, e[h].index - 1);
        for (h = 0; h < b; h++) f = this.selectionList[h],
            g = f.parent.getChildIndex(f), d.addObjectToAction(f, g);
        this.onChange();
        this.invalidateRender()
    }
};
_.saveChangedContentViewToHistory = function(a, b) {
    var c = new SetObjectProperties_action(this),
        d = new SetObjectProperties_action(this);
    this.history.addUndoRedoActions(c, d);
    this.content.localTransform = a;
    c.addObjectToAction(this.content, "localTransform", Bind(this.content, this.content.invalidateTransform));
    this.content.localTransform = b;
    d.addObjectToAction(this.content, "localTransform", Bind(this.content, this.content.invalidateTransform))
};
_.setPropertiesOfObjects = function(a, b, c, d) {
    var e = b.length;
    if (0 != e) {
        for (var f = 0; f < e; f++)
            for (var g in a) b[f][g] = a[g];
        c && this.invalidateRender();
        d && this.interactionDisplay.invalidateRender()
    }
};
_.setPropertiesOfObjects_undable = function(a, b, c, d, e) {
    var f = b.length;
    if (0 != f) {
        var g = new SetObjectProperties_action(this),
            h = new SetObjectProperties_action(this);
        this.history.addUndoRedoActions(g, h);
        var k = [],
            l;
        for (l in a) k.push(l);
        for (var k = k.join(","), q = 0; q < f; q++) {
            g.addObjectToAction(b[q], k, e);
            for (l in a) b[q][l] = a[l];
            h.addObjectToAction(b[q], k, e)
        }
        this.graphicObjectPropertyToGlobal(b[f - 1]);
        c && this.invalidateRender();
        d && this.interactionDisplay.invalidateRender();
        this.onChange()
    }
};
_.setPropertiesOfSelectedGraphicObjects_undable = function(a, b) {
    var c = this.selectionList;
    b && (this.selectionList = this.getAllNonGroupElementsInDepthAsArray(this.selectionList));
    this.setPropertiesOfObjects_undable(a, this.selectionList, !0, !0);
    this.selectionList = c
};
_.getAllNonGroupElementsInDepthAsArray = function(a) {
    for (var b = [], c = a.length, d = 0; d < c; d++) a[d] instanceof XOS_Group2D ? a[d].getAllNonGroupChildrenInDepthAsArray(b) : b.push(a[d]);
    return b
};
_.duplicateWithTransformAllSelectedGraphicObjects_undable = function() {
    this.duplicateAllSelectedGraphicObjects_undable();
    this.lastTransformAction ? this.lastTransformAction() : this.translateGraphicObjects(this.selectionList, 10, 10, !1);
    this.onChange();
    this.invalidateRender();
    this.interactionDisplay.invalidateRender()
};
_.duplicateAllSelectedGraphicObjects_undable = function() {
    var a = this.selectionList.length;
    if (0 != a) {
        for (var b = null, c = null, d = [], e = 0; e < a; e++) c = this.selectionList[e].parent, b = c.getChildIndex(this.selectionList[e]), d.push({
            obj: this.selectionList[e],
            index: b,
            layer: c
        });
        d.sort(function(a, b) {
            return a.index - b.index
        });
        this.deselectAllGraphicObjects();
        var b = null,
            c = new RemoveGraphicObject_action(this),
            f = new AddGraphicObject_action(this);
        this.history.addUndoRedoActions(c, f);
        for (e = 0; e < a; e++) b = d[e].obj.clone(), this.addGraphicObject(b,
            d[e].layer, -1), f.addObjectToAction(b), c.addObjectToAction(b), this.selectGraphicObject(b, !0);
        this.showSelectionProperties()
    }
};
_.setGradientsPointsOfSelectedGraphicObjects_undable = function(a, b) {
    if (0 != this.selectionList.length) {
        var c = new SetObjectProperties_action(this),
            d = new SetObjectProperties_action(this);
        this.history.addUndoRedoActions(c, d);
        for (var e, f, g, h = this.selectionList.length, k = 0; k < h; k++) {
            e = this.selectionList[k];
            f = null;
            switch (this.currentEditableProperty) {
                case "stroke":
                    e.strokeFillingType == XOS_DisplayObject2D.GRADIENT_FILLING && (f = e.strokeGradient);
                    break;
                case "fill":
                    e.fillFillingType == XOS_DisplayObject2D.GRADIENT_FILLING &&
                        (f = e.fillGradient)
            }
            f && (g = e.globalToLocal(a), e = e.globalToLocal(b), f instanceof XOS_RadialGradient ? (c.addObjectToAction(f, "x1,y1,x2,y2,r2", Bind(f, f.invalidateGradient)), f.x1 = f.x2 = g.x, f.y1 = f.y2 = g.y, f.r2 = e.distanceFromPoint(g), d.addObjectToAction(f, "x1,y1,x2,y2,r2", Bind(f, f.invalidateGradient))) : f instanceof XOS_LinearGradient && (c.addObjectToAction(f, "x1,y1,x2,y2", Bind(f, f.invalidateGradient)), f.x1 = g.x, f.y1 = g.y, f.x2 = e.x, f.y2 = e.y, d.addObjectToAction(f, "x1,y1,x2,y2", Bind(f, f.invalidateGradient))), f.invalidateGradient())
        }
        this.invalidateRender();
        this.onChange()
    }
};
_.reflectSelectedGraphicObjects_undable = function(a) {
    var b = this.selectionList.length;
    if (0 != b) {
        var b = (1 == b ? this.interactionDisplay.children[0].displayObject2DRef.getGlobalBounds() : this.interactionDisplay.children[0].bounds).getCenter(),
            c = 1,
            d = 1;
        switch (a) {
            case "horizontal":
                c = -1;
                break;
            case "vertical":
                d = -1
        }
        this.scaleGraphicObjects(this.selectionList, c, d, b, !1);
        a = new ScaleGraphicObject_action(this);
        var e = new ScaleGraphicObject_action(this);
        a.isLocalMode = e.isLocalMode = !1;
        this.history.addUndoRedoActions(a, e);
        for (var f = this.selectionList.length, g = 0; g < f; g++) a.addObjectToAction(this.selectionList[g]), e.addObjectToAction(this.selectionList[g]);
        a.setScaleValues(1 / c, 1 / d, b);
        e.setScaleValues(c, d, b);
        this.onChange();
        this.invalidateRender();
        this.interactionDisplay.invalidateRender()
    }
};
_.alignSelectedGraphicObjects_undable = function(a) {
    var b = this.selectionList.length;
    if (!(2 > b)) {
        var c = this.interactionDisplay.children[0].bounds,
            d = new SetObjectProperties_action(this),
            e = new SetObjectProperties_action(this);
        this.history.addUndoRedoActions(d, e);
        for (var f, g = 0; g < b; g++) {
            f = this.selectionList[g];
            d.addObjectToAction(f, "x,y");
            var h = f.getGlobalBounds();
            switch (a) {
                case "left":
                    f.translate(c.min.x - h.min.x, 0, !1);
                    break;
                case "vcenter":
                    f.translate(c.getCenter().x - h.getCenter().x, 0, !1);
                    break;
                case "right":
                    f.translate(c.max.x -
                        h.max.x, 0, !1);
                    break;
                case "top":
                    f.translate(0, c.min.y - h.min.y, !1);
                    break;
                case "hcenter":
                    f.translate(0, c.getCenter().y - h.getCenter().y, !1);
                    break;
                case "bottom":
                    f.translate(0, c.max.y - h.max.y, !1)
            }
            e.addObjectToAction(f, "x,y")
        }
        this.invalidateRender();
        this.interactionDisplay.invalidateRender()
    }
};
_.distributeSelectedGraphicObjects_undable = function(a) {
    var b = this.selectionList.length;
    if (!(3 > b)) {
        var c = new SetObjectProperties_action(this),
            d = new SetObjectProperties_action(this);
        this.history.addUndoRedoActions(c, d);
        var e = this.interactionDisplay.children[0].bounds.clone(),
            f, g, h, k;
        switch (a) {
            case "left":
                this.selectionList.sort(function(a, b) {
                    return a.getGlobalBounds().min.x - b.getGlobalBounds().min.x
                });
                h = this.selectionList[b - 1].getGlobalBounds();
                e.max.x = h.min.x;
                f = e.getWidth() / (b - 1);
                break;
            case "vcenter":
                this.selectionList.sort(function(a,
                    b) {
                    return a.getGlobalBounds().min.x - b.getGlobalBounds().min.x
                });
                k = this.selectionList[0].getGlobalBounds();
                h = this.selectionList[b - 1].getGlobalBounds();
                e.min.x = k.getCenter().x;
                e.max.x = h.getCenter().x;
                f = e.getWidth() / (b - 1);
                break;
            case "right":
                this.selectionList.sort(function(a, b) {
                    return a.getGlobalBounds().min.x - b.getGlobalBounds().min.x
                });
                k = this.selectionList[0].getGlobalBounds();
                e.min.x = k.max.x;
                f = e.getWidth() / (b - 1);
                break;
            case "top":
                this.selectionList.sort(function(a, b) {
                    return a.getGlobalBounds().min.y -
                        b.getGlobalBounds().min.y
                });
                k = this.selectionList[b - 1].getGlobalBounds();
                e.max.y = k.min.y;
                g = e.getHeight() / (b - 1);
                break;
            case "hcenter":
                this.selectionList.sort(function(a, b) {
                    return a.getGlobalBounds().min.y - b.getGlobalBounds().min.y
                });
                h = this.selectionList[0].getGlobalBounds();
                k = this.selectionList[b - 1].getGlobalBounds();
                e.min.y = h.getCenter().y;
                e.max.y = k.getCenter().y;
                g = e.getHeight() / (b - 1);
                break;
            case "bottom":
                this.selectionList.sort(function(a, b) {
                    return a.getGlobalBounds().min.y - b.getGlobalBounds().min.y
                });
                h = this.selectionList[0].getGlobalBounds();
                e.min.y = h.max.y;
                g = e.getHeight() / (b - 1);
                break;
            case "hspace":
                this.selectionList.sort(function(a, b) {
                    return a.getGlobalBounds().min.x - b.getGlobalBounds().min.x
                });
                for (h = f = 0; h < b; h++) f += this.selectionList[h].getGlobalBounds().getWidth();
                f = (e.getWidth() - f) / (b - 1);
                k = this.selectionList[0].getGlobalBounds();
                h = this.selectionList[b - 1].getGlobalBounds();
                e.min.x = k.max.x;
                e.max.x = h.min.x;
                break;
            case "vspace":
                this.selectionList.sort(function(a, b) {
                    return a.getGlobalBounds().min.y -
                        b.getGlobalBounds().min.y
                });
                for (h = g = 0; h < b; h++) g += this.selectionList[h].getGlobalBounds().getHeight();
                g = (e.getHeight() - g) / (b - 1);
                h = this.selectionList[0].getGlobalBounds();
                k = this.selectionList[b - 1].getGlobalBounds();
                e.min.y = h.max.y;
                e.max.y = k.min.y
        }
        var l;
        for (h = 1; h < b - 1; h++) {
            k = this.selectionList[h];
            c.addObjectToAction(k, "x,y");
            l = k.getGlobalBounds();
            switch (a) {
                case "left":
                    k.translate(e.min.x + f - l.min.x, 0, !1);
                    f += f;
                    break;
                case "vcenter":
                    k.translate(e.min.x + f - l.getWidth() / 2 - l.min.x, 0, !1);
                    f += f;
                    break;
                case "right":
                    k.translate(e.min.x +
                        f - l.getWidth() - l.min.x, 0, !1);
                    f += f;
                    break;
                case "top":
                    k.translate(0, e.min.y + g - l.min.y, !1);
                    g += g;
                    break;
                case "hcenter":
                    k.translate(0, e.min.y + g - l.getHeight() / 2 - l.min.y, !1);
                    g += g;
                    break;
                case "bottom":
                    k.translate(0, e.min.y + g - l.getHeight() - l.min.y, !1);
                    g += g;
                    break;
                case "hspace":
                    k.translate(e.min.x + f - l.min.x, 0, !1);
                    e.min.x = e.min.x + f + l.getWidth();
                    break;
                case "vspace":
                    k.translate(0, e.min.y + g - l.min.y, !1), e.min.y = e.min.y + g + l.getHeight()
            }
            d.addObjectToAction(k, "x,y")
        }
        this.invalidateRender();
        this.interactionDisplay.invalidateRender()
    }
};
_.editGraphicObjectText = function(a) {
    this.isTextEditingAction = !0;
    var b = a.toHTMLElement();
    this.htmlElement.appendChild(b);
    b.focus();
    b.selectAllTextInElement();
    a.visible = !1;
    this.invalidateRender();
    this.interactionDisplay.clear()
};
_.updateEditedGraphicObjectText = function() {
    var a = HTML("HTML_TEXT_CONTAINER");
    a.blur();
    a.textGraphicObjectRef.visible = !0;
    for (var b = "", b = document.body.innerText ? a.innerText : htmlToText(a.innerHTML), c = b.length - 1;
        "\n" == b.charAt(c);) c--;
    b = b.substring(0, c + 1);
    this.setPropertiesOfObjects_undable({
        innerText: b
    }, [a.textGraphicObjectRef], !0, !0);
    a.textGraphicObjectRef.invalidateLines();
    this.isTextEditingAction = !1;
    a.parentNode.removeChild(a);
    this.onChange()
};
_.textToPath_undable = function() {
    function a(a, c) {
        if (a) alert("Font could not be loaded: " + a);
        else {
            var g = b.toShape(c);
            d.replaceGraphicObjects_undable([b], [g], b.parent)
        }
    }
    if (0 != this.selectionList.length && this.selectionList[0] instanceof XOS_Text) {
        var b = this.selectionList[0],
            c = this.appRef.fontFamilyList.getFontFamilyDataByFontName(b.font);
        if (c) {
            var d = this;
            opentype.load(c.files.regular, a)
        }
    }
};
_.setFontFamilyOfGraphicObjects = function(a, b, c, d) {
    this.activeFontFamily = b;
    var e = this;
    c ? WebFont.load({
        google: {
            families: [this.activeFontFamily.replace(/ /g, "+")]
        },
        active: function() {
            e.setPropertiesOfObjects_undable({
                font: b
            }, a, !0, !0);
            d && d()
        },
        inactive: function() {
            console.log("il font " + b + " non pu\u00f2 essere caricato.")
        }
    }) : e.setPropertiesOfObjects_undable({
        font: b
    }, a, !0, !0)
};
_.calculateFontMetrics = function(a, b) {
    var c = document.createElement("span");
    c.style.font = b + "px " + a;
    c.textContent = "M";
    document.body.appendChild(c);
    var d = c.offsetHeight,
        e = d - b;
    document.body.removeChild(c);
    return {
        lineHeight: d,
        descent: e
    }
};
_.joinSelectedPoints_undable = function() {
    if ("pointsInteraction" == this.interactionMode && 0 != this.selectionList.length) {
        for (var a, b = [], c = this.interactionDisplay.children.length, d = 0; d < c; d++) a = this.interactionDisplay.children[d], b = b.concat(a.getSelectedHandles(!1));
        if (2 == b.length) {
            var e = b[0].pointRef,
                f = b[1].pointRef;
            if (e.pathRef == f.pathRef) {
                var g = e.pathRef;
                if (!g.closePath) {
                    var h = g.points;
                    if (e == g.getFirstPoint() && f == g.getLastPoint() || e == g.getLastPoint() && f == g.getFirstPoint())
                        if (e.equals(f)) {
                            var k = g.getFirstPoint(),
                                l = g.getLastPoint(),
                                q = l.clone(),
                                m = this;
                            a = function() {
                                g.replacePointAt(q, 0);
                                g.removePointAt(h.length - 1);
                                g.closePath = !0;
                                m.invalidateRender()
                            };
                            a();
                            this.history.addUndoRedoActions(new XOS_Action(function() {
                                g.replacePointAt(k, 0);
                                g.addPointAt(l, h.length - 1);
                                g.closePath = !1;
                                m.invalidateRender()
                            }), new XOS_Action(a))
                        } else k = g.getFirstPoint(), q = new XOS_LineTo(k.x, k.y), m = this, a = function() {
                            g.replacePointAt(q, 0);
                            g.closePath = !0;
                            m.invalidateRender()
                        }, a(), this.history.addUndoRedoActions(new XOS_Action(function() {
                            g.replacePointAt(k,
                                0);
                            g.closePath = !1;
                            m.invalidateRender()
                        }), new XOS_Action(a))
                }
            } else {
                var p = e.pathRef,
                    E = f.pathRef;
                if (!p.closePath && !E.closePath && (a = b[0].parent.shapeRef, b = b[1].parent.shapeRef, a != b)) {
                    var L;
                    a.paths.length >= b.paths.length ? (c = a.clone(), d = a.paths.indexOf(p), L = a.parent, idLevelNewShapeTarget = a.parent.getChildIndex(a)) : (c = b.clone(), d = b.paths.indexOf(E), L = b.parent, idLevelNewShapeTarget = b.parent.getChildIndex(b));
                    e == p.getFirstPoint() && f == E.getFirstPoint() ? (e = p.clone(), e.revertPoints(), p = E.clone(), b.localPointsToGlobalPoints(p.getEditablePoints(), !0), a.globalPointsToLocalPoints(p.getEditablePoints(), !0), E = new XOS_LineTo(p.getFirstPoint().x, p.getFirstPoint().y), p.replacePointAt(E, 0), e.addPoints(p.points), c.paths.replaceAt(e, d), this.replaceGraphicObjects_undable([a, b], [c], L, idLevelNewShapeTarget)) : e == p.getLastPoint() && f == E.getLastPoint() ? (e = p.clone(), p = E.clone(), p.revertPoints(), b.localPointsToGlobalPoints(p.getEditablePoints(), !0), a.globalPointsToLocalPoints(p.getEditablePoints(), !0), E = new XOS_LineTo(p.getFirstPoint().x, p.getFirstPoint().y),
                            p.replacePointAt(E, 0), e.addPoints(p.points), c.paths.replaceAt(e, d), this.replaceGraphicObjects_undable([a, b], [c], L, idLevelNewShapeTarget)) : e == p.getFirstPoint() && f == E.getLastPoint() ? (e = E.clone(), p = p.clone(), a.localPointsToGlobalPoints(p.getEditablePoints(), !0), b.globalPointsToLocalPoints(p.getEditablePoints(), !0), E = new XOS_LineTo(p.getFirstPoint().x, p.getFirstPoint().y), p.replacePointAt(E, 0), e.addPoints(p.points), c.paths.replaceAt(e, d), this.replaceGraphicObjects_undable([a, b], [c], L, idLevelNewShapeTarget)) :
                        e == p.getLastPoint() && f == E.getFirstPoint() && (e = p.clone(), p = E.clone(), b.localPointsToGlobalPoints(p.getEditablePoints(), !0), a.globalPointsToLocalPoints(p.getEditablePoints(), !0), E = new XOS_LineTo(p.getFirstPoint().x, p.getFirstPoint().y), p.replacePointAt(E, 0), e.addPoints(p.points), c.paths.replaceAt(e, d), this.replaceGraphicObjects_undable([a, b], [c], L, idLevelNewShapeTarget))
                }
            }
        }
    }
};
_.createObjectInteraction = function(a) {
    this.currentEditableGraphicObj = a;
    a = null;
    "boundsInteraction" == this.interactionMode ? (this.removeAllObjectInteraction(), a = 1 == this.selectionList.length ? new XOS_BoundsInteraction2D(this, this.currentEditableGraphicObj) : new XOS_BoundsInteraction2D(this, null), this.interactionDisplay.addChild(a)) : "pointsInteraction" == this.interactionMode && (a = new XOS_PointsInteraction(this, this.currentEditableGraphicObj), this.interactionDisplay.addChild(a));
    this.interactionDisplay.invalidateRender()
};
_.removeAllObjectInteraction = function() {
    this.interactionDisplay.removeAllChildren()
};
_.selectAll = function() {
    var a = this.content.numChildren(),
        b = this.content.children[a - 1];
    if (b.isolateObjectRef) this.selectAllInLayer(b);
    else
        for (b = 0; b < a; b++) this.selectAllInLayer(this.content.children[b]);
    this.showSelectionProperties()
};
_.selectAllInLayer = function(a) {
    if (!a.isLocked && !1 != a.visible)
        for (var b = a.numChildren(), c = 0; c < b; c++) this.selectGraphicObject(a.children[c], !0)
};
_.selectObjectsByRect = function(a, b, c, d) {
    this.graphicContext.strokeStyle = "#FF0000";
    this.graphicContext.lineWidth = 1;
    this.graphicContext.setTransform(1, 0, 0, 1, 0, 0);
    this.graphicContext.beginPath();
    this.graphicContext.rect(a, b, c, d);
    this.graphicContext.closePath();
    a = [];
    var e = null;
    b = this.content.children;
    this.currentLayer.isolateObjectRef && (b = [this.currentLayer]);
    c = b.length;
    for (d = 0; d < c; d++)
        if (e = b[d], !0 != e.isLocked && !1 != e.visible && ("pointsInteraction" == this.interactionMode ? e.getObjectsWithPointsInPath(a, !0) :
                e.getObjectsWithPointsInPath(a, !1), 0 < a.length))
            for (var e = a.length, f = 0; f < e; f++) this.selectGraphicObject(a[f], !0);
    this.showSelectionProperties()
};
_.setInteractionMode = function(a, b) {
    this.interactionDisplay.viewMode != b && (this.interactionDisplay.interactionViewMode = b, this.interactionDisplay.invalidateRender());
    var c = this.selectionList.length;
    if (0 < c && this.interactionMode != a) {
        var d = this.selectionList;
        this.deselectAllGraphicObjects();
        this.interactionMode = a;
        for (var e = 0; e < c; e++) this.selectGraphicObject(d[e], !0);
        this.showSelectionProperties()
    } else this.interactionMode = a
};
_.selectGraphicObject = function(a, b) {
    !0 != a.isSelected && (this.selectionList.push(a), a.isSelected = !0, b && !1 == a.isLocked && this.createObjectInteraction(a), this.currentEditableGraphicObj = a, this.graphicObjectPropertyToGlobal(this.currentEditableGraphicObj))
};
_.deselectGraphicObject = function(a) {
    for (var b = null, c = 0, d = 0, c = this.selectionList.length, d = 0; d < c; d++)
        if (a == this.selectionList[d]) {
            a.deselect();
            this.selectionList.splice(d, 1);
            break
        }
    if ("pointsInteraction" == this.interactionMode)
        for (d = 0; d < c; d++)
            if (b = this.interactionDisplay.children[d], a == b.shapeRef) {
                this.interactionDisplay.removeChildAt(d);
                break
            }
    this.interactionDisplay.invalidateRender()
};
_.deselectAllGraphicObjects = function() {
    for (var a = this.selectionList.length, b = 0; b < a; b++) this.selectionList[b].deselect();
    this.removeAllObjectInteraction();
    this.selectionList = [];
    this.currentEditableGraphicObj = null;
    this.showSelectionProperties();
    this.interactionDisplay.clear()
};
_.deselectAllGraphicObjectsPoints = function() {
    for (var a = this.selectionList.length, b = 0; b < a; b++) this.selectionList[b].deselectAllPoints();
    this.interactionDisplay.invalidateRender()
};
_.getSelectionList = function() {
    return this.selectionList
};
_.getSelectionBounds = function() {
    for (var a = new XOS_Bounding2D, b = this.selectionList.length, c = 0; c < b; c++) a.addBounds(this.selectionList[c].getGlobalBounds());
    return a
};
_.drawGraphicObjectListAsSelected = function(a) {
    this.interactionDisplay.graphicContext.strokeStyle = "#2E87FF";
    for (var b = a.length, c = 0; c < b; c++) a[c].drawAsSelected()
};
_.swapFillAndStrokePropertiesOfSelectedGraphicObject = function() {
    var a = {};
    this.globalPropertyToGraphicObject(a);
    var b = {};
    b.strokeAlpha = a.fillAlpha;
    b.fillAlpha = a.strokeAlpha;
    b.strokeFillingType = a.fillFillingType;
    b.fillFillingType = a.strokeFillingType;
    b.strokeColor = a.fillColor;
    b.fillColor = a.strokeColor;
    b.strokeGradient = a.fillGradient;
    b.fillGradient = a.strokeGradient;
    b.strokePattern = a.fillPattern;
    b.fillPattern = a.strokePattern;
    this.setPropertiesOfSelectedGraphicObjects_undable(b);
    this.showSelectionProperties()
};
_.globalPropertyToGraphicObject = function(a) {
    a.strokeAlpha = this.strokeAlpha;
    a.fillAlpha = this.fillAlpha;
    a.lineWidth = this.lineWidth;
    a.lineCap = this.lineCap;
    a.lineJoin = this.lineJoin;
    a.miterLimit = this.miterLimit;
    a.fillFillingType = this.fillFillingType;
    a.strokeFillingType = this.strokeFillingType;
    a.opacity = this.opacity;
    a.compositeOperation = this.compositeOperation;
    this.strokeFillingType != XOS_DisplayObject2D.NONE_FILLING && (this.strokeFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING ? a.strokeColor = this.strokeColor :
        this.strokeFillingType == XOS_DisplayObject2D.GRADIENT_FILLING ? a.strokeGradient = this.strokeGradient.clone() : this.strokeFillingType == XOS_DisplayObject2D.PATTERN_FILLING ? a.strokePattern = this.strokePattern.clone() : this.strokeFillingType == XOS_DisplayObject2D.BRUSH_FILLING && (a.brush = this.brush));
    this.fillFillingType != XOS_DisplayObject2D.NONE_FILLING && (this.fillFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING ? a.fillColor = this.fillColor : this.fillFillingType == XOS_DisplayObject2D.GRADIENT_FILLING ? a.fillGradient =
        this.fillGradient.clone() : this.fillFillingType == XOS_DisplayObject2D.PATTERN_FILLING && (a.fillPattern = this.fillPattern.clone()))
};
_.graphicObjectPropertyToGlobal = function(a) {
    if (a instanceof XOS_DisplayObject2D) {
        var b = {};
        b.fillFillingType = this.fillFillingType = a.fillFillingType;
        b.strokeFillingType = this.strokeFillingType = a.strokeFillingType;
        b.strokeAlpha = this.strokeAlpha = a.strokeAlpha;
        b.fillAlpha = this.fillAlpha = a.fillAlpha;
        b.lineWidth = this.lineWidth = a.lineWidth;
        b.lineCap = this.lineCap = a.lineCap;
        b.lineJoin = this.lineJoin = a.lineJoin;
        b.miterLimit = this.miterLimit = a.miterLimit;
        b.opacity = this.opacity = a.opacity;
        b.compositeOperation = this.compositeOperation =
            a.compositeOperation;
        a.strokeFillingType != XOS_DisplayObject2D.NONE_FILLING && (a.strokeFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING ? b.strokeColor = this.strokeColor = a.strokeColor : a.strokeFillingType == XOS_DisplayObject2D.GRADIENT_FILLING ? b.strokeGradient = this.strokeGradient = a.strokeGradient.clone() : a.strokeFillingType == XOS_DisplayObject2D.PATTERN_FILLING ? b.strokePattern = this.strokePattern = a.strokePattern : this.strokeFillingType == XOS_DisplayObject2D.BRUSH_FILLING && (b.brush = this.brush = a.brush));
        a.fillFillingType !=
            XOS_DisplayObject2D.NONE_FILLING && (a.fillFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING ? b.fillColor = this.fillColor = a.fillColor : a.fillFillingType == XOS_DisplayObject2D.GRADIENT_FILLING ? b.fillGradient = this.fillGradient = a.fillGradient.clone() : a.fillFillingType == XOS_DisplayObject2D.PATTERN_FILLING && (b.fillPattern = this.fillPattern = a.fillPattern));
        return b
    }
};
_.translateGraphicObjects_undable = function(a, b, c) {
    this.translateGraphicObjects(a, b, c, !1);
    this.createUndoRedo_translation(a, b, c);
    if ("pointsInteraction" == this.interactionMode)
        for (b = a.length, c = 0; c < b; c++) a[c].parent.invalidateBounds();
    this.onChange();
    this.invalidateRender();
    this.interactionDisplay.invalidateRender()
};
_.translateGraphicObjects = function(a, b, c, d) {
    for (var e = a.length, f = 0; f < e; f++) a[f].translate(b, c, d)
};
_.translateSelectedHandlePoints_undable = function(a, b) {
    var c = new TranslatePoints_action(this),
        d = new TranslatePoints_action(this);
    this.history.addUndoRedoActions(c, d);
    for (var e = [], f = null, g = null, h, k = this.interactionDisplay.children.length, l = 0; l < k; l++) {
        var f = this.interactionDisplay.children[l],
            g = f.getSelectedHandles(),
            q = [],
            m = g.length;
        for (h = 0; h < m; h++) q.push(g[h].pointRef);
        c.addObjectAndPointsToAction(f.shapeRef, q);
        d.addObjectAndPointsToAction(f.shapeRef, q);
        e = e.concat(g)
    }
    g = f = null;
    k = e.length;
    for (h = 0; h < k; h++) f =
        e[h], g = f.parent.shapeRef.globalToLocal({
            x: f.x + a,
            y: f.y + b
        }), f.pointRef.x = g.x, f.pointRef.y = g.y, f.parent.shapeRef.invalidateBounds();
    c.setTranslationValues(-a, -b);
    d.setTranslationValues(a, b);
    this.invalidateRender();
    this.interactionDisplay.invalidateRender()
};
_.rotateGraphicObjects = function(a, b, c) {
    for (var d = a.length, e = 0; e < d; e++) a[e].rotateFromPoint(b, c)
};
_.rotateGraphicObjects_undable = function(a, b, c) {
    for (var d = a.length, e = 0; e < d; e++) a[e].rotateFromPoint(b, c);
    this.createUndoRedo_rotation(a, b, c)
};
_.scaleGraphicObjects = function(a, b, c, d, e) {
    for (var f = a.length, g = 0; g < f; g++) a[g].scaleFromPoint(b, c, d, e)
};
_.normalizeGraphicObjects = function(a) {
    for (var b = a.length, c = 0; c < b; c++) a[c].normalizePoints()
};
_.invalidateGraphicObjectsBounds = function(a) {
    for (var b = a.length, c = 0; c < b; c++) a[c].invalidateBounds()
};
_.createUndoRedo_rotation = function(a, b, c) {
    var d = new RotateGraphicObject_action(this),
        e = new RotateGraphicObject_action(this);
    this.history.addUndoRedoActions(d, e);
    for (var f = a.length, g = 0; g < f; g++) d.addObjectToAction(a[g]), e.addObjectToAction(a[g]);
    d.setRotationValues(-b, c.clone());
    e.setRotationValues(b, c.clone())
};
_.createUndoRedo_scale = function(a, b, c, d, e) {
    var f = new ScaleGraphicObject_action(this),
        g = new ScaleGraphicObject_action(this);
    f.isLocalMode = g.isLocalMode = e;
    this.history.addUndoRedoActions(f, g);
    e = a.length;
    for (var h = 0; h < e; h++) f.addObjectToAction(a[h]), g.addObjectToAction(a[h]);
    f.setScaleValues(1 / b, 1 / c, d.clone());
    g.setScaleValues(b, c, d.clone())
};
_.createUndoRedo_translation = function(a, b, c) {
    var d = new TranslateGraphicObject_action(this),
        e = new TranslateGraphicObject_action(this);
    this.history.addUndoRedoActions(d, e);
    for (var f = a.length, g = 0; g < f; g++) d.addObjectToAction(a[g]), e.addObjectToAction(a[g]);
    d.setTranslationValues(-b, -c);
    e.setTranslationValues(b, c)
};
_.sortGraphicObjectListByIndex = function(a) {
    for (var b = a.length, c = [], d = a[0].parent, e, f = 0; f < b; f++) {
        e = a[f];
        if (d != e.parent) return alert("To make a group,The objects must be inside the same level or group!"), null;
        c.push({
            obj: e,
            index: e.parent.getChildIndex(e)
        })
    }
    c.sort(function(a, b) {
        return a.index - b.index
    });
    a = [];
    for (f = 0; f < b; f++) a.push(c[f].obj);
    return a
};
_.groupSelectedGraphicObjects = function() {
    var a = this.sortGraphicObjectListByIndex(this.selectionList);
    if (null != a) {
        var b = a[0].parent,
            c = new XOS_Group2D,
            d = b.globalToLocal(this.getSelectionBounds().min);
        c.setPosition(d.x, d.y);
        for (var e, f, g = a.length, h = 0; h < g; h++) e = a[h], f = e.getPosition(), addedChild = c.addChild(e.clone()), addedChild.setPosition(f.x - d.x, f.y - d.y);
        this.replaceGraphicObjects_undable(a, [c], b, -1);
        c.invalidateBounds();
        return c
    }
};
_.ungroupSelectedGraphicObjects = function() {
    for (var a = [], b = this.selectionList.length, c = 0; c < b; c++) this.selectionList[c] instanceof XOS_Group2D && a.push(this.selectionList[c]);
    for (var d = [], e, b = a.length, c = 0; c < b; c++) e = a[c], d.push({
        object: e,
        index: e.parent.getChildIndex(e)
    });
    d.sort(function(a, b) {
        return a.index - b.index
    });
    a = [];
    b = d.length;
    for (c = 0; c < b; c++) a.push(d[c].object);
    b = [];
    d = c = null;
    e = a.length;
    for (var f, g = 0; g < e; g++) {
        f = a[g];
        for (var c = f.children.length, h = 0; h < c; h++) d = f.children[h].clone(), d.localTransform =
            f.localTransform.concat(d.localTransform), b.push(d), d.isMask && (d.isMask = !1)
    }
    this.replaceGraphicObjects_undable(a, b, null, null)
};
_.toImage = function(a, b, c, d, e, f) {
    void 0 == c && (c = !1);
    var g = document.createElement("canvas");
    g.width = a;
    g.height = b;
    var h = g.getContext("2d");
    f && (h.fillStyle = f, h.fillRect(0, 0, a, b));
    !0 == c ? (a = Math.min(a / this.canvas.width, b / this.canvas.height), h.drawImage(this.canvas, 0, 0, this.canvas.width * a, this.canvas.height * a)) : h.drawImage(this.canvas, 0, 0, a, b);
    void 0 === e && (e = 1);
    try {
        return "jpeg" == d || "jpg" == d ? g.toDataURL("image/jpeg", e) : g.toDataURL("image/png")
    } catch (k) {
        return console.log(k), null
    }
};
_.currentPageToImage2 = function(a, b, c, d, e) {
    var f = this.canvas.width,
        g = this.canvas.height,
        h = this.content.localTransform.clone(),
        k = this.getLayerByName("workspace").children[0];
    this.canvas.width = k.width;
    this.canvas.height = k.height;
    var l = 1;
    d && this.canvas.width > d && (l = d / k.width, this.canvas.width = d, this.canvas.height = k.height * l);
    e && this.canvas.height > e && (this.canvas.height = e, l = e / k.height, this.canvas.width = k.width * l);
    this.content.localTransform.identity();
    this.content.localTransform = this.content.localTransform.appendScale(l,
        l);
    this.content.updateGlobalTransform();
    this.invalidateRender();
    k.visible = !1;
    c || "jpeg" == a || "jpg" == a ? (this.clear(), this.graphicContext.fillStyle = k.fillColor, this.graphicContext.fillRect(0, 0, this.canvas.width, this.canvas.height), this.draw(!1)) : this.draw();
    c = null;
    void 0 === b && (b = 1);
    try {
        c = "jpeg" == a || "jpg" == a ? this.canvas.toDataURL("image/jpeg", b) : this.canvas.toDataURL("image/png")
    } catch (q) {
        return console.log(q), k.visible = !0, this.canvas.width = f, this.canvas.height = g, this.content.localTransform = h, this.content.updateGlobalTransform(),
            this.invalidateRender(), this.interactionDisplay.invalidateRender(), null;
           // alert("not applicable");
    }
    k.visible = !0;
    this.canvas.width = f;
    this.canvas.height = g;
    this.content.localTransform = h;
    this.content.updateGlobalTransform();
    this.invalidateRender();
    this.interactionDisplay.invalidateRender();
    window.myCanvasPngImg[0]=c;
   console.log("youtube images="+c);
    //console.log(c);
};
_.currentPageToImage = function(a, b, c, d, e) {
    var pageName="";
    var person = prompt("Save as", $("#TABS_DOCUMENTS li.selected").text());
        if (person != null) {
            if(person == "")
            {
                alert("Invalid Name");
            }
            else
            {
                pageName=person;
                //$("#loadingPopups2").show();
            }
          
        }
        else
        {
            pageName="untitled";
            //$("#loadingPopups2").show(); 
        }
    
   // $("#loadingImg").show();
    var uId=$("#userId").val();
    var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
var encodedString = Base64.encode(uId);
    
    if (person) {
        var projectId=$("#project_ids").val();
        var currentProject=$("#currentProject").text();
        $.post(base_url+'script/saveDesign/pngSave.php',{img:window.myCanvasPngImg[0],usrId:uId,fileName:pageName,projectName:currentProject,projectId:projectId},function(data, status){
        //console.log(data);
        //console.log(this.canvas.toDataURL('image/png'));
        $("#loadingPopups2").hide();
        alert("Your PNG Image Save Successfully..!");
    });   
    }else{
        
    }
   // setTimeout(function(){$("#myFileSuccess").hide();/*showCurrentImages();*/},5000);

};
_.saveImgForGIF = function(a)
{
    var usrIds=$("#userId").val();
    $.ajax({
     url: '../convertPng.php?usersIds='+usrIds,
     type: 'POST',
     data: {
        // data: this.canvas.toDataURL('image/png')
        data: window.myCanvasPngImg[0]
     },
     complete: function(data, status)
     {
         if(status=='success')
         {
            var strs=data.responseText;
            var newId=strs.split("s/",3);
            var newId1=newId[1].split(".", 3);
          $("#imagePanels").append("<li class=\"imgOutPnl\" id=\""+newId1[0]+"\"><img src=\"../"+data.responseText+"\" class=\"imgSize\"><img src=\"../img/myDelete.png\" class=\"frameDeletsCls\" onclick=\"removeFrames('"+data.responseText+"')\"></li>");
         }
     }

 });
}
function showCurrentImages()
{
    $(".pages_wrapper").find("ul").html("");
    $("#myLoadrs").show();
 $.ajax({
             type:"GET",
             url:base_url+"script/saveDesign/getImages.php",
            success:function(mydata)
            {
                //alert("get all Images");
                $("#myLoadrs").hide();
                 $(".pages_wrapper").find("ul").append(mydata);
            }
        });
}
_.showSelectionProperties = function() {};
_.addEffectToSelectedObjects_undable = function(a) {
    function b() {
        var b, f = d.length;
        for (id = 0; id < f; id++) b = d[id], b.addEffect(a), c.selectGraphicObject(b, !0);
        c.showSelectionProperties();
        c.invalidateRender()
    }
    var c = this,
        d = this.selectionList;
    this.history.addUndoRedoActions(new XOS_Action(function() {
        var a, b = d.length;
        for (id = 0; id < b; id++) a = d[id], a.effectList.pop(), c.selectGraphicObject(a, !0);
        c.showSelectionProperties();
        c.invalidateRender()
    }), new XOS_Action(b));
    b()
};
_.removeEffectFromGraphicObject_undable = function(a) {
    function b() {
        e.effectList.addAt(a, f);
        d.selectGraphicObject(e, !0);
        d.invalidateRender();
        d.showSelectionProperties()
    }

    function c() {
        e.effectList.remove(a);
        d.selectGraphicObject(e, !0);
        d.invalidateRender();
        d.showSelectionProperties()
    }
    var d = this,
        e = a.graphicObjectTarget,
        f = e.effectList.indexOf(a); - 1 != f && (this.history.addUndoRedoActions(new XOS_Action(b), new XOS_Action(c)), c())
};
_.getListOfEqualEffectFromSelectedObjects = function(a) {
    for (var b = [], c = this.selectionList.length, d = 0; d < c; d++) {
        var e = this.selectionList[d].getEffectByName(a.name);
        e && b.push(e)
    }
    return b
};
_.addAnimationToSelectedObjects_undable = function(a) {
    function b() {
        var b, c = e.length;
        for (id = 0; id < c; id++) b = e[id], b.addAnimation(a), "CSS_ANIMATION" == a.type && (b.placeIntoHtmlLayer = "yes"), d.selectGraphicObject(b, !0);
        d.showSelectionProperties()
    }

    function c() {
        var a, b = e.length;
        for (id = 0; id < b; id++) a = e[id], a.animationList.pop(), d.selectGraphicObject(a, !0);
        d.showSelectionProperties()
    }
    var d = this,
        e = this.selectionList;
    0 != this.selectionList.length && (this.history.addUndoRedoActions(new XOS_Action(c), new XOS_Action(b)),
        b())
};
_.removeAnimationFromGraphicObject_undable = function(a) {
    function b() {
        e.animationList.addAt(a, f);
        d.selectGraphicObject(e, !0);
        d.showSelectionProperties()
    }

    function c() {
        e.animationList.remove(a);
        d.selectGraphicObject(e, !0);
        d.showSelectionProperties()
    }
    var d = this,
        e = a.graphicObjectTarget,
        f = e.animationList.indexOf(a); - 1 != f && (this.history.addUndoRedoActions(new XOS_Action(b), new XOS_Action(c)), c())
};
_.addTweenToSelectedObjects_undable = function(a) {
    function b() {
        var b, c = e.length;
        for (id = 0; id < c; id++) b = e[id], b.addTween(a), d.selectGraphicObject(b, !0);
        d.showSelectionProperties()
    }

    function c() {
        var a, b = e.length;
        for (id = 0; id < b; id++) a = e[id], a.tweenList.pop(), d.selectGraphicObject(a, !0);
        d.showSelectionProperties()
    }
    var d = this,
        e = this.selectionList;
    0 != this.selectionList.length && (this.history.addUndoRedoActions(new XOS_Action(c), new XOS_Action(b)), b())
};
_.removeTweenFromGraphicObject_undable = function(a) {
    function b() {
        e.tweenList.addAt(a, f);
        d.selectGraphicObject(e, !0);
        d.showSelectionProperties()
    }

    function c() {
        e.tweenList.remove(a);
        d.selectGraphicObject(e, !0);
        d.showSelectionProperties()
    }
    var d = this,
        e = a.graphicObjectTarget,
        f = e.tweenList.indexOf(a); - 1 != f && (this.history.addUndoRedoActions(new XOS_Action(b), new XOS_Action(c)), c())
};
_.setColorOfSelectedObjects = function(a, b) {
    "stroke" == this.currentEditableProperty ? (b || (b = this.strokeAlpha), "none" == a ? (this.strokeFillingType = XOS_DisplayObject2D.NONE_FILLING, this.setPropertiesOfSelectedGraphicObjects_undable({
            strokeFillingType: XOS_DisplayObject2D.NONE_FILLING,
            strokeAlpha: b
        }, !0)) : (this.strokeFillingType = XOS_DisplayObject2D.DIFFUSE_FILLING, this.setPropertiesOfSelectedGraphicObjects_undable({
            strokeFillingType: XOS_DisplayObject2D.DIFFUSE_FILLING,
            strokeColor: a,
            strokeAlpha: b
        }, !0)), this.strokeColor =
        a, this.strokeAlpha = b) : "fill" == this.currentEditableProperty && (b || (b = this.fillAlpha), "none" == a ? (this.fillFillingType = XOS_DisplayObject2D.NONE_FILLING, this.setPropertiesOfSelectedGraphicObjects_undable({
        fillFillingType: XOS_DisplayObject2D.NONE_FILLING,
        fillAlpha: b
    }, !0)) : (this.fillFillingType = XOS_DisplayObject2D.DIFFUSE_FILLING, this.setPropertiesOfSelectedGraphicObjects_undable({
        fillFillingType: XOS_DisplayObject2D.DIFFUSE_FILLING,
        fillColor: a,
        fillAlpha: b
    }, !0)), this.fillColor = a, this.fillAlpha = b)
};
_.setGradientOfSelectedObjects = function(a) {
    var b = this.selectionList;
    this.selectionList = this.getAllNonGroupElementsInDepthAsArray(this.selectionList);
    if ("string" === typeof a || a instanceof String) - 1 != a.indexOf("linear") ? a = XOS_LinearGradient.createFromHtmlGradient(a, this) : -1 != a.indexOf("radial") && (a = XOS_RadialGradient.createFromHtmlGradient(a, this));
    switch (this.currentEditableProperty) {
        case "stroke":
            a || (a = this.strokeGradient);
            this.strokeFillingType = XOS_DisplayObject2D.GRADIENT_FILLING;
            this.strokeGradient =
                a;
            break;
        case "fill":
            a || (a = this.fillGradient), this.fillFillingType = XOS_DisplayObject2D.GRADIENT_FILLING, this.fillGradient = a
    }
    var c = new SetObjectProperties_action(this),
        d = new SetObjectProperties_action(this);
    this.history.addUndoRedoActions(c, d);
    var e, f, g, h = this.selectionList.length;
    for (id = 0; id < h; id++) {
        e = this.selectionList[id];
        f = e.getLocalBounds();
        g = a.clone();
        if (g instanceof XOS_LinearGradient) g.setPoints(f.min.x, f.min.y, f.min.x, f.max.y);
        else if (a instanceof XOS_RadialGradient) {
            var k = f.getCenter();
            g.setPoints(k.x,
                k.y, k.x, k.y, 1, f.getWidth() / 2)
        }
        switch (this.currentEditableProperty) {
            case "stroke":
                e.strokeGradient && (e.strokeGradient instanceof XOS_LinearGradient && g instanceof XOS_LinearGradient || e.strokeGradient instanceof XOS_RadialGradient && g instanceof XOS_RadialGradient) && e.strokeGradient.copyPointsTo(g);
                c.addObjectToAction(e, "strokeFillingType,strokeGradient");
                e.strokeFillingType = XOS_DisplayObject2D.GRADIENT_FILLING;
                e.strokeGradient = g;
                d.addObjectToAction(e, "strokeFillingType,strokeGradient");
                break;
            case "fill":
                e.fillGradient &&
                    (e.fillGradient instanceof XOS_LinearGradient && g instanceof XOS_LinearGradient || e.fillGradient instanceof XOS_RadialGradient && g instanceof XOS_RadialGradient) && e.fillGradient.copyPointsTo(g), c.addObjectToAction(e, "fillFillingType,fillGradient"), e.fillFillingType = XOS_DisplayObject2D.GRADIENT_FILLING, e.fillGradient = g, d.addObjectToAction(e, "fillFillingType,fillGradient")
        }
        g.invalidateGradient()
    }
    this.selectionList = b;
    this.invalidateRender();
    return a
};
_.setPatternOfSelectedObjects = function(a) {
    if (a) {
        var b = new XOS_Pattern(this);
        b.load(a.src)
    }
    "stroke" == this.currentEditableProperty ? (b || (b = this.strokePattern), this.strokeFillingType = XOS_DisplayObject2D.PATTERN_FILLING, this.fillPattern = this.strokePattern = b, this.setPropertiesOfSelectedGraphicObjects_undable({
        strokeFillingType: XOS_DisplayObject2D.PATTERN_FILLING,
        strokePattern: this.strokePattern
    }, !0)) : "fill" == this.currentEditableProperty && (b || (b = this.fillPattern), this.fillFillingType = XOS_DisplayObject2D.PATTERN_FILLING,
        this.strokePattern = this.fillPattern = b, this.setPropertiesOfSelectedGraphicObjects_undable({
            fillFillingType: XOS_DisplayObject2D.PATTERN_FILLING,
            fillPattern: this.fillPattern
        }, !0))
};
_.mousePointToPenInfo = function(a, b, c, d, e) {
    var f = {
        position: null,
        snappedPosition: null,
        screenPosition: a,
        snappedScreenPosition: null,
        instance: null,
        snapType: null
    };
    f.position = null != b ? b.clone().invert().transformPoint(a) : a;
    if (!c) return f;
    b = this.screenSnapValue;
    e && (b = e);
    !0 == c.snapToGrid && this.penToSnap_grid(b);
    if (c.snapToBounds || c.snapToVertices || c.snapToContour) a = this.hitTestAll(a, b, d), a.hit && (f.instance = a.instance, f.path = a.path, f.idSegment = a.idSegment, f.localPoint = a.localPoint, !0 == c.snapToBounds && (b = this.penToSnap_bounds(b,
        f)), !0 != c.snapToVertices || f.snapType || this.penToSnap_vertices(b, f), !0 != c.snapToContour || f.snapType || this.penToSnap_contour(f));
    return f
};
_.penToSnap_grid = function(a, b) {
    b.snappedPosition = new XOS_Point2D(this.gridSnapValue * Math.round(b.position.x / a), this.gridSnapValue * Math.round(b.position.y / a))
};
_.penToSnap_bounds = function(a, b) {
    for (var c = b.screenPosition, d = b.instance, e = d.getLocalBounds().getPoints(), d = d.localPointsToGlobalPoints(e), e = d.length, f = a, g = null, h = null, k = 0; k < e; k++) h = d[k].distanceFromPoint(c), h < f && (g = k, f = h);
    null != g && (b.snapType = "BOUNDS", b.snappedScreenPosition = b.snappedPosition = d[g]);
    return f
};
_.penToSnap_vertices = function(a, b) {
    var c = b.instance.hitTestVertices(b.screenPosition, a);
    null != c && (b.snapType = "VERTEX", b.snappedScreenPosition = b.snappedPosition = b.instance.localToGlobal(c), b.vertex = c)
};
_.penToSnap_contour = function(a) {
    if (null != a.idSegment) {
        a.snapType = "CONTOUR";
        var b = a.path.getNearestPointOnSegment(a.localPoint, a.idSegment);
        a.snappedScreenPosition = a.snappedPosition = a.instance.localToGlobal(b.pointOnSegment)
    }
};
_.showSnap = function(a, b) {
    this.interactionDisplay.graphicContext.setTransform(1, 0, 0, 1, 0, 0);
    this.interactionDisplay.graphicContext.globalCompositeOperation = "source-over";
    this.interactionDisplay.graphicContext.globalAlpha = 1;
    this.interactionDisplay.graphicContext.beginPath();
    this.interactionDisplay.graphicContext.strokeStyle = "#FF0000";
    this.interactionDisplay.graphicContext.lineWidth = 2;
    this.interactionDisplay.graphicContext.drawEllipse(a - 6, b - 6, 12, 12);
    this.interactionDisplay.graphicContext.stroke()
};
_.onMouseDown = function(a) {
    if (this.isSpaceKeyPressed)
        if (a.altKey) this.tools.zoomView_tool.onMouseDown(a);
        else this.tools.panView_tool.onMouseDown(a);
    else this.tools.activeTool.onMouseDown(a)
};
_.onMouseMove = function(a) {
    this.tools.activeTool.onMouseMove(a)
};
_.onMouseUp = function(a) {};
_.onTouchStart = function(a) {
    this.tools.activeTool.onTouchStart(a)
};
_.onTouchMove = function(a) {
    this.tools.activeTool.onTouchMove(a)
};
_.onTouchEnd = function(a) {};
_.onTouchCancel = function(a) {};
_.onGestureStart = function(a) {};
_.onGestureEnd = function(a) {
    this.tools.activeTool.onGestureEnd(a)
};
_.onGestureChange = function(a) {
    this.tools.activeTool.onGestureChange(a)
};
ExtendClass(XOS_Shape, XOS_DisplayObject2D);

function XOS_Shape() {
    XOS_Shape.baseConstructor.call(this);
    this.type = "SHAPE";
    this.paths = [];
    this.isMask = !1
}
_ = XOS_Shape.prototype;
_.select = function() {
    this.isSelected = !0;
    this.selectAllPoints()
};
_.deselect = function() {
    this.isSelected = !1;
    this.deselectAllPoints()
};
_.selectAllPoints = function() {
    for (var a = this.paths.length, b = 0; b < a; b++) this.paths[b].selectAllPoints()
};
_.deselectAllPoints = function() {
    for (var a = this.paths.length, b = 0; b < a; b++) this.paths[b].deselectAllPoints()
};
_.hitTestGeometry = function(a, b) {
    this.applyTransform();
    if (this.hitTestBounds(a, b)) {
        if (this.fillFillingType != XOS_DisplayObject2D.NONE_FILLING && this.hitTestInside(a, b)) return {
            hit: !0,
            instance: this
        };
        var c = this.globalToLocal(a),
            d = this.hitTestContour(c, b);
        if (null != d) return {
            hit: !0,
            instance: this,
            path: d.path,
            idSegment: d.idSegment,
            localPoint: c
        }
    }
    return {
        hit: !1
    }
};
_.hitTestInside = function(a, b) {
    this.drawGeometryForHitTest();
    return this.graphicContext.isPointInPath(a.x, a.y)
};
_.hitTestContour = function(a, b) {
    for (var c = b * (1 / this.globalTransform.getScale().x), d, e = this.paths.length, f = 0; f < e; f++)
        if (d = this.paths[f].hitTestContour(a, c), null != d) return {
            path: this.paths[f],
            idSegment: d
        };
    return null
};
_.hitTestVertices = function(a, b) {
    for (var c = this.globalToLocal(a), d = b * (1 / this.globalTransform.getScale().x), e, f = this.paths.length, g = 0; g < f; g++)
        if (e = this.paths[g].hitTestVertices(c, d), null != e) return e;
    return null
};
_.hasPointsInPath = function() {
    for (var a, b = !1, c = this.paths.length, d = 0; d < c; d++) a = this.paths[d].hasPointsInPath(this), !0 == a && (b = !0);
    return b
};
_.updateGlobalTransform = function() {
    null != this.parent ? this.globalTransform = this.parent.globalTransform.concat(this.localTransform) : this.localTransform.copyTo(this.globalTransform);
    this.invalidTransform = !1
};
_.drawAsSelected = function() {
    var a = this.graphicContext;
    this.graphicContext = this.displayRef.interactionDisplay.graphicContext;
    this.invalidTransform && this.updateGlobalTransform();
    this.applyTransform();
    this.drawGeometry();
    this.closePath && this.graphicContext.closePath();
    this.graphicContext.stroke();
    this.graphicContext = a;
    for (var a = this.numChildren(), b = 0; b < a; b++) this.children[b].drawAsSelected()
};
_.draw = function() {
    !this.isMask && this.visible && (this.graphicContext.globalCompositeOperation = this.compositeOperation, this.invalidAlpha && this.updateGlobalAlpha(), this.invalidTransform && this.updateGlobalTransform(), this.applyTransform(), this.applyEffects() || (this.drawGeometry(), this.fillFillingType != XOS_DisplayObject2D.NONE_FILLING && this.drawFill(), this.strokeFillingType != XOS_DisplayObject2D.NONE_FILLING && this.drawStroke()))
};
_.drawFill = function() {
    this.fillFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING ? (this.graphicContext.globalAlpha = this.fillAlpha * this.globalAlpha, this.graphicContext.fillStyle = this.fillColor) : this.fillFillingType == XOS_DisplayObject2D.GRADIENT_FILLING ? (this.graphicContext.globalAlpha = this.globalAlpha, this.graphicContext.fillStyle = this.fillGradient.getGradient()) : this.fillFillingType == XOS_DisplayObject2D.PATTERN_FILLING && (this.graphicContext.globalAlpha = this.globalAlpha, this.graphicContext.fillStyle =
        this.fillPattern.pattern);
    this.graphicContext.fill()
};
_.drawStroke = function() {
    this.graphicContext.lineCap = this.lineCap;
    this.graphicContext.lineJoin = this.lineJoin;
    this.graphicContext.miterLimit = this.miterLimit;
    this.graphicContext.lineWidth = this.lineWidth;
    if (this.strokeFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING) this.graphicContext.globalAlpha = this.strokeAlpha * this.globalAlpha, this.graphicContext.strokeStyle = this.strokeColor;
    else if (this.strokeFillingType == XOS_DisplayObject2D.GRADIENT_FILLING) this.graphicContext.globalAlpha = this.globalAlpha, this.graphicContext.strokeStyle =
        this.strokeGradient.getGradient();
    else if (this.strokeFillingType == XOS_DisplayObject2D.PATTERN_FILLING) this.graphicContext.globalAlpha = this.globalAlpha, this.graphicContext.strokeStyle = this.strokePattern.pattern;
    else if (this.strokeFillingType == XOS_DisplayObject2D.BRUSH_FILLING) {
        this.drawStrokeWithBrush();
        return
    }
    this.graphicContext.stroke()
};
_.drawGeometry = function() {
    this.graphicContext.beginPath();
    for (var a = this.paths.length, b = 0; b < a; b++) this.paths[b].draw(this.graphicContext)
};
_.drawAsMask = function() {
    this.invalidTransform && this.updateGlobalTransform();
    this.applyTransform();
    this.drawGeometry();
    this.graphicContext.clip()
};
_.calculateLocalBounds = function() {
    this.bounds = new XOS_Bounding2D;
    for (var a = this.paths.length, b = 0; b < a; b++) 0 < this.paths[b].points.length && this.bounds.addPoints(this.paths[b].getAsPolygon().points);
    this.width = this.bounds.getWidth();
    this.height = this.bounds.getHeight();
    this.invalidBounds = !1
};
_.normalizePoints = function() {
    this.invalidTransform && this.updateGlobalTransform();
    for (var a = this.paths.length, b = 0; b < a; b++) this.localPointsToGlobalPoints(this.paths[b].getEditablePoints(), !0);
    var c = this.localTransform.getRotation(),
        d = this.localToGlobal(new XOS_Point2D(0, 0)),
        e = this.globalTransform.getScale(),
        b = this.displayRef.content.contentScale,
        e = Math.sqrt(Math.abs(e.x) / b.x * (Math.abs(e.y) / b.y));
    this.lineWidth *= e;
    for (var f = this.effectList.length, b = 0; b < f; b++) this.effectList[b].scaleProperties(e);
    this.fillFillingType ==
        XOS_DisplayObject2D.GRADIENT_FILLING && (b = this.fillGradient.getPointsAsArray(), this.localPointsToGlobalPoints(b, !0), this.fillGradient.setPointsByArray(b), this.fillGradient.r2 && (this.fillGradient.r2 *= e));
    this.strokeFillingType == XOS_DisplayObject2D.GRADIENT_FILLING && (b = this.strokeGradient.getPointsAsArray(), this.localPointsToGlobalPoints(b, !0), this.strokeGradient.setPointsByArray(b), this.strokeGradient.r2 && (this.strokeGradient.r2 *= e));
    this.localTransform.identity();
    this.localTransform = this.localTransform.appendRotation(c);
    this.updateGlobalTransform();
    c = this.parent.globalToLocal(d);
    this.localTransform = this.localTransform.prependTranslation(c.x, c.y);
    this.updateGlobalTransform();
    for (b = 0; b < a; b++) this.globalPointsToLocalPoints(this.paths[b].getEditablePoints(), !0);
    this.invalidateBounds();
    this.fillFillingType == XOS_DisplayObject2D.GRADIENT_FILLING && (b = this.fillGradient.getPointsAsArray(), this.globalPointsToLocalPoints(b, !0), this.fillGradient.setPointsByArray(b), this.fillGradient.invalidateGradient());
    this.strokeFillingType ==
        XOS_DisplayObject2D.GRADIENT_FILLING && (b = this.strokeGradient.getPointsAsArray(), this.globalPointsToLocalPoints(b, !0), this.strokeGradient.setPointsByArray(b), this.strokeGradient.invalidateGradient())
};
_.getEditablePoints = function() {
    for (var a = [], b = this.paths.length, c = 0; c < b; c++) a = a.concat(this.paths[c].getEditablePoints());
    return a
};
_.clone = function(a) {
    var b = new XOS_Shape;
    this.clonePropertiesTo(b);
    a && a.addChild(b);
    return b
};
_.clonePropertiesTo = function(a) {
    XOS_Shape.superClass.clonePropertiesTo.call(this, a);
    for (var b = this.paths.length, c = 0; c < b; c++) a.paths.push(this.paths[c].clone());
    a.isMask = this.isMask
};

function XOS_Path() {
    this.points = [];
    this.type = "PATH";
    this.closePath = !1
}
_ = XOS_Path.prototype;
_.selectAllPoints = function() {
    for (var a = this.points.length, b = 0; b < a; b++) this.points[b].select()
};
_.deselectAllPoints = function() {
    for (var a = this.points.length, b = 0; b < a; b++) this.points[b].deselect()
};
_.hitTestContour = function(a, b) {
    for (var c, d, e, f, g = this.points.length, h = 1; h < g; h++)
        if (d = this.points[h - 1], e = this.points[h], e instanceof XOS_QuadraticCurveTo || e instanceof XOS_CubicCurveTo)
            for (c = d, t = .1; 1 > t; t += .1) {
                f = e.getPointOnCurve(t, d);
                c = a.getDistanceFromLine(f.x, f.y, c.x, c.y);
                if (c < b) return h;
                c = f
            } else if (e instanceof XOS_LineTo && (c = a.getDistanceFromLine(e.x, e.y, d.x, d.y), c < b)) return h;
    if (this.closePath)
        if (d = e, e = this.points[0], e instanceof XOS_QuadraticCurveTo || e instanceof XOS_CubicCurveTo)
            for (c = d, t = .1; 1 >
                t; t += .1) {
                f = e.getPointOnCurve(t, d);
                c = a.getDistanceFromLine(f.x, f.y, c.x, c.y);
                if (c < b) return 0;
                c = f
            } else if (e instanceof XOS_LineTo && (c = a.getDistanceFromLine(e.x, e.y, d.x, d.y), c < b)) return 0;
    return null
};
_.hitTestVertices = function(a, b) {
    for (var c = this.points.length, d = 0; d < c; d++)
        if (this.points[d].distanceFromPoint(a) < b) return this.points[d];
    return null
};
_.getNearestPointOnSegment = function(a, b) {
    var c, d, e;
    c = 0 == b ? this.points[this.points.length - 1] : this.points[b - 1];
    d = this.points[b];
    d instanceof XOS_QuadraticCurveTo && (d = d.toCubicCurve(c));
    if (d instanceof XOS_CubicCurveTo) {
        var f = XOS_Path.nearestPointOnCurve(a, [c, d.controlPt1, d.controlPt2, d]);
        e = d.getPointOnCurve(f.location, c)
    } else d instanceof XOS_LineTo && (e = XOS_Path.nearestPointOnLine(c.x, c.y, d.x, d.y, a.x, a.y));
    f = {};
    f.pointOnSegment = e;
    f.segmentPtId = b;
    f.segmentPt = d;
    f.previousSegmentPt = c;
    return f
};
_.hasPointsInPath = function(a) {
    for (var b = this.points, c = b.length, d = null, e = !1, f = 0; f < c; f++) d = a.localToGlobal(b[f]), a.graphicContext.isPointInPath(d.x, d.y) && (e = !0, b[f].isSelected = b[f].isSelected ? !1 : !0);
    return e
};
_.draw = function(a) {
    var b = this.points.length;
    if (0 != b) {
        a.moveTo(this.points[0].x, this.points[0].y);
        for (var c = 1; c < b; c++) this.points[c].draw(a);
        this.closePath && (this.points[0].draw(a), a.closePath())
    }
};
_.getEditablePoints = function() {
    for (var a = [], b = this.points.length, c = 0; c < b; c++) this.points[c] instanceof XOS_QuadraticCurveTo ? a.push(this.points[c].controlPt) : this.points[c] instanceof XOS_CubicCurveTo && (a.push(this.points[c].controlPt1), a.push(this.points[c].controlPt2)), a.push(this.points[c]);
    return a
};
_.revertPoints = function() {
    for (var a = [], b, c, d, e = this.points.length - 1, f = 0; f < e; f++) b = this.points[f + 1].clone(), a.push(b), b.x = this.points[f].x, b.y = this.points[f].y, b instanceof XOS_CubicCurveTo && (c = b.controlPt1.x, d = b.controlPt1.y, b.controlPt1.x = b.controlPt2.x, b.controlPt1.y = b.controlPt2.y, b.controlPt2.x = c, b.controlPt2.y = d);
    b = this.points[e].clone();
    a.push(b);
    this.closePath && this.points[0] instanceof XOS_CubicCurveTo && (c = this.points[0].controlPt1.x, d = this.points[0].controlPt1.y, b.controlPt1.x = this.points[0].controlPt2.x,
        b.controlPt1.y = this.points[0].controlPt2.y, b.controlPt2.x = c, b.controlPt2.y = d);
    this.points = a.reverse();
    this.updatePathRef()
};
_.updatePathRef = function() {
    for (var a = this.points.length, b = 0; b < a; b++) this.points[b].pathRef = this
};
_.getFirstPoint = function() {
    return this.points[0]
};
_.getLastPoint = function() {
    return this.points[this.points.length - 1]
};
_.setPoints = function(a) {
    this.points = a;
    a = this.points.length;
    for (var b = 0; b < a; b++) this.points[b].pathRef = this
};
_.addPoints = function(a) {
    for (var b = a.length, c = 0; c < b; c++) this.addPoint(a[c])
};
_.addPoint = function(a, b) {
    a.pathRef = this;
    void 0 != b ? this.points.splice(b, 0, a) : this.points.push(a)
};
_.addPointAt = function(a, b, c) {
    a.pathRef = this;
    !0 == c ? this.points.splice(b, 1, a) : this.points.splice(b, 0, a)
};
_.replacePointAt = function(a, b) {
    a.pathRef = this;
    var c = this.points[b];
    this.points.splice(b, 1, a);
    return c
};
_.removePoint = function(a) {
    a = this.getPointIndex(a); - 1 < a && this.points.splice(a, 1)
};
_.removePointAt = function(a) {
    this.points.splice(a, 1)
};
_.getPointIndex = function(a) {
    return this.points.indexOf(a)
};
_.getPointAt = function(a) {
    return this.points[a]
};
_.splitAt = function(a) {
    var b = this.points.length,
        c, d;
    c = this.clone();
    d = this.clone();
    c.points.splice(a + 1, b - (a + 1));
    d.points.splice(0, a);
    c.closePath = !1;
    d.closePath = !1;
    return this.closePath ? (d.points = d.points.concat(c.points), [d]) : [c, d]
};
_.getAsPolygon = function(a) {
    var b = this.points.length;
    if (0 == b) return null;
    var c = .1;
    a && (c = a);
    a = new XOS_Path;
    for (var d = null, d = null, e = 0, f = 0; f < b; f++)
        if (this.points[f] instanceof XOS_QuadraticCurveTo || this.points[f] instanceof XOS_CubicCurveTo)
            if (0 < f)
                for (e = c; 1 > e; e += c) d = this.points[f].getPointOnCurve(e, this.points[f - 1]), d = new XOS_LineTo(d.x, d.y), d.isPointOfCurve = !0, a.addPoint(d);
            else a.addPoint(new XOS_MoveTo(this.points[f].x, this.points[f].y));
    else this.points[f] instanceof XOS_LineTo ? a.addPoint(new XOS_LineTo(this.points[f].x,
        this.points[f].y)) : this.points[f] instanceof XOS_MoveTo && a.addPoint(new XOS_MoveTo(this.points[f].x, this.points[f].y));
    if (this.closePath)
        if (f = 0, this.points[f] instanceof XOS_QuadraticCurveTo || this.points[f] instanceof XOS_CubicCurveTo)
            for (e = c, b = this.points[this.points.length - 1]; 1 >= e; e += c) d = this.points[f].getPointOnCurve(e, b), d = new XOS_LineTo(d.x, d.y), d.isPointOfCurve = !0, a.addPoint(d);
        else a.addPoint(new XOS_LineTo(this.points[f].x, this.points[f].y));
    return a
};
_.isClockwise = function() {
    return 0 < XOS_Path.getPolygonArea(this.getAsPolygon(.5)) ? !0 : !1
};
_.clone = function() {
    var a = new XOS_Path;
    this.clonePropertiesTo(a);
    return a
};
_.clonePropertiesTo = function(a) {
    var b = this.points.length;
    a.points = [];
    for (var b = this.points.length, c = 0; c < b; c++) a.addPoint(this.points[c].clone());
    a.closePath = this.closePath
};
XOS_Path.createEllipse = function(a, b, c, d) {
    var e = new XOS_Path,
        f = c / 2,
        g = d / 2,
        h = a + f,
        k = b + g;
    c = a + c;
    d = b + d;
    f *= .5522848;
    g *= .5522848;
    e.addPoint(new XOS_CubicCurveTo(h - f, d, a, k + g, a, k));
    e.addPoint(new XOS_CubicCurveTo(a, k - g, h - f, b, h, b));
    e.addPoint(new XOS_CubicCurveTo(h + f, b, c, k - g, c, k));
    e.addPoint(new XOS_CubicCurveTo(c, k + g, h + f, d, h, d));
    e.closePath = !0;
    return e
};
XOS_Path.createRectangle = function(a, b, c, d, e) {
    var f = new XOS_Path;
    0 < e ? (0 > c && (c = 0 - c, a -= c), 0 > d && (d = 0 - d, b -= d), f.addPoint(new XOS_MoveTo(a + e, b)), f.addPoint(new XOS_LineTo(a + c - e, b)), f.addPoint(new XOS_QuadraticCurveTo(a + c, b, a + c, b + e)), f.addPoint(new XOS_LineTo(a + c, b + d - e)), f.addPoint(new XOS_QuadraticCurveTo(a + c, b + d, a + c - e, b + d)), f.addPoint(new XOS_LineTo(a + e, b + d)), f.addPoint(new XOS_QuadraticCurveTo(a, b + d, a, b + d - e)), f.addPoint(new XOS_LineTo(a, b + e)), f.addPoint(new XOS_QuadraticCurveTo(a, b, a + e, b))) : (f.addPoint(new XOS_LineTo(a,
        b)), f.addPoint(new XOS_LineTo(a + c, b)), f.addPoint(new XOS_LineTo(a + c, b + d)), f.addPoint(new XOS_LineTo(a, b + d)));
    f.closePath = !0;
    return f
};
XOS_Path.createLine = function(a, b, c, d) {
    var e = new XOS_Path;
    e.addPoint(new XOS_MoveTo(a, b));
    e.addPoint(new XOS_LineTo(c, d));
    return e
};
XOS_Path.lineLineIntersection = function(a, b, c, d) {
    var e, f;
    e = d.x - c.x;
    f = d.y - c.y;
    d = b.x - a.x;
    b = b.y - a.y;
    var g = a.y - c.y,
        h = a.x - c.x;
    c = f * d - e * b;
    e = (e * g - f * h) / c;
    f = (d * g - b * h) / c;
    return 0 <= e && 1 >= e && 0 <= f && 1 >= f ? (f = a.y + e * b, e = a.x + e * d, {
        x: e,
        y: f
    }) : null
};
window.jsBezier && (XOS_Path.nearestPointOnCurve = window.jsBezier.nearestPointOnCurve);
XOS_Path.nearestPointOnLine = function(a, b, c, d, e, f, g) {
    var h = c - a,
        k = d - b;
    0 == h && 0 == k && (c += 1, d += 1, h = k = 1);
    var l = ((e - a) * h + (f - b) * k) / (h * h + k * k);
    0 > l || (1 < l ? (a = c, b = d) : (a += l * h, b += l * k));
    h = a - e;
    k = b - f;
    if (g) {
        if (Math.sqrt(h * h + k * k) < g) return {
            x: a,
            y: b
        }
    } else return {
        x: a,
        y: b
    };
    return null
};
XOS_Path.pointLineDistance = function(a, b, c, d, e, f) {
    var g = c - a,
        h = d - b;
    0 == g && 0 == h && (c += 1, d += 1, g = h = 1);
    var k = ((e - a) * g + (f - b) * h) / (g * g + h * h);
    0 > k ? g = a : 1 < k ? (g = c, b = d) : (g = a + k * g, b += k * h);
    g -= e;
    h = b - f;
    return Math.sqrt(g * g + h * h)
};
XOS_Path.getPolygonPerimeter = function(a) {
    var b = a.length,
        c = 0;
    a[0].distance = 0;
    for (var d = 1; d < b; d++) c += a[d].distanceFromPoint(a[d - 1]), a[d].distance = c;
    return c
};
XOS_Path.getPolygonArea = function(a) {
    var b, c, d = 0,
        e = a.length - 1;
    for (b = 0; b < e; b++) c = (b + 1) % e, d += a[b].x * a[c].y, d -= a[b].y * a[c].x;
    return d / 2
};
XOS_Path.invertPolygonPoints = function(a) {
    for (var b = [], c = a.length - 1; - 1 < c; c--) b.push(a[c]);
    return b
};
XOS_Path.getPolygonCW = function(a) {
    0 > XOS_Path.getPolygonArea(a) && (a = XOS_Path.invertPolygonPoints(a));
    return a
};
XOS_Path.getPolygonCCW = function(a) {
    0 < XOS_Path.getPolygonArea(a) && (a = XOS_Path.invertPolygonPoints(a));
    return a
};
XOS_Path.splitCubicBezier = function(a, b) {
    for (var c = []; 0 < a.length;) {
        for (var d = 0; d < a.length - 1; d++) c.unshift(a[d]), a[d] = XOS_Point2D.interpolatePoints(a[d], a[d + 1], b);
        c.unshift(a.pop())
    }
    return {
        b1: [{
            x: c[5].x,
            y: c[5].y
        }, {
            x: c[2].x,
            y: c[2].y
        }, {
            x: c[0].x,
            y: c[0].y
        }],
        b2: [{
            x: c[1].x,
            y: c[1].y
        }, {
            x: c[3].x,
            y: c[3].y
        }, {
            x: c[6].x,
            y: c[6].y
        }]
    }
};
XOS_Path.getPolygonOffset = function(a, b, c) {
    function d(a, b) {
        return Math.atan2(b.y - a.y, b.x - a.x)
    }

    function e(a, b, c, e) {
        a = d(b, a);
        b = d(b, c);
        c = b - a;
        b -= c / 2;
        0 < c && (b += Math.PI);
        c = Math.abs(e / Math.sin(c / 2));
        0 > e && (c = 0 - c);
        return new XOS_Point2D(Math.cos(b) * c, Math.sin(b) * c)
    }
    var f = [],
        g;
    !0 == c ? g = e(a[a.length - 1], a[0], a[1], b) : (g = d(a[0], a[1]), g = new XOS_Point2D(Math.cos(g + Math.PI / 2) * b, Math.sin(g + Math.PI / 2) * b));
    f.push(new XOS_LineTo(a[0].x + g.x, a[0].y + g.y));
    for (var h = a.length - 2, k = 0; k < h; k++) g = e(a[k], a[k + 1], a[k + 2], b), f.push(new XOS_LineTo(a[k +
        1].x + g.x, a[k + 1].y + g.y));
    !0 == c ? g = e(a[h], a[a.length - 1], a[0], b) : (c = d(a[a.length - 2], a[a.length - 1]), g = new XOS_Point2D(Math.cos(c + Math.PI / 2) * b, Math.sin(c + Math.PI / 2) * b));
    f.push(new XOS_LineTo(a[a.length - 1].x + g.x, a[a.length - 1].y + g.y));
    return f
};
ExtendClass(XOS_Page, XOS_DisplayObject2D);

function XOS_Page() {
    XOS_Page.baseConstructor.call(this);
    this.isLocked = !0;
    this.lineWidth = .2;
    this.strokeColor = "#000";
    this.fillColor = "#FFF";
    this.width = 842;
    this.height = 595;
    this.type = "page";
    this.pageAlignment = "center-horizontally"
}
_ = XOS_Page.prototype;
_.unlockAll = function() {};
_.drawGeometry = function() {
    this.graphicContext.rect(0, 0, this.width, this.height)
};
_.draw = function() {
    this.visible && (this.invalidTransform && this.updateGlobalTransform(), this.applyTransform(), this.graphicContext.beginPath(), this.drawGeometry(), this.graphicContext.closePath(), this.graphicContext.save(), "none" != this.fillColor && (this.graphicContext.shadowOffsetX = 4, this.graphicContext.shadowOffsetY = 4, this.graphicContext.shadowBlur = 4, this.graphicContext.shadowColor = "rgba(50,50, 50, 0.5)", this.graphicContext.fillStyle = this.fillColor, this.graphicContext.fill()), this.graphicContext.restore(),
        "none" != this.strokeColor && (this.graphicContext.globalAlpha = this.strokeAlpha, this.graphicContext.lineCap = this.lineCap, this.graphicContext.lineJoin = this.lineJoin, this.graphicContext.miterLimit = this.miterLimit, this.graphicContext.strokeStyle = this.strokeColor, this.graphicContext.lineWidth = this.lineWidth, this.graphicContext.stroke()))
};
_.clone = function() {
    var a = new XOS_Page,
        b = document.createElement("page");
    this.propertiesToXmlAttributes(b);
    a.xmlAttributesToProperties(b);
    return a
};
_.toXml = function(a) {
    var b = document.createElement("page");
    this.propertiesToXmlAttributes(b);
    a.appendChild(b)
};
ExtendClass(XOS_Layer, XOS_DisplayObject2D);

function XOS_Layer() {
    XOS_Layer.baseConstructor.call(this);
    this.type = "LAYER";
    this.drawMode = "preview"
}
_ = XOS_Layer.prototype;
_.draw = function() {
    this.isolateObjectRef && this.displayRef.fillContent("#FFF", .4);
    "wireframe" == this.drawMode ? this.visible && this.drawWireframe() : XOS_Layer.superClass.draw.call(this)
};
_.unlockAllGraphicObjects = function() {
    for (var a = this.numChildren(), b = 0; b < a; b++) this.children[b].unlock()
};
_.hitTestObject = function(a, b, c) {
    return this.isLocked || !this.visible ? {
        hit: !1
    } : this.hitTestChildren(a, b, c)
};
_.getObjectsWithPointsInPath = function(a, b) {
    for (var c = null, d = this.numChildren() - 1; - 1 < d; d--) c = this.children[d].getObjectsWithPointsInPath(a, b), null != c && a.push(c);
    return null
};
_.clone = function(a, b) {
    var c = new XOS_Layer;
    this.clonePropertiesTo(c);
    a && a.addChild(c);
    !0 == b && this.cloneChildren(c);
    return c
};
ExtendClass(XOS_Image, XOS_DisplayObject2D);

function XOS_Image() {
    XOS_Image.baseConstructor.call(this);
    this.type = "IMAGE";
    this.image = new Image;
    this.height = this.width = 0;
    this.isLoaded = !1;
    this.closePath = !0;
    this.onLoadFunctionListener = null;
    this.strokeFillingType = XOS_DisplayObject2D.NONE_FILLING;
    this.fillColor = this.strokeColor = "none";
    this.lineWidth = 0;
    this.url = ""
}
_ = XOS_Image.prototype;
_.__defineGetter__("imageUrl", function() {
    return this.url
});
_.__defineSetter__("imageUrl", function(a) {
    this.url = a;
    this.load(a)
});
_.setImage = function(a) {
    this.image = a;
    this.isLoaded = !0
};
_.load = function(a, b) {
    -1 != a.indexOf("https://www.googledrive.com/host/") && (this.image.crossOrigin = "anonymous"); - 1 != a.indexOf("http://new-photoviva.it") && (this.image.crossOrigin = "anonymous");
    this.isLoaded = !1;
    this.image.onload = Bind(this, this.onload);
    this.image.onerror = Bind(this, this.onerror);
    this.image.src = this.url = a;
    this.onLoadFunctionListener = b
};
_.onerror = function(a) {
   // console.log("image load error: " + this.url);
    if (null != this.displayRef) this.displayRef.onLoadImageError(this)
};
_.onload = function(a) {
    this.width = this.image.width;
    this.height = this.image.height;
    this.invalidateBounds();
    this.isLoaded = !0;
    if (this.onLoadFunctionListener) this.onLoadFunctionListener(this);
    this.invalidateBounds();
    if (null != this.displayRef) this.displayRef.onLoadImage(this)
};
_.draw = function() {
    this.invalidAlpha && this.updateGlobalAlpha();
    this.graphicContext.globalAlpha = this.globalAlpha;
    this.graphicContext.globalCompositeOperation = this.compositeOperation;
    this.invalidTransform && this.updateGlobalTransform();
    this.applyTransform();
    !0 == this.isLoaded && (this.applyEffects() || this.graphicContext.drawImage(this.image, 0, 0, this.width, this.height))
};
_.calculateLocalBounds = function() {
    this.bounds.min.x = this.bounds.min.y = 0;
    this.bounds.max.x = this.width;
    this.bounds.max.y = this.height;
    this.invalidBounds = !1
};
_.drawGeometryForHitTest = function() {
    this.graphicContext.beginPath();
    this.graphicContext.rect(0, 0, this.width, this.height)
};
_.clone = function(a) {
    var b = new XOS_Image;
    this.clonePropertiesTo(b);
    a && a.addChild(b);
    return b
};
_.clonePropertiesTo = function(a) {
    XOS_Image.superClass.clonePropertiesTo.call(this, a);
    a.load(this.url);
    a.width = this.width;
    a.height = this.height
};
ExtendClass(XOS_IncludeContent, XOS_Image);

function XOS_IncludeContent() {
    XOS_IncludeContent.baseConstructor.call(this);
    this.type = "INCLUDE";
    this.maintainAspectRatio = !1;
    this.__includeMode = "iframe";
    this.includeUrl = "";
    this.inlineContent = null;
    this.scrolling = "no";
    this.editingUrl = ""
}
_ = XOS_IncludeContent.prototype;
_.__defineGetter__("includeMode", function() {
    //console.log(this.__includeMode);
    return this.__includeMode
});
_.__defineSetter__("includeMode", function(a) {
    this.__includeMode = a;
    "" == this.url && this.load("");
    //console.log(this.url);
});
_.load = function(a, b) {
    "" == a ? ("" != this.editingUrl ? a = this.editingUrl : "foreignObject" == this.includeMode ? a = "../../DATA/janvas/components/_previews/includeHTML-foreignObject.svg" : "includeSVG-js" == this.includeMode ? a = "../../DATA/janvas/components/_previews/includeSVG-js.svg" : "includeSVG-php" == this.includeMode ? a = "../../DATA/janvas/components/_previews/includeSVG-php.svg" : "includeHTML-js" == this.includeMode ? a = "../../DATA/janvas/components/_previews/html.svg" : "includeHTML-php" == this.includeMode ? a = "../../DATA/janvas/components/_previews/includeHTML-php.svg" :
        "iframe" == this.includeMode ? a = "../includeHTML-iframe.svg" : "inlineHTML" == this.includeMode && (a = "../../DATA/janvas/components/_previews/inlineHTML.svg"), XOS_IncludeContent.superClass.load.call(this, a, b), this.url = "") : XOS_IncludeContent.superClass.load.call(this, a, b)
};
_.clone = function(a) {
    var b = new XOS_IncludeContent;
    this.clonePropertiesTo(b);
    a && a.addChild(b);
    return b
};
_.clonePropertiesTo = function(a) {
    XOS_IncludeContent.superClass.clonePropertiesTo.call(this, a);
    a.includeMode = this.includeMode;
    a.includeUrl = this.includeUrl;
    a.inlineContent = this.inlineContent;
    a.scrolling = this.scrolling;
    a.editingUrl = this.editingUrl;
    //console.log(a.includeUrl);
};
ExtendClass(XOS_Group2D, XOS_DisplayObject2D);

function XOS_Group2D() {
    XOS_Group2D.baseConstructor.call(this);
    this.type = "GROUP";
    this.mask = null
}
_ = XOS_Group2D.prototype;
_.normalizePoints = function() {
    this.invalidTransform && this.updateGlobalTransform();
    for (var a = this.globalTransform.getScale(), b = this.displayRef.content.contentScale, b = Math.sqrt(Math.abs(a.x) / b.x * (Math.abs(a.y) / b.y)), c = this.effectList.length, a = 0; a < c; a++) this.effectList[a].scaleProperties(b);
    a = this.localTransform.getRotation();
    b = this.localToGlobal(new XOS_Point2D(0, 0));
    this.localTransform.identity();
    this.localTransform = this.localTransform.prependRotation(a);
    a = this.parent.globalToLocal(b);
    this.localTransform =
        this.localTransform.prependTranslation(a.x, a.y);
    this.globalTransform = this.parent.globalTransform.concat(this.localTransform);
    this.invalidTransform = !1;
    b = this.numChildren();
    for (a = 0; a < b; a++) this.children[a].normalizePoints();
    this.invalidateBounds()
};
_.hitTestObject = function(a, b, c) {
    if (c && this.isSelected || this.isLocked || !this.visible) return {
        hit: !1
    };
    a = this.hitTestChildren(a, b, c);
    return !0 == a.hit ? (a.instanceGroup = this, a) : {
        hit: !1
    }
};
_.getObjectsWithPointsInPath = function(a, b) {
    var c = this.numChildren() - 1,
        d = null;
    if (!0 == b)
        for (; - 1 < c; c--) d = this.children[c].getObjectsWithPointsInPath(a, b), null != d && a.push(d);
    else
        for (; - 1 < c; c--)
            if (d = this.children[c].getObjectsWithPointsInPath(a, b), null != d) return this; return null
};
_.calculateLocalBounds = function() {
    this.bounds = new XOS_Bounding2D;
    for (var a = this.numChildren(), b, c = 0; c < a; c++) b = this.children[c].getLocalBounds().getPoints(), this.children[c].localPointsToGlobalPoints(b, !0), this.globalPointsToLocalPoints(b, !0), this.bounds.addPoints(b);
    this.width = this.bounds.getWidth();
    this.height = this.bounds.getHeight();
    this.invalidBounds = !1
};
_.getAllNonGroupChildrenInDepthAsArray = function(a) {
    for (var b = this.numChildren(), c = 0; c < b; c++) this.children[c] instanceof XOS_Group2D ? this.children[c].getAllNonGroupChildrenInDepthAsArray(a) : a.push(this.children[c])
};
_.draw = function() {
    this.visible && (this.graphicContext.globalCompositeOperation = this.compositeOperation, this.invalidAlpha && this.updateGlobalAlpha(), this.invalidTransform && this.updateGlobalTransform(), this.applyTransform(), this.mask ? (this.graphicContext.save(), this.mask.drawAsMask(), !1 == this.applyEffects() && this.drawChildren(), this.graphicContext.restore()) : !1 == this.applyEffects() && this.drawChildren())
};
_.drawAsSelected = function() {
    XOS_Group2D.superClass.drawAsSelected.call(this);
    for (var a = this.children.length, b = 0; b < a; b++) this.children[b].drawAsSelected()
};
_.clone = function(a) {
    var b = new XOS_Group2D;
    this.clonePropertiesTo(b);
    a && a.addChild(b);
    this.cloneChildren(b);
    this.mask && (b.mask = b.children[this.children.length - 1]);
    return b
};
ExtendClass(XOS_Text, XOS_DisplayObject2D);

function XOS_Text() {
    XOS_Text.baseConstructor.call(this);
    this.text = "";
    this.htmlTextElement = null;
    this.__font = "Arial";
    this.__fontWeight = this.__fontStyle = "normal";
    this.__fontSize = "44";
    this.__textAlign = "start";
    this.linesOffsetY = this.linesOffsetX = 0;
    this.textBaseline = "alphabetic";
    this.__lineHeight = this.baseLineHeight = null;
    this.lineHeightMode = "auto";
    this.textLines = [];
    this.strokeColor = "none";
    this.fillColor = "#000000";
    this.type = "TEXT";
    this.invalidLines = !0;
    this.height = this.width = 0;
    this.fontMetrics = {}
}
_ = XOS_Text.prototype;
_.__defineGetter__("fontSize", function() {
    return this.__fontSize
});
_.__defineSetter__("fontSize", function(a) {
    this.__fontSize = a;
    this.invalidateLines()
});
_.__defineGetter__("font", function() {
    return this.__font
});
_.__defineSetter__("font", function(a) {
    this.__font = a;
    this.invalidateLines()
});
_.__defineGetter__("fontStyle", function() {
    return this.__fontStyle
});
_.__defineSetter__("fontStyle", function(a) {
    this.__fontStyle = a;
    this.invalidateLines()
});
_.__defineGetter__("fontWeight", function() {
    return this.__fontWeight
});
_.__defineSetter__("fontWeight", function(a) {
    this.__fontWeight = a;
    this.invalidateLines()
});
_.__defineGetter__("lineHeight", function() {
    return "auto" == this.lineHeightMode ? "auto" : this.__lineHeight
});
_.__defineSetter__("lineHeight", function(a) {
    "auto" == a || 0 == a ? this.lineHeightMode = "auto" : (this.lineHeightMode = "custom", this.__lineHeight = a);
    this.invalidateLines()
});
_.__defineGetter__("textAlign", function() {
    return this.__textAlign
});
_.__defineSetter__("textAlign", function(a) {
    this.setAlignment(a)
});
_.__defineGetter__("innerText", function() {
    return this.text
});
_.__defineSetter__("innerText", function(a) {
    this.setText(a);
    this.invalidateLines()
});
_.invalidateLines = function() {
    this.invalidLines = !0
};
_.calculateLocalBounds = function() {
    this.bounds = new XOS_Bounding2D;
    var a = window.getComputedStyle(document.body).direction;
    switch (this.__textAlign) {
        case "left":
            this.bounds.min.x = "ltr" == a ? 0 : this.width;
            break;
        case "start":
            this.bounds.min.x = "ltr" == a ? 0 : this.width;
            break;
        case "end":
            this.bounds.min.x = "ltr" == a ? -this.width : 0;
            break;
        case "right":
            this.bounds.min.x = -this.width;
            break;
        case "center":
            this.bounds.min.x = -(this.width / 2)
    }
    this.bounds.min.y = this.linesOffsetY;
    this.bounds.max.x = this.bounds.min.x + this.width;
    this.bounds.max.y =
        this.bounds.min.y + this.height;
    this.invalidBounds = !1;
    this.setAlignment(this.__textAlign)
};
_.setText = function(a) {
    this.text = a;
    this.invalidateLines()
};
_.appendText = function(a) {
    this.text += a;
    this.invalidateLines()
};
_.setAlignment = function(a) {
    this.__textAlign = a;
    this.invalidateBounds()
};
_.textToLines = function() {
    if (this.graphicContext && (this.fontMetrics = this.displayRef.calculateFontMetrics(this.font, this.fontSize), 0 != this.fontMetrics.lineHeight)) {
        "auto" == this.lineHeightMode && (this.__lineHeight = this.fontMetrics.lineHeight);
        this.textLines = [];
        this.graphicContext.font = this.fontStyle + " " + this.fontWeight + " " + this.fontSize + "px " + this.font;
        this.graphicContext.textAlign = this.textAlign;
        this.graphicContext.textBaseline = this.textBaseline;
        this.width = 0;
        for (var a = String(this.text).split(/(?:\r\n|\r|\n)/),
                b, c = a.length, d = 0; d < c; d++) b = this.graphicContext.measureText(a[d]).width, b > this.width && (this.width = b), this.textLines.push(a[d]);
        this.height = this.textLines.length * this.__lineHeight;
        this.linesOffsetY = 0 - (this.__lineHeight - this.fontMetrics.descent);
        this.invalidLines = !1;
        this.invalidateBounds()
    }
};
_.toHTMLElement = function() {
    var a = document.createElement("div");
    a.textGraphicObjectRef = this;
    a.setAttribute("id", "HTML_TEXT_CONTAINER");
    a.setAttribute("contentEditable", "true");
    var b = "background-color:white;  display:block; position:absolute; min-width:20px; line-height:" + this.__lineHeight + "px;",
        b = b + (" -ms-transform:" + this.globalTransform.toString(3) + " translateY(" + this.bounds.min.y + "px); -ms-transform-origin: top left;"),
        b = b + (" -webkit-transform:" + this.globalTransform.toString(3) + " translateY(" + this.bounds.min.y +
            "px); -webkit-transform-origin: top left;"),
        b = b + (" transform:" + this.globalTransform.toString(3) + " translateY(" + this.bounds.min.y + "px); transform-origin: top left;"),
        b = b + " -moz-user-select: text; -webkit-user-select: text; -ms-user-select: text; user-select: text;" + (" font-size:" + this.fontSize + "px; "),
        b = b + (" font-family:" + this.font + ";"),
        b = b + (" font-style:" + this.fontStyle + ";"),
        b = b + (" font-weight:" + this.fontWeight + ";"),
        b = b + (" text-align:" + this.textAlign + ";");
    a.setAttribute("style", b);
    a.innerHTML = this.text.split(/(?:\r\n|\r|\n)/).join("<br/>");
    return a
};
_.toShape = function(a) {
    var b = new XOS_Shape,
        c = new XOS_Path;
    b.paths.add(c);
    for (var d = this.fontMetrics.lineHeight + this.linesOffsetY, e = this.textLines.length, f = 0; f < e; f++) {
        for (var g = a.getPath(this.textLines[f], this.linesOffsetX, d - this.fontMetrics.descent, this.fontSize).commands, h = 0; h < g.length; h += 1) cmd = g[h], "M" === cmd.type ? c.addPoint(new XOS_MoveTo(cmd.x, cmd.y)) : "L" === cmd.type ? c.addPoint(new XOS_LineTo(cmd.x, cmd.y)) : "C" === cmd.type ? c.addPoint(new XOS_CubicCurveTo(cmd.x1, cmd.y1, cmd.x2, cmd.y2, cmd.x, cmd.y)) : "Q" ===
            cmd.type ? c.addPoint(new XOS_QuadraticCurveTo(cmd.x1, cmd.y1, cmd.x, cmd.y)) : "Z" === cmd.type && (c.closePath = !0);
        d += this.__lineHeight
    }
    this.displayRef.globalPropertyToGraphicObject(b);
    b.localTransform = this.localTransform.clone();
    b.globalTransform = this.globalTransform.clone();
    return b
};
_.drawGeometryForHitTest = function() {
    var a = this.getLocalBounds();
    this.graphicContext.beginPath();
    this.graphicContext.rect(a.min.x, a.min.y, a.getWidth(), a.getHeight())
};
_.draw = function() {
    this.visible && (this.graphicContext.globalCompositeOperation = this.compositeOperation, this.invalidAlpha && this.updateGlobalAlpha(), this.invalidTransform && this.updateGlobalTransform(), this.applyTransform(), this.graphicContext.globalAlpha = this.globalAlpha, this.graphicContext.font = this.fontStyle + " " + this.fontWeight + " " + this.fontSize + "px " + this.font, this.graphicContext.textAlign = this.textAlign, this.graphicContext.textBaseline = this.textBaseline, this.invalidLines && this.textToLines(), this.applyEffects() ||
        (this.fillFillingType != XOS_DisplayObject2D.NONE_FILLING && this.drawFill(), this.strokeFillingType != XOS_DisplayObject2D.NONE_FILLING && this.drawStroke()))
};
_.drawFill = function() {
    this.graphicContext.globalAlpha = this.fillAlpha * this.globalAlpha;
    this.fillFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING ? this.graphicContext.fillStyle = this.fillColor : this.fillFillingType == XOS_DisplayObject2D.GRADIENT_FILLING ? this.graphicContext.fillStyle = this.fillGradient.getGradient() : this.fillFillingType == XOS_DisplayObject2D.PATTERN_FILLING && (this.graphicContext.fillStyle = this.fillPattern.pattern);
    for (var a = this.textLines.length, b = this.fontMetrics.lineHeight + this.linesOffsetY,
            c = 0; c < a; c++) this.graphicContext.fillText(this.textLines[c], this.linesOffsetX, b - this.fontMetrics.descent), b += this.__lineHeight
};
_.drawStroke = function() {
    this.graphicContext.globalAlpha = this.strokeAlpha * this.globalAlpha;
    this.graphicContext.lineCap = this.lineCap;
    this.graphicContext.lineJoin = this.lineJoin;
    this.graphicContext.miterLimit = this.miterLimit;
    this.graphicContext.lineWidth = this.lineWidth;
    this.strokeFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING ? this.graphicContext.strokeStyle = this.strokeColor : this.strokeFillingType == XOS_DisplayObject2D.GRADIENT_FILLING ? this.graphicContext.strokeStyle = this.strokeGradient.getGradient() :
        this.strokeFillingType == XOS_DisplayObject2D.PATTERN_FILLING && (this.graphicContext.strokeStyle = this.strokePattern.pattern);
    for (var a = this.textLines.length, b = this.fontMetrics.lineHeight + this.linesOffsetY, c = 0; c < a; c++) this.graphicContext.strokeText(this.textLines[c], this.linesOffsetX, b - this.fontMetrics.descent), b += this.__lineHeight
};
_.clone = function(a) {
    var b = new XOS_Text;
    this.clonePropertiesTo(b);
    a && a.addChild(b);
    return b
};
_.clonePropertiesTo = function(a) {
    XOS_Text.superClass.clonePropertiesTo.call(this, a);
    a.width = this.width;
    a.height = this.height;
    a.text = this.text;
    a.__font = this.__font;
    a.__fontStyle = this.__fontStyle;
    a.__fontWeight = this.__fontWeight;
    a.__fontSize = this.__fontSize;
    a.__textAlign = this.__textAlign;
    a.textBaseline = this.textBaseline;
    a.__lineHeight = this.__lineHeight;
    a.lineHeightMode = this.lineHeightMode;
    a.textLines = this.textLines.clone();
    a.strokeColor = this.strokeColor;
    a.fillColor = this.fillColor;
    a.linesOffsetX = this.linesOffsetX;
    a.linesOffsetY = this.linesOffsetY
};
_.getInspectorPanelName = function() {
    return "textBoxProperties2DPanel"
};
ExtendClass(XOS_TextBox, XOS_Text);

function XOS_TextBox() {
    XOS_TextBox.baseConstructor.call(this);
    this.width = 100;
    this.height = 10;
    this.type = "TEXTBOX";
    this.__fontSize = "22"
}
_ = XOS_TextBox.prototype;
_.calculateLocalBounds = function() {
    this.bounds = new XOS_Bounding2D;
    this.bounds.min.x = 0;
    this.bounds.min.y = 0;
    this.bounds.max.x = this.width;
    this.bounds.max.y = this.height;
    this.invalidBounds = !1;
    this.setAlignment(this.__textAlign)
};
_.setAlignment = function(a) {
    this.__textAlign = a;
    var b = window.getComputedStyle(document.body).direction;
    switch (a) {
        case "left":
            this.linesOffsetX = "ltr" == b ? 0 : this.width;
            break;
        case "start":
            this.linesOffsetX = "ltr" == b ? 0 : this.width;
            break;
        case "end":
            this.linesOffsetX = "ltr" == b ? this.width : 0;
            break;
        case "right":
            this.linesOffsetX = this.width;
            break;
        case "center":
            this.linesOffsetX = this.width / 2
    }
};
_.toHTMLElement = function() {
    var a = document.createElement("div");
    a.textGraphicObjectRef = this;
    a.setAttribute("id", "HTML_TEXT_CONTAINER");
    a.setAttribute("contentEditable", "true");
    var b = "background-color:white;  display:block; position:absolute; min-height:20px; min-width:20px; width:" + this.width + "px; max-width:" + this.width + "px; line-height:" + this.__lineHeight + "px;",
        b = b + (" -ms-transform:" + this.globalTransform.toString(3) + " translateY(" + this.bounds.min.y + "px); -ms-transform-origin: top left;"),
        b = b + (" -webkit-transform:" +
            this.globalTransform.toString(3) + " translateY(" + this.bounds.min.y + "px); -webkit-transform-origin: top left;"),
        b = b + (" transform:" + this.globalTransform.toString(3) + " translateY(" + this.bounds.min.y + "px); transform-origin: top left;"),
        b = b + (" font-size:" + this.fontSize + "px; "),
        b = b + (" font-style:" + this.fontStyle + ";"),
        b = b + (" font-weight:" + this.fontWeight + ";"),
        b = b + (" font-family:" + this.font + ";"),
        b = b + (" text-align:" + this.textAlign + ";");
    a.setAttribute("style", b + " -moz-user-select: text; -webkit-user-select: text; -ms-user-select: text; user-select: text;");
    a.innerHTML = this.text.split(/(?:\r\n|\r|\n)/).join("<br/>");
    return a
};
_.textToLines = function() {
    if (this.graphicContext && (this.fontMetrics = this.displayRef.calculateFontMetrics(this.font, this.fontSize), 0 != this.fontMetrics.lineHeight)) {
        "auto" == this.lineHeightMode && (this.__lineHeight = this.fontMetrics.lineHeight);
        this.textLines = [];
        for (var a, b, c, d = String(this.text).split(/(?:\r\n|\r|\n)/), e = d.length, f = 0; f < e; f++) {
            b = d[f].split(" ");
            for (var g = b.length, h = 0; h < g; h++) {
                c = b[h];
                a = this.graphicContext.measureText(c).width;
                if (a > this.width) {
                    var k = c.split("");
                    c = k[0];
                    for (var l = k.length, q =
                            0; q < l - 1;) q++, a = this.graphicContext.measureText(c + k[q]).width, a > this.width ? (this.textLines.push(c), c = k[q]) : c += k[q]
                }
                for (; h < g - 1;)
                    if (h++, a = this.graphicContext.measureText(c + " " + b[h]).width, a < this.width) c += " " + b[h];
                    else {
                        h--;
                        break
                    }
                this.textLines.push(c)
            }
        }
        this.height = this.textLines.length * this.__lineHeight;
        0 == this.height && (this.height = 20);
        this.invalidLines = !1;
        this.invalidateBounds()
    }
};
_.scaleFromPoint = function(a, b, c, d) {
    XOS_TextBox.superClass.scaleFromPoint.call(this, a, b, c, d);
    !0 == d && (this.localTransform = this.localTransform.appendScale(1 / a, 1 / b), this.width *= a, this.height *= b, this.invalidateLines(), this.invalidateBounds())
};
_.clone = function(a) {
    var b = new XOS_TextBox;
    this.clonePropertiesTo(b);
    a && a.addChild(b);
    return b
};
_.getInspectorPanelName = function() {
    return "textBoxProperties2DPanel"
};
ExtendClass(XOS_InstanceSymbol2D, XOS_Group2D);

function XOS_InstanceSymbol2D() {
    XOS_InstanceSymbol2D.baseConstructor.call(this);
    this.type = "SYMBOL";
    this.height = this.width = 100;
    this.idSymbolRef = null
}
_ = XOS_InstanceSymbol2D.prototype;
_.clone = function(a) {
    var b = new XOS_InstanceSymbol2D;
    this.clonePropertiesTo(b);
    this.cloneChildren(b);
    a && a.addChild(b);
    return b
};
_.clonePropertiesTo = function(a) {
    XOS_InstanceSymbol2D.superClass.clonePropertiesTo.call(this, a);
    a.width = this.width;
    a.height = this.height;
    a.idSymbolRef = this.idSymbolRef
};
_.normalizePoints = function() {
    this.invalidTransform && this.updateGlobalTransform();
    this.globalTransformToLocalTransform(this.globalTransform);
    this.invalidateBounds();
    var a = this.effectList.length;
    if (0 < a)
        for (var b = this.globalTransform.getScale().x / this.displayRef.content.contentScale.x, c = 0; c < a; c++) this.effectList[c].scale = b
};

function XOS_LinearGradient(a) {
    this.displayRef = a;
    this.id = null;
    this.x2 = this.y1 = this.x1 = 0;
    this.y2 = 200;
    this.localTransform = new XOS_Matrix2D;
    this.colors = [];
    this.gradient = null;
    this.gradientUnits = "";
    this.invalidGradient = !0
}
_ = XOS_LinearGradient.prototype;
_.setPoints = function(a, b, c, d) {
    this.x1 = a;
    this.y1 = b;
    this.x2 = c;
    this.y2 = d
};
_.copyPointsTo = function(a) {
    a.x1 = this.x1;
    a.y1 = this.y1;
    a.x2 = this.x2;
    a.y2 = this.y2
};
_.getPointsAsArray = function() {
    return [{
        x: this.x1,
        y: this.y1
    }, {
        x: this.x2,
        y: this.y2
    }]
};
_.setPointsByArray = function(a) {
    this.x1 = a[0].x;
    this.y1 = a[0].y;
    this.x2 = a[1].x;
    this.y2 = a[1].y
};
_.addColor = function(a) {
    this.colors.push(a);
    this.invalidateGradient()
};
_.updateGradient = function() {
    if (this.displayRef) {
        this.gradient = this.displayRef.graphicContext.createLinearGradient(this.x1, this.y1, this.x2+140, this.y2);
        var a = this.colors.length;
        try {
            for (var b = 0; b < a; b++) this.gradient.addColorStop(this.colors[b].offset, this.colors[b].color);
            this.invalidGradient = !1
        } catch (c) {
            this.invalidGradient = !1
        }
    }
};
_.invalidateGradient = function() {
    this.invalidGradient = !0
};
_.getGradient = function() {
    this.invalidGradient && this.updateGradient();
    return this.gradient
};
_.toRadialGradient = function() {
    for (var a = new XOS_RadialGradient(this.displayRef), b = this.colors.length, c = 0; c < b; c++) a.colors.push({
        offset: this.colors[c].offset,
        color: this.colors[c].color
    });
    return a
};
_.toHtmlStyle = function() {
    if (0 == this.colors.length) return "background-image: -webkit-linear-gradient(bottom, rgb(246,187,238) 44%, rgb(255,225,255) 72%);";
    var a;
    a = "background-image: -webkit-linear-gradient(left,";
    for (var b = this.colors.length - 1, c = 0; c < b; c++) a += this.colors[c].color + " " + 100 * this.colors[c].offset + "%,";
    return a += this.colors[c].color + " " + 100 * this.colors[c].offset + "%);"
};
_.clone = function(a) {
    a = new XOS_LinearGradient;
    this.clonePropertiesTo(a);
    return a
};
_.clonePropertiesTo = function(a) {
    for (var b = this.colors.length, c = 0; c < b; c++) a.colors.push({
        offset: this.colors[c].offset,
        color: this.colors[c].color
    });
    a.displayRef = this.displayRef;
    a.x1 = this.x1;
    a.y1 = this.y1;
    a.x2 = this.x2;
    a.y2 = this.y2;
    a.gradient = this.gradient;
    a.localTransform = this.localTransform
};
_.toXml = function(a) {
    var b = document.createElement("linearGradient");
    this.propertiesToXmlAttributes(b);
    for (var c = null, d = this.colors.length, e = 0; e < d; e++) c = document.createElement("stop"), c.setAttribute("offset", this.colors[e].offset), c.setAttribute("color", this.colors[e].color), b.appendChild(c);
    a.appendChild(b)
};
_.propertiesToXmlAttributes = function(a) {
    a.setAttribute("x1", this.x1);
    a.setAttribute("y1", this.y1);
    a.setAttribute("x2", this.x2);
    a.setAttribute("y2", this.y2);
    a.setAttribute("gradientUnits", this.gradientUnits)
};
_.createColorsFromHtmlGradient = function(a) {
    this.colors = [];
    var b = a.indexOf("("),
        c = a.lastIndexOf(")");
    a = a.substring(b + 1, c);
    var d, b = a.indexOf("rgb"); - 1 == b && (b = a.indexOf("rgba"));
    for (-1 == b && (b = a.indexOf("#")); - 1 != b;) c = a.indexOf(")", b + 1), d = a.substring(b, c + 1), b = c + 1, c = a.indexOf("%", b + 1), b = Number(a.substring(b, c)) / 100, this.addColor({
        offset: b,
        color: d
    }), b = a.indexOf("rgb", c + 1), -1 == b && (b = a.indexOf("rgba", c + 1)), -1 == b && (b = a.indexOf("#", c + 1));
    return this.colors
};
XOS_LinearGradient.createFromHtmlGradient = function(a, b) {
    var c = new XOS_LinearGradient(b);
    c.createColorsFromHtmlGradient(a);
    return c
};

function XOS_RadialGradient(a) {
    this.displayRef = a;
    this.id = null;
    this.y2 = this.x2 = this.y1 = this.x1 = 100;
    this.r1 = 1;
    this.r2 = 100;
    this.localTransform = new XOS_Matrix2D;
    this.colors = [];
    this.gradient = null;
    this.gradientUnits = "";
    this.invalidGradient = !0
}
_ = XOS_RadialGradient.prototype;
_.setPoints = function(a, b, c, d, e, f) {
    this.x1 = a;
    this.y1 = b;
    this.x2 = c;
    this.y2 = d;
    this.r1 = e;
    this.r2 = f
};
_.copyPointsTo = function(a) {
    a.x1 = this.x1;
    a.y1 = this.y1;
    a.x2 = this.x2;
    a.y2 = this.y2;
    a.r1 = this.r1;
    a.r2 = this.r2
};
_.getPointsAsArray = function() {
    return [{
        x: this.x1,
        y: this.y1
    }, {
        x: this.x2,
        y: this.y2
    }]
};
_.setPointsByArray = function(a) {
    this.x1 = a[0].x;
    this.y1 = a[0].y;
    this.x2 = a[1].x;
    this.y2 = a[1].y
};
_.addColor = function(a) {
    this.colors.push(a);
    this.invalidateGradient()
};
_.updateGradient = function() {
    if (this.displayRef) {
        this.gradient = this.displayRef.graphicContext.createRadialGradient(this.x1, this.y1, this.r1, this.x2, this.y2, this.r2);
        for (var a = this.colors.length, b = 0; b < a; b++) this.gradient.addColorStop(this.colors[b].offset, this.colors[b].color);
        this.invalidGradient = !1
    }
};
_.invalidateGradient = function() {
    this.invalidGradient = !0
};
_.getGradient = function() {
    this.invalidGradient && this.updateGradient();
    return this.gradient
};
_.toLinearGradient = function() {
    for (var a = new XOS_LinearGradient(this.displayRef), b = this.colors.length, c = 0; c < b; c++) a.colors.push({
        offset: this.colors[c].offset,
        color: this.colors[c].color
    });
    return a
};
_.toHtmlStyle = function() {
    if (0 == this.colors.length) return "background-image: -webkit-linear-gradient(bottom, rgb(246,187,238) 44%, rgb(255,225,255) 72%);";
    var a;
    a = "background-image: -webkit-radial-gradient(center, ellipse cover, ";
    for (var b = this.colors.length - 1, c = 0; c < b; c++) a += this.colors[c].color + " " + 100 * this.colors[c].offset + "%,";
    return a += this.colors[c].color + " " + 100 * this.colors[c].offset + "%);"
};
_.clone = function(a) {
    a = new XOS_RadialGradient;
    this.clonePropertiesTo(a);
    return a
};
_.clonePropertiesTo = function(a) {
    for (var b = this.colors.length, c = 0; c < b; c++) a.colors.push({
        offset: this.colors[c].offset,
        color: this.colors[c].color
    });
    a.displayRef = this.displayRef;
    a.x1 = this.x1;
    a.y1 = this.y1;
    a.x2 = this.x2;
    a.y2 = this.y2;
    a.r1 = this.r1;
    a.r2 = this.r2;
    a.gradient = this.gradient;
    a.localTransform = this.localTransform
};
_.toXml = function(a) {
    var b = document.createElement("radialGradient");
    this.propertiesToXmlAttributes(b);
    for (var c = null, d = this.colors.length, e = 0; e < d; e++) c = document.createElement("stop"), c.setAttribute("offset", this.colors[e].offset), c.setAttribute("color", this.colors[e].color), b.appendChild(c);
    a.appendChild(b)
};
_.propertiesToXmlAttributes = function(a) {
    a.setAttribute("x1", this.x1);
    a.setAttribute("y1", this.y1);
    a.setAttribute("r1", this.r1);
    a.setAttribute("x2", this.x2);
    a.setAttribute("y2", this.y2);
    a.setAttribute("r2", this.r2);
    a.setAttribute("gradientUnits", this.gradientUnits)
};
_.createColorsFromHtmlGradient = function(a) {
    this.colors = [];
    var b = a.indexOf("("),
        c = a.lastIndexOf(")");
    a = a.substring(b + 1, c);
    var d, b = a.indexOf("rgb"); - 1 == b && (b = a.indexOf("rgba"));
    for (-1 == b && (b = a.indexOf("#")); - 1 != b;) c = a.indexOf(")", b + 1), d = a.substring(b, c + 1), b = c + 1, c = a.indexOf("%", b + 1), b = Number(a.substring(b, c)) / 100, this.addColor({
        offset: b,
        color: d
    }), b = a.indexOf("rgb", c + 1), -1 == b && (b = a.indexOf("rgba", c + 1)), -1 == b && (b = a.indexOf("#", c + 1));
    return this.colors
};
XOS_RadialGradient.createFromHtmlGradient = function(a, b) {
    var c = new XOS_RadialGradient(b);
    c.createColorsFromHtmlGradient(a);
    return c
};

function XOS_Pattern(a) {
    this.displayRef = a;
    this.id = null;
    this.name = "";
    this.patternUnits = null;
    this.image = new Image;
    this.pattern = null;
    this.repeatMode = "repeat";
    this.localTransform = new XOS_Matrix2D;
    this.isLoaded = !1;
    this.onLoadFunctionListener = null;
    this.height = this.width = 0
}
_ = XOS_Pattern.prototype;
_.setImage = function(a) {
    this.image = a
};
_.load = function(a, b) {
    this.isLoaded = !1;
    this.image.src = a;
    this.image.onload = Bind(this, this.onload);
    this.onLoadFunctionListener = b
};
_.onload = function(a) {
    0 == this.width && (this.width = this.image.width);
    0 == this.height && (this.height = this.image.height);
    this.isLoaded = !0;
    if (this.onLoadFunctionListener) this.onLoadFunctionListener(this);
    null != this.displayRef && (this.pattern = this.displayRef.graphicContext.createPattern(this.image, this.repeatMode), this.displayRef.invalidateRender())
};
_.toHtmlStyle = function() {
    return "background-image:url(" + this.image.src + "); background-repeat: " + this.repeatMode + ";"
};
_.clone = function(a) {
    a = new XOS_Pattern;
    this.clonePropertiesTo(a);
    return a
};
_.clonePropertiesTo = function(a) {
    a.displayRef = this.displayRef;
    a.image = this.image;
    a.pattern = this.pattern;
    a.repeatMode = this.repeatMode;
    a.localTransform = this.localTransform;
    a.width = this.width;
    a.height = this.height
};

function XOS_Effect(a) {
    this.displayRef = a;
    this.graphicContextTarget = a.graphicContext;
    this.graphicObjectTarget = null;
    this.name = "BASE_Effect";
    this.type = "EFFECT";
    this.visible = !0
}
_ = XOS_Effect.prototype;
_.setTarget = function(a) {
    this.graphicObjectTarget = a;
    a.displayRef && (this.displayRef = a.displayRef);
    this.graphicContextTarget = a.graphicContext
};
_.setProperties = function(a) {};
_.scaleProperties = function(a) {};
_.apply = function() {
    this.graphicObjectTarget instanceof XOS_Shape ? this.applyToShape() : this.graphicObjectTarget instanceof XOS_Group2D ? this.applyToGroup() : this.graphicObjectTarget instanceof XOS_Text ? this.applyToText() : this.graphicObjectTarget instanceof XOS_Image && this.applyToImage()
};
_.applyToShape = function() {};
_.applyToGroup = function() {};
_.applyToImage = function() {};
_.applyToText = function() {};
_.getInspectorPanelName = function() {
    return "effectProperties2DPanel"
};
_.clone = function() {
    var a = new XOS_Effect;
    this.clonePropertiesTo(a);
    return a
};
_.clonePropertiesTo = function(a) {
    a.displayRef = this.displayRef;
    a.graphicContextTarget = this.graphicContextTarget
};
ExtendClass(XOS_Shadow_effect, XOS_Effect);

function XOS_Shadow_effect(a) {
    XOS_Shadow_effect.baseConstructor.call(this, a);
    this.name = "Shadow";
    this.shadowColor = "rgba(0,0,0,.5)";
    this.shadowBlur = this.shadowOffsetY = this.shadowOffsetX = 10;
    this.scale = 1
}
_ = XOS_Shadow_effect.prototype;
_.applyProperties = function() {
    var a = this.scale * this.displayRef.content.contentScale.x;
    this.graphicContextTarget.save();
    this.graphicContextTarget.shadowColor = this.shadowColor;
    this.graphicContextTarget.shadowOffsetX = this.shadowOffsetX * a;
    this.graphicContextTarget.shadowOffsetY = this.shadowOffsetY * a;
    this.graphicContextTarget.shadowBlur = this.shadowBlur * a
};
_.applyToShape = function() {
    this.applyProperties();
    this.graphicObjectTarget.drawGeometry();
    this.graphicObjectTarget.fillFillingType != XOS_DisplayObject2D.NONE_FILLING ? (this.graphicObjectTarget.drawFill(), this.graphicObjectTarget.strokeFillingType != XOS_DisplayObject2D.NONE_FILLING && (this.graphicContextTarget.shadowColor = "none", this.graphicObjectTarget.drawStroke())) : this.graphicObjectTarget.strokeFillingType != XOS_DisplayObject2D.NONE_FILLING && this.graphicObjectTarget.drawStroke();
    this.graphicContextTarget.restore()
};
_.applyToGroup = function() {
    this.applyProperties();
    this.graphicObjectTarget.drawChildren();
    this.graphicContextTarget.restore();
    this.graphicObjectTarget.drawChildren()
};
_.applyToImage = function() {
    this.applyProperties();
    this.graphicContextTarget.drawImage(this.graphicObjectTarget.image, 0, 0, this.graphicObjectTarget.width, this.graphicObjectTarget.height);
    this.graphicContextTarget.restore()
};
_.applyToText = function() {
    this.applyToShape()
};
_.setProperties = function(a) {
    this.shadowColor = a.shadowColor;
    this.shadowOffsetX = a.shadowOffsetX;
    this.shadowOffsetY = a.shadowOffsetY;
    this.shadowBlur = a.shadowBlur
};
_.scaleProperties = function(a) {
    this.shadowOffsetX *= a;
    this.shadowOffsetY *= a;
    this.shadowBlur *= a
};
_.clone = function() {
    var a = new XOS_Shadow_effect(this.displayRef);
    this.clonePropertiesTo(a);
    return a
};
_.clonePropertiesTo = function(a) {
    a.shadowColor = this.shadowColor;
    a.shadowOffsetX = this.shadowOffsetX;
    a.shadowOffsetY = this.shadowOffsetY;
    a.shadowBlur = this.shadowBlur;
    a.displayRef = this.displayRef;
    a.graphicContextTarget = this.graphicContextTarget
};

function XOS_SVGTween(a) {
    this.id = "";
    this.name = "generalTween";
    this.displayRef = a;
    this.graphicObjectTarget;
    this.dur = this.end = this.begin = "";
    this.repeatCount = 1;
    this.attributeName = "";
    this.attributeType = "auto";
    this.type = "";
    this.additive = "replace";
    this.accumulate = "none";
    this.fill = "remove";
    this.keyTimes = this.by = this.to = this.from = "";
    this.motionType = "easeInOut";
    this.keySplines = "";
    this.calcMode = "linear";
    this.restart = "always"
}
XOS_SVGTween.motionTypeList = {
    linear: "0 0 1 1",
    easeInOut: ".5 0 .5 1",
    easeIn: ".75 0 1 .25",
    easeOut: "0 .75 .25 1"
};
_ = XOS_SVGTween.prototype;
_.setTarget = function(a) {
    this.graphicObjectTarget = a;
    a.displayRef && (this.displayRef = a.displayRef)
};
_.getInspectorPanelName = function() {
    return "animateSvgXmlCssProperties2DPanel"
};
_.clone = function(a) {
    a = new XOS_SVGTween;
    this.clonePropertiesTo(a);
    return a
};
_.setProperties = function(a) {
    a.hasOwnProperty("id") && (this.id = a.id);
    a.hasOwnProperty("name") && (this.name = a.name);
    a.hasOwnProperty("begin") && (this.begin = a.begin);
    a.hasOwnProperty("dur") && (this.dur = a.dur);
    a.hasOwnProperty("repeatCount") && (this.repeatCount = a.repeatCount);
    a.hasOwnProperty("attributeName") && (this.attributeName = a.attributeName);
    a.hasOwnProperty("attributeType") && (this.attributeType = a.attributeType);
    a.hasOwnProperty("type") && (this.type = a.type);
    a.hasOwnProperty("additive") && (this.additive =
        a.additive);
    a.hasOwnProperty("accumulate") && (this.accumulate = a.accumulate);
    a.hasOwnProperty("fill") && (this.fill = a.fill);
    a.hasOwnProperty("from") && (this.from = a.from);
    a.hasOwnProperty("to") && (this.to = a.to);
    a.hasOwnProperty("motionType") && (this.motionType = a.motionType);
    a.hasOwnProperty("restart") && (this.restart = a.restart);
    a.hasOwnProperty("calcMode") && (this.calcMode = a.calcMode);
    a.hasOwnProperty("keySplines") && (this.keySplines = a.keySplines)
};
_.clonePropertiesTo = function(a) {
    a.id = this.id;
    a.name = this.name;
    a.begin = this.begin;
    a.dur = this.dur;
    a.repeatCount = this.repeatCount;
    a.attributeName = this.attributeName;
    a.attributeType = this.attributeType;
    a.type = this.type;
    a.additive = this.additive;
    a.accumulate = this.accumulate;
    a.fill = this.fill;
    a.from = this.from;
    a.to = this.to;
    a.motionType = this.motionType;
    a.restart = this.restart;
    a.calcMode = this.calcMode;
    a.keySplines = this.keySplines
};
ExtendClass(XOS_SVGTweenAnimateTransform, XOS_SVGTween);

function XOS_SVGTweenAnimateTransform(a) {
    XOS_SVGTweenAnimateTransform.baseConstructor.call(this, a);
    this.name = "animateTransform";
    this.attributeName = "transform";
    this.calcMode = "spline";
    this.type = "translate"
}
_ = XOS_SVGTweenAnimateTransform.prototype;
_.getInspectorPanelName = function() {
    return "animateSvgTransformProperties2DPanel"
};
_.clone = function(a) {
    a = new XOS_SVGTweenAnimateTransform;
    this.clonePropertiesTo(a);
    return a
};
ExtendClass(XOS_SVGTweenAnimate, XOS_SVGTween);

function XOS_SVGTweenAnimate(a) {
    XOS_SVGTweenAnimate.baseConstructor.call(this, a);
    this.name = "animate"
}
_ = XOS_SVGTweenAnimate.prototype;
_.getInspectorPanelName = function() {
    return "animateSvgXmlCssProperties2DPanel"
};
_.clone = function(a) {
    a = new XOS_SVGTweenAnimate;
    this.clonePropertiesTo(a);
    return a
};

function XOS_CSSAnimation() {
    this.id = "";
    this.type = "CSS_ANIMATION";
    this.animationName = "bounceIn";
    this.delay = 0;
    this.duration = 2;
    this.iterationCount = "1";
    this.fillMode = "both";
    this.graphicObjectTarget = null
}
_ = XOS_CSSAnimation.prototype;
_.setTarget = function(a) {
    this.graphicObjectTarget = a
};
_.clone = function(a) {
    a = new XOS_CSSAnimation;
    this.clonePropertiesTo(a);
    return a
};
_.setProperties = function(a) {
    a.hasOwnProperty("id") && (this.id = a.id);
    a.hasOwnProperty("animationName") && (this.animationName = a.animationName);
    a.hasOwnProperty("delay") && (this.delay = a.delay);
    a.hasOwnProperty("duration") && (this.duration = a.duration);
    a.hasOwnProperty("iterationCount") && (this.iterationCount = a.iterationCount);
    a.hasOwnProperty("fillMode") && (this.fillMode = a.fillMode)
};
_.clonePropertiesTo = function(a) {
    a.id = this.id;
    a.animationName = this.animationName;
    a.delay = this.delay;
    a.duration = this.duration;
    a.iterationCount = this.iterationCount;
    a.fillMode = this.fillMode
};

function XOS_DisplayObject2DUnserializer(a) {
    this.displayRef = a;
    this.defsLibrary = new DefsLibrary;
    this.options = {};
    this.html_svg = {};
    this.publisher = null;
    this.lastCurrentPt = {
        x: 0,
        y: 0
    };
    this.unitValue = 1;
    this.definedCSSClasses = {}
}
_ = XOS_DisplayObject2DUnserializer.prototype;
_.setUnitValue = function(a) {
    switch (a) {
        case "":
            this.unitValue = 1;
            break;
        case "pt":
            this.unitValue = 1.25;
            break;
        case "pc":
            this.unitValue = 15;
            break;
        case "mm":
            this.unitValue = 3.543307;
            break;
        case "cm":
            this.unitValue = 35.43307;
            break;
        case "in":
            this.unitValue = 90;
            break;
        case "em":
            this.unitValue = 1;
            break;
        case "ex":
            this.unitValue = 1
    }
};
_.unserialize = function(a, b, c) {
    c && (this.options = c);
    this.parseXmlNode(a, b);
    a = this.displayRef.getLayers();
    this.displayRef.setCurrentLayer(a[a.length - 1])
};
_.parseXmlChildren = function(a, b) {
    if (a)
        for (var c = a.childNodes.length, d = 0; d < c; d++) this.parseXmlNode(a.childNodes[d], b)
};
_.parseXmlNode = function(a, b) {
    var c = a.localName;
    switch (c) {
        case "parsererror":
           // alert("parsererror: " + c); 
            break;
        case "div":
            if (a.hasAttribute("data-svg-idi")) {
                var d = a.getAttribute("data-svg-idi");
                if (this.html_svg[d])
                    if (a.hasAttribute("data-inline-content")) {
                        if (this.html_svg[d] instanceof XOS_IncludeContent) {
                            this.html_svg[d].inlineContent = a.innerHTML.replace(/<!\[CDATA\[/g, "").replace(/\]\]\>/g, "");
                            break
                        }
                    } else {
                        var e = a.getElementsByTagName("svg");
                        if (0 == e.length) break;
                        var e = e[0],
                            f = e.childNodes[0];
                        f.setAttribute("class",
                            this.extractClassNames(a));
                        a.hasAttribute("id") && f.setAttribute("id", a.getAttribute("id"));
                        this.parseXmlChildren(e, this.html_svg[d]);
                        d = this.html_svg[d];
                        e = d.parent;
                        f = d.children[0];
                        f.localTransform = d.localTransform.clone();
                        f.placeIntoHtmlLayer = "yes";
                        var g = e.getChildIndex(d);
                        e.removeChild(d);
                        e.addChildAt(f, g);
                        break
                    }
            }
            this.parseXmlChildren(a, b);
            break;
        case "svg":
            a.hasAttribute("inkscape:version") && (this.publisher = "inkscape");
            this.options.ignorePageSize || (d = 740, e = 416, a.hasAttribute("width") && (f = a.getAttribute("width"),
                d = parseFloat(f)), a.hasAttribute("height") && (f = a.getAttribute("height"), e = parseFloat(f)), a.hasAttribute("pageAlignment") && this.displayRef.setPageAlignment(a.getAttribute("pageAlignment")), a.hasAttribute("bodyBackgroundColor") && (this.displayRef.bodyBackgroundColor = a.getAttribute("bodyBackgroundColor"), this.displayRef.setPageColor(this.displayRef.bodyBackgroundColor)), this.displayRef.setPageSize(d, e), this.displayRef.fitToView());
            this.parseXmlChildren(a, b);
            break;
        case "title":
            a.firstChild && (b == this.displayRef.content ?
                this.displayRef.title = a.firstChild.nodeValue : b.title = a.firstChild.nodeValue);
            break;
        case "desc":
            a.firstChild && (b == this.displayRef.content ? this.displayRef.description = a.firstChild.nodeValue : b.description = a.firstChild.nodeValue);
            break;
        case "link":
            if (a.hasAttribute("rel") && "stylesheet" == a.getAttribute("rel"))
                if (d = a.getAttribute("href"), -1 != d.indexOf("//fonts.googleapis.com/css?family")) {
                    var d = d.split("=")[1],
                        h = this.displayRef;
                    WebFont.load({
                        google: {
                            families: [d.replace(/ /g, "+")]
                        },
                        active: function() {
                            h.invalidateRender()
                        }
                    })
                } else this.displayRef.cssIncludes.push(d);
            break;
        case "meta":
            a.hasAttribute("name") ? "description" != a.getAttribute("name") && this.displayRef.metaTags.push(a.cloneNode(!0)) : a.hasAttribute("http-equiv") && a.hasAttribute("content") ? "text/html; charset=UTF-8" != a.getAttribute("content") && this.displayRef.metaTags.push(a.cloneNode(!0)) : this.displayRef.metaTags.push(a.cloneNode(!0));
            break;
        case "script":
            a.hasAttribute("xlink:href") ? (d = a.getAttribute("xlink:href"), this.displayRef.jsIncludes.push(d)) : a.hasAttribute("src") && (d = a.getAttribute("src"), this.displayRef.jsIncludes.push(d));
            break;
        case "a":
            a.hasAttribute("xlink:href") && (d = a.getAttribute("xlink:href"), e = "", a.hasAttribute("target") && (e = a.getAttribute("target")), f = b.children.length, this.parseXmlChildren(a, b), b.getChildAt(f).anchorLink = d, b.getChildAt(f).anchorLinkTarget = e);
            break;
        case "g":
            if (a.hasAttribute("type")) "LAYER" == a.getAttribute("type") && (!0 == this.options.ignoreLayers ? this.parseXmlChildren(a, this.displayRef.currentLayer) : (d = null, a.hasAttribute("name") && (d = this.displayRef.getLayerByName(a.getAttribute("name"))), null !=
                d ? this.parseXmlChildren(a, d) : (d = new XOS_Layer, this.parseGraphicProperties(a, d), this.displayRef.content.addChild(d), this.parseXmlChildren(a, d), a.hasAttribute("locked") && "true" == a.getAttribute("locked") && (d.isLocked = !0), console.log(a), console.log(d))));
            else if (b.addChild) {
                e = new XOS_Group2D;
                this.parseGraphicProperties(a, e);
                b.addChild(e);
                if (a.hasAttribute("data-svg-idi")) {
                    d = a.getAttribute("data-svg-idi");
                    this.html_svg[d] = e;
                    break
                }
                this.parseXmlChildren(a, e);
                this.checkAndSetClipPath(a, e)
            }
            break;
        case "image":
        //alert("jdfgd");
            if (a.hasAttribute("include-mode")) {
                e =
                    new XOS_IncludeContent;
                a.hasAttribute("include-mode") && (e.includeMode = a.getAttribute("include-mode"));
                a.hasAttribute("include-url") && (e.includeUrl = a.getAttribute("include-url"));
                console.log(e.includeUrl);
                a.hasAttribute("editing-url") && (e.editingUrl = a.getAttribute("editing-url"));
                a.hasAttribute("scrolling") && (e.scrolling = a.getAttribute("scrolling"));
                d = a.getAttribute("xlink:href");
                this.parseGraphicProperties(a, e);
                e.load(d);
                b.addChild(e);
                if ("inlineHTML" == a.getAttribute("include-mode") && a.firstChild && a.firstChild.nodeValue) {
                    e.inlineContent =
                        a.firstChild.nodeValue.replace(/<!\[CDATA\[/g, "").replace(/\]\]\>/g, "");
                    break
                }
                a.hasAttribute("data-svg-idi") && (d = a.getAttribute("data-svg-idi"), this.html_svg[d] = e)
            } else {
                if (b instanceof XOS_Pattern) {
                    a.hasAttribute("xlink:href") && (d = a.getAttribute("xlink:href"));
                    b.load(d);
                    break
                }
                e = new XOS_Image;
                this.parseGraphicProperties(a, e);
                a.hasAttribute("url") && (d = a.getAttribute("url"));
                a.hasAttribute("xlink:href") && (d = a.getAttribute("xlink:href"));
                e.load(d);
                b.addChild(e);
                this.parseXmlChildren(a, e)
            }
            break;
        case "symbol":
            d =
                new XOS_InstanceSymbol2D;
            this.parseGraphicProperties(a, d);
            this.parseXmlChildren(a, d);
            a.hasAttribute("viewBox") && (d.viewBox = this.parseViewBox(a.getAttribute("viewBox")));
            d.idSymbolRef = a.getAttribute("id");
            this.displayRef.addSymbolToLibrary(d.idSymbolRef, d);
            console.log(d);
            break;
        case "filter":
            this.parseEffect(a);
            break;
        case "animate":
        case "animateTransform":
            d = this.parseTween(a);
            b.addTween(d);
            break;
        case "cssAnimation":
            d = this.parseCssAnimation(a);
            b.addAnimation(d);
            break;
        case "pattern":
            d = new XOS_Pattern(this.displayRef);
            this.parsePattern(a, d);
            this.parseXmlChildren(a, d);
            break;
        case "lineargradient":
        case "linearGradient":
            d = new XOS_LinearGradient(this.displayRef);
            this.parseLinearGradient(a, d);
            this.parseXmlChildren(a, d);
            break;
        case "radialgradient":
        case "radialGradient":
            d = new XOS_RadialGradient(this.displayRef);
            this.parseRadialGradient(a, d);
            this.parseXmlChildren(a, d);
            break;
        case "stop":
            d = {
                color: "#FF0000"
            };
            d.offset = Number(a.getAttribute("offset"));
            if (a.hasAttribute("style"))
                for (var e = a.getAttribute("style").split(";"), f =
                        e.length, k = null, l, g = 0; g < f; g++) k = e[g].split(":"), l = k[0], k = k[1], "stop-color" == l && (d.color = k), "stop-opacity" == l && (l = XOS_ColorUtils.htmlColorStringToRGBA(d.color), l.a = parseFloat(k), d.color = "rgba(" + (255 * l.r).toFixed(0) + "," + (255 * l.g).toFixed(0) + "," + (255 * l.b).toFixed(0) + "," + l.a + ")");
            else a.hasAttribute("stop-color") && (d.color = a.getAttribute("stop-color")), a.hasAttribute("stop-opacity") && (l = XOS_ColorUtils.htmlColorStringToRGBA(d.color), l.a = parseFloat(a.getAttribute("stop-opacity")), d.color = "rgba(" + (255 * l.r).toFixed(0) +
                "," + (255 * l.g).toFixed(0) + "," + (255 * l.b).toFixed(0) + "," + l.a + ")");
            b.addColor(d);
            break;
        case "defs":
            this.parseXmlChildren(a, this.defsLibrary);
            break;
        case "clipPath":
            a.hasAttribute("id") && (e = a.getAttribute("id"), d = new ClipPath, this.defsLibrary[e] = d, this.parseXmlChildren(a, d));
            break;
        case "use":
            e = a.getAttribute("xlink:href").replace("#", "");
            b instanceof ClipPath ? b.shape = this.defsLibrary[e] : b instanceof XOS_DisplayObject2D && (d = this.defsLibrary[e], d instanceof XOS_InstanceSymbol2D ? (e = d.clone(), b.addChild(e),
                this.parseGraphicProperties(a, e), e.id = "", a.hasAttribute("id") && (e.id = a.getAttribute("id")), e.localTransform = e.localTransform.appendTranslation(-d.viewBox.position.x, -d.viewBox.position.y)) : (d = d.clone(), this.parseGraphicProperties(a, d), (e = this.checkAndSetClipPath(a, d)) ? b.addChild(e) : b.addChild(d)));
            break;
        case "rect":
            a.hasAttribute("width") && (e = Number(a.getAttribute("width")));
            a.hasAttribute("height") && (f = Number(a.getAttribute("height")));
            a.hasAttribute("rx") && (l = a.getAttribute("rx"));
            a.hasAttribute("ry") &&
                (l = a.getAttribute("ry"));
            d = new XOS_Shape;
            e = XOS_Path.createRectangle(0, 0, e, f, l);
            d.paths.add(e);
            this.parseGraphicProperties(a, d);
            a.hasAttribute("id") && (e = a.getAttribute("id"), this.defsLibrary[e] = d);
            b instanceof XOS_DisplayObject2D ? (e = this.checkAndSetClipPath(a, d)) ? b.addChild(e) : d.strokeFillingType == XOS_DisplayObject2D.NONE_FILLING && d.fillFillingType == XOS_DisplayObject2D.NONE_FILLING || b.addChild(d) : b instanceof ClipPath && (b.shape = d);
            break;
        case "arc":
            break;
        case "ellipse":
        case "circle":
            l = g = 0;
            a.hasAttribute("width") &&
                (e = Number(a.getAttribute("width")));
            a.hasAttribute("height") && (f = Number(a.getAttribute("height")));
            a.hasAttribute("cx") && a.hasAttribute("cy") ? a.hasAttribute("rx") && a.hasAttribute("ry") ? (g = Number(a.getAttribute("cx")) - Number(a.getAttribute("rx")), l = Number(a.getAttribute("cy")) - Number(a.getAttribute("ry")), e = 2 * Number(a.getAttribute("rx")), f = 2 * Number(a.getAttribute("ry"))) : a.hasAttribute("r") && (g = Number(a.getAttribute("cx")) - Number(a.getAttribute("r")), l = Number(a.getAttribute("cy")) - Number(a.getAttribute("r")),
                f = e = 2 * Number(a.getAttribute("r"))) : a.hasAttribute("rx") && a.hasAttribute("ry") ? (g = -Number(a.getAttribute("rx")), l = -Number(a.getAttribute("ry")), e = 2 * Number(a.getAttribute("rx")), f = 2 * Number(a.getAttribute("ry"))) : a.hasAttribute("r") && (g = -Number(a.getAttribute("r")), l = -Number(a.getAttribute("r")), f = e = 2 * Number(a.getAttribute("r")));
            d = new XOS_Shape;
            e = XOS_Path.createEllipse(g, l, e, f);
            d.paths.add(e);
            this.parseGraphicProperties(a, d);
            a.hasAttribute("id") && (e = a.getAttribute("id"), this.defsLibrary[e] = d);
            b instanceof
            XOS_DisplayObject2D ? (e = this.checkAndSetClipPath(a, d)) ? b.addChild(e) : d.strokeFillingType == XOS_DisplayObject2D.NONE_FILLING && d.fillFillingType == XOS_DisplayObject2D.NONE_FILLING || b.addChild(d) : b instanceof ClipPath && (b.shape = d);
            break;
        case "line":
            var q, m, p;
            a.hasAttribute("x1") && (k = Number(a.getAttribute("x1")));
            a.hasAttribute("y1") && (q = Number(a.getAttribute("y1")));
            a.hasAttribute("x2") && (m = Number(a.getAttribute("x2")));
            a.hasAttribute("y2") && (p = Number(a.getAttribute("y2")));
            d = new XOS_Shape;
            e = XOS_Path.createLine(k,
                q, m, p);
            d.paths.add(e);
            this.parseGraphicProperties(a, d);
            b.addChild(d);
            break;
        case "path":
        case "polygon":
        case "polyline":
            a.hasAttribute("d") && (g = this.parsePathPoints(String(a.getAttribute("d"))));
            a.hasAttribute("points") && (g = this.parsePolygonPoints(a.getAttribute("points")));
            d = new XOS_Shape;
            this.parseGraphicProperties(a, d);
            for (e = 0; e < g.length; e++) d.paths.add(g[e]);
            a.hasAttribute("id") && (e = a.getAttribute("id"), this.defsLibrary[e] = d);
            b instanceof XOS_DisplayObject2D ? (e = this.checkAndSetClipPath(a, d)) ? b.addChild(e) :
                (b.addChild(d), this.parseXmlChildren(a, d)) : b instanceof ClipPath && (b.shape = d);
            break;
        case "text":
            "textBox" == a.getAttribute("type") ? (d = new XOS_TextBox, this.parseGraphicProperties(a, d), b.addChild(d), a.hasAttribute("font-size") && (d.fontSize = a.getAttribute("font-size")), a.hasAttribute("text-align") && (d.textAlign = a.getAttribute("text-align")), a.hasAttribute("lineHeight") && (d.lineHeight = Number(a.getAttribute("lineHeight"))), a.hasAttribute("font-weight") && (d.fontWeight = a.getAttribute("font-weight")), a.hasAttribute("font-style") &&
                (d.fontStyle = a.getAttribute("font-style")), a.hasAttribute("font-family") && (d.font = a.getAttribute("font-family"))) : (d = new XOS_Text, this.parseGraphicProperties(a, d), b.addChild(d), a.hasAttribute("font-size") && (d.fontSize = a.getAttribute("font-size")), a.hasAttribute("text-align") && (d.textAlign = a.getAttribute("text-align")), a.hasAttribute("font-style") && (d.fontStyle = a.getAttribute("font-style")), a.hasAttribute("font-weight") && (d.fontWeight = a.getAttribute("font-weight")), a.hasAttribute("font-family") && (d.font =
                a.getAttribute("font-family")), a.hasAttribute("text-anchor") && (e = a.getAttribute("text-anchor"), "start" == e ? d.textAlign = "left" : "middle" == e ? d.textAlign = "center" : "end" == e && (d.textAlign = "right")), a.firstChild && a.firstChild.nodeValue && d.setText(a.firstChild.nodeValue));
            this.parseXmlChildren(a, d);
            break;
        case "tspan":
            b instanceof XOS_TextBox ? a.firstChild ? (d = a.firstChild.nodeValue, "" != b.text && (d = "\n" + d), b.appendText(d)) : b.appendText("\n") : b instanceof XOS_Text && a.firstChild && (d = a.firstChild.nodeValue, a.hasAttribute("x") &&
                "" != b.text && (d = 0 == parseInt(a.getAttribute("x")) ? "\n" + d : " " + d), b.appendText(d));
            break;
        case "style":
            a.firstChild && this.parseCSS(a.firstChild.nodeValue);
            break;
        default:
            this.parseXmlChildren(a, b)
    }
};
_.checkAndSetClipPath = function(a, b) {
    if (a.hasAttribute("clip-path")) {
        var c = a.getAttribute("clip-path");
        if (-1 != c.indexOf("url") && (c = this.getDefObjectFromUrlString(c))) {
            c = c.shape;
            c.isMask = !0;
            if (b instanceof XOS_Group2D) return b.mask = b.addChild(c.clone()), b;
            var d = new XOS_Group2D;
            d.addChild(b);
            d.mask = d.addChild(c.clone());
            return d
        }
    }
    return null
};
_.parseViewBox = function(a) {
    a = a.split(" ");
    var b = {};
    b.position = {
        x: Number(a[0]),
        y: Number(a[1])
    };
    b.size = {
        x: Number(a[2]),
        y: Number(a[3])
    };
    return b
};
_.getDefObjectFromUrlString = function(a) {
    var b = a.indexOf("("),
        c = a.indexOf(")");
    a = a.substring(b + 2, c);
    return this.defsLibrary[a]
};
_.parseGraphicProperties = function(a, b) {
    a.hasAttribute("id") && (b.id = a.getAttribute("id"), this.defsLibrary[b.id] = b);
    a.hasAttribute("placeIntoHtmlLayer") && (b.placeIntoHtmlLayer = a.getAttribute("placeIntoHtmlLayer"));
    a.hasAttribute("name") && (b.name = a.getAttribute("name"));
    a.hasAttribute("display") && "none" == a.getAttribute("display") && (b.visible = !1);
    a.hasAttribute("width") && (b.width = Number(a.getAttribute("width")));
    a.hasAttribute("height") && (b.height = Number(a.getAttribute("height")));
    a.hasAttribute("compositeOperation") &&
        (b.compositeOperation = a.getAttribute("compositeOperation"));
    a.hasAttribute("transform") && (b.localTransform = this.parseTransform(a.getAttribute("transform")));
    a.hasAttribute("x") && a.hasAttribute("y") && (b.localTransform = b.localTransform.appendTranslation(Number(a.getAttribute("x")), Number(a.getAttribute("y"))));
    a.hasAttribute("scaleX") && a.hasAttribute("scaleY") && b.scale(Number(a.getAttribute("scaleX")), Number(a.getAttribute("scaleY")));
    a.hasAttribute("rotation") && b.rotate(Number(a.getAttribute("rotation")));
    a.hasAttribute("stroke-width") && (b.lineWidth = Number(a.getAttribute("stroke-width")));
    a.hasAttribute("stroke-linecap") && (b.lineCap = a.getAttribute("stroke-linecap"));
    a.hasAttribute("stroke-linejoin") && (b.lineJoin = a.getAttribute("stroke-linejoin"));
    a.hasAttribute("stroke-miterlimit") && (b.miterLimit = Number(a.getAttribute("stroke-miterlimit")));
    if (a.hasAttribute("stroke"))
        if (b.strokeColor = a.getAttribute("stroke"), "none" == b.strokeColor) b.strokeFillingType = XOS_DisplayObject2D.NONE_FILLING;
        else if (-1 != b.strokeColor.indexOf("url")) {
        var c =
            this.getDefObjectFromUrlString(b.strokeColor);
        c instanceof XOS_LinearGradient || c instanceof XOS_RadialGradient ? (b.strokeGradient = c.clone(), b.strokeFillingType = XOS_DisplayObject2D.GRADIENT_FILLING, this.calculateRelativeGradientPoints(b, b.strokeGradient)) : c instanceof XOS_Pattern ? (b.strokeFillingType = XOS_DisplayObject2D.PATTERN_FILLING, b.strokePattern = c) : b.fillFillingType = XOS_DisplayObject2D.DIFFUSE_FILLING
    } else b.strokeFillingType = XOS_DisplayObject2D.DIFFUSE_FILLING;
    else b.strokeFillingType = XOS_DisplayObject2D.NONE_FILLING,
        b.strokeColor = "none";
    a.hasAttribute("fill") ? (b.fillColor = a.getAttribute("fill"), "none" == b.fillColor ? b.fillFillingType = XOS_DisplayObject2D.NONE_FILLING : -1 != b.fillColor.indexOf("url") ? (c = this.getDefObjectFromUrlString(b.fillColor), c instanceof XOS_LinearGradient || c instanceof XOS_RadialGradient ? (b.fillFillingType = XOS_DisplayObject2D.GRADIENT_FILLING, b.fillGradient = c, this.calculateRelativeGradientPoints(b, b.fillGradient)) : c instanceof XOS_Pattern ? (b.fillFillingType = XOS_DisplayObject2D.PATTERN_FILLING,
        b.fillPattern = c) : b.fillFillingType = XOS_DisplayObject2D.DIFFUSE_FILLING) : b.fillFillingType = XOS_DisplayObject2D.DIFFUSE_FILLING) : (b.fillColor = "#000", b.fillFillingType = XOS_DisplayObject2D.DIFFUSE_FILLING);
    a.hasAttribute("stroke-alpha") && (b.strokeAlpha = Number(a.getAttribute("stroke-alpha")));
    a.hasAttribute("fill-alpha") && (b.fillAlpha = Number(a.getAttribute("fill-alpha")));
    a.hasAttribute("stroke-opacity") && (b.strokeAlpha = Number(a.getAttribute("stroke-opacity")));
    a.hasAttribute("fill-opacity") && (b.fillAlpha =
        Number(a.getAttribute("fill-opacity")));
    a.hasAttribute("opacity") && (b.opacity = Number(a.getAttribute("opacity")));
    a.hasAttribute("filter") && (c = a.getAttribute("filter"), -1 != c.indexOf("url") && (c = this.getDefObjectFromUrlString(c)) && b.addEffect(c));
    if (a.hasAttribute("class")) {
        b.classNameList = this.extractClassNames(a);
        for (var c = b.classNameList.split(" "), d, e = c.length, f = 0; f < e; f++)(d = this.definedCSSClasses[c[f].trim()]) && this.propertiesToTarget(d, b)
    }
    a.hasAttribute("style") && (d = this.styleToProperties(a.getAttribute("style")),
        this.propertiesToTarget(d, b))
};
_.parseTransform = function(a) {
    var b = new XOS_Matrix2D,
        c = a.indexOf("matrix(");
    if (-1 != c) {
        var c = c + 7,
            d = a.indexOf(")", c);
        a = this.parseValues(a.substring(c, d));
        b.a = Number(a[0]);
        b.b = Number(a[1]);
        b.c = Number(a[2]);
        b.d = Number(a[3]);
        b.e = Number(a[4]);
        b.f = Number(a[5])
    } else c = a.indexOf("translate("), -1 != c && (c += 10, d = a.indexOf(")", c), a = this.parseValues(a.substring(c, d)), b = b.appendTranslation(a[0], a[1]));
    return b
};
_.parseCSS = function(a) {
    var b, c, d;
    for (b = 0; a[b];) {
        switch (a[b]) {
            case ".":
            case "#":
                c = a.indexOf("{", b);
                d = a.indexOf("}", b);
                if (-1 == c || -1 == d) return;
                b = a.substring(b + 1, c).trim();
                this.definedCSSClasses[b] = this.styleToProperties(a.substring(c + 1, d));
                b = d
        }
        b++
    }
};
_.styleToProperties = function(a) {
    var b = {};
    a = a.split(";");
    for (var c, d = a.length, e = 0; e < d; e++) c = a[e].split(":"), 2 == c.length && (b[c[0].trim()] = c[1].trim());
    return b
};
_.propertiesToTarget = function(a, b) {
    a["stroke-width"] && (b.lineWidth = Number(a["stroke-width"]));
    a["stroke-linecap"] && (b.lineCap = a["stroke-linecap"]);
    a["stroke-linejoin"] && (b.lineJoin = a["stroke-linejoin"]);
    a["stroke-miterlimit"] && (b.miterLimit = Number(a["stroke-miterlimit"]));
    if (a.stroke)
        if (b.strokeColor = a.stroke, "none" == b.strokeColor) b.strokeFillingType = XOS_DisplayObject2D.NONE_FILLING;
        else if (-1 != b.strokeColor.indexOf("url")) {
        var c = this.getDefObjectFromUrlString(b.strokeColor);
        c instanceof XOS_LinearGradient ||
            c instanceof XOS_RadialGradient ? (b.strokeGradient = c.clone(), b.strokeFillingType = XOS_DisplayObject2D.GRADIENT_FILLING, this.calculateRelativeGradientPoints(b, b.strokeGradient)) : c instanceof XOS_Pattern && (b.strokeFillingType = XOS_DisplayObject2D.PATTERN_FILLING, b.strokePattern = c)
    } else b.strokeFillingType = XOS_DisplayObject2D.DIFFUSE_FILLING;
    a.fill && (b.fillColor = a.fill, "none" == b.fillColor ? b.fillFillingType = XOS_DisplayObject2D.NONE_FILLING : -1 != b.fillColor.indexOf("url") ? (c = this.getDefObjectFromUrlString(b.fillColor),
        c instanceof XOS_LinearGradient || c instanceof XOS_RadialGradient ? (b.fillFillingType = XOS_DisplayObject2D.GRADIENT_FILLING, b.fillGradient = c, this.calculateRelativeGradientPoints(b, b.fillGradient)) : c instanceof XOS_Pattern && (b.fillFillingType = XOS_DisplayObject2D.PATTERN_FILLING, b.fillPattern = c)) : b.fillFillingType = XOS_DisplayObject2D.DIFFUSE_FILLING);
    a["stroke-alpha"] && (b.strokeAlpha = Number(a["stroke-alpha"]));
    a["fill-alpha"] && (b.fillAlpha = Number(a["fill-alpha"]));
    a["stroke-opacity"] && (b.strokeAlpha = Number(a["stroke-opacity"]));
    a["fill-opacity"] && (b.fillAlpha = Number(a["fill-opacity"]));
    a.opacity && (b.opacity = Number(a.opacity));
    a["font-size"] && (b.fontSize = parseFloat(a["font-size"]));
    a["text-align"] && (b.textAlign = a["text-align"]);
    a["font-style"] && (b.fontStyle = a["font-style"]);
    a["font-weight"] && (b.fontWeight = a["font-weight"]);
    a["font-family"] && (b.font = a["font-family"]);
    a["text-anchor"] && (c = a["text-anchor"], "start" == c ? b.textAlign = "left" : "middle" == c ? b.textAlign = "center" : "end" == c && (b.textAlign = "right"))
};
_.parsePattern = function(a, b) {
    a.hasAttribute("id") && (b.id = a.getAttribute("id"), this.defsLibrary[b.id] = b);
    a.hasAttribute("patternUnits") && (b.patternUnits = a.getAttribute("patternUnits"));
    a.hasAttribute("width") && (b.width = Number(a.getAttribute("width")));
    a.hasAttribute("height") && (b.height = Number(a.getAttribute("height")))
};
_.parseEffect = function(a) {
    var b = null;
    a.hasAttribute("type") && "Shadow" == a.getAttribute("type") && (b = new XOS_Shadow_effect(this.displayRef), a.hasAttribute("shadowOffsetX") && (b.shadowOffsetX = Number(a.getAttribute("shadowOffsetX"))), a.hasAttribute("shadowOffsetY") && (b.shadowOffsetY = Number(a.getAttribute("shadowOffsetY"))), a.hasAttribute("shadowBlur") && (b.shadowBlur = Number(a.getAttribute("shadowBlur"))), a.hasAttribute("shadowColor") && (b.shadowColor = a.getAttribute("shadowColor")), b.id = a.getAttribute("id"),
        this.defsLibrary[b.id] = b);
    return b
};
_.extractClassNames = function(a) {
    var b = "";
    if (a.hasAttribute("class")) {
        a = a.getAttribute("class").split(/\s+/);
        for (var c = "", d = a.length, e = 0; e < d; e++) c = a[e], -1 == c.indexOf("_animation_") && (b += a[e].trim() + " ")
    }
    return b
};
_.parseCssAnimation = function(a) {
    var b = new XOS_CSSAnimation;
    a.hasAttribute("id") && (b.id = a.getAttribute("id"));
    a.hasAttribute("animationName") && (b.animationName = a.getAttribute("animationName"));
    a.hasAttribute("delay") && (b.delay = a.getAttribute("delay"));
    a.hasAttribute("duration") && (b.duration = a.getAttribute("duration"));
    a.hasAttribute("iterationCount") && (b.iterationCount = a.getAttribute("iterationCount"));
    a.hasAttribute("fillMode") && (b.fillMode = a.getAttribute("fillMode"));
    return b
};
_.parseTween = function(a) {
    var b = null;
    "animateTransform" == a.nodeName ? b = new XOS_SVGTweenAnimateTransform(this.displayRef) : "animate" == a.nodeName && (b = new XOS_SVGTweenAnimate(this.displayRef));
    a.hasAttribute("id") && (b.id = a.getAttribute("id"));
    a.hasAttribute("begin") && (b.begin = a.getAttribute("begin"));
    a.hasAttribute("end") && (b.end = a.getAttribute("end"));
    a.hasAttribute("dur") && (b.dur = parseFloat(a.getAttribute("dur")));
    a.hasAttribute("repeatCount") && (b.repeatCount = a.getAttribute("repeatCount"));
    a.hasAttribute("restart") &&
        (b.restart = a.getAttribute("restart"));
    a.hasAttribute("attributeName") && (b.attributeName = a.getAttribute("attributeName"));
    a.hasAttribute("attributeType") && (b.attributeType = a.getAttribute("attributeType"));
    a.hasAttribute("type") && (b.type = a.getAttribute("type"));
    a.hasAttribute("additive") && (b.additive = a.getAttribute("additive"));
    a.hasAttribute("accumulate") && (b.accumulate = a.getAttribute("accumulate"));
    a.hasAttribute("fill") && (b.fill = a.getAttribute("fill"));
    a.hasAttribute("from") && (b.from = a.getAttribute("from"));
    a.hasAttribute("to") && (b.to = a.getAttribute("to"));
    a.hasAttribute("calcMode") && (b.calcMode = a.getAttribute("calcMode"));
    a.hasAttribute("values") && (values = a.getAttribute("values").split(";"), b.from = values[0], b.to = values[1]);
    a.hasAttribute("keySplines") && (keySplines = a.getAttribute("keySplines"), keySplines == XOS_SVGTween.motionTypeList.linear && (b.motionType = "linear"), keySplines == XOS_SVGTween.motionTypeList.easeInOut && (b.motionType = "easeInOut"), keySplines == XOS_SVGTween.motionTypeList.easeIn && (b.motionType =
        "easeIn"), keySplines == XOS_SVGTween.motionTypeList.easeOut && (b.motionType = "easeOut"));
    return b
};
_.calculateRelativeGradientPoints = function(a, b) {
    if (!b.localTransform.isIdentity()) {
        var c = a.localTransform.clone().invert().concat(b.localTransform),
            d = c.transformPoint({
                x: b.x1,
                y: b.y1
            });
        b.x1 = d.x;
        b.y1 = d.y;
        d = c.transformPoint({
            x: b.x2,
            y: b.y2
        });
        b.x2 = d.x;
        b.y2 = d.y
    }
};
_.parseLinearGradient = function(a, b) {
    a.hasAttribute("x1") && (b.x1 = Number(a.getAttribute("x1")));
    a.hasAttribute("y1") && (b.y1 = Number(a.getAttribute("y1")));
    a.hasAttribute("x2") && (b.x2 = Number(a.getAttribute("x2")));
    a.hasAttribute("y2") && (b.y2 = Number(a.getAttribute("y2")));
    a.hasAttribute("gradientUnits") && (b.gradientUnits = a.getAttribute("gradientUnits"));
    a.hasAttribute("id") && (b.id = a.getAttribute("id"), this.defsLibrary[b.id] = b);
    a.hasAttribute("gradientTransform") && (b.localTransform = this.parseTransform(a.getAttribute("gradientTransform")));
    if (a.hasAttribute("xlink:href")) {
        var c = a.getAttribute("xlink:href").substr(1);
        if (c = this.defsLibrary[c]) b.colors = c.colors
    }
};
_.parseRadialGradient = function(a, b) {
    a.hasAttribute("id") && (b.id = a.getAttribute("id"), this.defsLibrary[b.id] = b);
    a.hasAttribute("x1") && (b.x1 = Number(a.getAttribute("x1")));
    a.hasAttribute("y1") && (b.y1 = Number(a.getAttribute("y1")));
    a.hasAttribute("r1") && (b.r1 = Number(a.getAttribute("r1")));
    a.hasAttribute("x2") && (b.x2 = Number(a.getAttribute("x2")));
    a.hasAttribute("y2") && (b.y2 = Number(a.getAttribute("y2")));
    a.hasAttribute("r2") && (b.r2 = Number(a.getAttribute("r2")));
    a.hasAttribute("cx") && (b.x2 = b.x1 = Number(a.getAttribute("cx")));
    a.hasAttribute("cy") && (b.y2 = b.y1 = Number(a.getAttribute("cy")));
    a.hasAttribute("r") && (b.r1 = 0, b.r2 = Number(a.getAttribute("r")));
    a.hasAttribute("gradientUnits") && (b.gradientUnits = a.getAttribute("gradientUnits"));
    a.hasAttribute("gradientTransform") && (b.localTransform = this.parseTransform(a.getAttribute("gradientTransform")));
    if (a.hasAttribute("xlink:href")) {
        var c = a.getAttribute("xlink:href").substr(1);
        if (c = this.defsLibrary[c]) b.colors = c.colors
    }
};
_.parseValues = function(a) {
    return (a = a.match(/-?[.0-9]+(?:e[-+]?\d+)?/ig)) ? a.map(Number) : []
};
_.scaleValues = function(a, b) {
    for (var c = a.length, d = 0; d < c; d++) a[d] /= b;
    return a
};
_.parsePolygonPoints = function(a) {
    var b = new XOS_Path,
        c = [];
    c.push(b);
    a = this.parseValues(a);
    b.addPoint(new XOS_MoveTo(a[0], a[1]));
    for (var d = a.length, e = 2; e < d; e += 2) b.addPoint(new XOS_LineTo(a[e], a[e + 1]));
    b.closePath = !0;
    return c
};
_.parsePathPoints = function(a) {
    function b(b) {
        for (; a[b];) {
            switch (a[b]) {
                case "M":
                case "m":
                case "L":
                case "l":
                case "V":
                case "H":
                case "h":
                case "V":
                case "v":
                case "A":
                case "a":
                case "Q":
                case "q":
                case "C":
                case "c":
                case "S":
                case "s":
                case "Z":
                case "z":
                    return b
            }
            b++
        }
        return a.length
    }
    for (var c = [], d = null, e = null, f = null, g = 0; a[g];) switch (a[g]) {
        case "M":
            g++;
            d = new XOS_Path;
            c.push(d);
            e = g;
            g = b(g);
            e = this.parseValues(a.substring(e, g));
            f = new XOS_MoveTo(e[0], e[1]);
            d.addPoint(f);
            break;
        case "m":
            g++;
            e = g;
            g = b(g);
            e = this.parseValues(a.substring(e,
                g));
            d = new XOS_Path;
            c.push(d);
            f = f ? new XOS_MoveTo(f.x + e[0], f.y + e[1]) : new XOS_MoveTo(e[0], e[1]);
            d.addPoint(f);
            for (var h = e.length, k = 2; k < h; k += 2) f = new XOS_LineTo(f.x + e[k], f.y + e[k + 1]), d.addPoint(f);
            break;
        case "L":
            g++;
            e = g;
            g = b(g);
            e = this.parseValues(a.substring(e, g));
            h = e.length;
            for (k = 0; k < h; k += 2) f = new XOS_LineTo(e[k], e[k + 1]), d.addPoint(f);
            break;
        case "l":
            g++;
            e = g;
            g = b(g);
            e = this.parseValues(a.substring(e, g));
            h = e.length;
            for (k = 0; k < h; k += 2) f = new XOS_LineTo(f.x + e[k], f.y + e[k + 1]), d.addPoint(f);
            break;
        case "V":
            g++;
            e = g;
            g =
                b(g);
            e = this.parseValues(a.substring(e, g));
            f = new XOS_LineTo(f.x, e[0]);
            d.addPoint(f);
            break;
        case "v":
            g++;
            e = g;
            g = b(g);
            e = this.parseValues(a.substring(e, g));
            f = new XOS_LineTo(f.x, f.y + e[0]);
            d.addPoint(f);
            break;
        case "H":
            g++;
            e = g;
            g = b(g);
            e = this.parseValues(a.substring(e, g));
            f = new XOS_LineTo(e[0], f.y);
            d.addPoint(f);
            break;
        case "h":
            g++;
            e = g;
            g = b(g);
            e = this.parseValues(a.substring(e, g));
            f = new XOS_LineTo(f.x + e[0], f.y);
            d.addPoint(f);
            break;
        case "A":
            g++;
            e = g;
            g = b(g);
            e = this.parseValues(a.substring(e, g));
            h = e[5];
            e = e[6];
            f = new XOS_LineTo(h,
                e);
            d.addPoint(f);
            break;
        case "a":
            g++;
            e = g;
            g = b(g);
            e = this.parseValues(a.substring(e, g));
            h = e[5];
            e = e[6];
            f = new XOS_LineTo(f.x + h, f.y + e);
            d.addPoint(f);
            break;
        case "Q":
            g++;
            e = g;
            g = b(g);
            e = this.parseValues(a.substring(e, g));
            h = e.length;
            for (k = 0; k < h; k += 4) {
                var l = new XOS_QuadraticCurveTo(0, 0, 0, 0);
                l.controlPt.x = e[k];
                l.controlPt.y = e[k + 1];
                l.x = e[k + 2];
                l.y = e[k + 3];
                d.addPoint(l);
                f = l
            }
            break;
        case "q":
            g++;
            e = g;
            g = b(g);
            e = this.parseValues(a.substring(e, g));
            h = e.length;
            for (k = 0; k < h; k += 4) l = new XOS_QuadraticCurveTo(0, 0, 0, 0), l.controlPt.x =
                f.x + e[k], l.controlPt.y = f.y + e[k + 1], l.x = f.x + e[k + 2], l.y = f.y + e[k + 3], d.addPoint(l), f = l;
            break;
        case "C":
            g++;
            e = g;
            g = b(g);
            e = this.parseValues(a.substring(e, g));
            h = e.length;
            for (k = 0; k < h; k += 6) l = new XOS_CubicCurveTo(0, 0, 0, 0, 0, 0), l.controlPt1.x = e[k], l.controlPt1.y = e[k + 1], l.controlPt2.x = e[k + 2], l.controlPt2.y = e[k + 3], l.x = e[k + 4], l.y = e[k + 5], d.addPoint(l), f = l;
            break;
        case "c":
            g++;
            e = g;
            g = b(g);
            e = this.parseValues(a.substring(e, g));
            h = e.length;
            for (k = 0; k < h; k += 6) l = new XOS_CubicCurveTo(0, 0, 0, 0, 0, 0), l.controlPt1.x = f.x + e[k], l.controlPt1.y =
                f.y + e[k + 1], l.controlPt2.x = f.x + e[k + 2], l.controlPt2.y = f.y + e[k + 3], l.x = f.x + e[k + 4], l.y = f.y + e[k + 5], d.addPoint(l), f = l;
            break;
        case "S":
            g++;
            e = g;
            g = b(g);
            e = this.parseValues(a.substring(e, g));
            h = e.length;
            for (k = 0; k < h; k += 4) l = new XOS_CubicCurveTo(0, 0, 0, 0, 0, 0), l.controlPt1.x = f.x + (f.x - f.controlPt2.x), l.controlPt1.y = f.y + (f.y - f.controlPt2.y), l.controlPt2.x = e[k], l.controlPt2.y = e[k + 1], l.x = e[k + 2], l.y = e[k + 3], d.addPoint(l), f = l;
            break;
        case "s":
            g++;
            e = g;
            g = b(g);
            e = this.parseValues(a.substring(e, g));
            h = e.length;
            for (k = 0; k < h; k += 4) l =
                new XOS_CubicCurveTo(0, 0, 0, 0, 0, 0), f.controlPt2 ? (l.controlPt1.x = f.x + (f.x - f.controlPt2.x), l.controlPt1.y = f.y + (f.y - f.controlPt2.y)) : (l.controlPt1.x = f.x, l.controlPt1.y = f.y), l.controlPt2.x = f.x + e[k], l.controlPt2.y = f.y + e[k + 1], l.x = f.x + e[k + 2], l.y = f.y + e[k + 3], d.addPoint(l), f = l;
            break;
        case "Z":
        case "z":
            d.closePath = !0;
            g++;
            break;
        default:
            g++
    }
    return c
};

function DefsLibrary() {}

function ClipPath() {
    this.path = null
};

function XOS_DisplayObject2DSerializer(a) {
    this.displayRef = a;
    this.xmlDoc = null;
    this.symbols = {};
    this.styleNode = this.defsNode = null;
    this.javascriptCodeAtEndPage = "";
    this.parser = null;
    this.outputCSS = !0;
    this.sharedProjectsLibs = {};
    this.sharedProjectsLibs.animateCSS = "https://googledrive.com/host/0BwRlR3z6e0egZWdfSmJqSGZKZnM/animate.min.css?note=please+download+this+file+in+your+local+project";
    this.sharedProjectsLibs.includeSVG = "https://googledrive.com/host/0BwRlR3z6e0egTEQwX09iNXlJd1U/includeSVG.js?note=please+download+this+file+in+your+local+project";
    this.fontFamilies = {};
    this.ID_COUNTER = -1;
    this.timestamp = (new Date).getTime()
}
_ = XOS_DisplayObject2DSerializer.prototype;
_.generateID = function() {
    this.ID_COUNTER++;
    return "_" + this.timestamp + "_" + this.ID_COUNTER + "_"
};
_.unescapeHTML = function(a) {
    return a.replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&amp;/g, "&")
};
_.createNodes_DEFS_CSS = function() {
    this.defsNode = this.xmlDoc.createElement("defs");
    this.xmlDoc.documentElement.appendChild(this.defsNode);
    this.styleNode_animations = this.xmlDoc.createElement("style");
    this.styleNode_animations.setAttribute("type", "text/css");
    this.xmlDoc.documentElement.appendChild(this.styleNode_animations);
    this.styleNode = this.xmlDoc.createElement("style");
    this.styleNode.setAttribute("type", "text/css");
    this.xmlDoc.documentElement.appendChild(this.styleNode)
};
_.serialize = function(a) {
    this.createXmlDocument("page");
    this.writeTitleAndDescription(this.xmlDoc.documentElement, this.displayRef);
    this.createNodes_DEFS_CSS();
    this.writePageClassStyle();
    this.writeIncludes_JS(this.xmlDoc.documentElement);
    this.writeChildren(this.xmlDoc.documentElement, this.displayRef.content);
    this.writeIncludes_CSS(this.defsNode);
    return this.unescapeHTML((new XMLSerializer).serializeToString(this.xmlDoc))
};
_.serializeSelection = function() {
    this.createXmlDocument("page");
    this.createNodes_DEFS_CSS();
    for (var a = this.displayRef.content.children, b = a.length, c, d = 1; d < b; d++) {
        c = a[d];
        var e = c.getSelectedChildren(),
            f = e.length;
        if (0 < f)
            for (var g = c.globalTransform.clone().invert(), h = this.writeLayer(this.xmlDoc.documentElement, c), k = 0; k < f; k++) c = e[k].localTransform.clone(), e[k].localTransform = g.concat(e[k].globalTransform), this.write(h, e[k]), e[k].localTransform = c
    }
    return this.unescapeHTML((new XMLSerializer).serializeToString(this.xmlDoc))
};
_.serializeHTML = function() {
    this.createHtmlDocument();
    this.createXmlDocument("");
    this.writeTitleAndDescription(this.xmlDoc.documentElement, this.displayRef);
    this.defsNode = this.xmlDoc.createElement("defs");
    this.xmlDoc.documentElement.appendChild(this.defsNode);
    this.styleNode = this.htmlDocument.createElement("style");
    this.styleNode.setAttribute("type", "text/css");
    this.styleNode.appendChild(this.htmlDocument.createTextNode(""));
    this.writePageClassStyle();
    this.styleNode_animations = this.htmlDocument.createElement("style");
    this.styleNode_animations.setAttribute("type", "text/css");
    this.writeChildren(this.xmlDoc.documentElement, this.displayRef.content);
    this.htmlHead.appendChild(this.styleNode);
    this.writeIncludes_CSS(this.defsNode);
    "" != this.styleNode_animations.textContent && this.htmlHead.appendChild(this.styleNode_animations);
    this.writeIncludes_JS(this.xmlDoc.documentElement);
    this.htmlPage_svg_layer.appendChild(this.xmlDoc.documentElement);
    return this.unescapeHTML((new XMLSerializer).serializeToString(this.htmlDocument))
};
_.createHtmlDocument = function() {
    this.htmlDocument = (new DOMParser).parseFromString("<!DOCTYPE html><html></html>", "text/xml");
    this.htmlHead = this.htmlDocument.createElement("head");
    var a = this.htmlDocument.createElement("meta");
    a.setAttribute("http-equiv", "Content-Type");
    a.setAttribute("content", "text/html; charset=UTF-8");
    this.htmlDocument.documentElement.appendChild(a);
    this.htmlHead.appendChild(a);
    this.writeMetaTags_HTML();
    this.htmlDocument.documentElement.appendChild(this.htmlHead);
    this.htmlBody =
        this.htmlDocument.createElement("body");
    this.htmlDocument.documentElement.appendChild(this.htmlBody);
    this.htmlPage_container = this.htmlDocument.createElement("div");
    this.htmlPage_container.setAttribute("class", "page");
    this.htmlBody.appendChild(this.htmlPage_container);
    this.htmlPage_svg_layer = this.htmlDocument.createElement("div");
    this.htmlPage_svg_layer.setAttribute("class", "svg-layer");
    this.htmlPage_container.appendChild(this.htmlPage_svg_layer);
    this.htmlPage_html_layer = this.htmlDocument.createElement("div");
    this.htmlPage_html_layer.setAttribute("class", "html-layer");
    this.htmlPage_container.appendChild(this.htmlPage_html_layer);
    var a = this.htmlDocument.createElement("title"),
        b = this.htmlDocument.createTextNode(this.displayRef.title);
    a.appendChild(b);
    this.htmlHead.appendChild(a);
    a = this.htmlDocument.createElement("meta");
    a.setAttribute("name", "description");
    a.setAttribute("content", this.displayRef.description);
    this.htmlHead.appendChild(a)
};
_.createXmlDocument = function(a) {
    this.parser = new DOMParser;
    var b = this.displayRef.getPageAlignment(),
        c = this.displayRef.getPageSize(),
        d = "",
        d = "no-margins" == b ? 'pageAlignment="' + this.displayRef.getPageAlignment() + '" x="0px" y="0px" width="' + c.x + 'px" height="' + c.y + 'px" enable-background="new 0 0 ' + c.x + " " + c.y + '" xml:space="preserve" ' : 'pageAlignment="' + this.displayRef.getPageAlignment() + '" x="0px" y="0px" width="' + c.x + 'px" height="' + c.y + 'px" viewBox="0 0 ' + c.x + " " + c.y + '" enable-background="new 0 0 ' + c.x +
        " " + c.y + '" xml:space="preserve" ';
    "rgba(255,255,255,1)" != this.displayRef.bodyBackgroundColor && "rgb(255, 255, 255)" != this.displayRef.bodyBackgroundColor && (d += ' bodyBackgroundColor="' + this.displayRef.bodyBackgroundColor + '" ');
    this.xmlDoc = this.parser.parseFromString('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="' + a + '" ' + d + " ></svg>", "text/xml")
};
_.writePageClassStyle = function() {
    this.htmlPage_html_layer ? this.writePageClassStyleHTML() : "rgba(255,255,255,1)" != this.displayRef.bodyBackgroundColor && "rgb(255, 255, 255)" != this.displayRef.bodyBackgroundColor && this.styleNode.appendChild(this.xmlDocument.createTextNode("svg{ background-color:" + this.displayRef.bodyBackgroundColor + ";}\n"))
};
_.writePageClassStyleHTML = function() {
    var a = this.displayRef.getPageSize(),
        b = this.displayRef.getPageAlignment(),
        c = "body{ margin:0; background-color:" + this.displayRef.bodyBackgroundColor + ";}\n";
    "center-horizontally" == b || "horizontal" == b ? c += ".page{ position:absolute; display:block; left:50%; top:0; margin-left:-" + a.x / 2 + "px; width:" + a.x + "px; }\n" : "center-horizontally-vertically" == b || "horizontal-vertical" == b ? c += ".page{ position:absolute; display:block; left:50%; top:50%; margin-left:-" + a.x / 2 + "px; margin-top:-" +
        a.y / 2 + "px; width:" + a.x + "px; height:" + a.y + "px; }\n" : "no-margins" == b ? (c += ".page{ position:absolute; display:block; width:100%; height:100%; overflow:hidden; }\n.page>div{ position:absolute; left:0; top:0; width:100%; height:100%; }\n", c += ".page>div>svg{ width:100%; height:100%; }\n") : c += ".page{ position:relative; }\n";
    this.styleNode.appendChild(this.htmlDocument.createTextNode(c))
};
_.writeIncludes_JS = function(a) {
    if (this.htmlPage_html_layer) this.writeIncludes_JS_HTML(a);
    else
        for (var b = "", c = this.displayRef.jsIncludes.length, d = 0; d < c; d++)
            if (b = this.displayRef.jsIncludes[d].trim(), 0 < b.length) {
                var e = this.xmlDoc.createElement("script");
                e.appendChild(this.xmlDoc.createTextNode(""));
                e.setAttribute("xlink:href", b);
                e.setAttribute("type", "text/ecmascript");
                a.appendChild(e)
            }
};
_.writeIncludes_JS_HTML = function(a) {
    a = "";
    for (var b = this.displayRef.jsIncludes.length, c = 0; c < b; c++)
        if (a = this.displayRef.jsIncludes[c].trim(), 0 < a.length) {
            var d = this.htmlDocument.createElement("script");
            d.appendChild(this.htmlDocument.createTextNode(""));
            d.setAttribute("src", a);
            d.setAttribute("type", "text/javascript");
            this.htmlHead.appendChild(d)
        }
};
_.writeIncludes_CSS = function(a) {
    if (this.htmlPage_html_layer) this.writeIncludes_CSS_HTML(a);
    else
        for (var b = "", c = this.displayRef.cssIncludes.length, d = 0; d < c; d++)
            if (b = this.displayRef.cssIncludes[d].trim(), 0 < b.length) {
                var e = this.xmlDoc.createElement("link");
                e.setAttribute("xmlns", "http://www.w3.org/1999/xhtml");
                e.setAttribute("rel", "stylesheet");
                e.setAttribute("href", b);
                a.appendChild(e)
            }
};
_.writeIncludes_CSS_HTML = function(a) {
    a = "";
    for (var b = this.displayRef.cssIncludes.length, c = 0; c < b; c++)
        if (a = this.displayRef.cssIncludes[c].trim(), 0 < a.length) {
            var d = this.htmlDocument.createElement("link");
            d.setAttribute("rel", "stylesheet");
            d.setAttribute("type", "text/css");
            d.setAttribute("href", a);
            this.htmlHead.appendChild(d)
        }
};
_.writeMetaTags_HTML = function() {
    for (var a = this.displayRef.metaTags.length, b = 0; b < a; b++) this.htmlHead.appendChild(this.displayRef.metaTags[b].cloneNode(!0))
};
_.writeChildren = function(a, b) {
    for (var c = b.children.length, d = 0; d < c; d++) this.write(a, b.children[d])
};
_.write = function(a, b) {
    if ("no" != b.placeIntoHtmlLayer && this.htmlPage_html_layer) {
        var c = this.xmlDoc.createElement("g");
        c.setAttribute("data-svg-idi", b.idi);
        this.writeTransform(c, b);
        a.appendChild(c);
        this.writeGraphicObjectIntoHtmlLayer(a, b)
    } else switch (b.type) {
        case "LAYER":
            c = this.writeLayer(a, b);
            this.writeChildren(c, b);
            break;
        case "GROUP":
            null != b.anchorLink && "" != b.anchorLink && (a = this.writeAnchorLink(a, b));
            this.writeGraphicEffects(a, b);
            c = this.writeGroup(a, b);
            this.writeTitleAndDescription(c, b);
            this.writeAppliedGraphicEffectList(c,
                b);
            this.writeAppliedTweenList(c, b);
            this.writeAppliedAnimationList(c, b);
            this.writeChildren(c, b);
            break;
        case "SYMBOL":
            null != b.anchorLink && "" != b.anchorLink && (a = this.writeAnchorLink(a, b));
            this.writeGraphicEffects(a, b);
            c = this.writeUseSymbol(a, b);
            this.writeTitleAndDescription(c, b);
            this.writeAppliedGraphicEffectList(c, b);
            this.writeAppliedTweenList(c, b);
            this.writeAppliedAnimationList(c, b);
            this.writeSymbolDefinition(b.idSymbolRef);
            break;
        case "INCLUDE":
            null != b.anchorLink && "" != b.anchorLink && (a = this.writeAnchorLink(a,
                b));
            this.writeGraphicEffects(a, b);
            this.writeIncludeContent(a, b);
            break;
        case "SHAPE":
            null != b.anchorLink && "" != b.anchorLink && (a = this.writeAnchorLink(a, b));
            b.isMask ? this.writeClipPath(a, b) : (this.writeGraphicFillingType(a, b), this.writeGraphicEffects(a, b), c = this.writeShape(a, b), this.writeTitleAndDescription(c, b), this.writeAppliedGraphicEffectList(c, b), this.writeAppliedTweenList(c, b), this.writeAppliedAnimationList(c, b));
            break;
        case "TEXT":
            null != b.anchorLink && "" != b.anchorLink && (a = this.writeAnchorLink(a, b));
            this.writeGraphicFillingType(a, b);
            this.writeGraphicEffects(a, b);
            c = this.writeText(a, b);
            this.writeTitleAndDescription(c, b);
            this.writeAppliedGraphicEffectList(c, b);
            this.writeAppliedTweenList(c, b);
            this.writeAppliedAnimationList(c, b);
            break;
        case "TEXTBOX":
            null != b.anchorLink && "" != b.anchorLink && (a = this.writeAnchorLink(a, b));
            this.writeGraphicFillingType(a, b);
            this.writeGraphicEffects(a, b);
            c = this.writeTextBox(a, b);
            this.writeTitleAndDescription(c, b);
            this.writeAppliedGraphicEffectList(c, b);
            this.writeAppliedTweenList(c,
                b);
            this.writeAppliedAnimationList(c, b);
            break;
        case "IMAGE":
            null != b.anchorLink && "" != b.anchorLink && (a = this.writeAnchorLink(a, b)), this.writeGraphicEffects(a, b), c = this.writeImage(a, b), this.writeTitleAndDescription(c, b), this.writeAppliedGraphicEffectList(c, b), this.writeAppliedTweenList(c, b), this.writeAppliedAnimationList(c, b)
    }
};
_.writeTitleAndDescription = function(a, b) {
    var c, d;
    "" != b.title && (c = this.xmlDoc.createElement("title"), d = this.xmlDoc.createTextNode(b.title), c.appendChild(d), a.appendChild(c));
    "" != b.description && (c = this.xmlDoc.createElement("desc"), d = this.xmlDoc.createTextNode(b.description), c.appendChild(d), a.appendChild(c))
};
_.writeClassAndID = function(a, b) {
    "ignore" != b.classNameList && ("" != b.classNameList && a.setAttribute("class", b.classNameList), "" != b.id && a.setAttribute("id", b.id))
};
_.writeAnchorLink = function(a, b) {
    var c = this.xmlDoc.createElement("a");
    c.setAttribute("xlink:href", b.anchorLink);
    "" != b.anchorLinkTarget && c.setAttribute("target", b.anchorLinkTarget);
    a.appendChild(c);
    return c
};
_.writeLayer = function(a, b) {
    var c = this.writeGroup(a, b);
    c.setAttribute("type", "LAYER");
    c.setAttribute("name", b.name);
    c.setAttribute("id", b.name);
    b.isLocked && c.setAttribute("locked", "true");
    !1 == b.visible && c.setAttribute("display", "none");
    return c
};
_.writeClipPath = function(a, b) {
    var c = this.xmlDoc.createElement("clipPath");
    c.setAttribute("id", b.id);
    var d = this.writeShape(c, b);
    d.removeAttribute("id");
    c.appendChild(d);
    this.defsNode.appendChild(c)
};
_.writePattern = function(a, b) {
    var c = this.xmlDoc.createElement("pattern");
    b.id = this.generateID();
    c.setAttribute("id", b.id);
    c.setAttribute("patternUnits", "userSpaceOnUse");
    0 != b.width && c.setAttribute("width", b.width);
    0 != b.height && c.setAttribute("height", b.height);
    var d = b.image,
        e = this.xmlDoc.createElement("image");
    e.setAttribute("xlink:href", d.src);
    e.setAttribute("x", "0");
    e.setAttribute("y", "0");
    0 != d.width && e.setAttribute("width", d.width);
    0 != d.height && e.setAttribute("height", d.height);
    c.appendChild(e);
    this.defsNode.appendChild(c)
};
_.writeGradient = function(a, b) {
    var c;
    b instanceof XOS_LinearGradient ? (c = this.xmlDoc.createElement("linearGradient"), c.setAttribute("x1", b.x1), c.setAttribute("y1", b.y1), c.setAttribute("x2", b.x2), c.setAttribute("y2", b.y2)) : b instanceof XOS_RadialGradient && (c = this.xmlDoc.createElement("radialGradient"), c.setAttribute("cx", b.x1), c.setAttribute("cy", b.y1), c.setAttribute("r", b.r2));
    b.id = this.generateID();
    c.setAttribute("id", b.id);
    c.setAttribute("gradientUnits", "userSpaceOnUse");
    c.setAttribute("gradientTransform",
        "matrix(1 0 0 1 0 0)");
    for (var d, e, f = b.colors.length, g = 0; g < f; g++) d = this.xmlDoc.createElement("stop"), d.setAttribute("offset", b.colors[g].offset), e = XOS_ColorUtils.htmlColorStringToRGBA(b.colors[g].color), 1 > e.a ? d.setAttribute("style", "stop-color:" + b.colors[g].color + "; stop-opacity:" + e.a + ";") : d.setAttribute("style", "stop-color:" + b.colors[g].color), c.appendChild(d);
    this.defsNode.appendChild(c);
    return c
};
_.writeIncludeContent = function(a, b) {
    var c = this.writeImage(a, b);
    "" == b.url && c.setAttribute("visibility", "collapse");
    c.setAttribute("data-svg-idi", b.idi);
    "" != b.includeUrl && c.setAttribute("include-url", b.includeUrl);
    c.setAttribute("include-mode", b.includeMode);
    "" != b.editingUrl && c.setAttribute("editing-url", b.editingUrl);
    "no" != b.scrolling && c.setAttribute("scrolling", b.scrolling);
    var d = b.getPositionInContent();
    d.x = Math.round(d.x);
    d.y = Math.round(d.y);
    var e = b.getSize();
    e.x = Math.round(e.x);
    e.y = Math.round(e.y);
    "foreignObject" == b.includeMode ? (c = document.createElementNS("http://www.w3.org/2000/svg", "foreignObject"), c.setAttribute("x", d.x), c.setAttribute("y", d.y), c.setAttribute("width", e.x), c.setAttribute("height", e.y), d = document.createElement("iframe"), d.setAttribute("xmlns", "http://www.w3.org/1999/xhtml"), d.setAttribute("width", e.x), d.setAttribute("height", e.y), d.setAttribute("src", b.includeUrl), d.setAttribute("frameborder", "0"), d.setAttribute("allowfullscreen", "true"), d.setAttribute("scrolling", b.scrolling),
        c.appendChild(d), a.appendChild(c)) : "includeSVG-js" != b.includeMode && ("includeSVG-php" == b.includeMode ? (c = this.xmlDoc.createElement("g"), this.writeClassAndID(c, b), c.appendChild(this.xmlDoc.createTextNode("<?php include '" + b.includeUrl + "';  ?>")), c.setAttribute("type", "IGNORE"), this.writeTransform(c, b), this.writeClassAndID(c, b), a.appendChild(c)) : "includeHTML-js" != b.includeMode && ("includeHTML-php" == b.includeMode ? this.htmlPage_html_layer && (c = "position:absolute; display:block; left:" + d.x + "px; top:" + d.y +
        "px;  width:" + e.x + "px; height:" + e.y + "px;", e = document.createElement("div"), this.writeClassAndID(e, b), e.setAttribute("style", c), e.appendChild(document.createTextNode("\x3c!--startCDATA--\x3e<?php include '" + b.includeUrl + "';  ?>\x3c!--endCDATA--\x3e")), this.htmlPage_html_layer.appendChild(e)) : "iframe" == b.includeMode ? this.htmlPage_html_layer && (c = "position:absolute; display:block; left:" + d.x + "px; top:" + d.y + "px;  width:" + e.x + "px; height:" + e.y + "px;", d = document.createElement("iframe"), this.writeClassAndID(d,
        b), d.setAttribute("xmlns", "http://www.w3.org/1999/xhtml"), d.setAttribute("width", e.x), d.setAttribute("height", e.y), d.setAttribute("style", c), d.setAttribute("src", b.includeUrl), d.setAttribute("frameborder", "0"), d.setAttribute("allowfullscreen", "true"), d.setAttribute("scrolling", b.scrolling), this.htmlPage_html_layer.appendChild(d)) : "inlineHTML" == b.includeMode && (this.htmlPage_html_layer ? (c = "position:absolute; display:block; left:" + d.x + "px; top:" + d.y + "px;  width:" + e.x + "px; height:" + e.y + "px;", e = document.createElement("div"),
        this.writeClassAndID(e, b), e.setAttribute("style", c), e.setAttribute("data-inline-content", "true"), e.setAttribute("data-svg-idi", b.idi), e.appendChild(document.createTextNode("\x3c!--startCDATA--\x3e" + b.inlineContent + "\x3c!--endCDATA--\x3e")), this.htmlPage_html_layer.appendChild(e)) : c.appendChild(document.createTextNode("\x3c!--startCDATA--\x3e" + b.inlineContent + "\x3c!--endCDATA--\x3e")))))
};
_.writeGraphicObjectIntoHtmlLayer = function(a, b) {
    var c = b.displayRef.content.localTransform.clone();
    b.displayRef.content.localTransform.identity();
    b.displayRef.content.updateGlobalTransform();
    var d = b.getGlobalBounds(),
        e, f;
    e = d.getWidth();
    f = d.getHeight();
    e = Math.round(e + 20);
    f = Math.round(f + 20);
    var g, h;
    g = Math.round(d.min.x - 10);
    h = Math.round(d.min.y - 10);
    b.displayRef.content.localTransform = c;
    b.displayRef.content.updateGlobalTransform();
    g = "position:absolute; display:block; left:" + g + "px; top:" + h + "px;  width:" +
        e + "px; height:" + f + "px;";
    c = document.createElement("div");
    c.setAttribute("data-svg-idi", b.idi);
    c.setAttribute("style", g);
    g = document.createElement("svg");
    g.setAttribute("version", "1.1");
    g.setAttribute("xmlns", "http://www.w3.org/2000/svg");
    g.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
    g.setAttribute("xml:space", "preserve");
    g.setAttribute("width", e);
    g.setAttribute("height", f);
    g.setAttribute("viewBox", "-10 -10 " + e + " " + f);
    e = b.localTransform.clone();
    f = b.getPosition();
    b.setPosition(f.x - d.min.x,
        f.y - d.min.y);
    d = b.classNameList;
    f = b.placeIntoHtmlLayer;
    b.placeIntoHtmlLayer = "no";
    c.setAttribute("class", d);
    "" != b.id && c.setAttribute("id", b.id);
    0 < b.animationList.length && this.writeCSSAnimationsSequence(c, b);
    b.classNameList = "ignore";
    this.write(g, b);
    b.placeIntoHtmlLayer = f;
    b.localTransform = e;
    b.classNameList = d;
    c.appendChild(g);
    this.htmlPage_html_layer.appendChild(c)
};
_.writeImage = function(a, b) {
    var c = this.xmlDoc.createElement("image");
    this.writeTransform(c, b);
    1 != b.fillAlpha && c.setAttribute("opacity", b.fillAlpha);
    c.setAttribute("xlink:href", b.url);
    "" != b.name && c.setAttribute("name", b.name);
    this.writeClassAndID(c, b);
    0 != b.width && c.setAttribute("width", b.width);
    0 != b.height && c.setAttribute("height", b.height);
    1 != b.opacity && c.setAttribute("opacity", b.opacity);
    a.appendChild(c);
    return c
};
_.writeGroup = function(a, b) {
    var c = this.xmlDoc.createElement("g");
    this.writeTransform(c, b);
    1 != b.opacity && c.setAttribute("opacity", b.opacity);
    this.writeClassAndID(c, b);
    b.mask && (b.mask.id = this.generateID(), c.setAttribute("clip-path", "url(#" + b.mask.id + ")"));
    "no" != b.placeIntoHtmlLayer && c.setAttribute("placeIntoHtmlLayer", "yes");
    a.appendChild(c);
    return c
};
_.writeSymbolDefinition = function(a) {
    var b = this.displayRef.getSymbolFromLibrary(a);
    if (!this.symbols[a]) {
        this.symbols[a] = b;
        var c = this.xmlDoc.createElement("symbol");
        c.setAttribute("id", a);
        a = b.getLocalBounds();
        c.setAttribute("viewBox", a.min.x + " " + a.min.y + " " + a.getWidth() + " " + a.getHeight());
        this.defsNode.appendChild(c);
        this.writeChildren(c, b)
    }
};
_.writeUseSymbol = function(a, b) {
    var c = this.xmlDoc.createElement("use");
    this.writeTransform(c, b);
    1 != b.opacity && c.setAttribute("opacity", b.opacity);
    this.writeClassAndID(c, b);
    b.mask && (b.mask.id = this.generateID(), c.setAttribute("clip-path", "url(#" + b.mask.id + ")"));
    c.setAttribute("xlink:href", "#" + b.idSymbolRef);
    c.setAttribute("overflow", "visible");
    var d = this.displayRef.getSymbolFromLibrary(b.idSymbolRef).getLocalBounds();
    c.setAttribute("x", d.min.x);
    c.setAttribute("y", d.min.y);
    c.setAttribute("width", d.getWidth());
    c.setAttribute("height", d.getHeight());
    a.appendChild(c);
    return c
};
_.writeText = function(a, b) {
    var c = this.xmlDoc.createElement("text");
    "" != b.name && c.setAttribute("name", b.name);
    this.writeClassAndID(c, b);
    this.writeTransform(c, b);
    this.writeGraphicProperties(c, b);
    this.writeCSSFontFamily(b.font);
    c.setAttribute("font-family", b.font);
    "normal" != b.fontStyle && c.setAttribute("font-style", b.fontStyle);
    "normal" != b.fontWeight && c.setAttribute("font-weight", b.fontWeight);
    c.setAttribute("font-size", b.fontSize);
    c.setAttribute("text-align", b.textAlign);
    var d;
    "left " == b.textAlign ? d =
        "start" : "center" == b.textAlign ? d = "middle" : "right" == b.textAlign && (d = "end");
    c.setAttribute("text-anchor", d);
    for (var e, f = b.textLines, g = f.length, h = b.fontMetrics.lineHeight + b.linesOffsetY, k = 0; k < g; k++) d = this.xmlDoc.createElement("tspan"), d.setAttribute("x", b.linesOffsetX), d.setAttribute("y", h - b.fontMetrics.descent), e = this.xmlDoc.createCDATASection(f[k]), d.appendChild(e), c.appendChild(d), h += b.__lineHeight;
    a.appendChild(c);
    return c
};
_.writeTextBox = function(a, b) {
    var c = this.xmlDoc.createElement("text");
    "" != b.name && c.setAttribute("name", b.name);
    this.writeClassAndID(c, b);
    c.setAttribute("type", "textBox");
    this.writeTransform(c, b);
    this.writeGraphicProperties(c, b);
    this.writeCSSFontFamily(b.font);
    c.setAttribute("font-family", b.font);
    "normal" != b.fontStyle && c.setAttribute("font-style", b.fontStyle);
    "normal" != b.fontWeight && c.setAttribute("font-weight", b.fontWeight);
    c.setAttribute("font-size", b.fontSize);
    c.setAttribute("text-align", b.textAlign);
    "auto" != b.lineHeightMode && c.setAttribute("lineHeight", b.lineHeight);
    var d;
    "left" == b.textAlign ? d = "start" : "center" == b.textAlign ? d = "middle" : "right" == b.textAlign && (d = "end");
    c.setAttribute("text-anchor", d);
    for (var e, f = b.textLines, g = f.length, h = b.fontMetrics.lineHeight, k = 0; k < g; k++) d = this.xmlDoc.createElement("tspan"), d.setAttribute("x", b.linesOffsetX), d.setAttribute("y", h - b.fontMetrics.descent), e = this.xmlDoc.createCDATASection(f[k]), d.appendChild(e), c.appendChild(d), h += b.__lineHeight;
    a.appendChild(c);
    return c
};
_.writeCSSFontFamily = function(a) {
    if (this.htmlPage_html_layer) this.writeCSSFontFamily_HTML(a);
    else if (!1 != this.outputCSS && !0 != this.fontFamilies[a])
        for (var b = document.getElementsByTagName("link"), c, d = b.length, e = 0; e < d; e++)
            if (c = b[e], c.hasAttribute("rel") && "stylesheet" == c.getAttribute("rel") && c.hasAttribute("href")) {
                var f = c.getAttribute("href");
                if (-1 != f.indexOf("fonts.googleapis.com/css?family=")) {
                    var g = f.split("=")[1].split("+").join(" ");
                    a == g && !0 != this.fontFamilies[g] && (c = c.cloneNode(), c.setAttribute("href",
                        f.replace("http:", "").replace("https:", "")), this.defsNode.appendChild(c), this.fontFamilies[g] = !0)
                }
            }
};
_.writeCSSFontFamily_HTML = function(a) {
    if (!1 != this.outputCSS && !0 != this.fontFamilies[a])
        for (var b = document.getElementsByTagName("link"), c, d = b.length, e = 0; e < d; e++)
            if (c = b[e], c.hasAttribute("rel") && "stylesheet" == c.getAttribute("rel") && c.hasAttribute("href")) {
                var f = c.getAttribute("href");
                if (-1 != f.indexOf("fonts.googleapis.com/css?family=")) {
                    var g = f.split("=")[1].split("+").join(" ");
                    a == g && !0 != this.fontFamilies[g] && (c = c.cloneNode(), c.setAttribute("href", f.replace("http:", "").replace("https:", "")), this.htmlHead.appendChild(c),
                        this.fontFamilies[g] = !0)
                }
            }
};
_.writeShape = function(a, b) {
    var c = this.xmlDoc.createElement("path");
    "" != b.name && c.setAttribute("name", b.name);
    this.writeClassAndID(c, b);
    this.writeTransform(c, b);
    this.writeGraphicProperties(c, b);
    var d, e, f, g, h, k = "";
    e = b.paths.length;
    for (var l = 0; l < e; l++) {
        d = b.paths[l];
        f = d.points;
        g = f.length;
        h = f[0];
        for (var k = k + ("M" + h.x + "," + h.y + " "), q = 1; q < g; q++) h = f[q], h instanceof XOS_MoveTo ? k += "M" + h.x + "," + h.y + " " : h instanceof XOS_QuadraticCurveTo ? k += "Q" + h.controlPt.x + "," + h.controlPt.y + " " + h.x + "," + h.y + " " : h instanceof XOS_CubicCurveTo ?
            k += "C" + h.controlPt1.x + "," + h.controlPt1.y + " " + h.controlPt2.x + "," + h.controlPt2.y + " " + h.x + "," + h.y + " " : h instanceof XOS_LineTo && (k += "L" + h.x + "," + h.y + " ");
        d.closePath && (h = f[0], h instanceof XOS_QuadraticCurveTo ? k += "Q" + h.controlPt.x + "," + h.controlPt.y + " " + h.x + "," + h.y + " " : h instanceof XOS_CubicCurveTo ? k += "C" + h.controlPt1.x + "," + h.controlPt1.y + " " + h.controlPt2.x + "," + h.controlPt2.y + " " + h.x + "," + h.y + " " : h instanceof XOS_LineTo && (k += "L" + h.x + "," + h.y + " "), k += "Z ")
    }
    c.setAttribute("d", k);
    a.appendChild(c);
    return c
};
_.writeGraphicEffects = function(a, b) {
    if (0 != b.effectList.length)
        for (var c, d = b.effectList.length, e = 0; e < d; e++) c = b.effectList[e], "Shadow" == c.name && this.writeShadowEffect(a, c)
};
_.writeShadowEffect = function(a, b) {
    var c = this.generateID();
    b.id = c;
    var d = XOS_ColorUtils.htmlColorStringToRGBA(b.shadowColor),
        c = "" + (' <filter id="' + c + '" x="-20%" y="-20%" width="200%" height="200%" type="Shadow" shadowOffsetX="' + b.shadowOffsetX + '" shadowOffsetY="' + b.shadowOffsetY + '" shadowBlur="' + b.shadowBlur + '" shadowColor="' + b.shadowColor + '" >'),
        c = c + (' <feOffset result="offOut" in="SourceGraphic" dx="' + b.shadowOffsetX + '" dy="' + b.shadowOffsetY + '" /> '),
        c = c + (' <feColorMatrix result = "matrixOut" in = "offOut" type = "matrix" values = "' +
            d.r + " 0 0 0 0 0 " + d.g + " 0 0 0 0 0 " + d.b + " 0 0 0 0 0 " + d.a + ' 0"/> '),
        c = c + (' <feGaussianBlur result="blurOut" in="matrixOut" stdDeviation="' + b.shadowBlur / 2.5 + '" /> '),
        c = c + ' <feBlend in="SourceGraphic" in2="blurOut" mode="normal" />  </filter> ',
        d = this.parser.parseFromString(c, "text/xml");
    this.defsNode.appendChild(d.firstChild)
};
_.writeBlendModeEffects = function() {
    var a;
    a = ' <filter id="multiply">   <feBlend mode="multiply" in2="BackgroundImage" in="SourceGraphic"/> </filter>';
    a = this.parser.parseFromString(a, "text/xml");
    this.defsNode.appendChild(a.firstChild);
    a = ' <filter id="screen">   <feBlend mode="screen" in2="BackgroundImage" in="SourceGraphic"/>';
    a += " </filter>";
    a = this.parser.parseFromString(a, "text/xml");
    this.defsNode.appendChild(a.firstChild);
    a = ' <filter id="darken">   <feBlend mode="darken" in2="BackgroundImage" in="SourceGraphic"/>';
    a += " </filter>";
    a = this.parser.parseFromString(a, "text/xml");
    this.defsNode.appendChild(a.firstChild);
    a = ' <filter id="lighten">   <feBlend mode="lighten" in2="BackgroundImage" in="SourceGraphic"/>';
    a += " </filter>";
    a = this.parser.parseFromString(a, "text/xml");
    this.defsNode.appendChild(a.firstChild);
    this.blendModeEffectsWrote = !0
};
_.writeTransform = function(a, b) {
    b.localTransform.isIdentity() || a.setAttribute("transform", "matrix(" + b.localTransform.a + " " + b.localTransform.b + " " + b.localTransform.c + " " + b.localTransform.d + " " + b.localTransform.e + " " + b.localTransform.f + ")")
};
_.writeGraphicFillingType = function(a, b) {
    b.fillFillingType == XOS_DisplayObject2D.GRADIENT_FILLING && this.writeGradient(a, b.fillGradient);
    b.strokeFillingType == XOS_DisplayObject2D.GRADIENT_FILLING && this.writeGradient(a, b.strokeGradient);
    b.fillFillingType == XOS_DisplayObject2D.PATTERN_FILLING && this.writePattern(a, b.fillPattern);
    b.strokeFillingType == XOS_DisplayObject2D.PATTERN_FILLING && this.writePattern(a, b.strokePattern)
};
_.writeAppliedGraphicEffectList = function(a, b) {
    0 != b.effectList.length && a.setAttribute("filter", "url(#" + b.effectList[0].id + ")")
};
_.writeAppliedAnimationList = function(a, b) {
    var c = b.animationList.length;
    if (0 != c) {
        for (var d, e = 0; e < c; e++) {
            d = b.animationList[e];
            var f = this.xmlDoc.createElement("cssAnimation");
            f.setAttribute("animationName", d.animationName);
            f.setAttribute("type", d.type);
            f.setAttribute("delay", d.delay);
            f.setAttribute("duration", d.duration);
            f.setAttribute("iterationCount", d.iterationCount);
            f.setAttribute("fillMode", d.fillMode);
            a.appendChild(f)
        }
        this.writeCSSAnimationsSequence(a, b)
    }
};
_.writeCSSAnimationsSequence = function(a, b) {
    if ("ignore" != b.classNameList) {
        var c = b.animationList.length;
        if (0 != c) {
            !1 == this.isIncludedCSS("animate.") && this.displayRef.cssIncludes.push(this.sharedProjectsLibs.animateCSS);
            var d = "";
            a.hasAttribute("class") && (d = a.getAttribute("class"));
            var e = "_animation_" + b.idi;
            a.setAttribute("class", d + (" " + e));
            for (var f = b.animationList[0], d = f.animationName, g = f.delay + "s", h = f.duration + "s", k = f.fillMode, l = f.iterationCount, q = 1; q < c; q++) f = b.animationList[q], d += ", " + f.animationName,
                g += ", " + f.delay + "s", h += ", " + f.duration + "s", k += ", " + f.fillMode, l += ", " + f.iterationCount;
            c = "." + e + "{\n" + ("-webkit-animation-name:" + d + ";\n");
            c += "-webkit-animation-delay:" + g + ";\n";
            c += "-webkit-animation-duration:" + h + ";\n";
            c += "-webkit-animation-fill-mode:" + k + ";\n";
            c += "-webkit-animation-iteration-count:" + l + ";\n";
            c += "-webkit-transform:translateZ(0px);\n";
            c += "animation:" + d + ";\n";
            c += "animation-delay:" + g + ";\n";
            c += "animation-duration:" + h + ";\n";
            c += "animation-fill-mode:" + k + ";\n";
            c += "animation-iteration-count:" +
                l + ";\n";
            c += "}\n";
            this.htmlPage_html_layer ? this.styleNode_animations.appendChild(this.htmlDocument.createTextNode(c)) : this.styleNode_animations.appendChild(this.xmlDoc.createTextNode(c))
        }
    }
};
_.isIncludedCSS = function(a) {
    for (var b = this.displayRef.cssIncludes.length, c = 0; c < b; c++)
        if (-1 != this.displayRef.cssIncludes[c].indexOf(a)) return !0;
    return !1
};
_.isIncludedJS = function(a) {
    for (var b = this.displayRef.jsIncludes.length, c = 0; c < b; c++)
        if (-1 != this.displayRef.jsIncludes[c].indexOf(a)) return !0;
    return !1
};
_.writeAppliedTweenList = function(a, b) {
    var c = b.tweenList.length;
    if (0 != c)
        for (var d, e = 0; e < c; e++) d = b.tweenList[e], this.writeTween(a, d)
};
_.writeTween = function(a, b) {
    var c = this.xmlDoc.createElement(b.name);
    c.setAttribute("xmlns", "http://www.w3.org/2000/svg");
    "" != b.id && c.setAttribute("id", b.id);
    c.setAttribute("attributeName", b.attributeName);
    "auto" != b.attributeType && c.setAttribute("attributeType", b.attributeType);
    "" != b.type && c.setAttribute("type", b.type);
    "" != b.begin && c.setAttribute("begin", b.begin);
    "" != b.end && c.setAttribute("end", b.end);
    "" != b.dur && c.setAttribute("dur", b.dur);
    "always" != b.restart && c.setAttribute("restart", b.restart);
    "" !=
    b.repeatCount && c.setAttribute("repeatCount", b.repeatCount);
    "remove" != b.fill && c.setAttribute("fill", b.fill);
    "replace" != b.additive && c.setAttribute("additive", b.additive);
    "none" != b.accumulate && c.setAttribute("accumulate", b.accumulate);
    "linear" != b.calcMode && c.setAttribute("calcMode", b.calcMode);
    "spline" == b.calcMode ? b.keySplines ? c.setAttribute("keySplines", b.keySplines) : (c.setAttribute("keySplines", XOS_SVGTween.motionTypeList[b.motionType]), c.setAttribute("values", b.from + ";" + b.to)) : ("" != b.from && c.setAttribute("from",
        b.from), "" != b.to && c.setAttribute("to", b.to));
    a.appendChild(c)
};
_.writeGraphicProperties = function(a, b) {
    0 != b.width && a.setAttribute("width", b.width);
    0 != b.height && a.setAttribute("height", b.height);
    !0 != b.visible && a.setAttribute("visible", b.visible);
    !1 != b.isLocked && a.setAttribute("isLocked", b.isLocked);
    1 != b.lineWidth && a.setAttribute("stroke-width", b.lineWidth);
    "butt" != b.lineCap && a.setAttribute("stroke-linecap", b.lineCap);
    "miter" != b.lineJoin ? a.setAttribute("stroke-linejoin", b.lineJoin) : 10 != b.miterLimit && a.setAttribute("stroke-miterlimit", b.miterLimit);
    b.strokeFillingType ==
        XOS_DisplayObject2D.NONE_FILLING ? a.setAttribute("stroke", "none") : b.strokeFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING ? (a.setAttribute("stroke", b.strokeColor), 1 != b.strokeAlpha && a.setAttribute("stroke-opacity", b.strokeAlpha)) : b.strokeFillingType == XOS_DisplayObject2D.GRADIENT_FILLING ? a.setAttribute("stroke", "url(#" + b.strokeGradient.id + ")") : b.strokeFillingType == XOS_DisplayObject2D.PATTERN_FILLING && a.setAttribute("stroke", "url(#" + b.strokePattern.id + ")");
    b.fillFillingType == XOS_DisplayObject2D.NONE_FILLING ?
        a.setAttribute("fill", "none") : b.fillFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING ? (a.setAttribute("fill", b.fillColor), 1 != b.fillAlpha && a.setAttribute("fill-opacity", b.fillAlpha)) : b.fillFillingType == XOS_DisplayObject2D.GRADIENT_FILLING ? a.setAttribute("fill", "url(#" + b.fillGradient.id + ")") : b.fillFillingType == XOS_DisplayObject2D.PATTERN_FILLING && a.setAttribute("fill", "url(#" + b.fillPattern.id + ")");
    1 != b.opacity && a.setAttribute("opacity", b.opacity);
    "source-over" != b.compositeOperation && a.setAttribute("compositeOperation",
        b.compositeOperation);
    "no" != b.placeIntoHtmlLayer && a.setAttribute("placeIntoHtmlLayer", "yes")
};
ExtendClass(XOS_PointHandle2D, XOS_DisplayObject2D);

function XOS_PointHandle2D(a, b) {
    XOS_PointHandle2D.baseConstructor.call(this);
    this.pointRef = a;
    this.interactionTool = b;
    this.strokeColor = "#2E87FF";
    this.fillColor = "#FFFFFF";
    this.selectionColor = "#2E87FF";
    this.isControlPoint = !1
}
_ = XOS_PointHandle2D.prototype;
_.drawGeometry = function() {
    this.pointRef instanceof XOS_ControlPoint ? "disabled" != this.displayRef.interactionViewMode && (DEVICE_WITH_TOUCH_EVENTS ? this.graphicContext.drawEllipse(-4, -4, 8, 8) : this.graphicContext.drawEllipse(-2, -2, 4, 4)) : "disabled" == this.displayRef.interactionViewMode ? this.graphicContext.rect(-2, -2, 4, 4) : DEVICE_WITH_TOUCH_EVENTS ? this.graphicContext.rect(-4, -4, 8, 8) : this.graphicContext.rect(-2.5, -2.5, 5, 5)
};
_.drawGeometryForHitTest = function() {
    this.visible && (DEVICE_WITH_TOUCH_EVENTS ? this.pointRef instanceof XOS_ControlPoint ? this.graphicContext.rect(-30, -30, 60, 60) : this.graphicContext.rect(-20, -20, 40, 40) : this.pointRef instanceof XOS_ControlPoint ? this.graphicContext.rect(-10, -10, 20, 20) : this.graphicContext.rect(-5, -5, 10, 10))
};
_.draw = function() {
    this.visible && (this.invalidTransform && this.updateGlobalTransform(), this.applyTransform(), this.graphicContext.beginPath(), this.drawGeometry(), this.graphicContext.closePath(), "disabled" == this.displayRef.interactionViewMode ? (this.graphicContext.fillStyle = this.selectionColor, this.graphicContext.fill()) : (this.pointRef && this.pointRef.isSelected ? this.graphicContext.fillStyle = this.selectionColor : (this.graphicContext.strokeStyle = this.strokeColor, this.graphicContext.fillStyle = this.fillColor),
        this.graphicContext.fill(), this.graphicContext.stroke()))
};
_.onMouseDown = function(a) {
    this.interactionTool.clickedHandle = this;
    this.interactionTool.onMouseDown(a)
};
ExtendClass(XOS_ButtonHandle2D, XOS_Image);

function XOS_ButtonHandle2D(a) {
    XOS_ButtonHandle2D.baseConstructor.call(this);
    this.interactionTool = a;
    this.defaultOverHandleCursor = "pointer"
}
_ = XOS_ButtonHandle2D.prototype;
_.onMouseDown = function(a) {
    this.interactionTool.clickedHandle = this;
    this.interactionTool.onMouseDown(a)
};
_.draw = function() {
    "disabled" != this.displayRef.interactionViewMode && (this.graphicContext.globalAlpha = this.fillAlpha, this.graphicContext.globalCompositeOperation = this.compositeOperation, this.invalidTransform && this.updateGlobalTransform(), this.applyTransform(), !0 == this.isLoaded && this.graphicContext.drawImage(this.image, -(this.width / 2), -(this.height / 2), this.width, this.height), this.drawPivot())
};
_.drawGeometryForHitTest = function() {
    this.graphicContext.beginPath();
    this.graphicContext.rect(-(this.width / 2), -(this.height / 2), this.width, this.height)
};
ExtendClass(XOS_PointsInteraction, XOS_DisplayObject2D);

function XOS_PointsInteraction(a, b) {
    XOS_PointsInteraction.baseConstructor.call(this);
    this.displayObjectEditorRef = a;
    this.translatePointTool = this.displayObjectEditorRef.tools.translatePointHandle_tool;
    this.shapeRef = b;
    this.createInteractionHandles();
    this.clickedHandleNextControlPt = this.clickedHandlePreviousControlPt = this.clickedHandle = null
}
_ = XOS_PointsInteraction.prototype;
_.hitTestObject = function(a, b, c) {
    return this.hitTestChildren(a, b, c)
};
_.createInteractionHandles = function() {
    for (var a = this.shapeRef.getEditablePoints(), b = a.length, c = 0; c < b; c++) this.addChild(new XOS_PointHandle2D(a[c], this.translatePointTool));
    this.hideAllControlPoints()
};
_.hideAllControlPoints = function() {
    for (var a = this.children.length, b = 0; b < a; b++) this.children[b].pointRef instanceof XOS_ControlPoint && (this.children[b].visible = !1)
};
_.getNearHandleControlPoints = function(a) {
    var b = this.children[a],
        c = null,
        d = null,
        e = b.pointRef,
        f = e.pathRef;
    if (e == f.getLastPoint()) {
        if (e = this.children[a - 1], e.pointRef instanceof XOS_ControlPoint && (c = e), f.closePath) {
            var g = f.getFirstPoint(),
                f = null;
            g instanceof XOS_CubicCurveTo ? f = g.controlPt1 : g instanceof XOS_QuadraticCurveTo && (f = g.controlPt);
            if (null != f) {
                for (a -= 1; this.children[a].pointRef != f;) a--;
                d = this.children[a]
            }
        }
    } else e == f.getFirstPoint() ? (g = this.children[a + 1], g.pointRef instanceof XOS_ControlPoint &&
        (d = g), f.closePath && (e instanceof XOS_CubicCurveTo || e instanceof XOS_QuadraticCurveTo) && (c = e = this.children[a - 1])) : (e = this.children[a - 1], g = this.children[a + 1], e.pointRef instanceof XOS_ControlPoint && (c = e), g.pointRef instanceof XOS_ControlPoint && (d = g));
    return {
        handle: b,
        handlePreviousControlPt: c,
        handleNextControlPt: d
    }
};
_.showControlPoints = function(a) {
    this.clickedHandle = a;
    this.clickedHandleNextControlPt = this.clickedHandlePreviousControlPt = null;
    a = this.getChildIndex(a);
    a = this.getNearHandleControlPoints(a);
    a.handlePreviousControlPt && !0 != a.handlePreviousControlPt.pointRef.collapsed && (a.handlePreviousControlPt.visible = !0, this.clickedHandlePreviousControlPt = a.handlePreviousControlPt);
    a.handleNextControlPt && !0 != a.handleNextControlPt.pointRef.collapsed && (a.handleNextControlPt.visible = !0, this.clickedHandleNextControlPt = a.handleNextControlPt)
};
_.updateInteractionHandles = function() {
    for (var a = null, b = this.numChildren(), c = 0; c < b; c++) a = this.shapeRef.localToGlobal(this.children[c].pointRef), this.children[c].setPosition(a.x, a.y)
};
_.draw = function() {
    this.invalidTransform && this.updateGlobalTransform();
    this.applyTransform();
    this.updateInteractionHandles();
    this.drawChildren()
};
_.drawChildren = function() {
    this.drawInteractionLines();
    for (var a = this.numChildren(), b = 0; b < a; b++) this.children[b].draw()
};
_.drawInteractionLines = function() {
    "disabled" != this.displayRef.interactionViewMode && (this.graphicContext.beginPath(), this.graphicContext.strokeStyle = "#2E87FF", this.graphicContext.lineWidth = 1, this.clickedHandlePreviousControlPt && (this.graphicContext.moveTo(this.clickedHandlePreviousControlPt.x, this.clickedHandlePreviousControlPt.y), this.graphicContext.lineTo(this.clickedHandle.x, this.clickedHandle.y)), this.clickedHandleNextControlPt && (this.graphicContext.moveTo(this.clickedHandleNextControlPt.x, this.clickedHandleNextControlPt.y),
        this.graphicContext.lineTo(this.clickedHandle.x, this.clickedHandle.y)), this.graphicContext.closePath(), this.graphicContext.stroke())
};
_.getSelectedHandles = function(a) {
    var b = null,
        c = [],
        d = this.children.length,
        e = 0;
    !1 != a && (a = !0);
    if (a)
        for (e = 0; e < d; e++) b = this.children[e], !b.pointRef.isSelected || b.pointRef instanceof XOS_ControlPoint || (a = this.getNearHandleControlPoints(e), a.handlePreviousControlPt && (a.handlePreviousControlPt.pointRef.isSelected = !0), a.handleNextControlPt && (a.handleNextControlPt.pointRef.isSelected = !0));
    for (e = 0; e < d; e++) b = this.children[e], b.pointRef.isSelected && c.push(b);
    return c
};
ExtendClass(XOS_BoundsInteraction2D, XOS_DisplayObject2D);

function XOS_BoundsInteraction2D(a, b) {
    XOS_BoundsInteraction2D.baseConstructor.call(this);
    this.displayObjectEditorRef = a;
    this.bounds = null;
    this.displayObject2DRef = b;
    this.maintainAspectRatio = !1;
    this.displayObject2DRef ? (this.isLocalBounds = !0, this.maintainAspectRatio = this.displayObject2DRef.maintainAspectRatio) : this.isLocalBounds = !1;
    this.scaleBoundsTool = this.displayObjectEditorRef.tools.scaleBoundsHandle_tool;
    this.rotateBoundsTool = this.displayObjectEditorRef.tools.rotateBoundsHandle_tool;
    this.rotationHandle =
        null;
    this.createInteractionHandles();
    this.type = "boundsInteraction"
}
_ = XOS_BoundsInteraction2D.prototype;
_.hitTestObject = function(a, b, c) {
    return this.hitTestChildren(a, b, c)
};
_.createInteractionHandles = function() {
    for (var a = 0; 8 > a; a++) this.addChild(new XOS_PointHandle2D(null, this.scaleBoundsTool));
    this.rotationHandle = new XOS_ButtonHandle2D(this.rotateBoundsTool);
    this.rotationHandle.load(this.displayObjectEditorRef.rotateBoundsIconUrl);
    this.rotationHandle.defaultOverHandleCursor = "rotate_cur";
    this.addChild(this.rotationHandle)
};
_.updateInteractionHandles = function() {
    if (this.isLocalBounds) this.updateLocalInteractionHandles();
    else {
        this.bounds = this.displayObjectEditorRef.getSelectionBounds();
        var a = this.bounds.min.x + (this.bounds.max.x - this.bounds.min.x) / 2,
            b = this.bounds.min.y + (this.bounds.max.y - this.bounds.min.y) / 2;
        this.children[0].setPosition(this.bounds.min.x, this.bounds.min.y);
        this.children[1].setPosition(a, this.bounds.min.y);
        this.children[2].setPosition(this.bounds.max.x, this.bounds.min.y);
        this.children[3].setPosition(this.bounds.max.x,
            b);
        this.children[4].setPosition(this.bounds.max.x, this.bounds.max.y);
        this.children[5].setPosition(a, this.bounds.max.y);
        this.children[6].setPosition(this.bounds.min.x, this.bounds.max.y);
        this.children[7].setPosition(this.bounds.min.x, b);
        this.rotationHandle.setPosition(a, this.bounds.max.y + 20)
    }
};
_.updateLocalInteractionHandles = function() {
    this.bounds = this.displayObject2DRef.getLocalBounds();
    var a = null,
        b = this.bounds.min.x + (this.bounds.max.x - this.bounds.min.x) / 2,
        c = this.bounds.min.y + (this.bounds.max.y - this.bounds.min.y) / 2,
        a = this.displayObject2DRef.localToGlobal({
            x: this.bounds.min.x,
            y: this.bounds.min.y
        });
    this.children[0].setPosition(a.x, a.y);
    a = this.displayObject2DRef.localToGlobal({
        x: b,
        y: this.bounds.min.y
    });
    this.children[1].setPosition(a.x, a.y);
    a = this.displayObject2DRef.localToGlobal({
        x: this.bounds.max.x,
        y: this.bounds.min.y
    });
    this.children[2].setPosition(a.x, a.y);
    a = this.displayObject2DRef.localToGlobal({
        x: this.bounds.max.x,
        y: c
    });
    this.children[3].setPosition(a.x, a.y);
    a = this.displayObject2DRef.localToGlobal({
        x: this.bounds.max.x,
        y: this.bounds.max.y
    });
    this.children[4].setPosition(a.x, a.y);
    a = this.displayObject2DRef.localToGlobal({
        x: b,
        y: this.bounds.max.y
    });
    this.children[5].setPosition(a.x, a.y);
    b = this.displayObject2DRef.getRotation();
    this.rotationHandle.setRotation(b);
    this.rotationHandle.setPosition(a.x,
        a.y);
    this.rotationHandle.translate(0, 20, !0);
    a = this.displayObject2DRef.localToGlobal({
        x: this.bounds.min.x,
        y: this.bounds.max.y
    });
    this.children[6].setPosition(a.x, a.y);
    a = this.displayObject2DRef.localToGlobal({
        x: this.bounds.min.x,
        y: c
    });
    this.children[7].setPosition(a.x, a.y)
};
_.setInteractionHandlesVisibility = function(a) {
    for (var b = this.children.length, c = 0; c < b; c++) this.children[c].visible = a
};
_.drawChildren = function() {
    this.graphicContext.beginPath();
    this.graphicContext.strokeStyle = "#2E87FF";
    this.graphicContext.lineWidth = 1;
    this.graphicContext.moveTo(this.children[0].x, this.children[0].y);
    for (var a = 0; 8 > a; a++) this.graphicContext.lineTo(this.children[a].x, this.children[a].y);
    this.graphicContext.closePath();
    this.graphicContext.stroke();
    for (a = 0; 9 > a; a++) this.children[a].draw()
};
_.draw = function() {
    !1 != this.visible && (this.updateInteractionHandles(), this.drawChildren())
};

function ScaleGraphicObject_action(a) {
    this.displayObjectEditorRef = a;
    this.objectList = [];
    this.pivot = null;
    this.scaleY = this.scaleX = 0;
    this.isLocalMode = !1
}
_ = ScaleGraphicObject_action.prototype;
_.execute = function() {
    this.displayObjectEditorRef.scaleGraphicObjects(this.objectList, this.scaleX, this.scaleY, this.pivot, this.isLocalMode);
    this.displayObjectEditorRef.normalizeGraphicObjects(this.objectList);
    this.displayObjectEditorRef.onChange();
    this.displayObjectEditorRef.invalidateRender()
};
_.addObjectToAction = function(a) {
    this.objectList.push(a)
};
_.setScaleValues = function(a, b, c) {
    this.scaleX = a;
    this.scaleY = b;
    this.pivot = c
};

function TranslateGraphicObject_action(a) {
    this.displayObjectEditorRef = a;
    this.objectList = [];
    this.translationY = this.translationX = 0
}
_ = TranslateGraphicObject_action.prototype;
_.execute = function() {
    this.displayObjectEditorRef.translateGraphicObjects(this.objectList, this.translationX, this.translationY);
    for (var a = this.objectList.length, b = 0; b < a; b++) this.displayObjectEditorRef.selectGraphicObject(this.objectList[b], !0);
    this.displayObjectEditorRef.onChange();
    this.displayObjectEditorRef.invalidateRender();
    this.displayObjectEditorRef.showSelectionProperties()
};
_.addObjectToAction = function(a) {
    this.objectList.push(a)
};
_.setTranslationValues = function(a, b) {
    this.translationX = a;
    this.translationY = b
};

function RotateGraphicObject_action(a) {
    this.displayObjectEditorRef = a;
    this.objectList = [];
    this.rotation = 0;
    this.pivot = null;
    this.pivotY = this.pivotX = 0
}
_ = RotateGraphicObject_action.prototype;
_.execute = function() {
    this.displayObjectEditorRef.rotateGraphicObjects(this.objectList, this.rotation, this.pivot);
    for (var a = this.objectList.length, b = 0; b < a; b++) this.displayObjectEditorRef.selectGraphicObject(this.objectList[b], !0);
    this.displayObjectEditorRef.onChange();
    this.displayObjectEditorRef.invalidateRender();
    this.displayObjectEditorRef.showSelectionProperties()
};
_.addObjectToAction = function(a) {
    this.objectList.push(a)
};
_.setRotationValues = function(a, b) {
    this.rotation = a;
    this.pivot = b
};

function AddGraphicObject_action(a) {
    this.displayObjectEditorRef = a;
    //console.log(this.displayObjectEditorRef);
    this.objectList = []
}
_ = AddGraphicObject_action.prototype;
_.execute = function() {
    for (var a = this.objectList.length, b = 0; b < a; b++) this.displayObjectEditorRef.addGraphicObject(this.objectList[b].displayObject, this.objectList[b].parentRef, this.objectList[b].index), this.displayObjectEditorRef.selectGraphicObject(this.objectList[b].displayObject, !0);
    this.displayObjectEditorRef.onChange();
    this.displayObjectEditorRef.invalidateRender();
    this.displayObjectEditorRef.showSelectionProperties()
};
_.addObjectToAction = function(a) {
    this.objectList.push({
        displayObject: a,
        parentRef: a.parent,
        index: a.parent.getChildIndex(a)
    })
};

function RemoveGraphicObject_action(a) {
    this.displayObjectEditorRef = a;
    this.objectList = []
}
_ = RemoveGraphicObject_action.prototype;
_.execute = function() {
    for (var a = this.objectList.length, b = 0; b < a; b++) this.objectList[b].parent.removeChild(this.objectList[b]);
    this.displayObjectEditorRef.onChange();
    this.displayObjectEditorRef.invalidateRender()
};
_.addObjectToAction = function(a) {
    this.objectList.push(a)
};

function SetGraphicObjectIndex_action(a) {
    this.displayObjectEditorRef = a;
    this.objectList = []
}
_ = SetGraphicObjectIndex_action.prototype;
_.execute = function() {
    for (var a = null, b = this.objectList.length, c = 0; c < b; c++) a = this.objectList[c], a.displayObject.parent.setChildIndex(a.displayObject, a.index), this.displayObjectEditorRef.selectGraphicObject(a.displayObject, !0);
    this.displayObjectEditorRef.onChange();
    this.displayObjectEditorRef.invalidateRender();
    this.displayObjectEditorRef.showSelectionProperties()
};
_.addObjectToAction = function(a, b) {
    this.objectList.push({
        displayObject: a,
        index: b
    })
};

function TranslatePoints_action(a) {
    this.displayObjectEditorRef = a;
    this.objectList = [];
    this.translationY = this.translationX = 0
}
_ = TranslatePoints_action.prototype;
_.execute = function() {
    for (var a = null, b = a = null, c = 0, d = this.objectList.length, e = 0; e < d; e++) {
        b = this.objectList[e];
        this.displayObjectEditorRef.selectGraphicObject(this.objectList[e].shape, !0);
        for (var c = b.points.length, f = 0; f < c; f++) b.points[f].isSelected = !0, a = b.shape.localToGlobal(b.points[f]), a.x += this.translationX, a.y += this.translationY, a = b.shape.globalToLocal(a), b.points[f].x = a.x, b.points[f].y = a.y;
        b.shape.invalidateBounds()
    }
    this.displayObjectEditorRef.onChange();
    this.displayObjectEditorRef.invalidateRender();
    this.displayObjectEditorRef.interactionDisplay.invalidateRender();
    this.displayObjectEditorRef.showSelectionProperties()
};
_.addObjectAndPointsToAction = function(a, b) {
    this.objectList.push({
        shape: a,
        points: b
    })
};
_.setTranslationValues = function(a, b) {
    this.translationX = a;
    this.translationY = b
};

function SetObjectProperties_action(a) {
    this.displayObjectEditorRef = a;
    this.objectList = []
}
_ = SetObjectProperties_action.prototype;
_.execute = function() {
    for (var a = null, b = null, c = null, d = "", e = "", f = this.objectList.length, g = 0; g < f; g++) {
        a = this.objectList[g].objectTarget;
        b = this.objectList[g].properties;
        onPropertiesChangeFunction = this.objectList[g].onPropertiesChangeFunction;
        for (var c = b.length, h = 0; h < c; h++) d = b[h].property, e = b[h].value, a[d] = e;
        a instanceof XOS_DisplayObject2D && (a instanceof XOS_Layer || a != this.displayObjectEditorRef.content && this.displayObjectEditorRef.selectGraphicObject(a, !0));
        onPropertiesChangeFunction && onPropertiesChangeFunction()
    }
    this.displayObjectEditorRef.onChange();
    this.displayObjectEditorRef.invalidateRender();
    this.displayObjectEditorRef.interactionDisplay.invalidateRender();
    this.displayObjectEditorRef.showSelectionProperties()
};
_.addObjectToAction = function(a, b, c) {
    var d = [];
    b = b.split(",");
    for (var e = "", f = b.length, g = 0; g < f; g++) e = b[g], d.push({
        property: e,
        value: a[e]
    });
    this.objectList.push({
        objectTarget: a,
        properties: d,
        onPropertiesChangeFunction: c
    })
};

function XOS_BaseTool_tool(a) {
    this.displayObjectEditorRef = a;
    this.interactionCanvasContext = this.displayObjectEditorRef.interactionDisplay.graphicContext;
    this.pt1 = new XOS_Point2D(0, 0);
    this.pt2 = new XOS_Point2D(0, 0);
    this.defaultOverHandleCursor = this.onDragOverContourCursor = this.onDragOverVertexCursor = this.onDragOverInstanceCursor = this.onDragCursor = this.defaultOverContourCursor = this.defaultOverVertexCursor = this.defaultOverInstanceCursor = this.defaultCursor = "default_cur";
    this.fingerTouches = this.startContentLocalTransform =
        null;
    this.gesturePreviousScale = this.gesturePreviousCenter = 0;
    this.gestureHasChangedView = !1;
    this.lastMouseDownTime = 0
}
_ = XOS_BaseTool_tool.prototype;
_.onActivate = function() {
    this.displayObjectEditorRef.setCursor(this.defaultCursor);
    this.displayObjectEditorRef.setInteractionMode(this.displayObjectEditorRef.interactionMode, "disabled")
};
_.onDeactivate = function() {};
_.getPointSnappedToAngle = function(a, b, c, d) {
    c || (c = Math.PI / 4);
    d || (d = 0);
    var e = b.x - a.x,
        f = b.y - a.y;
    b = Math.atan2(f, e);
    e = Math.sqrt(e * e + f * f);
    b = d + Math.round((b - d) / c) * c;
    return new XOS_Point2D(a.x + e * Math.cos(b), a.y + e * Math.sin(b))
};
_.isDoubleClick = function() {
    var a = (new Date).getTime();
    //alert("click on items"); 
    if (250 > a - this.lastMouseDownTime) return !0;
    this.lastMouseDownTime = a;
    return !1
};
_.onDoubleClick = function() {};
_.defineStartPenPosition = function(a) {
    a = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(a);
    var b = this.displayObjectEditorRef.interactionDisplay.hitTestAll(a);
    if (b.hit) return this.pt2.x = this.pt1.x = b.instance.x, this.pt2.y = this.pt1.y = b.instance.y, b.snapType = "HANDLE", b;
    a = this.displayObjectEditorRef.mousePointToPenInfo(a, null, {
        snapToVertices: !0,
        snapToContour: !0
    });
    b = null != a.snappedPosition ? a.snappedPosition : a.position;
    this.pt2.x = this.pt1.x = b.x;
    this.pt2.y = this.pt1.y = b.y;
    return a
};
_.onMouseDown = function(a) {
    DEVICE_WITH_TOUCH_EVENTS || GLOBAL_MOUSE_LISTENER.resetState();
    if (this.isDoubleClick()) this.onDoubleClick();
    else DEVICE_WITH_TOUCH_EVENTS && this.defineStartPenPosition(GLOBAL_MOUSE_LISTENER.mouseDownPt), GLOBAL_MOUSE_LISTENER.objectTarget = this
};
_.onMouseMove = function(a) {
    if (!GLOBAL_MOUSE_LISTENER.objectTarget)
        if (a = this.defineStartPenPosition(GLOBAL_MOUSE_LISTENER.mouseMovePt), a.snapType) switch (a.snapType) {
                case "HANDLE":
                    this.onMouseMoveOverHandle(a);
                    break;
                case "VERTEX":
                    this.onMouseMoveOverVertex(a);
                    break;
                case "BOUNDS":
                    this.onMouseMoveOverBoundsVertex(a);
                    break;
                case "CONTOUR":
                    this.onMouseMoveOverContour(a)
            } else if (a.instance) this.onMouseMoveOverInstance(a);
            else this.displayObjectEditorRef.setCursor(this.defaultCursor)
};
_.onMouseMoveOverInstance = function(a) {
    this.displayObjectEditorRef.setCursor(this.defaultOverInstanceCursor)
};
_.onMouseMoveOverHandle = function(a) {
    "disabled" == this.displayObjectEditorRef.interactionDisplay.interactionViewMode ? this.displayObjectEditorRef.setCursor(this.defaultOverHandleCursor) : a.instance.defaultOverHandleCursor ? this.displayObjectEditorRef.setCursor(a.instance.defaultOverHandleCursor) : this.displayObjectEditorRef.setCursor(this.defaultOverHandleCursor)
};
_.onMouseMoveOverVertex = function(a) {
    this.displayObjectEditorRef.setCursor(this.defaultOverVertexCursor)
};
_.onMouseMoveOverBoundsVertex = function(a) {
    this.displayObjectEditorRef.setCursor(this.defaultCursor)
};
_.onMouseMoveOverContour = function(a) {
    this.displayObjectEditorRef.setCursor(this.defaultOverContourCursor)
};
_.onGlobalMouseStartDrag = function(a) {};
_.onGlobalMouseDrag = function(a) {};
_.onGlobalMouseUp = function(a) {};
_.onGlobalMouseDragStartPause = function(a) {};
_.onGlobalMouseEndDrag = function(a) {};
_.onTouchStart = function(a) {
    this.fingerTouches = [];
    if (1 == a.targetTouches.length) this.onMouseDown(a);
    else 2 == a.targetTouches.length && (this.fingerTouches = a.targetTouches, this.gesturePreviousScale = 1, this.gesturePreviousCenter = {
            x: this.fingerTouches[0].pageX + (this.fingerTouches[1].pageX - this.fingerTouches[0].pageX) / 2,
            y: this.fingerTouches[0].pageY + (this.fingerTouches[1].pageY - this.fingerTouches[0].pageY) / 2
        }, this.gesturePreviousCenter = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(this.gesturePreviousCenter),
        this.exitByGesture(), this.startContentLocalTransform = this.displayObjectEditorRef.content.localTransform.clone())
};
_.onTouchMove = function(a) {
    this.fingerTouches = 2 == a.targetTouches.length && 2 == a.touches.length ? a.targetTouches : []
};
_.onGestureStart = function(a) {};
_.onGestureEnd = function(a) {
    !0 == this.gestureHasChangedView && (a = this.displayObjectEditorRef.content.localTransform.clone(), this.displayObjectEditorRef.saveChangedContentViewToHistory(this.startContentLocalTransform, a), this.displayObjectEditorRef.interactionDisplay.visible = !0, this.displayObjectEditorRef.interactionDisplay.invalidateRender(), this.gestureHasChangedView = !1)
};
_.onGestureChange = function(a) {
    if (2 == this.fingerTouches.length) {
        this.gestureHasChangedView = !0;
        var b = a.scale / this.gesturePreviousScale,
            c = {
                x: this.fingerTouches[0].pageX + (this.fingerTouches[1].pageX - this.fingerTouches[0].pageX) / 2,
                y: this.fingerTouches[0].pageY + (this.fingerTouches[1].pageY - this.fingerTouches[0].pageY) / 2
            },
            c = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(c);
        this.displayObjectEditorRef.scaleGraphicObjects([this.displayObjectEditorRef.content], b, b, c, !0);
        this.displayObjectEditorRef.content.translate(c.x -
            this.gesturePreviousCenter.x, c.y - this.gesturePreviousCenter.y);
        this.gesturePreviousScale = a.scale;
        this.gesturePreviousCenter.x = c.x;
        this.gesturePreviousCenter.y = c.y;
        this.displayObjectEditorRef.content.invalidateTransform();
        this.displayObjectEditorRef.invalidateRender();
        this.displayObjectEditorRef.interactionDisplay.visible && (this.displayObjectEditorRef.interactionDisplay.visible = !1, this.displayObjectEditorRef.interactionDisplay.clear())
    }
};
_.exitByGesture = function() {
    GLOBAL_MOUSE_LISTENER.resetState();
    this.displayObjectEditorRef.setCursor(this.defaultCursor)
};
ExtendClass(XOS_TranslatePointHandle2D_tool, XOS_BaseTool_tool);

function XOS_TranslatePointHandle2D_tool(a) {
    XOS_TranslatePointHandle2D_tool.baseConstructor.call(this, a);
    this.pointSelection = this.redoAction = this.undoAction = this.clickedHandle_id = this.clickedHandle = null;
    this.startTranslationPt = new XOS_Point2D(0, 0);
    this.currentTranslationPt = new XOS_Point2D(0, 0);
    this.onDragCursor = "translateVertex_cur";
    this.onDragOverVertexCursor = "translateVertexOverVertex_cur"
}
_ = XOS_TranslatePointHandle2D_tool.prototype;
_.onMouseDown = function(a) {
    var b = this.clickedHandle.pointRef,
        c = this.clickedHandle.parent;
    b instanceof XOS_ControlPoint || (c.hideAllControlPoints(), a.shiftKey ? b.isSelected ? b.isSelected = !1 : (b.isSelected = !0, c.showControlPoints(this.clickedHandle)) : (!1 == b.isSelected && (c.hideAllControlPoints(), this.displayObjectEditorRef.deselectAllGraphicObjectsPoints(), b.isSelected = !0), c.showControlPoints(this.clickedHandle)), this.displayObjectEditorRef.interactionDisplay.invalidateRender());
    this.startTranslationPt.x =
        this.currentTranslationPt.x = this.clickedHandle.x;
    this.startTranslationPt.y = this.currentTranslationPt.y = this.clickedHandle.y;
    GLOBAL_MOUSE_LISTENER.objectTarget = this
};
_.onGestureEnd = function(a) {};
_.onGestureChange = function(a) {};
_.exitByGesture = function() {};
_.onGlobalMouseStartDrag = function(a) {
    this.displayObjectEditorRef.setCursor(this.onDragCursor);
    this.undoAction = new TranslatePoints_action(this.displayObjectEditorRef);
    this.redoAction = new TranslatePoints_action(this.displayObjectEditorRef);
    this.displayObjectEditorRef.history.addUndoRedoActions(this.undoAction, this.redoAction);
    this.clickedHandle.pointRef instanceof XOS_ControlPoint ? this.addObjectAndPointsToUndoRedoAction(this.clickedHandle.parent.shapeRef, [this.clickedHandle]) : this.createHandleSelectionArray()
};
_.onGlobalMouseDrag = function(a) {
    var b = this.displayObjectEditorRef.mousePointToPenInfo(this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt), null, {
            snapToVertices: !0,
            snapToContour: !0
        }, !0),
        c = b.position;
    a.shiftKey && (c = this.getPointSnappedToAngle(this.startTranslationPt, c));
    b.snappedPosition ? (this.displayObjectEditorRef.setCursor(this.onDragOverVertexCursor), c = b.snappedPosition) : this.displayObjectEditorRef.setCursor(this.onDragCursor);
    this.doMouseDragAction(c)
};
_.doMouseDragAction = function(a) {
    var b = a.x - this.clickedHandle.x;
    a = a.y - this.clickedHandle.y;
    this.clickedHandle.pointRef instanceof XOS_ControlPoint ? this.translateClickedControlPoint(b, a) : this.translateSelectedPoints(b, a);
    this.displayObjectEditorRef.interactionDisplay.invalidateRender()
};
_.onGlobalMouseEndDrag = function(a) {
    this.displayObjectEditorRef.setCursor(this.defaultCursor);
    a = this.clickedHandle.x - this.startTranslationPt.x;
    var b = this.clickedHandle.y - this.startTranslationPt.y;
    this.undoAction.setTranslationValues(-a, -b);
    this.redoAction.setTranslationValues(a, b);
    this.displayObjectEditorRef.invalidateRender();
    this.displayObjectEditorRef.showSelectionProperties()
};
_.onGlobalMouseUp = function(a) {};
_.createHandleSelectionArray = function() {
    this.selectedHandles = [];
    for (var a = null, b = null, c = this.displayObjectEditorRef.interactionDisplay.children.length, d = 0; d < c; d++) a = this.displayObjectEditorRef.interactionDisplay.children[d], b = a.getSelectedHandles(), this.addObjectAndPointsToUndoRedoAction(a.shapeRef, b), this.selectedHandles = this.selectedHandles.concat(b)
};
_.translateSelectedPoints = function(a, b) {
    for (var c = null, d = null, e = this.selectedHandles.length, f = 0; f < e; f++) c = this.selectedHandles[f], d = c.parent.shapeRef.globalToLocal({
        x: c.x + a,
        y: c.y + b
    }), c.pointRef.x = d.x, c.pointRef.y = d.y, c.parent.shapeRef.invalidateBounds()
};
_.translateClickedControlPoint = function(a, b) {
    var c = this.clickedHandle.parent.shapeRef.globalToLocal({
        x: this.clickedHandle.x + a,
        y: this.clickedHandle.y + b
    });
    this.clickedHandle.pointRef.x = c.x;
    this.clickedHandle.pointRef.y = c.y;
    this.clickedHandle.parent.shapeRef.invalidateBounds()
};
_.addObjectAndPointsToUndoRedoAction = function(a, b) {
    for (var c = [], d = b.length, e = 0; e < d; e++) c.push(b[e].pointRef);
    this.undoAction.addObjectAndPointsToAction(a, c);
    this.redoAction.addObjectAndPointsToAction(a, c)
};
ExtendClass(XOS_ScaleBoundsHandle2D_tool, XOS_BaseTool_tool);

function XOS_ScaleBoundsHandle2D_tool(a) {
    XOS_ScaleBoundsHandle2D_tool.baseConstructor.call(this, a);
    this.clickedHandle = null;
    this.clickedHandle_id = -1;
    this.boundsInteraction = null;
    this.maintainAspectRatio = !1;
    this.endBounds = this.previousBounds = this.startBounds = null;
    this.pivotPos = new XOS_Point2D(0, 0);
    this.pivotPos = new XOS_Point2D(0, 0);
    this.pivotCentered = !1;
    this.onDragOverInstanceCursor = this.onDragCursor = "boundingHandle_cur";
    this.onDragOverVertexCursor = "boundingHandleDragOverVertex_cur";
    this.savedGlobalTransform =
        null
}
_ = XOS_ScaleBoundsHandle2D_tool.prototype;
_.onMouseDown = function(a) {
    this.displayObjectEditorRef.tools.activeTool = this;
    GLOBAL_MOUSE_LISTENER.objectTarget = this
};
_.onGestureEnd = function(a) {};
_.onGestureChange = function(a) {};
_.exitByGesture = function() {};
_.onGlobalMouseStartDrag = function(a) {
    this.displayObjectEditorRef.setCursor(this.onDragCursor);
    this.clickedHandle.isSelected = !0;
    this.boundsInteraction = this.clickedHandle.parent;
    this.boundsInteraction.setInteractionHandlesVisibility(!1);
    this.clickedHandle_id = this.boundsInteraction.getChildIndex(this.clickedHandle);
    this.startBounds = this.boundsInteraction.bounds.clone();
    this.previousBounds = this.boundsInteraction.bounds.clone();
    this.endBounds = this.boundsInteraction.bounds.clone();
    this.savedGlobalTransform =
        null;
    this.boundsInteraction.isLocalBounds && (this.savedGlobalTransform = this.boundsInteraction.displayObject2DRef.globalTransform.clone());
    a.altKey ? (this.pivotCentered = !0, this.pivotPos = this.startBounds.getCenter()) : (this.pivotCentered = !1, a = 4 > this.clickedHandle_id ? this.clickedHandle_id + 4 : this.clickedHandle_id - 4, this.pivotPos.x = this.boundsInteraction.children[a].x, this.pivotPos.y = this.boundsInteraction.children[a].y)
};
_.onGlobalMouseDrag = function(a) {
    var b = this.displayObjectEditorRef.mousePointToPenInfo(this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt), this.savedGlobalTransform, {
            snapToVertices: !0,
            snapToContour: !0
        }, !0),
        c = b.position;
    b.snappedPosition ? (c = b.snappedPosition, this.boundsInteraction.isLocalBounds && (c = this.savedGlobalTransform.clone().invert().transformPoint(c)), this.displayObjectEditorRef.setCursor(this.onDragOverVertexCursor)) : this.displayObjectEditorRef.setCursor(this.onDragCursor);
    this.maintainAspectRatio = a.shiftKey ? !0 : !1;
    this.doMouseDragAction(c)
};
_.onGlobalMouseEndDrag = function(a) {
    this.displayObjectEditorRef.setCursor(this.defaultCursor);
    a = this.displayObjectEditorRef.selectionList;
    var b = this.endBounds.getWidth() / this.startBounds.getWidth(),
        c = this.endBounds.getHeight() / this.startBounds.getHeight();
    this.displayObjectEditorRef.createUndoRedo_scale(a, b, c, this.endPivotPos, this.boundsInteraction.isLocalBounds);
    this.boundsInteraction.setInteractionHandlesVisibility(!0);
    this.displayObjectEditorRef.invalidateRender();
    this.displayObjectEditorRef.interactionDisplay.invalidateRender();
    this.displayObjectEditorRef.normalizeGraphicObjects(a);
    this.displayObjectEditorRef.onChange();
    this.displayObjectEditorRef.showSelectionProperties()
};
_.onGlobalMouseUp = function(a) {
    this.clickedHandle.isSelected = !1;
    this.displayObjectEditorRef.setActiveToolByName("selectGlobal_tool")
};
_.doMouseDragAction = function(a) {
    this.endPivotPos = this.pivotPos.clone();
    if (this.pivotCentered) {
        switch (this.clickedHandle_id) {
            case 0:
                this.endBounds.min.x = a.x;
                this.endBounds.min.y = a.y;
                this.endBounds.max.x = this.endPivotPos.x + (this.endPivotPos.x - this.endBounds.min.x);
                this.endBounds.max.y = this.endPivotPos.y + (this.endPivotPos.y - this.endBounds.min.y);
                break;
            case 1:
                this.endBounds.min.y = a.y;
                this.endBounds.max.y = this.endPivotPos.y + (this.endPivotPos.y - this.endBounds.min.y);
                break;
            case 2:
                this.endBounds.max.x = a.x;
                this.endBounds.min.y = a.y;
                this.endBounds.max.y = this.endPivotPos.y + (this.endPivotPos.y - this.endBounds.min.y);
                this.endBounds.min.x = this.endPivotPos.x + (this.endPivotPos.x - this.endBounds.max.x);
                break;
            case 3:
                this.endBounds.max.x = a.x;
                this.endBounds.min.x = this.endPivotPos.x + (this.endPivotPos.x - this.endBounds.max.x);
                break;
            case 4:
                this.endBounds.max.x = a.x;
                this.endBounds.max.y = a.y;
                this.endBounds.min.x = this.endPivotPos.x + (this.endPivotPos.x - this.endBounds.max.x);
                this.endBounds.min.y = this.endPivotPos.y + (this.endPivotPos.y -
                    this.endBounds.max.y);
                break;
            case 5:
                this.endBounds.max.y = a.y;
                this.endBounds.min.y = this.endPivotPos.y + (this.endPivotPos.y - this.endBounds.max.y);
                break;
            case 6:
                this.endBounds.min.x = a.x;
                this.endBounds.max.y = a.y;
                this.endBounds.max.x = this.endPivotPos.x + (this.endPivotPos.x - this.endBounds.min.x);
                this.endBounds.min.y = this.endPivotPos.y + (this.endPivotPos.y - this.endBounds.max.y);
                break;
            case 7:
                this.endBounds.min.x = a.x, this.endBounds.max.x = this.endPivotPos.x + (this.endPivotPos.x - this.endBounds.min.x)
        }
        this.boundsInteraction.isLocalBounds &&
            (this.endPivotPos = this.savedGlobalTransform.transformPoint(this.endPivotPos))
    } else switch (this.clickedHandle_id) {
        case 0:
            this.endBounds.min.x = a.x;
            this.endBounds.min.y = a.y;
            break;
        case 1:
            this.endBounds.min.y = a.y;
            break;
        case 2:
            this.endBounds.max.x = a.x;
            this.endBounds.min.y = a.y;
            break;
        case 3:
            this.endBounds.max.x = a.x;
            break;
        case 4:
            this.endBounds.max.x = a.x;
            this.endBounds.max.y = a.y;
            break;
        case 5:
            this.endBounds.max.y = a.y;
            break;
        case 6:
            this.endBounds.min.x = a.x;
            this.endBounds.max.y = a.y;
            break;
        case 7:
            this.endBounds.min.x =
                a.x
    }
    if (this.boundsInteraction.maintainAspectRatio || this.maintainAspectRatio) 5 == this.clickedHandle_id || 1 == this.clickedHandle_id ? (a = this.startBounds.getWidth() / this.startBounds.getHeight(), this.endBounds.setWidth(this.endBounds.getHeight() * a)) : (a = this.startBounds.getHeight() / this.startBounds.getWidth(), this.endBounds.setHeight(this.endBounds.getWidth() * a));
    a = this.endBounds.getWidth() / this.previousBounds.getWidth();
    var b = this.endBounds.getHeight() / this.previousBounds.getHeight();
    isNaN(a) || isNaN(b) ||
        Infinity == a || -Infinity == a || Infinity == b || -Infinity == b || 0 == a || 0 == b || (this.displayObjectEditorRef.scaleGraphicObjects(this.displayObjectEditorRef.selectionList, a, b, this.endPivotPos, this.boundsInteraction.isLocalBounds), this.displayObjectEditorRef.interactionDisplay.invalidateRender(), this.previousBounds = this.endBounds.clone())
};
ExtendClass(XOS_RotateBoundsHandle2D_tool, XOS_BaseTool_tool);

function XOS_RotateBoundsHandle2D_tool(a) {
    XOS_RotateBoundsHandle2D_tool.baseConstructor.call(this, a);
    this.endRotation = this.startRotation = this.previousRotation = 0;
    this.centerOfRotation = null;
    this.onDragCursor = this.defaultOverHandleCursor = this.defaultOverVertexCursor = this.defaultOverContourCursor = this.defaultOverInstanceCursor = this.defaultCursor = "rotate_cur"
}
_ = XOS_RotateBoundsHandle2D_tool.prototype;
_.onMouseDown = function(a) {
    this.displayObjectEditorRef.tools.activeTool = this;
    GLOBAL_MOUSE_LISTENER.objectTarget = this;
    this.boundsInteraction = this.clickedHandle.parent;
    this.centerOfRotation = this.boundsInteraction.bounds.getCenter();
    this.boundsInteraction.isLocalBounds && (this.centerOfRotation = this.boundsInteraction.displayObject2DRef.localToGlobal(this.centerOfRotation))
};
_.onGlobalMouseStartDrag = function(a) {
    this.boundsInteraction.visible = !1;
    var b = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseDownPt);
    this.pt1.x = b.x;
    this.pt1.y = b.y;
    this.pt2.x = b.x;
    this.pt2.y = b.y;
    this.previousRotation = this.endRotation = this.startRotation = this.pt2.getAngleFromPoint(this.centerOfRotation);
    b = !1;
    if (a.altKey || this.displayObjectEditorRef.isAltKeyPressed) b = !0;
    b && this.displayObjectEditorRef.duplicateAllSelectedGraphicObjects_undable()
};
_.onGlobalMouseDrag = function(a) {
    var b = this.displayObjectEditorRef.mousePointToPenInfo(this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt), null, {
        snapToVertices: !0,
        snapToContour: !0
    }, !0).position;
    this.pt2.x = b.x;
    this.pt2.y = b.y;
    this.endRotation = this.pt2.getAngleFromPoint(this.centerOfRotation);
    a.shiftKey && (a = Math.PI / 4, this.endRotation = this.startRotation + Math.round((this.endRotation - this.startRotation) / a) * a);
    for (a = this.endRotation - this.previousRotation; 0 >
        a;) a += 2 * Math.PI;
    this.displayObjectEditorRef.rotateGraphicObjects(this.displayObjectEditorRef.selectionList, a, this.centerOfRotation);
    this.displayObjectEditorRef.interactionDisplay.invalidateRender();
    this.previousRotation = this.endRotation
};
_.onGlobalMouseEndDrag = function(a) {
    this.boundsInteraction.visible = !0;
    this.displayObjectEditorRef.onChange();
    this.displayObjectEditorRef.invalidateRender();
    this.displayObjectEditorRef.interactionDisplay.invalidateRender();
    this.displayObjectEditorRef.createUndoRedo_rotation(this.displayObjectEditorRef.selectionList, this.endRotation - this.startRotation, this.centerOfRotation);
    this.displayObjectEditorRef.showSelectionProperties()
};
_.onGlobalMouseUp = function(a) {
    this.displayObjectEditorRef.setActiveToolByName("selectGlobal_tool")
};
_.onGestureEnd = function(a) {};
_.onGestureChange = function(a) {};
_.exitByGesture = function() {};
ExtendClass(XOS_Select_tool, XOS_BaseTool_tool);

function XOS_Select_tool(a) {
    XOS_Select_tool.baseConstructor.call(this, a);
    this.rectTemp = {
        x: 0,
        y: 0,
        width: 0,
        height: 0
    };
    this.clickedObject = null;
    this.enableMultipleSelection = !1;
    this.interactionMode = "boundsInteraction";

}
_ = XOS_Select_tool.prototype;
_.onActivate = function() {
    "pointsInteraction" == this.interactionMode ? (this.defaultCursor = "arrowWhite_cur", this.defaultOverInstanceCursor = "arrowWhiteOverInstance_cur", this.defaultOverHandleCursor = this.defaultOverVertexCursor = "arrowWhiteOverVertex_cur") : (this.defaultCursor = "default_cur", this.defaultOverInstanceCursor = "arrowBlackOverInstance_cur", this.defaultOverVertexCursor = "arrowBlackOverVertex_cur", this.defaultOverHandleCursor = "boundingHandle_cur");
    this.defaultOverContourCursor = this.defaultOverVertexCursor;
    this.displayObjectEditorRef.setInteractionMode(this.interactionMode, "enabled");
    this.displayObjectEditorRef.setCursor(this.defaultCursor);
    this.enableMultipleSelection = !1
};
_.onMouseDown = function(a) {
    this.clickedObject = null;
    var b = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt),
        c = this.displayObjectEditorRef.interactionDisplay.hitTestAll(b);
    if (c.hit) c.instance.onMouseDown(a);
    else {
        c = this.displayObjectEditorRef.currentLayer.isolateObjectRef ? this.displayObjectEditorRef.currentLayer.hitTestObject(b, 5) : this.displayObjectEditorRef.hitTestAll(b, 5);
        if (c.hit)
            if (c.instanceGroup && "boundsInteraction" == this.displayObjectEditorRef.interactionMode) {
                if (this.clickedObject =
                    c.instanceGroup, this.isDoubleClick()) {
                    this.displayObjectEditorRef.editGraphicObjectInIsolateLayer(this.clickedObject);
                    this.displayObjectEditorRef.deselectAllGraphicObjects();
                    return
                }
            } else this.clickedObject = c.instance;
        b = !1;
        if (a.shiftKey || this.displayObjectEditorRef.isShiftKeyPressed) b = !0;
        if (null != this.clickedObject) !0 == this.clickedObject.isSelected ? b ? this.displayObjectEditorRef.deselectGraphicObject(this.clickedObject) : this.displayObjectEditorRef.showSelectionProperties() : (b || this.displayObjectEditorRef.deselectAllGraphicObjects(),
            this.displayObjectEditorRef.selectGraphicObject(this.clickedObject, !0), this.displayObjectEditorRef.showSelectionProperties());
        else if (b || this.displayObjectEditorRef.deselectAllGraphicObjects(), this.isDoubleClick()) {
            this.displayObjectEditorRef.currentLayer.isolateObjectRef && this.displayObjectEditorRef.exitFromIsolateLayerEditing();
            return
        }
        GLOBAL_MOUSE_LISTENER.objectTarget = this
    }
};
_.onGlobalMouseStartDrag = function(a) {
    var b = !1;
    if (a.altKey || this.displayObjectEditorRef.isAltKeyPressed) b = !0;
    null != this.clickedObject && (b && (this.displayObjectEditorRef.duplicateAllSelectedGraphicObjects_undable(), this.displayObjectEditorRef.tools.translateObjects_tool.saveActionToUndoRedoHistory = !1), this.displayObjectEditorRef.tools.translateObjects_tool.onGlobalMouseStartDrag(a))
};
_.onGlobalMouseDrag = function(a) {
    this.clearInteractiveRect();
    this.drawInteractiveRect()
};
_.onGlobalMouseDragStartPause = function(a) {};
_.onGlobalMouseEndDrag = function(a) {
    this.clearInteractiveRect();
    a = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseDownPt);
    var b = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt);
    this.displayObjectEditorRef.selectObjectsByRect(a.x, a.y, b.x - a.x, b.y - a.y)
};
_.onGlobalMouseDoubleClick = function(a) {
    this.clickedObject instanceof XOS_Text && this.displayObjectEditorRef.editGraphicObjectText(this.clickedObject)
};
_.drawInteractiveRect = function() {
    var a = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseDownPt),
        b = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt);
    this.rectTemp.x = a.x;
    this.rectTemp.y = a.y;
    this.rectTemp.width = b.x - a.x;
    this.rectTemp.height = b.y - a.y;
    this.interactionCanvasContext.setTransform(1, 0, 0, 1, 0, 0);
    this.interactionCanvasContext.globalCompositeOperation = "source-over";
    this.interactionCanvasContext.lineWidth =
        1;
    this.interactionCanvasContext.strokeStyle = "#4e4e4e";
    this.interactionCanvasContext.beginPath();
    this.interactionCanvasContext.rect(this.rectTemp.x, this.rectTemp.y, this.rectTemp.width, this.rectTemp.height);
    this.interactionCanvasContext.stroke();
    this.interactionCanvasContext.closePath()
};
_.clearInteractiveRect = function() {
    this.interactionCanvasContext.setTransform(1, 0, 0, 1, 0, 0);
    this.interactionCanvasContext.lineWidth = 3;
    this.interactionCanvasContext.strokeStyle = "rgba(0, 0, 0, 0)";
    this.interactionCanvasContext.globalCompositeOperation = "source-out";
    this.interactionCanvasContext.beginPath();
    this.interactionCanvasContext.rect(this.rectTemp.x, this.rectTemp.y, this.rectTemp.width, this.rectTemp.height);
    this.interactionCanvasContext.stroke();
    this.interactionCanvasContext.closePath()
};
ExtendClass(XOS_DrawLine_tool, XOS_BaseTool_tool);

function XOS_DrawLine_tool(a) {
    XOS_DrawLine_tool.baseConstructor.call(this, a);
    this.defaultOverInstanceCursor = this.defaultCursor = "line_cur";
    this.defaultOverContourCursor = this.defaultOverHandleCursor = this.defaultOverVertexCursor = "lineOverVertex_cur";
    this.onDragCursor = "crosshair_cur";
    this.onDragOverVertexCursor = "crosshairOverVertex_cur";
    //setIconToShape("img/line-2.png");
}
_ = XOS_DrawLine_tool.prototype;
_.onMouseDown = function(a) {
    this.displayObjectEditorRef.getCurrentLayer().isLocked ? alert("The Layer target is locked!") : (XOS_DrawLine_tool.superClass.onMouseDown.call(this, a), this.displayObjectEditorRef.deselectAllGraphicObjects())
};
_.onGlobalMouseStartDrag = function(a) {
    this.displayObjectEditorRef.setCursor(this.onDragCursor)
};
_.onGlobalMouseDrag = function(a) {
    a.shiftKey && GLOBAL_MOUSE_LISTENER.calculateSnapToAngle();
    a = this.displayObjectEditorRef.mousePointToPenInfo(this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt), null, {
        snapToVertices: !0,
        snapToContour: !0
    }, !1);
    a.snappedPosition ? (a = a.snappedPosition, this.displayObjectEditorRef.setCursor(this.onDragOverVertexCursor)) : (a = a.position, this.displayObjectEditorRef.setCursor(this.onDragCursor));
    this.drawInteractivePath("clear");
    this.pt2.x = a.x;
    this.pt2.y = a.y;
    this.drawInteractivePath()
};
_.onGlobalMouseEndDrag = function(a) {
    this.displayObjectEditorRef.setCursor(this.defaultCursor);
    this.drawInteractivePath("clear");
    this.createGraphicObject()
};
_.drawInteractiveMode = function(a) {
    this.interactionCanvasContext.setTransform(1, 0, 0, 1, 0, 0);
    "clear" == a ? (this.interactionCanvasContext.lineWidth = 4, this.interactionCanvasContext.strokeStyle = "rgba(0, 0, 0, 0)", this.interactionCanvasContext.globalCompositeOperation = "source-out") : (this.interactionCanvasContext.lineWidth = 1, this.interactionCanvasContext.strokeStyle = "#2E87FF", this.interactionCanvasContext.globalCompositeOperation = "source-over")
};
_.drawInteractivePath = function(a) {
    this.drawInteractiveMode(a);
    this.interactionCanvasContext.beginPath();
    this.interactionCanvasContext.moveTo(this.pt1.x, this.pt1.y);
    this.interactionCanvasContext.lineTo(this.pt2.x, this.pt2.y);
    this.interactionCanvasContext.stroke();
    this.interactionCanvasContext.closePath()
};
_.createGraphicObject = function(a) {
    a = new XOS_Shape;
    var b = new XOS_Path;
    a.paths.add(b);
    this.pt1 = this.displayObjectEditorRef.content.globalToLocal(this.pt1);
    this.pt2 = this.displayObjectEditorRef.content.globalToLocal(this.pt2);
    b.addPoint(new XOS_MoveTo(0, 0));
    b.addPoint(new XOS_LineTo(this.pt2.x - this.pt1.x, this.pt2.y - this.pt1.y));
    a.setPosition(this.pt1.x, this.pt1.y);
    this.addGraphicObjectToDocument(a)
};
_.addGraphicObjectToDocument = function(a) {
    this.displayObjectEditorRef.globalPropertyToGraphicObject(a);
    this.displayObjectEditorRef.addGraphicObject_undable(a, null, -1);
    this.displayObjectEditorRef.invalidateRender();
    this.displayObjectEditorRef.selectGraphicObject(a, !0);
    this.displayObjectEditorRef.showSelectionProperties()
};
ExtendClass(XOS_DrawRectangle_tool, XOS_DrawLine_tool);

function XOS_DrawRectangle_tool(a) {
    XOS_DrawRectangle_tool.baseConstructor.call(this, a);
    this.defaultOverInstanceCursor = this.defaultCursor = "rect_cur";
    this.defaultOverContourCursor = this.defaultOverHandleCursor = this.defaultOverVertexCursor = "rectOverVertex_cur";
    this.cornerRadius = 0;
    $("#SELECT_GLOBAL_2D_toolButton").trigger("click",function(){alert("select object clicked !");});
}
_ = XOS_DrawRectangle_tool.prototype;
_.drawInteractivePath = function(a) {
    this.drawInteractiveMode(a);
    this.interactionCanvasContext.beginPath();
    0 < this.cornerRadius ? this.interactionCanvasContext.roundedRect(this.pt1.x, this.pt1.y, this.pt2.x - this.pt1.x, this.pt2.y - this.pt1.y, this.cornerRadius) : this.interactionCanvasContext.rect(this.pt1.x, this.pt1.y, this.pt2.x - this.pt1.x, this.pt2.y - this.pt1.y);
    this.interactionCanvasContext.stroke();
    this.interactionCanvasContext.closePath()
};
_.createGraphicObject = function(a) {
    a = new XOS_Shape;
    this.pt1 = this.displayObjectEditorRef.content.globalToLocal(this.pt1);
    this.pt2 = this.displayObjectEditorRef.content.globalToLocal(this.pt2);
    var b = XOS_Path.createRectangle(0, 0, this.pt2.x - this.pt1.x, this.pt2.y - this.pt1.y, this.cornerRadius);
    a.paths.add(b);
    a.setPosition(this.pt1.x, this.pt1.y);
    this.addGraphicObjectToDocument(a)
};
_.showToolProperties = function() {
    var a = prompt("Corner radius", this.cornerRadius);
    null != a && (this.cornerRadius = parseFloat(a))
};
_.onDoubleClick = function() {
    this.showToolProperties()
};
ExtendClass(XOS_DrawEllipse_tool, XOS_DrawLine_tool);

function XOS_DrawEllipse_tool(a) {
    XOS_DrawEllipse_tool.baseConstructor.call(this, a);
    this.defaultOverInstanceCursor = this.defaultCursor = "ellipse_cur";
    this.defaultOverContourCursor = this.defaultOverHandleCursor = this.defaultOverVertexCursor = "ellipseOverVertex_cur"
   // setIconToShape("img/circle2.png");
}
_ = XOS_DrawEllipse_tool.prototype;
_.drawInteractivePath = function(a) {
    this.drawInteractiveMode(a);
    this.interactionCanvasContext.beginPath();
    this.interactionCanvasContext.drawEllipse(this.pt1.x, this.pt1.y, this.pt2.x - this.pt1.x, this.pt2.y - this.pt1.y);
    this.interactionCanvasContext.stroke();
    this.interactionCanvasContext.closePath()
};
_.createGraphicObject = function(a) {
    a = new XOS_Shape;
    this.pt1 = this.displayObjectEditorRef.content.globalToLocal(this.pt1);
    this.pt2 = this.displayObjectEditorRef.content.globalToLocal(this.pt2);
    var b = XOS_Path.createEllipse(0, 0, this.pt2.x - this.pt1.x, this.pt2.y - this.pt1.y);
    a.paths.add(b);
    a.setPosition(this.pt1.x, this.pt1.y);
    this.addGraphicObjectToDocument(a)
};
ExtendClass(XOS_DrawPath_tool, XOS_BaseTool_tool);

function XOS_DrawPath_tool(a) {
    XOS_DrawPath_tool.baseConstructor.call(this, a);
    this.currentPath = this.currentShape = null;
    this.defaultOverInstanceCursor = this.onDragCursor = this.defaultCursor = "path_cur";
    this.defaultOverContourCursor = this.defaultOverHandleCursor = this.defaultOverVertexCursor = "pathOverVertex_cur";
    this.clickedHandle = this.currentAddedPoint = null;
    this.stopCurveContinuity = !1
}
_ = XOS_DrawPath_tool.prototype;
_.onActivate = function() {
    this.displayObjectEditorRef.setCursor(this.defaultCursor);
    this.currentPath = null;
    this.displayObjectEditorRef.setInteractionMode("pointsInteraction", "disabled")
};
_.onDeactivate = function() {
    this.currentPath && (this.displayObjectEditorRef.invalidateRender(), this.displayObjectEditorRef.selectGraphicObject(this.currentShape, !0), this.displayObjectEditorRef.showSelectionProperties());
    this.currentPath = null
};
_.onMouseDown = function(a) {
    this.displayObjectEditorRef.getCurrentLayer().isLocked ? alert("The Layer target is locked!") : (XOS_DrawPath_tool.superClass.onMouseDown.call(this, a), this.displayObjectEditorRef.setCursor(this.onDragCursor), this.displayObjectEditorRef.deselectAllGraphicObjects(), this.pt1 = this.displayObjectEditorRef.content.globalToLocal(this.pt1))
};
_.onGlobalMouseUp = function(a) {
    this.stopCurveContinuity = !1;
    this.currentPath && !1 == GLOBAL_MOUSE_LISTENER.isDragAction && (this.clickedHandle ? this.clickedHandle.pointRef == this.currentPath.getLastPoint() ? this.stopCurveContinuity = !0 : this.clickedHandle.pointRef == this.currentPath.getFirstPoint() && (this.currentPath.closePath = !0, this.addClosePathPoint_undable(this.createNewPoint("lineTo"))) : this.addNewPoint_undable(this.createNewPoint("lineTo")));
    null == this.currentPath && (this.addNewShape_undable(), this.displayObjectEditorRef.setInteractionMode("pointsInteraction",
        "enabled"));
    this.displayObjectEditorRef.selectGraphicObject(this.currentShape, !0);
    this.displayObjectEditorRef.showSelectionProperties();
    this.displayObjectEditorRef.invalidateRender()
};
_.onGlobalMouseStartDrag = function(a) {
    null == this.currentPath ? (this.addNewShape_undable(), this.currentPath.replacePointAt(this.createNewPoint("cubicCurveTo"), 0), this.currentAddedPoint = this.currentPath.points[0]) : this.clickedHandle ? (this.clickedHandle.pointRef == this.currentPath.getFirstPoint() && (this.currentPath.closePath = !0, this.addClosePathPoint_undable(this.createNewPoint("cubicCurveTo"))), this.clickedHandle.pointRef == this.currentPath.getLastPoint() && (this.currentAddedPoint = this.currentPath.getLastPoint())) :
        this.addNewPoint_undable(this.createNewPoint("cubicCurveTo"))
};
_.onGlobalMouseDrag = function(a) {
    a = this.displayObjectEditorRef.mousePointToPenInfo(this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt));
    this.pt2.x = a.position.x;
    this.pt2.y = a.position.y;
    this.pt2 = this.displayObjectEditorRef.content.globalToLocal(this.pt2);
    a = this.pt1.x - (this.pt2.x - this.pt1.x);
    var b = this.pt1.y - (this.pt2.y - this.pt1.y);
    this.currentAddedPoint instanceof XOS_QuadraticCurveTo ? this.currentAddedPoint.controlPt.set(a, b) : this.currentAddedPoint instanceof
    XOS_CubicCurveTo && this.currentAddedPoint.controlPt2.set(a, b);
    this.drawInteractiveEditing()
};
_.drawInteractiveEditing = function() {
    var a = this.displayObjectEditorRef.interactionDisplay.graphicContext;
    this.displayObjectEditorRef.interactionDisplay.clear();
    a.lineWidth = 1;
    a.strokeStyle = "#2E87FF";
    a.fillStyle = "#FFFFFF";
    var b = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseDownPt),
        c = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt);
    this.currentShape.drawAsSelected();
    a.setTransform(1, 0, 0, 1, 0, 0);
    a.beginPath();
    var d = b.x - (c.x - b.x),
        e = b.y - (c.y - b.y);
    a.moveTo(d, e);
    a.lineTo(c.x, c.y);
    a.stroke();
    a.beginPath();
    DEVICE_WITH_TOUCH_EVENTS ? (a.rect(b.x - 4, b.y - 4, 8, 8), a.drawEllipse(d - 4, e - 4, 8, 8), a.drawEllipse(c.x - 4, c.y - 4, 8, 8)) : (a.rect(b.x - 2.5, b.y - 2.5, 5, 5), a.drawEllipse(d - 3, e - 3, 6, 6), a.drawEllipse(c.x - 3, c.y - 3, 6, 6));
    a.fill();
    a.stroke()
};
_.createNewPoint = function(a) {
    var b = null;
    "lineTo" == a ? b = new XOS_LineTo(this.pt1.x, this.pt1.y) : "quadraticCurveTo" == a ? b = new XOS_QuadraticCurveTo(this.pt2.x, this.pt2.y, this.pt1.x, this.pt1.y) : "cubicCurveTo" == a && (b = this.currentPath.points[this.currentPath.points.length - 1], b instanceof XOS_CubicCurveTo ? this.stopCurveContinuity ? (b = new XOS_CubicCurveTo(b.x, b.y, this.pt2.x, this.pt2.y, this.pt1.x, this.pt1.y), b.controlPt1.collapsed = !0) : (a = b.x - (b.controlPt2.x - b.x), b = b.y - (b.controlPt2.y - b.y), b = new XOS_CubicCurveTo(a,
        b, this.pt2.x, this.pt2.y, this.pt1.x, this.pt1.y)) : b instanceof XOS_QuadraticCurveTo ? this.stopCurveContinuity ? (b = new XOS_CubicCurveTo(b.x, b.y, this.pt2.x, this.pt2.y, this.pt1.x, this.pt1.y), b.controlPt1.collapsed = !0) : (a = b.x - (b.controlPt.x - b.x), b = b.y - (b.controlPt.y - b.y), b = new XOS_CubicCurveTo(a, b, this.pt2.x, this.pt2.y, this.pt1.x, this.pt1.y)) : b = new XOS_QuadraticCurveTo(this.pt2.x, this.pt2.y, this.pt1.x, this.pt1.y));
    return b
};
_.addNewShape_undable = function() {
    this.currentShape = new XOS_Shape;
    this.currentPath = new XOS_Path;
    this.currentShape.paths.add(this.currentPath);
    this.currentAddedPoint = new XOS_MoveTo(this.pt1.x, this.pt1.y);
    this.currentPath.addPoint(this.currentAddedPoint);
    this.displayObjectEditorRef.globalPropertyToGraphicObject(this.currentShape);
    this.displayObjectEditorRef.addGraphicObject_undable(this.currentShape, null, -1)
};
_.addNewPoint_undable = function(a) {
    function b() {
        d.deselectAllGraphicObjects();
        f.addPointAt(a, f.points.length);
        f.closePath = g;
        d.selectGraphicObject(e, !0);
        d.showSelectionProperties();
        d.invalidateRender()
    }

    function c() {
        d.deselectAllGraphicObjects();
        f.removePointAt(f.points.length - 1);
        f.closePath = g;
        d.selectGraphicObject(e, !0);
        d.showSelectionProperties();
        d.invalidateRender()
    }
    if (this.currentShape.displayRef) {
        this.currentPath.addPoint(a);
        this.currentAddedPoint = a;
        this.currentShape.invalidateBounds();
        var d = this.displayObjectEditorRef,
            e = this.currentShape,
            f = this.currentPath,
            g = f.closePath,
            h = new XOS_Action(c),
            k = new XOS_Action(b);
        this.displayObjectEditorRef.history.addUndoRedoActions(h, k)
    } else this.addNewShape_undable()
};
_.addClosePathPoint_undable = function(a) {
    this.currentAddedPoint = a;
    this.currentShape.invalidateBounds();
    var b = this.currentPath.replacePointAt(a, 0),
        c = this.displayObjectEditorRef,
        d = this.currentPath,
        e = this.currentShape,
        f = new XOS_Action(function() {
            c.deselectAllGraphicObjects();
            d.replacePointAt(b, 0);
            d.closePath = !1;
            c.selectGraphicObject(e, !0);
            c.showSelectionProperties();
            c.invalidateRender()
        }),
        g = new XOS_Action(function() {
            c.deselectAllGraphicObjects();
            d.replacePointAt(a, 0);
            d.closePath = !0;
            c.selectGraphicObject(e, !0);
            c.showSelectionProperties();
            c.invalidateRender()
        });
    this.displayObjectEditorRef.history.addUndoRedoActions(f, g)
};
ExtendClass(XOS_DrawFreeHand_tool, XOS_BaseTool_tool);

function XOS_DrawFreeHand_tool(a) {
    XOS_DrawFreeHand_tool.baseConstructor.call(this, a);
    this.draftPoints = null;
    this.defaultOverContourCursor = this.defaultOverInstanceCursor = this.defaultOverHandleCursor = this.defaultOverVertexCursor = this.defaultCursor = "pen_cur";
    this.onDragCursor = "crosshair_cur";
    this.onDragOverVertexCursor = "crosshairOverVertex_cur"
}
_ = XOS_DrawFreeHand_tool.prototype;
_.onActivate = function() {
    this.displayObjectEditorRef.setInteractionMode("pointsInteraction", "disabled");
    this.displayObjectEditorRef.setCursor(this.defaultCursor)
};
_.onMouseDown = function(a) {
    this.displayObjectEditorRef.getCurrentLayer().isLocked ? alert("The Layer target is locked!") : XOS_DrawFreeHand_tool.superClass.onMouseDown.call(this, a)
};
_.onGlobalMouseStartDrag = function(a) {
    a = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseDownPt);
    this.displayObjectEditorRef.setCursor(this.onDragCursor);
    this.displayObjectEditorRef.deselectAllGraphicObjects();
    this.interactionCanvasContext.setTransform(1, 0, 0, 1, 0, 0);
    this.interactionCanvasContext.globalAlpha = 1;
    this.interactionCanvasContext.globalCompositeOperation = "source-over";
    this.interactionCanvasContext.lineWidth = 1;
    this.interactionCanvasContext.strokeStyle =
        "#2E87FF";
    this.interactionCanvasContext.beginPath();
    this.interactionCanvasContext.moveTo(a.x, a.y);
    this.draftPoints = [];
    this.draftPoints.push(new XOS_Point2D(a.x, a.y))
};
_.onGlobalMouseDrag = function(a) {
    a = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt);
    this.interactionCanvasContext.lineTo(a.x, a.y);
    this.interactionCanvasContext.stroke();
    this.draftPoints.push(new XOS_Point2D(a.x, a.y))
};
_.onGlobalMouseEndDrag = function(a) {
    this.displayObjectEditorRef.setCursor(this.defaultCursor);
    this.createGraphicObject()
};
_.createGraphicObject = function() {
    var a = (new PathSmoother(this.draftPoints, 10)).fit(),
        b = a.getEditablePoints();
    this.displayObjectEditorRef.content.globalPointsToLocalPoints(b, !0);
    b = new XOS_Shape;
    b.paths.add(a);
    this.displayObjectEditorRef.globalPropertyToGraphicObject(b);
    this.displayObjectEditorRef.addGraphicObject_undable(b, null, -1);
    this.displayObjectEditorRef.invalidateRender();
    this.displayObjectEditorRef.selectGraphicObject(b, !0);
    this.displayObjectEditorRef.showSelectionProperties();
    this.draftPoints = []
};
ExtendClass(XOS_DrawBrush_tool, XOS_DrawFreeHand_tool);

function XOS_DrawBrush_tool(a) {
    XOS_DrawBrush_tool.baseConstructor.call(this, a);
    this.thickness = 5
}
_ = XOS_DrawBrush_tool.prototype;
_.showToolProperties = function() {
    var a = prompt("Thickness", this.thickness);
    null != a && (this.thickness = parseFloat(a))
};
_.onDoubleClick = function() {
    setTimeout(Bind(this, this.showToolProperties), 10)
};
_.createGraphicObject = function() {
    var a = new PathSmoother(this.draftPoints, 10),
        b = a.fit();
    this.displayObjectEditorRef.content.globalPointsToLocalPoints(b.getEditablePoints(), !0);
    a = b.getAsPolygon();
    a = XOS_Path.getPolygonOffset(a.points, this.thickness);
    a = new PathSmoother(a, 10);
    a = a.fit();
    a.revertPoints();
    b.points[0] = new XOS_LineTo(b.points[0].x, b.points[0].y);
    a.points[0] = new XOS_LineTo(a.points[0].x, a.points[0].y);
    b.setPoints(b.points.concat(a.points));
    b.closePath = !0;
    a = new XOS_Shape;
    a.paths.add(b);
    this.displayObjectEditorRef.globalPropertyToGraphicObject(a);
    this.displayObjectEditorRef.addGraphicObject_undable(a, null, -1);
    this.displayObjectEditorRef.selectGraphicObject(a, !0);
    this.displayObjectEditorRef.invalidateRender();
    this.displayObjectEditorRef.showSelectionProperties();
    this.draftPoints = []
};
ExtendClass(XOS_Pan_tool, XOS_BaseTool_tool);

function XOS_Pan_tool(a) {
    XOS_Pan_tool.baseConstructor.call(this, a);
    this.defaultOverInstanceCursor = this.defaultOverContourCursor = this.defaultOverHandleCursor = this.defaultOverVertexCursor = this.defaultCursor = "hand_cur";
    this.onDragCursor = "hand_close_cur";
    this.previousTranslationPt = new XOS_Point2D(0, 0);
    this.currentTranslationPt = new XOS_Point2D(0, 0)
}
_ = XOS_Pan_tool.prototype;
_.onGlobalMouseStartDrag = function(a) {
   // console.log("XOS_Pan_tool onGlobalMouseStartDrag");
    this.displayObjectEditorRef.setCursor(this.onDragCursor);
    this.startContentLocalTransform = this.displayObjectEditorRef.content.localTransform.clone();
    this.previousTranslationPt.x = this.currentTranslationPt.x = GLOBAL_MOUSE_LISTENER.mouseDownPt.x;
    this.previousTranslationPt.y = this.currentTranslationPt.y = GLOBAL_MOUSE_LISTENER.mouseDownPt.y;
    this.displayObjectEditorRef.interactionDisplay.clear()
};
_.onGlobalMouseDrag = function(a) {
    this.currentTranslationPt.x = GLOBAL_MOUSE_LISTENER.mouseMovePt.x;
    this.currentTranslationPt.y = GLOBAL_MOUSE_LISTENER.mouseMovePt.y;
    this.displayObjectEditorRef.content.translate(this.currentTranslationPt.x - this.previousTranslationPt.x, this.currentTranslationPt.y - this.previousTranslationPt.y);
    this.previousTranslationPt.x = this.currentTranslationPt.x;
    this.previousTranslationPt.y = this.currentTranslationPt.y;
    this.displayObjectEditorRef.invalidateRender()
};
_.onGlobalMouseDragStartPause = function(a) {};
_.onGlobalMouseEndDrag = function(a) {
    this.displayObjectEditorRef.interactionDisplay.invalidateRender();
    a = this.displayObjectEditorRef.content.localTransform.clone();
    this.displayObjectEditorRef.saveChangedContentViewToHistory(this.startContentLocalTransform, a);
    this.displayObjectEditorRef.setCursor(this.displayObjectEditorRef.tools.activeTool.defaultCursor)
};
ExtendClass(XOS_Zoom_tool, XOS_BaseTool_tool);

function XOS_Zoom_tool(a) {
    XOS_Zoom_tool.baseConstructor.call(this, a);
    this.scaleStep = .005;
    this.endScaleY = this.endScaleX = 0.50;
    this.pivotPointInPenPos = new XOS_Point2D(0, 0);
    this.defaultOverInstanceCursor = this.defaultOverContourCursor = this.defaultOverHandleCursor = this.defaultOverVertexCursor = this.defaultCursor = this.onDragCursor = "zoom_cur";
    this.previousTranslationPt = new XOS_Point2D(0, 0);
    this.currentTranslationPt = new XOS_Point2D(0, 0);
    this.startContentLocalTransform = null
}
_ = XOS_Zoom_tool.prototype;
_.onDoubleClick = function() {
    this.displayObjectEditorRef.fitToView()
};
_.onGlobalMouseStartDrag = function(a) {
    this.startContentLocalTransform = this.displayObjectEditorRef.content.localTransform.clone();
    a = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseDownPt);
    this.pivotPointInPenPos.x = this.previousTranslationPt.x = this.currentTranslationPt.x = a.x;
    this.pivotPointInPenPos.y = this.previousTranslationPt.y = this.currentTranslationPt.y = a.y;
    this.displayObjectEditorRef.interactionDisplay.clear()
};
_.onGlobalMouseDrag = function(a) {
    a = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt);
    this.currentTranslationPt.x = a.x;
    this.currentTranslationPt.y = a.y;
    a = 1 + (this.currentTranslationPt.y - this.previousTranslationPt.y) * this.scaleStep;
    a = Math.abs(a);
    this.displayObjectEditorRef.scaleGraphicObjects([this.displayObjectEditorRef.content], a, a, this.pivotPointInPenPos, !1);
    this.previousTranslationPt.x = this.currentTranslationPt.x;
    this.previousTranslationPt.y = this.currentTranslationPt.y;
    this.displayObjectEditorRef.invalidateRender()
};
_.onGlobalMouseDragStartPause = function(a) {};
_.onGlobalMouseEndDrag = function(a) {
    this.displayObjectEditorRef.interactionDisplay.invalidateRender();
    a = this.displayObjectEditorRef.content.localTransform.clone();
    this.displayObjectEditorRef.saveChangedContentViewToHistory(this.startContentLocalTransform, a);
     if(this.startContentLocalTransform.a > 3 || this.startContentLocalTransform.a < 0.3)
    {
        this.displayObjectEditorRef.fitToView();
        console.log("canvas is "+this.startContentLocalTransform.a);
    }
};
ExtendClass(XOS_Rotate_tool, XOS_BaseTool_tool);

function XOS_Rotate_tool(a) {
    XOS_Rotate_tool.baseConstructor.call(this, a);
    this.endRotation = this.startRotation = this.previousRotation = 0;
    this.centerOfRotation = null;
    this.defaultOverContourCursor = this.defaultOverInstanceCursor = this.defaultCursor = "rotate_cur";
    this.defaultOverHandleCursor = this.defaultOverVertexCursor = "rotateOverVertex_cur";
    this.onDragCursor = "translateVertex_cur";
    this.onDragOverVertexCursor = "translateVertexOverVertex_cur"
}
_ = XOS_Rotate_tool.prototype;
_.onActivate = function() {
    this.displayObjectEditorRef.setInteractionMode(this.displayObjectEditorRef.interactionMode, "disabled");
    this.displayObjectEditorRef.setCursor(this.defaultCursor);
    var a = this.displayObjectEditorRef.interactionDisplay.children[0];
    a instanceof XOS_BoundsInteraction2D ? (this.centerOfRotation = a.bounds.getCenter(), a.isLocalBounds && (this.centerOfRotation = a.displayObject2DRef.localToGlobal(this.centerOfRotation))) : this.centerOfRotation = this.pt1
};
_.onGlobalMouseStartDrag = function(a) {
    this.displayObjectEditorRef.interactionDisplay.children[0].visible = !1;
    this.previousRotation = this.endRotation = this.startRotation = this.pt2.getAngleFromPoint(this.centerOfRotation);
    var b = !1;
    if (a.altKey || this.displayObjectEditorRef.isAltKeyPressed) b = !0;
    b && this.displayObjectEditorRef.duplicateAllSelectedGraphicObjects_undable()
};
_.onGlobalMouseDrag = function(a) {
    var b = this.displayObjectEditorRef.mousePointToPenInfo(this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt), null, {
        snapToVertices: !0,
        snapToContour: !0
    }, !0);
    b.snappedPosition ? (b = b.snappedPosition, this.displayObjectEditorRef.setCursor(this.onDragOverVertexCursor)) : (b = b.position, this.displayObjectEditorRef.setCursor(this.onDragCursor));
    this.pt2.x = b.x;
    this.pt2.y = b.y;
    this.endRotation = this.pt2.getAngleFromPoint(this.centerOfRotation);
    a.shiftKey && (a = Math.PI / 4, this.endRotation = this.startRotation + Math.round((this.endRotation - this.startRotation) / a) * a);
    this.displayObjectEditorRef.rotateGraphicObjects(this.displayObjectEditorRef.selectionList, this.endRotation - this.previousRotation, this.centerOfRotation);
    this.displayObjectEditorRef.interactionDisplay.invalidateRender();
    this.previousRotation = this.endRotation
};
_.onGlobalMouseEndDrag = function(a) {
    this.displayObjectEditorRef.interactionDisplay.children[0].visible = !0;
    this.displayObjectEditorRef.invalidateRender();
    this.displayObjectEditorRef.interactionDisplay.invalidateRender();
    var b = this.endRotation - this.startRotation;
    this.displayObjectEditorRef.createUndoRedo_rotation(this.displayObjectEditorRef.selectionList, b, this.centerOfRotation);
    this.displayObjectEditorRef.showSelectionProperties();
    var c = this.centerOfRotation;
    this.displayObjectEditorRef.lastTransformAction =
        function() {
            this.rotateGraphicObjects_undable(this.selectionList, b, c)
        }
};
_.onGlobalMouseUp = function(a) {
    !1 == GLOBAL_MOUSE_LISTENER.isDragAction && (this.centerOfRotation.x = this.pt1.x, this.centerOfRotation.y = this.pt1.y)
};
_.onGestureEnd = function(a) {};
_.onGestureChange = function(a) {};
_.exitByGesture = function() {};
ExtendClass(XOS_EyeDropper_tool, XOS_BaseTool_tool);

function XOS_EyeDropper_tool(a) {
    XOS_EyeDropper_tool.baseConstructor.call(this, a);
    this.defaultOverInstanceCursor = this.defaultOverContourCursor = this.defaultOverHandleCursor = this.defaultOverVertexCursor = this.defaultCursor = this.onDragCursor = "eyedropper_cur"
}
XOS_EyeDropper_tool.prototype.onMouseDown = function(a) {
    a = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt);
    var b = this.displayObjectEditorRef.hitTestAll(a);
    b.hit ? "IMAGE" == b.instance.type ? (a = this.displayObjectEditorRef.getColorAtPoint(a), b = XOS_ColorUtils.rgbToHex(a.r / 255, a.g / 255, a.b / 255, !0), console.log(a), console.log(b), this.displayObjectEditorRef.setColorOfSelectedObjects(b, 1)) : (this.displayObjectEditorRef.setPropertiesOfSelectedGraphicObjects_undable(this.displayObjectEditorRef.graphicObjectPropertyToGlobal(b.instance), !0), this.displayObjectEditorRef.showSelectionProperties()) : this.displayObjectEditorRef.setColorOfSelectedObjects("#FFFFFF", 1)
};
ExtendClass(XOS_DrawTextBox_tool, XOS_DrawLine_tool);

function XOS_DrawTextBox_tool(a) {
    XOS_DrawTextBox_tool.baseConstructor.call(this, a);
    this.defaultOverInstanceCursor = this.defaultOverContourCursor = this.defaultOverHandleCursor = this.defaultOverVertexCursor = this.defaultCursor = "textBox_cur";
    this.objectClassCreationType = XOS_TextBox
}
_ = XOS_DrawTextBox_tool.prototype;
_.onActivate = function() {
    this.displayObjectEditorRef.setInteractionMode("boundsInteraction", "disabled");
    this.displayObjectEditorRef.setCursor(this.defaultCursor)
};
_.onMouseDown = function(a) {
    this.displayObjectEditorRef.getCurrentLayer().isLocked ? alert("The Layer target is locked!") : XOS_DrawTextBox_tool.superClass.onMouseDown.call(this, a)
};
_.onGlobalMouseUp = function(a) {
    //console.log("onGlobalMouseUp " + GLOBAL_MOUSE_LISTENER.isDragAction);
    if (!GLOBAL_MOUSE_LISTENER.isDragAction) {
        this.defineStartPenPosition(GLOBAL_MOUSE_LISTENER.mouseMovePt);
        this.pt2.x = this.pt1.x + 200;
        this.pt2.y = this.pt1.y + 20;
        var b = this.createGraphicObject(null),
            c = this.displayObjectEditorRef;
        setTimeout(function() {
            c.editGraphicObjectText(b)
        }, 10)
    }
};
_.drawInteractivePath = function(a) {
    this.drawInteractiveMode(a);
    this.interactionCanvasContext.beginPath();
    this.interactionCanvasContext.rect(this.pt1.x, this.pt1.y, this.pt2.x - this.pt1.x, this.pt2.y - this.pt1.y);
    this.interactionCanvasContext.stroke();
    this.interactionCanvasContext.closePath()
};
_.createGraphicObject = function(a) {
    var b, c, d;
    a = Math.min(this.pt1.x, this.pt2.x);
    b = Math.min(this.pt1.y, this.pt2.y);
    c = Math.max(this.pt1.x, this.pt2.x);
    d = Math.max(this.pt1.y, this.pt2.y);
    this.pt1.set(a, b);
    this.pt2.set(c, d);
    this.pt1 = this.displayObjectEditorRef.content.globalToLocal(this.pt1);
    this.pt2 = this.displayObjectEditorRef.content.globalToLocal(this.pt2);
    a = new this.objectClassCreationType;
    a.setPosition(this.pt1.x, this.pt1.y);
    a.width = this.pt2.x - this.pt1.x;
    a.height = this.pt2.y - this.pt1.y;
    a.setText("Hello text.");
    a.setAlignment("left");
    this.addGraphicObjectToDocument(a);
    this.displayObjectEditorRef.interactionDisplay.invalidateRender();
    a.strokeFillingType = XOS_DisplayObject2D.NONE_FILLING;
    a.strokeColor = "none";
    a.fillFillingType = XOS_DisplayObject2D.DIFFUSE_FILLING;
    a.fillColor = "#000000";
    this.displayObjectEditorRef.setActiveToolByName(null, HTML("SELECT_GLOBAL_2D_toolButton"));
    return a
};
ExtendClass(XOS_EditGradientPoints_tool, XOS_DrawLine_tool);

function XOS_EditGradientPoints_tool(a) {
    XOS_EditGradientPoints_tool.baseConstructor.call(this, a);
    this.defaultOverInstanceCursor = this.defaultCursor = "editGradientPoints_cur";
    this.defaultOverContourCursor = this.defaultOverHandleCursor = this.defaultOverVertexCursor = "editGradientPointsOverVertex_cur"
}
_ = XOS_EditGradientPoints_tool.prototype;
_.onMouseDown = function(a) {
    DEVICE_WITH_TOUCH_EVENTS && this.defineStartPenPosition(GLOBAL_MOUSE_LISTENER.mouseDownPt);
    GLOBAL_MOUSE_LISTENER.objectTarget = this
};
_.createGraphicObject = function(a) {
    this.displayObjectEditorRef.setGradientsPointsOfSelectedGraphicObjects_undable(this.pt1, this.pt2)
};
ExtendClass(XOS_TranslateObjects_tool, XOS_BaseTool_tool);

function XOS_TranslateObjects_tool(a) {
    XOS_TranslateObjects_tool.baseConstructor.call(this, a);
    this.startTranslationPt = new XOS_Point2D(0, 0);
    this.endTranslationPt = new XOS_Point2D(0, 0);
    this.lastTranslationPt = new XOS_Point2D(0, 0);
    this.currentTranslationPt = new XOS_Point2D(0, 0);
    this.onDragCursor = "translateObject_cur";
    this.onDragOverVertexCursor = "translateObjectOverVertex_cur";
    this.saveActionToUndoRedoHistory = !0
}
_ = XOS_TranslateObjects_tool.prototype;
_.onActivate = function() {};
_.onMouseDown = function(a) {};
_.onGlobalMouseStartDrag = function(a) {
    this.displayObjectEditorRef.interactionDisplay.children[0].visible = !1;
    GLOBAL_MOUSE_LISTENER.objectTarget = this;
    this.displayObjectEditorRef.setCursor(this.onDragCursor);
    a = this.displayObjectEditorRef.mousePointToPenInfo(this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseDownPt), null, {
        snapToVertices: !0,
        snapToContour: !0
    }, !1);
    a = null != a.snappedPosition ? a.snappedPosition : a.position;
    this.lastTranslationPt.x = this.startTranslationPt.x =
        a.x;
    this.lastTranslationPt.y = this.startTranslationPt.y = a.y
};
_.onGlobalMouseDrag = function(a) {
    a.shiftKey && GLOBAL_MOUSE_LISTENER.calculateSnapToAngle();
    a = this.displayObjectEditorRef.mousePointToPenInfo(this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt), null, {
        snapToVertices: !0,
        snapToContour: !0
    }, !0);
    var b = a.position;
    a.snappedPosition ? (this.displayObjectEditorRef.setCursor(this.onDragOverVertexCursor), b = a.snappedPosition) : this.displayObjectEditorRef.setCursor(this.onDragCursor);
    this.currentTranslationPt.x = b.x;
    this.currentTranslationPt.y = b.y;
    this.translateObjects()
};
_.onGlobalMouseEndDrag = function(a) {
    this.displayObjectEditorRef.interactionDisplay.children[0].visible = !0;
    this.displayObjectEditorRef.setCursor(this.defaultCursor);
    this.displayObjectEditorRef.invalidateRender();
    this.displayObjectEditorRef.interactionDisplay.invalidateRender();
    this.endTranslationPt.x = this.lastTranslationPt.x;
    this.endTranslationPt.y = this.lastTranslationPt.y;
    var b = this.endTranslationPt.x - this.startTranslationPt.x,
        c = this.endTranslationPt.y - this.startTranslationPt.y;
    this.displayObjectEditorRef.lastTransformAction =
        function() {
            this.translateGraphicObjects(this.selectionList, b, c, !1)
        };
    !0 == this.saveActionToUndoRedoHistory && this.displayObjectEditorRef.createUndoRedo_translation(this.displayObjectEditorRef.selectionList, b, c);
    this.saveActionToUndoRedoHistory = !0;
    a = this.displayObjectEditorRef.selectionList;
    var d = a.length;
    if ("pointsInteraction" == this.displayObjectEditorRef.interactionMode)
        for (var e = 0; e < d; e++) a[e].parent.invalidateBounds();
    this.displayObjectEditorRef.onChange();
    this.displayObjectEditorRef.showSelectionProperties()
};
_.onGlobalMouseUp = function(a) {};
_.translateObjects = function() {
    this.displayObjectEditorRef.translateGraphicObjects(this.displayObjectEditorRef.selectionList, this.currentTranslationPt.x - this.lastTranslationPt.x, this.currentTranslationPt.y - this.lastTranslationPt.y);
    this.lastTranslationPt.x = this.currentTranslationPt.x;
    this.lastTranslationPt.y = this.currentTranslationPt.y;
    this.displayObjectEditorRef.interactionDisplay.invalidateRender()
};
_.onGestureEnd = function(a) {};
_.onGestureChange = function(a) {};
_.exitByGesture = function() {};
ExtendClass(XOS_SplitPath_tool, XOS_BaseTool_tool);

function XOS_SplitPath_tool(a) {
    XOS_SplitPath_tool.baseConstructor.call(this, a);
    this.defaultOverInstanceCursor = this.onDragCursor = this.defaultCursor = "pathCutter_cur";
    this.defaultOverContourCursor = this.defaultOverHandleCursor = this.defaultOverVertexCursor = "pathCutterOverVertex_cur"
}
_ = XOS_SplitPath_tool.prototype;
_.onActivate = function() {
    this.displayObjectEditorRef.setCursor(this.defaultCursor);
    this.displayObjectEditorRef.setInteractionMode("pointsInteraction")
};
_.onMouseDown = function(a) {
    var b, c, d;
    c = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt);
    if (0 < this.displayObjectEditorRef.selectionList.length && (d = this.displayObjectEditorRef.interactionDisplay.hitTestAll(c), d.hit)) {
        b = d.instance;
        a = b.parent.shapeRef;
        c = b.pointRef.pathRef;
        b = c.getPointIndex(b.pointRef);
        this.splitPathAt_undable(a, c, b);
        return
    }
    d = this.displayObjectEditorRef.hitTestAll(c);
    if (d.instance instanceof XOS_Shape) {
        a = d.instance;
        if (d.path) c = d.path,
            b = d.idSegment, d = d.localPoint;
        else if (d = a.globalToLocal(c), b = a.hitTestContour(d, 10)) c = b.path, b = b.idSegment;
        else return;
        this.splitPathAtNearestPoint_undable(a, c, b, d)
    }
};
_.splitPathAt_undable = function(a, b, c) {
    b = b.splitAt(c);
    c = [];
    for (var d = b.length, e = 0; e < d; e++) {
        var f = a.clone();
        f.paths = [];
        f.paths.add(b[e]);
        c.push(f)
    }
    this.createUndoRedo(a, c)
};
_.splitPathAtNearestPoint_undable = function(a, b, c, d) {
    var e, f;
    e = 0 == c ? b.points[b.points.length - 1] : b.points[c - 1];
    f = b.points[c];
    f instanceof XOS_QuadraticCurveTo && (f = f.toCubicCurve(e));
    if (f instanceof XOS_CubicCurveTo) var g = [e, f.controlPt1, f.controlPt2, f],
        h = XOS_Path.nearestPointOnCurve(d, g),
        h = XOS_Path.splitCubicBezier(g, h.location),
        g = new XOS_CubicCurveTo(h.b1[0].x, h.b1[0].y, h.b1[1].x, h.b1[1].y, h.b1[2].x, h.b1[2].y),
        h = new XOS_CubicCurveTo(h.b2[0].x, h.b2[0].y, h.b2[1].x, h.b2[1].y, h.b2[2].x, h.b2[2].y);
    if (f instanceof XOS_LineTo) {
        g = XOS_Path.nearestPointOnLine(e.x, e.y, f.x, f.y, d.x, d.y, 5);
        if (null == g) return;
        h = f.splitAtPoint(g);
        g = h[0];
        h = h[1]
    }
    a.parent.getChildIndex(a);
    f = b.clone();
    b = [];
    0 == c ? (f.addPointAt(g, f.points.length), f.addPointAt(h, 0), f.removePointAt(1), b = f.splitAt(f.points.length - 1)) : (f.addPointAt(g, c), f.addPointAt(h, c + 1), f.removePointAt(c + 2), b = f.splitAt(c));
    c = [];
    f = b.length;
    for (g = 0; g < f; g++) d = a.clone(), d.paths = [], d.paths.add(b[g]), c.push(d);
    this.createUndoRedo(a, c)
};
_.createUndoRedo = function(a, b) {
    function c() {
        d.deselectAllGraphicObjects();
        e.removeChild(a);
        for (var c = b.length, g = 0; g < c; g++) d.addGraphicObject(b[g], e, f + g);
        d.selectGraphicObject(b[b.length - 1], !0);
        d.showSelectionProperties();
        d.invalidateRender()
    }
    var d = this.displayObjectEditorRef,
        e = a.parent,
        f = e.getChildIndex(a),
        g = new XOS_Action(function() {
            d.deselectAllGraphicObjects();
            for (var c = b.length, g = 0; g < c; g++) e.removeChild(b[g]);
            d.addGraphicObject(a, e, f);
            d.selectGraphicObject(a, !0);
            d.showSelectionProperties();
            d.invalidateRender()
        }),
        h = new XOS_Action(c);
    this.displayObjectEditorRef.history.addUndoRedoActions(g, h);
    c()
};
ExtendClass(XOS_RemovePathPoint_tool, XOS_BaseTool_tool);

function XOS_RemovePathPoint_tool(a) {
    XOS_RemovePathPoint_tool.baseConstructor.call(this, a);
    this.defaultOverContourCursor = this.defaultOverInstanceCursor = this.onDragCursor = this.defaultCursor = "pathRemovePoint_cur";
    this.defaultOverHandleCursor = this.defaultOverVertexCursor = "pathRemovePointOverVertex_cur"
}
_ = XOS_RemovePathPoint_tool.prototype;
_.onActivate = function() {
    this.displayObjectEditorRef.setCursor(this.defaultCursor);
    this.displayObjectEditorRef.setInteractionMode("pointsInteraction")
};
_.onMouseDown = function(a) {
    0 != this.displayObjectEditorRef.selectionList.length && (a = this.displayObjectEditorRef.interactionDisplay.hitTestAll(this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt)), a.hit && (a = a.instance, this.removePathPoint_undable(a.parent.shapeRef, a.pointRef)))
};
_.removePathPoint_undable = function(a, b) {
    function c() {
        e.removePoint(b);
        a.invalidateBounds();
        d.deselectAllGraphicObjects();
        d.selectGraphicObject(a, !0);
        d.showSelectionProperties();
        d.invalidateRender()
    }
    var d = this.displayObjectEditorRef,
        e = b.pathRef,
        f = e.getPointIndex(b),
        g = new XOS_Action(function() {
            e.addPointAt(b, f);
            a.invalidateBounds();
            d.deselectAllGraphicObjects();
            d.selectGraphicObject(a, !0);
            d.showSelectionProperties();
            d.invalidateRender()
        }),
        h = new XOS_Action(c);
    this.displayObjectEditorRef.history.addUndoRedoActions(g,
        h);
    c()
};
ExtendClass(XOS_AddPathPoint_tool, XOS_BaseTool_tool);

function XOS_AddPathPoint_tool(a) {
    XOS_AddPathPoint_tool.baseConstructor.call(this, a);
    this.defaultOverInstanceCursor = this.defaultOverContourCursor = this.onDragCursor = this.defaultCursor = "pathAddPoint_cur";
    this.defaultOverHandleCursor = this.defaultOverVertexCursor = this.defaultOverContourCursor = "pathAddPointOverVertex_cur"
}
_ = XOS_AddPathPoint_tool.prototype;
_.onActivate = function() {
    this.displayObjectEditorRef.setCursor(this.defaultCursor);
    this.displayObjectEditorRef.setInteractionMode("pointsInteraction")
};
_.onMouseDown = function(a) {
    var b = this.displayObjectEditorRef.hitTestAll(this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt));
    if (b.hit) {
        var c, d;
        if (b.instance instanceof XOS_Shape) {
            a = b.instance;
            if (b.path) c = b.path, d = b.idSegment, b = b.localPoint;
            else if (b = a.globalToLocal(this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt)), d = a.hitTestContour(b, 10)) c = d.path, d = d.idSegment;
            else return;
            this.addPathPointAtNearestPoint_undable(a,
                c, d, b)
        }
    }
};
_.addPathPointAtNearestPoint_undable = function(a, b, c, d) {
    function e() {
        0 == c ? (b.addPointAt(h, b.points.length), b.addPointAt(k, 0), b.removePointAt(1)) : (b.addPointAt(h, c), b.addPointAt(k, c + 1), b.removePointAt(c + 2));
        l.deselectAllGraphicObjects();
        l.selectGraphicObject(a, !0);
        l.showSelectionProperties();
        l.invalidateRender()
    }
    var f, g;
    f = 0 == c ? b.points[b.points.length - 1] : b.points[c - 1];
    g = b.points[c];
    g instanceof XOS_QuadraticCurveTo && (g = g.toCubicCurve(f));
    if (g instanceof XOS_CubicCurveTo) {
        f = [f, g.controlPt1, g.controlPt2,
            g
        ];
        d = XOS_Path.nearestPointOnCurve(d, f);
        d = XOS_Path.splitCubicBezier(f, d.location);
        var h = new XOS_CubicCurveTo(d.b1[0].x, d.b1[0].y, d.b1[1].x, d.b1[1].y, d.b1[2].x, d.b1[2].y),
            k = new XOS_CubicCurveTo(d.b2[0].x, d.b2[0].y, d.b2[1].x, d.b2[1].y, d.b2[2].x, d.b2[2].y)
    } else if (g instanceof XOS_LineTo) {
        d = XOS_Path.nearestPointOnLine(f.x, f.y, g.x, g.y, d.x, d.y, 5);
        if (null == d) return;
        d = g.splitAtPoint(d);
        h = d[0];
        k = d[1]
    }
    var l = this.displayObjectEditorRef;
    d = new XOS_Action(function() {
        0 == c ? (b.removePointAt(b.points.length - 1), b.removePointAt(0),
            b.addPointAt(g, 0)) : (b.addPointAt(g, c + 2), b.removePointAt(c + 1), b.removePointAt(c));
        l.deselectAllGraphicObjects();
        l.selectGraphicObject(a, !0);
        l.showSelectionProperties();
        l.invalidateRender()
    });
    f = new XOS_Action(e);
    this.displayObjectEditorRef.history.addUndoRedoActions(d, f);
    e()
};
ExtendClass(XOS_ConvertPathPoint_tool, XOS_BaseTool_tool);

function XOS_ConvertPathPoint_tool(a) {
    XOS_ConvertPathPoint_tool.baseConstructor.call(this, a);
    this.defaultOverContourCursor = this.defaultOverInstanceCursor = this.onDragCursor = this.defaultCursor = "convertPathPoint_cur";
    this.defaultOverHandleCursor = this.defaultOverVertexCursor = "convertPathPointOverVertex_cur";
    this.idNextPointTarget = this.idPointTarget = this.nextControlPoint = this.previousControlPoint = this.newNextPointTarget = this.newPointTarget = this.savedNextPointTarget = this.savedPointTarget = this.pathTarget =
        this.shapeTarget = this.clickedHandle = null
}
_ = XOS_ConvertPathPoint_tool.prototype;
_.onActivate = function() {
    this.displayObjectEditorRef.setCursor(this.defaultCursor);
    this.displayObjectEditorRef.setInteractionMode("pointsInteraction")
};
_.resetState = function() {
    this.idNextPointTarget = this.idPointTarget = this.nextControlPoint = this.previousControlPoint = this.newNextPointTarget = this.newPointTarget = this.savedNextPointTarget = this.savedPointTarget = this.pathTarget = this.shapeTarget = this.clickedHandle = null
};
_.onMouseDown = function(a) {
    0 != this.displayObjectEditorRef.selectionList.length && (this.resetState(), a = this.displayObjectEditorRef.interactionDisplay.hitTestAll(this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt)), a.hit && (this.clickedHandle = a.instance, this.pt2.x = this.pt1.x = this.clickedHandle.pointRef.x, this.pt2.y = this.pt1.y = this.clickedHandle.pointRef.y, this.shapeTarget = this.clickedHandle.parent.shapeRef, this.pathTarget = this.clickedHandle.pointRef.pathRef,
        GLOBAL_MOUSE_LISTENER.objectTarget = this))
};
_.onGlobalMouseStartDrag = function(a) {
    this.convertPointToCurve(this.clickedHandle.pointRef)
};
_.onGlobalMouseDrag = function(a) {
    this.doMouseDragAction()
};
_.onGlobalMouseEndDrag = function(a) {
    var b = this.shapeTarget,
        c = this.pathTarget,
        d = this.savedPointTarget,
        e = this.newPointTarget.clone(),
        f = this.idPointTarget;
    if (this.savedNextPointTarget) var g = this.savedNextPointTarget,
        h = this.newNextPointTarget.clone(),
        k = this.idNextPointTarget;
    var l = this.displayObjectEditorRef;
    a = new XOS_Action(function() {
        c.addPointAt(d, f, !0);
        g && c.addPointAt(g, k, !0);
        b.invalidateBounds();
        l.deselectAllGraphicObjects();
        l.selectGraphicObject(b, !0);
        l.invalidateRender();
        l.showSelectionProperties()
    });
    var q = new XOS_Action(function() {
        c.addPointAt(e, f, !0);
        h && c.addPointAt(h, k, !0);
        b.invalidateBounds();
        l.deselectAllGraphicObjects();
        l.selectGraphicObject(b, !0);
        l.invalidateRender();
        l.showSelectionProperties()
    });
    this.displayObjectEditorRef.history.addUndoRedoActions(a, q);
    this.shapeTarget.invalidateBounds();
    this.displayObjectEditorRef.deselectAllGraphicObjects();
    this.displayObjectEditorRef.selectGraphicObject(this.shapeTarget, !0);
    this.displayObjectEditorRef.showSelectionProperties();
    this.displayObjectEditorRef.invalidateRender();
    this.displayObjectEditorRef.setCursor(this.defaultCursor)
};
_.convertPointToCurve = function(a) {
    this.idPointTarget = this.pathTarget.getPointIndex(a);
    this.savedPointTarget = a;
    this.savedPointTarget instanceof XOS_CubicCurveTo ? (this.newPointTarget = new XOS_CubicCurveTo(this.savedPointTarget.controlPt1.x, this.savedPointTarget.controlPt1.y, this.savedPointTarget.x, this.savedPointTarget.y, this.savedPointTarget.x, this.savedPointTarget.y), this.previousControlPoint = this.newPointTarget.controlPt2) : (this.newPointTarget = new XOS_QuadraticCurveTo(this.savedPointTarget.x, this.savedPointTarget.y,
        this.savedPointTarget.x, this.savedPointTarget.y), this.previousControlPoint = this.newPointTarget.controlPt);
    this.pathTarget.addPointAt(this.newPointTarget, this.idPointTarget, !0);
    this.idNextPointTarget = this.savedNextPointTarget = null;
    if (this.idPointTarget == this.pathTarget.points.length - 1)
        if (this.pathTarget.closePath) this.idNextPointTarget = 0;
        else return;
    else this.idNextPointTarget = this.idPointTarget + 1;
    this.savedNextPointTarget = this.pathTarget.points[this.idNextPointTarget];
    this.savedNextPointTarget instanceof
    XOS_CubicCurveTo ? (this.newNextPointTarget = new XOS_CubicCurveTo(this.savedPointTarget.x, this.savedPointTarget.y, this.savedNextPointTarget.controlPt2.x, this.savedNextPointTarget.controlPt2.y, this.savedNextPointTarget.x, this.savedNextPointTarget.y), this.nextControlPoint = this.newNextPointTarget.controlPt1, this.newNextPointTarget.controlPt1.collapsed = !1, this.newNextPointTarget.controlPt2.collapsed = !1) : (this.newNextPointTarget = new XOS_QuadraticCurveTo(this.savedPointTarget.x, this.savedPointTarget.y, this.savedNextPointTarget.x,
        this.savedNextPointTarget.y), this.nextControlPoint = this.newNextPointTarget.controlPt, this.newNextPointTarget.controlPt.collapsed = !1);
    this.pathTarget.addPointAt(this.newNextPointTarget, this.idNextPointTarget, !0)
};
_.doMouseDragAction = function() {
    var a = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseDownPt),
        b = this.displayObjectEditorRef.interactionDisplay.canvas.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt);
    this.pt2.x = b.x;
    this.pt2.y = b.y;
    this.pt2 = this.shapeTarget.globalToLocal(this.pt2);
    this.previousControlPoint.set(this.pt1.x - (this.pt2.x - this.pt1.x), this.pt1.y - (this.pt2.y - this.pt1.y));
    this.savedNextPointTarget && this.nextControlPoint.set(this.pt2.x, this.pt2.y);
    var c = this.displayObjectEditorRef.interactionDisplay.graphicContext;
    this.displayObjectEditorRef.interactionDisplay.clear();
    c.lineWidth = 1;
    c.strokeStyle = "#2E87FF";
    c.fillStyle = "#FFFFFF";
    this.shapeTarget.drawAsSelected();
    c.setTransform(1, 0, 0, 1, 0, 0);
    c.beginPath();
    var d = a.x - (b.x - a.x),
        e = a.y - (b.y - a.y);
    c.moveTo(d, e);
    c.lineTo(b.x, b.y);
    c.stroke();
    c.beginPath();
    DEVICE_WITH_TOUCH_EVENTS ? (c.rect(a.x - 4, a.y - 4, 8, 8), c.drawEllipse(d - 4, e - 4, 8, 8), c.drawEllipse(b.x - 4, b.y - 4, 8, 8)) : (c.rect(a.x - 2.5, a.y - 2.5, 5, 5), c.drawEllipse(d -
        3, e - 3, 6, 6), c.drawEllipse(b.x - 3, b.y - 3, 6, 6));
    c.fill();
    c.stroke()
};
ExtendClass(GraphicProperties2D_panel, XOS_PropertyInspectorPanel);

function GraphicProperties2D_panel(a, b) {
    GraphicProperties2D_panel.baseConstructor.call(this, a, b);
    this.DEGREE_TO_RADIANT = Math.PI / 180;
    this.RADIANT_TO_DEGREE = 180 / Math.PI;
    this.calculatedProperties = {}
}
_ = GraphicProperties2D_panel.prototype;
_.init = function() {
    GraphicProperties2D_panel.superClass.init.call(this);
    var a = this;
    this.editTitleDescription_button = this.htmlElement.getChildById("editTitleDescription_button");
    CreateEventListener(this.editTitleDescription_button, "mousedown", function() {
        a.appRef.activeDocumentController.editTitleDescription(a.currentEditableGraphicObj)
    });
    a = this;
    this.editInlineContent_button = this.htmlElement.getChildById("editInlineContent_button");
    CreateEventListener(this.editInlineContent_button, "mousedown", function() {
        a.appRef.activeDocumentController.editInlineContent(a.currentEditableGraphicObj)
    });
    this.lineOptionsButton = this.htmlElement.getChildById("LINE_OPTIONS_BUTTON");
    CreateEventListener(this.lineOptionsButton, "mousedown", function(b) {
        b = a.lineOptionsButton.getGlobalPosition();
        a.appRef.contextMenus.lineOptionsContextMenu.showAt(b.x - 180, b.y)
    });
    this.fontListButton = this.htmlElement.getChildById("FONT_LIST_MENU");
    CreateEventListener(this.fontListButton, "mousedown", function(b) {
        a.appRef.resourcesPanel.setActivePanel(a.appRef.fontsLibraryPanel.htmlElement);
        a.appRef.resourcesWin.open()
    })
};
_.onBeforeUpdate = function() {
    this.appRef.inspectorPanel.panels.fillAndStrokeProperties2DPanel.update(this.currentEditableGraphicObj);
    var a = this.appRef.activeDocumentController.interactionDisplay.children[0];
    if (a instanceof XOS_BoundsInteraction2D && a.isLocalBounds) {
        var b = this.currentEditableGraphicObj.getPositionInContent();
        this.calculatedProperties.positionX = this.currentEditableGraphicObj.displayRef.baseToUnitOfMeasure(b.x);
        this.calculatedProperties.positionY = this.currentEditableGraphicObj.displayRef.baseToUnitOfMeasure(b.y)
    } else b =
        this.appRef.activeDocumentController.getSelectionBounds(), b = this.appRef.activeDocumentController.content.globalToLocal(b.min), this.calculatedProperties.positionX = this.appRef.activeDocumentController.baseToUnitOfMeasure(b.x), this.calculatedProperties.positionY = this.appRef.activeDocumentController.baseToUnitOfMeasure(b.y);
    if (a instanceof XOS_BoundsInteraction2D)
        if (a.isLocalBounds) b = this.currentEditableGraphicObj.getSize(), this.calculatedProperties.sizeX = this.currentEditableGraphicObj.displayRef.baseToUnitOfMeasure(b.x),
            this.calculatedProperties.sizeY = this.currentEditableGraphicObj.displayRef.baseToUnitOfMeasure(b.y);
        else {
            var c = this.appRef.activeDocumentController.content.contentScale.x,
                b = this.appRef.activeDocumentController.getSelectionBounds();
            this.calculatedProperties.sizeX = this.appRef.activeDocumentController.baseToUnitOfMeasure(b.getWidth() / c);
            this.calculatedProperties.sizeY = this.appRef.activeDocumentController.baseToUnitOfMeasure(b.getHeight() / c)
            //alert(this.calculatedProperties.sizeY);
        }
    b = this.currentEditableGraphicObj.getScale();
    this.calculatedProperties.scaleX =
        b.x.toFixed(this.currentEditableGraphicObj.displayRef.fixedUnitDecimal);
    this.calculatedProperties.scaleY = b.y.toFixed(this.currentEditableGraphicObj.displayRef.fixedUnitDecimal);
    this.calculatedProperties.rotation = 0;
    a instanceof XOS_BoundsInteraction2D && a.isLocalBounds && (a = this.currentEditableGraphicObj.getRotation(), this.calculatedProperties.rotation = Number(a * this.RADIANT_TO_DEGREE).toFixed(this.currentEditableGraphicObj.displayRef.fixedUnitDecimal))
};
_.getValueForInputProperty = function(a) {
    var b = a.parentNode.parentNode;
    b.style.display = "block";
    switch (a.name) {
        case "fontList":
            return this.editableObject.font;
        case "lineWidth":
            return this.currentEditableGraphicObj.lineWidth.toFixed(this.currentEditableGraphicObj.displayRef.fixedUnitDecimal);
        case "positionX":
        case "positionY":
        case "sizeX":
        case "sizeY":
        case "rotation":
            return this.calculatedProperties[a.name];
        case "scaleX":
        case "scaleY":
            return 1 == this.calculatedProperties.scaleX && 1 == this.calculatedProperties.scaleY &&
                (b.style.display = "none"), this.calculatedProperties[a.name];
        default:
            if (null != this.editableObject[a.name] && void 0 != this.editableObject[a.name]) return this.editableObject[a.name];
            b.classList.contains("inspector_parameter") && (b.style.display = "none");
            return ""
    }
};
_.evalNumericValue = function(a, b) {
    a = a.evalAsMathExpression();
    if (null == a) return null;
    b && (a = this.appRef.activeDocumentController.unitOfMeasureToBase(a));
    return a
};
_.onInputPropertyChanged = function(a) {
    if (this.currentEditableGraphicObj) switch (a.name) {
        case "positionX":
        case "positionY":
            this.onInputPropertyChanged_position(a);
            break;
        case "sizeX":
        case "sizeY":
            this.onInputPropertyChanged_size(a);
            break;
        case "scaleX":
            this.appRef.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
                scaleX: this.evalNumericValue(a.value)
            });
            break;
        case "scaleY":
            this.appRef.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
                scaleY: this.evalNumericValue(a.value)
            });
            break;
        case "rotation":
       // alert("asdasdad asd");
            this.onInputPropertyChanged_rotation(a);
            break;
        case "lineWidth":
            this.appRef.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
                lineWidth: this.evalNumericValue(a.value)
            });
            break;
        case "fontSize":
            this.appRef.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
                fontSize: Number(a.value)
            }, !0);
            break;
        case "lineHeight":
            this.appRef.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
                lineHeight: Number(a.value)
            }, !0);
            break;
        case "font":
            var b =
                this.appRef.fontFamilyList.getFontFamilyDataByFontName(a.value);
            b ? this.appRef.activeDocumentController.setFontFamilyOfGraphicObjects(this.appRef.activeDocumentController.selectionList, b.family, !0) : this.appRef.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
                font: a.value
            }, !0);
            break;
        case "fontList":
            window.scrollTo(0, 0);
            this.appRef.activeDocumentController.setFontFamilyOfGraphicObjects(this.appRef.activeDocumentController.selectionList, this.appRef.fontFamilyList.items[a.selectedIndex -
                1].family, !0);
            this.fontListMenu_select.fontTextField.value = this.appRef.fontFamilyList.items[a.selectedIndex - 1].family;
            break;
        case "fontStyle":
            window.scrollTo(0, 0);
            this.appRef.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
                fontStyle: a.value
            }, !0);
            break;
        case "fontWeight":
            window.scrollTo(0, 0);
            this.appRef.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
                fontWeight: a.value
            }, !0);
            break;
        case "compositeOperation":
            this.appRef.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
                compositeOperation: a.value
            }, !0);
            break;
        case "includeUrl":
            var b = a.value,
                c = b.indexOf('src="');
                console.log(b);
                youtube_video_temp[0]=b;
            if (-1 != c) {
                var d = b.indexOf('"', c + 5),
                    b = b.substring(c + 5, d);
                a.value = b
            }
            this.appRef.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
                includeUrl: b
            });
            console.log(this.appRef.activeDocumentController.selectionList);
            break;
        default:
            b = {}, b[a.name] = a.value, this.appRef.activeDocumentController.setPropertiesOfObjects_undable(b, [this.currentEditableGraphicObj])
    }
};
_.onInputPropertyChanged_position = function(a) {
    var b = a.value.evalAsMathExpression();
    if (null != b) {
        var c = this.appRef.activeDocumentController.interactionDisplay.children[0],
            d = this.appRef.activeDocumentController.content.localToGlobal(new XOS_Point2D(0, 0)),
            e, f = this.appRef.activeDocumentController.content.contentScale.x;
        bounds = this.appRef.activeDocumentController.getSelectionBounds();
        e = bounds.min;
        var b = this.appRef.activeDocumentController.unitOfMeasureToBase(b) * f,
            g = f = 0;
        "positionX" == a.name ? f = d.x + b - e.x : "positionY" ==
            a.name && (g = d.y + b - e.y);
        this.appRef.activeDocumentController.translateGraphicObjects(this.appRef.activeDocumentController.selectionList, f, g);
        this.appRef.activeDocumentController.createUndoRedo_translation(this.appRef.activeDocumentController.selectionList, f, g);
        c instanceof XOS_PointsInteraction && this.appRef.activeDocumentController.invalidateGraphicObjectsBounds(this.appRef.activeDocumentController.selectionList);
        this.appRef.activeDocumentController.onChange();
        this.appRef.activeDocumentController.invalidateRender();
        this.appRef.activeDocumentController.interactionDisplay.invalidateRender()
    }
};
_.onInputPropertyChanged_size = function(a) {
    var b = a.value.evalAsMathExpression();
    if (null != b) {
        var c, d, e = !1,
            f = this.appRef.activeDocumentController.interactionDisplay.children[0];
        f instanceof XOS_BoundsInteraction2D ? (d = f.bounds, c = d.min, f.isLocalBounds && (d = d.clone(), c = f.displayObject2DRef.localToGlobal(c), e = !0, d.max = d.max.multiply(f.displayObject2DRef.getScale()))) : (d = this.appRef.activeDocumentController.getSelectionBounds(), c = d.min);
        var g = f = 1;
        "sizeX" == a.name ? f = this.appRef.activeDocumentController.unitOfMeasureToBase(b) /
            d.getWidth() : "sizeY" == a.name && (g = this.appRef.activeDocumentController.unitOfMeasureToBase(b) / d.getHeight());

        this.appRef.activeDocumentController.scaleGraphicObjects(this.appRef.activeDocumentController.selectionList, f, g, c, e);
        this.appRef.activeDocumentController.normalizeGraphicObjects(this.appRef.activeDocumentController.selectionList);
        this.appRef.activeDocumentController.createUndoRedo_scale(this.appRef.activeDocumentController.selectionList, f, g, c, e);
        this.appRef.activeDocumentController.onChange();
        this.appRef.activeDocumentController.invalidateRender();
        this.appRef.activeDocumentController.interactionDisplay.invalidateRender()
        //alert('set');
    }
};
_.onInputPropertyChanged_rotation = function(a) {
    a = a.value.evalAsMathExpression();
    // if (null != a) {
    //     a *= this.DEGREE_TO_RADIANT;
    //     var b, c = this.appRef.activeDocumentController.interactionDisplay.children[0];
    //     c instanceof XOS_BoundsInteraction2D && (b = c.bounds.getCenter(), c.isLocalBounds ? this.appRef.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
    //         rotation: a
    //     }) : (this.appRef.activeDocumentController.rotateGraphicObjects(this.appRef.activeDocumentController.selectionList, a, b), this.appRef.activeDocumentController.createUndoRedo_rotation(this.appRef.activeDocumentController.selectionList,
    //         a, b), this.appRef.activeDocumentController.invalidateRender(), this.appRef.activeDocumentController.interactionDisplay.invalidateRender()))
    // }
    if (null != a) {
        a *= this.DEGREE_TO_RADIANT;
        var b, c = this.appRef.activeDocumentController.interactionDisplay.children[0];
        c instanceof XOS_BoundsInteraction2D && (b = c.bounds.getCenter(), c.isLocalBounds ? this.appRef.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
            rotation: a
        }) : (this.appRef.activeDocumentController.rotateGraphicObjects(this.appRef.activeDocumentController.selectionList, a, b), this.appRef.activeDocumentController.createUndoRedo_rotation(this.appRef.activeDocumentController.selectionList,
            a, b), this.appRef.activeDocumentController.invalidateRender(), this.appRef.activeDocumentController.interactionDisplay.invalidateRender()))
    }
};
_.popolateFontListMenu = function() {
    this.fontListMenu_select = this.htmlElement.getChildById("fontList");
    this.fontListMenu_select.fontTextField = this.htmlElement.getChildById("font");
    var a, b;
    a = document.createElement("option");
    b = document.createTextNode("select font");
    a.appendChild(b);
    this.fontListMenu_select.appendChild(a);
    for (var c = this.appRef.fontFamilyList.items, d = c.length, e = 0; e < d; e++) b = c[e], a = document.createElement("option"), b = document.createTextNode(b.family), a.appendChild(b), this.fontListMenu_select.appendChild(a)
};

function PageProperties2D_panel(a, b) {
    this.htmlElement = a;
    this.htmlElement.enableScrollingWithTouchMove();
    this.appRef = b;
    this.init()
}
_ = PageProperties2D_panel.prototype;
_.init = function() {
    var a = this.appRef;
    this.docSizeMenu_select = this.htmlElement.getChildById("DOC_SIZE_MENU");
    CreateEventListener(this.docSizeMenu_select, "change", function() {
        var b = this.options[this.selectedIndex].value.split(",");
        a.activeDocumentController.setPageSize(b[0], b[1]);
        a.activeDocumentController.fitToView();
        a.activeDocumentController.showSelectionProperties();
        this.options[0].selected = !0
    });
    var b = this.pageAlignmentMenu_select = this.htmlElement.getChildById("pageAlignment");
    CreateEventListener(this.pageAlignmentMenu_select,
        "change",
        function() {
            a.activeDocumentController.setPageAlignment(b.value)
        });
    var c = this.bodyBackgroundColor_element = this.htmlElement.getChildById("bodyBackgroundColor");
    CreateEventListener(c, "mousedown", function() {
        var b = this.getGlobalPosition();
        a.colorPicker.showAndSet(b.x - 160, b.y, c.style.backgroundColor, null, !0, null, function(b) {
            a.activeDocumentController.bodyBackgroundColor = b.currentColorAsHtmlStringRGBA();
            c.style.backgroundColor = a.activeDocumentController.bodyBackgroundColor;
            a.activeDocumentController.setPageColor(a.activeDocumentController.bodyBackgroundColor)
        })
    });
    this.sizeX_textfield = this.htmlElement.getChildById("sizeX");
    CreateEventListener(this.sizeX_textfield, "change", function() {
        a.activeDocumentController.setPropertiesOfObjects_undable({
            pageWidth: Number(this.value)
        }, [a.activeDocumentController], !0);
    });
    this.sizeY_textfield = this.htmlElement.getChildById("sizeY");
    //alert(this.sizeY_textfield);
    CreateEventListener(this.sizeY_textfield, "change", function() {
        a.activeDocumentController.setPropertiesOfObjects_undable({
            pageHeight: Number(this.value)
        }, [a.activeDocumentController], !0)

    });
    this.screenSnapValue_textfield =
        this.htmlElement.getChildById("screenSnapValue");
    CreateEventListener(this.screenSnapValue_textfield, "change", function() {
        a.activeDocumentController.setScreenSnapValue(Number(this.value))
    });
    this.gridSnapValue_textfield = this.htmlElement.getChildById("gridSnapValue");
    CreateEventListener(this.gridSnapValue_textfield, "change", function() {
        a.activeDocumentController.setGridSnapValue(Number(this.value))
    });
    this.unityMenu_select = this.htmlElement.getChildById("UNITY_MENU");
    CreateEventListener(this.unityMenu_select,
        "change",
        function() {
            a.activeDocumentController.setUnitOfMeasure(a.activeDocumentController[this.options[this.selectedIndex].value]);
            a.activeDocumentController.showSelectionProperties()
        });
    this.unityDecimalsMenu_select = this.htmlElement.getChildById("UNITY_DECIMALS_MENU");
    CreateEventListener(this.unityDecimalsMenu_select, "change", function() {
        a.activeDocumentController.setFixedDecimalToShow(Number(this.options[this.selectedIndex].value));
        a.activeDocumentController.showSelectionProperties()
    });
    this.moveByArrowValue_textfield =
        this.htmlElement.getChildById("moveByArrowValue");
    CreateEventListener(this.moveByArrowValue_textfield, "change", function() {
        a.activeDocumentController.moveByArrowValue = a.activeDocumentController.unitOfMeasureToBase(Number(this.value))
    });
    this.javascriptIncludes_button = this.htmlElement.getChildById("editIncludes_button");
    CreateEventListener(this.javascriptIncludes_button, "mousedown", function() {
        a.activeDocumentController.editIncludes()
    });
    this.editTitleDescription_button = this.htmlElement.getChildById("editTitleDescription_button");
    CreateEventListener(this.editTitleDescription_button, "mousedown", function() {
        a.activeDocumentController.editTitleDescription(a.activeDocumentController)
    })
};
_.update = function(a) {
    var b = this.appRef.activeDocumentController.getLayerByName("workspace").children[0];
    this.sizeX_textfield.value = this.appRef.activeDocumentController.baseToUnitOfMeasure(b.width);
    this.sizeY_textfield.value = this.appRef.activeDocumentController.baseToUnitOfMeasure(b.height);
    this.screenSnapValue_textfield.value = this.appRef.activeDocumentController.screenSnapValue;
    this.gridSnapValue_textfield.value = this.appRef.activeDocumentController.baseToUnitOfMeasure(this.appRef.activeDocumentController.gridSnapValue);
    this.moveByArrowValue_textfield.value = this.appRef.activeDocumentController.baseToUnitOfMeasure(this.appRef.activeDocumentController.moveByArrowValue);
    this.appRef.inspectorPanel.panels.fillAndStrokeProperties2DPanel.update(a);
    this.pageAlignmentMenu_select.value = this.appRef.activeDocumentController.pageAlignment;
    this.bodyBackgroundColor_element.style.backgroundColor = this.appRef.activeDocumentController.bodyBackgroundColor
};

function FillAndStrokeProperties2D_panel(a, b) {
    this.htmlElement = a;
    this.appRef = b;
    this.currentEditableGraphicObj = null;
    this.currentEditableProperty = "fill";
    this.init()
}
_ = FillAndStrokeProperties2D_panel.prototype;
_.init = function() {
    this.strokeButton = this.htmlElement.getChildById("STROKE_BUTTON");
    CreateEventListener(this.strokeButton, "mousedown", Bind(this, this.onClickOnStrokeButton));
    this.fillButton = this.htmlElement.getChildById("FILL_BUTTON");
    CreateEventListener(this.fillButton, "mousedown", Bind(this, this.onClickOnFillButton));
    var a = this.appRef,
        b = this;
    this.diffuseButton = this.htmlElement.getChildById("DIFFUSE_BUTTON");
    CreateEventListener(this.diffuseButton, "mousedown", function() {
        a.activeDocumentController.setColorOfSelectedObjects(this.style.backgroundColor,
            1);
        b.updateStrokeProperty();
        b.updateFillProperty()
    });
    var c = this.gradientButton = this.htmlElement.getChildById("GRADIENT_BUTTON");
    CreateEventListener(this.gradientButton, "mousedown", function() {
        a.activeDocumentController.setGradientOfSelectedObjects(c.style.cssText);
        b.updateStrokeProperty();
        b.updateFillProperty()
    });
    this.patternButton = this.htmlElement.getChildById("PATTERN_BUTTON");
    CreateEventListener(this.patternButton, "mousedown", function() {
        a.activeDocumentController.setPatternOfSelectedObjects();
        b.updateStrokeProperty();
        b.updateFillProperty()
    });
    this.noneButton = this.htmlElement.getChildById("NONE_BUTTON");
    CreateEventListener(this.noneButton, "mousedown", function() {
        a.activeDocumentController.setColorOfSelectedObjects("none", 1);
        b.updateStrokeProperty();
        b.updateFillProperty()
    });
    this.swapFillStrokeProperties = this.htmlElement.getChildById("SWAP_FILL_STROKE_BUTTON");
    CreateEventListener(this.swapFillStrokeProperties, "mousedown", Bind(this, this.onClickOnSwapFillStrokeButton));
    this.alphaSlider = new XOS_Slider(this.htmlElement.getChildById("ALPHA_SLIDER"),
        0, 1, 1, Bind(this, this.onAlphaSliderChange), Bind(this, this.onAlphaSliderChangeStart), Bind(this, this.onAlphaSliderChangeEnd))
};
_.update = function(a) {
    this.htmlElement.style.display = "inline-block";
    this.currentEditableGraphicObj = a;
    this.updateStrokeProperty();
    this.updateFillProperty();
    this.alphaSlider.setValue(a.alpha)
};
_.onClickOnSwapFillStrokeButton = function(a) {
    this.appRef.activeDocumentController.swapFillAndStrokePropertiesOfSelectedGraphicObject()
};
_.onClickOnFillButton = function(a) {
    this.appRef.activeDocumentController.currentEditableProperty = this.currentEditableProperty = "fill";
    this.fillButton.parentElement.insertBefore(this.fillButton, this.fillButton.next);
    var b = -262,
        c;
    a.target == this.fillButton ? (a = this.fillButton.getBoundingClientRect(), c = a.left, a = a.top) : (b = -131, c = a.pageX, a = a.pageY);
    if (this.currentEditableGraphicObj.fillFillingType == XOS_DisplayObject2D.GRADIENT_FILLING) this.appRef.contextMenus.gradientEditor.showAndSet(c - 254, a + -92, this.currentEditableGraphicObj.fillGradient);
    else {
        var d = "#000",
            e = 1;
        this.currentEditableGraphicObj.fillFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING ? (d = this.currentEditableGraphicObj.fillColor, e = this.currentEditableGraphicObj.fillAlpha) : (d = this.appRef.activeDocumentController.fillColor, e = this.appRef.activeDocumentController.fillAlpha);
        c -= 133;
        this.appRef.colorPicker.showAndSet(c, a + b, d, e, !0, Bind(this, this.onColorPickerChange), Bind(this, this.onColorPickerChangeEnd))
    }
};
_.onClickOnStrokeButton = function(a) {
    this.appRef.activeDocumentController.currentEditableProperty = this.currentEditableProperty = "stroke";
    this.strokeButton.parentElement.insertBefore(this.strokeButton, this.strokeButton.next);
    var b = -262,
        c;
    a.target == this.strokeButton || a.target.parentNode == this.strokeButton ? (a = this.strokeButton.getBoundingClientRect(), c = a.left, a = a.top) : (b = -131, c = a.pageX, a = a.pageY);
    if (this.currentEditableGraphicObj.strokeFillingType == XOS_DisplayObject2D.GRADIENT_FILLING) this.appRef.contextMenus.gradientEditor.showAndSet(c -
        254, a + -92, this.currentEditableGraphicObj.strokeGradient);
    else {
        var d = "#000",
            e = 1;
        this.currentEditableGraphicObj.strokeFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING ? (d = this.currentEditableGraphicObj.strokeColor, e = this.currentEditableGraphicObj.strokeAlpha) : (d = this.appRef.activeDocumentController.strokeColor, e = this.appRef.activeDocumentController.strokeAlpha);
        c -= 133;
        this.appRef.colorPicker.showAndSet(c, a + b, d, e, !0, Bind(this, this.onColorPickerChange), Bind(this, this.onColorPickerChangeEnd))
    }
};
_.updateStrokeProperty = function() {
    this.strokeButton.style.cssText = "";
    this.appRef.activeDocumentController.strokeFillingType == XOS_DisplayObject2D.NONE_FILLING ? (this.strokeButton.style.borderColor = "#FFF", this.strokeButton.style.backgroundColor = "none", this.strokeButton.classList.add("noneColor")) : this.appRef.activeDocumentController.strokeFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING ? (this.strokeButton.classList.remove("noneColor"), this.strokeButton.style.backgroundColor = this.appRef.activeDocumentController.strokeColor) :
        this.appRef.activeDocumentController.strokeFillingType == XOS_DisplayObject2D.GRADIENT_FILLING ? (this.strokeButton.classList.remove("noneColor"), this.strokeButton.style.borderColor = "#000", this.strokeButton.style.backgroundColor = "none", this.strokeButton.style.cssText = this.appRef.activeDocumentController.strokeGradient.toHtmlStyle()) : this.appRef.activeDocumentController.strokeFillingType == XOS_DisplayObject2D.PATTERN_FILLING && (this.strokeButton.classList.remove("noneColor"), this.strokeButton.style.borderColor =
            "#000", this.strokeButton.style.backgroundColor = "none", this.strokeButton.style.cssText = this.appRef.activeDocumentController.strokePattern.toHtmlStyle());
    "stroke" == this.currentEditableProperty && (this.diffuseButton.style.backgroundColor = this.appRef.activeDocumentController.strokeColor, this.gradientButton.style.cssText = this.appRef.activeDocumentController.strokeGradient.toHtmlStyle(), this.patternButton.style.cssText = this.appRef.activeDocumentController.strokePattern.toHtmlStyle())
};
_.updateFillProperty = function() {
    this.fillButton.style.cssText = "";
    this.appRef.activeDocumentController.fillFillingType == XOS_DisplayObject2D.NONE_FILLING ? (this.fillButton.style.backgroundColor = "white", this.fillButton.classList.add("noneColor")) : this.appRef.activeDocumentController.fillFillingType == XOS_DisplayObject2D.DIFFUSE_FILLING ? (this.fillButton.style.backgroundColor = this.appRef.activeDocumentController.fillColor, this.fillButton.classList.remove("noneColor")) : this.appRef.activeDocumentController.fillFillingType ==
        XOS_DisplayObject2D.GRADIENT_FILLING ? (this.fillButton.classList.remove("noneColor"), this.fillButton.style.cssText = this.appRef.activeDocumentController.fillGradient.toHtmlStyle()) : this.appRef.activeDocumentController.fillFillingType == XOS_DisplayObject2D.PATTERN_FILLING && (this.fillButton.classList.remove("noneColor"), this.fillButton.style.cssText = this.appRef.activeDocumentController.fillPattern.toHtmlStyle());
    "fill" == this.currentEditableProperty && (this.diffuseButton.style.backgroundColor = this.appRef.activeDocumentController.fillColor,
        this.gradientButton.style.cssText = this.appRef.activeDocumentController.fillGradient.toHtmlStyle(), this.patternButton.style.cssText = this.appRef.activeDocumentController.fillPattern.toHtmlStyle())
};
_.onColorPickerChange = function(a) {
    var b = this.appRef.activeDocumentController.selectionList;
    0 != b.length && ("stroke" == this.currentEditableProperty ? this.appRef.activeDocumentController.setPropertiesOfObjects({
        strokeColor: a.hexColor,
        strokeAlpha: a.alpha
    }, b, !0, !1) : "fill" == this.currentEditableProperty && this.appRef.activeDocumentController.setPropertiesOfObjects({
        fillColor: a.hexColor,
        fillAlpha: a.alpha
    }, b, !0, !1))
};
_.onColorPickerChangeEnd = function(a) {
    var b = this.appRef.activeDocumentController.selectionList;
    0 < b.length && ("stroke" == this.currentEditableProperty ? this.appRef.activeDocumentController.setPropertiesOfObjects({
        strokeColor: a.previous_hexColor,
        strokeAlpha: a.previous_alpha
    }, b, !0, !1) : "fill" == this.currentEditableProperty && this.appRef.activeDocumentController.setPropertiesOfObjects({
        fillColor: a.previous_hexColor,
        fillAlpha: a.previous_alpha
    }, b, !0, !1), this.appRef.activeDocumentController.setColorOfSelectedObjects(a.hexColor,
        a.alpha));
    "stroke" == this.currentEditableProperty ? (this.appRef.activeDocumentController.strokeColor = a.hexColor, this.appRef.activeDocumentController.strokeAlpha = a.alpha, this.updateStrokeProperty()) : "fill" == this.currentEditableProperty && (this.appRef.activeDocumentController.fillColor = a.hexColor, this.appRef.activeDocumentController.fillAlpha = a.alpha, this.updateFillProperty())
};
_.onAlphaSliderChangeStart = function() {
    var a = this.appRef.activeDocumentController.getSelectionList();
    this.opacityList = [];
    for (var b = 0; b < a.length; b++) this.opacityList.push(a[b].opacity)
};
_.onAlphaSliderChange = function() {
    this.appRef.activeDocumentController.setPropertiesOfObjects({
        opacity: this.alphaSlider.currentValue
    }, this.appRef.activeDocumentController.getSelectionList(), !0)
};
_.onAlphaSliderChangeEnd = function() {
    for (var a = this.appRef.activeDocumentController.getSelectionList(), b = 0; b < a.length; b++) a[b].opacity = this.opacityList[b];
    this.appRef.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
        opacity: this.alphaSlider.currentValue
    })
};
ExtendClass(GradientEditor_panel, XOS_ContextMenu);

function GradientEditor_panel(a, b) {
    GradientEditor_panel.baseConstructor.call(this, a);
    this.htmlElement = a;
    this.appRef = b;
    this.editableGradient = null;
    this.isChanged = !1;
    this.gradientHtmlElement = this.htmlElement.getChildById("GRADIENT");
    CreateEventListener(this.gradientHtmlElement, "mousedown", this.onMouseDown, this);
    this.gradientType_select = this.htmlElement.getChildById("GRADIENT_TYPE");
    CreateEventListener(this.gradientType_select, "change", Bind(this, this.onChangeGradientType));
    this.htmlGradientButton = HTML("GRADIENT_BUTTON")
}
_ = GradientEditor_panel.prototype;
_.showAndSet = function(a, b, c) {
    this.editableGradient = c;
    this.showAt(a, b);
    a = this.editableGradient;
    this.editableGradient instanceof XOS_RadialGradient && (a = this.editableGradient.toLinearGradient());
    this.gradientHtmlElement.style.cssText = a.toHtmlStyle();
    this.createEditablePoints();
    this.gradientType_select.value = c instanceof XOS_LinearGradient ? "linear" : "radial"
};
_.onChangeGradientType = function(a) {
    var b = null;
    a = a.target.value;
    "radial" == a && this.editableGradient instanceof XOS_LinearGradient ? b = this.editableGradient.toRadialGradient() : "linear" == a && this.editableGradient instanceof XOS_RadialGradient && (b = this.editableGradient.toLinearGradient());
    b && (this.editableGradient = b, this.updateHtmlGradient(), this.appRef.activeDocumentController.setGradientOfSelectedObjects(b))
};
_.createEditablePoints = function() {
    this.gradientHtmlElement.innerHTML = "";
    for (var a, b, c = this.editableGradient.colors.length, d = 0; d < c; d++) a = this.editableGradient.colors[d], b = a.offset, a = a.color, this.addEditablePoint(this.gradientHtmlElement.offsetWidth * b - 9, a)
};
_.addEditablePoint = function(a, b) {
    var c = document.createElement("div");
    c.id = "GRADIENT_POINT";
    htmlColor = document.createElement("div");
    htmlColor.id = "COLOR";
    htmlColor.style.backgroundColor = b;
    c.appendChild(htmlColor);
    c.style.left = a + "px";
    this.gradientHtmlElement.appendChild(c)
};
_.updateHtmlGradient = function() {
    for (var a = [], b = 0; b < this.gradientHtmlElement.childNodes.length; b++) "none" != this.gradientHtmlElement.childNodes[b].style.display && a.push(this.gradientHtmlElement.childNodes[b]);
    a.sort(function(a, b) {
        return parseInt(a.style.left) - parseInt(b.style.left)
    });
    var c;
    c = "background-image: -webkit-linear-gradient(left,";
    for (var d = a.length - 1, b = 0; b < d; b++) c += a[b].firstChild.style.backgroundColor + " " + .4 * (parseInt(a[b].style.left) + 9) + "%,";
    c += a[b].firstChild.style.backgroundColor + " " +
        .4 * (parseInt(a[b].style.left) + 9) + "%);";
    this.htmlGradientButton.style.cssText = this.gradientHtmlElement.style.cssText = c;
    if (this.editableGradient instanceof XOS_RadialGradient) {
        c = "background-image: -webkit-radial-gradient(center, ellipse cover, ";
        for (b = 0; b < d; b++) c += a[b].firstChild.style.backgroundColor + " " + .4 * (parseInt(a[b].style.left) + 9) + "%,";
        c += a[b].firstChild.style.backgroundColor + " " + .4 * (parseInt(a[b].style.left) + 9) + "%);";
        this.htmlGradientButton.style.cssText = c
    }
};
_.onMouseDown = function(a) {
    "GRADIENT" == a.target.id ? (a = this.gradientHtmlElement.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt), this.addEditablePoint(a.x - 9, "#fff"), this.updateHtmlGradient(), this.editableGradient = this.appRef.activeDocumentController.setGradientOfSelectedObjects(this.htmlGradientButton.style.cssText)) : "COLOR" == a.target.id && (this.draggableElement = a.target.parentNode, a = this.draggableElement.getGlobalPosition(), a.x -= 150, this.appRef.colorPicker.showAndSet(a.x, a.y - 282, this.draggableElement.firstChild.style.backgroundColor,
        1, !1, Bind(this, this.onColorPickerChange), Bind(this, this.onColorPickerChangeEnd)), a = this.draggableElement.getPosition(), this.draggableElement.dragOffsetX = GLOBAL_MOUSE_LISTENER.mouseMovePt.x - a.x, this.draggableElement.dragOffsetY = GLOBAL_MOUSE_LISTENER.mouseMovePt.y - a.y, GLOBAL_MOUSE_LISTENER.objectTarget = this)
};
_.onGlobalMouseStartDrag = function(a) {
    this.appRef.colorPicker.hide()
};
_.onGlobalMouseEndDrag = function(a) {
    a = this.draggableElement.getGlobalPosition();
    this.appRef.colorPicker.showAndSet(a.x, a.y - 282, this.draggableElement.firstChild.style.backgroundColor, 1, !1, Bind(this, this.onColorPickerChange), Bind(this, this.onColorPickerChangeEnd));
    this.editableGradient = this.appRef.activeDocumentController.setGradientOfSelectedObjects(this.htmlGradientButton.style.cssText)
};
_.onGlobalMouseDrag = function(a) {
    a = this.htmlElement.globalToLocal(GLOBAL_MOUSE_LISTENER.mouseMovePt);
    this.draggableElement.style.display = 0 > a.x || a.x > this.htmlElement.offsetWidth || 0 > a.y || a.y > this.htmlElement.offsetHeight ? "none" : "block";
    a = GLOBAL_MOUSE_LISTENER.mouseMovePt.x - this.draggableElement.dragOffsetX; - 9 > a ? a = -9 : a > this.gradientHtmlElement.offsetWidth - 9 && (a = this.gradientHtmlElement.offsetWidth - 9);
    this.draggableElement.setPosition(a, 0);
    this.updateHtmlGradient()
};
_.onColorPickerChange = function(a) {
    a = (255 * a.rgb.r).toFixed(0) + "," + (255 * a.rgb.g).toFixed(0) + "," + (255 * a.rgb.b).toFixed(0) + "," + a.alpha;
    this.draggableElement.firstChild.style.cssText = "background-color: rgba(" + a + ");";
    this.updateHtmlGradient()
};
_.onColorPickerChangeEnd = function(a) {
    this.editableGradient = this.appRef.activeDocumentController.setGradientOfSelectedObjects(this.htmlGradientButton.style.cssText)
};

function EffectProperties2D_panel(a, b) {
    this.htmlElement = a;
    this.htmlElement.enableScrollingWithTouchMove();
    this.appRef = b;
    this.currentEditableEffect = null;
    this.textFields = {};
    this.colorButtons = {};
    this.init()
}
_ = EffectProperties2D_panel.prototype;
_.init = function() {
    this.connectTextFieldEventListener("shadowOffsetX");
    this.connectTextFieldEventListener("shadowOffsetY");
    this.connectTextFieldEventListener("shadowBlur");
    this.connectColorButtonsEventListener("shadowColor")
};
_.update = function(a) {
    this.appRef.inspectorPanel.panels.fillAndStrokeProperties2DPanel.htmlElement.style.display = "none";
    this.currentEditableEffect = a;
    for (name in this.textFields) this.textFields[name].value = this.currentEditableEffect[name];
    for (name in this.colorButtons) this.colorButtons[name].style.backgroundColor = this.currentEditableEffect[name]
};
_.connectTextFieldEventListener = function(a) {
    var b = this,
        c = this.textFields[a] = this.htmlElement.getChildById(a);
    CreateEventListener(c, "change", function() {
        var c = {};
        c[a] = this.value;
        b.appRef.activeDocumentController.setPropertiesOfObjects_undable(c, b.appRef.activeDocumentController.getListOfEqualEffectFromSelectedObjects(b.currentEditableEffect), !0)
    })
};
_.connectColorButtonsEventListener = function(a) {
    var b = this,
        c = this.colorButtons[a] = this.htmlElement.getChildById(a);
    CreateEventListener(c, "mousedown", function() {
        var d = this.getGlobalPosition();
        b.appRef.colorPicker.showAndSet(d.x - 160, d.y, c.style.backgroundColor, null, !0, null, function(d) {
            c.style.backgroundColor = d.currentColorAsHtmlStringRGBA();
            d = {};
            d[a] = c.style.backgroundColor;
            b.appRef.activeDocumentController.setPropertiesOfObjects_undable(d, b.appRef.activeDocumentController.getListOfEqualEffectFromSelectedObjects(b.currentEditableEffect), !0)
        })
    })
};
ExtendClass(AnimationProperties2D_panel, XOS_PropertyInspectorPanel);

function AnimationProperties2D_panel(a, b) {
    AnimationProperties2D_panel.baseConstructor.call(this, a, b)
}
_ = AnimationProperties2D_panel.prototype;
_.init = function() {
    AnimationProperties2D_panel.superClass.init.call(this);
    var a = this.htmlElement.querySelector("#ANIMATED_ELEMENT");
    CreateEventListener(this.htmlElement.querySelector("#ANIMATION_LIST"), "change", function(b) {
      //  console.log("onAnimationNameChange");
       // console.log(b.target.value);
        a.setAttribute("class", "animated " + b.target.value)
    })
};
_.update = function(a) {
    AnimationProperties2D_panel.superClass.update.call(this, a);
    this.appRef.inspectorPanel.panels.fillAndStrokeProperties2DPanel.htmlElement.style.display = "none"
};
_.onBeforeUpdate = function() {};
_.getValueForInputProperty = function(a) {
    var b = a.parentNode.parentNode;
    b.style.display = "block";
    switch (a.name) {
        case "cazz":
            break;
        default:
            if (null != this.editableObject[a.name] && void 0 != this.editableObject[a.name]) return this.editableObject[a.name];
            b.classList.contains("inspector_parameter") && (b.style.display = "none");
            return ""
    }
};
_.onInputPropertyChanged = function(a) {
    if (this.currentEditableGraphicObj) switch (a.name) {
        case "cazz":
            break;
        default:
            var b = {};
            b[a.name] = a.value;
            this.appRef.activeDocumentController.setPropertiesOfObjects_undable(b, [this.currentEditableGraphicObj]);
            this.appRef.animationsPanel.update(this.currentEditableGraphicObj.graphicObjectTarget)
    }
};

function FontsLibrary_panel(a, b, c) {
    this.htmlElement = a;
    this.htmlElement.enableScrollingWithTouchMove();
    this.appRef = b;
    this.libraryRootDir = c;
    this.init()
}
_ = FontsLibrary_panel.prototype;
_.init = function() {
    var a = this;
    CreateEventListener(document.querySelector("#FONTS_LIBRARY_button .popupMenu"), "mousedown", function(b) {
        b.target.hasAttribute("libraryType") ? "fontLibrary" == b.target.getAttribute("libraryType") && a.drawFontFamilyCategory(b.target.getAttribute("category")) : a.drawFontFamilyCategory("Sans Serif")
    });
    CreateEventListener(document.querySelector("#FONTS_LIBRARY_PANEL"), "mousedown", function(b) {
        if (b = b.target.getElementOrParentByNodeName("LI")) b = b.querySelector("a").innerHTML, a.appRef.activeDocumentController.setFontFamilyOfGraphicObjects(a.appRef.activeDocumentController.selectionList,
            b, !0,
            function() {
                a.appRef.activeDocumentController.showSelectionProperties()
            })
    });
    this.loadFontFamilyList()
};
_.loadLibrary = function(a) {
    var b = this.htmlElement.querySelector(".content");
    b.innerHTML = '<div class="contentLoadingProgress" ></div>';
    var c = this;
    SendAndLoad(a, null, function(a) {
        b.innerHTML = a.target.responseText;
        c.createLibraryItemsInteraction(b.getElementsByTagName("ul")[0])
    })
};
_.saveFontPreview = function() {
    var a = this.appRef.activeDocumentController.selectionList[0].font,
        b = this.appRef.activeDocumentController.currentPageToImage("png");
   // console.log("imageName " + a);
   // console.log("imageData " + b);
    SendAndLoad("service.php", {
        action: "saveImage",
        imageName: a,
        imageData: b
    }, function(a) {
       // console.log(a);
       // console.log("onSaveFontPreview")
    })
};
_.drawFontFamilyCategory = function(a) {
    var b = this.htmlElement;
    b.innerHTML = "";
    //for (var c, d = this.appRef.fontFamilyList.categories[a].items, e = d.length, f = 0; f < e; f++) a = document.createElement("li"), c = document.createElement("a"), c.innerHTML = d[f].family, a.appendChild(c), a.appendChild(c), b.appendChild(a)
	 for (var c, d = this.appRef.fontFamilyList.categories[a].items, e = d.length, f = 0; f < e; f++) a = document.createElement("li"), c = document.createElement("a"),c.setAttribute("style","font-family:"+d[f].family),c.innerHTML = d[f].family, a.appendChild(c), c = document.createElement("button"),c.setAttribute("onClick","chkFontView('"+d[f].family+"')" ),c.setAttribute("style","float:right" ),c.innerHTML='',c.src = this.libraryRootDir + "/fonts/_previews/" + d[f].family, a.appendChild(c), b.appendChild(a)
};
 function chkFontView (fonts) {
alert("You Have selected this font family:-"+fonts);
}
_.loadFontFamilyList = function() {
    var a = this;
    SendAndLoad(a.libraryRootDir + "js/ant_tool_js/webfonts.json", null, function(b) {
        a.appRef.fontFamilyList = JSON.parse(b.target.responseText);
        a.appRef.fontFamilyList.getFontFamilyDataByFontName = function(b) {
            for (var c = a.appRef.fontFamilyList.items, d = c.length, h = 0; h < d; h++)
                if (c[h].family == b) return c[h];
            return null
        };
        var c = a.appRef.fontFamilyList.items,
            d = {};
        a.appRef.fontFamilyList.categories = d;
        d["Sans Serif"] = {};
        d["Sans Serif"].items = [];
        d.Display = {};
        d.Display.items = [];
        d.Serif = {};
        d.Serif.items = [];
        d.Handwriting = {};
        d.Handwriting.items = [];
        d.Miscellanea = {};
        d.Miscellanea.items = [];
        SendAndLoad(a.libraryRootDir + "js/ant_tool_js/webfonts_categories.json", null, function(b) {
            b = JSON.parse(b.target.responseText);
            for (var f, g = c.length, h = 0; h < g; h++) f = c[h].family, -1 != b["Sans Serif"].indexOf(f) ? (c[h].category = "Sans Serif", d["Sans Serif"].items.push(c[h])) : -1 != b.Display.indexOf(f) ? (c[h].category = "Display", d.Display.items.push(c[h])) : -1 != b.Serif.indexOf(f) ? (c[h].category = "Serif", d.Serif.items.push(c[h])) :
                -1 != b.Handwriting.indexOf(f) ? (c[h].category = "Handwriting", d.Handwriting.items.push(c[h])) : (c[h].category = "Miscellanea", d.Miscellanea.items.push(c[h]));
            a.drawFontFamilyCategory("Sans Serif")
        })
    })
};

function GradientsLibrary_panel(a, b, c) {
    this.htmlElement = a;
    this.htmlElement.enableScrollingWithTouchMove();
    this.appRef = b;
    this.libraryRootDir = c;
    this.init()
}
_ = GradientsLibrary_panel.prototype;
_.init = function() {
    var a = this;
    CreateEventListener(document.querySelector("#GRADIENTS_LIBRARY_button .popupMenu"), "mousedown", function(b) {
        b.target.hasAttribute("url") && a.loadLibrary(b.target.getAttribute("url"))
    });
    CreateEventListener(document.querySelector("#GRADIENTS_LIBRARY_PANEL"), "mousedown", function(b) {
        b = b.target.getElementOrParentByNodeName("LI");
        a.appRef.activeDocumentController.setGradientOfSelectedObjects(b.style.cssText);
        a.appRef.activeDocumentController.showSelectionProperties()
    });
    this.loadLibrary(this.libraryRootDir +
        "js/ant_tool_js/linears.html")
};
_.loadLibrary = function(a) {
    var b = this.htmlElement;
    b.innerHTML = '<div class="contentLoadingProgress" ></div>';
    SendAndLoad(a, null, function(a) {
        b.innerHTML = a.target.responseText
    })
};

function PatternsLibrary_panel(a, b, c) {
    this.htmlElement = a;
    this.htmlElement.enableScrollingWithTouchMove();
    this.appRef = b;
    this.libraryRootDir = c;
    this.init()
}
_ = PatternsLibrary_panel.prototype;
_.init = function() {
    var a = this;
    CreateEventListener(document.querySelector("#PATTERNS_LIBRARY_button .popupMenu"), "mousedown", function(b) {
        b.target.hasAttribute("url") && a.loadLibrary(b.target.getAttribute("url"))
    });
    CreateEventListener(document.querySelector("#PATTERNS_LIBRARY_PANEL"), "mousedown", function(b) {
        b = b.target.getElementOrParentByNodeName("LI");
       a.appRef.activeDocumentController.setPatternOfSelectedObjects(b.getElementsByTagName("img")[0]);
        a.appRef.activeDocumentController.showSelectionProperties()
    });
    this.loadLibrary(base_url + "/patterncoller.html")
};
_.loadLibrary = function(a) {
    var b = this.htmlElement;
    b.innerHTML = '<div class="contentLoadingProgress" ></div>';
    SendAndLoad(a, null, function(a) {
        b.innerHTML = a.target.responseText
    })
};

function SymbolsLibrary2D_panel(a, b, c) {
    this.htmlElement = a;
    this.htmlElement.enableScrollingWithTouchMove();
    this.appRef = b;
    this.libraryRootDir = c;
    this.init()
}
_ = SymbolsLibrary2D_panel.prototype;
_.init = function() {
    var a = this;
    CreateEventListener(document.querySelector("#SYMBOLS_LIBRARY_button .popupMenu"), "mousedown", function(b) {
        b.target.hasAttribute("url") && a.loadLibrary(b.target.getAttribute("url"))
    });
    this.searchInput = this.htmlElement.querySelector("#SYMBOLS_LIBRARY_PANEL .header input");
    CreateEventListener(this.searchInput, "change", function(b) {
       // console.log("onSearchChange");
        a.loadOpenClipArtLibrary("http://openclipart.org/search/json/?query=" + a.searchInput.value + "&page=1&amount=20")
    });
        
         this.loadLibrary(base_url +"components.html");
    
};
_.loadLibrary = function(a) {
    var b = this.htmlElement.querySelector("ul");
    b.innerHTML = '<div class="contentLoadingProgress" ></div>';
    SendAndLoad(a, null, function(a) {
        function d(a) {
            SetTransferData(this)
        }
        b.innerHTML = a.target.responseText;
        a = b.querySelectorAll("img");
        for (var e = 0; e < a.length; e++) CreateEventListener(a[e], "dragstart", d)
    })
};
_.loadOpenClipArtLibrary = function(a) {
    var b = this.htmlElement.querySelector("ul");
    b.innerHTML = '<div class="contentLoadingProgress" ></div>';
    SendAndLoad(a, null, function(a) {
        function d(a) {
            SetTransferData(this)
        }
        a = JSON.parse(a.target.responseText);
        if ("success" == a.msg) {
            b.innerHTML = "";
            for (var e, f = a.payload, g = 0; g < f.length; g++) a = document.createElement("li"), e = document.createElement("a"), e.appendChild(document.createTextNode(f[g].title)), e = document.createElement("img"), e.setAttribute("src", f[g].svg.png_thumb),
                e.setAttribute("itemType", "svg"), e.setAttribute("downloadUrl", f[g].svg.url), a.appendChild(e), b.appendChild(a);
            f = b.querySelectorAll("img");
            for (g = 0; g < f.length; g++) CreateEventListener(f[g], "dragstart", d)
        }
    })
};

function AnimationsLibrary_panel(a, b, c) {
    this.htmlElement = a;
    this.appRef = b;
    this.animatedElement = this.htmlElement.querySelector("#ANIMATED_ELEMENT");
    this.init()
}
_ = AnimationsLibrary_panel.prototype;
_.init = function() {
    var a = this;
    CreateEventListener(this.htmlElement.querySelector(".animationList"), "change", function(b) {
       // console.log("onAnimationListChange");
       // console.log(b.target.value);
        a.animatedElement.setAttribute("class", "animated " + b.target.value)
    })
};
ExtendClass(Layers2D_panel, XOS_ListView);

function Layers2D_panel(a, b) {
    Layers2D_panel.baseConstructor.call(this, a);
    this.appRef = b;
    var c = this.htmlViewElement.parentNode;
    CreateEventListener(c.getChildById("CREATE_LAYER_button"), "mousedown", this.createLayer, this);
    CreateEventListener(c.getChildById("REMOVE_LAYER_button"), "mousedown", this.removeLayer, this);
    CreateEventListener(c.getChildById("MOVE_LAYER_UP_button"), "mousedown", this.moveLayerUp, this);
    CreateEventListener(c.getChildById("MOVE_LAYER_DOWN_button"), "mousedown", this.moveLayerDown, this);
    CreateEventListener(c.getChildById("DUPLICATE_LAYER_button"), "mousedown", this.duplicateLayer, this);
    CreateEventListener(c.getChildById("MOVE_SELECTION_IN_LAYER_button"), "mousedown", this.moveSelectedGraphicObjectsInCurrentLayer, this)
    $("#LAYERS_2D_PANEL li").on("click",function()
       {
        showHideToolBox();
    });
}
function showHideToolBox()
{

     var a=$("#LAYERS_2D_PANEL li.selected").text();
          if(a=="workspace")
          {
                   $("#LAYERS_EDITING_TOOLBAR").hide();
          }
         else
         {
           $("#LAYERS_EDITING_TOOLBAR").show();
         }
      
}
_ = Layers2D_panel.prototype;
_.createLayer = function(a) {
    this.selectedItems[0].layerRef.displayRef.addLayer_undable("untitled layer");
    // $("#LAYERS_2D_PANEL").find("li").last().hide();

};
_.duplicateLayer = function(a)
{
    //alert("dublicate layer called");
    this.copyLayer(a);
};
_.copyLayer = function(a)
{
    //console.log(a);
    layerContent[0]=this.selectedItems[0].layerRef.displayRef.currentLayer.children;
  //console.log(this.selectedItems[0]);
   //this.myLyaerContents(layerContent);
    this.selectedItems[0].layerRef.displayRef.copyNewLayers("untitled layer");
}
_.pasteLayer = function(a)
{
    
};
_.removeLayer = function(a) {
    if (0 == this.selectedItems.length) alert("The layer you want remove must be selected.");
    else {
        a = this.selectedItems[0].layerRef.displayRef;
        a.removeLayer_undable(this.selectedItems[0].layerRef);
        var b = a.getLayers();
        a.currentLayer = b[b.length - 1];
        this.update(a)
    }
};
_.moveLayerUp = function(a) {
    a = this.selectedItems[0].layerRef;
    a.displayRef.moveLayer_undable(a, "up")
};
_.moveLayerDown = function(a) {
    a = this.selectedItems[0].layerRef;
    a.displayRef.moveLayer_undable(a, "down")
};
_.moveSelectedGraphicObjectsInCurrentLayer = function(a) {
    this.selectedItems[0].layerRef.displayRef.moveSelectedGraphicObjectsInCurrentLayer()
};
_.onEndEditItemName = function(a) {
    console.log(a.value);
    var b = this.selectedItems[0].layerRef,
        c = this;
    b.displayRef.setPropertiesOfObjects_undable({
        name: a.value
    }, [b], null, null, function() {
        c.update(b.displayRef)
    });
    Layers2D_panel.superClass.onEndEditItemName.call(this, a)
};
_.onMouseDown = function(a) {
    if ("LI" == a.target.parentElement.nodeName) {
        var b = a.target.parentElement,
            c = b.layerRef,
            d = b.layerRef.displayRef;
        d.currentLayer = c;
       // console.log(d.currentLayer);
       // convertLayerToImage(a);
        if ("DIV" == a.target.nodeName) {
            var e = this;
            switch (a.target.getAttribute("id")) {
                case "visibleState":
                    d.setPropertiesOfObjects_undable({
                        visible: c.visible ? !1 : !0
                    }, [c], !0, !0, function() {
                        e.update(d)
                    });
                    this.update(d);
                    d.deselectAllGraphicObjects();
                    break;
                case "lockedState":
                    d.setPropertiesOfObjects_undable({
                        isLocked: c.isLocked ? !1 : !0
                    }, [c], !0, !0, function() {
                        e.update(d)
                    });
                    this.update(d);
                    d.deselectAllGraphicObjects();
                    break;
                default:
                    Layers2D_panel.superClass.onMouseDown.call(this, a)
            }
        }
    }
};
_.update = function(a) {
    if (!this.locked) {
        this.htmlViewElement.innerHTML = "";
        a = a.getLayers();
        var b = a.length,
            c, d = a[b - 1];
        d.displayRef.clearIsolateLayerEditingLabels();
        if (d.isolateObjectRef) {
            for (c = 0; c < b; c++) a[c].isolateObjectRef && a[c].displayRef.addIsolateLayerEditingLabel(a[c].name + " > ");
            d.displayRef.addIsolateLayerEditingLabel("content")
        } else
            for (this.selectedItems = [], c = b = a.length - 1; - 1 < c; c--) this.createItem(a[c])
    }
};
_.createItem = function(a) {
    var b = document.createElement("li");

    b.layerRef = a;
    a.visible ? b.classList.add("visibleTrue") : b.classList.add("visibleFalse");
    a.isLocked ? b.classList.add("lockedTrue") : b.classList.add("lockedFalse");
    a.displayRef.currentLayer == a && (b.classList.add("selected"), this.selectedItems.push(b));
    element_div = document.createElement("div");
    element_div.setAttribute("id", "visibleState");
    b.appendChild(element_div);
    element_div = document.createElement("div");
    element_div.setAttribute("id", "lockedState");
    b.appendChild(element_div);
    element_div = document.createElement("div");
    element_div.setAttribute("id", "title");
    element_div.appendChild(document.createTextNode(a.name));
    b.appendChild(element_div);
    this.htmlViewElement.appendChild(b)
    if ($(b).text() == 'workspace') {
       b.setAttribute('id','myWorkspace'); 
        // $("#myWorkspace").removeClass('lockedTrue');
         //$("#myWorkspace").addClass('lockedFalse');
    }else{}

    /*$("#myWorkspace").click(function()
    {
        $("#LAYERS_EDITING_TOOLBAR").hide();
    });*/
     if($("#LAYERS_2D_PANEL li.selected").text()=="workspace")
     {
       $("#REMOVE_LAYER_button").hide();
     }
     else
     {
       $("#REMOVE_LAYER_button").show();
     }
     
$("#LAYERS_2D_PANEL li").click(function()
{
     if($("#LAYERS_2D_PANEL li.selected").text()=="workspace")
     {
       $("#REMOVE_LAYER_button").hide();
     }
     else
     {
       $("#REMOVE_LAYER_button").show();
     }
     //$("#myWorkspace div#lockedState").hide();
});
   
    //alert($(b).text());
};
ExtendClass(Effects2D_panel, XOS_ListView);

function Effects2D_panel(a, b) {
    Effects2D_panel.baseConstructor.call(this, a);
    this.appRef = b;
    CreateEventListener(this.htmlViewElement.parentNode.getChildById("REMOVE_EFFECT_button"), "mousedown", this.removeItem, this)
}
_ = Effects2D_panel.prototype;
_.onMouseDown = function(a) {
    Effects2D_panel.superClass.onMouseDown.call(this, a);
    var b = this.selectedItems[0].effectRef;
    this.appRef.inspectorPanel.update(b);
    var c = b.graphicObjectTarget.displayRef;
    if ("DIV" == a.target.nodeName) switch (a.target.getAttribute("id")) {
        case "visibleState":
            a = a.target.parentElement, a.classList.remove("visibleFalse"), b.visible ? (a.classList.remove("visibleTrue"), a.classList.add("visibleFalse"), a = !1) : (a.classList.remove("visibleFalse"), a.classList.add("visibleTrue"), a = !0), c.setPropertiesOfObjects_undable({
                    visible: a
                },
                c.getListOfEqualEffectFromSelectedObjects(b), !0, !0)
    }
};
_.removeItem = function(a) {
    0 == this.selectedItems.length ? alert("Please select the effect to remove!") : (a = this.selectedItems[0].effectRef.graphicObjectTarget, a.displayRef.removeEffectFromGraphicObject_undable(this.selectedItems[0].effectRef), this.update(a))
};
_.update = function(a) {
    this.selectedItems = [];
    a = a.effectList;
    var b = a.length;
    this.htmlViewElement.innerHTML = 0 == b ? '<li><div id="title">No effects.</div></li>' : "";
    for (var c = 0; c < b; c++) this.createItem(a[c])
};
_.createItem = function(a) {
    var b = document.createElement("li");
    a.visible ? b.classList.add("visibleTrue") : b.classList.add("visibleFalse");
    b.effectRef = a;
    element_div = document.createElement("div");
    element_div.setAttribute("id", "visibleState");
    b.appendChild(element_div);
    element_div = document.createElement("div");
    element_div.setAttribute("id", "title");
    element_div.appendChild(document.createTextNode(a.name));
    b.appendChild(element_div);
    this.htmlViewElement.appendChild(b)
};
ExtendClass(Animations2D_panel, XOS_ListView);

function Animations2D_panel(a, b) {
    Animations2D_panel.baseConstructor.call(this, a);
    this.appRef = b;
    var c = this.htmlViewElement.parentNode;
    CreateEventListener(c.getChildById("CREATE_ANIMATION_button"), "mousedown", this.addItem, this);
    CreateEventListener(c.getChildById("REMOVE_ANIMATION_button"), "mousedown", this.removeItem, this)
}
_ = Animations2D_panel.prototype;
_.addItem = function(a) {
    a = new XOS_CSSAnimation;
    this.appRef.activeDocumentController.addAnimationToSelectedObjects_undable(a);
    this.appRef.activeDocumentController.showSelectionProperties()
};
_.onMouseDown = function(a) {
    Animations2D_panel.superClass.onMouseDown.call(this, a);
    this.appRef.inspectorPanel.update(this.selectedItems[0].animationRef)
};
_.removeItem = function(a) {
    0 == this.selectedItems.length ? alert("Please select the effect to remove!") : (a = this.selectedItems[0].animationRef.graphicObjectTarget, a.displayRef.removeAnimationFromGraphicObject_undable(this.selectedItems[0].animationRef), this.update(a))
};
_.update = function(a) {
    this.selectedItems = [];
    a = a.animationList;
    var b = a.length;
    this.htmlViewElement.innerHTML = 0 == b ? '<li><div id="title">No animations.</div></li>' : "";
    for (var c = 0; c < b; c++) this.createItem(a[c])
};
_.createItem = function(a) {
    var b = document.createElement("li");
    a.visible ? b.classList.add("visibleTrue") : b.classList.add("visibleFalse");
    b.animationRef = a;
    element_div = document.createElement("div");
    element_div.setAttribute("class", "icon");
    b.appendChild(element_div);
    element_div = document.createElement("div");
    element_div.setAttribute("id", "title");
    element_div.appendChild(document.createTextNode(a.animationName + " - delay:" + a.delay + " - duration:" + a.duration));
    b.appendChild(element_div);
    this.htmlViewElement.appendChild(b)
};
(function() {
    function a(a, b) {
        if (!a) throw Error(b);
    }

    function b() {
        this.commands = [];
        this.fill = "black";
        this.stroke = null;
        this.strokeWidth = 1
    }

    function c(a, b, c, d, e) {
        a.beginPath();
        a.moveTo(b, c);
        a.lineTo(d, e);
        a.stroke()
    }

    function d(a, b) {
        this.dataView = a;
        this.offset = b;
        this.relativeOffset = 0
    }

    function e(a, b) {
        this.font = a;
        this.index = b;
        this.xMin = this.yMin = this.xMax = this.yMax = this.numberOfContours = 0;
        this.points = []
    }

    function f() {
        this.supported = !0;
        this.glyphs = []
    }

    function g(a, b, c, d, e) {
        1 === (b >> d & 1) ? (a = a.parseByte(), 1 !== (b >>
            e & 1) && (a = -a), a = c + a) : a = 1 === (b >> e & 1) ? c : c + a.parseShort();
        return a
    }

    function h(b, c, f, h) {
        var k, l, v, m, p;
        b = new d(b, c);
        f = new e(h, f);
        f.numberOfContours = b.parseShort();
        f.xMin = b.parseShort();
        f.yMin = b.parseShort();
        f.xMax = b.parseShort();
        f.yMax = b.parseShort();
        if (0 < f.numberOfContours) {
            v = f.endPointIndices = [];
            for (k = 0; k < f.numberOfContours; k += 1) v.push(b.parseUShort());
            f.instructionLength = b.parseUShort();
            f.instructions = [];
            for (k = 0; k < f.instructionLength; k += 1) f.instructions.push(b.parseByte());
            m = v[v.length - 1] + 1;
            h = [];
            for (k =
                0; k < m; k += 1)
                if (c = b.parseByte(), h.push(c), 1 === (c >> 3 & 1))
                    for (p = b.parseByte(), l = 0; l < p; l += 1) h.push(c), k += 1;
            a(h.length === m, "Bad flags.");
            if (0 < v.length) {
                l = [];
                if (0 < m) {
                    for (k = 0; k < m; k += 1) c = h[k], p = {}, p.onCurve = 1 === (c >> 0 & 1), p.lastPointOfContour = 0 <= v.indexOf(k), l.push(p);
                    for (k = v = 0; k < m; k += 1) c = h[k], p = l[k], p.x = g(b, c, v, 1, 4), v = p.x;
                    for (k = v = 0; k < m; k += 1) c = h[k], p = l[k], p.y = g(b, c, v, 2, 5), v = p.y
                }
                f.points = l
            } else f.points = []
        } else if (0 === f.numberOfContours) f.points = [];
        else
            for (f.isComposite = !0, f.points = [], f.components = [], h = !0; h;) c = {}, h = b.parseUShort(), c.glyphIndex = b.parseUShort(), 1 === (h >> 0 & 1) ? (k = b.parseShort(), m = b.parseShort()) : (k = b.parseByte(), m = b.parseByte()), c.dx = k, c.dy = m, 1 === (h >> 3 & 1) ? b.parseShort() : 1 === (h >> 6 & 1) ? (b.parseShort(), b.parseShort()) : 1 === (h >> 7 & 1) && (b.parseShort(), b.parseShort(), b.parseShort(), b.parseShort()), f.components.push(c), h = 1 === (h >> 5 & 1);
        return f
    }

    function k(a, b) {
        p = p || require("fs");
        p.readFile(a, function(a, c) {
            if (a) return b(a.message);
            for (var d = new ArrayBuffer(c.length), e = new Uint8Array(d), f = 0; f < c.length; ++f) e[f] =
                c[f];
            b(null, d)
        })
    }

    function l(a, b) {
        var c = new XMLHttpRequest;
        c.open("get", a, !0);
        c.responseType = "arraybuffer";
        c.onload = function() {
            return 200 !== c.status ? b("Font could not be loaded: " + c.statusText) : b(null, c.response)
        };
        c.send()
    }
    var q, m;
    q = {};
    b.prototype.moveTo = function(a, b) {
        this.commands.push({
            type: "M",
            x: a,
            y: b
        })
    };
    b.prototype.lineTo = function(a, b) {
        this.commands.push({
            type: "L",
            x: a,
            y: b
        })
    };
    b.prototype.curveTo = b.prototype.bezierCurveTo = function(a, b, c, d, e, f) {
        this.commands.push({
            type: "C",
            x1: a,
            y1: b,
            x2: c,
            y2: d,
            x: e,
            y: f
        })
    };
    b.prototype.quadTo = b.prototype.quadraticCurveTo = function(a, b, c, d) {
        this.commands.push({
            type: "Q",
            x1: a,
            y1: b,
            x: c,
            y: d
        })
    };
    b.prototype.close = b.prototype.closePath = function() {
        this.commands.push({
            type: "Z"
        })
    };
    b.prototype.extend = function(a) {
        a.commands && (a = a.commands);
        Array.prototype.push.apply(this.commands, a)
    };
    b.prototype.draw = function(a) {
        var b, c;
        a.beginPath();
        for (b = 0; b < this.commands.length; b += 1) c = this.commands[b], "M" === c.type ? a.moveTo(c.x, c.y) : "L" === c.type ? a.lineTo(c.x, c.y) : "C" === c.type ? a.bezierCurveTo(c.x1,
            c.y1, c.x2, c.y2, c.x, c.y) : "Q" === c.type ? a.quadraticCurveTo(c.x1, c.y1, c.x, c.y) : "Z" === c.type && a.closePath();
        this.fill && (a.fillStyle = this.fill, a.fill());
        this.stroke && (a.strokeStyle = this.stroke, a.lineWidth = this.strokeWidth, a.stroke())
    };
    m = {
        "byte": 1,
        uShort: 2,
        "short": 2,
        uLong: 4,
        fixed: 4,
        longDateTime: 8,
        tag: 4
    };
    d.prototype.parseByte = function() {
        var a = this.dataView.getUint8(this.offset + this.relativeOffset);
        this.relativeOffset += 1;
        return a
    };
    d.prototype.parseUShort = function() {
        var a = this.dataView.getUint16(this.offset +
            this.relativeOffset, !1);
        this.relativeOffset += 2;
        return a
    };
    d.prototype.parseShort = function() {
        var a = this.dataView.getInt16(this.offset + this.relativeOffset, !1);
        this.relativeOffset += 2;
        return a
    };
    d.prototype.parseULong = function() {
        var a = this.dataView.getUint32(this.offset + this.relativeOffset, !1);
        this.relativeOffset += 4;
        return a
    };
    d.prototype.skip = function(a, b) {
        void 0 === b && (b = 1);
        this.relativeOffset += m[a] * b
    };
    e.prototype.getContours = function() {
        var b, c, d, e;
        b = [];
        c = [];
        for (d = 0; d < this.points.length; d += 1) e = this.points[d],
            c.push(e), e.lastPointOfContour && (b.push(c), c = []);
        a(0 === c.length, "There are still points left in the current contour.");
        return b
    };
    e.prototype.getPath = function(a, c, d) {
        var e, f, g, h, k, l, m, p, q, A;
        a = void 0 !== a ? a : 0;
        c = void 0 !== c ? c : 0;
        d = 1 / this.font.unitsPerEm * (void 0 !== d ? d : 72);
        e = new b;
        if (!this.points) return e;
        f = this.getContours();
        for (g = 0; g < f.length; g += 1) {
            k = f[g];
            m = k[0];
            A = null;
            for (h = 0; h < k.length; h += 1)
                if (l = k[h], p = 0 === h ? k[k.length - 1] : k[h - 1], 0 === h) l.onCurve ? (e.moveTo(a + l.x * d, c + -l.y * d), A = null) : (A = q = {
                    x: (p.x + l.x) / 2,
                    y: (p.y + l.y) / 2
                }, e.moveTo(a + q.x * d, c + -q.y * d));
                else if (p.onCurve && l.onCurve) e.lineTo(a + l.x * d, c + -l.y * d);
            else if (p.onCurve && !l.onCurve) A = l;
            else if (p.onCurve || l.onCurve)
                if (!p.onCurve && l.onCurve) e.quadraticCurveTo(a + A.x * d, c + -A.y * d, a + l.x * d, c + -l.y * d), A = null;
                else throw Error("Invalid state.");
            else q = {
                x: (p.x + l.x) / 2,
                y: (p.y + l.y) / 2
            }, e.quadraticCurveTo(a + p.x * d, c + -p.y * d, a + q.x * d, c + -q.y * d), A = l;
            A ? e.quadraticCurveTo(a + A.x * d, c + -A.y * d, a + m.x * d, c + -m.y * d) : e.lineTo(a + m.x * d, c + -m.y * d)
        }
        e.closePath();
        return e
    };
    e.prototype.draw =
        function(a, b, c, d) {
            this.getPath(b, c, d).draw(a)
        };
    e.prototype.drawPoints = function(a, b, c, d) {
        function e(b, c, d, f) {
            var g, h = 2 * Math.PI;
            a.beginPath();
            for (g = 0; g < b.length; g += 1) a.moveTo(c + b[g].x * f, d + -b[g].y * f), a.arc(c + b[g].x * f, d + -b[g].y * f, 2, 0, h, !1);
            a.closePath();
            a.fill()
        }
        var f, g, h, k, l;
        b = void 0 !== b ? b : 0;
        c = void 0 !== c ? c : 0;
        d = 1 / this.font.unitsPerEm * (void 0 !== d ? d : 24);
        if (f = this.points) {
            k = [];
            l = [];
            for (g = 0; g < f.length; g += 1) h = f[g], h.onCurve ? k.push(h) : l.push(h);
            a.fillStyle = "blue";
            e(k, b, c, d);
            a.fillStyle = "red";
            e(l, b, c, d)
        }
    };
    e.prototype.drawMetrics =
        function(a, b, d, e) {
            b = void 0 !== b ? b : 0;
            d = void 0 !== d ? d : 0;
            e = 1 / this.font.unitsPerEm * (void 0 !== e ? e : 24);
            a.lineWidth = 1;
            a.strokeStyle = "black";
            c(a, b, -1E4, b, 1E4);
            c(a, -1E4, d, 1E4, d);
            a.strokeStyle = "blue";
            c(a, b + this.xMin * e, -1E4, b + this.xMin * e, 1E4);
            c(a, b + this.xMax * e, -1E4, b + this.xMax * e, 1E4);
            c(a, -1E4, d + -this.yMin * e, 1E4, d + -this.yMin * e);
            c(a, -1E4, d + -this.yMax * e, 1E4, d + -this.yMax * e);
            a.strokeStyle = "green";
            c(a, b + this.advanceWidth * e, -1E4, b + this.advanceWidth * e, 1E4)
        };
    f.prototype.charToGlyphIndex = function(a) {
        var b, c, d, e;
        b = this.cmap;
        a = a.charCodeAt(0);
        c = 0;
        for (e = b.length - 1; c < e;) d = c + e + 1 >> 1, a < b[d].start ? e = d - 1 : c = d;
        return b[c].start <= a && a <= b[c].end ? b[c].idDelta + (b[c].ids ? b[c].ids[a - b[c].start] : a) & 65535 : 0
    };
    f.prototype.charToGlyph = function(b) {
        var c, d;
        c = this.charToGlyphIndex(b);
        d = this.glyphs[c];
        a(void 0 !== d, "Could not find glyph for character " + b + " glyph index " + c);
        return d
    };
    f.prototype.stringToGlyphs = function(a) {
        var b, c, d;
        d = [];
        for (b = 0; b < a.length; b += 1) c = a[b], d.push(this.charToGlyph(c));
        return d
    };
    f.prototype.getKerningValue = function(a,
        b) {
        a = a.index || a;
        b = b.index || b;
        return this.kerningPairs[a + "," + b] || 0
    };
    f.prototype._eachGlyph = function(a, b, c, d, e, f) {
        var g, h, k, l;
        if (this.supported)
            for (b = void 0 !== b ? b : 0, c = void 0 !== c ? c : 0, d = void 0 !== d ? d : 72, e = e || {}, g = void 0 === e.kerning ? !0 : e.kerning, h = 1 / this.unitsPerEm * d, a = this.stringToGlyphs(a), k = 0; k < a.length; k += 1) l = a[k], f(l, b, c, d, e), l.advanceWidth && (b += l.advanceWidth * h), g && k < a.length - 1 && (l = this.getKerningValue(l, a[k + 1]), b += l * h)
    };
    f.prototype.getPath = function(a, c, d, e, f) {
        var g = new b;
        this._eachGlyph(a, c, d, e,
            f,
            function(a, b, c, d) {
                a = a.getPath(b, c, d);
                g.extend(a)
            });
        return g
    };
    f.prototype.draw = function(a, b, c, d, e, f) {
        this.getPath(b, c, d, e, f).draw(a)
    };
    f.prototype.drawPoints = function(a, b, c, d, e, f) {
        this._eachGlyph(b, c, d, e, f, function(b, c, d, e) {
            b.drawPoints(a, c, d, e)
        })
    };
    f.prototype.drawMetrics = function(a, b, c, d, e, f) {
        this._eachGlyph(b, c, d, e, f, function(b, c, d, e) {
            b.drawMetrics(a, c, d, e)
        })
    };
    var p;
    q.parse = function(b) {
        var c, g, k, l, m, p, q, O, M, R, S, A;
        c = new f;
        b = new DataView(b, 0);
        g = b.getInt16(0, !1);
        k = b.getUint16(2, !1);
        g += k / 65535;
        a(1 ===
            g, "Unsupported OpenType version " + g);
        g = b.getUint16(4, !1);
        l = 12;
        for (k = 0; k < g; k += 1) {
            m = b;
            var W = l;
            p = "";
            for (var u = void 0, u = W; u < W + 4; u += 1) p += String.fromCharCode(m.getInt8(u));
            m = p;
            p = b.getUint32(l + 8, !1);
            switch (m) {
                case "cmap":
                    m = c;
                    var W = b,
                        D = u = void 0,
                        U = void 0,
                        da = void 0,
                        oa = void 0,
                        sa = oa = u = da = U = u = void 0,
                        D = void 0,
                        u = W.getUint16(p, !1);
                    a(0 === u, "cmap table version should be 0.");
                    D = W.getUint16(p + 2, !1);
                    U = -1;
                    for (u = 0; u < D; u += 1)
                        if (da = W.getUint16(p + 4 + 8 * u, !1), oa = W.getUint16(p + 4 + 8 * u + 2, !1), 3 === da && (1 === oa || 0 === oa)) {
                            U = W.getUint32(p +
                                4 + 8 * u + 4, !1);
                            break
                        }
                    if (-1 === U) W = null;
                    else {
                        D = new d(W, p + U);
                        u = D.parseUShort();
                        a(4 === u, "Only format 4 cmap tables are supported.");
                        D.skip("uShort", 2);
                        U = D.parseUShort() / 2;
                        D.skip("uShort", 3);
                        da = [];
                        for (u = 0; u < U; u += 1) da[u] = {
                            end: D.parseUShort()
                        };
                        D.skip("uShort");
                        for (u = 0; u < U; u += 1) da[u].start = D.parseUShort(), da[u].length = da[u].end - da[u].start;
                        for (u = 0; u < U; u += 1) da[u].idDelta = D.parseShort();
                        for (u = 0; u < U; u += 1)
                            if (sa = D.parseUShort(), 0 < sa) {
                                da[u].ids = [];
                                for (oa = 0; oa < da[u].length; oa += 1) da[u].ids[oa] = W.getUint16(p + D.relativeOffset +
                                    sa, !1), sa += 2;
                                da[u].idDelta = D.parseUShort()
                            }
                        W = da
                    }
                    m.cmap = W;
                    c.cmap || (c.cmap = [], c.supported = !1);
                    break;
                case "head":
                    S = b.getUint32(p + 12, !1);
                    a(1594834165 === S, "Font header has wrong magic number.");
                    c.unitsPerEm = b.getUint16(p + 18, !1);
                    S = b.getUint16(p + 50, !1);
                    break;
                case "hhea":
                    c.ascender = b.getInt16(p + 4, !1);
                    c.descender = b.getInt16(p + 6, !1);
                    c.numberOfHMetrics = b.getUint16(p + 34, !1);
                    break;
                case "hmtx":
                    q = p;
                    break;
                case "maxp":
                    c.numGlyphs = A = b.getUint16(p + 4, !1);
                    break;
                case "glyf":
                    O = p;
                    break;
                case "loca":
                    M = p;
                    break;
                case "kern":
                    R =
                        p
            }
            l += 16
        }
        if (O && M) {
            S = 0 === S;
            g = A;
            M = new d(b, M);
            k = S ? M.parseUShort : M.parseULong;
            A = [];
            for (m = 0; m < g + 1; m += 1) l = k.call(M), S && (l *= 2), A.push(l);
            S = O;
            O = [];
            for (M = 0; M < A.length - 1; M += 1) g = A[M], k = A[M + 1], g !== k ? O.push(h(b, S + g, M, c)) : O.push(new e(c, M));
            for (M = 0; M < O.length; M += 1)
                if (S = O[M], S.isComposite)
                    for (A = 0; A < S.components.length; A += 1)
                        if (l = S.components[A], g = O[l.glyphIndex], g.points) {
                            g = g.points;
                            k = l.dx;
                            l = l.dy;
                            p = p = W = m = void 0;
                            m = [];
                            for (W = 0; W < g.length; W += 1) p = g[W], p = {
                                    x: p.x + k,
                                    y: p.y + l,
                                    onCurve: p.onCurve,
                                    lastPointOfContour: p.lastPointOfContour
                                },
                                m.push(p);
                            g = m;
                            S.points.push.apply(S.points, g)
                        }
            c.glyphs = O;
            O = c.numberOfHMetrics;
            M = c.numGlyphs;
            A = c.glyphs;
            var ka, Y;
            q = new d(b, q);
            for (S = 0; S < M; S += 1) S < O && (ka = q.parseUShort(), Y = q.parseShort()), g = A[S], g.advanceWidth = ka, g.leftSideBearing = Y;
            if (R) {
                ka = {};
                R = new d(b, R);
                b = R.parseUShort();
                a(0 === b, "Unsupported kern table version.");
                b = R.parseUShort();
                a(1 === b, "Unsupported number of kern sub-tables.");
                b = R.parseUShort();
                a(0 === b, "Unsupported kern sub-table version.");
                R.skip("uShort", 2);
                b = R.parseUShort();
                R.skip("uShort",
                    3);
                for (Y = 0; Y < b; Y += 1) q = R.parseUShort(), O = R.parseUShort(), M = R.parseShort(), ka[q + "," + O] = M;
                c.kerningPairs = ka
            } else c.kerningPairs = {}
        } else c.supported = !1;
        return c
    };
    q.load = function(a, b) {
        ("undefined" !== typeof module && module.exports ? k : l)(a, function(a, c) {
            if (a) return b(a);
            var d = q.parse(c);
            return d.supported ? b(null, d) : b("Font is not supported (is this a Postscript font?)")
        })
    };
    "function" === typeof define && define.amd ? define([], function() {
            return q
        }) : "object" === typeof module && module.exports ? module.exports = q : this.opentype =
        q
}).call(this);

function htmlToText(a, b) {
    var c = a;
    b && b.preprocessing && (c = b.preprocessing(c));
    c = c.replace(/(?:\n|\r\n|\r)/ig, " ").replace(/<\s*script[^>]*>[\s\S]*?<\/script>/mig, "").replace(/<\s*style[^>]*>[\s\S]*?<\/style>/mig, "").replace(/\x3c!--.*?--\x3e/mig, "").replace(/<!DOCTYPE.*?>/ig, "");
    b && b.tagreplacement && (c = b.tagreplacement(c));
    var d = "p h[1-6] dl dt dd ol ul dir address blockquote center hr pre form textarea table".split(" "),
        e = "div li del ins fieldset legend tr th caption thead tbody tfoot".split(" ");
    for (i = 0; i < d.length; i++) var f = RegExp("</?\\s*" + d[i] + "[^>]*>", "ig"),
        c = c.replace(f, "\n\n");
    for (i = 0; i < e.length; i++) f = RegExp("<\\s*" + e[i] + "[^>]*>", "ig"), c = c.replace(f, "\n");
    c = c.replace(/<\s*br[^>]*\/?\s*>/ig, "\n");
    c = c.replace(/(<([^>]+)>)/ig, "").replace(/([^\n\S]+)\n/g, "\n").replace(/([^\n\S]+)$/, "").replace(/^\n+/, "").replace(/\n+$/, "").replace(/&([^;]+);/g, decodeHtmlEntity);
    b && b.postprocessing && (c = b.postprocessing(c));
    return c
}

function decodeHtmlEntity(a, b) {
    var c;
    c = "#" == b.substr(0, 1) ? "x" == b.substr(1, 1) ? parseInt(b.substr(2), 16) : parseInt(b.substr(1), 10) : ENTITIES_MAP[b];
    return void 0 === c || NaN === c ? "&" + b + ";" : String.fromCharCode(c)
}
var ENTITIES_MAP = {
    nbsp: 160,
    iexcl: 161,
    cent: 162,
    pound: 163,
    curren: 164,
    yen: 165,
    brvbar: 166,
    sect: 167,
    uml: 168,
    copy: 169,
    ordf: 170,
    laquo: 171,
    not: 172,
    shy: 173,
    reg: 174,
    macr: 175,
    deg: 176,
    plusmn: 177,
    sup2: 178,
    sup3: 179,
    acute: 180,
    micro: 181,
    para: 182,
    middot: 183,
    cedil: 184,
    sup1: 185,
    ordm: 186,
    raquo: 187,
    frac14: 188,
    frac12: 189,
    frac34: 190,
    iquest: 191,
    Agrave: 192,
    Aacute: 193,
    Acirc: 194,
    Atilde: 195,
    Auml: 196,
    Aring: 197,
    AElig: 198,
    Ccedil: 199,
    Egrave: 200,
    Eacute: 201,
    Ecirc: 202,
    Euml: 203,
    Igrave: 204,
    Iacute: 205,
    Icirc: 206,
    Iuml: 207,
    ETH: 208,
    Ntilde: 209,
    Ograve: 210,
    Oacute: 211,
    Ocirc: 212,
    Otilde: 213,
    Ouml: 214,
    times: 215,
    Oslash: 216,
    Ugrave: 217,
    Uacute: 218,
    Ucirc: 219,
    Uuml: 220,
    Yacute: 221,
    THORN: 222,
    szlig: 223,
    agrave: 224,
    aacute: 225,
    acirc: 226,
    atilde: 227,
    auml: 228,
    aring: 229,
    aelig: 230,
    ccedil: 231,
    egrave: 232,
    eacute: 233,
    ecirc: 234,
    euml: 235,
    igrave: 236,
    iacute: 237,
    icirc: 238,
    iuml: 239,
    eth: 240,
    ntilde: 241,
    ograve: 242,
    oacute: 243,
    ocirc: 244,
    otilde: 245,
    ouml: 246,
    divide: 247,
    oslash: 248,
    ugrave: 249,
    uacute: 250,
    ucirc: 251,
    uuml: 252,
    yacute: 253,
    thorn: 254,
    yuml: 255,
    quot: 34,
    amp: 38,
    lt: 60,
    gt: 62,
    OElig: 338,
    oelig: 339,
    Scaron: 352,
    scaron: 353,
    Yuml: 376,
    circ: 710,
    tilde: 732,
    ensp: 8194,
    emsp: 8195,
    thinsp: 8201,
    zwnj: 8204,
    zwj: 8205,
    lrm: 8206,
    rlm: 8207,
    ndash: 8211,
    mdash: 8212,
    lsquo: 8216,
    rsquo: 8217,
    sbquo: 8218,
    ldquo: 8220,
    rdquo: 8221,
    bdquo: 8222,
    dagger: 8224,
    Dagger: 8225,
    permil: 8240,
    lsaquo: 8249,
    rsaquo: 8250,
    euro: 8364
};
window.CodeMirror = function() {
    function a(c, d) {
        if (!(this instanceof a)) return new a(c, d);
        this.options = d = d || {};
        for (var e in Fc) !d.hasOwnProperty(e) && Fc.hasOwnProperty(e) && (d[e] = Fc[e]);
        m(d);
        e = this.display = b(c, "string" == typeof d.value ? 0 : d.value.first);
        e.wrapper.CodeMirror = this;
        k(this);
        d.autofocus && !Gc && pa(this);
        this.state = {
            keyMaps: [],
            overlays: [],
            modeGen: 0,
            overwrite: !1,
            focused: !1,
            suppressEdits: !1,
            pasteIncoming: !1,
            draggingText: !1,
            highlight: new Hc
        };
        g(this);
        d.lineWrapping && (this.display.wrapper.className +=
            " CodeMirror-wrap");
        var f = d.value;
        "string" == typeof f && (f = new qa(d.value, d.mode));
        N(this, pd)(this, f);
        Z && setTimeout(za(ta, this, !0), 20);
        Ra(this);
        var ha;
        try {
            ha = document.activeElement == e.input
        } catch (h) {}
        ha || d.autofocus && !Gc ? setTimeout(za(Bb, this), 20) : Ic(this);
        N(this, function() {
            for (var a in ib)
                if (ib.propertyIsEnumerable(a)) ib[a](this, d[a], qd);
            for (a = 0; a < Jc.length; ++a) Jc[a](this)
        })()
    }

    function b(a, b) {
        var c = {},
            d = c.input = C("textarea", null, null, "position: absolute; padding: 0; width: 1px; height: 1em; outline: none; font-size: 4px;");
        ia ? d.style.width = "1000px" : d.setAttribute("wrap", "off");
        Cb && (d.style.border = "1px solid black");
        d.setAttribute("autocorrect", "off");
        d.setAttribute("autocapitalize", "off");
        d.setAttribute("spellcheck", "false");
        c.inputDiv = C("div", [d], null, "overflow: hidden; position: relative; width: 3px; height: 0px;");
        c.scrollbarH = C("div", [C("div", null, null, "height: 1px")], "CodeMirror-hscrollbar");
        c.scrollbarV = C("div", [C("div", null, null, "width: 1px")], "CodeMirror-vscrollbar");
        c.scrollbarFiller = C("div", null, "CodeMirror-scrollbar-filler");
        c.gutterFiller = C("div", null, "CodeMirror-gutter-filler");
        c.lineDiv = C("div", null, "CodeMirror-code");
        c.selectionDiv = C("div", null, null, "position: relative; z-index: 1");
        c.cursor = C("div", "\u00a0", "CodeMirror-cursor");
        c.otherCursor = C("div", "\u00a0", "CodeMirror-cursor CodeMirror-secondarycursor");
        c.measure = C("div", null, "CodeMirror-measure");
        c.lineSpace = C("div", [c.measure, c.selectionDiv, c.lineDiv, c.cursor, c.otherCursor], null, "position: relative; outline: none");
        c.mover = C("div", [C("div", [c.lineSpace], "CodeMirror-lines")],
            null, "position: relative");
        c.sizer = C("div", [c.mover], "CodeMirror-sizer");
        c.heightForcer = C("div", null, null, "position: absolute; height: " + jb + "px; width: 1px;");
        c.gutters = C("div", null, "CodeMirror-gutters");
        c.lineGutter = null;
        c.scroller = C("div", [c.sizer, c.heightForcer, c.gutters], "CodeMirror-scroll");
        c.scroller.setAttribute("tabIndex", "-1");
        c.wrapper = C("div", [c.inputDiv, c.scrollbarH, c.scrollbarV, c.scrollbarFiller, c.gutterFiller, c.scroller], "CodeMirror");
        kb && (c.gutters.style.zIndex = -1, c.scroller.style.paddingRight =
            0);
        a.appendChild ? a.appendChild(c.wrapper) : a(c.wrapper);
        Cb && (d.style.width = "0px");
        ia || (c.scroller.draggable = !0);
        Kc ? (c.inputDiv.style.height = "1px", c.inputDiv.style.position = "absolute") : kb && (c.scrollbarH.style.minWidth = c.scrollbarV.style.minWidth = "18px");
        c.viewOffset = c.lastSizeC = 0;
        c.showingFrom = c.showingTo = b;
        c.lineNumWidth = c.lineNumInnerWidth = c.lineNumChars = null;
        c.prevInput = "";
        c.alignWidgets = !1;
        c.pollingFast = !1;
        c.poll = new Hc;
        c.cachedCharWidth = c.cachedTextHeight = null;
        c.measureLineCache = [];
        c.measureLineCachePos =
            0;
        c.inaccurateSelection = !1;
        c.maxLine = null;
        c.maxLineLength = 0;
        c.maxLineChanged = !1;
        c.wheelDX = c.wheelDY = c.wheelStartX = c.wheelStartY = null;
        return c
    }

    function c(b) {
        b.doc.mode = a.getMode(b.options, b.doc.modeOption);
        b.doc.iter(function(a) {
            a.stateAfter && (a.stateAfter = null);
            a.styles && (a.styles = null)
        });
        b.doc.frontier = b.doc.first;
        U(b, 100);
        b.state.modeGen++;
        b.curOp && aa(b)
    }

    function d(a) {
        var b = Aa(a.display),
            c = a.options.lineWrapping,
            d = c && Math.max(5, a.display.scroller.clientWidth / Db(a.display) - 3);
        return function(e) {
            return Xa(a.doc,
                e) ? 0 : c ? (Math.ceil(e.text.length / d) || 1) * b : b
        }
    }

    function e(a) {
        var b = a.doc,
            c = d(a);
        b.iter(function(a) {
            var b = c(a);
            b != a.height && Ba(a, b)
        })
    }

    function f(a) {
        var b = Na[a.options.keyMap],
            c = b.style;
        a.display.wrapper.className = a.display.wrapper.className.replace(/\s*cm-keymap-\S+/g, "") + (c ? " cm-keymap-" + c : "");
        a.state.disableInput = b.disableInput
    }

    function g(a) {
        a.display.wrapper.className = a.display.wrapper.className.replace(/\s*cm-s-\S+/g, "") + a.options.theme.replace(/(^|\s)\s*/g, " cm-s-");
        ua(a)
    }

    function h(a) {
        k(a);
        aa(a);
        setTimeout(function() {
            L(a)
        }, 20)
    }

    function k(a) {
        var b = a.display.gutters,
            c = a.options.gutters;
        Eb(b);
        for (var d = 0; d < c.length; ++d) {
            var e = c[d],
                f = b.appendChild(C("div", null, "CodeMirror-gutter " + e));
            "CodeMirror-linenumbers" == e && (a.display.lineGutter = f, f.style.width = (a.display.lineNumWidth || 1) + "px")
        }
        b.style.display = d ? "" : "none"
    }

    function l(a, b) {
        if (0 == b.height) return 0;
        for (var c = b.text.length, d, e = b; d = lb(e, -1);) d = d.find(), e = G(a, d.from.line), c += d.from.ch - d.to.ch;
        for (e = b; d = gc(e);) d = d.find(), c -= e.text.length - d.from.ch,
            e = G(a, d.to.line), c += e.text.length - d.to.ch;
        return c
    }

    function q(a) {
        var b = a.display,
            c = a.doc;
        b.maxLine = G(c, c.first);
        b.maxLineLength = l(c, b.maxLine);
        b.maxLineChanged = !0;
        c.iter(function(a) {
            var P = l(c, a);
            P > b.maxLineLength && (b.maxLineLength = P, b.maxLine = a)
        })
    }

    function m(a) {
        var b = Ea(a.gutters, "CodeMirror-linenumbers"); - 1 == b && a.lineNumbers ? a.gutters = a.gutters.concat(["CodeMirror-linenumbers"]) : -1 < b && !a.lineNumbers && (a.gutters = a.gutters.slice(0), a.gutters.splice(b, 1))
    }

    function p(a) {
        var b = a.display,
            c = a.doc.height +
            (b.mover.offsetHeight - b.lineSpace.offsetHeight);
        b.sizer.style.minHeight = b.heightForcer.style.top = c + "px";
        b.gutters.style.height = Math.max(c, b.scroller.clientHeight - jb) + "px";
        var c = Math.max(c, b.scroller.scrollHeight),
            d = b.scroller.scrollWidth > b.scroller.clientWidth + 1,
            e = c > b.scroller.clientHeight + 1;
        e ? (b.scrollbarV.style.display = "block", b.scrollbarV.style.bottom = d ? Fb(b.measure) + "px" : "0", b.scrollbarV.firstChild.style.height = c - b.scroller.clientHeight + b.scrollbarV.clientHeight + "px") : (b.scrollbarV.style.display =
            "", b.scrollbarV.firstChild.style.height = "0");
        d ? (b.scrollbarH.style.display = "block", b.scrollbarH.style.right = e ? Fb(b.measure) + "px" : "0", b.scrollbarH.firstChild.style.width = b.scroller.scrollWidth - b.scroller.clientWidth + b.scrollbarH.clientWidth + "px") : (b.scrollbarH.style.display = "", b.scrollbarH.firstChild.style.width = "0");
        d && e ? (b.scrollbarFiller.style.display = "block", b.scrollbarFiller.style.height = b.scrollbarFiller.style.width = Fb(b.measure) + "px") : b.scrollbarFiller.style.display = "";
        d && a.options.coverGutterNextToScrollbar &&
            a.options.fixedGutter ? (b.gutterFiller.style.display = "block", b.gutterFiller.style.height = Fb(b.measure) + "px", b.gutterFiller.style.width = b.gutters.offsetWidth + "px") : b.gutterFiller.style.display = "";
        he && 0 === Fb(b.measure) && (b.scrollbarV.style.minWidth = b.scrollbarH.style.minHeight = ie ? "18px" : "12px", b.scrollbarV.style.pointerEvents = b.scrollbarH.style.pointerEvents = "none")
    }

    function E(a, b, c) {
        var d = a.scroller.scrollTop,
            e = a.wrapper.clientHeight;
        "number" == typeof c ? d = c : c && (d = c.top, e = c.bottom - c.top);
        d = Math.floor(d -
            a.lineSpace.offsetTop);
        a = Math.ceil(d + e);
        return {
            from: Gb(b, d),
            to: Gb(b, a)
        }
    }

    function L(a) {
        var b = a.display;
        if (b.alignWidgets || b.gutters.firstChild && a.options.fixedGutter) {
            for (var c = r(b) - b.scroller.scrollLeft + a.doc.scrollLeft, d = b.gutters.offsetWidth, e = c + "px", f = b.lineDiv.firstChild; f; f = f.nextSibling)
                if (f.alignable)
                    for (var g = 0, h = f.alignable; g < h.length; ++g) h[g].style.left = e;
            a.options.fixedGutter && (b.gutters.style.left = c + d + "px")
        }
    }

    function H(a) {
        if (!a.options.lineNumbers) return !1;
        var b = a.doc,
            b = F(a.options, b.first +
                b.size - 1);
        a = a.display;
        if (b.length != a.lineNumChars) {
            var c = a.measure.appendChild(C("div", [C("div", b)], "CodeMirror-linenumber CodeMirror-gutter-elt")),
                d = c.firstChild.offsetWidth,
                c = c.offsetWidth - d;
            a.lineGutter.style.width = "";
            a.lineNumInnerWidth = Math.max(d, a.lineGutter.offsetWidth - c);
            a.lineNumWidth = a.lineNumInnerWidth + c;
            a.lineNumChars = a.lineNumInnerWidth ? b.length : -1;
            a.lineGutter.style.width = a.lineNumWidth + "px";
            return !0
        }
        return !1
    }

    function F(a, b) {
        return String(a.lineNumberFormatter(b + a.firstLineNumber))
    }

    function r(a) {
        return $(a.scroller).left - $(a.sizer).left
    }

    function B(a, b, c, d) {
        for (var e = a.display.showingFrom, f = a.display.showingTo, g, h = E(a.display, a.doc, c), k = !0;; k = !1) {
            var l = a.display.scroller.clientWidth;
            if (!v(a, b, h, d)) break;
            g = !0;
            b = [];
            W(a);
            p(a);
            if (k && a.options.lineWrapping && l != a.display.scroller.clientWidth) d = !0;
            else if (d = !1, c && (c = Math.min(a.display.scroller.scrollHeight - a.display.scroller.clientHeight, "number" == typeof c ? c : c.top)), h = E(a.display, a.doc, c), h.from >= a.display.showingFrom && h.to <= a.display.showingTo) break
        }
        g &&
            (la(a, "update", a), a.display.showingFrom == e && a.display.showingTo == f || la(a, "viewportChange", a, a.display.showingFrom, a.display.showingTo));
        return g
    }

    function v(a, b, c, d) {
        var e = a.display,
            f = a.doc;
        if (!e.wrapper.clientWidth) e.showingFrom = e.showingTo = f.first, e.viewOffset = 0;
        else if (!(!d && 0 == b.length && c.from > e.showingFrom && c.to < e.showingTo)) {
            H(a) && (b = [{
                from: f.first,
                to: f.first + f.size
            }]);
            var g = e.sizer.style.marginLeft = e.gutters.offsetWidth + "px";
            e.scrollbarH.style.left = a.options.fixedGutter ? g : "0";
            g = Infinity;
            if (a.options.lineNumbers)
                for (var h =
                        0; h < b.length; ++h) b[h].diff && b[h].from < g && (g = b[h].from);
            var h = f.first + f.size,
                k = Math.max(c.from - a.options.viewportMargin, f.first);
            c = Math.min(h, c.to + a.options.viewportMargin);
            e.showingFrom < k && 20 > k - e.showingFrom && (k = Math.max(f.first, e.showingFrom));
            e.showingTo > c && 20 > e.showingTo - c && (c = Math.min(h, e.showingTo));
            if (Hb)
                for (k = va(Oa(f, G(f, k))); c < h && Xa(f, G(f, c));) ++c;
            var l = [{
                    from: Math.max(e.showingFrom, f.first),
                    to: Math.min(e.showingTo, h)
                }],
                l = l[0].from >= l[0].to ? [] : M(l, b);
            if (Hb)
                for (h = 0; h < l.length; ++h) {
                    b = l[h];
                    for (var n; n =
                        gc(G(f, b.to - 1));)
                        if (n = n.find().from.line, n > b.from) b.to = n;
                        else {
                            l.splice(h--, 1);
                            break
                        }
                }
            for (h = f = 0; h < l.length; ++h) b = l[h], b.from < k && (b.from = k), b.to > c && (b.to = c), b.from >= b.to ? l.splice(h--, 1) : f += b.to - b.from;
            if (d || f != c - k || k != e.showingFrom || c != e.showingTo) {
                l.sort(function(a, b) {
                    return a.from - b.from
                });
                try {
                    var p = document.activeElement
                } catch (m) {}
                f < .7 * (c - k) && (e.lineDiv.style.display = "none");
                S(a, k, c, l, g);
                e.lineDiv.style.display = "";
                p && document.activeElement != p && p.offsetHeight && p.focus();
                if (k != e.showingFrom || c != e.showingTo ||
                    e.lastSizeC != e.wrapper.clientHeight) e.lastSizeC = e.wrapper.clientHeight, U(a, 400);
                e.showingFrom = k;
                e.showingTo = c;
                s(a);
                O(a);
                return !0
            }
            O(a)
        }
    }

    function s(a) {
        a = a.display;
        for (var b = a.lineDiv.offsetTop, c = a.lineDiv.firstChild, d; c; c = c.nextSibling)
            if (c.lineObj) {
                if (kb) {
                    var e = c.offsetTop + c.offsetHeight;
                    d = e - b;
                    b = e
                } else d = $(c), d = d.bottom - d.top;
                e = c.lineObj.height - d;
                2 > d && (d = Aa(a));
                if (.001 < e || -.001 > e)
                    if (Ba(c.lineObj, d), d = c.lineObj.widgets)
                        for (e = 0; e < d.length; ++e) d[e].height = d[e].node.offsetHeight
            }
    }

    function O(a) {
        var b = a.display.viewOffset =
            Ib(a, G(a.doc, a.display.showingFrom));
        a.display.mover.style.top = b + "px"
    }

    function M(a, b) {
        for (var c = 0, d = b.length || 0; c < d; ++c) {
            for (var e = b[c], f = [], g = e.diff || 0, h = 0, k = a.length; h < k; ++h) {
                var l = a[h];
                e.to <= l.from && e.diff ? f.push({
                    from: l.from + g,
                    to: l.to + g
                }) : e.to <= l.from || e.from >= l.to ? f.push(l) : (e.from > l.from && f.push({
                    from: l.from,
                    to: e.from
                }), e.to < l.to && f.push({
                    from: e.to + g,
                    to: l.to + g
                }))
            }
            a = f
        }
        return a
    }

    function R(a) {
        for (var b = a.display, c = {}, d = {}, e = b.gutters.firstChild, f = 0; e; e = e.nextSibling, ++f) c[a.options.gutters[f]] =
            e.offsetLeft, d[a.options.gutters[f]] = e.offsetWidth;
        return {
            fixedPos: r(b),
            gutterTotalWidth: b.gutters.offsetWidth,
            gutterLeft: c,
            gutterWidth: d,
            wrapperWidth: b.wrapper.clientWidth
        }
    }

    function S(a, b, c, d, e) {
        function f(b) {
            var c = b.nextSibling;
            ia && mb && a.display.currentWheelTarget == b ? (b.style.display = "none", b.lineObj = null) : b.parentNode.removeChild(b);
            return c
        }
        var g = R(a),
            h = a.display,
            k = a.options.lineNumbers;
        d.length || ia && a.display.currentWheelTarget || Eb(h.lineDiv);
        var l = h.lineDiv,
            n = l.firstChild,
            p = d.shift(),
            m = b;
        for (a.doc.iter(b,
                c,
                function(b) {
                    p && p.to == m && (p = d.shift());
                    if (Xa(a.doc, b)) {
                        if (0 != b.height && Ba(b, 0), b.widgets && n && n.previousSibling)
                            for (var c = 0; c < b.widgets.length; ++c) {
                                var T = b.widgets[c];
                                if (T.showIfHidden) {
                                    var ga = n.previousSibling;
                                    if (/pre/i.test(ga.nodeName)) {
                                        var h = C("div", null, null, "position: relative");
                                        ga.parentNode.replaceChild(h, ga);
                                        h.appendChild(ga);
                                        ga = h
                                    }
                                    h = ga.appendChild(C("div", [T.node], "CodeMirror-linewidget"));
                                    T.handleMouseEvents || (h.ignoreEvents = !0);
                                    A(T, h, ga, g)
                                }
                            }
                    } else if (p && p.from <= m && p.to > m) {
                        for (; n.lineObj !=
                            b;) n = f(n);
                        k && e <= m && n.lineNumber && rd(n.lineNumber, F(a.options, m));
                        n = n.nextSibling
                    } else {
                        if (b.widgets)
                            for (var h = 0, ea = n; ea && 20 > h; ++h, ea = ea.nextSibling)
                                if (ea.lineObj == b && /div/i.test(ea.nodeName)) {
                                    c = ea;
                                    break
                                }
                        var r = m,
                            ea = c,
                            z = Lc(a, b),
                            h = z.pre,
                            q = b.gutterMarkers,
                            D = a.display,
                            ba = z.bgClass ? z.bgClass + " " + (b.bgClass || "") : b.bgClass;
                        if (a.options.lineNumbers || q || ba || b.wrapClass || b.widgets) {
                            if (ea) {
                                ea.alignable = null;
                                for (var ca = !0, E = 0, v = null, B = ea.firstChild, H; B; B = H)
                                    if (H = B.nextSibling, /\bCodeMirror-linewidget\b/.test(B.className)) {
                                        for (z =
                                            0; z < b.widgets.length; ++z) {
                                            var s = b.widgets[z];
                                            if (s.node == B.firstChild) {
                                                s.above || v || (v = B);
                                                A(s, B, ea, g);
                                                ++E;
                                                break
                                            }
                                        }
                                        if (z == b.widgets.length) {
                                            ca = !1;
                                            break
                                        }
                                    } else ea.removeChild(B);
                                ea.insertBefore(h, v);
                                ca && E == b.widgets.length && (T = ea, ea.className = b.wrapClass || "")
                            }
                            T || (T = C("div", null, b.wrapClass, "position: relative"), T.appendChild(h));
                            ba && T.insertBefore(C("div", null, ba + " CodeMirror-linebackground"), T.firstChild);
                            if (a.options.lineNumbers || q)
                                if (ga = T.insertBefore(C("div", null, null, "position: absolute; left: " + (a.options.fixedGutter ?
                                        g.fixedPos : -g.gutterTotalWidth) + "px"), T.firstChild), a.options.fixedGutter && (T.alignable || (T.alignable = [])).push(ga), !a.options.lineNumbers || q && q["CodeMirror-linenumbers"] || (T.lineNumber = ga.appendChild(C("div", F(a.options, r), "CodeMirror-linenumber CodeMirror-gutter-elt", "left: " + g.gutterLeft["CodeMirror-linenumbers"] + "px; width: " + D.lineNumInnerWidth + "px"))), q)
                                    for (z = 0; z < a.options.gutters.length; ++z) s = a.options.gutters[z], (r = q.hasOwnProperty(s) && q[s]) && ga.appendChild(C("div", [r], "CodeMirror-gutter-elt",
                                        "left: " + g.gutterLeft[s] + "px; width: " + g.gutterWidth[s] + "px"));
                            kb && (T.style.zIndex = 2);
                            if (b.widgets && T != ea)
                                for (z = 0, ea = b.widgets; z < ea.length; ++z) s = ea[z], q = C("div", [s.node], "CodeMirror-linewidget"), s.handleMouseEvents || (q.ignoreEvents = !0), A(s, q, T, g), s.above ? T.insertBefore(q, a.options.lineNumbers && 0 != b.height ? ga : h) : T.appendChild(q), la(s, "redraw")
                        } else T = h;
                        if (T != c) l.insertBefore(T, n);
                        else {
                            for (; n != c;) n = f(n);
                            n = n.nextSibling
                        }
                        T.lineObj = b
                    }++m
                }); n;) n = f(n)
    }

    function A(a, b, c, d) {
        a.noHScroll && ((c.alignable || (c.alignable = [])).push(b), c = d.wrapperWidth, b.style.left = d.fixedPos + "px", a.coverGutter || (c -= d.gutterTotalWidth, b.style.paddingLeft = d.gutterTotalWidth + "px"), b.style.width = c + "px");
        a.coverGutter && (b.style.zIndex = 5, b.style.position = "relative", a.noHScroll || (b.style.marginLeft = -d.gutterTotalWidth + "px"))
    }

    function W(a) {
        var b = a.display,
            c = V(a.doc.sel.from, a.doc.sel.to);
        if (c || a.options.showCursorWhenSelecting) {
            var d = a.display,
                e = ma(a, a.doc.sel.head, "div");
            d.cursor.style.left = e.left + "px";
            d.cursor.style.top = e.top + "px";
            d.cursor.style.height =
                Math.max(0, e.bottom - e.top) * a.options.cursorHeight + "px";
            d.cursor.style.display = "";
            e.other ? (d.otherCursor.style.display = "", d.otherCursor.style.left = e.other.left + "px", d.otherCursor.style.top = e.other.top + "px", d.otherCursor.style.height = .85 * (e.other.bottom - e.other.top) + "px") : d.otherCursor.style.display = "none"
        } else b.cursor.style.display = b.otherCursor.style.display = "none";
        c ? b.selectionDiv.style.display = "none" : u(a);
        a.options.moveInputWithCursor && (a = ma(a, a.doc.sel.head, "div"), c = $(b.wrapper), d = $(b.lineDiv),
            b.inputDiv.style.top = Math.max(0, Math.min(b.wrapper.clientHeight - 10, a.top + d.top - c.top)) + "px", b.inputDiv.style.left = Math.max(0, Math.min(b.wrapper.clientWidth - 10, a.left + d.left - c.left)) + "px")
    }

    function u(a) {
        function b(a, c, P, d) {
            0 > c && (c = 0);
            g.appendChild(C("div", null, "CodeMirror-selected", "position: absolute; left: " + a + "px; top: " + c + "px; width: " + (null == P ? h - a : P) + "px; height: " + (d - c) + "px"))
        }

        function c(d, f, ga) {
            var g = G(e, d),
                od = g.text.length,
                gb, hb;
            je(Fa(g), f || 0, null == ga ? od : ga, function(c, e, ha) {
                var l = nb(a, w(d,
                        c), "div", g, "left"),
                    n, hc;
                c == e ? (n = l, ha = hc = l.left) : (n = nb(a, w(d, e - 1), "div", g, "right"), "rtl" == ha && (ha = l, l = n, n = ha), ha = l.left, hc = n.right);
                null == f && 0 == c && (ha = k);
                3 < n.top - l.top && (b(ha, l.top, null, l.bottom), ha = k, l.bottom < n.top && b(ha, l.bottom, null, n.top));
                null == ga && e == od && (hc = h);
                if (!gb || l.top < gb.top || l.top == gb.top && l.left < gb.left) gb = l;
                if (!hb || n.bottom > hb.bottom || n.bottom == hb.bottom && n.right > hb.right) hb = n;
                ha < k + 1 && (ha = k);
                b(ha, n.top, hc - ha, n.bottom)
            });
            return {
                start: gb,
                end: hb
            }
        }
        var d = a.display,
            e = a.doc,
            f = a.doc.sel,
            g =
            document.createDocumentFragment(),
            h = d.lineSpace.offsetWidth,
            k = Ga(a.display.measure, C("pre", null, null, "text-align: left")).appendChild(C("span", "x")).offsetLeft;
        if (f.from.line == f.to.line) c(f.from.line, f.from.ch, f.to.ch);
        else {
            var l = G(e, f.from.line),
                n = G(e, f.to.line),
                n = Oa(e, l) == Oa(e, n),
                l = c(f.from.line, f.from.ch, n ? l.text.length : null).end,
                f = c(f.to.line, n ? 0 : null, f.to.ch).start;
            n && (l.top < f.top - 2 ? (b(l.right, l.top, null, l.bottom), b(k, f.top, f.left, f.bottom)) : b(l.right, l.top, f.left - l.right, l.bottom));
            l.bottom <
                f.top && b(k, l.bottom, null, f.top)
        }
        Ga(d.selectionDiv, g);
        d.selectionDiv.style.display = ""
    }

    function D(a) {
        if (a.state.focused) {
            var b = a.display;
            clearInterval(b.blinker);
            var c = !0;
            b.cursor.style.visibility = b.otherCursor.style.visibility = "";
            0 < a.options.cursorBlinkRate && (b.blinker = setInterval(function() {
                b.cursor.style.visibility = b.otherCursor.style.visibility = (c = !c) ? "" : "hidden"
            }, a.options.cursorBlinkRate))
        }
    }

    function U(a, b) {
        a.doc.mode.startState && a.doc.frontier < a.display.showingTo && a.state.highlight.set(b, za(da,
            a))
    }

    function da(a) {
        var b = a.doc;
        b.frontier < b.first && (b.frontier = b.first);
        if (!(b.frontier >= a.display.showingTo)) {
            var c = +new Date + a.options.workTime,
                d = ob(b.mode, sa(a, b.frontier)),
                e = [],
                f;
            b.iter(b.frontier, Math.min(b.first + b.size, a.display.showingTo + 500), function(g) {
                if (b.frontier >= a.display.showingFrom) {
                    var h = g.styles;
                    g.styles = sd(a, g, d, !0);
                    for (var k = !h || h.length != g.styles.length, l = 0; !k && l < h.length; ++l) k = h[l] != g.styles[l];
                    k && (f && f.end == b.frontier ? f.end++ : e.push(f = {
                        start: b.frontier,
                        end: b.frontier + 1
                    }));
                    g.stateAfter =
                        ob(b.mode, d)
                } else Mc(a, g.text, d), g.stateAfter = 0 == b.frontier % 5 ? ob(b.mode, d) : null;
                ++b.frontier;
                if (+new Date > c) return U(a, a.options.workDelay), !0
            });
            e.length && N(a, function() {
                for (var a = 0; a < e.length; ++a) aa(this, e[a].start, e[a].end)
            })()
        }
    }

    function oa(a, b, c) {
        for (var d, e, f = a.doc, g = c ? -1 : b - (a.doc.mode.innerMode ? 1E3 : 100); b > g; --b) {
            if (b <= f.first) return f.first;
            var h = G(f, b - 1);
            if (h.stateAfter && (!c || b <= f.frontier)) return b;
            h = pb(h.text, null, a.options.tabSize);
            if (null == e || d > h) e = b - 1, d = h
        }
        return e
    }

    function sa(a, b, c) {
        var d =
            a.doc,
            e = a.display;
        if (!d.mode.startState) return !0;
        var f = oa(a, b, c),
            g = f > d.first && G(d, f - 1).stateAfter,
            g = g ? ob(d.mode, g) : td(d.mode);
        d.iter(f, b, function(c) {
            Mc(a, c.text, g);
            c.stateAfter = f == b - 1 || 0 == f % 5 || f >= e.showingFrom && f < e.showingTo ? ob(d.mode, g) : null;
            ++f
        });
        c && (d.frontier = f);
        return g
    }

    function ka(a, b, c, d, e) {
        var f = -1;
        d = d || qb(a, b);
        if (d.crude) return c = d.left + c * d.width, {
            left: c,
            right: c + d.width,
            top: d.top,
            bottom: d.bottom
        };
        for (a = c;; a += f) {
            var g = d[a];
            if (g) break;
            0 > f && 0 == a && (f = 1)
        }
        e = a > c ? "left" : a < c ? "right" : e;
        "left" == e &&
            g.leftSide ? g = g.leftSide : "right" == e && g.rightSide && (g = g.rightSide);
        return {
            left: a < c ? g.right : g.left,
            right: a > c ? g.left : g.right,
            top: g.top,
            bottom: g.bottom
        }
    }

    function Y(a, b) {
        for (var c = a.display.measureLineCache, d = 0; d < c.length; ++d) {
            var e = c[d];
            if (e.text == b.text && e.markedSpans == b.markedSpans && a.display.scroller.clientWidth == e.width && e.classes == b.textClass + "|" + b.wrapClass) return e
        }
    }

    function Jb(a, b) {
        var c = Y(a, b);
        c && (c.text = c.measure = c.markedSpans = null)
    }

    function qb(a, b) {
        var c = Y(a, b);
        if (c) return c.measure;
        var c = ic(a,
                b),
            d = a.display.measureLineCache,
            e = {
                text: b.text,
                width: a.display.scroller.clientWidth,
                markedSpans: b.markedSpans,
                measure: c,
                classes: b.textClass + "|" + b.wrapClass
            };
        16 == d.length ? d[++a.display.measureLineCachePos % 16] = e : d.push(e);
        return c
    }

    function ic(a, b) {
        function c(a) {
            var b = a.top - z.top,
                P = a.bottom - z.top;
            P > q && (P = q);
            0 > b && (b = 0);
            for (var d = r.length - 2; 0 <= d; d -= 2) {
                var e = r[d],
                    T = r[d + 1];
                if (!(e > P || T < b) && (e <= b && T >= P || b <= e && P >= T || Math.min(P, T) - Math.max(b, e) >= P - b >> 1)) {
                    r[d] = Math.min(b, e);
                    r[d + 1] = Math.max(P, T);
                    break
                }
            }
            0 > d && (d =
                r.length, r.push(b, P));
            return {
                left: a.left - z.left,
                right: a.right - z.left,
                top: d,
                bottom: null
            }
        }

        function d(a) {
            a.bottom = r[a.top + 1];
            a.top = r[a.top]
        }
        if (!a.options.lineWrapping && b.text.length >= a.options.crudeMeasuringFrom) return Kb(a, b);
        var e = a.display,
            f = ud(b.text.length),
            g = Lc(a, b, f, !0).pre;
        if (Z && !kb && !a.options.lineWrapping && 100 < g.childNodes.length) {
            for (var h = document.createDocumentFragment(), k = g.childNodes.length, l = 0, n = Math.ceil(k / 10); l < n; ++l) {
                for (var p = C("div", null, null, "display: inline-block"), m = 0; 10 > m && k; ++m) p.appendChild(g.firstChild),
                    --k;
                h.appendChild(p)
            }
            g.appendChild(h)
        }
        Ga(e.measure, g);
        var z = $(e.lineDiv),
            r = [],
            h = ud(b.text.length),
            q = g.offsetHeight;
        wa && e.measure.first != g && Ga(e.measure, g);
        for (l = 0; l < f.length; ++l)
            if (e = f[l]) g = e, k = null, /\bCodeMirror-widget\b/.test(e.className) && e.getClientRects && (1 == e.firstChild.nodeType && (g = e.firstChild), n = g.getClientRects(), 1 < n.length && (k = h[l] = c(n[0]), k.rightSide = c(n[n.length - 1]))), k || (k = h[l] = c($(g))), e.measureRight && (k.right = $(e.measureRight).left), e.leftSide && (k.leftSide = c($(e.leftSide)));
        Eb(a.display.measure);
        for (l = 0; l < h.length; ++l)
            if (e = h[l]) d(e), e.leftSide && d(e.leftSide), e.rightSide && d(e.rightSide);
        return h
    }

    function Kb(a, b) {
        var c = new Sa(b.text.slice(0, 100), null);
        b.textClass && (c.textClass = b.textClass);
        var d = ic(a, c),
            e = ka(a, c, 0, d, "left"),
            c = ka(a, c, 99, d, "right");
        return {
            crude: !0,
            top: e.top,
            left: e.left,
            bottom: e.bottom,
            width: (c.right - e.left) / 100
        }
    }

    function ua(a) {
        a.display.measureLineCache.length = a.display.measureLineCachePos = 0;
        a.display.cachedCharWidth = a.display.cachedTextHeight = null;
        a.options.lineWrapping || (a.display.maxLineChanged = !0);
        a.display.lineNumChars = null
    }

    function Lb(a, b, c, d) {
        if (b.widgets)
            for (var e = 0; e < b.widgets.length; ++e)
                if (b.widgets[e].above) {
                    var f = jc(b.widgets[e]);
                    c.top += f;
                    c.bottom += f
                }
        if ("line" == d) return c;
        d || (d = "local");
        b = Ib(a, b);
        b = "local" == d ? b + a.display.lineSpace.offsetTop : b - a.display.viewOffset;
        if ("page" == d || "window" == d) a = $(a.display.lineSpace), b += a.top + ("window" == d ? 0 : window.pageYOffset || (document.documentElement || document.body).scrollTop), d = a.left + ("window" == d ? 0 : window.pageXOffset || (document.documentElement ||
            document.body).scrollLeft), c.left += d, c.right += d;
        c.top += b;
        c.bottom += b;
        return c
    }

    function rb(a, b, c) {
        if ("div" == c) return b;
        var d = b.left;
        b = b.top;
        "page" == c ? (d -= window.pageXOffset || (document.documentElement || document.body).scrollLeft, b -= window.pageYOffset || (document.documentElement || document.body).scrollTop) : "local" != c && c || (c = $(a.display.sizer), d += c.left, b += c.top);
        a = $(a.display.lineSpace);
        return {
            left: d - a.left,
            top: b - a.top
        }
    }

    function nb(a, b, c, d, e) {
        d || (d = G(a.doc, b.line));
        return Lb(a, d, ka(a, d, b.ch, null, e), c)
    }

    function ma(a, b, c, d, e) {
        function f(b, T) {
            var g = ka(a, d, b, e, T ? "right" : "left");
            T ? g.left = g.right : g.right = g.left;
            return Lb(a, d, g, c)
        }

        function g(a, b) {
            var c = h[b],
                P = c.level % 2;
            a == Nc(c) && b && c.level < h[b - 1].level ? (c = h[--b], a = Oc(c) - (c.level % 2 ? 0 : 1), P = !0) : a == Oc(c) && b < h.length - 1 && c.level < h[b + 1].level && (c = h[++b], a = Nc(c) - c.level % 2, P = !1);
            return P && a == c.to && a > c.from ? f(a - 1) : f(a, P)
        }
        d = d || G(a.doc, b.line);
        e || (e = qb(a, d));
        var h = Fa(d);
        b = b.ch;
        if (!h) return f(b);
        var k = Pc(h, b),
            k = g(b, k);
        null != sb && (k.other = g(b, sb));
        return k
    }

    function Ya(a,
        b, c, d) {
        a = new w(a, b);
        a.xRel = d;
        c && (a.outside = !0);
        return a
    }

    function Mb(a, b, c) {
        var d = a.doc;
        c += a.display.viewOffset;
        if (0 > c) return Ya(d.first, 0, !0, -1);
        var e = Gb(d, c),
            f = d.first + d.size - 1;
        if (e > f) return Ya(d.first + d.size - 1, G(d, f).text.length, !0, 1);
        for (0 > b && (b = 0);;) {
            var f = G(d, e),
                e = Qc(a, f, e, b, c),
                g = (f = gc(f)) && f.find();
            if (f && (e.ch > g.from.ch || e.ch == g.from.ch && 0 < e.xRel)) e = g.to.line;
            else return e
        }
    }

    function Qc(a, b, c, d, e) {
        function f(d) {
            d = ma(a, w(c, d), "line", b, l);
            h = !0;
            if (g > d.bottom) return d.left - k;
            if (g < d.top) return d.left +
                k;
            h = !1;
            return d.left
        }
        var g = e - Ib(a, b),
            h = !1,
            k = 2 * a.display.wrapper.clientWidth,
            l = qb(a, b),
            n = Fa(b),
            p = b.text.length;
        e = kc(b);
        var m = lc(b),
            z = f(e),
            r = h,
            q = f(m),
            D = h;
        if (d > q) return Ya(c, m, D, 1);
        for (;;) {
            if (n ? m == e || m == Rc(b, e, 1) : 1 >= m - e) {
                n = d < z || d - z <= q - d ? e : m;
                for (d -= n == e ? z : q; Sc.test(b.text.charAt(n));) ++n;
                return Ya(c, n, n == e ? r : D, 0 > d ? -1 : d ? 1 : 0)
            }
            var ba = Math.ceil(p / 2),
                ca = e + ba;
            if (n)
                for (var ca = e, F = 0; F < ba; ++F) ca = Rc(b, ca, 1);
            F = f(ca);
            if (F > d) {
                m = ca;
                q = F;
                if (D = h) q += 1E3;
                p = ba
            } else e = ca, z = F, r = h, p -= ba
        }
    }

    function Aa(a) {
        if (null != a.cachedTextHeight) return a.cachedTextHeight;
        if (null == Za) {
            Za = C("pre");
            for (var b = 0; 49 > b; ++b) Za.appendChild(document.createTextNode("x")), Za.appendChild(C("br"));
            Za.appendChild(document.createTextNode("x"))
        }
        Ga(a.measure, Za);
        b = Za.offsetHeight / 50;
        3 < b && (a.cachedTextHeight = b);
        Eb(a.measure);
        return b || 1
    }

    function Db(a) {
        if (null != a.cachedCharWidth) return a.cachedCharWidth;
        var b = C("span", "x"),
            c = C("pre", [b]);
        Ga(a.measure, c);
        b = b.offsetWidth;
        2 < b && (a.cachedCharWidth = b);
        return b || 10
    }

    function xa(a) {
        a.curOp = {
            changes: [],
            forceUpdate: !1,
            updateInput: null,
            userSelChange: null,
            textChanged: null,
            selectionChanged: !1,
            cursorActivity: !1,
            updateMaxLine: !1,
            updateScrollPos: !1,
            id: ++ke
        };
        mc++ || (Ta = [])
    }

    function Ha(a) {
        var b = a.curOp,
            c = a.doc,
            d = a.display;
        a.curOp = null;
        b.updateMaxLine && q(a);
        if (d.maxLineChanged && !a.options.lineWrapping && d.maxLine) {
            var e;
            e = d.maxLine;
            var f = !1;
            if (e.markedSpans)
                for (var g = 0; g < e.markedSpans; ++g) {
                    var h = e.markedSpans[g];
                    !h.collapsed || null != h.to && h.to != e.text.length || (f = !0)
                }(f = !f && Y(a, e)) || e.text.length >= a.options.crudeMeasuringFrom ? e = ka(a, e, e.text.length, f && f.measure,
                    "right").right : (e = Lc(a, e, null, !0).pre, f = e.appendChild(Nb(a.display.measure)), Ga(a.display.measure, e), e = $(f).right - $(a.display.lineDiv).left);
            d.sizer.style.minWidth = Math.max(0, e + 3 + jb) + "px";
            d.maxLineChanged = !1;
            e = Math.max(0, d.sizer.offsetLeft + d.sizer.offsetWidth - d.scroller.clientWidth);
            e < c.scrollLeft && !b.updateScrollPos && Ua(a, Math.min(d.scroller.scrollLeft, e), !0)
        }
        var k, l;
        b.updateScrollPos ? k = b.updateScrollPos : b.selectionChanged && d.scroller.clientHeight && (k = ma(a, c.sel.head), k = nc(a, k.left, k.top, k.left,
            k.bottom));
        if (b.changes.length || b.forceUpdate || k && null != k.scrollTop) l = B(a, b.changes, k && k.scrollTop, b.forceUpdate), a.display.scroller.offsetHeight && (a.doc.scrollTop = a.display.scroller.scrollTop);
        !l && b.selectionChanged && W(a);
        if (b.updateScrollPos) l = Math.max(0, Math.min(d.scroller.scrollHeight - d.scroller.clientHeight, k.scrollTop)), k = Math.max(0, Math.min(d.scroller.scrollWidth - d.scroller.clientWidth, k.scrollLeft)), d.scroller.scrollTop = d.scrollbarV.scrollTop = c.scrollTop = l, d.scroller.scrollLeft = d.scrollbarH.scrollLeft =
            c.scrollLeft = k, L(a), b.scrollToPos && vd(a, J(a.doc, b.scrollToPos.from), J(a.doc, b.scrollToPos.to), b.scrollToPos.margin);
        else if (k && (c = vd(a, a.doc.sel.head, null, a.options.cursorScrollMargin), a.state.focused && (d = a.display, l = $(d.sizer), k = null, 0 > c.top + l.top ? k = !0 : c.bottom + l.top > (window.innerHeight || document.documentElement.clientHeight) && (k = !1), null != k && !le))) {
            if (l = "none" == d.cursor.style.display) d.cursor.style.display = "", d.cursor.style.left = c.left + "px", d.cursor.style.top = c.top - d.viewOffset + "px";
            d.cursor.scrollIntoView(k);
            l && (d.cursor.style.display = "none")
        }
        b.selectionChanged && D(a);
        a.state.focused && b.updateInput && ta(a, b.userSelChange);
        c = b.maybeHiddenMarkers;
        d = b.maybeUnhiddenMarkers;
        if (c)
            for (k = 0; k < c.length; ++k) c[k].lines.length || ja(c[k], "hide");
        if (d)
            for (k = 0; k < d.length; ++k) d[k].lines.length && ja(d[k], "unhide");
        var n;
        --mc || (n = Ta, Ta = null);
        b.textChanged && ja(a, "change", a, b.textChanged);
        b.cursorActivity && ja(a, "cursorActivity", a);
        if (n)
            for (k = 0; k < n.length; ++k) n[k]()
    }

    function N(a, b) {
        return function() {
            var c = a || this,
                d = !c.curOp;
            d &&
                xa(c);
            try {
                var e = b.apply(c, arguments)
            } finally {
                d && Ha(c)
            }
            return e
        }
    }

    function $a(a) {
        return function() {
            var b = this.cm && !this.cm.curOp,
                c;
            b && xa(this.cm);
            try {
                c = a.apply(this, arguments)
            } finally {
                b && Ha(this.cm)
            }
            return c
        }
    }

    function Ob(a, b) {
        var c = !a.curOp,
            d;
        c && xa(a);
        try {
            d = b()
        } finally {
            c && Ha(a)
        }
        return d
    }

    function aa(a, b, c, d) {
        null == b && (b = a.doc.first);
        null == c && (c = a.doc.first + a.doc.size);
        a.curOp.changes.push({
            from: b,
            to: c,
            diff: d
        })
    }

    function tb(a) {
        a.display.pollingFast || a.display.poll.set(a.options.pollInterval, function() {
            ub(a);
            a.state.focused && tb(a)
        })
    }

    function ab(a) {
        function b() {
            ub(a) || c ? (a.display.pollingFast = !1, tb(a)) : (c = !0, a.display.poll.set(60, b))
        }
        var c = !1;
        a.display.pollingFast = !0;
        a.display.poll.set(20, b)
    }

    function ub(a) {
        var b = a.display.input,
            c = a.display.prevInput,
            d = a.doc,
            e = d.sel;
        if (!a.state.focused || me(b) || Ia(a) || a.state.disableInput) return !1;
        a.state.pasteIncoming && a.state.fakedLastChar && (b.value = b.value.substring(0, b.value.length - 1), a.state.fakedLastChar = !1);
        var f = b.value;
        if (f == c && V(e.from, e.to)) return !1;
        if (Z && !wa &&
            a.display.inputHasSelection === f) return ta(a, !0), !1;
        var g = !a.curOp;
        g && xa(a);
        e.shift = !1;
        for (var h = 0, k = Math.min(c.length, f.length); h < k && c.charCodeAt(h) == f.charCodeAt(h);) ++h;
        k = e.from;
        e = e.to;
        h < c.length ? k = w(k.line, k.ch - (c.length - h)) : a.state.overwrite && V(k, e) && !a.state.pasteIncoming && (e = w(e.line, Math.min(G(d, e.line).text.length, e.ch + (f.length - h))));
        c = a.curOp.updateInput;
        h = {
            from: k,
            to: e,
            text: bb(f.slice(h)),
            origin: a.state.pasteIncoming ? "paste" : "+input"
        };
        vb(a.doc, h, "end");
        a.curOp.updateInput = c;
        la(a, "inputRead",
            a, h);
        1E3 < f.length || -1 < f.indexOf("\n") ? b.value = a.display.prevInput = "" : a.display.prevInput = f;
        g && Ha(a);
        a.state.pasteIncoming = !1;
        return !0
    }

    function ta(a, b) {
        var c, d, e = a.doc;
        V(e.sel.from, e.sel.to) ? b && (a.display.prevInput = a.display.input.value = "", Z && !wa && (a.display.inputHasSelection = null)) : (a.display.prevInput = "", d = (c = wd && (100 < e.sel.to.line - e.sel.from.line || 1E3 < (d = a.getSelection()).length)) ? "-" : d || a.getSelection(), a.display.input.value = d, a.state.focused && xd(a.display.input), Z && !wa && (a.display.inputHasSelection =
            d));
        a.display.inaccurateSelection = c
    }

    function pa(a) {
        "nocursor" == a.options.readOnly || Gc && document.activeElement == a.display.input || a.display.input.focus()
    }

    function Ia(a) {
        return a.options.readOnly || a.doc.cantEdit
    }

    function Ra(a) {
        function b() {
            a.state.focused && setTimeout(za(pa, a), 0)
        }

        function c() {
            null == h && (h = setTimeout(function() {
                h = null;
                g.cachedCharWidth = g.cachedTextHeight = Pb = null;
                ua(a);
                Ob(a, za(aa, a))
            }, 100))
        }

        function d() {
            for (var a = g.wrapper.parentNode; a && a != document.body; a = a.parentNode);
            a ? setTimeout(d, 5E3) :
                Pa(window, "resize", c)
        }

        function e(b) {
            Ja(a, b) || a.options.onDragEvent && a.options.onDragEvent(a, Qb(b)) || Rb(b)
        }

        function f() {
            g.inaccurateSelection && (g.prevInput = "", g.inaccurateSelection = !1, g.input.value = a.getSelection(), xd(g.input))
        }
        var g = a.display;
        Q(g.scroller, "mousedown", N(a, oc));
        Z ? Q(g.scroller, "dblclick", N(a, function(b) {
            if (!Ja(a, b)) {
                var c = ya(a, b);
                !c || wb(a, b, "gutterClick", !0, la) || Ka(a.display, b) || (fa(b), b = Tc(G(a.doc, c.line).text, c), na(a.doc, b.from, b.to))
            }
        })) : Q(g.scroller, "dblclick", function(b) {
            Ja(a, b) ||
                fa(b)
        });
        Q(g.lineSpace, "selectstart", function(a) {
            Ka(g, a) || fa(a)
        });
        Uc || Q(g.scroller, "contextmenu", function(b) {
            yd(a, b)
        });
        Q(g.scroller, "scroll", function() {
            g.scroller.clientHeight && (La(a, g.scroller.scrollTop), Ua(a, g.scroller.scrollLeft, !0), ja(a, "scroll", a))
        });
        Q(g.scrollbarV, "scroll", function() {
            g.scroller.clientHeight && La(a, g.scrollbarV.scrollTop)
        });
        Q(g.scrollbarH, "scroll", function() {
            g.scroller.clientHeight && Ua(a, g.scrollbarH.scrollLeft)
        });
        Q(g.scroller, "mousewheel", function(b) {
            Sb(a, b)
        });
        Q(g.scroller, "DOMMouseScroll",
            function(b) {
                Sb(a, b)
            });
        Q(g.scrollbarH, "mousedown", b);
        Q(g.scrollbarV, "mousedown", b);
        Q(g.wrapper, "scroll", function() {
            g.wrapper.scrollTop = g.wrapper.scrollLeft = 0
        });
        var h;
        Q(window, "resize", c);
        setTimeout(d, 5E3);
        Q(g.input, "keyup", N(a, function(b) {
            Ja(a, b) || a.options.onKeyEvent && a.options.onKeyEvent(a, Qb(b)) || 16 != b.keyCode || (a.doc.sel.shift = !1)
        }));
        Q(g.input, "input", function() {
            Z && !wa && a.display.inputHasSelection && (a.display.inputHasSelection = null);
            ab(a)
        });
        Q(g.input, "keydown", N(a, ba));
        Q(g.input, "keypress", N(a,
            ca));
        Q(g.input, "focus", za(Bb, a));
        Q(g.input, "blur", za(Ic, a));
        a.options.dragDrop && (Q(g.scroller, "dragstart", function(b) {
            if (Z && (!a.state.draggingText || 100 > +new Date - zd)) Rb(b);
            else if (!Ja(a, b) && !Ka(a.display, b)) {
                var c = a.getSelection();
                b.dataTransfer.setData("Text", c);
                b.dataTransfer.setDragImage && !Vc && (c = C("img", null, null, "position: fixed; left: 0; top: 0;"), c.src = "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==", Ca && (c.width = c.height = 1, a.display.wrapper.appendChild(c), c._top =
                    c.offsetTop), b.dataTransfer.setDragImage(c, 0, 0), Ca && c.parentNode.removeChild(c))
            }
        }), Q(g.scroller, "dragenter", e), Q(g.scroller, "dragover", e), Q(g.scroller, "drop", N(a, cb)));
        Q(g.scroller, "paste", function(b) {
            Ka(g, b) || (pa(a), ab(a))
        });
        Q(g.input, "paste", function() {
            if (ia && !a.state.fakedLastChar && !(200 > new Date - a.state.lastMiddleDown)) {
                var b = g.input.selectionStart,
                    c = g.input.selectionEnd;
                g.input.value += "$";
                g.input.selectionStart = b;
                g.input.selectionEnd = c;
                a.state.fakedLastChar = !0
            }
            a.state.pasteIncoming = !0;
            ab(a)
        });
        Q(g.input, "cut", f);
        Q(g.input, "copy", f);
        Kc && Q(g.sizer, "mouseup", function() {
            document.activeElement == g.input && g.input.blur();
            pa(a)
        })
    }

    function Ka(a, b) {
        for (var c = b.target || b.srcElement; c != a.wrapper; c = c.parentNode)
            if (!c || c.ignoreEvents || c.parentNode == a.sizer && c != a.mover) return !0
    }

    function ya(a, b, c) {
        var d = a.display;
        if (!c && (c = b.target || b.srcElement, c == d.scrollbarH || c == d.scrollbarH.firstChild || c == d.scrollbarV || c == d.scrollbarV.firstChild || c == d.scrollbarFiller || c == d.gutterFiller)) return null;
        var e, f, d = $(d.lineSpace);
        try {
            e = b.clientX, f = b.clientY
        } catch (g) {
            return null
        }
        return Mb(a, e - d.left, f - d.top)
    }

    function oc(a) {
        function b(a) {
            if (!V(r, a))
                if (r = a, "single" == n) na(e.doc, J(g, k), a);
                else if (m = J(g, m), z = J(g, z), "double" == n) {
                var c = Tc(G(g, a.line).text, a);
                X(a, m) ? na(e.doc, c.from, z) : na(e.doc, m, c.to)
            } else "triple" == n && (X(a, m) ? na(e.doc, z, J(g, w(a.line, 0))) : na(e.doc, m, J(g, w(a.line + 1, 0))))
        }

        function c(a) {
            var d = ++D,
                P = ya(e, a, !0);
            if (P)
                if (V(P, p)) {
                    var h = a.clientY < q.top ? -20 : a.clientY > q.bottom ? 20 : 0;
                    h && setTimeout(N(e, function() {
                        D == d && (f.scroller.scrollTop +=
                            h, c(a))
                    }), 50)
                } else {
                    e.state.focused || Bb(e);
                    p = P;
                    b(P);
                    var k = E(f, g);
                    (P.line >= k.to || P.line < k.from) && setTimeout(N(e, function() {
                        D == d && c(a)
                    }), 150)
                }
        }

        function d(a) {
            D = Infinity;
            fa(a);
            pa(e);
            Pa(document, "mousemove", ba);
            Pa(document, "mouseup", ca)
        }
        if (!Ja(this, a)) {
            var e = this,
                f = e.display,
                g = e.doc,
                h = g.sel;
            h.shift = a.shiftKey;
            if (Ka(f, a)) ia || (f.scroller.draggable = !1, setTimeout(function() {
                f.scroller.draggable = !0
            }, 100));
            else if (!wb(e, a, "gutterClick", !0, la)) {
                var k = ya(e, a);
                switch (Ad(a)) {
                    case 3:
                        Uc && yd.call(e, e, a);
                        return;
                    case 2:
                        ia &&
                            (e.state.lastMiddleDown = +new Date);
                        k && na(e.doc, k);
                        setTimeout(za(pa, e), 20);
                        fa(a);
                        return
                }
                if (k) {
                    e.state.focused || Bb(e);
                    var l = +new Date,
                        n = "single";
                    pc && pc.time > l - 400 && V(pc.pos, k) ? (n = "triple", fa(a), setTimeout(za(pa, e), 20), ne(e, k.line)) : qc && qc.time > l - 400 && V(qc.pos, k) ? (n = "double", pc = {
                        time: l,
                        pos: k
                    }, fa(a), l = Tc(G(g, k.line).text, k), na(e.doc, l.from, l.to)) : qc = {
                        time: l,
                        pos: k
                    };
                    var p = k;
                    if (!e.options.dragDrop || !oe || Ia(e) || V(h.from, h.to) || X(k, h.from) || X(h.to, k) || "single" != n) {
                        fa(a);
                        "single" == n && na(e.doc, J(g, k));
                        var m =
                            h.from,
                            z = h.to,
                            r = k,
                            q = $(f.wrapper),
                            D = 0,
                            ba = N(e, function(a) {
                                Z || Ad(a) ? c(a) : d(a)
                            }),
                            ca = N(e, d);
                        Q(document, "mousemove", ba);
                        Q(document, "mouseup", ca)
                    } else {
                        var F = N(e, function(b) {
                            ia && (f.scroller.draggable = !1);
                            e.state.draggingText = !1;
                            Pa(document, "mouseup", F);
                            Pa(f.scroller, "drop", F);
                            10 > Math.abs(a.clientX - b.clientX) + Math.abs(a.clientY - b.clientY) && (fa(b), na(e.doc, k), pa(e))
                        });
                        ia && (f.scroller.draggable = !0);
                        e.state.draggingText = F;
                        f.scroller.dragDrop && f.scroller.dragDrop();
                        Q(document, "mouseup", F);
                        Q(f.scroller, "drop",
                            F)
                    }
                } else(a.target || a.srcElement) == f.scroller && fa(a)
            }
        }
    }

    function wb(a, b, c, d, e) {
        try {
            var f = b.clientX,
                g = b.clientY
        } catch (h) {
            return !1
        }
        if (f >= Math.floor($(a.display.gutters).right)) return !1;
        d && fa(b);
        d = a.display;
        var k = $(d.lineDiv);
        if (g > k.bottom || !Ma(a, c)) return Wc(b);
        g -= k.top - d.viewOffset;
        for (k = 0; k < a.options.gutters.length; ++k) {
            var l = d.gutters.childNodes[k];
            if (l && $(l).right >= f) return f = Gb(a.doc, g), e(a, c, a, f, a.options.gutters[k], b), Wc(b)
        }
    }

    function Tb(a, b) {
        return Ma(a, "gutterContextMenu") ? wb(a, b, "gutterContextMenu", !1, ja) : !1
    }

    function cb(a) {
        var b = this;
        if (!(Ja(b, a) || Ka(b.display, a) || b.options.onDragEvent && b.options.onDragEvent(b, Qb(a)))) {
            fa(a);
            Z && (zd = +new Date);
            var c = ya(b, a, !0),
                d = a.dataTransfer.files;
            if (c && !Ia(b))
                if (d && d.length && window.FileReader && window.File) {
                    var e = d.length,
                        f = Array(e),
                        g = 0;
                    a = function(a, d) {
                        var P = new FileReader;
                        P.onload = function() {
                            f[d] = P.result;
                            ++g == e && (c = J(b.doc, c), vb(b.doc, {
                                from: c,
                                to: c,
                                text: bb(f.join("\n")),
                                origin: "paste"
                            }, "around"))
                        };
                        P.readAsText(a)
                    };
                    for (var h = 0; h < e; ++h) a(d[h], h)
                } else if (!b.state.draggingText ||
                X(c, b.doc.sel.from) || X(b.doc.sel.to, c)) try {
                if (f = a.dataTransfer.getData("Text")) {
                    var h = b.doc.sel.from,
                        k = b.doc.sel.to;
                    Qa(b.doc, c, c);
                    b.state.draggingText && Va(b.doc, "", h, k, "paste");
                    b.replaceSelection(f, null, "paste");
                    pa(b)
                }
            } catch (l) {} else b.state.draggingText(a), setTimeout(za(pa, b), 20)
        }
    }

    function La(a, b) {
        2 > Math.abs(a.doc.scrollTop - b) || (a.doc.scrollTop = b, xb || B(a, [], b), a.display.scroller.scrollTop != b && (a.display.scroller.scrollTop = b), a.display.scrollbarV.scrollTop != b && (a.display.scrollbarV.scrollTop = b), xb &&
            B(a, []), U(a, 100))
    }

    function Ua(a, b, c) {
        (c ? b == a.doc.scrollLeft : 2 > Math.abs(a.doc.scrollLeft - b)) || (b = Math.min(b, a.display.scroller.scrollWidth - a.display.scroller.clientWidth), a.doc.scrollLeft = b, L(a), a.display.scroller.scrollLeft != b && (a.display.scroller.scrollLeft = b), a.display.scrollbarH.scrollLeft != b && (a.display.scrollbarH.scrollLeft = b))
    }

    function Sb(a, b) {
        var c = b.wheelDeltaX,
            d = b.wheelDeltaY;
        null == c && b.detail && b.axis == b.HORIZONTAL_AXIS && (c = b.detail);
        null == d && b.detail && b.axis == b.VERTICAL_AXIS ? d = b.detail :
            null == d && (d = b.wheelDelta);
        var e = a.display,
            f = e.scroller;
        if (c && f.scrollWidth > f.clientWidth || d && f.scrollHeight > f.clientHeight) {
            if (d && mb && ia)
                for (var g = b.target; g != f; g = g.parentNode)
                    if (g.lineObj) {
                        a.display.currentWheelTarget = g;
                        break
                    }
            if (!c || xb || Ca || null == Da) {
                if (d && null != Da) {
                    var g = d * Da,
                        h = a.doc.scrollTop,
                        k = h + e.wrapper.clientHeight;
                    0 > g ? h = Math.max(0, h + g - 50) : k = Math.min(a.doc.height, k + g + 50);
                    B(a, [], {
                        top: h,
                        bottom: k
                    })
                }
                20 > rc && (null == e.wheelStartX ? (e.wheelStartX = f.scrollLeft, e.wheelStartY = f.scrollTop, e.wheelDX = c, e.wheelDY =
                    d, setTimeout(function() {
                        if (null != e.wheelStartX) {
                            var a = f.scrollLeft - e.wheelStartX,
                                b = f.scrollTop - e.wheelStartY,
                                a = b && e.wheelDY && b / e.wheelDY || a && e.wheelDX && a / e.wheelDX;
                            e.wheelStartX = e.wheelStartY = null;
                            a && (Da = (Da * rc + a) / (rc + 1), ++rc)
                        }
                    }, 200)) : (e.wheelDX += c, e.wheelDY += d))
            } else d && La(a, Math.max(0, Math.min(f.scrollTop + d * Da, f.scrollHeight - f.clientHeight))), Ua(a, Math.max(0, Math.min(f.scrollLeft + c * Da, f.scrollWidth - f.clientWidth))), fa(b), e.wheelStartX = null
        }
    }

    function I(a, b, c) {
        if ("string" == typeof b && (b = Xc[b], !b)) return !1;
        a.display.pollingFast && ub(a) && (a.display.pollingFast = !1);
        var d = a.doc,
            e = d.sel.shift,
            f = !1;
        try {
            Ia(a) && (a.state.suppressEdits = !0), c && (d.sel.shift = !1), f = b(a) != Bd
        } finally {
            d.sel.shift = e, a.state.suppressEdits = !1
        }
        return f
    }

    function sc(a) {
        var b = a.state.keyMaps.slice(0);
        a.options.extraKeys && b.push(a.options.extraKeys);
        b.push(a.options.keyMap);
        return b
    }

    function n(a, b) {
        var c = Yc(a.options.keyMap),
            d = c.auto;
        clearTimeout(Cd);
        d && !Dd(b) && (Cd = setTimeout(function() {
            Yc(a.options.keyMap) == c && (a.options.keyMap = d.call ? d.call(null,
                a) : d, f(a))
        }, 50));
        var e = Ed(b, !0),
            g = !1;
        if (!e) return !1;
        g = sc(a);
        if (g = b.shiftKey ? Ub("Shift-" + e, g, function(b) {
                return I(a, b, !0)
            }) || Ub(e, g, function(b) {
                if ("string" == typeof b ? /^go[A-Z]/.test(b) : b.motion) return I(a, b)
            }) : Ub(e, g, function(b) {
                return I(a, b)
            })) fa(b), D(a), wa && (b.oldKeyCode = b.keyCode, b.keyCode = 0), la(a, "keyHandled", a, e, b);
        return g
    }

    function z(a, b, c) {
        var d = Ub("'" + c + "'", sc(a), function(b) {
            return I(a, b, !0)
        });
        d && (fa(b), D(a), la(a, "keyHandled", a, "'" + c + "'", b));
        return d
    }

    function ba(a) {
        this.state.focused || Bb(this);
        if (!(Ja(this, a) || this.options.onKeyEvent && this.options.onKeyEvent(this, Qb(a)))) {
            Z && 27 == a.keyCode && (a.returnValue = !1);
            var b = a.keyCode;
            this.doc.sel.shift = 16 == b || a.shiftKey;
            var c = n(this, a);
            Ca && (Zc = c ? b : null, !c && 88 == b && !wd && (mb ? a.metaKey : a.ctrlKey) && this.replaceSelection(""))
        }
    }

    function ca(a) {
        var b = this;
        if (!(Ja(b, a) || b.options.onKeyEvent && b.options.onKeyEvent(b, Qb(a)))) {
            var c = a.keyCode,
                d = a.charCode;
            Ca && c == Zc ? (Zc = null, fa(a)) : (Ca && (!a.which || 10 > a.which) || Kc) && n(b, a) || (c = String.fromCharCode(null == d ? c : d), this.options.electricChars &&
                this.doc.mode.electricChars && this.options.smartIndent && !Ia(this) && -1 < this.doc.mode.electricChars.indexOf(c) && setTimeout(N(b, function() {
                    tc(b, b.doc.sel.to.line, "smart")
                }), 75), z(b, a, c) || (Z && !wa && (b.display.inputHasSelection = null), ab(b)))
        }
    }

    function Bb(a) {
        "nocursor" != a.options.readOnly && (a.state.focused || (ja(a, "focus", a), a.state.focused = !0, -1 == a.display.wrapper.className.search(/\bCodeMirror-focused\b/) && (a.display.wrapper.className += " CodeMirror-focused"), a.curOp || (ta(a, !0), ia && setTimeout(za(ta, a, !0),
            0))), tb(a), D(a))
    }

    function Ic(a) {
        a.state.focused && (ja(a, "blur", a), a.state.focused = !1, a.display.wrapper.className = a.display.wrapper.className.replace(" CodeMirror-focused", ""));
        clearInterval(a.display.blinker);
        setTimeout(function() {
            a.state.focused || (a.doc.sel.shift = !1)
        }, 150)
    }

    function yd(a, b) {
        function c() {
            if (null != e.input.selectionStart) {
                var a = e.input.value = "\u200b" + (V(f.from, f.to) ? "" : e.input.value);
                e.prevInput = "\u200b";
                e.input.selectionStart = 1;
                e.input.selectionEnd = a.length
            }
        }

        function d() {
            e.inputDiv.style.position =
                "relative";
            e.input.style.cssText = k;
            wa && (e.scrollbarV.scrollTop = e.scroller.scrollTop = h);
            tb(a);
            if (null != e.input.selectionStart) {
                Z && !wa || c();
                clearTimeout($c);
                var b = 0,
                    f = function() {
                        " " == e.prevInput && 0 == e.input.selectionStart ? N(a, Xc.selectAll)(a) : 10 > b++ ? $c = setTimeout(f, 500) : ta(a)
                    };
                $c = setTimeout(f, 200)
            }
        }
        if (!Ja(a, b, "contextmenu")) {
            var e = a.display,
                f = a.doc.sel;
            if (!Ka(e, b) && !Tb(a, b)) {
                var g = ya(a, b),
                    h = e.scroller.scrollTop;
                if (g && !Ca) {
                    a.options.resetSelectionOnContextMenu && (V(f.from, f.to) || X(g, f.from) || !X(g, f.to)) &&
                        N(a, Qa)(a.doc, g, g);
                    var k = e.input.style.cssText;
                    e.inputDiv.style.position = "absolute";
                    e.input.style.cssText = "position: fixed; width: 30px; height: 30px; top: " + (b.clientY - 5) + "px; left: " + (b.clientX - 5) + "px; z-index: 1000; background: white; outline: none;border-width: 0; outline: none; overflow: hidden; opacity: .05; -ms-opacity: .05; filter: alpha(opacity=5);";
                    pa(a);
                    ta(a, !0);
                    V(f.from, f.to) && (e.input.value = e.prevInput = " ");
                    Z && !wa && c();
                    if (Uc) {
                        Rb(b);
                        var l = function() {
                            Pa(window, "mouseup", l);
                            setTimeout(d,
                                20)
                        };
                        Q(window, "mouseup", l)
                    } else setTimeout(d, 50)
                }
            }
        }
    }

    function Fd(a, b, c) {
        if (!X(b.from, c)) return J(a, c);
        var d = b.text.length - 1 - (b.to.line - b.from.line);
        if (c.line > b.to.line + d) return b = c.line - d, d = a.first + a.size - 1, b > d ? w(d, G(a, d).text.length) : uc(c, G(a, b).text.length);
        if (c.line == b.to.line + d) return uc(c, ra(b.text).length + (1 == b.text.length ? b.from.ch : 0) + G(a, b.to.line).text.length - b.to.ch);
        a = c.line - b.from.line;
        return uc(c, b.text[a].length + (a ? 0 : b.from.ch))
    }

    function ad(a, b, c) {
        if (c && "object" == typeof c) return {
            anchor: Fd(a,
                b, c.anchor),
            head: Fd(a, b, c.head)
        };
        if ("start" == c) return {
            anchor: b.from,
            head: b.from
        };
        var d = bd(b);
        if ("around" == c) return {
            anchor: b.from,
            head: d
        };
        if ("end" == c) return {
            anchor: d,
            head: d
        };
        c = function(a) {
            if (X(a, b.from)) return a;
            if (!X(b.to, a)) return d;
            var c = a.line + b.text.length - (b.to.line - b.from.line) - 1,
                e = a.ch;
            a.line == b.to.line && (e += d.ch - b.to.ch);
            return w(c, e)
        };
        return {
            anchor: c(a.sel.anchor),
            head: c(a.sel.head)
        }
    }

    function Gd(a, b, c) {
        b = {
            canceled: !1,
            from: b.from,
            to: b.to,
            text: b.text,
            origin: b.origin,
            cancel: function() {
                this.canceled = !0
            }
        };
        c && (b.update = function(b, c, d, e) {
            b && (this.from = J(a, b));
            c && (this.to = J(a, c));
            d && (this.text = d);
            void 0 !== e && (this.origin = e)
        });
        ja(a, "beforeChange", a, b);
        a.cm && ja(a.cm, "beforeChange", a.cm, b);
        return b.canceled ? null : {
            from: b.from,
            to: b.to,
            text: b.text,
            origin: b.origin
        }
    }

    function vb(a, b, c, d) {
        if (a.cm) {
            if (!a.cm.curOp) return N(a.cm, vb)(a, b, c, d);
            if (a.cm.state.suppressEdits) return
        }
        if (Ma(a, "beforeChange") || a.cm && Ma(a.cm, "beforeChange"))
            if (b = Gd(a, b, !0), !b) return;
        if (d = Hd && !d && pe(a, b.from, b.to)) {
            for (var e = d.length - 1; 1 <=
                e; --e) cd(a, {
                from: d[e].from,
                to: d[e].to,
                text: [""]
            });
            d.length && cd(a, {
                from: d[0].from,
                to: d[0].to,
                text: b.text
            }, c)
        } else cd(a, b, c)
    }

    function cd(a, b, c) {
        if (1 != b.text.length || "" != b.text[0] || !V(b.from, b.to)) {
            c = ad(a, b, c);
            Id(a, b, c, a.cm ? a.cm.curOp.id : NaN);
            Vb(a, b, c, dd(a, b));
            var d = [];
            yb(a, function(a, c) {
                c || -1 != Ea(d, a.history) || (Jd(a.history, b), d.push(a.history));
                Vb(a, b, null, dd(a, b))
            })
        }
    }

    function Kd(a, b) {
        if (!a.cm || !a.cm.state.suppressEdits) {
            var c = a.history,
                d = ("undo" == b ? c.done : c.undone).pop();
            if (d) {
                var e = {
                    changes: [],
                    anchorBefore: d.anchorAfter,
                    headBefore: d.headAfter,
                    anchorAfter: d.anchorBefore,
                    headAfter: d.headBefore,
                    generation: c.generation
                };
                ("undo" == b ? c.undone : c.done).push(e);
                c.generation = d.generation || ++c.maxGeneration;
                for (var f = Ma(a, "beforeChange") || a.cm && Ma(a.cm, "beforeChange"), g = d.changes.length - 1; 0 <= g; --g) {
                    var h = d.changes[g];
                    h.origin = b;
                    if (f && !Gd(a, h, !1)) {
                        ("undo" == b ? c.done : c.undone).length = 0;
                        break
                    }
                    e.changes.push(ed(a, h));
                    var k = g ? ad(a, h, null) : {
                        anchor: d.anchorBefore,
                        head: d.headBefore
                    };
                    Vb(a, h, k, Ld(a, h));
                    var l = [];
                    yb(a, function(a, b) {
                        b || -1 !=
                            Ea(l, a.history) || (Jd(a.history, h), l.push(a.history));
                        Vb(a, h, null, Ld(a, h))
                    })
                }
            }
        }
    }

    function Md(a, b) {
        function c(a) {
            return w(a.line + b, a.ch)
        }
        a.first += b;
        a.cm && aa(a.cm, a.first, a.first, b);
        a.sel.head = c(a.sel.head);
        a.sel.anchor = c(a.sel.anchor);
        a.sel.from = c(a.sel.from);
        a.sel.to = c(a.sel.to)
    }

    function Vb(a, b, c, d) {
        if (a.cm && !a.cm.curOp) return N(a.cm, Vb)(a, b, c, d);
        if (b.to.line < a.first) Md(a, b.text.length - 1 - (b.to.line - b.from.line));
        else if (!(b.from.line > a.lastLine())) {
            if (b.from.line < a.first) {
                var e = b.text.length - 1 - (a.first -
                    b.from.line);
                Md(a, e);
                b = {
                    from: w(a.first, 0),
                    to: w(b.to.line + e, b.to.ch),
                    text: [ra(b.text)],
                    origin: b.origin
                }
            }
            e = a.lastLine();
            b.to.line > e && (b = {
                from: b.from,
                to: w(e, G(a, e).text.length),
                text: [b.text[0]],
                origin: b.origin
            });
            b.removed = fd(a, b.from, b.to);
            c || (c = ad(a, b, null));
            a.cm ? qe(a.cm, b, d, c) : gd(a, b, d, c)
        }
    }

    function qe(a, b, c, e) {
        var f = a.doc,
            g = a.display,
            h = b.from,
            k = b.to,
            n = !1,
            p = h.line;
        a.options.lineWrapping || (p = va(Oa(f, G(f, h.line))), f.iter(p, k.line + 1, function(a) {
            if (a == g.maxLine) return n = !0
        }));
        X(f.sel.head, b.from) || X(b.to,
            f.sel.head) || (a.curOp.cursorActivity = !0);
        gd(f, b, c, e, d(a));
        a.options.lineWrapping || (f.iter(p, h.line + b.text.length, function(a) {
            var b = l(f, a);
            b > g.maxLineLength && (g.maxLine = a, g.maxLineLength = b, g.maxLineChanged = !0, n = !1)
        }), n && (a.curOp.updateMaxLine = !0));
        f.frontier = Math.min(f.frontier, h.line);
        U(a, 400);
        aa(a, h.line, k.line + 1, b.text.length - (k.line - h.line) - 1);
        if (Ma(a, "change"))
            if (b = {
                    from: h,
                    to: k,
                    text: b.text,
                    removed: b.removed,
                    origin: b.origin
                }, a.curOp.textChanged) {
                for (a = a.curOp.textChanged; a.next; a = a.next);
                a.next =
                    b
            } else a.curOp.textChanged = b
    }

    function Va(a, b, c, d, e) {
        d || (d = c);
        if (X(d, c)) {
            var f = d;
            d = c;
            c = f
        }
        "string" == typeof b && (b = bb(b));
        vb(a, {
            from: c,
            to: d,
            text: b,
            origin: e
        }, null)
    }

    function w(a, b) {
        if (!(this instanceof w)) return new w(a, b);
        this.line = a;
        this.ch = b
    }

    function V(a, b) {
        return a.line == b.line && a.ch == b.ch
    }

    function X(a, b) {
        return a.line < b.line || a.line == b.line && a.ch < b.ch
    }

    function db(a) {
        return w(a.line, a.ch)
    }

    function J(a, b) {
        if (b.line < a.first) return w(a.first, 0);
        var c = a.first + a.size - 1;
        return b.line > c ? w(c, G(a, c).text.length) :
            uc(b, G(a, b.line).text.length)
    }

    function uc(a, b) {
        var c = a.ch;
        return null == c || c > b ? w(a.line, b) : 0 > c ? w(a.line, 0) : a
    }

    function zb(a, b) {
        return b >= a.first && b < a.first + a.size
    }

    function na(a, b, c, d) {
        if (a.sel.shift || a.sel.extend) {
            var e = a.sel.anchor;
            if (c) {
                var f = X(b, e);
                f != X(c, e) ? (e = b, b = c) : f != X(b, c) && (b = c)
            }
            Qa(a, e, b, d)
        } else Qa(a, b, c || b, d);
        a.cm && (a.cm.curOp.userSelChange = !0)
    }

    function Qa(a, b, c, d, e) {
        if (!e && Ma(a, "beforeSelectionChange") || a.cm && Ma(a.cm, "beforeSelectionChange")) b = {
            anchor: b,
            head: c
        }, ja(a, "beforeSelectionChange",
            a, b), a.cm && ja(a.cm, "beforeSelectionChange", a.cm, b), b.anchor = J(a, b.anchor), b.head = J(a, b.head), c = b.head, b = b.anchor;
        var f = a.sel;
        f.goalColumn = null;
        null == d && (d = X(c, f.head) ? -1 : 1);
        if (e || !V(b, f.anchor)) b = vc(a, b, d, "push" != e);
        if (e || !V(c, f.head)) c = vc(a, c, d, "push" != e);
        V(f.anchor, b) && V(f.head, c) || (f.anchor = b, f.head = c, d = X(c, b), f.from = d ? c : b, f.to = d ? b : c, a.cm && (a.cm.curOp.updateInput = a.cm.curOp.selectionChanged = a.cm.curOp.cursorActivity = !0), la(a, "cursorActivity", a))
    }

    function Nd(a) {
        Qa(a.doc, a.doc.sel.from, a.doc.sel.to,
            null, "push")
    }

    function vc(a, b, c, d) {
        var e = !1,
            f = b,
            g = c || 1;
        a.cantEdit = !1;
        a: for (;;) {
            var h = G(a, f.line);
            if (h.markedSpans)
                for (var k = 0; k < h.markedSpans.length; ++k) {
                    var l = h.markedSpans[k],
                        n = l.marker;
                    if ((null == l.from || (n.inclusiveLeft ? l.from <= f.ch : l.from < f.ch)) && (null == l.to || (n.inclusiveRight ? l.to >= f.ch : l.to > f.ch))) {
                        if (d && (ja(n, "beforeCursorEnter"), n.explicitlyCleared))
                            if (h.markedSpans) {
                                --k;
                                continue
                            } else break;
                        if (n.atomic) {
                            k = n.find()[0 > g ? "from" : "to"];
                            if (V(k, f) && (k.ch += g, 0 > k.ch ? k = k.line > a.first ? J(a, w(k.line - 1)) :
                                    null : k.ch > h.text.length && (k = k.line < a.first + a.size - 1 ? w(k.line + 1, 0) : null), !k)) {
                                if (e) {
                                    if (!d) return vc(a, b, c, !0);
                                    a.cantEdit = !0;
                                    return w(a.first, 0)
                                }
                                e = !0;
                                k = b;
                                g = -g
                            }
                            f = k;
                            continue a
                        }
                    }
                }
            return f
        }
    }

    function vd(a, b, c, d) {
        for (null == d && (d = 0);;) {
            var e = !1,
                f = ma(a, b),
                g = c && c != b ? ma(a, c) : f,
                g = nc(a, Math.min(f.left, g.left), Math.min(f.top, g.top) - d, Math.max(f.left, g.left), Math.max(f.bottom, g.bottom) + d),
                h = a.doc.scrollTop,
                k = a.doc.scrollLeft;
            null != g.scrollTop && (La(a, g.scrollTop), 1 < Math.abs(a.doc.scrollTop - h) && (e = !0));
            null != g.scrollLeft &&
                (Ua(a, g.scrollLeft), 1 < Math.abs(a.doc.scrollLeft - k) && (e = !0));
            if (!e) return f
        }
    }

    function nc(a, b, c, d, e) {
        var f = a.display,
            g = Aa(a.display);
        0 > c && (c = 0);
        var h = f.scroller.clientHeight - jb,
            k = f.scroller.scrollTop,
            l = {};
        a = a.doc.height + (f.mover.offsetHeight - f.lineSpace.offsetHeight);
        var n = c < g,
            g = e > a - g;
        c < k ? l.scrollTop = n ? 0 : c : e > k + h && (c = Math.min(c, (g ? a : e) - h), c != k && (l.scrollTop = c));
        k = f.scroller.clientWidth - jb;
        c = f.scroller.scrollLeft;
        b += f.gutters.offsetWidth;
        d += f.gutters.offsetWidth;
        f = f.gutters.offsetWidth;
        e = b < f + 10;
        b < c + f ||
            e ? (e && (b = 0), l.scrollLeft = Math.max(0, b - 10 - f)) : d > k + c - 3 && (l.scrollLeft = d + 10 - k);
        return l
    }

    function wc(a, b, c) {
        a.curOp.updateScrollPos = {
            scrollLeft: null == b ? a.doc.scrollLeft : b,
            scrollTop: null == c ? a.doc.scrollTop : c
        }
    }

    function hd(a, b, c) {
        var d = a.curOp.updateScrollPos || (a.curOp.updateScrollPos = {
            scrollLeft: a.doc.scrollLeft,
            scrollTop: a.doc.scrollTop
        });
        a = a.display.scroller;
        d.scrollTop = Math.max(0, Math.min(a.scrollHeight - a.clientHeight, d.scrollTop + c));
        d.scrollLeft = Math.max(0, Math.min(a.scrollWidth - a.clientWidth, d.scrollLeft +
            b))
    }

    function tc(a, b, c, d) {
        var e = a.doc;
        null == c && (c = "add");
        if ("smart" == c)
            if (a.doc.mode.indent) var f = sa(a, b);
            else c = "prev";
        var g = a.options.tabSize,
            h = G(e, b),
            k = pb(h.text, null, g),
            l = h.text.match(/^\s*/)[0],
            n;
        if ("smart" == c && (n = a.doc.mode.indent(f, h.text.slice(l.length), h.text), n == Bd)) {
            if (!d) return;
            c = "prev"
        }
        "prev" == c ? n = b > e.first ? pb(G(e, b - 1).text, null, g) : 0 : "add" == c ? n = k + a.options.indentUnit : "subtract" == c ? n = k - a.options.indentUnit : "number" == typeof c && (n = k + c);
        n = Math.max(0, n);
        c = "";
        d = 0;
        if (a.options.indentWithTabs)
            for (f =
                Math.floor(n / g); f; --f) d += g, c += "\t";
        d < n && (c += Od(n - d));
        c != l ? Va(a.doc, c, w(b, 0), w(b, l.length), "+input") : e.sel.head.line == b && e.sel.head.ch < l.length && Qa(e, w(b, l.length), w(b, l.length), 1);
        h.stateAfter = null
    }

    function xc(a, b, c) {
        var d = b,
            e = b,
            f = a.doc;
        "number" == typeof b ? e = G(f, Math.max(f.first, Math.min(b, f.first + f.size - 1))) : d = va(b);
        if (null != d && c(e, d)) aa(a, d, d + 1);
        else return null;
        return e
    }

    function jd(a, b, c, d, e) {
        function f(b) {
            var d = (e ? Rc : Pd)(k, h, c, !0);
            if (null == d) {
                if (b = !b) b = g + c, b < a.first || b >= a.first + a.size ? b = l = !1 : (g =
                    b, b = k = G(a, b));
                if (b) h = e ? (0 > c ? lc : kc)(k) : 0 > c ? k.text.length : 0;
                else return l = !1
            } else h = d;
            return !0
        }
        var g = b.line,
            h = b.ch;
        b = c;
        var k = G(a, g),
            l = !0;
        if ("char" == d) f();
        else if ("column" == d) f(!0);
        else if ("word" == d || "group" == d) {
            var n = null;
            d = "group" == d;
            for (var p = !0; !(0 > c) || f(!p); p = !1) {
                var m = k.text.charAt(h) || "\n",
                    m = Wb(m) ? "w" : d ? /\s/.test(m) ? null : "p" : null;
                if (n && n != m) {
                    0 > c && (c = 1, f());
                    break
                }
                m && (n = m);
                if (0 < c && !f(!p)) break
            }
        }
        b = vc(a, w(g, h), b, !0);
        l || (b.hitSide = !0);
        return b
    }

    function Qd(a, b, c, d) {
        var e = a.doc,
            f = b.left,
            g;
        "page" == d ?
            (d = Math.min(a.display.wrapper.clientHeight, window.innerHeight || document.documentElement.clientHeight), g = b.top + c * (d - (0 > c ? 1.5 : .5) * Aa(a.display))) : "line" == d && (g = 0 < c ? b.bottom + 3 : b.top - 3);
        for (;;) {
            var h = Mb(a, f, g);
            if (!h.outside) break;
            if (0 > c ? 0 >= g : g >= e.height) {
                h.hitSide = !0;
                break
            }
            g += 5 * c
        }
        return h
    }

    function Tc(a, b) {
        var c = b.ch,
            d = b.ch;
        if (a) {
            (0 > b.xRel || d == a.length) && c ? --c : ++d;
            for (var e = a.charAt(c), e = Wb(e) ? Wb : /\s/.test(e) ? function(a) {
                    return /\s/.test(a)
                } : function(a) {
                    return !/\s/.test(a) && !Wb(a)
                }; 0 < c && e(a.charAt(c - 1));) --c;
            for (; d < a.length && e(a.charAt(d));) ++d
        }
        return {
            from: w(b.line, c),
            to: w(b.line, d)
        }
    }

    function ne(a, b) {
        na(a.doc, w(b, 0), J(a.doc, w(b + 1, 0)))
    }

    function K(b, c, d, e) {
        a.defaults[b] = c;
        d && (ib[b] = e ? function(a, b, c) {
            c != qd && d(a, b, c)
        } : d)
    }

    function ob(a, b) {
        if (!0 === b) return b;
        if (a.copyState) return a.copyState(b);
        var c = {},
            d;
        for (d in b) {
            var e = b[d];
            e instanceof Array && (e = e.concat([]));
            c[d] = e
        }
        return c
    }

    function td(a, b, c) {
        return a.startState ? a.startState(b, c) : !0
    }

    function Yc(a) {
        return "string" == typeof a ? Na[a] : a
    }

    function Ub(a, b, c) {
        function d(b) {
            b =
                Yc(b);
            var e = b[a];
            if (!1 === e) return "stop";
            if (null != e && c(e)) return !0;
            if (b.nofallthrough) return "stop";
            b = b.fallthrough;
            if (null == b) return !1;
            if ("[object Array]" != Object.prototype.toString.call(b)) return d(b);
            for (var e = 0, f = b.length; e < f; ++e) {
                var g = d(b[e]);
                if (g) return g
            }
            return !1
        }
        for (var e = 0; e < b.length; ++e) {
            var f = d(b[e]);
            if (f) return "stop" != f
        }
    }

    function Dd(a) {
        a = eb[a.keyCode];
        return "Ctrl" == a || "Alt" == a || "Shift" == a || "Mod" == a
    }

    function Ed(a, b) {
        if (Ca && 34 == a.keyCode && a["char"]) return !1;
        var c = eb[a.keyCode];
        if (null == c ||
            a.altGraphKey) return !1;
        a.altKey && (c = "Alt-" + c);
        if (Rd ? a.metaKey : a.ctrlKey) c = "Ctrl-" + c;
        if (Rd ? a.ctrlKey : a.metaKey) c = "Cmd-" + c;
        !b && a.shiftKey && (c = "Shift-" + c);
        return c
    }

    function Xb(a, b) {
        this.pos = this.start = 0;
        this.string = a;
        this.tabSize = b || 8;
        this.lastColumnPos = this.lastColumnValue = 0
    }

    function Wa(a, b) {
        this.lines = [];
        this.type = b;
        this.doc = a
    }

    function Yb(a, b, c, d, e) {
        if (d && d.shared) return re(a, b, c, d, e);
        if (a.cm && !a.cm.curOp) return N(a.cm, Yb)(a, b, c, d, e);
        var f = new Wa(a, e);
        if (X(c, b) || V(b, c) && "range" == e && (!d.inclusiveLeft ||
                !d.inclusiveRight)) return f;
        d && yc(d, f);
        f.replacedWith && (f.collapsed = !0, f.replacedWith = C("span", [f.replacedWith], "CodeMirror-widget"), d.handleMouseEvents || (f.replacedWith.ignoreEvents = !0));
        f.collapsed && (Hb = !0);
        f.addToHistory && Id(a, {
            from: b,
            to: c,
            origin: "markText"
        }, {
            head: a.sel.head,
            anchor: a.sel.anchor
        }, NaN);
        var g = b.line,
            h = 0,
            k, l, n = a.cm,
            p;
        a.iter(g, c.line + 1, function(d) {
            n && f.collapsed && !n.options.lineWrapping && Oa(a, d) == n.display.maxLine && (p = !0);
            var e = {
                from: null,
                to: null,
                marker: f
            };
            h += d.text.length;
            g == b.line &&
                (e.from = b.ch, h -= b.ch);
            g == c.line && (e.to = c.ch, h -= d.text.length - c.ch);
            f.collapsed && (g == c.line && (l = lb(d, c.ch)), g == b.line ? k = lb(d, b.ch) : Ba(d, 0));
            d.markedSpans = d.markedSpans ? d.markedSpans.concat([e]) : [e];
            e.marker.attachLine(d);
            ++g
        });
        f.collapsed && a.iter(b.line, c.line + 1, function(b) {
            Xa(a, b) && Ba(b, 0)
        });
        f.clearOnEnter && Q(f, "beforeCursorEnter", function() {
            f.clear()
        });
        f.readOnly && (Hd = !0, (a.history.done.length || a.history.undone.length) && a.clearHistory());
        if (f.collapsed) {
            if (k != l) throw Error("Inserting collapsed marker overlapping an existing one");
            f.size = h;
            f.atomic = !0
        }
        n && (p && (n.curOp.updateMaxLine = !0), (f.className || f.title || f.startStyle || f.endStyle || f.collapsed) && aa(n, b.line, c.line + 1), f.atomic && Nd(n));
        return f
    }

    function Zb(a, b) {
        this.markers = a;
        this.primary = b;
        for (var c = 0, d = this; c < a.length; ++c) a[c].parent = this, Q(a[c], "clear", function() {
            d.clear()
        })
    }

    function re(a, b, c, d, e) {
        d = yc(d);
        d.shared = !1;
        var f = [Yb(a, b, c, d, e)],
            g = f[0],
            h = d.replacedWith;
        yb(a, function(a) {
            h && (d.replacedWith = h.cloneNode(!0));
            f.push(Yb(a, J(a, b), J(a, c), d, e));
            for (var P = 0; P < a.linked.length; ++P)
                if (a.linked[P].isParent) return;
            g = ra(f)
        });
        return new Zb(f, g)
    }

    function $b(a, b) {
        if (a)
            for (var c = 0; c < a.length; ++c) {
                var d = a[c];
                if (d.marker == b) return d
            }
    }

    function dd(a, b) {
        var c = zb(a, b.from.line) && G(a, b.from.line).markedSpans,
            d = zb(a, b.to.line) && G(a, b.to.line).markedSpans;
        if (!c && !d) return null;
        var e = b.from.ch,
            f = b.to.ch,
            g = V(b.from, b.to);
        if (c)
            for (var h = 0, k; h < c.length; ++h) {
                var l = c[h],
                    n = l.marker;
                if (null == l.from || (n.inclusiveLeft ? l.from <= e : l.from < e) || (n.inclusiveLeft && n.inclusiveRight || "bookmark" == n.type) && l.from == e && (!g || !l.marker.insertLeft)) {
                    var p =
                        null == l.to || (n.inclusiveRight ? l.to >= e : l.to > e);
                    (k || (k = [])).push({
                        from: l.from,
                        to: p ? null : l.to,
                        marker: n
                    })
                }
            }
        c = k;
        if (d)
            for (var h = 0, m; h < d.length; ++h)
                if (k = d[h], l = k.marker, null == k.to || (l.inclusiveRight ? k.to >= f : k.to > f) || "bookmark" == l.type && k.from == f && (!g || k.marker.insertLeft)) n = null == k.from || (l.inclusiveLeft ? k.from <= f : k.from < f), (m || (m = [])).push({
                    from: n ? null : k.from - f,
                    to: null == k.to ? null : k.to - f,
                    marker: l
                });
        d = m;
        g = 1 == b.text.length;
        m = ra(b.text).length + (g ? e : 0);
        if (c)
            for (f = 0; f < c.length; ++f)
                if (h = c[f], null == h.to)(k = $b(d,
                    h.marker), k) ? g && (h.to = null == k.to ? null : k.to + m) : h.to = e;
        if (d)
            for (f = 0; f < d.length; ++f) h = d[f], null != h.to && (h.to += m), null == h.from ? (k = $b(c, h.marker), k || (h.from = m, g && (c || (c = [])).push(h))) : (h.from += m, g && (c || (c = [])).push(h));
        if (g && c) {
            for (f = 0; f < c.length; ++f) null != c[f].from && c[f].from == c[f].to && "bookmark" != c[f].marker.type && c.splice(f--, 1);
            c.length || (c = null)
        }
        e = [c];
        if (!g) {
            var g = b.text.length - 2,
                z;
            if (0 < g && c)
                for (f = 0; f < c.length; ++f) null == c[f].to && (z || (z = [])).push({
                    from: null,
                    to: null,
                    marker: c[f].marker
                });
            for (f = 0; f < g; ++f) e.push(z);
            e.push(d)
        }
        return e
    }

    function Ld(a, b) {
        var c;
        if (c = b["spans_" + a.id]) {
            for (var d = 0, e = []; d < b.text.length; ++d) e.push(se(c[d]));
            c = e
        } else c = null;
        d = dd(a, b);
        if (!c) return d;
        if (!d) return c;
        for (e = 0; e < c.length; ++e) {
            var f = c[e],
                g = d[e];
            if (f && g) {
                var h = 0;
                a: for (; h < g.length; ++h) {
                    for (var k = g[h], l = 0; l < f.length; ++l)
                        if (f[l].marker == k.marker) continue a;
                    f.push(k)
                }
            } else g && (c[e] = g)
        }
        return c
    }

    function pe(a, b, c) {
        var d = null;
        a.iter(b.line, c.line + 1, function(a) {
            if (a.markedSpans)
                for (var b = 0; b < a.markedSpans.length; ++b) {
                    var c = a.markedSpans[b].marker;
                    !c.readOnly || d && -1 != Ea(d, c) || (d || (d = [])).push(c)
                }
        });
        if (!d) return null;
        a = [{
            from: b,
            to: c
        }];
        for (b = 0; b < d.length; ++b) {
            c = d[b];
            for (var e = c.find(), f = 0; f < a.length; ++f) {
                var g = a[f];
                if (!X(g.to, e.from) && !X(e.to, g.from)) {
                    var h = [f, 1];
                    (X(g.from, e.from) || !c.inclusiveLeft && V(g.from, e.from)) && h.push({
                        from: g.from,
                        to: e.from
                    });
                    (X(e.to, g.to) || !c.inclusiveRight && V(g.to, e.to)) && h.push({
                        from: e.to,
                        to: g.to
                    });
                    a.splice.apply(a, h);
                    f += h.length - 1
                }
            }
        }
        return a
    }

    function lb(a, b) {
        var c = Hb && a.markedSpans,
            d;
        if (c)
            for (var e, f = 0; f < c.length; ++f) e =
                c[f], e.marker.collapsed && (null == e.from || e.from < b) && (null == e.to || e.to > b) && (!d || d.width < e.marker.width) && (d = e.marker);
        return d
    }

    function gc(a) {
        return lb(a, a.text.length + 1)
    }

    function Oa(a, b) {
        for (var c; c = lb(b, -1);) b = G(a, c.find().from.line);
        return b
    }

    function Xa(a, b) {
        var c = Hb && b.markedSpans;
        if (c)
            for (var d, e = 0; e < c.length; ++e)
                if (d = c[e], d.marker.collapsed && (null == d.from || !d.marker.replacedWith && 0 == d.from && d.marker.inclusiveLeft && kd(a, b, d))) return !0
    }

    function kd(a, b, c) {
        if (null == c.to) return b = c.marker.find().to,
            b = G(a, b.line), kd(a, b, $b(b.markedSpans, c.marker));
        if (c.marker.inclusiveRight && c.to == b.text.length) return !0;
        for (var d, e = 0; e < b.markedSpans.length; ++e)
            if (d = b.markedSpans[e], d.marker.collapsed && !d.marker.replacedWith && d.from == c.to && (d.marker.inclusiveLeft || c.marker.inclusiveRight) && kd(a, b, d)) return !0
    }

    function Sd(a) {
        var b = a.markedSpans;
        if (b) {
            for (var c = 0; c < b.length; ++c) b[c].marker.detachLine(a);
            a.markedSpans = null
        }
    }

    function Td(a, b) {
        if (b) {
            for (var c = 0; c < b.length; ++c) b[c].marker.attachLine(a);
            a.markedSpans =
                b
        }
    }

    function Ud(a) {
        return function() {
            var b = !this.cm.curOp;
            b && xa(this.cm);
            try {
                var c = a.apply(this, arguments)
            } finally {
                b && Ha(this.cm)
            }
            return c
        }
    }

    function jc(a) {
        if (null != a.height) return a.height;
        a.node.parentNode && 1 == a.node.parentNode.nodeType || Ga(a.cm.display.measure, C("div", [a.node], null, "position: relative"));
        return a.height = a.node.offsetHeight
    }

    function te(a, b, c, d) {
        var e = new zc(a, c, d);
        e.noHScroll && (a.display.alignWidgets = !0);
        xc(a, b, function(b) {
            var c = b.widgets || (b.widgets = []);
            null == e.insertAt ? c.push(e) :
                c.splice(Math.min(c.length - 1, Math.max(0, e.insertAt)), 0, e);
            e.line = b;
            if (!Xa(a.doc, b) || e.showIfHidden) c = Ib(a, b) < a.doc.scrollTop, Ba(b, b.height + jc(e)), c && hd(a, 0, e.height);
            return !0
        });
        return e
    }

    function Vd(a, b, c, d, e, f) {
        var g = c.flattenSpans;
        null == g && (g = a.options.flattenSpans);
        var h = 0,
            k = null,
            l = new Xb(b, a.options.tabSize),
            n;
        for ("" == b && c.blankLine && c.blankLine(d); !l.eol();) l.pos > a.options.maxHighlightLength ? (g = !1, f && Mc(a, b, d, l.pos), l.pos = b.length, n = null) : n = c.token(l, d), g && k == n || (h < l.start && e(l.start, k), h = l.start,
            k = n), l.start = l.pos;
        for (; h < l.pos;) a = Math.min(l.pos, h + 5E4), e(a, k), h = a
    }

    function sd(a, b, c, d) {
        var e = [a.state.modeGen];
        Vd(a, b.text, a.doc.mode, c, function(a, b) {
            e.push(a, b)
        }, d);
        for (c = 0; c < a.state.overlays.length; ++c) {
            var f = a.state.overlays[c],
                g = 1,
                h = 0;
            Vd(a, b.text, f.mode, !0, function(a, b) {
                for (var c = g; h < a;) {
                    var d = e[g];
                    d > a && e.splice(g, 1, a, e[g + 1], d);
                    g += 2;
                    h = Math.min(a, d)
                }
                if (b)
                    if (f.opaque) e.splice(c, g - c, a, b), g = c + 2;
                    else
                        for (; c < g; c += 2) d = e[c + 1], e[c + 1] = d ? d + " " + b : b
            })
        }
        return e
    }

    function Wd(a, b) {
        b.styles && b.styles[0] == a.state.modeGen ||
            (b.styles = sd(a, b, b.stateAfter = sa(a, va(b))));
        return b.styles
    }

    function Mc(a, b, c, d) {
        var e = a.doc.mode,
            f = new Xb(b, a.options.tabSize);
        f.start = f.pos = d || 0;
        for ("" == b && e.blankLine && e.blankLine(c); !f.eol() && f.pos <= a.options.maxHighlightLength;) e.token(f, c), f.start = f.pos
    }

    function Xd(a, b) {
        if (!a) return null;
        for (;;) {
            var c = a.match(/(?:^|\s)line-(background-)?(\S+)/);
            if (!c) break;
            a = a.slice(0, c.index) + a.slice(c.index + c[0].length);
            var d = c[1] ? "bgClass" : "textClass";
            null == b[d] ? b[d] = c[2] : (new RegExp("(?:^|s)" + c[2] + "(?:$|s)")).test(b[d]) ||
                (b[d] += " " + c[2])
        }
        return Yd[a] || (Yd[a] = "cm-" + a.replace(/ +/g, " cm-"))
    }

    function Lc(a, b, c, d) {
        for (var e, f = b, g = !0; e = lb(f, -1);) f = G(a.doc, e.find().from.line);
        d = {
            pre: C("pre"),
            col: 0,
            pos: 0,
            measure: null,
            measuredSomething: !1,
            cm: a,
            copyWidgets: d
        };
        do {
            f.text && (g = !1);
            d.measure = f == b && c;
            d.pos = 0;
            d.addToken = d.measure ? ue : Zd;
            (Z || ia) && a.getOption("lineWrapping") && (d.addToken = ve(d.addToken));
            a: {
                e = d;
                var h = Wd(a, f),
                    k = f.markedSpans,
                    l = f.text,
                    n = 0;
                if (k)
                    for (var p = l.length, m = 0, z = 1, r = "", q = void 0, D = 0, ba = void 0, ca = void 0, F = void 0, B =
                            void 0, s = void 0;;) {
                        if (D == m) {
                            for (var ba = ca = F = B = "", s = null, D = Infinity, E = [], v = 0; v < k.length; ++v) {
                                var H = k[v],
                                    U = H.marker;
                                H.from <= m && (null == H.to || H.to > m) ? (null != H.to && D > H.to && (D = H.to, ca = ""), U.className && (ba += " " + U.className), U.startStyle && H.from == m && (F += " " + U.startStyle), U.endStyle && H.to == D && (ca += " " + U.endStyle), U.title && !B && (B = U.title), U.collapsed && (!s || s.marker.size < U.size) && (s = H)) : H.from > m && D > H.from && (D = H.from);
                                "bookmark" == U.type && H.from == m && U.replacedWith && E.push(U)
                            }
                            if (s && (s.from || 0) == m && ($d(e, (null ==
                                    s.to ? p : s.to) - m, s.marker, null == s.from), null == s.to)) {
                                e = s.marker.find();
                                break a
                            }
                            if (!s && E.length)
                                for (v = 0; v < E.length; ++v) $d(e, 0, E[v])
                        }
                        if (m >= p) break;
                        for (E = Math.min(p, D);;) {
                            if (r) {
                                v = m + r.length;
                                s || (H = v > E ? r.slice(0, E - m) : r, e.addToken(e, H, q ? q + ba : ba, F, m + H.length == D ? ca : "", B));
                                if (v >= E) {
                                    r = r.slice(E - m);
                                    m = E;
                                    break
                                }
                                m = v;
                                F = ""
                            }
                            r = l.slice(n, n = h[z++]);
                            q = Xd(h[z++], e)
                        }
                    } else
                        for (var z = 1; z < h.length; z += 2) e.addToken(e, l.slice(n, n = h[z]), Xd(h[z + 1], e));e = void 0
            }
            c && f == b && !d.measuredSomething && (c[0] = d.pre.appendChild(Nb(a.display.measure)),
                d.measuredSomething = !0);
            e && (f = G(a.doc, e.to.line))
        } while (e);
        !c || d.measuredSomething || c[0] || (c[0] = d.pre.appendChild(g ? C("span", "\u00a0") : Nb(a.display.measure)));
        d.pre.firstChild || Xa(a.doc, b) || d.pre.appendChild(document.createTextNode("\u00a0"));
        var u;
        c && (Z || we) && (u = Fa(f)) && (g = u.length - 1, u[g].from == u[g].to && --g, f = u[g], u = u[g - 1], f.from + 1 == f.to && u && f.level < u.level && (c = c[d.pos - 1]) && c.parentNode.insertBefore(c.measureRight = Nb(a.display.measure), c.nextSibling));
        if (c = d.textClass ? d.textClass + " " + (b.textClass ||
                "") : b.textClass) d.pre.className = c;
        ja(a, "renderLine", a, b, d.pre);
        return d
    }

    function Zd(a, b, c, d, e, f) {
        if (b) {
            var g = a.cm.options.specialChars;
            if (g.test(b))
                for (var h = document.createDocumentFragment(), k = 0;;) {
                    g.lastIndex = k;
                    var l = g.exec(b),
                        n = l ? l.index - k : b.length - k;
                    n && (h.appendChild(document.createTextNode(b.slice(k, k + n))), a.col += n);
                    if (!l) break;
                    k += n + 1;
                    "\t" == l[0] ? (l = a.cm.options.tabSize, l -= a.col % l, h.appendChild(C("span", Od(l), "cm-tab")), a.col += l) : (l = a.cm.options.specialCharPlaceholder(l[0]), h.appendChild(l), a.col +=
                        1)
                } else {
                    a.col += b.length;
                    var h = document.createTextNode(b)
                }
            if (c || d || e || a.measure) return b = c || "", d && (b += d), e && (b += e), l = C("span", [h], b), f && (l.title = f), a.pre.appendChild(l);
            a.pre.appendChild(h)
        }
    }

    function ue(a, b, c, d, e) {
        for (var f = a.cm.options.lineWrapping, g = 0; g < b.length; ++g) {
            var h = b.charAt(g),
                k = 0 == g;
            "\ud800" <= h && "\udbff" > h && g < b.length - 1 ? (h = b.slice(g, g + 2), ++g) : g && f && ac(b, g) && a.pre.appendChild(C("wbr"));
            var l = a.measure[a.pos],
                k = a.measure[a.pos] = Zd(a, h, c, k && d, g == b.length - 1 && e);
            l && (k.leftSide = l.leftSide || l);
            Z && f && " " == h && g && !/\s/.test(b.charAt(g - 1)) && g < b.length - 1 && !/\s/.test(b.charAt(g + 1)) && (k.style.whiteSpace = "normal");
            a.pos += h.length
        }
        b.length && (a.measuredSomething = !0)
    }

    function ve(a) {
        function b(a) {
            for (var c = " ", d = 0; d < a.length - 2; ++d) c += d % 2 ? " " : "\u00a0";
            return c + " "
        }
        return function(c, d, e, f, g, h) {
            return a(c, d.replace(/ {3,}/g, b), e, f, g, h)
        }
    }

    function $d(a, b, c, d) {
        if (d = !d && c.replacedWith)
            if (a.copyWidgets && (d = d.cloneNode(!0)), a.pre.appendChild(d), a.measure) {
                if (b) a.measure[a.pos] = d;
                else {
                    var e = Nb(a.cm.display.measure);
                    if ("bookmark" != c.type || c.insertLeft) {
                        if (a.measure[a.pos]) return;
                        a.measure[a.pos] = a.pre.insertBefore(e, d)
                    } else a.measure[a.pos] = a.pre.appendChild(e)
                }
                a.measuredSomething = !0
            }
        a.pos += b
    }

    function gd(a, b, c, d, e) {
        function f(a, c, d) {
            a.text = c;
            a.stateAfter && (a.stateAfter = null);
            a.styles && (a.styles = null);
            null != a.order && (a.order = null);
            Sd(a);
            Td(a, d);
            c = e ? e(a) : 1;
            c != a.height && Ba(a, c);
            la(a, "change", a, b)
        }
        var g = b.from,
            h = b.to,
            k = b.text,
            l = G(a, g.line),
            n = G(a, h.line),
            m = ra(k),
            p = c ? c[k.length - 1] : null,
            z = h.line - g.line;
        if (0 != g.ch ||
            0 != h.ch || "" != m || a.cm && !a.cm.options.wholeLineUpdateBefore)
            if (l == n)
                if (1 == k.length) f(l, l.text.slice(0, g.ch) + m + l.text.slice(h.ch), p);
                else {
                    D = [];
                    r = 1;
                    for (q = k.length - 1; r < q; ++r) D.push(new Sa(k[r], c ? c[r] : null, e));
                    D.push(new Sa(m + l.text.slice(h.ch), p, e));
                    f(l, l.text.slice(0, g.ch) + k[0], c ? c[0] : null);
                    a.insert(g.line + 1, D)
                } else if (1 == k.length) f(l, l.text.slice(0, g.ch) + k[0] + n.text.slice(h.ch), c ? c[0] : null), a.remove(g.line + 1, z);
        else {
            f(l, l.text.slice(0, g.ch) + k[0], c ? c[0] : null);
            f(n, m + n.text.slice(h.ch), p);
            r = 1;
            q = k.length -
                1;
            for (D = []; r < q; ++r) D.push(new Sa(k[r], c ? c[r] : null, e));
            1 < z && a.remove(g.line + 1, z - 1);
            a.insert(g.line + 1, D)
        } else {
            for (var r = 0, q = k.length - 1, D = []; r < q; ++r) D.push(new Sa(k[r], c ? c[r] : null, e));
            f(n, n.text, p);
            z && a.remove(g.line, z);
            D.length && a.insert(g.line, D)
        }
        la(a, "change", a, b);
        Qa(a, d.anchor, d.head, null, !0)
    }

    function Ac(a) {
        this.lines = a;
        this.parent = null;
        for (var b = 0, c = a.length, d = 0; b < c; ++b) a[b].parent = this, d += a[b].height;
        this.height = d
    }

    function bc(a) {
        this.children = a;
        for (var b = 0, c = 0, d = 0, e = a.length; d < e; ++d) {
            var f = a[d],
                b = b + f.chunkSize(),
                c = c + f.height;
            f.parent = this
        }
        this.size = b;
        this.height = c;
        this.parent = null
    }

    function yb(a, b, c) {
        function d(a, e, f) {
            if (a.linked)
                for (var g = 0; g < a.linked.length; ++g) {
                    var P = a.linked[g];
                    if (P.doc != e) {
                        var h = f && P.sharedHist;
                        if (!c || h) b(P.doc, h), d(P.doc, a, h)
                    }
                }
        }
        d(a, null, !0)
    }

    function pd(a, b) {
        if (b.cm) throw Error("This document is already in use.");
        a.doc = b;
        b.cm = a;
        e(a);
        c(a);
        a.options.lineWrapping || q(a);
        a.options.mode = b.modeOption;
        aa(a)
    }

    function G(a, b) {
        for (b -= a.first; !a.lines;)
            for (var c = 0;; ++c) {
                var d = a.children[c],
                    e = d.chunkSize();
                if (b < e) {
                    a = d;
                    break
                }
                b -= e
            }
        return a.lines[b]
    }

    function fd(a, b, c) {
        var d = [],
            e = b.line;
        a.iter(b.line, c.line + 1, function(a) {
            a = a.text;
            e == c.line && (a = a.slice(0, c.ch));
            e == b.line && (a = a.slice(b.ch));
            d.push(a);
            ++e
        });
        return d
    }

    function ld(a, b, c) {
        var d = [];
        a.iter(b, c, function(a) {
            d.push(a.text)
        });
        return d
    }

    function Ba(a, b) {
        for (var c = b - a.height, d = a; d; d = d.parent) d.height += c
    }

    function va(a) {
        if (null == a.parent) return null;
        var b = a.parent;
        a = Ea(b.lines, a);
        for (var c = b.parent; c; b = c, c = c.parent)
            for (var d = 0; c.children[d] !=
                b; ++d) a += c.children[d].chunkSize();
        return a + b.first
    }

    function Gb(a, b) {
        var c = a.first;
        a: do {
            for (var d = 0, e = a.children.length; d < e; ++d) {
                var f = a.children[d],
                    g = f.height;
                if (b < g) {
                    a = f;
                    continue a
                }
                b -= g;
                c += f.chunkSize()
            }
            return c
        } while (!a.lines);
        d = 0;
        for (e = a.lines.length; d < e; ++d) {
            f = a.lines[d].height;
            if (b < f) break;
            b -= f
        }
        return c + d
    }

    function Ib(a, b) {
        b = Oa(a.doc, b);
        for (var c = 0, d = b.parent, e = 0; e < d.lines.length; ++e) {
            var f = d.lines[e];
            if (f == b) break;
            else c += f.height
        }
        for (f = d.parent; f; d = f, f = d.parent)
            for (e = 0; e < f.children.length; ++e) {
                var g =
                    f.children[e];
                if (g == d) break;
                else c += g.height
            }
        return c
    }

    function Fa(a) {
        var b = a.order;
        null == b && (b = a.order = xe(a.text));
        return b
    }

    function Bc(a) {
        return {
            done: [],
            undone: [],
            undoDepth: Infinity,
            lastTime: 0,
            lastOp: null,
            lastOrigin: null,
            generation: a || 1,
            maxGeneration: a || 1
        }
    }

    function ae(a, b, c, d) {
        var e = b["spans_" + a.id],
            f = 0;
        a.iter(Math.max(a.first, c), Math.min(a.first + a.size, d), function(c) {
            c.markedSpans && ((e || (e = b["spans_" + a.id] = {}))[f] = c.markedSpans);
            ++f
        })
    }

    function ed(a, b) {
        var c = {
            from: {
                line: b.from.line,
                ch: b.from.ch
            },
            to: bd(b),
            text: fd(a, b.from, b.to)
        };
        ae(a, c, b.from.line, b.to.line + 1);
        yb(a, function(a) {
            ae(a, c, b.from.line, b.to.line + 1)
        }, !0);
        return c
    }

    function Id(a, b, c, d) {
        var e = a.history;
        e.undone.length = 0;
        var f = +new Date,
            g = ra(e.done);
        if (g && (e.lastOp == d || e.lastOrigin == b.origin && b.origin && ("+" == b.origin.charAt(0) && a.cm && e.lastTime > f - a.cm.options.historyEventDelay || "*" == b.origin.charAt(0)))) {
            var h = ra(g.changes);
            V(b.from, b.to) && V(b.from, h.to) ? h.to = bd(b) : g.changes.push(ed(a, b));
            g.anchorAfter = c.anchor;
            g.headAfter = c.head
        } else
            for (g = {
                    changes: [ed(a, b)],
                    generation: e.generation,
                    anchorBefore: a.sel.anchor,
                    headBefore: a.sel.head,
                    anchorAfter: c.anchor,
                    headAfter: c.head
                }, e.done.push(g), e.generation = ++e.maxGeneration; e.done.length > e.undoDepth;) e.done.shift();
        e.lastTime = f;
        e.lastOp = d;
        e.lastOrigin = b.origin
    }

    function se(a) {
        if (!a) return null;
        for (var b = 0, c; b < a.length; ++b) a[b].marker.explicitlyCleared ? c || (c = a.slice(0, b)) : c && c.push(a[b]);
        return c ? c.length ? c : null : a
    }

    function Cc(a, b) {
        for (var c = 0, d = []; c < a.length; ++c) {
            var e = a[c],
                f = e.changes,
                g = [];
            d.push({
                changes: g,
                anchorBefore: e.anchorBefore,
                headBefore: e.headBefore,
                anchorAfter: e.anchorAfter,
                headAfter: e.headAfter
            });
            for (e = 0; e < f.length; ++e) {
                var h = f[e],
                    k;
                g.push({
                    from: h.from,
                    to: h.to,
                    text: h.text
                });
                if (b)
                    for (var l in h)(k = l.match(/^spans_(\d+)$/)) && -1 < Ea(b, Number(k[1])) && (ra(g)[l] = h[l], delete h[l])
            }
        }
        return d
    }

    function Dc(a, b, c, d) {
        c < a.line ? a.line += d : b < a.line && (a.line = b, a.ch = 0)
    }

    function be(a, b, c, d) {
        for (var e = 0; e < a.length; ++e) {
            for (var f = a[e], g = !0, h = 0; h < f.changes.length; ++h) {
                var k = f.changes[h];
                f.copied || (k.from = db(k.from),
                    k.to = db(k.to));
                if (c < k.from.line) k.from.line += d, k.to.line += d;
                else if (b <= k.to.line) {
                    g = !1;
                    break
                }
            }
            f.copied || (f.anchorBefore = db(f.anchorBefore), f.headBefore = db(f.headBefore), f.anchorAfter = db(f.anchorAfter), f.readAfter = db(f.headAfter), f.copied = !0);
            g ? (Dc(f.anchorBefore), Dc(f.headBefore), Dc(f.anchorAfter), Dc(f.headAfter)) : (a.splice(0, e + 1), e = 0)
        }
    }

    function Jd(a, b) {
        var c = b.from.line,
            d = b.to.line,
            e = b.text.length - (d - c) - 1;
        be(a.done, c, d, e);
        be(a.undone, c, d, e)
    }

    function ye() {
        Rb(this)
    }

    function Qb(a) {
        a.stop || (a.stop = ye);
        return a
    }

    function fa(a) {
        a.preventDefault ? a.preventDefault() : a.returnValue = !1
    }

    function ce(a) {
        a.stopPropagation ? a.stopPropagation() : a.cancelBubble = !0
    }

    function Wc(a) {
        return null != a.defaultPrevented ? a.defaultPrevented : !1 == a.returnValue
    }

    function Rb(a) {
        fa(a);
        ce(a)
    }

    function Ad(a) {
        var b = a.which;
        null == b && (a.button & 1 ? b = 1 : a.button & 2 ? b = 3 : a.button & 4 && (b = 2));
        mb && a.ctrlKey && 1 == b && (b = 3);
        return b
    }

    function Q(a, b, c) {
        a.addEventListener ? a.addEventListener(b, c, !1) : a.attachEvent ? a.attachEvent("on" + b, c) : (a = a._handlers ||
            (a._handlers = {}), (a[b] || (a[b] = [])).push(c))
    }

    function Pa(a, b, c) {
        if (a.removeEventListener) a.removeEventListener(b, c, !1);
        else if (a.detachEvent) a.detachEvent("on" + b, c);
        else if (a = a._handlers && a._handlers[b])
            for (b = 0; b < a.length; ++b)
                if (a[b] == c) {
                    a.splice(b, 1);
                    break
                }
    }

    function ja(a, b) {
        var c = a._handlers && a._handlers[b];
        if (c)
            for (var d = Array.prototype.slice.call(arguments, 2), e = 0; e < c.length; ++e) c[e].apply(null, d)
    }

    function la(a, b) {
        function c(a) {
            return function() {
                a.apply(null, e)
            }
        }
        var d = a._handlers && a._handlers[b];
        if (d) {
            var e = Array.prototype.slice.call(arguments, 2);
            Ta || (++mc, Ta = [], setTimeout(ze, 0));
            for (var f = 0; f < d.length; ++f) Ta.push(c(d[f]))
        }
    }

    function Ja(a, b, c) {
        ja(a, c || b.type, a, b);
        return Wc(b) || b.codemirrorIgnore
    }

    function ze() {
        --mc;
        var a = Ta;
        Ta = null;
        for (var b = 0; b < a.length; ++b) a[b]()
    }

    function Ma(a, b) {
        var c = a._handlers && a._handlers[b];
        return c && 0 < c.length
    }

    function Ab(a) {
        a.prototype.on = function(a, b) {
            Q(this, a, b)
        };
        a.prototype.off = function(a, b) {
            Pa(this, a, b)
        }
    }

    function Hc() {
        this.id = null
    }

    function pb(a, b, c, d, e) {
        null ==
            b && (b = a.search(/[^\s\u00a0]/), -1 == b && (b = a.length));
        d = d || 0;
        for (e = e || 0; d < b; ++d) "\t" == a.charAt(d) ? e += c - e % c : ++e;
        return e
    }

    function Od(a) {
        for (; Ec.length <= a;) Ec.push(ra(Ec) + " ");
        return Ec[a]
    }

    function ra(a) {
        return a[a.length - 1]
    }

    function xd(a) {
        if (Cb) a.selectionStart = 0, a.selectionEnd = a.value.length;
        else try {
            a.select()
        } catch (b) {}
    }

    function Ea(a, b) {
        if (a.indexOf) return a.indexOf(b);
        for (var c = 0, d = a.length; c < d; ++c)
            if (a[c] == b) return c;
        return -1
    }

    function de(a, b) {
        function c() {}
        c.prototype = a;
        var d = new c;
        b && yc(b, d);
        return d
    }

    function yc(a, b) {
        b || (b = {});
        for (var c in a) a.hasOwnProperty(c) && (b[c] = a[c]);
        return b
    }

    function ud(a) {
        for (var b = [], c = 0; c < a; ++c) b.push(void 0);
        return b
    }

    function za(a) {
        var b = Array.prototype.slice.call(arguments, 1);
        return function() {
            return a.apply(null, b)
        }
    }

    function Wb(a) {
        return /\w/.test(a) || "\u0080" < a && (a.toUpperCase() != a.toLowerCase() || Ae.test(a))
    }

    function ee(a) {
        for (var b in a)
            if (a.hasOwnProperty(b) && a[b]) return !1;
        return !0
    }

    function C(a, b, c, d) {
        a = document.createElement(a);
        c && (a.className = c);
        d && (a.style.cssText =
            d);
        if ("string" == typeof b) rd(a, b);
        else if (b)
            for (c = 0; c < b.length; ++c) a.appendChild(b[c]);
        return a
    }

    function Eb(a) {
        for (var b = a.childNodes.length; 0 < b; --b) a.removeChild(a.firstChild);
        return a
    }

    function Ga(a, b) {
        return Eb(a).appendChild(b)
    }

    function rd(a, b) {
        wa ? (a.innerHTML = "", a.appendChild(document.createTextNode(b))) : a.textContent = b
    }

    function $(a) {
        return a.getBoundingClientRect()
    }

    function ac() {
        return !1
    }

    function Fb(a) {
        if (null != Pb) return Pb;
        var b = C("div", null, null, "width: 50px; height: 50px; overflow-x: scroll");
        Ga(a, b);
        b.offsetWidth && (Pb = b.offsetHeight - b.clientHeight);
        return Pb || 0
    }

    function Nb(a) {
        if (null == md) {
            var b = C("span", "\u200b");
            Ga(a, C("span", [b, document.createTextNode("x")]));
            0 != a.firstChild.offsetHeight && (md = 1 >= b.offsetWidth && 2 < b.offsetHeight && !kb)
        }
        return md ? C("span", "\u200b") : C("span", "\u00a0", null, "display: inline-block; width: 1px; margin-right: -1px")
    }

    function je(a, b, c, d) {
        if (!a) return d(b, c, "ltr");
        for (var e = !1, f = 0; f < a.length; ++f) {
            var g = a[f];
            if (g.from < c && g.to > b || b == c && g.to == b) d(Math.max(g.from,
                b), Math.min(g.to, c), 1 == g.level ? "rtl" : "ltr"), e = !0
        }
        e || d(b, c, "ltr")
    }

    function Nc(a) {
        return a.level % 2 ? a.to : a.from
    }

    function Oc(a) {
        return a.level % 2 ? a.from : a.to
    }

    function kc(a) {
        return (a = Fa(a)) ? Nc(a[0]) : 0
    }

    function lc(a) {
        var b = Fa(a);
        return b ? Oc(ra(b)) : a.text.length
    }

    function fe(a, b) {
        var c = G(a.doc, b),
            d = Oa(a.doc, c);
        d != c && (b = va(d));
        d = (c = Fa(d)) ? c[0].level % 2 ? lc(d) : kc(d) : 0;
        return w(b, d)
    }

    function Be(a, b) {
        for (var c, d; c = gc(d = G(a.doc, b));) b = c.find().to.line;
        d = (c = Fa(d)) ? c[0].level % 2 ? kc(d) : lc(d) : d.text.length;
        return w(b,
            d)
    }

    function Pc(a, b) {
        for (var c = 0, d; c < a.length; ++c) {
            var e = a[c];
            if (e.from < b && e.to > b) return sb = null, c;
            if (e.from == b || e.to == b)
                if (null == d) d = c;
                else {
                    var e = e.level,
                        f = a[d].level,
                        g = a[0].level,
                        e = e == g ? !0 : f == g ? !1 : e < f;
                    if (e) return sb = d, c;
                    sb = c;
                    return d
                }
        }
        sb = null;
        return d
    }

    function nd(a, b, c, d) {
        if (!d) return b + c;
        do b += c; while (0 < b && Sc.test(a.text.charAt(b)));
        return b
    }

    function Rc(a, b, c, d) {
        var e = Fa(a);
        if (!e) return Pd(a, b, c, d);
        var f = Pc(e, b),
            g = e[f];
        for (b = nd(a, b, g.level % 2 ? -c : c, d);;) {
            if (b > g.from && b < g.to) return b;
            if (b == g.from ||
                b == g.to) {
                if (Pc(e, b) == f) return b;
                g = e[f + c];
                return 0 < c == g.level % 2 ? g.to : g.from
            }
            g = e[f += c];
            if (!g) return null;
            b = 0 < c == g.level % 2 ? nd(a, g.to, -1, d) : nd(a, g.from, 1, d)
        }
    }

    function Pd(a, b, c, d) {
        b += c;
        if (d)
            for (; 0 < b && Sc.test(a.text.charAt(b));) b += c;
        return 0 > b || b > a.text.length ? null : b
    }
    var xb = /gecko\/\d/i.test(navigator.userAgent),
        Z = /MSIE \d/.test(navigator.userAgent),
        kb = Z && (null == document.documentMode || 8 > document.documentMode),
        wa = Z && (null == document.documentMode || 9 > document.documentMode),
        we = /Trident\/([7-9]|\d{2,})\./.test(navigator.userAgent),
        ia = /WebKit\//.test(navigator.userAgent),
        Ce = ia && /Qt\/\d+\.\d+/.test(navigator.userAgent),
        De = /Chrome\//.test(navigator.userAgent),
        Ca = /Opera\//.test(navigator.userAgent),
        Vc = /Apple Computer/.test(navigator.vendor),
        Kc = /KHTML\//.test(navigator.userAgent),
        he = /Mac OS X 1\d\D([7-9]|\d\d)\D/.test(navigator.userAgent),
        ie = /Mac OS X 1\d\D([8-9]|\d\d)\D/.test(navigator.userAgent),
        le = /PhantomJS/.test(navigator.userAgent),
        Cb = /AppleWebKit/.test(navigator.userAgent) && /Mobile\/\w+/.test(navigator.userAgent),
        Gc = Cb ||
        /Android|webOS|BlackBerry|Opera Mini|Opera Mobi|IEMobile/i.test(navigator.userAgent),
        mb = Cb || /Mac/.test(navigator.platform),
        Ee = /win/i.test(navigator.platform),
        fb = Ca && navigator.userAgent.match(/Version\/(\d*\.\d*)/);
    fb && (fb = Number(fb[1]));
    fb && 15 <= fb && (Ca = !1, ia = !0);
    var Rd = mb && (Ce || Ca && (null == fb || 12.11 > fb)),
        Uc = xb || Z && !wa,
        Hd = !1,
        Hb = !1,
        Za, ke = 0,
        qc, pc, zd = 0,
        rc = 0,
        Da = null;
    Z ? Da = -.53 : xb ? Da = 15 : De ? Da = -.7 : Vc && (Da = -1 / 3);
    var Cd, Zc = null,
        $c, bd = a.changeEnd = function(a) {
            return a.text ? w(a.from.line + a.text.length - 1, ra(a.text).length +
                (1 == a.text.length ? a.from.ch : 0)) : a.to
        };
    a.Pos = w;
    a.prototype = {
        constructor: a,
        focus: function() {
            window.focus();
            pa(this);
            ab(this)
        },
        setOption: function(a, b) {
            var c = this.options,
                d = c[a];
            if (c[a] != b || "mode" == a) c[a] = b, ib.hasOwnProperty(a) && N(this, ib[a])(this, b, d)
        },
        getOption: function(a) {
            return this.options[a]
        },
        getDoc: function() {
            return this.doc
        },
        addKeyMap: function(a, b) {
            this.state.keyMaps[b ? "push" : "unshift"](a)
        },
        removeKeyMap: function(a) {
            for (var b = this.state.keyMaps, c = 0; c < b.length; ++c)
                if (b[c] == a || "string" != typeof b[c] &&
                    b[c].name == a) return b.splice(c, 1), !0
        },
        addOverlay: N(null, function(b, c) {
            var d = b.token ? b : a.getMode(this.options, b);
            if (d.startState) throw Error("Overlays may not be stateful.");
            this.state.overlays.push({
                mode: d,
                modeSpec: b,
                opaque: c && c.opaque
            });
            this.state.modeGen++;
            aa(this)
        }),
        removeOverlay: N(null, function(a) {
            for (var b = this.state.overlays, c = 0; c < b.length; ++c) {
                var d = b[c].modeSpec;
                if (d == a || "string" == typeof a && d.name == a) {
                    b.splice(c, 1);
                    this.state.modeGen++;
                    aa(this);
                    break
                }
            }
        }),
        indentLine: N(null, function(a, b, c) {
            "string" !=
            typeof b && "number" != typeof b && (b = null == b ? this.options.smartIndent ? "smart" : "prev" : b ? "add" : "subtract");
            zb(this.doc, a) && tc(this, a, b, c)
        }),
        indentSelection: N(null, function(a) {
            var b = this.doc.sel;
            if (V(b.from, b.to)) return tc(this, b.from.line, a);
            for (var c = b.to.line - (b.to.ch ? 0 : 1), b = b.from.line; b <= c; ++b) tc(this, b, a)
        }),
        getTokenAt: function(a, b) {
            var c = this.doc;
            a = J(c, a);
            for (var d = sa(this, a.line, b), e = this.doc.mode, c = G(c, a.line), c = new Xb(c.text, this.options.tabSize); c.pos < a.ch && !c.eol();) {
                c.start = c.pos;
                var f = e.token(c,
                    d)
            }
            return {
                start: c.start,
                end: c.pos,
                string: c.current(),
                className: f || null,
                type: f || null,
                state: d
            }
        },
        getTokenTypeAt: function(a) {
            a = J(this.doc, a);
            var b = Wd(this, G(this.doc, a.line)),
                c = 0,
                d = (b.length - 1) / 2;
            a = a.ch;
            if (0 == a) return b[2];
            for (;;) {
                var e = c + d >> 1;
                if ((e ? b[2 * e - 1] : 0) >= a) d = e;
                else if (b[2 * e + 1] < a) c = e + 1;
                else return b[2 * e + 2]
            }
        },
        getModeAt: function(b) {
            var c = this.doc.mode;
            return c.innerMode ? a.innerMode(c, this.getTokenAt(b).state).mode : c
        },
        getHelper: function(a, b) {
            if (cc.hasOwnProperty(b)) {
                var c = cc[b],
                    d = this.getModeAt(a);
                return d[b] && c[d[b]] || d.helperType && c[d.helperType] || c[d.name]
            }
        },
        getStateAfter: function(a, b) {
            var c = this.doc;
            a = Math.max(c.first, Math.min(null == a ? c.first + c.size - 1 : a, c.first + c.size - 1));
            return sa(this, a + 1, b)
        },
        cursorCoords: function(a, b) {
            var c;
            c = this.doc.sel;
            c = null == a ? c.head : "object" == typeof a ? J(this.doc, a) : a ? c.from : c.to;
            return ma(this, c, b || "page")
        },
        charCoords: function(a, b) {
            return nb(this, J(this.doc, a), b || "page")
        },
        coordsChar: function(a, b) {
            a = rb(this, a, b || "page");
            return Mb(this, a.left, a.top)
        },
        lineAtHeight: function(a,
            b) {
            a = rb(this, {
                top: a,
                left: 0
            }, b || "page").top;
            return Gb(this.doc, a + this.display.viewOffset)
        },
        heightAtLine: function(a, b) {
            var c = !1,
                d = this.doc.first + this.doc.size - 1;
            a < this.doc.first ? a = this.doc.first : a > d && (a = d, c = !0);
            d = G(this.doc, a);
            return Lb(this, G(this.doc, a), {
                top: 0,
                left: 0
            }, b || "page").top + (c ? d.height : 0)
        },
        defaultTextHeight: function() {
            return Aa(this.display)
        },
        defaultCharWidth: function() {
            return Db(this.display)
        },
        setGutterMarker: N(null, function(a, b, c) {
            return xc(this, a, function(a) {
                var d = a.gutterMarkers || (a.gutterMarkers = {});
                d[b] = c;
                !c && ee(d) && (a.gutterMarkers = null);
                return !0
            })
        }),
        clearGutter: N(null, function(a) {
            var b = this,
                c = b.doc,
                d = c.first;
            c.iter(function(c) {
                c.gutterMarkers && c.gutterMarkers[a] && (c.gutterMarkers[a] = null, aa(b, d, d + 1), ee(c.gutterMarkers) && (c.gutterMarkers = null));
                ++d
            })
        }),
        addLineClass: N(null, function(a, b, c) {
            return xc(this, a, function(a) {
                var d = "text" == b ? "textClass" : "background" == b ? "bgClass" : "wrapClass";
                if (a[d]) {
                    if ((new RegExp("(?:^|\\s)" + c + "(?:$|\\s)")).test(a[d])) return !1;
                    a[d] += " " + c
                } else a[d] = c;
                return !0
            })
        }),
        removeLineClass: N(null, function(a, b, c) {
            return xc(this, a, function(a) {
                var d = "text" == b ? "textClass" : "background" == b ? "bgClass" : "wrapClass",
                    e = a[d];
                if (e)
                    if (null == c) a[d] = null;
                    else {
                        var f = e.match(new RegExp("(?:^|\\s+)" + c + "(?:$|\\s+)"));
                        if (!f) return !1;
                        var g = f.index + f[0].length;
                        a[d] = e.slice(0, f.index) + (f.index && g != e.length ? " " : "") + e.slice(g) || null
                    } else return !1;
                return !0
            })
        }),
        addLineWidget: N(null, function(a, b, c) {
            return te(this, a, b, c)
        }),
        removeLineWidget: function(a) {
            a.clear()
        },
        lineInfo: function(a) {
            if ("number" ==
                typeof a) {
                if (!zb(this.doc, a)) return null;
                var b = a;
                a = G(this.doc, a);
                if (!a) return null
            } else if (b = va(a), null == b) return null;
            return {
                line: b,
                handle: a,
                text: a.text,
                gutterMarkers: a.gutterMarkers,
                textClass: a.textClass,
                bgClass: a.bgClass,
                wrapClass: a.wrapClass,
                widgets: a.widgets
            }
        },
        getViewport: function() {
            return {
                from: this.display.showingFrom,
                to: this.display.showingTo
            }
        },
        addWidget: function(a, b, c, d, e) {
            var f = this.display;
            a = ma(this, J(this.doc, a));
            var g = a.bottom,
                h = a.left;
            b.style.position = "absolute";
            f.sizer.appendChild(b);
            if ("over" == d) g = a.top;
            else if ("above" == d || "near" == d) {
                var k = Math.max(f.wrapper.clientHeight, this.doc.height),
                    l = Math.max(f.sizer.clientWidth, f.lineSpace.clientWidth);
                ("above" == d || a.bottom + b.offsetHeight > k) && a.top > b.offsetHeight ? g = a.top - b.offsetHeight : a.bottom + b.offsetHeight <= k && (g = a.bottom);
                h + b.offsetWidth > l && (h = l - b.offsetWidth)
            }
            b.style.top = g + "px";
            b.style.left = b.style.right = "";
            "right" == e ? (h = f.sizer.clientWidth - b.offsetWidth, b.style.right = "0px") : ("left" == e ? h = 0 : "middle" == e && (h = (f.sizer.clientWidth - b.offsetWidth) /
                2), b.style.left = h + "px");
            c && (a = nc(this, h, g, h + b.offsetWidth, g + b.offsetHeight), null != a.scrollTop && La(this, a.scrollTop), null != a.scrollLeft && Ua(this, a.scrollLeft))
        },
        triggerOnKeyDown: N(null, ba),
        execCommand: function(a) {
            return Xc[a](this)
        },
        findPosH: function(a, b, c, d) {
            var e = 1;
            0 > b && (e = -1, b = -b);
            var f = 0;
            for (a = J(this.doc, a); f < b && (a = jd(this.doc, a, e, c, d), !a.hitSide); ++f);
            return a
        },
        moveH: N(null, function(a, b) {
            var c = this.doc.sel,
                c = c.shift || c.extend || V(c.from, c.to) ? jd(this.doc, c.head, a, b, this.options.rtlMoveVisually) :
                0 > a ? c.from : c.to;
            na(this.doc, c, c, a)
        }),
        deleteH: N(null, function(a, b) {
            var c = this.doc.sel;
            V(c.from, c.to) ? Va(this.doc, "", c.from, jd(this.doc, c.head, a, b, !1), "+delete") : Va(this.doc, "", c.from, c.to, "+delete");
            this.curOp.userSelChange = !0
        }),
        findPosV: function(a, b, c, d) {
            var e = 1;
            0 > b && (e = -1, b = -b);
            var f = 0;
            for (a = J(this.doc, a); f < b && (a = ma(this, a, "div"), null == d ? d = a.left : a.left = d, a = Qd(this, a, e, c), !a.hitSide); ++f);
            return a
        },
        moveV: N(null, function(a, b) {
            var c = this.doc.sel,
                d = ma(this, c.head, "div");
            null != c.goalColumn && (d.left =
                c.goalColumn);
            var e = Qd(this, d, a, b);
            "page" == b && hd(this, 0, nb(this, e, "div").top - d.top);
            na(this.doc, e, e, a);
            c.goalColumn = d.left
        }),
        toggleOverwrite: function(a) {
            if (null == a || a != this.state.overwrite)(this.state.overwrite = !this.state.overwrite) ? this.display.cursor.className += " CodeMirror-overwrite" : this.display.cursor.className = this.display.cursor.className.replace(" CodeMirror-overwrite", "")
        },
        hasFocus: function() {
            return this.state.focused
        },
        scrollTo: N(null, function(a, b) {
            wc(this, a, b)
        }),
        getScrollInfo: function() {
            var a =
                this.display.scroller,
                b = jb;
            return {
                left: a.scrollLeft,
                top: a.scrollTop,
                height: a.scrollHeight - b,
                width: a.scrollWidth - b,
                clientHeight: a.clientHeight - b,
                clientWidth: a.clientWidth - b
            }
        },
        scrollIntoView: N(null, function(a, b) {
            null == a ? a = {
                from: this.doc.sel.head,
                to: null
            } : "number" == typeof a ? a = {
                from: w(a, 0),
                to: null
            } : null == a.from && (a = {
                from: a,
                to: null
            });
            a.to || (a.to = a.from);
            b || (b = 0);
            var c = a;
            null != a.from.line && (this.curOp.scrollToPos = {
                from: a.from,
                to: a.to,
                margin: b
            }, c = {
                from: ma(this, a.from),
                to: ma(this, a.to)
            });
            c = nc(this, Math.min(c.from.left,
                c.to.left), Math.min(c.from.top, c.to.top) - b, Math.max(c.from.right, c.to.right), Math.max(c.from.bottom, c.to.bottom) + b);
            wc(this, c.scrollLeft, c.scrollTop)
        }),
        setSize: N(null, function(a, b) {
            function c(a) {
                return "number" == typeof a || /^\d+$/.test(String(a)) ? a + "px" : a
            }
            null != a && (this.display.wrapper.style.width = c(a));
            null != b && (this.display.wrapper.style.height = c(b));
            this.options.lineWrapping && (this.display.measureLineCache.length = this.display.measureLineCachePos = 0);
            this.curOp.forceUpdate = !0
        }),
        operation: function(a) {
            return Ob(this,
                a)
        },
        refresh: N(null, function() {
            var a = null == this.display.cachedTextHeight;
            ua(this);
            wc(this, this.doc.scrollLeft, this.doc.scrollTop);
            aa(this);
            a && e(this)
        }),
        swapDoc: N(null, function(a) {
            var b = this.doc;
            b.cm = null;
            pd(this, a);
            ua(this);
            ta(this, !0);
            wc(this, a.scrollLeft, a.scrollTop);
            la(this, "swapDoc", this, b);
            return b
        }),
        getInputField: function() {
            return this.display.input
        },
        getWrapperElement: function() {
            return this.display.wrapper
        },
        getScrollerElement: function() {
            return this.display.scroller
        },
        getGutterElement: function() {
            return this.display.gutters
        }
    };
    Ab(a);
    var ib = a.optionHandlers = {},
        Fc = a.defaults = {},
        qd = a.Init = {
            toString: function() {
                return "CodeMirror.Init"
            }
        };
    K("value", "", function(a, b) {
        a.setValue(b)
    }, !0);
    K("mode", null, function(a, b) {
        a.doc.modeOption = b;
        c(a)
    }, !0);
    K("indentUnit", 2, c, !0);
    K("indentWithTabs", !1);
    K("smartIndent", !0);
    K("tabSize", 4, function(a) {
        c(a);
        ua(a);
        aa(a)
    }, !0);
    K("specialChars", /[\t\u0000-\u0019\u00ad\u200b\u2028\u2029\ufeff]/g, function(a, b) {
        a.options.specialChars = new RegExp(b.source + (b.test("\t") ? "" : "|\t"), "g");
        a.refresh()
    }, !0);
    K("specialCharPlaceholder",
        function(a) {
            var b = C("span", "\u2022", "cm-invalidchar");
            b.title = "\\u" + a.charCodeAt(0).toString(16);
            return b
        },
        function(a) {
            a.refresh()
        }, !0);
    K("electricChars", !0);
    K("rtlMoveVisually", !Ee);
    K("wholeLineUpdateBefore", !0);
    K("theme", "default", function(a) {
        g(a);
        h(a)
    }, !0);
    K("keyMap", "default", f);
    K("extraKeys", null);
    K("onKeyEvent", null);
    K("onDragEvent", null);
    K("lineWrapping", !1, function(a) {
        a.options.lineWrapping ? (a.display.wrapper.className += " CodeMirror-wrap", a.display.sizer.style.minWidth = "") : (a.display.wrapper.className =
            a.display.wrapper.className.replace(" CodeMirror-wrap", ""), q(a));
        e(a);
        aa(a);
        ua(a);
        setTimeout(function() {
            p(a)
        }, 100)
    }, !0);
    K("gutters", [], function(a) {
        m(a.options);
        h(a)
    }, !0);
    K("fixedGutter", !0, function(a, b) {
        a.display.gutters.style.left = b ? r(a.display) + "px" : "0";
        a.refresh()
    }, !0);
    K("coverGutterNextToScrollbar", !1, p, !0);
    K("lineNumbers", !1, function(a) {
        m(a.options);
        h(a)
    }, !0);
    K("firstLineNumber", 1, h, !0);
    K("lineNumberFormatter", function(a) {
        return a
    }, h, !0);
    K("showCursorWhenSelecting", !1, W, !0);
    K("resetSelectionOnContextMenu", !0);
    K("readOnly", !1, function(a, b) {
        "nocursor" == b ? (Ic(a), a.display.input.blur(), a.display.disabled = !0) : (a.display.disabled = !1, b || ta(a, !0))
    });
    K("dragDrop", !0);
    K("cursorBlinkRate", 530);
    K("cursorScrollMargin", 0);
    K("cursorHeight", 1);
    K("workTime", 100);
    K("workDelay", 100);
    K("flattenSpans", !0);
    K("pollInterval", 100);
    K("undoDepth", 40, function(a, b) {
        a.doc.history.undoDepth = b
    });
    K("historyEventDelay", 500);
    K("viewportMargin", 10, function(a) {
        a.refresh()
    }, !0);
    K("maxHighlightLength", 1E4, function(a) {
        c(a);
        a.refresh()
    }, !0);
    K("crudeMeasuringFrom", 1E4);
    K("moveInputWithCursor", !0, function(a, b) {
        b || (a.display.inputDiv.style.top = a.display.inputDiv.style.left = 0)
    });
    K("tabindex", null, function(a, b) {
        a.display.input.tabIndex = b || ""
    });
    K("autofocus", null);
    var ge = a.modes = {},
        dc = a.mimeModes = {};
    a.defineMode = function(b, c) {
        a.defaults.mode || "null" == b || (a.defaults.mode = b);
        if (2 < arguments.length) {
            c.dependencies = [];
            for (var d = 2; d < arguments.length; ++d) c.dependencies.push(arguments[d])
        }
        ge[b] = c
    };
    a.defineMIME = function(a, b) {
        dc[a] = b
    };
    a.resolveMode =
        function(b) {
            if ("string" == typeof b && dc.hasOwnProperty(b)) b = dc[b];
            else if (b && "string" == typeof b.name && dc.hasOwnProperty(b.name)) {
                var c = dc[b.name];
                b = de(c, b);
                b.name = c.name
            } else if ("string" == typeof b && /^[\w\-]+\/[\w\-]+\+xml$/.test(b)) return a.resolveMode("application/xml");
            return "string" == typeof b ? {
                name: b
            } : b || {
                name: "null"
            }
        };
    a.getMode = function(b, c) {
        c = a.resolveMode(c);
        var d = ge[c.name];
        if (!d) return a.getMode(b, "text/plain");
        d = d(b, c);
        if (ec.hasOwnProperty(c.name)) {
            var e = ec[c.name],
                f;
            for (f in e) e.hasOwnProperty(f) &&
                (d.hasOwnProperty(f) && (d["_" + f] = d[f]), d[f] = e[f])
        }
        d.name = c.name;
        return d
    };
    a.defineMode("null", function() {
        return {
            token: function(a) {
                a.skipToEnd()
            }
        }
    });
    a.defineMIME("text/plain", "null");
    var ec = a.modeExtensions = {};
    a.extendMode = function(a, b) {
        var c = ec.hasOwnProperty(a) ? ec[a] : ec[a] = {};
        yc(b, c)
    };
    a.defineExtension = function(b, c) {
        a.prototype[b] = c
    };
    a.defineDocExtension = function(a, b) {
        qa.prototype[a] = b
    };
    a.defineOption = K;
    var Jc = [];
    a.defineInitHook = function(a) {
        Jc.push(a)
    };
    var cc = a.helpers = {};
    a.registerHelper = function(b,
        c, d) {
        cc.hasOwnProperty(b) || (cc[b] = a[b] = {});
        cc[b][c] = d
    };
    a.isWordChar = Wb;
    a.copyState = ob;
    a.startState = td;
    a.innerMode = function(a, b) {
        for (; a.innerMode;) {
            var c = a.innerMode(b);
            if (!c || c.mode == a) break;
            b = c.state;
            a = c.mode
        }
        return c || {
            mode: a,
            state: b
        }
    };
    var Xc = a.commands = {
            selectAll: function(a) {
                a.setSelection(w(a.firstLine(), 0), w(a.lastLine()))
            },
            killLine: function(a) {
                var b = a.getCursor(!0),
                    c = a.getCursor(!1),
                    d = !V(b, c);
                d || a.getLine(b.line).length != b.ch ? a.replaceRange("", b, d ? c : w(b.line), "+delete") : a.replaceRange("", b,
                    w(b.line + 1, 0), "+delete")
            },
            deleteLine: function(a) {
                var b = a.getCursor().line;
                a.replaceRange("", w(b, 0), w(b), "+delete")
            },
            delLineLeft: function(a) {
                var b = a.getCursor();
                a.replaceRange("", w(b.line, 0), b, "+delete")
            },
            undo: function(a) {
                a.undo()
            },
            redo: function(a) {
                a.redo()
            },
            goDocStart: function(a) {
                a.extendSelection(w(a.firstLine(), 0))
            },
            goDocEnd: function(a) {
                a.extendSelection(w(a.lastLine()))
            },
            goLineStart: function(a) {
                a.extendSelection(fe(a, a.getCursor().line))
            },
            goLineStartSmart: function(a) {
                var b = a.getCursor(),
                    c = fe(a,
                        b.line),
                    d = a.getLineHandle(c.line),
                    e = Fa(d);
                e && 0 != e[0].level ? a.extendSelection(c) : (d = Math.max(0, d.text.search(/\S/)), a.extendSelection(w(c.line, b.line == c.line && b.ch <= d && b.ch ? 0 : d)))
            },
            goLineEnd: function(a) {
                a.extendSelection(Be(a, a.getCursor().line))
            },
            goLineRight: function(a) {
                var b = a.charCoords(a.getCursor(), "div").top + 5;
                a.extendSelection(a.coordsChar({
                    left: a.display.lineDiv.offsetWidth + 100,
                    top: b
                }, "div"))
            },
            goLineLeft: function(a) {
                var b = a.charCoords(a.getCursor(), "div").top + 5;
                a.extendSelection(a.coordsChar({
                    left: 0,
                    top: b
                }, "div"))
            },
            goLineUp: function(a) {
                a.moveV(-1, "line")
            },
            goLineDown: function(a) {
                a.moveV(1, "line")
            },
            goPageUp: function(a) {
                a.moveV(-1, "page")
            },
            goPageDown: function(a) {
                a.moveV(1, "page")
            },
            goCharLeft: function(a) {
                a.moveH(-1, "char")
            },
            goCharRight: function(a) {
                a.moveH(1, "char")
            },
            goColumnLeft: function(a) {
                a.moveH(-1, "column")
            },
            goColumnRight: function(a) {
                a.moveH(1, "column")
            },
            goWordLeft: function(a) {
                a.moveH(-1, "word")
            },
            goGroupRight: function(a) {
                a.moveH(1, "group")
            },
            goGroupLeft: function(a) {
                a.moveH(-1, "group")
            },
            goWordRight: function(a) {
                a.moveH(1,
                    "word")
            },
            delCharBefore: function(a) {
                a.deleteH(-1, "char")
            },
            delCharAfter: function(a) {
                a.deleteH(1, "char")
            },
            delWordBefore: function(a) {
                a.deleteH(-1, "word")
            },
            delWordAfter: function(a) {
                a.deleteH(1, "word")
            },
            delGroupBefore: function(a) {
                a.deleteH(-1, "group")
            },
            delGroupAfter: function(a) {
                a.deleteH(1, "group")
            },
            indentAuto: function(a) {
                a.indentSelection("smart")
            },
            indentMore: function(a) {
                a.indentSelection("add")
            },
            indentLess: function(a) {
                a.indentSelection("subtract")
            },
            insertTab: function(a) {
                a.replaceSelection("\t", "end",
                    "+input")
            },
            defaultTab: function(a) {
                a.somethingSelected() ? a.indentSelection("add") : a.replaceSelection("\t", "end", "+input")
            },
            transposeChars: function(a) {
                var b = a.getCursor(),
                    c = a.getLine(b.line);
                0 < b.ch && b.ch < c.length - 1 && a.replaceRange(c.charAt(b.ch) + c.charAt(b.ch - 1), w(b.line, b.ch - 1), w(b.line, b.ch + 1))
            },
            newlineAndIndent: function(a) {
                N(a, function() {
                    a.replaceSelection("\n", "end", "+input");
                    a.indentLine(a.getCursor().line, null, !0)
                })()
            },
            toggleOverwrite: function(a) {
                a.toggleOverwrite()
            }
        },
        Na = a.keyMap = {};
    Na.basic = {
        Left: "goCharLeft",
        Right: "goCharRight",
        Up: "goLineUp",
        Down: "goLineDown",
        End: "goLineEnd",
        Home: "goLineStartSmart",
        PageUp: "goPageUp",
        PageDown: "goPageDown",
        Delete: "delCharAfter",
        Backspace: "delCharBefore",
        "Shift-Backspace": "delCharBefore",
        Tab: "defaultTab",
        "Shift-Tab": "indentAuto",
        Enter: "newlineAndIndent",
        Insert: "toggleOverwrite"
    };
    Na.pcDefault = {
        "Ctrl-A": "selectAll",
        "Ctrl-D": "deleteLine",
        "Ctrl-Z": "undo",
        "Shift-Ctrl-Z": "redo",
        "Ctrl-Y": "redo",
        "Ctrl-Home": "goDocStart",
        "Alt-Up": "goDocStart",
        "Ctrl-End": "goDocEnd",
        "Ctrl-Down": "goDocEnd",
        "Ctrl-Left": "goGroupLeft",
        "Ctrl-Right": "goGroupRight",
        "Alt-Left": "goLineStart",
        "Alt-Right": "goLineEnd",
        "Ctrl-Backspace": "delGroupBefore",
        "Ctrl-Delete": "delGroupAfter",
        "Ctrl-S": "save",
        "Ctrl-F": "find",
        "Ctrl-G": "findNext",
        "Shift-Ctrl-G": "findPrev",
        "Shift-Ctrl-F": "replace",
        "Shift-Ctrl-R": "replaceAll",
        "Ctrl-[": "indentLess",
        "Ctrl-]": "indentMore",
        fallthrough: "basic"
    };
    Na.macDefault = {
        "Cmd-A": "selectAll",
        "Cmd-D": "deleteLine",
        "Cmd-Z": "undo",
        "Shift-Cmd-Z": "redo",
        "Cmd-Y": "redo",
        "Cmd-Up": "goDocStart",
        "Cmd-End": "goDocEnd",
        "Cmd-Down": "goDocEnd",
        "Alt-Left": "goGroupLeft",
        "Alt-Right": "goGroupRight",
        "Cmd-Left": "goLineStart",
        "Cmd-Right": "goLineEnd",
        "Alt-Backspace": "delGroupBefore",
        "Ctrl-Alt-Backspace": "delGroupAfter",
        "Alt-Delete": "delGroupAfter",
        "Cmd-S": "save",
        "Cmd-F": "find",
        "Cmd-G": "findNext",
        "Shift-Cmd-G": "findPrev",
        "Cmd-Alt-F": "replace",
        "Shift-Cmd-Alt-F": "replaceAll",
        "Cmd-[": "indentLess",
        "Cmd-]": "indentMore",
        "Cmd-Backspace": "delLineLeft",
        fallthrough: ["basic", "emacsy"]
    };
    Na["default"] = mb ? Na.macDefault :
        Na.pcDefault;
    Na.emacsy = {
        "Ctrl-F": "goCharRight",
        "Ctrl-B": "goCharLeft",
        "Ctrl-P": "goLineUp",
        "Ctrl-N": "goLineDown",
        "Alt-F": "goWordRight",
        "Alt-B": "goWordLeft",
        "Ctrl-A": "goLineStart",
        "Ctrl-E": "goLineEnd",
        "Ctrl-V": "goPageDown",
        "Shift-Ctrl-V": "goPageUp",
        "Ctrl-D": "delCharAfter",
        "Ctrl-H": "delCharBefore",
        "Alt-D": "delWordAfter",
        "Alt-Backspace": "delWordBefore",
        "Ctrl-K": "killLine",
        "Ctrl-T": "transposeChars"
    };
    a.lookupKey = Ub;
    a.isModifierKey = Dd;
    a.keyName = Ed;
    a.fromTextArea = function(b, c) {
        function d() {
            b.value = n.getValue()
        }
        c || (c = {});
        c.value = b.value;
        !c.tabindex && b.tabindex && (c.tabindex = b.tabindex);
        !c.placeholder && b.placeholder && (c.placeholder = b.placeholder);
        if (null == c.autofocus) {
            var e = document.body;
            try {
                e = document.activeElement
            } catch (f) {}
            c.autofocus = e == b || null != b.getAttribute("autofocus") && e == document.body
        }
        if (b.form && (Q(b.form, "submit", d), !c.leaveSubmitMethodAlone)) {
            var g = b.form,
                h = g.submit;
            try {
                var k = g.submit = function() {
                    d();
                    g.submit = h;
                    g.submit();
                    g.submit = k
                }
            } catch (l) {}
        }
        b.style.display = "none";
        var n = a(function(a) {
            b.parentNode.insertBefore(a,
                b.nextSibling)
        }, c);
        n.save = d;
        n.getTextArea = function() {
            return b
        };
        n.toTextArea = function() {
            d();
            b.parentNode.removeChild(n.getWrapperElement());
            b.style.display = "";
            b.form && (Pa(b.form, "submit", d), "function" == typeof b.form.submit && (b.form.submit = h))
        };
        return n
    };
    Xb.prototype = {
        eol: function() {
            return this.pos >= this.string.length
        },
        sol: function() {
            return 0 == this.pos
        },
        peek: function() {
            return this.string.charAt(this.pos) || void 0
        },
        next: function() {
            if (this.pos < this.string.length) return this.string.charAt(this.pos++)
        },
        eat: function(a) {
            var b = this.string.charAt(this.pos);
            if ("string" == typeof a ? b == a : b && (a.test ? a.test(b) : a(b))) return ++this.pos, b
        },
        eatWhile: function(a) {
            for (var b = this.pos; this.eat(a););
            return this.pos > b
        },
        eatSpace: function() {
            for (var a = this.pos;
                /[\s\u00a0]/.test(this.string.charAt(this.pos));) ++this.pos;
            return this.pos > a
        },
        skipToEnd: function() {
            this.pos = this.string.length
        },
        skipTo: function(a) {
            a = this.string.indexOf(a, this.pos);
            if (-1 < a) return this.pos = a, !0
        },
        backUp: function(a) {
            this.pos -= a
        },
        column: function() {
            this.lastColumnPos <
                this.start && (this.lastColumnValue = pb(this.string, this.start, this.tabSize, this.lastColumnPos, this.lastColumnValue), this.lastColumnPos = this.start);
            return this.lastColumnValue
        },
        indentation: function() {
            return pb(this.string, null, this.tabSize)
        },
        match: function(a, b, c) {
            if ("string" == typeof a) {
                var d = function(a) {
                        return c ? a.toLowerCase() : a
                    },
                    e = this.string.substr(this.pos, a.length);
                if (d(e) == d(a)) return !1 !== b && (this.pos += a.length), !0
            } else {
                if ((a = this.string.slice(this.pos).match(a)) && 0 < a.index) return null;
                a && !1 !==
                    b && (this.pos += a[0].length);
                return a
            }
        },
        current: function() {
            return this.string.slice(this.start, this.pos)
        }
    };
    a.StringStream = Xb;
    a.TextMarker = Wa;
    Ab(Wa);
    Wa.prototype.clear = function() {
        if (!this.explicitlyCleared) {
            var a = this.doc.cm,
                b = a && !a.curOp;
            b && xa(a);
            if (Ma(this, "clear")) {
                var c = this.find();
                c && la(this, "clear", c.from, c.to)
            }
            for (var d = c = null, e = 0; e < this.lines.length; ++e) {
                var f = this.lines[e],
                    g = $b(f.markedSpans, this);
                null != g.to && (d = va(f));
                for (var h = f, k = f.markedSpans, n = g, m = void 0, p = 0; p < k.length; ++p) k[p] != n && (m ||
                    (m = [])).push(k[p]);
                h.markedSpans = m;
                null != g.from ? c = va(f) : this.collapsed && !Xa(this.doc, f) && a && Ba(f, Aa(a.display))
            }
            if (a && this.collapsed && !a.options.lineWrapping)
                for (e = 0; e < this.lines.length; ++e) f = Oa(a.doc, this.lines[e]), g = l(a.doc, f), g > a.display.maxLineLength && (a.display.maxLine = f, a.display.maxLineLength = g, a.display.maxLineChanged = !0);
            null != c && a && aa(a, c, d + 1);
            this.lines.length = 0;
            this.explicitlyCleared = !0;
            this.atomic && this.doc.cantEdit && (this.doc.cantEdit = !1, a && Nd(a));
            b && Ha(a)
        }
    };
    Wa.prototype.find = function() {
        for (var a,
                b, c = 0; c < this.lines.length; ++c) {
            var d = this.lines[c],
                e = $b(d.markedSpans, this);
            if (null != e.from || null != e.to) d = va(d), null != e.from && (a = w(d, e.from)), null != e.to && (b = w(d, e.to))
        }
        return "bookmark" == this.type ? a : a && {
            from: a,
            to: b
        }
    };
    Wa.prototype.changed = function() {
        var a = this.find(),
            b = this.doc.cm;
        if (a && b) {
            "bookmark" != this.type && (a = a.from);
            var c = G(this.doc, a.line);
            Jb(b, c);
            if (a.line >= b.display.showingFrom && a.line < b.display.showingTo) {
                for (a = b.display.lineDiv.firstChild; a; a = a.nextSibling)
                    if (a.lineObj == c) {
                        a.offsetHeight !=
                            c.height && Ba(c, a.offsetHeight);
                        break
                    }
                Ob(b, function() {
                    b.curOp.selectionChanged = b.curOp.forceUpdate = b.curOp.updateMaxLine = !0
                })
            }
        }
    };
    Wa.prototype.attachLine = function(a) {
        if (!this.lines.length && this.doc.cm) {
            var b = this.doc.cm.curOp;
            b.maybeHiddenMarkers && -1 != Ea(b.maybeHiddenMarkers, this) || (b.maybeUnhiddenMarkers || (b.maybeUnhiddenMarkers = [])).push(this)
        }
        this.lines.push(a)
    };
    Wa.prototype.detachLine = function(a) {
        this.lines.splice(Ea(this.lines, a), 1);
        !this.lines.length && this.doc.cm && (a = this.doc.cm.curOp, (a.maybeHiddenMarkers ||
            (a.maybeHiddenMarkers = [])).push(this))
    };
    a.SharedTextMarker = Zb;
    Ab(Zb);
    Zb.prototype.clear = function() {
        if (!this.explicitlyCleared) {
            this.explicitlyCleared = !0;
            for (var a = 0; a < this.markers.length; ++a) this.markers[a].clear();
            la(this, "clear")
        }
    };
    Zb.prototype.find = function() {
        return this.primary.find()
    };
    var zc = a.LineWidget = function(a, b, c) {
        if (c)
            for (var d in c) c.hasOwnProperty(d) && (this[d] = c[d]);
        this.cm = a;
        this.node = b
    };
    Ab(zc);
    zc.prototype.clear = Ud(function() {
        var a = this.line.widgets,
            b = va(this.line);
        if (null != b && a) {
            for (var c =
                    0; c < a.length; ++c) a[c] == this && a.splice(c--, 1);
            a.length || (this.line.widgets = null);
            a = Ib(this.cm, this.line) < this.cm.doc.scrollTop;
            Ba(this.line, Math.max(0, this.line.height - jc(this)));
            a && hd(this.cm, 0, -this.height);
            aa(this.cm, b, b + 1)
        }
    });
    zc.prototype.changed = Ud(function() {
        var a = this.height;
        this.height = null;
        if (a = jc(this) - a) Ba(this.line, this.line.height + a), a = va(this.line), aa(this.cm, a, a + 1)
    });
    var Sa = a.Line = function(a, b, c) {
        this.text = a;
        Td(this, b);
        this.height = c ? c(this) : 1
    };
    Ab(Sa);
    Sa.prototype.lineNo = function() {
        return va(this)
    };
    var Yd = {};
    Ac.prototype = {
        chunkSize: function() {
            return this.lines.length
        },
        removeInner: function(a, b) {
            for (var c = a, d = a + b; c < d; ++c) {
                var e = this.lines[c];
                this.height -= e.height;
                var f = e;
                f.parent = null;
                Sd(f);
                la(e, "delete")
            }
            this.lines.splice(a, b)
        },
        collapse: function(a) {
            a.splice.apply(a, [a.length, 0].concat(this.lines))
        },
        insertInner: function(a, b, c) {
            this.height += c;
            this.lines = this.lines.slice(0, a).concat(b).concat(this.lines.slice(a));
            a = 0;
            for (c = b.length; a < c; ++a) b[a].parent = this
        },
        iterN: function(a, b, c) {
            for (b = a + b; a < b; ++a)
                if (c(this.lines[a])) return !0
        }
    };
    bc.prototype = {
        chunkSize: function() {
            return this.size
        },
        removeInner: function(a, b) {
            this.size -= b;
            for (var c = 0; c < this.children.length; ++c) {
                var d = this.children[c],
                    e = d.chunkSize();
                if (a < e) {
                    var f = Math.min(b, e - a),
                        g = d.height;
                    d.removeInner(a, f);
                    this.height -= g - d.height;
                    e == f && (this.children.splice(c--, 1), d.parent = null);
                    if (0 == (b -= f)) break;
                    a = 0
                } else a -= e
            }
            25 > this.size - b && (c = [], this.collapse(c), this.children = [new Ac(c)], this.children[0].parent = this)
        },
        collapse: function(a) {
            for (var b = 0, c = this.children.length; b < c; ++b) this.children[b].collapse(a)
        },
        insertInner: function(a, b, c) {
            this.size += b.length;
            this.height += c;
            for (var d = 0, e = this.children.length; d < e; ++d) {
                var f = this.children[d],
                    g = f.chunkSize();
                if (a <= g) {
                    f.insertInner(a, b, c);
                    if (f.lines && 50 < f.lines.length) {
                        for (; 50 < f.lines.length;) a = f.lines.splice(f.lines.length - 25, 25), a = new Ac(a), f.height -= a.height, this.children.splice(d + 1, 0, a), a.parent = this;
                        this.maybeSpill()
                    }
                    break
                }
                a -= g
            }
        },
        maybeSpill: function() {
            if (!(10 >= this.children.length)) {
                var a = this;
                do {
                    var b = a.children.splice(a.children.length - 5, 5),
                        b = new bc(b);
                    if (a.parent) {
                        a.size -= b.size;
                        a.height -= b.height;
                        var c = Ea(a.parent.children, a);
                        a.parent.children.splice(c + 1, 0, b)
                    } else c = new bc(a.children), c.parent = a, a.children = [c, b], a = c;
                    b.parent = a.parent
                } while (10 < a.children.length);
                a.parent.maybeSpill()
            }
        },
        iterN: function(a, b, c) {
            for (var d = 0, e = this.children.length; d < e; ++d) {
                var f = this.children[d],
                    g = f.chunkSize();
                if (a < g) {
                    g = Math.min(b, g - a);
                    if (f.iterN(a, g, c)) return !0;
                    if (0 == (b -= g)) break;
                    a = 0
                } else a -= g
            }
        }
    };
    var Fe = 0,
        qa = a.Doc = function(a, b, c) {
            if (!(this instanceof qa)) return new qa(a,
                b, c);
            null == c && (c = 0);
            bc.call(this, [new Ac([new Sa("", null)])]);
            this.first = c;
            this.scrollTop = this.scrollLeft = 0;
            this.cantEdit = !1;
            this.history = Bc();
            this.cleanGeneration = 1;
            this.frontier = c;
            c = w(c, 0);
            this.sel = {
                from: c,
                to: c,
                head: c,
                anchor: c,
                shift: !1,
                extend: !1,
                goalColumn: null
            };
            this.id = ++Fe;
            this.modeOption = b;
            "string" == typeof a && (a = bb(a));
            gd(this, {
                from: c,
                to: c,
                text: a
            }, null, {
                head: c,
                anchor: c
            })
        };
    qa.prototype = de(bc.prototype, {
        constructor: qa,
        iter: function(a, b, c) {
            c ? this.iterN(a - this.first, b - a, c) : this.iterN(this.first,
                this.first + this.size, a)
        },
        insert: function(a, b) {
            for (var c = 0, d = 0, e = b.length; d < e; ++d) c += b[d].height;
            this.insertInner(a - this.first, b, c)
        },
        remove: function(a, b) {
            this.removeInner(a - this.first, b)
        },
        getValue: function(a) {
            var b = ld(this, this.first, this.first + this.size);
            return !1 === a ? b : b.join(a || "\n")
        },
        setValue: function(a) {
            var b = w(this.first, 0),
                c = this.first + this.size - 1;
            vb(this, {
                from: b,
                to: w(c, G(this, c).text.length),
                text: bb(a),
                origin: "setValue"
            }, {
                head: b,
                anchor: b
            }, !0)
        },
        replaceRange: function(a, b, c, d) {
            b = J(this, b);
            c = c ?
                J(this, c) : b;
            Va(this, a, b, c, d)
        },
        getRange: function(a, b, c) {
            a = fd(this, J(this, a), J(this, b));
            return !1 === c ? a : a.join(c || "\n")
        },
        getLine: function(a) {
            return (a = this.getLineHandle(a)) && a.text
        },
        setLine: function(a, b) {
            zb(this, a) && Va(this, b, w(a, 0), J(this, w(a)))
        },
        removeLine: function(a) {
            a ? Va(this, "", J(this, w(a - 1)), J(this, w(a))) : Va(this, "", w(0, 0), J(this, w(1, 0)))
        },
        getLineHandle: function(a) {
            if (zb(this, a)) return G(this, a)
        },
        getLineNumber: function(a) {
            return va(a)
        },
        getLineHandleVisualStart: function(a) {
            "number" == typeof a && (a =
                G(this, a));
            return Oa(this, a)
        },
        lineCount: function() {
            return this.size
        },
        firstLine: function() {
            return this.first
        },
        lastLine: function() {
            return this.first + this.size - 1
        },
        clipPos: function(a) {
            return J(this, a)
        },
        getCursor: function(a) {
            var b = this.sel;
            return db(null == a || "head" == a ? b.head : "anchor" == a ? b.anchor : "end" == a || !1 === a ? b.to : b.from)
        },
        somethingSelected: function() {
            return !V(this.sel.head, this.sel.anchor)
        },
        setCursor: $a(function(a, b, c) {
            a = J(this, "number" == typeof a ? w(a, b || 0) : a);
            c ? na(this, a) : Qa(this, a, a)
        }),
        setSelection: $a(function(a,
            b, c) {
            Qa(this, J(this, a), J(this, b || a), c)
        }),
        extendSelection: $a(function(a, b, c) {
            na(this, J(this, a), b && J(this, b), c)
        }),
        getSelection: function(a) {
            return this.getRange(this.sel.from, this.sel.to, a)
        },
        replaceSelection: function(a, b, c) {
            vb(this, {
                from: this.sel.from,
                to: this.sel.to,
                text: bb(a),
                origin: c
            }, b || "around")
        },
        undo: $a(function() {
            Kd(this, "undo")
        }),
        redo: $a(function() {
            Kd(this, "redo")
        }),
        setExtending: function(a) {
            this.sel.extend = a
        },
        historySize: function() {
            var a = this.history;
            return {
                undo: a.done.length,
                redo: a.undone.length
            }
        },
        clearHistory: function() {
            this.history = Bc(this.history.maxGeneration)
        },
        markClean: function() {
            this.cleanGeneration = this.changeGeneration()
        },
        changeGeneration: function() {
            this.history.lastOp = this.history.lastOrigin = null;
            return this.history.generation
        },
        isClean: function(a) {
            return this.history.generation == (a || this.cleanGeneration)
        },
        getHistory: function() {
            return {
                done: Cc(this.history.done),
                undone: Cc(this.history.undone)
            }
        },
        setHistory: function(a) {
            var b = this.history = Bc(this.history.maxGeneration);
            b.done = a.done.slice(0);
            b.undone = a.undone.slice(0)
        },
        markText: function(a, b, c) {
            return Yb(this, J(this, a), J(this, b), c, "range")
        },
        setBookmark: function(a, b) {
            var c = {
                replacedWith: b && (null == b.nodeType ? b.widget : b),
                insertLeft: b && b.insertLeft
            };
            a = J(this, a);
            return Yb(this, a, a, c, "bookmark")
        },
        findMarksAt: function(a) {
            a = J(this, a);
            var b = [],
                c = G(this, a.line).markedSpans;
            if (c)
                for (var d = 0; d < c.length; ++d) {
                    var e = c[d];
                    (null == e.from || e.from <= a.ch) && (null == e.to || e.to >= a.ch) && b.push(e.marker.parent || e.marker)
                }
            return b
        },
        getAllMarks: function() {
            var a = [];
            this.iter(function(b) {
                if (b = b.markedSpans)
                    for (var c = 0; c < b.length; ++c) null != b[c].from && a.push(b[c].marker)
            });
            return a
        },
        posFromIndex: function(a) {
            var b, c = this.first;
            this.iter(function(d) {
                d = d.text.length + 1;
                if (d > a) return b = a, !0;
                a -= d;
                ++c
            });
            return J(this, w(c, b))
        },
        indexFromPos: function(a) {
            a = J(this, a);
            var b = a.ch;
            if (a.line < this.first || 0 > a.ch) return 0;
            this.iter(this.first, a.line, function(a) {
                b += a.text.length + 1
            });
            return b
        },
        copy: function(a) {
            var b = new qa(ld(this, this.first, this.first + this.size), this.modeOption, this.first);
            b.scrollTop = this.scrollTop;
            b.scrollLeft = this.scrollLeft;
            b.sel = {
                from: this.sel.from,
                to: this.sel.to,
                head: this.sel.head,
                anchor: this.sel.anchor,
                shift: this.sel.shift,
                extend: !1,
                goalColumn: this.sel.goalColumn
            };
            a && (b.history.undoDepth = this.history.undoDepth, b.setHistory(this.getHistory()));
            return b
        },
        linkedDoc: function(a) {
            a || (a = {});
            var b = this.first,
                c = this.first + this.size;
            null != a.from && a.from > b && (b = a.from);
            null != a.to && a.to < c && (c = a.to);
            b = new qa(ld(this, b, c), a.mode || this.modeOption, b);
            a.sharedHist && (b.history =
                this.history);
            (this.linked || (this.linked = [])).push({
                doc: b,
                sharedHist: a.sharedHist
            });
            b.linked = [{
                doc: this,
                isParent: !0,
                sharedHist: a.sharedHist
            }];
            return b
        },
        unlinkDoc: function(b) {
            b instanceof a && (b = b.doc);
            if (this.linked)
                for (var c = 0; c < this.linked.length; ++c)
                    if (this.linked[c].doc == b) {
                        this.linked.splice(c, 1);
                        b.unlinkDoc(this);
                        break
                    }
            if (b.history == this.history) {
                var d = [b.id];
                yb(b, function(a) {
                    d.push(a.id)
                }, !0);
                b.history = Bc();
                b.history.done = Cc(this.history.done, d);
                b.history.undone = Cc(this.history.undone, d)
            }
        },
        iterLinkedDocs: function(a) {
            yb(this, a)
        },
        getMode: function() {
            return this.mode
        },
        getEditor: function() {
            return this.cm
        }
    });
    qa.prototype.eachLine = qa.prototype.iter;
    var Ge = ["iter", "insert", "remove", "copy", "getEditor"],
        fc;
    for (fc in qa.prototype) qa.prototype.hasOwnProperty(fc) && 0 > Ea(Ge, fc) && (a.prototype[fc] = function(a) {
        return function() {
            return a.apply(this.doc, arguments)
        }
    }(qa.prototype[fc]));
    Ab(qa);
    a.e_stop = Rb;
    a.e_preventDefault = fa;
    a.e_stopPropagation = ce;
    var Ta, mc = 0;
    a.on = Q;
    a.off = Pa;
    a.signal = ja;
    var jb = 30,
        Bd = a.Pass = {
            toString: function() {
                return "CodeMirror.Pass"
            }
        };
    Hc.prototype = {
        set: function(a, b) {
            clearTimeout(this.id);
            this.id = setTimeout(b, a)
        }
    };
    a.countColumn = pb;
    var Ec = [""],
        Ae = /[\u3040-\u309f\u30a0-\u30ff\u3400-\u4db5\u4e00-\u9fcc\uac00-\ud7af]/,
        Sc = /[\u0300-\u036F\u0483-\u0487\u0488-\u0489\u0591-\u05BD\u05BF\u05C1-\u05C2\u05C4-\u05C5\u05C7\u0610-\u061A\u064B-\u065F\u0670\u06D6-\u06DC\u06DF-\u06E4\u06E7-\u06E8\u06EA-\u06ED\uA66F\u1DC0\u2013\u1DFF\u20D0\u2013\u20FF\uA670-\uA672\uA674-\uA67D\uA69F\udc00-\udfff\uFE20\u2013\uFE2F]/;
    a.replaceGetRect = function(a) {
        $ = a
    };
    var oe = function() {
        if (wa) return !1;
        var a = C("div");
        return "draggable" in a || "dragDrop" in a
    }();
    xb ? ac = function(a, b) {
        return 36 == a.charCodeAt(b - 1) && 39 == a.charCodeAt(b)
    } : Vc && !/Version\/([6-9]|\d\d)\b/.test(navigator.userAgent) ? ac = function(a, b) {
        return /\-[^ \-?]|\?[^ !\'\"\),.\-\/:;\?\]\}]/.test(a.slice(b - 1, b + 1))
    } : ia && /Chrome\/(?:29|[3-9]\d|\d\d\d)\./.test(navigator.userAgent) ? ac = function(a, b) {
        var c = a.charCodeAt(b - 1);
        return 8208 <= c && 8212 >= c
    } : ia && (ac = function(a, b) {
        if (1 < b && 45 ==
            a.charCodeAt(b - 1)) {
            if (/\w/.test(a.charAt(b - 2)) && /[^\-?\.]/.test(a.charAt(b))) return !0;
            if (2 < b && /[\d\.,]/.test(a.charAt(b - 2)) && /[\d\.,]/.test(a.charAt(b))) return !1
        }
        return /[~!#%&*)=+}\]\\|\"\.>,:;][({[<]|-[^\-?\.\u2010-\u201f\u2026]|\?[\w~`@#$%\^&*(_=+{[|><]|\u2026[\w~`@#$%\^&*(_=+{[><]/.test(a.slice(b - 1, b + 1))
    });
    var Pb, md, bb = 3 != "\n\nb".split(/\n/).length ? function(a) {
        for (var b = 0, c = [], d = a.length; b <= d;) {
            var e = a.indexOf("\n", b); - 1 == e && (e = a.length);
            var f = a.slice(b, "\r" == a.charAt(e - 1) ? e - 1 : e),
                g = f.indexOf("\r"); - 1 != g ? (c.push(f.slice(0, g)), b += g + 1) : (c.push(f), b = e + 1)
        }
        return c
    } : function(a) {
        return a.split(/\r\n?|\n/)
    };
    a.splitLines = bb;
    var me = window.getSelection ? function(a) {
            try {
                return a.selectionStart != a.selectionEnd
            } catch (b) {
                return !1
            }
        } : function(a) {
            try {
                var b = a.ownerDocument.selection.createRange()
            } catch (c) {}
            return b && b.parentElement() == a ? 0 != b.compareEndPoints("StartToEnd", b) : !1
        },
        wd = function() {
            var a = C("div");
            if ("oncopy" in a) return !0;
            a.setAttribute("oncopy", "return;");
            return "function" == typeof a.oncopy
        }(),
        eb = {
            3: "Enter",
            8: "Backspace",
            9: "Tab",
            13: "Enter",
            16: "Shift",
            17: "Ctrl",
            18: "Alt",
            19: "Pause",
            20: "CapsLock",
            27: "Esc",
            32: "Space",
            33: "PageUp",
            34: "PageDown",
            35: "End",
            36: "Home",
            37: "Left",
            38: "Up",
            39: "Right",
            40: "Down",
            44: "PrintScrn",
            45: "Insert",
            46: "Delete",
            59: ";",
            91: "Mod",
            92: "Mod",
            93: "Mod",
            109: "-",
            107: "=",
            127: "Delete",
            186: ";",
            187: "=",
            188: ",",
            189: "-",
            190: ".",
            191: "/",
            192: "`",
            219: "[",
            220: "\\",
            221: "]",
            222: "'",
            63276: "PageUp",
            63277: "PageDown",
            63275: "End",
            63273: "Home",
            63234: "Left",
            63232: "Up",
            63235: "Right",
            63233: "Down",
            63302: "Insert",
            63272: "Delete"
        };
    a.keyNames = eb;
    (function() {
        for (var a = 0; 10 > a; a++) eb[a + 48] = String(a);
        for (a = 65; 90 >= a; a++) eb[a] = String.fromCharCode(a);
        for (a = 1; 12 >= a; a++) eb[a + 111] = eb[a + 63235] = "F" + a
    })();
    var sb, xe = function() {
        function a(d) {
            return 255 >= d ? b.charAt(d) : 1424 <= d && 1524 >= d ? "R" : 1536 <= d && 1791 >= d ? c.charAt(d - 1536) : 1792 <= d && 2220 >= d ? "r" : "L"
        }
        var b = "bbbbbbbbbtstwsbbbbbbbbbbbbbbssstwNN%%%NNNNNN,N,N1111111111NNNNNNNLLLLLLLLLLLLLLLLLLLLLLLLLLNNNNNNLLLLLLLLLLLLLLLLLLLLLLLLLLNNNNbbbbbbsbbbbbbbbbbbbbbbbbbbbbbbbbb,N%%%%NNNNLNNNNN%%11NLNNN1LNNNNNLLLLLLLLLLLLLLLLLLLLLLLNLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLNLLLLLLLL",
            c = "rrrrrrrrrrrr,rNNmmmmmmrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrmmmmmmmmmmmmmmrrrrrrrnnnnnnnnnn%nnrrrmrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrmmmmmmmmmmmmmmmmmmmNmmmmrrrrrrrrrrrrrrrrrr",
            d = /[\u0590-\u05f4\u0600-\u06ff\u0700-\u08ac]/,
            e = /[stwN]/,
            f = /[LRr]/,
            g = /[Lb1n]/,
            h = /[1n]/;
        return function(b) {
            if (!d.test(b)) return !1;
            for (var c = b.length, k = [], l = 0, n; l < c; ++l) k.push(a(b.charCodeAt(l)));
            for (var l = 0, m = "L"; l < c; ++l) n = k[l], "m" == n ? k[l] = m :
                m = n;
            l = 0;
            for (m = "L"; l < c; ++l) n = k[l], "1" == n && "r" == m ? k[l] = "n" : f.test(n) && (m = n, "r" == n && (k[l] = "R"));
            l = 1;
            for (m = k[0]; l < c - 1; ++l) n = k[l], "+" == n && "1" == m && "1" == k[l + 1] ? k[l] = "1" : "," != n || m != k[l + 1] || "1" != m && "n" != m || (k[l] = m), m = n;
            for (l = 0; l < c; ++l)
                if (n = k[l], "," == n) k[l] = "N";
                else if ("%" == n) {
                for (m = l + 1; m < c && "%" == k[m]; ++m);
                var p = l && "!" == k[l - 1] || m < c - 1 && "1" == k[m] ? "1" : "N";
                for (n = l; n < m; ++n) k[n] = p;
                l = m - 1
            }
            l = 0;
            for (m = "L"; l < c; ++l) n = k[l], "L" == m && "1" == n ? k[l] = "L" : f.test(n) && (m = n);
            for (l = 0; l < c; ++l)
                if (e.test(k[l])) {
                    for (m = l + 1; m < c && e.test(k[m]); ++m);
                    n = "L" == (m < c - 1 ? k[m] : "L");
                    p = "L" == (l ? k[l - 1] : "L") || n ? "L" : "R";
                    for (n = l; n < m; ++n) k[n] = p;
                    l = m - 1
                }
            for (var m = [], z, l = 0; l < c;)
                if (g.test(k[l])) {
                    n = l;
                    for (++l; l < c && g.test(k[l]); ++l);
                    m.push({
                        from: n,
                        to: l,
                        level: 0
                    })
                } else {
                    var r = l,
                        p = m.length;
                    for (++l; l < c && "L" != k[l]; ++l);
                    for (n = r; n < l;)
                        if (h.test(k[n])) {
                            r < n && m.splice(p, 0, {
                                from: r,
                                to: n,
                                level: 1
                            });
                            r = n;
                            for (++n; n < l && h.test(k[n]); ++n);
                            m.splice(p, 0, {
                                from: r,
                                to: n,
                                level: 2
                            });
                            r = n
                        } else ++n;
                    r < l && m.splice(p, 0, {
                        from: r,
                        to: l,
                        level: 1
                    })
                }
            1 == m[0].level && (z = b.match(/^\s+/)) && (m[0].from = z[0].length, m.unshift({
                from: 0,
                to: z[0].length,
                level: 0
            }));
            1 == ra(m).level && (z = b.match(/\s+$/)) && (ra(m).to -= z[0].length, m.push({
                from: c - z[0].length,
                to: c,
                level: 0
            }));
            m[0].level != ra(m).level && m.push({
                from: c,
                to: c,
                level: m[0].level
            });
            return m
        }
    }();
    a.version = "3.20.0";
    return a
}();
CodeMirror.defineMode("xml", function(a, b) {
    function c(a, b) {
        function c(d) {
            b.tokenize = d;
            return d(a, b)
        }
        var e = a.next();
        if ("<" == e) {
            if (a.eat("!")) return a.eat("[") ? a.match("CDATA[") ? c(f("atom", "]]\x3e")) : null : a.match("--") ? c(f("comment", "--\x3e")) : a.match("DOCTYPE", !0, !0) ? (a.eatWhile(/[\w\._\-]/), c(g(1))) : null;
            if (a.eat("?")) return a.eatWhile(/[\w\._\-]/), b.tokenize = f("meta", "?>"), "meta";
            e = a.eat("/");
            R = "";
            for (var h; h = a.eat(/[^\s\u00a0=<>\"\'\/?]/);) R += h;
            if (!R) return "tag error";
            S = e ? "closeTag" : "openTag";
            b.tokenize =
                d;
            return "tag"
        }
        if ("&" == e) return (a.eat("#") ? a.eat("x") ? a.eatWhile(/[a-fA-F\d]/) && a.eat(";") : a.eatWhile(/[\d]/) && a.eat(";") : a.eatWhile(/[\w\.\-:]/) && a.eat(";")) ? "atom" : "error";
        a.eatWhile(/[^&<]/);
        return null
    }

    function d(a, b) {
        var d = a.next();
        if (">" == d || "/" == d && a.eat(">")) return b.tokenize = c, S = ">" == d ? "endTag" : "selfcloseTag", "tag";
        if ("=" == d) return S = "equals", null;
        if ("<" == d) return b.tokenize = c, (d = b.tokenize(a, b)) ? d + " error" : "error";
        if (/[\'\"]/.test(d)) return b.tokenize = e(d), b.stringStartCol = a.column(), b.tokenize(a,
            b);
        a.eatWhile(/[^\s\u00a0=<>\"\']/);
        return "word"
    }

    function e(a) {
        var b = function(b, c) {
            for (; !b.eol();)
                if (b.next() == a) {
                    c.tokenize = d;
                    break
                }
            return "string"
        };
        b.isInAttribute = !0;
        return b
    }

    function f(a, b) {
        return function(d, e) {
            for (; !d.eol();) {
                if (d.match(b)) {
                    e.tokenize = c;
                    break
                }
                d.next()
            }
            return a
        }
    }

    function g(a) {
        return function(b, d) {
            for (var e; null != (e = b.next());) {
                if ("<" == e) return d.tokenize = g(a + 1), d.tokenize(b, d);
                if (">" == e)
                    if (1 == a) {
                        d.tokenize = c;
                        break
                    } else return d.tokenize = g(a - 1), d.tokenize(b, d)
            }
            return "meta"
        }
    }

    function h() {
        for (var a =
                arguments.length - 1; 0 <= a; a--) A.cc.push(arguments[a])
    }

    function k() {
        h.apply(null, arguments);
        return !0
    }

    function l() {
        A.context && (A.context = A.context.prev)
    }

    function q(a) {
        return "openTag" == a ? (A.tagName = R, A.tagStart = W.column(), k(L, m(A.startOfLine))) : "closeTag" == a ? (a = !1, A.context ? A.context.tagName != R && (O.implicitlyClosed.hasOwnProperty(A.context.tagName.toLowerCase()) && l(), a = !A.context || A.context.tagName != R) : a = !0, a && (u = "error"), k(p(a))) : k()
    }

    function m(a) {
        return function(b) {
            var c = A.tagName;
            A.tagName = A.tagStart =
                null;
            if ("selfcloseTag" == b || "endTag" == b && O.autoSelfClosers.hasOwnProperty(c.toLowerCase())) return E(c.toLowerCase()), k();
            "endTag" == b && (E(c.toLowerCase()), b = O.doNotIndent.hasOwnProperty(c) || A.context && A.context.noIndent, A.context = {
                prev: A.context,
                tagName: c,
                indent: A.indented,
                startOfLine: a,
                noIndent: b
            });
            return k()
        }
    }

    function p(a) {
        return function(b) {
            a && (u = "error");
            if ("endTag" == b) return l(), k();
            u = "error";
            return k(arguments.callee)
        }
    }

    function E(a) {
        for (var b; A.context;) {
            b = A.context.tagName.toLowerCase();
            if (!O.contextGrabbers.hasOwnProperty(b) ||
                !O.contextGrabbers[b].hasOwnProperty(a)) break;
            l()
        }
    }

    function L(a) {
        if ("word" == a) return u = "attribute", k(H, L);
        if ("endTag" == a || "selfcloseTag" == a) return h();
        u = "error";
        return k(L)
    }

    function H(a) {
        if ("equals" == a) return k(F, L);
        if (!O.allowMissing) u = "error";
        else if ("word" == a) return u = "attribute", k(H, L);
        return "endTag" == a || "selfcloseTag" == a ? h() : k()
    }

    function F(a) {
        if ("string" == a) return k(r);
        if ("word" == a && O.allowUnquoted) return u = "string", k();
        u = "error";
        return "endTag" == a || "selfCloseTag" == a ? h() : k()
    }

    function r(a) {
        return "string" ==
            a ? k(r) : h()
    }
    var B = a.indentUnit,
        v = b.multilineTagIndentFactor || 1,
        s = b.multilineTagIndentPastTag || !0,
        O = b.htmlMode ? {
            autoSelfClosers: {
                area: !0,
                base: !0,
                br: !0,
                col: !0,
                command: !0,
                embed: !0,
                frame: !0,
                hr: !0,
                img: !0,
                input: !0,
                keygen: !0,
                link: !0,
                meta: !0,
                param: !0,
                source: !0,
                track: !0,
                wbr: !0
            },
            implicitlyClosed: {
                dd: !0,
                li: !0,
                optgroup: !0,
                option: !0,
                p: !0,
                rp: !0,
                rt: !0,
                tbody: !0,
                td: !0,
                tfoot: !0,
                th: !0,
                tr: !0
            },
            contextGrabbers: {
                dd: {
                    dd: !0,
                    dt: !0
                },
                dt: {
                    dd: !0,
                    dt: !0
                },
                li: {
                    li: !0
                },
                option: {
                    option: !0,
                    optgroup: !0
                },
                optgroup: {
                    optgroup: !0
                },
                p: {
                    address: !0,
                    article: !0,
                    aside: !0,
                    blockquote: !0,
                    dir: !0,
                    div: !0,
                    dl: !0,
                    fieldset: !0,
                    footer: !0,
                    form: !0,
                    h1: !0,
                    h2: !0,
                    h3: !0,
                    h4: !0,
                    h5: !0,
                    h6: !0,
                    header: !0,
                    hgroup: !0,
                    hr: !0,
                    menu: !0,
                    nav: !0,
                    ol: !0,
                    p: !0,
                    pre: !0,
                    section: !0,
                    table: !0,
                    ul: !0
                },
                rp: {
                    rp: !0,
                    rt: !0
                },
                rt: {
                    rp: !0,
                    rt: !0
                },
                tbody: {
                    tbody: !0,
                    tfoot: !0
                },
                td: {
                    td: !0,
                    th: !0
                },
                tfoot: {
                    tbody: !0
                },
                th: {
                    td: !0,
                    th: !0
                },
                thead: {
                    tbody: !0,
                    tfoot: !0
                },
                tr: {
                    tr: !0
                }
            },
            doNotIndent: {
                pre: !0
            },
            allowUnquoted: !0,
            allowMissing: !0
        } : {
            autoSelfClosers: {},
            implicitlyClosed: {},
            contextGrabbers: {},
            doNotIndent: {},
            allowUnquoted: !1,
            allowMissing: !1
        },
        M = b.alignCDATA,
        R, S, A, W, u;
    return {
        startState: function() {
            return {
                tokenize: c,
                cc: [],
                indented: 0,
                startOfLine: !0,
                tagName: null,
                tagStart: null,
                context: null
            }
        },
        token: function(a, b) {
            !b.tagName && a.sol() && (b.startOfLine = !0, b.indented = a.indentation());
            if (a.eatSpace()) return null;
            u = S = R = null;
            var c = b.tokenize(a, b);
            b.type = S;
            if ((c || S) && "comment" != c)
                for (A = b, W = a; !(b.cc.pop() || q)(S || c););
            b.startOfLine = !1;
            u && (c = "error" == u ? c + " error" : u);
            return c
        },
        indent: function(a, b, e) {
            var f = a.context;
            if (a.tokenize.isInAttribute) return a.stringStartCol +
                1;
            if (a.tokenize != d && a.tokenize != c || f && f.noIndent) return e ? e.match(/^(\s*)/)[0].length : 0;
            if (a.tagName) return s ? a.tagStart + a.tagName.length + 2 : a.tagStart + B * v;
            if (M && /<!\[CDATA\[/.test(b)) return 0;
            f && /^<\//.test(b) && (f = f.prev);
            for (; f && !f.startOfLine;) f = f.prev;
            return f ? f.indent + B : 0
        },
        electricChars: "/",
        blockCommentStart: "\x3c!--",
        blockCommentEnd: "--\x3e",
        configuration: b.htmlMode ? "html" : "xml",
        helperType: b.htmlMode ? "html" : "xml"
    }
});
CodeMirror.defineMIME("text/xml", "xml");
CodeMirror.defineMIME("application/xml", "xml");
CodeMirror.mimeModes.hasOwnProperty("text/html") || CodeMirror.defineMIME("text/html", {
    name: "xml",
    htmlMode: !0
});
CodeMirror.defineMode("javascript", function(a, b) {
    function c(a, b) {
        for (var c = !1, d; null != (d = a.next());) {
            if (d == b && !c) return !1;
            c = !c && "\\" == d
        }
        return c
    }

    function d(a, b, c) {
        cb = a;
        La = c;
        return b
    }

    function e(a, b) {
        var e = a.next();
        if ('"' == e || "'" == e) return b.tokenize = f(e), b.tokenize(a, b);
        if ("." == e && a.match(/^\d+(?:[eE][+\-]?\d+)?/)) return d("number", "number");
        if ("." == e && a.match("..")) return d("spread", "meta");
        if (/[\[\]{}\(\),;\:\.]/.test(e)) return d(e);
        if ("=" == e && a.eat(">")) return d("=>");
        if ("0" == e && a.eat(/x/i)) return a.eatWhile(/[\da-f]/i),
            d("number", "number");
        if (/\d/.test(e)) return a.match(/^\d*(?:\.\d*)?(?:[eE][+\-]?\d+)?/), d("number", "number");
        if ("/" == e) {
            if (a.eat("*")) return b.tokenize = g, g(a, b);
            if (a.eat("/")) return a.skipToEnd(), d("comment", "comment");
            if ("operator" == b.lastType || "keyword c" == b.lastType || "sof" == b.lastType || /^[\[{}\(,;:]$/.test(b.lastType)) return c(a, "/"), a.eatWhile(/[gimy]/), d("regexp", "string-2");
            a.eatWhile(Tb);
            return d("operator", null, a.current())
        }
        if ("`" == e) return b.tokenize = h, h(a, b);
        if ("#" == e) return a.skipToEnd(),
            d("error", "error");
        if (Tb.test(e)) return a.eatWhile(Tb), d("operator", null, a.current());
        a.eatWhile(/[\w\$_]/);
        var e = a.current(),
            k = wb.propertyIsEnumerable(e) && wb[e];
        return k && "." != b.lastType ? d(k.type, k.style, e) : d("variable", "variable", e)
    }

    function f(a) {
        return function(b, f) {
            c(b, a) || (f.tokenize = e);
            return d("string", "string")
        }
    }

    function g(a, b) {
        for (var c = !1, f; f = a.next();) {
            if ("/" == f && c) {
                b.tokenize = e;
                break
            }
            c = "*" == f
        }
        return d("comment", "comment")
    }

    function h(a, b) {
        for (var c = !1, f; null != (f = a.next());) {
            if (!c && ("`" ==
                    f || "$" == f && a.eat("{"))) {
                b.tokenize = e;
                break
            }
            c = !c && "\\" == f
        }
        return d("quasi", "string-2", a.current())
    }

    function k(a, b) {
        b.fatArrowAt && (b.fatArrowAt = null);
        var c = a.string.indexOf("=>", a.start);
        if (!(0 > c)) {
            for (var d = 0, e = !1, c = c - 1; 0 <= c; --c) {
                var f = a.string.charAt(c),
                    g = Ua.indexOf(f);
                if (0 <= g && 3 > g) {
                    if (!d) {
                        ++c;
                        break
                    }
                    if (0 == --d) break
                } else if (3 <= g && 6 > g) ++d;
                else if (/[$\w]/.test(f)) e = !0;
                else if (e && !d) {
                    ++c;
                    break
                }
            }
            e && !d && (b.fatArrowAt = c)
        }
    }

    function l(a, b, c, d, e, f) {
        this.indented = a;
        this.column = b;
        this.type = c;
        this.prev = e;
        this.info =
            f;
        null != d && (this.align = d)
    }

    function q() {
        for (var a = arguments.length - 1; 0 <= a; a--) I.cc.push(arguments[a])
    }

    function m() {
        q.apply(null, arguments);
        return !0
    }

    function p(a) {
        function c(b) {
            for (; b; b = b.next)
                if (b.name == a) return !0;
            return !1
        }
        var d = I.state;
        d.context ? (I.marked = "def", c(d.localVars) || (d.localVars = {
            name: a,
            next: d.localVars
        })) : !c(d.globalVars) && b.globalVars && (d.globalVars = {
            name: a,
            next: d.globalVars
        })
    }

    function E() {
        I.state.context = {
            prev: I.state.context,
            vars: I.state.localVars
        };
        I.state.localVars = sc
    }

    function L() {
        I.state.localVars =
            I.state.context.vars;
        I.state.context = I.state.context.prev
    }

    function H(a, b) {
        var c = function() {
            var c = I.state,
                d = c.indented;
            "stat" == c.lexical.type && (d = c.lexical.indented);
            c.lexical = new l(d, I.stream.column(), a, null, c.lexical, b)
        };
        c.lex = !0;
        return c
    }

    function F() {
        var a = I.state;
        a.lexical.prev && (")" == a.lexical.type && (a.indented = a.lexical.indented), a.lexical = a.lexical.prev)
    }

    function r(a) {
        return function(b) {
            return b == a ? m() : ";" == a ? q() : m(arguments.callee)
        }
    }

    function B(a, b) {
        return "var" == a ? m(H("vardef", b.length), Kb,
            r(";"), F) : "keyword a" == a ? m(H("form"), v, B, F) : "keyword b" == a ? m(H("form"), B, F) : "{" == a ? m(H("}"), Jb, F) : ";" == a ? m() : "if" == a ? m(H("form"), v, B, F, ma) : "function" == a ? m(xa) : "for" == a ? m(H("form"), Ya, F, B, F) : "variable" == a ? m(H("stat"), U) : "switch" == a ? m(H("form"), v, H("}", "switch"), r("{"), Jb, F, F) : "case" == a ? m(v, r(":")) : "default" == a ? m(r(":")) : "catch" == a ? m(H("form"), E, r("("), Ha, r(")"), B, F, L) : "module" == a ? m(H("form"), E, aa, L, F) : "class" == a ? m(H("form"), N, Ob, F) : "export" == a ? m(H("form"), tb, F) : "import" == a ? m(H("form"), ab, F) : q(H("stat"),
            v, r(";"), F)
    }

    function v(a) {
        return O(a, !1)
    }

    function s(a) {
        return O(a, !0)
    }

    function O(a, b) {
        if (I.state.fatArrowAt == I.stream.start) {
            var c = b ? D : u;
            if ("(" == a) return m(E, Y(ua, ")"), r("=>"), c, L);
            if ("variable" == a) return q(E, ua, r("=>"), c, L)
        }
        c = b ? A : S;
        return Sb.hasOwnProperty(a) ? m(c) : "function" == a ? m(xa) : "keyword c" == a ? m(b ? R : M) : "(" == a ? m(H(")"), M, Ia, r(")"), F, c) : "operator" == a || "spread" == a ? m(b ? s : v) : "[" == a ? m(H("]"), s, pa, F, c) : "{" == a ? m(Y(oa, "}"), c) : m()
    }

    function M(a) {
        return a.match(/[;\}\)\],]/) ? q() : q(v)
    }

    function R(a) {
        return a.match(/[;\}\)\],]/) ?
            q() : q(s)
    }

    function S(a, b) {
        return "," == a ? m(v) : A(a, b, !1)
    }

    function A(a, b, c) {
        var d = !1 == c ? S : A,
            e = !1 == c ? v : s;
        if ("=>" == b) return m(E, c ? D : u, L);
        if ("operator" == a) return /\+\+|--/.test(b) ? m(d) : "?" == b ? m(v, r(":"), e) : m(e);
        if ("quasi" == a) {
            I.cc.push(d);
            if (!b) debugger;
            a = "${" != b.slice(b.length - 2) ? m() : m(v, W);
            return a
        }
        if (";" != a) {
            if ("(" == a) return m(Y(s, ")", "call"), d);
            if ("." == a) return m(da, d);
            if ("[" == a) return m(H("]"), M, r("]"), F, d)
        }
    }

    function W(a) {
        if ("}" == a) return I.marked = "string-2", I.state.tokenize = h, m()
    }

    function u(a) {
        k(I.stream,
            I.state);
        return "{" == a ? q(B) : q(v)
    }

    function D(a) {
        k(I.stream, I.state);
        return "{" == a ? q(B) : q(s)
    }

    function U(a) {
        return ":" == a ? m(F, B) : q(S, r(";"), F)
    }

    function da(a) {
        if ("variable" == a) return I.marked = "property", m()
    }

    function oa(a, b) {
        if ("variable" == a) {
            if (I.marked = "property", "get" == b || "set" == b) return m(sa)
        } else if ("number" == a || "string" == a) I.marked = a + " property";
        else if ("[" == a) return m(v, r("]"), ka);
        if (Sb.hasOwnProperty(a)) return m(ka)
    }

    function sa(a) {
        if ("variable" != a) return q(ka);
        I.marked = "property";
        return m(xa)
    }

    function ka(a) {
        if (":" ==
            a) return m(s);
        if ("(" == a) return q(xa)
    }

    function Y(a, b, c) {
        function d(c) {
            return "," == c ? (c = I.state.lexical, "call" == c.info && (c.pos = (c.pos || 0) + 1), m(a, d)) : c == b ? m() : m(r(b))
        }
        return function(e) {
            return e == b ? m() : !1 === c ? q(a, d) : q(H(b, c), a, d, F)
        }
    }

    function Jb(a) {
        return "}" == a ? m() : q(B, Jb)
    }

    function qb(a) {
        if (oc && ":" == a) return m(ic)
    }

    function ic(a) {
        if ("variable" == a) return I.marked = "variable-3", m()
    }

    function Kb() {
        return q(ua, qb, rb, nb)
    }

    function ua(a, b) {
        if ("variable" == a) return p(b), m();
        if ("[" == a) return m(Y(ua, "]"));
        if ("{" == a) return m(Y(Lb,
            "}"))
    }

    function Lb(a, b) {
        if ("variable" == a && !I.stream.match(/^\s*:/, !1)) return p(b), m(rb);
        "variable" == a && (I.marked = "property");
        return m(r(":"), ua, rb)
    }

    function rb(a, b) {
        if ("=" == b) return m(s)
    }

    function nb(a) {
        if ("," == a) return m(Kb)
    }

    function ma(a, b) {
        if ("keyword b" == a && "else" == b) return m(H("form"), B, F)
    }

    function Ya(a) {
        if ("(" == a) return m(H(")"), Mb, r(")"))
    }

    function Mb(a) {
        return "var" == a ? m(Kb, r(";"), Aa) : ";" == a ? m(Aa) : "variable" == a ? m(Qc) : q(v, r(";"), Aa)
    }

    function Qc(a, b) {
        return "in" == b || "of" == b ? (I.marked = "keyword", m(v)) :
            m(S, Aa)
    }

    function Aa(a, b) {
        return ";" == a ? m(Db) : "in" == b || "of" == b ? (I.marked = "keyword", m(v)) : q(v, r(";"), Db)
    }

    function Db(a) {
        ")" != a && m(v)
    }

    function xa(a, b) {
        if ("*" == b) return I.marked = "keyword", m(xa);
        if ("variable" == a) return p(b), m(xa);
        if ("(" == a) return m(E, Y(Ha, ")"), B, L)
    }

    function Ha(a) {
        return "spread" == a ? m(Ha) : q(ua, qb)
    }

    function N(a, b) {
        if ("variable" == a) return p(b), m($a)
    }

    function $a(a, b) {
        if ("extends" == b) return m(v)
    }

    function Ob(a) {
        if ("{" == a) return m(Y(oa, "}"))
    }

    function aa(a, b) {
        if ("string" == a) return m(B);
        if ("variable" ==
            a) return p(b), m(ta)
    }

    function tb(a, b) {
        return "*" == b ? (I.marked = "keyword", m(ta, r(";"))) : "default" == b ? (I.marked = "keyword", m(v, r(";"))) : q(B)
    }

    function ab(a) {
        return "string" == a ? m() : q(ub, ta)
    }

    function ub(a, b) {
        if ("{" == a) return m(Y(ub, "}"));
        "variable" == a && p(b);
        return m()
    }

    function ta(a, b) {
        if ("from" == b) return I.marked = "keyword", m(v)
    }

    function pa(a) {
        return "for" == a ? q(Ia) : "," == a ? m(Y(s, "]", !1)) : q(Y(s, "]", !1))
    }

    function Ia(a) {
        if ("for" == a) return m(Ya, Ia);
        if ("if" == a) return m(v, Ia)
    }
    var Ra = a.indentUnit,
        Ka = b.statementIndent,
        ya = b.json,
        oc = b.typescript,
        wb = function() {
            function a(b) {
                return {
                    type: b,
                    style: "keyword"
                }
            }
            var b = a("keyword a"),
                c = a("keyword b"),
                d = a("keyword c"),
                e = a("operator"),
                f = {
                    type: "atom",
                    style: "atom"
                },
                b = {
                    "if": a("if"),
                    "while": b,
                    "with": b,
                    "else": c,
                    "do": c,
                    "try": c,
                    "finally": c,
                    "return": d,
                    "break": d,
                    "continue": d,
                    "new": d,
                    "delete": d,
                    "throw": d,
                    "var": a("var"),
                    "const": a("var"),
                    let: a("var"),
                    "function": a("function"),
                    "catch": a("catch"),
                    "for": a("for"),
                    "switch": a("switch"),
                    "case": a("case"),
                    "default": a("default"),
                    "in": e,
                    "typeof": e,
                    "instanceof": e,
                    "true": f,
                    "false": f,
                    "null": f,
                    undefined: f,
                    NaN: f,
                    Infinity: f,
                    "this": a("this"),
                    module: a("module"),
                    "class": a("class"),
                    "super": a("atom"),
                    yield: d,
                    "export": a("export"),
                    "import": a("import"),
                    "extends": d
                };
            if (oc) {
                var c = {
                        type: "variable",
                        style: "variable-3"
                    },
                    c = {
                        "interface": a("interface"),
                        "extends": a("extends"),
                        constructor: a("constructor"),
                        "public": a("public"),
                        "private": a("private"),
                        "protected": a("protected"),
                        "static": a("static"),
                        string: c,
                        number: c,
                        bool: c,
                        any: c
                    },
                    g;
                for (g in c) b[g] = c[g]
            }
            return b
        }(),
        Tb =
        /[+\-*&%=<>!?|~^]/,
        cb, La, Ua = "([{}])",
        Sb = {
            atom: !0,
            number: !0,
            variable: !0,
            string: !0,
            regexp: !0,
            "this": !0
        },
        I = {
            state: null,
            column: null,
            marked: null,
            cc: null
        },
        sc = {
            name: "this",
            next: {
                name: "arguments"
            }
        };
    F.lex = !0;
    return {
        startState: function(a) {
            a = {
                tokenize: e,
                lastType: "sof",
                cc: [],
                lexical: new l((a || 0) - Ra, 0, "block", !1),
                localVars: b.localVars,
                context: b.localVars && {
                    vars: b.localVars
                },
                indented: 0
            };
            b.globalVars && (a.globalVars = b.globalVars);
            return a
        },
        token: function(a, b) {
            a.sol() && (b.lexical.hasOwnProperty("align") || (b.lexical.align = !1), b.indented = a.indentation(), k(a, b));
            if (b.tokenize != g && a.eatSpace()) return null;
            var c = b.tokenize(a, b);
            if ("comment" == cb) return c;
            b.lastType = "operator" != cb || "++" != La && "--" != La ? cb : "incdec";
            a: {
                var d = cb,
                    e = La,
                    f = b.cc;I.state = b;I.stream = a;I.marked = null;I.cc = f;b.lexical.hasOwnProperty("align") || (b.lexical.align = !0);
                for (;;)
                    if ((f.length ? f.pop() : ya ? v : B)(d, e)) {
                        for (; f.length && f[f.length - 1].lex;) f.pop()();
                        if (I.marked) {
                            c = I.marked;
                            break a
                        }
                        if (d = "variable" == d) b: {
                            for (d = b.localVars; d; d = d.next)
                                if (d.name == e) {
                                    d = !0;
                                    break b
                                }
                            for (f =
                                b.context; f; f = f.prev)
                                for (d = f.vars; d; d = d.next)
                                    if (d.name == e) {
                                        d = !0;
                                        break b
                                    }
                            d = void 0
                        }
                        if (d) {
                            c = "variable-2";
                            break a
                        }
                        break a
                    }
                c = void 0
            }
            return c
        },
        indent: function(a, c) {
            if (a.tokenize == g) return CodeMirror.Pass;
            if (a.tokenize != e) return 0;
            for (var d = c && c.charAt(0), f = a.lexical, h = a.cc.length - 1; 0 <= h; --h) {
                var k = a.cc[h];
                if (k == F) f = f.prev;
                else if (k != ma) break
            }
            "stat" == f.type && "}" == d && (f = f.prev);
            Ka && ")" == f.type && "stat" == f.prev.type && (f = f.prev);
            h = f.type;
            k = d == h;
            return "vardef" == h ? f.indented + ("operator" == a.lastType || "," == a.lastType ?
                f.info + 1 : 0) : "form" == h && "{" == d ? f.indented : "form" == h ? f.indented + Ra : "stat" == h ? f.indented + ("operator" == a.lastType || "," == a.lastType ? Ka || Ra : 0) : "switch" != f.info || k || !1 == b.doubleIndentSwitch ? f.align ? f.column + (k ? 0 : 1) : f.indented + (k ? 0 : Ra) : f.indented + (/^(?:case|default)\b/.test(c) ? Ra : 2 * Ra)
        },
        electricChars: ":{}",
        blockCommentStart: ya ? null : "/*",
        blockCommentEnd: ya ? null : "*/",
        lineComment: ya ? null : "//",
        fold: "brace",
        helperType: ya ? "json" : "javascript",
        jsonMode: ya
    }
});
CodeMirror.defineMIME("text/javascript", "javascript");
CodeMirror.defineMIME("text/ecmascript", "javascript");
CodeMirror.defineMIME("application/javascript", "javascript");
CodeMirror.defineMIME("application/ecmascript", "javascript");
CodeMirror.defineMIME("application/json", {
    name: "javascript",
    json: !0
});
CodeMirror.defineMIME("application/x-json", {
    name: "javascript",
    json: !0
});
CodeMirror.defineMIME("text/typescript", {
    name: "javascript",
    typescript: !0
});
CodeMirror.defineMIME("application/typescript", {
    name: "javascript",
    typescript: !0
});
CodeMirror.defineMode("css", function(a, b) {
    function c(a, b) {
        L = b;
        return a
    }

    function d(a, b) {
        var d = a.next();
        if (h[d]) {
            var g = h[d](a, b);
            if (!1 !== g) return g
        }
        if ("@" == d) return a.eatWhile(/[\w\\\-]/), c("def", a.current());
        if ("=" == d) c(null, "compare");
        else if ("~" != d && "|" != d || !a.eat("=")) {
            if ('"' == d || "'" == d) return b.tokenize = e(d), b.tokenize(a, b);
            if ("#" == d) return a.eatWhile(/[\w\\\-]/), c("atom", "hash");
            if ("!" == d) return a.match(/^\s*\w*/), c("keyword", "important");
            if (/\d/.test(d) || "." == d && a.eat(/\d/)) return a.eatWhile(/[\w.%]/),
                c("number", "unit");
            if ("-" === d) {
                if (/\d/.test(a.peek())) return a.eatWhile(/[\w.%]/), c("number", "unit");
                if (a.match(/^[^-]+-/)) return c("meta", "meta")
            } else {
                if (/[,+>*\/]/.test(d)) return c(null, "select-op");
                if ("." == d && a.match(/^-?[_a-z][_a-z0-9-]*/i)) return c("qualifier", "qualifier");
                if (":" == d) return c("operator", d);
                if (/[;{}\[\]\(\)]/.test(d)) return c(null, d);
                "u" == d && a.match("rl(") ? (a.backUp(1), b.tokenize = f) : a.eatWhile(/[\w\\\-]/);
                return c("property", "variable")
            }
        } else return c(null, "compare")
    }

    function e(a,
        b) {
        return function(e, f) {
            for (var g = !1, h; null != (h = e.next()) && (h != a || g);) g = !g && "\\" == h;
            g || (b && e.backUp(1), f.tokenize = d);
            return c("string", "string")
        }
    }

    function f(a, b) {
        a.next();
        a.match(/\s*[\"\']/, !1) ? b.tokenize = d : b.tokenize = e(")", !0);
        return c(null, "(")
    }
    b.propertyKeywords || (b = CodeMirror.resolveMode("text/css"));
    var g = a.indentUnit || a.tabSize || 2,
        h = b.hooks || {},
        k = b.atMediaTypes || {},
        l = b.atMediaFeatures || {},
        q = b.propertyKeywords || {},
        m = b.colorKeywords || {},
        p = b.valueKeywords || {},
        E = !!b.allowNested,
        L = null;
    return {
        startState: function(a) {
            return {
                tokenize: d,
                baseIndent: a || 0,
                stack: [],
                lastToken: null
            }
        },
        token: function(a, b) {
            b.tokenize = b.tokenize || d;
            if (b.tokenize == d && a.eatSpace()) return null;
            var e = b.tokenize(a, b);
            e && "string" != typeof e && (e = c(e[0], e[1]));
            var f = b.stack[b.stack.length - 1];
            if ("variable" == e) return "variable-definition" == L && b.stack.push("propertyValue"), b.lastToken = "variable-2";
            if ("property" == e) {
                var g = a.current().toLowerCase();
                "propertyValue" == f ? e = p.hasOwnProperty(g) ? "string-2" : m.hasOwnProperty(g) ? "keyword" : "variable-2" : "rule" == f ? q.hasOwnProperty(g) ||
                    (e += " error") : "block" == f ? e = q.hasOwnProperty(g) ? "property" : m.hasOwnProperty(g) ? "keyword" : p.hasOwnProperty(g) ? "string-2" : "tag" : f && "@media{" != f ? "@media" == f ? e = k[a.current()] ? "attribute" : /^(only|not)$/.test(g) ? "keyword" : "and" == g ? "error" : l.hasOwnProperty(g) ? "error" : "attribute error" : "@mediaType" == f ? k.hasOwnProperty(g) ? e = "attribute" : "and" == g ? e = "operator" : (/^(only|not)$/.test(g), e = "error") : "@mediaType(" == f ? q.hasOwnProperty(g) || (e = k.hasOwnProperty(g) ? "error" : "and" == g ? "operator" : /^(only|not)$/.test(g) ? "error" :
                        e + " error") : e = "@import" == f ? "tag" : "error" : e = "tag"
            } else "atom" == e ? f && "@media{" != f && "block" != f ? "propertyValue" == f ? /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/.test(a.current()) || (e += " error") : e = "error" : e = "builtin" : "@media" == f && "{" == L && (e = "error");
            if ("{" == L) "@media" == f || "@mediaType" == f ? b.stack[b.stack.length - 1] = "@media{" : b.stack.push(E ? "block" : "rule");
            else if ("}" == L)
                for ("interpolation" == f && (e = "operator"); b.stack.length && !(f = b.stack.pop(), -1 < f.indexOf("{") || "block" == f || "rule" == f););
            else if ("interpolation" == L) b.stack.push("interpolation");
            else if ("@media" == L) b.stack.push("@media");
            else if ("@import" == L) b.stack.push("@import");
            else if ("@media" == f && /\b(keyword|attribute)\b/.test(e)) b.stack[b.stack.length - 1] = "@mediaType";
            else if ("@mediaType" == f && "," == a.current()) b.stack[b.stack.length - 1] = "@media";
            else if ("(" == L) "@media" == f || "@mediaType" == f ? (b.stack[b.stack.length - 1] = "@mediaType", b.stack.push("@mediaType(")) : b.stack.push("(");
            else if (")" == L)
                for (; b.stack.length && !(f = b.stack.pop(), -1 < f.indexOf("(")););
            else ":" == L && "property" == b.lastToken ?
                b.stack.push("propertyValue") : "propertyValue" == f && ";" == L ? b.stack.pop() : "@import" == f && ";" == L && b.stack.pop();
            return b.lastToken = e
        },
        indent: function(a, b) {
            var c = a.stack.length;
            /^\}/.test(b) && (c -= "propertyValue" == a.stack[c - 1] ? 2 : 1);
            return a.baseIndent + c * g
        },
        electricChars: "}",
        blockCommentStart: "/*",
        blockCommentEnd: "*/",
        fold: "brace"
    }
});
(function() {
    function a(a) {
        for (var b = {}, c = 0; c < a.length; ++c) b[a[c]] = !0;
        return b
    }

    function b(a, b) {
        for (var c = !1, d; null != (d = a.next());) {
            if (c && "/" == d) {
                b.tokenize = null;
                break
            }
            c = "*" == d
        }
        return ["comment", "comment"]
    }
    var c = a("all aural braille handheld print projection screen tty tv embossed".split(" ")),
        d = a("width min-width max-width height min-height max-height device-width min-device-width max-device-width device-height min-device-height max-device-height aspect-ratio min-aspect-ratio max-aspect-ratio device-aspect-ratio min-device-aspect-ratio max-device-aspect-ratio color min-color max-color color-index min-color-index max-color-index monochrome min-monochrome max-monochrome resolution min-resolution max-resolution scan grid".split(" ")),
        e = a("align-content align-items align-self alignment-adjust alignment-baseline anchor-point animation animation-delay animation-direction animation-duration animation-iteration-count animation-name animation-play-state animation-timing-function appearance azimuth backface-visibility background background-attachment background-clip background-color background-image background-origin background-position background-repeat background-size baseline-shift binding bleed bookmark-label bookmark-level bookmark-state bookmark-target border border-bottom border-bottom-color border-bottom-left-radius border-bottom-right-radius border-bottom-style border-bottom-width border-collapse border-color border-image border-image-outset border-image-repeat border-image-slice border-image-source border-image-width border-left border-left-color border-left-style border-left-width border-radius border-right border-right-color border-right-style border-right-width border-spacing border-style border-top border-top-color border-top-left-radius border-top-right-radius border-top-style border-top-width border-width bottom box-decoration-break box-shadow box-sizing break-after break-before break-inside caption-side clear clip color color-profile column-count column-fill column-gap column-rule column-rule-color column-rule-style column-rule-width column-span column-width columns content counter-increment counter-reset crop cue cue-after cue-before cursor direction display dominant-baseline drop-initial-after-adjust drop-initial-after-align drop-initial-before-adjust drop-initial-before-align drop-initial-size drop-initial-value elevation empty-cells fit fit-position flex flex-basis flex-direction flex-flow flex-grow flex-shrink flex-wrap float float-offset flow-from flow-into font font-feature-settings font-family font-kerning font-language-override font-size font-size-adjust font-stretch font-style font-synthesis font-variant font-variant-alternates font-variant-caps font-variant-east-asian font-variant-ligatures font-variant-numeric font-variant-position font-weight grid-cell grid-column grid-column-align grid-column-sizing grid-column-span grid-columns grid-flow grid-row grid-row-align grid-row-sizing grid-row-span grid-rows grid-template hanging-punctuation height hyphens icon image-orientation image-rendering image-resolution inline-box-align justify-content left letter-spacing line-break line-height line-stacking line-stacking-ruby line-stacking-shift line-stacking-strategy list-style list-style-image list-style-position list-style-type margin margin-bottom margin-left margin-right margin-top marker-offset marks marquee-direction marquee-loop marquee-play-count marquee-speed marquee-style max-height max-width min-height min-width move-to nav-down nav-index nav-left nav-right nav-up opacity order orphans outline outline-color outline-offset outline-style outline-width overflow overflow-style overflow-wrap overflow-x overflow-y padding padding-bottom padding-left padding-right padding-top page page-break-after page-break-before page-break-inside page-policy pause pause-after pause-before perspective perspective-origin pitch pitch-range play-during position presentation-level punctuation-trim quotes region-break-after region-break-before region-break-inside region-fragment rendering-intent resize rest rest-after rest-before richness right rotation rotation-point ruby-align ruby-overhang ruby-position ruby-span shape-inside shape-outside size speak speak-as speak-header speak-numeral speak-punctuation speech-rate stress string-set tab-size table-layout target target-name target-new target-position text-align text-align-last text-decoration text-decoration-color text-decoration-line text-decoration-skip text-decoration-style text-emphasis text-emphasis-color text-emphasis-position text-emphasis-style text-height text-indent text-justify text-outline text-overflow text-shadow text-size-adjust text-space-collapse text-transform text-underline-position text-wrap top transform transform-origin transform-style transition transition-delay transition-duration transition-property transition-timing-function unicode-bidi vertical-align visibility voice-balance voice-duration voice-family voice-pitch voice-range voice-rate voice-stress voice-volume volume white-space widows width word-break word-spacing word-wrap z-index zoom clip-path clip-rule mask enable-background filter flood-color flood-opacity lighting-color stop-color stop-opacity pointer-events color-interpolation color-interpolation-filters color-profile color-rendering fill fill-opacity fill-rule image-rendering marker marker-end marker-mid marker-start shape-rendering stroke stroke-dasharray stroke-dashoffset stroke-linecap stroke-linejoin stroke-miterlimit stroke-opacity stroke-width text-rendering baseline-shift dominant-baseline glyph-orientation-horizontal glyph-orientation-vertical kerning text-anchor writing-mode".split(" ")),
        f = a("aliceblue antiquewhite aqua aquamarine azure beige bisque black blanchedalmond blue blueviolet brown burlywood cadetblue chartreuse chocolate coral cornflowerblue cornsilk crimson cyan darkblue darkcyan darkgoldenrod darkgray darkgreen darkkhaki darkmagenta darkolivegreen darkorange darkorchid darkred darksalmon darkseagreen darkslateblue darkslategray darkturquoise darkviolet deeppink deepskyblue dimgray dodgerblue firebrick floralwhite forestgreen fuchsia gainsboro ghostwhite gold goldenrod gray grey green greenyellow honeydew hotpink indianred indigo ivory khaki lavender lavenderblush lawngreen lemonchiffon lightblue lightcoral lightcyan lightgoldenrodyellow lightgray lightgreen lightpink lightsalmon lightseagreen lightskyblue lightslategray lightsteelblue lightyellow lime limegreen linen magenta maroon mediumaquamarine mediumblue mediumorchid mediumpurple mediumseagreen mediumslateblue mediumspringgreen mediumturquoise mediumvioletred midnightblue mintcream mistyrose moccasin navajowhite navy oldlace olive olivedrab orange orangered orchid palegoldenrod palegreen paleturquoise palevioletred papayawhip peachpuff peru pink plum powderblue purple red rosybrown royalblue saddlebrown salmon sandybrown seagreen seashell sienna silver skyblue slateblue slategray snow springgreen steelblue tan teal thistle tomato turquoise violet wheat white whitesmoke yellow yellowgreen".split(" ")),
        g = a("above absolute activeborder activecaption afar after-white-space ahead alias all all-scroll alternate always amharic amharic-abegede antialiased appworkspace arabic-indic armenian asterisks auto avoid avoid-column avoid-page avoid-region background backwards baseline below bidi-override binary bengali blink block block-axis bold bolder border border-box both bottom break break-all break-word button button-bevel buttonface buttonhighlight buttonshadow buttontext cambodian capitalize caps-lock-indicator caption captiontext caret cell center checkbox circle cjk-earthly-branch cjk-heavenly-stem cjk-ideographic clear clip close-quote col-resize collapse column compact condensed contain content content-box context-menu continuous copy cover crop cross crosshair currentcolor cursive dashed decimal decimal-leading-zero default default-button destination-atop destination-in destination-out destination-over devanagari disc discard document dot-dash dot-dot-dash dotted double down e-resize ease ease-in ease-in-out ease-out element ellipse ellipsis embed end ethiopic ethiopic-abegede ethiopic-abegede-am-et ethiopic-abegede-gez ethiopic-abegede-ti-er ethiopic-abegede-ti-et ethiopic-halehame-aa-er ethiopic-halehame-aa-et ethiopic-halehame-am-et ethiopic-halehame-gez ethiopic-halehame-om-et ethiopic-halehame-sid-et ethiopic-halehame-so-et ethiopic-halehame-ti-er ethiopic-halehame-ti-et ethiopic-halehame-tig ew-resize expanded extra-condensed extra-expanded fantasy fast fill fixed flat footnotes forwards from geometricPrecision georgian graytext groove gujarati gurmukhi hand hangul hangul-consonant hebrew help hidden hide higher highlight highlighttext hiragana hiragana-iroha horizontal hsl hsla icon ignore inactiveborder inactivecaption inactivecaptiontext infinite infobackground infotext inherit initial inline inline-axis inline-block inline-table inset inside intrinsic invert italic justify kannada katakana katakana-iroha keep-all khmer landscape lao large larger left level lighter line-through linear lines list-item listbox listitem local logical loud lower lower-alpha lower-armenian lower-greek lower-hexadecimal lower-latin lower-norwegian lower-roman lowercase ltr malayalam match media-controls-background media-current-time-display media-fullscreen-button media-mute-button media-play-button media-return-to-realtime-button media-rewind-button media-seek-back-button media-seek-forward-button media-slider media-sliderthumb media-time-remaining-display media-volume-slider media-volume-slider-container media-volume-sliderthumb medium menu menulist menulist-button menulist-text menulist-textfield menutext message-box middle min-intrinsic mix mongolian monospace move multiple myanmar n-resize narrower ne-resize nesw-resize no-close-quote no-drop no-open-quote no-repeat none normal not-allowed nowrap ns-resize nw-resize nwse-resize oblique octal open-quote optimizeLegibility optimizeSpeed oriya oromo outset outside outside-shape overlay overline padding padding-box painted page paused persian plus-darker plus-lighter pointer polygon portrait pre pre-line pre-wrap preserve-3d progress push-button radio read-only read-write read-write-plaintext-only rectangle region relative repeat repeat-x repeat-y reset reverse rgb rgba ridge right round row-resize rtl run-in running s-resize sans-serif scroll scrollbar se-resize searchfield searchfield-cancel-button searchfield-decoration searchfield-results-button searchfield-results-decoration semi-condensed semi-expanded separate serif show sidama single skip-white-space slide slider-horizontal slider-vertical sliderthumb-horizontal sliderthumb-vertical slow small small-caps small-caption smaller solid somali source-atop source-in source-out source-over space square square-button start static status-bar stretch stroke sub subpixel-antialiased super sw-resize table table-caption table-cell table-column table-column-group table-footer-group table-header-group table-row table-row-group telugu text text-bottom text-top textarea textfield thai thick thin threeddarkshadow threedface threedhighlight threedlightshadow threedshadow tibetan tigre tigrinya-er tigrinya-er-abegede tigrinya-et tigrinya-et-abegede to top transparent ultra-condensed ultra-expanded underline up upper-alpha upper-armenian upper-greek upper-hexadecimal upper-latin upper-norwegian upper-roman uppercase urdu url vertical vertical-text visible visibleFill visiblePainted visibleStroke visual w-resize wait wave wider window windowframe windowtext x-large x-small xor xx-large xx-small".split(" "));
    CodeMirror.defineMIME("text/css", {
        atMediaTypes: c,
        atMediaFeatures: d,
        propertyKeywords: e,
        colorKeywords: f,
        valueKeywords: g,
        hooks: {
            "<": function(a, b) {
                function c(a, b) {
                    for (var d = 0, e; null != (e = a.next());) {
                        if (2 <= d && ">" == e) {
                            b.tokenize = null;
                            break
                        }
                        d = "-" == e ? d + 1 : 0
                    }
                    return ["comment", "comment"]
                }
                if (a.eat("!")) return b.tokenize = c, c(a, b)
            },
            "/": function(a, c) {
                return a.eat("*") ? (c.tokenize = b, b(a, c)) : !1
            }
        },
        name: "css"
    });
    CodeMirror.defineMIME("text/x-scss", {
        atMediaTypes: c,
        atMediaFeatures: d,
        propertyKeywords: e,
        colorKeywords: f,
        valueKeywords: g,
        allowNested: !0,
        hooks: {
            ":": function(a) {
                return a.match(/\s*{/) ? [null, "{"] : !1
            },
            $: function(a) {
                a.match(/^[\w-]+/);
                return ":" == a.peek() ? ["variable", "variable-definition"] : ["variable", "variable"]
            },
            ",": function(a, b) {
                if ("propertyValue" == b.stack[b.stack.length - 1] && a.match(/^ *\$/, !1)) return ["operator", ";"]
            },
            "/": function(a, c) {
                return a.eat("/") ? (a.skipToEnd(), ["comment", "comment"]) : a.eat("*") ? (c.tokenize = b, b(a, c)) : ["operator", "operator"]
            },
            "#": function(a) {
                if (a.eat("{")) return ["operator", "interpolation"];
                a.eatWhile(/[\w\\\-]/);
                return ["atom", "hash"]
            }
        },
        name: "css"
    })
})();
CodeMirror.defineMode("htmlmixed", function(a, b) {
    function c(a, b) {
        var c = b.htmlState.tagName,
            d = g.token(a, b.htmlState);
        if ("script" == c && /\btag\b/.test(d) && ">" == a.current()) {
            (c = (c = a.string.slice(Math.max(0, a.pos - 100), a.pos).match(/\btype\s*=\s*("[^"]+"|'[^']+'|\S+)[^<]*$/i)) ? c[1] : "") && /[\"\']/.test(c.charAt(0)) && (c = c.slice(1, c.length - 1));
            for (var l = 0; l < k.length; ++l) {
                var m = k[l];
                if ("string" == typeof m.matches ? c == m.matches : m.matches.test(c)) {
                    m.mode && (b.token = e, b.localMode = m.mode, b.localState = m.mode.startState &&
                        m.mode.startState(g.indent(b.htmlState, "")));
                    break
                }
            }
        } else "style" == c && /\btag\b/.test(d) && ">" == a.current() && (b.token = f, b.localMode = h, b.localState = h.startState(g.indent(b.htmlState, "")));
        return d
    }

    function d(a, b, c) {
        var d = a.current(),
            e = d.search(b); - 1 < e ? a.backUp(d.length - e) : d.match(/<\/?$/) && (a.backUp(d.length), a.match(b, !1) || a.match(d));
        return c
    }

    function e(a, b) {
        return a.match(/^<\/\s*script\s*>/i, !1) ? (b.token = c, b.localState = b.localMode = null, c(a, b)) : d(a, /<\/\s*script\s*>/, b.localMode.token(a, b.localState))
    }

    function f(a, b) {
        return a.match(/^<\/\s*style\s*>/i, !1) ? (b.token = c, b.localState = b.localMode = null, c(a, b)) : d(a, /<\/\s*style\s*>/, h.token(a, b.localState))
    }
    var g = CodeMirror.getMode(a, {
            name: "xml",
            htmlMode: !0
        }),
        h = CodeMirror.getMode(a, "css"),
        k = [],
        l = b && b.scriptTypes;
    k.push({
        matches: /^(?:text|application)\/(?:x-)?(?:java|ecma)script$|^$/i,
        mode: CodeMirror.getMode(a, "javascript")
    });
    if (l)
        for (var q = 0; q < l.length; ++q) {
            var m = l[q];
            k.push({
                matches: m.matches,
                mode: m.mode && CodeMirror.getMode(a, m.mode)
            })
        }
    k.push({
        matches: /./,
        mode: CodeMirror.getMode(a, "text/plain")
    });
    return {
        startState: function() {
            var a = g.startState();
            return {
                token: c,
                localMode: null,
                localState: null,
                htmlState: a
            }
        },
        copyState: function(a) {
            if (a.localState) var b = CodeMirror.copyState(a.localMode, a.localState);
            return {
                token: a.token,
                localMode: a.localMode,
                localState: b,
                htmlState: CodeMirror.copyState(g, a.htmlState)
            }
        },
        token: function(a, b) {
            return b.token(a, b)
        },
        indent: function(a, b) {
            return !a.localMode || /^\s*<\//.test(b) ? g.indent(a.htmlState, b) : a.localMode.indent ? a.localMode.indent(a.localState,
                b) : CodeMirror.Pass
        },
        electricChars: "/{}:",
        innerMode: function(a) {
            return {
                state: a.localState || a.htmlState,
                mode: a.localMode || g
            }
        }
    }
}, "xml", "javascript", "css");
CodeMirror.defineMIME("text/html", "htmlmixed");
ExtendClass(Janvas_document2D, XOS_EditableDocument2D);

function Janvas_document2D(a, b, c) {
    Janvas_document2D.baseConstructor.call(this, a, b, c);
    this.defaultFileExtension = "html"
}
_ = Janvas_document2D.prototype;
_.onShow = function(a) {
    this.onResize();
    this.showSelectionProperties();
    this.setActiveToolByName(null, HTML("SELECT_GLOBAL_2D_toolButton"))
};
_.onHide = function() {};
_.editIncludes = function() {
    this.appRef.includesEditWin.open(!0);
    HTML("CSS_INCLUDES_TEXT_CONTAINER").value = this.cssIncludes.join("\n");
    HTML("JS_INCLUDES_TEXT_CONTAINER").value = this.jsIncludes.join("\n")
};
_.updateEditedIncludes = function() {
    this.cssIncludes = HTML("CSS_INCLUDES_TEXT_CONTAINER").value.split("\n");
    this.jsIncludes = HTML("JS_INCLUDES_TEXT_CONTAINER").value.split("\n");
    this.appRef.includesEditWin.close();
    this.onChange()
};
_.updateEditedInlineContent = function() {
    this.appRef.inlineContentEdit_win.displayObjectTarget.inlineContent = this.appRef.inlineContentEdit_win.codemirror.getValue();
    this.appRef.inlineContentEdit_win.close();
    this.onChange()
};
_.editInlineContent = function(a) {
    this.appRef.inlineContentEdit_win.displayObjectTarget = a;
    this.appRef.inlineContentEdit_win.open(!0);
    this.appRef.inlineContentEdit_win.codemirror.setValue(a.inlineContent)
};
_.editTitleDescription = function(a) {
    this.appRef.titleDescriptionEditWin.displayObjectTarget = a;
    this.appRef.titleDescriptionEditWin.open(!0);
    HTML("ELEMENT_TITLE").value = a.title;
    HTML("ELEMENT_DESCRIPTION").value = a.description
};
_.updateEditedTitleDescription = function() {
    var a = this.appRef.titleDescriptionEditWin.displayObjectTarget;
    a.title = HTML("ELEMENT_TITLE").value;
    a.description = HTML("ELEMENT_DESCRIPTION").value;
    this.appRef.titleDescriptionEditWin.close();
    this.onChange();
   // console.log(a)
};
_.showSelectionProperties = function() {
    if (this.currentEditableGraphicObj) {
        var a = this.currentEditableGraphicObj.getParentByClass(XOS_Layer);
        a && (this.currentLayer = a)
    } else this.currentEditableGraphicObj = this;
    this.appRef.inspectorPanel.update(this.currentEditableGraphicObj);
    this.appRef.layersPanel.update(this);
    this.appRef.effectsPanel.update(this.currentEditableGraphicObj);
    this.appRef.animationsPanel.update(this.currentEditableGraphicObj)
};
_.onMouseDown = function(a) {
    this.isTextEditingAction ? this.updateEditedGraphicObjectText() : 3 == a.which ? this.appRef.contextMenus.edit2DContextMenu.showAt(a.clientX, a.clientY) : Janvas_document2D.superClass.onMouseDown.call(this, a)
};
_.doKeyDownAction = function(a) {
    Janvas_document2D.superClass.doKeyDownAction.call(this, a);
    var b = a.keyCode;
    if (!0 == a.metaKey || !0 == a.ctrlKey) {
        switch (b) {
            case 83:
                this.appRef.saveDocument()
        }
        return EventPreventDefault(a)
    }
    switch (b) {
        case 86:
            this.setActiveToolByName("selectGlobal_tool", HTML("SELECT_GLOBAL_2D_toolButton"));
            break;
        case 65:
            this.setActiveToolByName("selectLocal_tool", HTML("SELECT_LOCAL_2D_toolButton"));
            break;
        case 90:
            this.setActiveToolByName("zoomView_tool", HTML("ZOOM_2D_toolButton"));
            break;
        case 78:
            this.setActiveToolByName("drawFreeHand_tool",
                HTML("FREEHAND_2D_toolButton"));
            break;
        case 80:
            this.setActiveToolByName("drawPath_tool", HTML("PATH_2D_toolButton"));
            break;
        case 76:
            this.setActiveToolByName("drawLine_tool", HTML("LINE_2D_toolButton"));
            break;
        case 67:
            this.setActiveToolByName("drawEllipse_tool", HTML("ELLIPSE_2D_toolButton"));
            break;
        case 81:
            this.setActiveToolByName("drawRectangle_tool", HTML("RECT_2D_toolButton"));
            break;
        case 73:
            this.setActiveToolByName("eyeDropper_tool", HTML("EYEDROPPER_2D_toolButton"));
            break;
        case 82:
            this.setActiveToolByName("rotate_tool",
                HTML("ROTATE_2D_toolButton"))
    }
    return null
};
_.onDropDocumentOnDesktopAsImage = function(a) {
    TrackAnalyticsEvent("drop doc image to desktop");
    var b = this.appRef.documentsPanel.getDocumentTitle(this.htmlElement.idDocument),
        b = b.split(".");
    1 < b.length && b.pop();
    var b = b.join(b) + ".png",
        c = this.currentPageToImage();
    a.dataTransfer.setData("DownloadURL", "image/png:" + b + ":" + c)
};
_.onDropDocumentOnDesktopAsSVG = function(a) {
    var b = this.appRef.documentsPanel.getDocumentTitle(this.htmlElement.idDocument),
        c = (new XOS_DisplayObject2DSerializer(this)).serialize();
    a.dataTransfer.setData("DownloadURL", "image/svg+xml:" + b + ".svg:" + c)
};
_.onDocumentSaved = function(a) {
   // console.log("onDocumentSaved");
   // console.log(a);
    a.id && (this.htmlElement.id = a.id);
    this.isChanged = !1;
    this.appRef.documentsPanel.updateStates(this.htmlElement)
};
_.onChange = function() {
    !0 != this.isChanged && (Janvas_document2D.superClass.onChange.call(this), this.appRef.documentsPanel.updateStates(this.htmlElement))
};
_.onLoadDocumentFromDrive = function(a) {
    //alert('data');
    Janvas_document2D.superClass.onLoadDocumentFromDrive.call(this, a);
    this.appRef.documentsPanel.updateStates(this.htmlElement)
    console.log(a);
};
Janvas_document2D.createDocument = function(a, b, c) {
    return new Janvas_document2D(a, b, c)
};
                //479044035529-3auk06feta5391jgttphcj357dp8ldvt.apps.googleusercontent.com
var CLIENT_ID = "99074186391-2h2ehropbftrjora42aptkk5cornl3vn.apps.googleusercontent.com",
    SCOPES = ["https://www.googleapis.com/auth/drive", "https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile"],
    APPLICATION = null;

function init() {
    APPLICATION = new Janvas_application
}
// function myNewDocuments() {
//     APPLICATION = new Janvas_application
// }
window.addEventListener("load", init, !1);
ExtendClass(Janvas_application, XOS_Application);

function Janvas_application() {
    //console.log(this);
    Janvas_application.baseConstructor.call(this);
   this.createDocument("Home.html", null, "html", "localStorage:start_template_2d").addDefaultLayers();
    
    /*var usrIds=$("#userId").val();
    var projectId=$("#project_ids").val();  
   $.ajax({
    type:"GET",
    url:base_url+'script/saveDesign/get_All_file.php',
    data:"usrIds="+usrIds,
    success:function(resultsCnt)
    {
        console.log(resultsCnt);
    }
   });*/ 
    setTimeout(Bind(this, this.initFrameInterval), 10)
}
_ = Janvas_application.prototype;
_.initFrameInterval = function() {
    function a(c) {
        b.activeDocumentController && b.activeDocumentController.draw();
        window.requestAnimationFrame(a)
    }
    var b = this;
    a()
};
_.createDocumentByType = function(a, b, c, d) {
    d = null;
    switch (a) {
        case "svg":
        case "html":
        case "htm":
        case "php":
            d = Janvas_document2D.createDocument(this, b, c), this.documentsPanel.addPanel(d.htmlElement), d.onResize(), d.fitToView()
    }
    return d
};
_.initGlobalInterface = function() {
    function a() {
        var a = Number(this.getAttribute("value"));
        c.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
            lineWidth: a
        }, !0);
        c.activeDocumentController.lineWidth = a;
        c.activeDocumentController.showSelectionProperties()
    }

    function b(a) {
        a = this.getAttribute("effectClassName");
        window[a] && (a = new window[a](c.activeDocumentController), "" != this.getAttribute("effectProperties") && a.setProperties(JSON.parse(this.getAttribute("effectProperties").replace(/'/g, '"'))),
            c.activeDocumentController.addEffectToSelectedObjects_undable(a), c.activeDocumentController.showSelectionProperties())
    }
    Janvas_application.superClass.initGlobalInterface.call(this);
    var c = this;
    this.resourcesWin = new XOS_Window(HTML("RESOURCES_WIN"));
    this.layersWin = new XOS_Window(HTML("LAYERS_WIN"));
    this.effectsWin = new XOS_Window(HTML("EFFECTS_WIN"));
    this.animationsWin = new XOS_Window(HTML("ANIMATIONS_WIN"));
    this.driveFileBrowserWin = new XOS_Window(HTML("DRIVE_FILE_BROWSER_WIN"));
    this.includesEditWin = new XOS_Window(HTML("INCLUDES_EDIT_WIN"));
    CreateEventListener(HTML("SAVE_INCLUDES_BUTTON"), "click", function() {
        c.activeDocumentController.updateEditedIncludes()
    });
    this.inlineContentEdit_win = new XOS_Window(HTML("INLINE_CONTENT_EDIT_WIN"));
    CreateEventListener(HTML("UPDATE_INLINE_CONTENT_BUTTON"), "click", function() {
        c.activeDocumentController.updateEditedInlineContent()
    });
    this.inlineContentEdit_win.textContainer = HTML("INLINE_CONTENT_TEXT_CONTAINER");
    this.inlineContentEdit_win.codemirror = CodeMirror.fromTextArea(this.inlineContentEdit_win.textContainer, {
        name: "htmlmixed",
        mode: "text/html"
    });
    this.inlineContentEdit_win.codemirror.setSize("100%", "100%");
    this.titleDescriptionEditWin = new XOS_Window(HTML("TITLE_DESCRIPTION_EDIT_WIN"));
    CreateEventListener(HTML("SAVE_TITLE_DESCRIPTION_BUTTON"), "click", function() {
        c.activeDocumentController.updateEditedTitleDescription()
    });
    this.layersPanel = new Layers2D_panel(HTML("LAYERS_2D_PANEL"), c);
    this.effectsPanel = new Effects2D_panel(HTML("EFFECTS_2D_PANEL"), c);
    this.animationsPanel = new Animations2D_panel(HTML("ANIMATIONS_2D_PANEL"),
        c);
    this.fontsLibraryPanel = new FontsLibrary_panel(HTML("FONTS_LIBRARY_PANEL"), c, base_url);
    this.gradientsLibraryPanel = new GradientsLibrary_panel(HTML("GRADIENTS_LIBRARY_PANEL"), c, base_url);
    this.patternsLibraryPanel = new PatternsLibrary_panel(HTML("PATTERNS_LIBRARY_PANEL"), c, "../newHappyEdit");
    this.symbolsLibraryPanel = new SymbolsLibrary2D_panel(HTML("SYMBOLS_LIBRARY_PANEL"), c, "../newHappyEdit");
    CreateEventListener(HTML("RESOURCES_WIN_button"), "mousedown", function() {
        c.resourcesWin.open(!0)
    });
    CreateEventListener(HTML("LAYERS_WIN_button"), "mousedown", function() {
        c.layersWin.open(!0)
    });
    CreateEventListener(HTML("EFFECTS_WIN_button"), "mousedown", function() {
        c.effectsWin.open(!0)
    });
    CreateEventListener(HTML("ANIMATIONS_WIN_button"), "mousedown", function() {
        c.animationsWin.open(!0)
    });
    CreateEventListener(HTML("DRIVE_FILE_BROWSER_WIN_button"), "mousedown", function() {
        c.driveFileBrowserWin.open(!0);
        $("#DRIVE_FILE_BROWSER_WIN").show();
        $("#DRIVE_FILE_BROWSER_PANEL").load(location.href+" #DRIVE_FILE_BROWSER_PANEL");
       // $(".myFilesContents").load('allfiles.php');
        "" == c.driveFileBrowserPanel.htmlViewElement.innerHTML && c.driveFileBrowserPanel.loadFileList()
    });
    for (var d =
            function(a) {
                c.activeDocumentController.setActiveToolByName(null, this);
                return EventPreventDefault(a)
            }, e = HTML("MAIN_TOOLBAR_2D"), f = e.querySelectorAll('li[toolName$="_tool"]'), g = 0; g < f.length; g++) CreateEventListener(f[g], "mousedown", d);
    CreateEventListener(e.getChildById("SEND_BACK_toolButton"), "mousedown", function(a) {
        c.activeDocumentController.changeSelectedGraphicObjectsIndex_undable("back");
        return EventPreventDefault(a)
    });
    CreateEventListener(e.getChildById("BRING_FRONT_toolButton"), "mousedown", function(a) {
        c.activeDocumentController.changeSelectedGraphicObjectsIndex_undable("front");
        return EventPreventDefault(a)
    });
    CreateEventListener(e.getChildById("BRING_UP_toolButton"), "mousedown", function(a) {
        c.activeDocumentController.changeSelectedGraphicObjectsIndex_undable(1);
        return EventPreventDefault(a)
    });
    CreateEventListener(e.getChildById("BRING_DOWN_toolButton"), "mousedown", function(a) {
        c.activeDocumentController.changeSelectedGraphicObjectsIndex_undable(-1);
        return EventPreventDefault(a)
    });
    CreateEventListener(e.getChildById("TEXT_ALIGN_LEFT"), "mousedown", function() {
        c.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
            textAlign: "left"
        }, !0)
    });
    CreateEventListener(e.getChildById("TEXT_ALIGN_CENTER"), "mousedown", function() {
        c.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
            textAlign: "center"
        }, !0)
    });
    CreateEventListener(e.getChildById("TEXT_ALIGN_RIGHT"), "mousedown", function() {
        c.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
            textAlign: "right"
        }, !0)
    });
    CreateEventListener(e.getChildById("ALIGN_LEFT"), "mousedown", function() {
        c.activeDocumentController.alignSelectedGraphicObjects_undable("left")
    });
    CreateEventListener(e.getChildById("ALIGN_VCENTER"), "mousedown", function() {
        c.activeDocumentController.alignSelectedGraphicObjects_undable("vcenter")
    });
    CreateEventListener(e.getChildById("ALIGN_RIGHT"), "mousedown", function() {
        c.activeDocumentController.alignSelectedGraphicObjects_undable("right")
    });
    CreateEventListener(e.getChildById("ALIGN_TOP"), "mousedown", function() {
        c.activeDocumentController.alignSelectedGraphicObjects_undable("top")
    });
    CreateEventListener(e.getChildById("ALIGN_HCENTER"), "mousedown",
        function() {
            c.activeDocumentController.alignSelectedGraphicObjects_undable("hcenter")
        });
    CreateEventListener(e.getChildById("ALIGN_BOTTOM"), "mousedown", function() {
        c.activeDocumentController.alignSelectedGraphicObjects_undable("bottom")
    });
    CreateEventListener(e.getChildById("DISTRIBUITE_LEFT"), "mousedown", function() {
        c.activeDocumentController.distributeSelectedGraphicObjects_undable("left")
    });
    CreateEventListener(e.getChildById("DISTRIBUITE_VCENTER"), "mousedown", function() {
        c.activeDocumentController.distributeSelectedGraphicObjects_undable("vcenter")
    });
    CreateEventListener(e.getChildById("DISTRIBUITE_RIGHT"), "mousedown", function() {
        c.activeDocumentController.distributeSelectedGraphicObjects_undable("right")
    });
    CreateEventListener(e.getChildById("DISTRIBUITE_TOP"), "mousedown", function() {
        c.activeDocumentController.distributeSelectedGraphicObjects_undable("top")
    });
    CreateEventListener(e.getChildById("DISTRIBUITE_HCENTER"), "mousedown", function() {
        c.activeDocumentController.distributeSelectedGraphicObjects_undable("hcenter")
    });
    CreateEventListener(e.getChildById("DISTRIBUITE_BOTTOM"),
        "mousedown",
        function() {
            c.activeDocumentController.distributeSelectedGraphicObjects_undable("bottom")
        });
    CreateEventListener(e.getChildById("DISTRIBUITE_HSPACE"), "mousedown", function() {
        c.activeDocumentController.distributeSelectedGraphicObjects_undable("hspace")
    });
    CreateEventListener(e.getChildById("DISTRIBUITE_VSPACE"), "mousedown", function() {
        c.activeDocumentController.distributeSelectedGraphicObjects_undable("vspace")
    });
    CreateEventListener(e.getChildById("HOR_REFLECT_toolButton"), "mousedown", function(a) {
        c.activeDocumentController.reflectSelectedGraphicObjects_undable("horizontal");
        return EventPreventDefault(a)
    });
    CreateEventListener(e.getChildById("VER_REFLECT_toolButton"), "mousedown", function(a) {
        c.activeDocumentController.reflectSelectedGraphicObjects_undable("vertical");
        return EventPreventDefault(a)
    });
   CreateEventListener(e.getChildById("GROUP_menuItem"), "mousedown", function() {
        c.activeDocumentController.groupSelectedGraphicObjects();
        hideSubmenu("editMenus");
    });
    CreateEventListener(e.getChildById("UNGROUP_menuItem"), "mousedown", function() {
        c.activeDocumentController.ungroupSelectedGraphicObjects();
        hideSubmenu("editMenus");
    });
    CreateEventListener(e.getChildById("DUPLICATE_menuItem"),
        "mousedown",
        function() {
            c.activeDocumentController.duplicateWithTransformAllSelectedGraphicObjects_undable()
        });
    CreateEventListener(e.getChildById("LOCK_SELECTION_menuItem"), "mousedown", function() {
        c.activeDocumentController.lockSelectedGraphicObjects()
    });
    CreateEventListener(e.getChildById("UNLOCK_ALL_menuItem"), "mousedown", function() {
        c.activeDocumentController.unlockAllGraphicObjects()
    });
    CreateEventListener(e.getChildById("CREATE_COMPOSED_PATH_menuItem"), "mousedown", function() {
        c.activeDocumentController.createComposedPathFromSelection()
    });
    CreateEventListener(e.getChildById("CREATE_MASK_menuItem"), "mousedown", function() {
        c.activeDocumentController.createMaskFromSelection()
    });
    CreateEventListener(e.getChildById("CUT_menuItem"), "mousedown", function() {
        c.activeDocumentController.cutSelectedGraphicObjects();
       hideSubmenu("editMenus");
    }); 
    CreateEventListener(e.getChildById("COPY_menuItem"), "mousedown", function() {
        c.activeDocumentController.copySelectedGraphicObjects();
        hideSubmenu("editMenus");
    });
    CreateEventListener(e.getChildById("PASTE_menuItem"), "mousedown", function() {
        c.activeDocumentController.pasteObjects();
        hideSubmenu("editMenus");
    });
     CreateEventListener(e.getChildById("PASTEMY_menuItem"), "mousedown", function() {
        c.activeDocumentController.pasteMyObjects();
        hideSubmenu("editMenus");
    });
    CreateEventListener(e.getChildById("PASTE_IN_PLACE_menuItem"), "mousedown", function() {
        c.activeDocumentController.pasteObjects({
            pasteInPlace: !0
        });hideSubmenu("editMenus");
    });
    CreateEventListener(e.getChildById("SELECT_ALL_menuItem"), "mousedown", function() {
        c.activeDocumentController.selectAll();hideSubmenu("docMenus");
        hideSubmenu("editMenus");
    });
    CreateEventListener(e.getChildById("JOIN_POINTS_menuItem"), "mousedown", function() {
        c.activeDocumentController.joinSelectedPoints_undable();hideSubmenu("docMenus");
    });
    CreateEventListener(e.getChildById("TEXT_TO_PATH_menuItem"), "mousedown", function() {
        c.activeDocumentController.textToPath_undable()
    });
    c.contextMenus.gradientEditor = new GradientEditor_panel(HTML("GRADIENT_EDITOR"), c);
    g = HTML("EDIT_2D_CONTEXT_MENU");
    c.contextMenus.edit2DContextMenu = new XOS_ContextMenu(g);
    /*CreateEventListener(g.getChildById("GROUP_menuItem"), "mousedown", function() {
        c.activeDocumentController.groupSelectedGraphicObjects()
    });*/
    CreateEventListener(g.getChildById("UNGROUP_menuItem"), "mousedown", function() {
        c.activeDocumentController.ungroupSelectedGraphicObjects();
        hideSubmenu("editMenus");
    });
    CreateEventListener(g.getChildById("DUPLICATE_menuItem"), "mousedown",
        function() {
            c.activeDocumentController.duplicateWithTransformAllSelectedGraphicObjects_undable();
            hideSubmenu("editMenus");
        });
    CreateEventListener(g.getChildById("LOCK_SELECTION_menuItem"), "mousedown", function() {
        c.activeDocumentController.lockSelectedGraphicObjects();
        hideSubmenu("editMenus");
    });
    CreateEventListener(g.getChildById("UNLOCK_ALL_menuItem"), "mousedown", function() {
        c.activeDocumentController.unlockAllGraphicObjects();
        hideSubmenu("editMenus");
    });
    c.contextMenus.lineOptionsContextMenu = new XOS_ContextMenu(HTML("LINE_OPTIONS_CONTEXT_MENU"));
    d = HTML("LINE_OPTIONS_CONTEXT_MENU").querySelectorAll("#LINE_WIDTH_menuItem");
    for (g = 0; g < d.length; g++) CreateEventListener(d[g], "mousedown", a);
    CreateEventListener(HTML("STROKE_WIDTH_menuItem"), "mousedown", function(a) {
        c.contextMenus.lineOptionsContextMenu.showAt(a.pageX, a.pageY)
    });
    CreateEventListener(HTML("LINE_CAP_BUTT_menuItem"), "mousedown", function() {
        c.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
            lineCap: "butt"
        }, !0)
    });
    CreateEventListener(HTML("LINE_CAP_ROUND_menuItem"), "mousedown", function() {
        c.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
            lineCap: "round"
        }, !0)
    });
    CreateEventListener(HTML("LINE_CAP_SQUARE_menuItem"), "mousedown", function() {
        c.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
            lineCap: "square"
        }, !0)
    });
    CreateEventListener(HTML("LINE_JOIN_MITER_menuItem"), "mousedown", function() {
        c.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
            lineJoin: "miter"
        }, !0)
    });
    CreateEventListener(HTML("LINE_JOIN_ROUND_menuItem"), "mousedown", function() {
        c.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
            lineJoin: "round"
        }, !0)
    });
    CreateEventListener(HTML("LINE_JOIN_BEVEL_menuItem"), "mousedown", function() {
        c.activeDocumentController.setPropertiesOfSelectedGraphicObjects_undable({
            lineJoin: "bevel"
        }, !0)
    });
    c.inspectorPanel.update = function(a) {
        if (a) {
            this.currentEditableGraphicObj = a;
            var b = "pageProperties2DPanel";
            switch (this.currentEditableGraphicObj.type) {
                case "GROUP":
                case "SYMBOL":
                case "INCLUDE":
                case "SHAPE":
                case "IMAGE":
                case "TEXT":
                case "TEXTBOX":
                    b = "instanceProperties2DPanel";
                    break;
                case "EFFECT":
                    b = "effectProperties2DPanel";
                    break;
                case "CSS_ANIMATION":
                    b = "animationProperties2DPanel"
            }
            b = this.panels[b];
            this.setActiveRadioPanel(b.htmlElement);
            b.update(a)
        }
    };
    c.inspectorPanel.panels.pageProperties2DPanel = new PageProperties2D_panel(HTML("PAGE_PROPERTIES_2D_PANEL"), c);
    c.inspectorPanel.panels.instanceProperties2DPanel = new GraphicProperties2D_panel(HTML("GRAPHIC_PROPERTIES_2D_PANEL"), c);
    c.inspectorPanel.panels.effectProperties2DPanel = new EffectProperties2D_panel(HTML("EFFECT_PROPERTIES_2D_PANEL"), c);
    c.inspectorPanel.panels.animationProperties2DPanel =
        new AnimationProperties2D_panel(HTML("ANIMATION_PROPERTIES_2D_PANEL"), c);
    c.inspectorPanel.panels.fillAndStrokeProperties2DPanel = new FillAndStrokeProperties2D_panel(HTML("FILL_STROKE_PROPERTIES_PANEL"), c);
    CreateEventListener(HTML("FILL_menuItem"), "mousedown", function(a) {
        c.inspectorPanel.panels.fillAndStrokeProperties2DPanel.onClickOnFillButton(a)
    });
    CreateEventListener(HTML("STROKE_menuItem"), "mousedown", function(a) {
        c.inspectorPanel.panels.fillAndStrokeProperties2DPanel.onClickOnStrokeButton(a)
    });
    CreateEventListener(e.getChildById("EFFECT_OUTER_SHADOW_menuItem"),
        "mousedown", b);
    CreateEventListener(e.getChildById("EFFECT_INNER_SHADOW_menuItem"), "mousedown", b);
    CreateEventListener(e.getChildById("USE_IMAGE_AS_PATTERN_menuItem"), "mousedown", function() {
        c.activeDocumentController.useSelectedImageAsPattern()
    });
    CreateEventListener(e.getChildById("UNDO_toolButton"), "mousedown", function() {
        c.activeDocumentController.undo()
    });
    CreateEventListener(e.getChildById("REDO_toolButton"), "mousedown", function() {
        c.activeDocumentController.redo()
    });
    CreateEventListener(e.getChildById("CUSTOM_MENU_toolButton"), "mousedown", function() {
        c.activeDocumentController.custom_menu()
    });
    CreateEventListener(e.getChildById("TRASH_toolButton"),
        "mousedown",
        function() {
            c.activeDocumentController.removeSelectedGraphicObjects_undable()
        });
    CreateEventListener(e.getChildById("PRINT_toolButton"), "mousedown", function() {
        c.printDocument()
    });
    CreateEventListener(e.getChildById("CREATE_FILE_HTML_menuItem"), "mousedown", function(a) {
        var b = a.target;
        console.log(b);
        console.log(c);
        b.hasAttribute("fileExtension") && (a = b.getAttribute("docTitle"), b = b.getAttribute("fileExtension"), console.log("createNewDocument"), c.createDocument("Home" + "." + b, null, b).addDefaultLayers())
        $("#docMenus").hide();
        $("#TABS_DOCUMENTS li.selected").addClass("unsaved");
    });
    $("#addPages").click(function(a)
    {
        //console.log(c);
        document.getElementById("addPages").hasAttribute("fileExtension") && (a = document.getElementById("addPages").getAttribute("docTitle"), b = document.getElementById("addPages").getAttribute("fileExtension"), console.log("createNewDocument"), c.createDocument("Home" + "." + b, null, b).addDefaultLayers())
        $("#docMenus").hide();
        $("#TABS_DOCUMENTS li.selected").addClass("unsaved");

    });
        // CreateEventListener(e.getChildById("CREATE_FILE_HTML_menuItem"), "mousedown", function(a) {
        // var b = a.target;
        // b.hasAttribute("fileExtension") && (a = b.getAttribute("docTitle"), b = b.getAttribute("fileExtension"), console.log("createNewDocument"), c.createDocument("Home" + "." + b, null, b).addDefaultLayers())
        // $("#docMenus").hide();
        // $("#TABS_DOCUMENTS li.selected").addClass("unsaved");
        // });
    CreateEventListener(e.getChildById("OPEN_menuItem"), "mouseup", function() {
        c.chooseDocumentToOpen()
    });
    CreateEventListener(e.getChildById("SAVE_menuItem"), "mouseup", function(a) {
       //console.log(window.userSession)
            c.activeDocumentController.openInBrowserAsImage2("png");
            //console.log("youtube testing");
           // alert("youtube function called");
            a.shiftKey ? c.saveFontPreview() : c.saveDocument()
        
    });
    CreateEventListener(e.getChildById("SAVE_AS_menuItem"), "mouseup", function() {
        
             c.activeDocumentController.openInBrowserAsImage2("png");
             c.saveDocumentAs();
        //alert("youtube function called");
        
        
    });
    // CreateEventListener(e.getChildById("SAVE_AS_START_TEMPLATE_menuItem"), "mouseup", function() {
    //     // c.activeDocumentController.saveDocumentToLocalStorage("start_template_2d");
    //     //alert("The default template has been saved.")
    // });
    CreateEventListener(e.getChildById("RESET_START_TEMPLATE_menuItem"), "mouseup", function() {
        c.activeDocumentController.removeDocumentFromLocalStorage("start_template_2d");
       // alert("the default template has been removed.")
      // canvas.clearRect(0,0,canvas.width,canvas.height); 
    });
    CreateEventListener(e.getChildById("GENERATE_IMAGE_PNG_menuItem"), "mouseup", function(a) {
         c.activeDocumentController.openInBrowserAsImage2("png");
        c.activeDocumentController.openInBrowserAsImage("png");
    });
     CreateEventListener(e.getChildById("saveImgForGifs"), "mouseup", function(a) {
        c.activeDocumentController.openInBrowserAsImage2("png");
        c.activeDocumentController.saveGifs("png");
    });
    
    CreateEventListener(e.getChildById("GENERATE_IMAGE_JPG_menuItem"), "mouseup", function(a) {
        c.activeDocumentController.openInBrowserAsImage("jpg")
    });
    CreateEventListener(HTML("showPagePreview_button"), "click", function(a) {
        c.activeDocumentController.openInBrowserAsSVG(a)
    });
    CreateEventListener(e.getChildById("SAVE_menuItem"), "dragstart", function(a) {
        c.activeDocumentController.onDropDocumentOnDesktopAsSVG(a);

    })
};

function myCustomDialog(){
    $("#myoverlay").show();
     $( "#dialog" ).dialog();
     $("#filename").val($("#TABS_DOCUMENTS li.selected").text());
}
function updateDocumentToUserFolder(id,html,thumb,filename){
     var currentProject=$("#currentProject").text();
     $("#myoverlay").show();
    $( "#updateHtmlDesignFile" ).dialog();
     var uId=$("#userId").val();
     var tabId = id;
     var htmlData = html;
     //var htmlData = window.localStorage.getItem('start_template_2d');
    // var thumbPic = thumb;
    console.log(window.localStorage.getItem('start_template_2d'));
   var thumbPic = window.myCanvasPngImg[0];
     var fileName = filename;
     $.post(base_url+'script/saveDesign/designUpdate.php',{user_id:uId,tabId:tabId,htmlData:htmlData,thumbPic:thumbPic,fileName:fileName,currentProject:currentProject},function(data, status){
        // alert(data);
        console.log(thumbPic);
        $("#updateHtmlDesignFile").html("Your document is saved");
       $("#updateHtmlDesignFile").dialog("close");
        $("#myoverlay").hide();

     });
     
}
$( document ).ready(function() {
    $("#filename").keypress(function( e ) {
    if(e.which === 32) 
        return false;
        });
    $("#saveHtmlPage").click(function(){
            console.log($("#includeUrl").val());
            var uId=$("#userId").val();
            var projectId=$("#project_ids").val();
            //alert(projectId);
            var filename=$("#filename").val();
            if(filename=="" || filename ==".html")
            {
                alert("Invalid Name");
            }
            else
            {
                // $("#loadingPopups2").show();
                var myCanvas = document.getElementsByTagName("canvas");
               // var base64StringImg =window.myImgTempStorage[0];
               var base64StringImg =window.myCanvasPngImg[0];
               //console.log(base64StringImg);
                var htmlData = window.localStorage.getItem('start_template_2d');
                console.log(htmlData);
                var currentProject=$("#currentProject").text();
                console.log(currentProject);
                var overWriteData = '';
                // alert(filename); 
                $.post(base_url+'script/saveDesign/save.php',{img:base64StringImg,usrId:uId,fileName:filename,htmlData:htmlData,overWriteData:'',projectName:currentProject,projectId:projectId},function(data, status){
                    // alert(data);

                    if (data == 201) {
                        //alert('File name already used by you');

                        if (confirm('File name already used by you, Do you want to overwrite it?')) {
                            $.post(base_url+'script/saveDesign/save.php',{img:base64StringImg,usrId:uId,fileName:filename,htmlData:htmlData,overWriteData:'Yes',projectName:currentProject,projectId:projectId},function(data, status){
                                    $("#loadingPopups2").hide();
                                    alert('Document saved successfully!');
                                    $("#dialog").dialog("close");
                                    $("#myoverlay").hide();
                                    $("#filename").val("untitled.html");
                            });
                        }else{

                        }
                        
                    }else{
                        if (data == 102) {
                            alert('one or more of the special characters found in-'+ filename);
                        }else{
                            
                            $.post(base_url+'script/saveDesign/save.php',{img:base64StringImg,usrId:uId,fileName:filename,htmlData:htmlData,overWriteData:'',projectName:currentProject,projectId:projectId},function(data, status){

                                     $("#loadingPopups2").hide();
                                     alert('Document saved');
                                     $("#dialog").dialog("close");
                                     $("#myoverlay").hide();
                                     $("#filename").val("untitled.html");
                            });
                            

                        }
                       // $( ".ui-dialog" ).hide();
                    }
                });
             }
           
        });
     // var uId=$("#userId").val();
    // var myCanvas;
    // myCanvas = document.getElementsByTagName("canvas");
    // $.post(base_url+'script/saveDesign/save.php',{img:myCanvas[0].toDataURL("image/png"),usrId:uId,fileName:window.localStorage.getItem('start_template_2d')},function(data, status){
    //     alert(data);
    // });

    $("#DOCUMENTS").click(function()
    {
       $("#CONTEXT_MENUS div.activated").removeClass('activated');
       //$("#LAYERS_2D_PANEL li").last().hide();
    });

    /* $("#LAYERS_2D_PANEL li").on("click",function()
       {
        showHideToolBox();
    });  */
   

     //$('canvas').on('contextmenu', function(e){ return false; console.log("context menu closed");});

     $('#projectsName').keypress(function (e) {
         var key = e.which;
         if(key == 13)  // the enter key code
          {
            newProjects();
          }
        });
        $('#parentMenu').keypress(function (e) {
         var key = e.which;
         if(key == 13)  // the enter key code
          {
            createMenus();
          }
        });   
     // $("#TABS_DOCUMENTS li.selected").click(function()
     // {
     //    alert("click on tabs");
     // });
    $("#addPages").click(function()
    {
        //$("#docMenus li").eq(0).trigger("click");    
    });

    //sortable menu code start here..
                $('ol.sortable').nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div',
            helper: 'clone',
            items: 'li',
            opacity: .6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 3,

            isTree: true,
            expandOnHover: 700,
            startCollapsed: true
        });
               

        $('.disclose').on('click', function() {
            $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
        })

        $('#serialize').click(function(){
            serialized = $('ol.sortable').nestedSortable('serialize');
            $('#serializeOutput').text(serialized+'\n\n');
        })

        $('#toHierarchy').click(function(e){
            hiered = $('ol.sortable').nestedSortable('toHierarchy', {startDepthCount: 0});
            hiered = dump(hiered);
            (typeof($('#toHierarchyOutput')[0].textContent) != 'undefined') ?
            $('#toHierarchyOutput')[0].textContent = hiered : $('#toHierarchyOutput')[0].innerText = hiered;
        })

        $('#toArray').click(function(e){
            arraied = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});
            arraied = dump(arraied);
            (typeof($('#toArrayOutput')[0].textContent) != 'undefined') ?
            $('#toArrayOutput')[0].textContent = arraied : $('#toArrayOutput')[0].innerText = arraied;
        });

        $("#addItems").keypress(function(event) {
            if(event.which == 13)
            {
                addNewMenu();
            }       
        });

    //sortable menu code end here
});

function myFunction()
{
    return false;
    console.log("contextmenu close");
}
function previewImage(imPaths)
{
	$("#imagePreviewPopup").show();
	$(".prevTitle").text("Image Preview-"+imPaths);
	$("#ImgPreviewSrc").html("<img src='canvasPng/"+imPaths+"'>");
}

function closePop(img)
{
	$("#"+img).hide();
}
function hideSubmenu(menuIds)
{
    $("#"+menuIds).hide();
}
function setIconToShape(liIds,setImgs)
{
    $("#"+liIds+" img").eq(0).attr("src",setImgs);
}

function openFileBrowser()
{
   $("#fileUplaods").click();  
}
function imageEditPopup()
{
    $(".myImgEdits").show();
    $("#imgEditPopTab").hide();
}
function closeMyPopups()
{
    $(".myImgEdits").hide();
    $("#imgEditPopTab").hide();
}
function AnimationPanel()
{
    $("#frmAnim").slideToggle();
}
function generateGIFS()
{
    if($("#imagePanels li").length > 1)
    {
        if($("#delayTime").val() > 15 || $("#delayTime").val() < 0)
        {
            alert("please select delay time bellow 15");
        }
        else
        {
            var gifNames = prompt("Please enter GIF name", "untitled");
            var usrIds=$("#userId").val();
            var projectId=$("#project_ids").val();
            var currentProject=$("#currentProject").text();
            //alert(projectId); 
            console.log(currentProject);
            $.ajax({
                type:"GET",
                url:"../gifConv.php",
                data:"delayTimes="+$("#delayTime").val()+"&loops="+$("#loopTime").val()+"&usersIds="+usrIds+"&gifNames="+gifNames+"&projectId="+projectId+"&currentProject="+currentProject,
             success: function(datas)
             {
                 //alert("Image Save successfully..!");
                var win = window.open(datas, '_blank');
                if(win){
                    //Browser has allowed it to be opened
                    win.focus();
                }
                $("#imagePanels").html("");
             }
            });
        }
    
    }
    else
    {
        alert("Select more than one images");
    }
}

function showWin()
{   
    $("#blocks").hide();
    $("#RESOURCES_WIN").show();
    //$(".myHideCls").hide();
    //$("#videoIdss").show();
}
function closeVideoPnl()
{
    $("#RESOURCES_WIN").hide();
}
var showTemplates = function(tempCategories)
{
    $("#SYMBOLS_LIBRARY_button ul li").eq(0).attr("id","videoss");
    $("#SYMBOLS_LIBRARY_button ul li").eq(1).attr("id","ofices");

    //console.log(this.canvas);
    $("#RESOURCES_WIN").show();
    if(tempCategories=="websiteTemplates")
    {
        //$(".myHideCls").hide();
        //$("#webIdss").show();
    }
    else if(tempCategories=="invoiceTemplates")
    {
       $("#ofices").click();
    }
}
function removeFrames(framImgName)
{
           // var newId=framImgName.split(".",3);
            var newId=framImgName.split("s/",3);
            var newId1=newId[1].split(".", 3);
    $.ajax({
        type:"GET",
        url:"../deleteImgs.php",
        data:"imgNames="+framImgName,
        success:function()
        {
            //alert("image delete successfully");
            $("#"+newId1[0]).hide();

        }
    })
}
function rename_file()
{
    var file_oldname=$("#TABS_DOCUMENTS li.selected").text();
    var uId=$("#userId").val();
     var projectsName=$("#currentProject").text();
     console.log(projectsName);
    //console.log(uId); 
    var a=prompt("Rename this file",$("#TABS_DOCUMENTS li.selected").text());
    if(a==null)
    {}
    else
    {
        if($("#TABS_DOCUMENTS li.selected").hasClass('unsaved') == true)
        {
            $("#TABS_DOCUMENTS li.selected").find("div").text(a);
        }
        else
        {
            $.ajax({
            type:"GET",
            url:base_url+'script/saveDesign/rename_file.php',
            data:"oldname="+file_oldname+"&newName="+a+"&user_id="+uId+"&projectsName="+projectsName,
            success:function(results)
            {
                if(results=="renamed")
                {
                 alert("file rename successfully");
                 $("#TABS_DOCUMENTS li.selected").find("div").text(a);
                }
                else if(results == "exist")
                {
                    alert("File name is already exist");
                }
                else
                {
                    alert("file rename failed "); 
                }
            }
            });
        }
        
    }
}
function openProjects()
{
    // $("#projectsPanel").show();
    // $("#myoverlay").show();
    window.open(base_url+"ant_tool_page", "_blank");
}
function close_pro_div()
{
    $("#projectsPanel").hide();
    $("#myoverlay").hide();
}
function newProjects()
{

    var projectsName=$("#projectsName").val();
    if(projectsName == "")
    {
        alert("Please enter valid project name");
    }
    else
    {
        var uId=$("#userId").val();
        $.ajax({
            type:"GET",
            url:base_url+'script/saveDesign/new_projects.php',
            data:"projectsName="+projectsName+"&user_id="+uId,
            success:function(results)
            {
                //alert(results);
                $("#projectList").html(results);
                $("#projectsName").val("");
            }
        });
    }
    
}
$(window).load(function()
{
    //$("#myoverlay").show(); 
//    $("#projectsPanel").show();
    localStorage.setItem('projectName', "");
   var uIds=$("#userId").val();
   var proj_names=localStorage.getItem('projectName');
    if(proj_names==null)
    {
        
        $.ajax({
        type:"GET",
        url:base_url+"script/saveDesign/get_project_list.php",
        data:"uIds="+uIds,
        beforeSend: function() {
            //$("#loadingPopups2").show();
        },
         success:function(results)
        {
            //alert(results);
            $("#loadingPopups2").hide();
            $("#projectList").html(results);
        }
    });
    }
    else
    {
  //        $("#myoverlay").show(); 
//         $("#projectsPanel").show();
         $.ajax({
        type:"GET",
        url:base_url+"script/saveDesign/get_project_list.php",
        data:"uIds="+uIds,
        beforeSend: function() {
          //  $("#loadingPopups2").show();
        },
         success:function(results)
        {
            //alert(results);
            $("#loadingPopups2").hide();
            $("#projectList").html(results);
        }
        });
    }
});

function openThisProject(projectName)
{
    var proj_names=localStorage.getItem('projectName');
    if(proj_names==null)
    {
        var uIds=$("#userId").val();
        $("#currentProject").html("<b>"+projectName+"</b>");
        close_pro_div();
        // alert(window.location);
        // window.open(base_url+"index.php/ant_tool_page/tool_page/"+projectName,"_blank");
        localStorage.setItem('projectName', projectName);
        var fcookie='mycookie';
        document.cookie=fcookie+"=" + projectName;

        $.ajax({
            type:"GET",
            url:base_url+"script/saveDesign/get_project_id.php",
            data:"project_name="+projectName+"&uIds="+uIds,
            success:function(results)
            {
                //alert(results);
                $("#project_ids").val(results);
                //init();
                //location.reload(); 
               // window.addEventListener("load", init, 1); 
               $("#TABS_DOCUMENTS").html(""); 
               $("#DOCUMENTS").html("");
            }
        });
    }
    else
    {
        window.open(base_url+"ant_tool_page","_blank");
    }

    //$("#DRIVE_FILE_BROWSER_WIN").show();
} 
function closeOverlay()
{
    $("#myoverlay").hide();
}
function downloadPdf(userIds)
{
    var projectName=$("#currentProject").text();
    var p_id=$("#userId").val();
    var u_id=window.btoa(p_id);
    $.ajax({
        type:"GET",
        url:base_url+'dashboard/pdf/'+u_id+'/'+projectName,
        success:function(result)
        {
            alert(result);
        }
    });
}
// function addNewDocs()
// {
    
// }
function renameFiles()
{
    var getDefaultFileName=$("#TABS_DOCUMENTS li.selected").find("div").text();
    $("#TABS_DOCUMENTS li.selected").find("div").html("<input type='text' id='renameTexts' value='"+getDefaultFileName+"' onChange='renameMyFile()' style='width: 150px;'>");
    $("#renameTexts").focus();
}
function renameMyFile() 
{
    $("#TABS_DOCUMENTS li.selected").find("div").html($("#renameTexts").val());
}
function dublicate_page()
{
    
}
function getVideoTitle()
{
    var titles=$("#videoTitles").val();
    var video_urls=youtube_video_temp[0];
    videoContents.push({"video_title":titles,"video_urls":video_urls});
    console.log(videoContents);
}
function showMenuPopup()
{
    $("#myoverlay").show();
    $("#menuPopup").css("z-index","1000");
    $("#menuPopup").show();
}
function closeMenuPopups()
{
    $("#myoverlay").hide();
    $("#menuPopup").css("z-index","1000");
    $("#menuPopup").hide();
}

//menu sortable js start hebrew
$(document).ready(function(){

// $.ajax({
//      type:"POST",
//         url:base_url+"ant_menus/get_parent_menu/",
//         success:function(response)
//         {
//             var statuss=JSON.parse(response);
//             //alert(statuss[0].menu_name);
//             if(response == 404)
//             {
//                 alert("Menu loads failed");
//             }
//             else
//             {
//                 for(var i=0;i<statuss.length;i++)
//                 {
//                     $("#submenus").append("<option value='"+statuss[i].menu_id+"'>"+statuss[i].menu_name+"</option>");
//                 }
                
//             }
//         }

//     });
    $("#submenus").change(function()
    {
        var parent_menus=$(this).val();
         $.ajax({
                type:"GET",
                url:base_url+"ant_menus/get_submenu/"+parent_menus,
                success:function(response)
                {
                    var statuss=JSON.parse(response);
                    if(response == 404)
                    {
                        alert("Menu creation failed");
                    }
                    else
                    {
                        $("#demo ol").html("");
                        if(statuss == "")
                        {
                            $("#demo ol").html("<span class='menuTitles'>No submenus found !</span>");
                        }
                        else
                        {   
                            for(var i=0;i<statuss.length;i++)
                            {
                                $("#demo ol").first().append("<li id='list2_"+statuss[i].submenu_id+"' class='menu_verticals'><div><span class='disclose'><span></span></span>"+statuss[i].submenu_name+"<span style='float:right;' onclick=\"deleteThisMenu('list2_"+statuss[i].submenu_id+"')\">X</span><span style='float:right;margin-right:15px;' onclick=\"addToVisual('list2_"+statuss[i].submenu_id+"')\">Add to Visual</span></div>");
                            }
                        }
                              
                    }
                }
            });
    });
    
});

        
    function dump(arr,level) {
        var dumped_text = "";
        if(!level) level = 0;

        //The padding given at the beginning of the line.
        var level_padding = "";
        for(var j=0;j<level+1;j++) level_padding += "    ";

        if(typeof(arr) == 'object') { //Array/Hashes/Objects
            for(var item in arr) {
                var value = arr[item];

                if(typeof(value) == 'object') { //If it is an array,
                    dumped_text += level_padding + "'" + item + "' ...\n";
                    dumped_text += dump(value,level+1);
                } else {
                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                }
            }
        } else { //Strings/Chars/Numbers etc.
            dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
        }
        return dumped_text;
    }
    function getHtmlCode()
    {
        $("#htmlCodes").text($("#demo").html());
    }
    var menuIds=0;
    function addNewMenu()
    {
        var menus=$("#addItems").val();
        var parent_menu_id=$("#submenus").val();
        if(menus == "")
        {
            alert("Invalid menu name");
        }
        else
        {
            $.ajax({
                type:"POST",
                url:base_url+"ant_menus/add_submenu/"+menus+"/"+parent_menu_id,
                success:function(response)
                {
                    if(response == 404)
                    {
                        alert("Menu creation failed");
                    }
                    else
                    {
                        $("#demo ol").first().append("<li id='list2_"+menuIds+"' class='menu_verticals'><div><span class='disclose'><span></span></span>"+menus+"<span style='float:right;' onclick=\"deleteThisMenu('list2_"+menuIds+"')\">X</span><span style='float:right;margin-right:15px;' onclick=\"addToVisual('list2_"+menuIds+"')\">Add to Visual</span></div>");
                        $("#addItems").val("");
                    }
                }
            });
            
            menuIds++;
        }
        //$("#visualMenus").append("<li>"+menus+"<br><img src='black.png' class='imgSizew'><br><img src='blue.jpg' class='imgSizew menuDown' id='arrowDown_"+menuIds+"' onclick=\"arrowDown('arrowDown_"+menuIds+"')\"><img src='blue.jpg' class='imgSizew menuHorizontal' id='arrowHorizontal_"+menuIds+"' onclick=\"arrowHorrizontal('arrowHorizontal_"+menuIds+"')\" style='display:none;'></li>");
        
    }
    function deleteThisMenu(menu_id)
    {
        if(confirm("Do you want to delete this menu ?"))
        {
            $("#"+menu_id).remove();
        }
        else
        {}
    }
    function arrowDown(ids)
    {
        $("#"+ids).hide();
        var id_no=ids.split("_");
        $("#list2_"+id_no[1]).addClass("menu_horizontals").removeClass("menu_verticals");
        $("#"+ids).next().show();
    }
    function arrowHorrizontal(ids)
    {
        $("#"+ids).hide();
        var id_no=ids.split("_");
        $("#list2_"+id_no[1]).removeClass("menu_horizontals").addClass("menu_verticals");
        $("#"+ids).prev().show();
    }
    function addToVisual(menuIds)
    {
        var menuExist=0;
        var str=menuIds.split("_");
        var a=$("#"+menuIds).text();
        var menus=a.split("X");
        $("#visualMenus").append("<li>"+menus[0]+"<br><img src='"+base_url+"img/ant_tool_img/black.png' class='imgSizew'><br><img src='"+base_url+"img/ant_tool_img/blue.jpg' class='imgSizew menuDown' id='arrowDown_"+str[1]+"' onclick=\"arrowDown('arrowDown_"+str[1]+"')\"><img src='"+base_url+"img/ant_tool_img/blue.jpg' class='imgSizew menuHorizontal' id='arrowHorizontal_"+str[1]+"' onclick=\"arrowHorrizontal('arrowHorizontal_"+str[1]+"')\" style='display:none;'></li>");
        // $("#visualMenus li").each(function()
        // {
        //   if($(this).text() == menus[0])
        //   {
        //     alert("This menu is already exist");
        //   }
        //   else
        //   {
        //      menuExist=1;
        //   }
  
  //    });
  //    if(menuExist == 1)
  //    {
            
  //    }
  //    else
  //    {
            
  //    }
        
    }

    function saveMenu()
    {
        alert("save menu successfully");
    }
    function check_sessions()
    {
        var a;
       //alert("sadsdsd");
        $.ajax({
            type:"GET",
            url:base_url+"userprofile/check_sessions/",
            success:function(response)
            {
                 if(response == 101)
                 {
                    window.userSession=true;
                 }
                 else
                 {
                    window.userSession=false;
                 }
            }
        });
        //alert(a);
    }
//menu functionality start here
//Vipul Bhagwat
//date:7-09-2016
function createMenus()
{
    var uId=$("#userId").val();
    var projectId=$("#project_ids").val();
    var parentMenu=$("#parentMenu").val();
    //alert("user_id="+uId+"  Project_id="+projectId+" parentMenu="+parentMenu);
    $.ajax({
        type:"POST",
        url:base_url+"ant_menus/create_parent_menu/"+parentMenu+"/"+projectId+"/"+uId,
        success:function(response)
        {
            if(response == 404)
            {
                alert("Menu creation failed");
            }
            else
            {
                $("#submenus").append("<option value='"+response+"'>"+parentMenu+"</option>");
                $("#parentMenu").val("");
            }
        }
    })

}