var myApp = angular.module("myApp", ['ui.router']);
var base_url = "http://"+window.location.hostname+"/designer_tool/";
myApp.config(function($stateProvider, $urlRouterProvider){
	$urlRouterProvider.otherwise("/home");
	$stateProvider.state("/home",{
					url:"/home",
					templateUrl:base_url+"home/loadInitialHomeView"
					});
});