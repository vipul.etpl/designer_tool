
$(document).ready(function() {
$("#fname").bind('keypress',function(evt){
  var name=document.getElementById("fname").value;
  //alert(name.length);
  //regex = /^[A-z0-9]+$/;
   evt = (evt) ? evt : window.event
        var charCode = (evt.which) ? evt.which : event.keyCode        

         if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122) && (charCode < 31 || charCode > 33))
            return false;

        return true;
  //alert(name);
}) 

$("#pass").change(function(){
  var password=document.getElementById("pass").value;
  var length=password.length;
  var min="8";
  if(length>=min)
  {
  	$(".pmessage").hide();
  	return true;
  	
  }
  else
  {
  	$(".pmessage").show();
  	$(".pmessage").html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Length of password is minimum 8</div>');
  	return false;
  }
  
}) 

$("#cpp").change(function(){
  var cpassword=document.getElementById("cpp").value;
  var password=document.getElementById("pass").value;
  //var length=cpassword.length;
  //var min="8";
  //alert(cpassword);
  //alert(password);
  if(password==cpassword)
  {
  	$(".cmessage").hide();
  	return true;
  	
  }
  else
  {
  	$(".cmessage").show();
  	$(".cmessage").html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Confirm Password and Password should be matched</div>');
  	return false;
  }
  
}) 

$("#email").change(function(){
  var email=document.getElementById("email").value;
  if(document.getElementById("email").value == "Email ID" || document.getElementById("email").value.replace(/^\s+|\s+$/g,'') == "" || !document.getElementById("email").value.match(/^[a-zA-Z]+[a-zA-Z0-9]+@+[a-zA-Z\-]+[.]+[a-zA-Z\-]+$/))
  {
  	$(".emessage").show();
  	$(".emessage").html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Please enter a valid email id</div>');
  	document.getElementById("email").focus();
  	return false; 	
  }
  else
  {
  	$(".emessage").hide();
  	return true;
  }
  
}) 

});